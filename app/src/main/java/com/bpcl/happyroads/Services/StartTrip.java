package com.bpcl.happyroads.Services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.iid.FirebaseInstanceId;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.ExploreActivity;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.PushNotification;
import com.bpcl.happyroads.Pojo.TripCompleted;
import com.bpcl.happyroads.Pojo.UpdateDeviceToken;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.R;
import com.bpcl.happyroads.StartTripActivity;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.GPSTracker;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dhavalkaka on 18/11/2016.
 */
public class StartTrip extends Service {

    DBHandler dbHandler;
    List<Create_Trip> UpComingTripList;
    List<Create_Trip> OngoingTripList;
    List<PushNotification> pushNotificationList;
    Integer push_time_hrs = 2;
    String current_date_after = "", current_date_before = "";
    SharedPreferences preferences;
    GPSTracker gpsTracker;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    private Handler datehandler;
    private Runnable daterunnable;
    private Handler handler;
    private Runnable runnable;
    SessionManager sessionManager;

    public static boolean CheckDates(String startDate, String endDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
        boolean b = false;

        try {
            Log.d("checkdate", dfDate.parse(startDate.toString()) + "enddate " + dfDate.parse(endDate.toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = false;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare", String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }
    public static boolean CheckDates1(String startDate, String endDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
        boolean b = false;

        try {
            Log.d("checkdate", dfDate.parse(startDate.toString()) + "enddate " + dfDate.parse(endDate.toString()) +"  "+dfDate.parse(startDate).before(dfDate.parse(endDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = false;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = true; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("System out", "datecompare in checkdate 1:"+String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        dbHandler = new DBHandler(StartTrip.this);
        gpsTracker = new GPSTracker(StartTrip.this);
        UpComingTripList = new ArrayList<>();
        UpComingTripList = dbHandler.get_All_TRIP("UpComing");
        sessionManager = new SessionManager(this);
        OngoingTripList = new ArrayList<>();
        OngoingTripList = dbHandler.get_All_TRIP("OnGoing");
        preferences = getSharedPreferences(Constants.TripTrackPREFERENCES, 0);

        pushNotificationList = dbHandler.get_push_notification_list();
        preferences.edit().putString("servicepop", "").commit();
      // UpdateDeviceToken();

        datehandler = new Handler();
        daterunnable = new Runnable() {
            @Override
            public void run() {
                if (sessionManager.isLoggedIn()) {
                    UpComingTripList = new ArrayList<>();
                    UpComingTripList = dbHandler.get_All_TRIP("UpComing");

                    OngoingTripList = new ArrayList<>();
                    OngoingTripList = dbHandler.get_All_TRIP("OnGoing");
                    if(OngoingTripList.size()>0)
                    UpdateDeviceToken();
                }
                datehandler.postDelayed(daterunnable, 100000);
            }
        };
        datehandler.postDelayed(daterunnable, 100000);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (sessionManager.isLoggedIn())
                    date_popup();
                handler.postDelayed(runnable, 30000);

            }
        };
        handler.postDelayed(runnable, 10000);
    }

    private boolean isApplicationBroughtToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);

        if (!tasks.isEmpty()) {
            Log.d("System out", " ticket 5!---");
            ComponentName topActivity = tasks.get(0).topActivity;
            Log.d("System out", " ticket 5!---");
            String ActivityName = topActivity.getClassName().toString();
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                Log.d("System out", " in if " + topActivity.getPackageName() + " hello..hi.. " + context.getPackageName());
                return true;
            }

        }
        return false;
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.6;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }


 /*   private void sendNotification(String messageBody, Integer tripid) {

        Intent intent = new Intent(this, ExploreActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        *//*intent.putExtra("trip", "true");
        intent.putExtra("tripid", tripid);*//*

        preferences.edit().putString("servicepopup", "").commit();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(largeIcon)
                .setContentTitle("Happy Roads")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }*/
 private void sendNotification(String messageBody, Integer tripid) {

     Intent intent = new Intent(this, ExploreActivity.class);
     intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


     PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


     // Create remote view and set bigContentView.
     RemoteViews expandedView = new RemoteViews(this.getPackageName(),
             R.layout.custom_notification);
     expandedView.setTextViewText(R.id.tv_trip_name, "Happy Roads");
     expandedView.setTextViewText(R.id.tv_trip_date, messageBody);

     Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);
     Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

     Notification notification = new NotificationCompat.Builder(StartTrip.this)
             .setSmallIcon(R.drawable.ic_stat_directions_bike)
             .setLargeIcon(largeIcon)
             .setAutoCancel(true)
             .setContentIntent(pendingIntent)
             .setSound(defaultSoundUri)
             .setCustomBigContentView(expandedView)
             .setContentText(messageBody)
             .setContentTitle("Happy Roads").build();

     NotificationManager notificationManager =
             (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);

     notificationManager.notify(0, notification);
 }

    public void starttripdialog(final int tripid) {
        Intent intent = new Intent(StartTrip.this, StartTripActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("tripid", tripid);

        startActivity(intent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }




    public void date_popup() {
        try {
            Calendar calendarcurrent = Calendar.getInstance();
            calendarcurrent.set(Calendar.HOUR_OF_DAY, push_time_hrs);

            current_date_before = mFormatter.format(calendarcurrent.getTime());

            Calendar calendarcurrent1 = Calendar.getInstance();
            //  calendarcurrent1.set(Calendar.HOUR_OF_DAY, push_time_hrs*2);

            current_date_after = mFormatter.format(calendarcurrent1.getTime());

            for (int i = 0; i < UpComingTripList.size(); i++) {

                Calendar twohrsbefore = Calendar.getInstance();
                Calendar twohrsafter = Calendar.getInstance();
                Date datebrfore = null;

                try {


                    datebrfore = mFormatter.parse(UpComingTripList.get(i).getStartdate() + " " + UpComingTripList.get(i).getStarttime().trim());


                } catch (ParseException e) {
                    e.printStackTrace();
                }
                twohrsbefore.setTime(datebrfore);
                twohrsafter.setTime(datebrfore);
                twohrsbefore.add(Calendar.HOUR, -push_time_hrs);
                twohrsafter.add(Calendar.HOUR, +push_time_hrs);

                String startdate = mFormatter.format(twohrsbefore.getTime());
                String tripactualDate = mFormatter.format(twohrsafter.getTime());

                Calendar calendar = Calendar.getInstance();

                String currentdate = mFormatter.format(calendar.getTime());

                if (CheckDates(startdate, currentdate)) {
                    sendNotification(UpComingTripList.get(i).getTripname() + " trip ready to start", UpComingTripList.get(i).getTripId());
                    //  preferences.edit().putString("servicepop", "true").commit();
                }


                if (gpsTracker.isGPSEnabled) {
                    try {
                        double distanceofmarkers;

                        distanceofmarkers = distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(), UpComingTripList.get(i).getSource_latitude(), UpComingTripList.get(i).getSource_longitude());

                        //double distanceofmarkers = distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(), UpComingTripList.get(i).getSource_latitude(), UpComingTripList.get(i).getSource_longitude());
                        if ((distanceofmarkers * 1000) < 1000 && preferences.getString("trackpopup", "true").equalsIgnoreCase("") && (mFormatter.parse(currentdate).after(mFormatter.parse(startdate)) && mFormatter.parse(currentdate).before(mFormatter.parse(tripactualDate)))) {
                            starttripdialog(UpComingTripList.get(i).getTripId());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (UpComingTripList.get(i).getReturnstatus())

                    {
                        twohrsbefore = Calendar.getInstance();
                        try {


                            datebrfore = mFormatter.parse(UpComingTripList.get(i).getReturn_start_date() + " " + UpComingTripList.get(i).getReturn_start_time().trim());


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        twohrsbefore.setTime(datebrfore);
                        twohrsbefore.add(Calendar.HOUR, -push_time_hrs);

                        startdate = mFormatter.format(twohrsbefore.getTime());
                        if (CheckDates(startdate, currentdate)) {
                            sendNotification(UpComingTripList.get(i).getTripname() + " return trip ready to start", UpComingTripList.get(i).getTripId());
                            //  preferences.edit().putString("servicepop", "true").commit();
                        }
                    }

                }
           /* try {

                if ((mFormatter.parse(UpComingTripList.get(i).getStartdate() + " " + UpComingTripList.get(i).getStarttime() + ":01").after(mFormatter.parse(current_date_before)) || (mFormatter.parse(UpComingTripList.get(i).getStartdate() + " " + UpComingTripList.get(i).getStarttime() + ":01").before(mFormatter.parse(current_date_before)))) && preferences.getString("servicepop", "").equalsIgnoreCase("")) {
                    sendNotification(UpComingTripList.get(i).getTripname() + " trip ready to start", i);
                  //  preferences.edit().putString("servicepop", "true").commit();

                    double distanceofmarkers = distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(), UpComingTripList.get(i).getSource_latitude(), UpComingTripList.get(i).getSource_longitude());
                    if ((distanceofmarkers * 1000) < 1000 && preferences.getString("servicepopup", "").equalsIgnoreCase("")) {
                        starttripdialog(UpComingTripList.get(i).getTripId());
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }*/
            }

            for (int i = 0; i < OngoingTripList.size(); i++) {

                Date dateReturnend = null;
                Date dateTripend = null;


                try {

                    Calendar calendar1 = Calendar.getInstance();
                    String currentdate = mFormatter.format(calendar1.getTime());

                    if (OngoingTripList.get(0).getTripIsOneSideCompleted() && OngoingTripList.get(0).getReturnstatus()) {

                        dateReturnend = mFormatter.parse(OngoingTripList.get(i).getReturn_end_date() + " " + OngoingTripList.get(i).getReturn_end_time());
                    } else {
                        dateTripend = mFormatter.parse(OngoingTripList.get(i).getEnddate() + " " + OngoingTripList.get(i).getEndtime());
                    }


                } catch (ParseException e) {
                    e.printStackTrace();
                }


                //one way trip end

                if (dateTripend != null)
                {
                    Calendar thirtyminbefore = Calendar.getInstance();

                    thirtyminbefore.setTime(dateTripend);
                    thirtyminbefore.add(Calendar.MINUTE, 30);

                    String thiryminafterdate = mFormatter.format(thirtyminbefore.getTime());

                    Calendar calendar = Calendar.getInstance();

                    String currentdate = mFormatter.format(calendar.getTime());

                    if (CheckDates1(currentdate, thiryminafterdate) && preferences.getBoolean("trip_start_tracking", false)) {
                        preferences.edit().putBoolean("trip_start_tracking", false).commit();
                        preferences.edit().putString("DirectionJson", "").commit();
                        NotificationManager notificationManager =
                                (NotificationManager) StartTrip.this.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancelAll();


                        if ( OngoingTripList.get(0).getReturnstatus())
                        {
                            update_trip_one_way_completed(OngoingTripList.get(i).getTripId(), "OnGoing");
                        }
                        else
                        {
                            update_trip_one_way_completed(OngoingTripList.get(i).getTripId(), "Completed");
                        }




                    }
                }
                //Return way trip end
                if (dateReturnend != null) {
                    Calendar thirtyminbefore1 = Calendar.getInstance();
                    thirtyminbefore1.setTime(dateReturnend);
                    thirtyminbefore1.add(Calendar.MINUTE, 30);

                    String thiryminafterdate = mFormatter.format(thirtyminbefore1.getTime());

                    Calendar calendar = Calendar.getInstance();

                    String currentdate = mFormatter.format(calendar.getTime());

                    if (CheckDates1(currentdate, thiryminafterdate) && preferences.getBoolean("trip_start_tracking", false)) {
                        preferences.edit().putBoolean("trip_start_tracking", false).commit();
                        preferences.edit().putString("DirectionJson", "").commit();
                        NotificationManager notificationManager =
                                (NotificationManager) StartTrip.this.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancelAll();
                        update_trip_one_way_completed(OngoingTripList.get(i).getTripId(), "Completed");

                    }
                }
            }
        }catch (Exception e){}

    }
    public void update_trip_one_way_completed(final Integer tripid,String tripstatus)
    {
        try{

            preferences.edit().putBoolean("trip_start_tracking", false).commit();
            preferences.edit().putString("DirectionJson", "").commit();


            Boolean tripisonesidecompleted=false;




                tripisonesidecompleted=true;


            String json ="";

            json ="[{     \"TripId\": \""+tripid+"\", \"TripIsOneSideCompleted\":"+tripisonesidecompleted+",\"TripStatus\":\""+tripstatus+"\"}]";

            Log.d("trip_completed_services", json);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ArrayList<TripCompleted>> call = apiService.UpdateTripToOneSideCompleted(json);
            final Boolean finalTripisonesidecompleted = tripisonesidecompleted;
            final String finalTripstatus = tripstatus;
            final String finalTripstatus1 = tripstatus;
            call.enqueue(new Callback<ArrayList<TripCompleted>>() {
                @Override
                public void onResponse(Call<ArrayList<TripCompleted>> call, final Response<ArrayList<TripCompleted>> response) {

                    if(!response.body().isEmpty()) {
                        if(response.body().get(0).getResStatus())
                        {

                          update_trip(tripid, finalTripstatus);
                        }
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<TripCompleted>> call, Throwable t) {
                    // Log error here since request failed
                    dbHandler.update_trip_one_way_completed(tripid, finalTripstatus1,1);


                    OngoingTripList = new ArrayList<>();
                    OngoingTripList = dbHandler.get_All_TRIP("OnGoing");
                    t.printStackTrace();


                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void update_trip(Integer tripid, final String tripstatus ) {
        try {
           final List<Create_Trip>  createTripList = dbHandler.get_TRIP_By_TripId(tripid);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String tripdate="",tripenddate="",returndate="",returndatetimeeta="";
            tripdate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime() + ":00";
            tripenddate = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime() + ":00";

            returndate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time() + ":00";


            returndatetimeeta = createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time() + ":00";

            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {

                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            if(returndate.length()>3) {
                try {
                    returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {

                    returndate = "";
                }
            }

            if(returndatetimeeta.length()>3) {
                try {
                    returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

                } catch (ParseException e) {

                    returndatetimeeta = "";

                }
            }


            String json = "[{     \"TripName\": \"" + createTripList.get(0).getTripname() + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(0).getSourceId() + ",\"TripDestination_End\":" + createTripList.get(0).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + createTripList.get(0).getTripId() + "\",\"TripStatus\": \"" + tripstatus + "\",\"TripGooglePlaceDetails\":\"" + createTripList.get(0).getDes_address() +"\",\"TripKm\": \""+createTripList.get(0).getKM()+"\" ,\"TripTime\":\""+createTripList.get(0).getTime()+"\",\"TripReturnKm\":\""+createTripList.get(0).getReturnKM()+"\",\"TripReturnTime\":\""+createTripList.get(0).getReturnTime()+"\" }]";

            Log.d("json_CreateTrip1", json);

             Call<UpdateTrip> call = apiService.updateTrip(json);
            call.enqueue(new Callback<UpdateTrip>() {
                @Override
                public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                    if (response.body() != null)
                    {


                            dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(),tripstatus,0);


                        OngoingTripList = new ArrayList<>();
                        OngoingTripList = dbHandler.get_All_TRIP("OnGoing");
                    }
                }

                @Override
                public void onFailure(Call<UpdateTrip> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(),tripstatus,1);


                    OngoingTripList = new ArrayList<>();
                    OngoingTripList = dbHandler.get_All_TRIP("OnGoing");

                }
            });
        } catch (Exception e) {
        }
    }
    private void UpdateDeviceToken() {


        //  pb_dialog.show();
        try {


            if (gpsTracker.isGPSEnabled) {
                Location location = gpsTracker.getLocation();

                if (location != null) {

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    String json = "[{\"umDeviceType\": \"" + "Android" + "\",\"umUDID\":\"" + FirebaseInstanceId.getInstance().getToken() + "\",\"umLat\":\"" + location.getLatitude() + "\",\"umLong\":\"" + location.getLongitude() + "\",\"umId\":\"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
                    Log.d("System out", " update device token json" + json);
                    Call<ArrayList<UpdateDeviceToken>> call = apiService.DeviceTokenUpdate(json);
                    call.enqueue(new Callback<ArrayList<UpdateDeviceToken>>() {
                        @Override
                        public void onResponse(Call<ArrayList<UpdateDeviceToken>> call, Response<ArrayList<UpdateDeviceToken>> response) {
                            if (response.body() != null) {


                                if (response.body().get(0).getResStatus()) {
                                    Log.d("System out", "Device token update successfully.");
                                }


                            }


                        }

                        @Override
                        public void onFailure(Call<ArrayList<UpdateDeviceToken>> call, Throwable t) {
                            // Log error here since request failed

                            t.printStackTrace();


                        }

                    });
                }
            }
        }catch (Exception e)
        {

        }

    }
}
