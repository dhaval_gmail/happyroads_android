package com.bpcl.happyroads.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.ExploreActivity;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.PushNotification;
import com.bpcl.happyroads.R;
import com.bpcl.happyroads.StartTripActivity;
import com.bpcl.happyroads.Utils.GPSTracker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by fipl on 03/12/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    DBHandler dbHandler;
    List<Create_Trip> UpComingTripList;
    List<Create_Trip> OngoingTripList;
    List<PushNotification> pushNotificationList;
    Integer push_time_hrs = 2;
    String current_date_after = "", current_date_before = "";
    SharedPreferences preferences;
    GPSTracker gpsTracker;
    boolean isScreenOn;
    PowerManager pm;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    private Handler datehandler;
    private Runnable daterunnable;
    private Handler handler;
    private Runnable runnable;
    private int Notificationsend = 0;
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        //get and send location information

      /*  dbHandler = new DBHandler(context);
        gpsTracker = new GPSTracker(context);
        UpComingTripList = new ArrayList<>();
        UpComingTripList = dbHandler.get_All_TRIP("UpComing");

        OngoingTripList = new ArrayList<>();
        OngoingTripList = dbHandler.get_All_TRIP("OnGoing");
        preferences = context.getSharedPreferences("My_Pref", 0);

        pushNotificationList = dbHandler.get_push_notification_list();
        preferences.edit().putString("servicepop", "").commit();

        Constants.executeLogcat(context);
        this.context=context;

        final SessionManager sessionManager = new SessionManager(context);
        datehandler = new Handler();
        daterunnable = new Runnable() {
            @Override
            public void run() {
                UpComingTripList = new ArrayList<>();
                UpComingTripList = dbHandler.get_All_TRIP("UpComing");

                OngoingTripList = new ArrayList<>();
                OngoingTripList = dbHandler.get_All_TRIP("OnGoing");
                datehandler.postDelayed(daterunnable, 100000);
            }
        };
        datehandler.postDelayed(daterunnable, 100000);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (sessionManager.isLoggedIn())
                    date_popup();
                handler.postDelayed(runnable, 30000);

            }
        };
        handler.postDelayed(runnable, 10000);

        pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        isScreenOn = pm.isScreenOn();

        date_popup();*/

        context.startService(new Intent(context, StartTrip.class));
    }

    public void date_popup() {
        Calendar calendarcurrent = Calendar.getInstance();
        calendarcurrent.set(Calendar.HOUR_OF_DAY, push_time_hrs);

        current_date_before = mFormatter.format(calendarcurrent.getTime());

        Calendar calendarcurrent1 = Calendar.getInstance();
        //  calendarcurrent1.set(Calendar.HOUR_OF_DAY, push_time_hrs*2);

        current_date_after = mFormatter.format(calendarcurrent1.getTime());

        for (int i = 0; i < UpComingTripList.size(); i++) {
            Calendar twohrsbefore = Calendar.getInstance();
            Calendar twohrsafter = Calendar.getInstance();
            Date datebrfore = null;
            try {
                datebrfore = mFormatter.parse(UpComingTripList.get(i).getStartdate() + " " + UpComingTripList.get(i).getStarttime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            twohrsbefore.setTime(datebrfore);
            twohrsafter.setTime(datebrfore);
            twohrsbefore.add(Calendar.HOUR, -push_time_hrs);
            twohrsafter.add(Calendar.HOUR, +push_time_hrs);

            String startdate = mFormatter.format(twohrsbefore.getTime());
            String tripactualDate = mFormatter.format(twohrsafter.getTime());

            Calendar calendar = Calendar.getInstance();
            String currentdate = mFormatter.format(calendar.getTime());
            try {
                Log.i("checkdate", "current date after--" + (mFormatter.parse(currentdate).after(mFormatter.parse(startdate))) + "--current date before--" + (mFormatter.parse(currentdate).before(mFormatter.parse(tripactualDate))));
                if ((mFormatter.parse(currentdate).after(mFormatter.parse(startdate))) && (mFormatter.parse(currentdate).before(mFormatter.parse(tripactualDate))) && Notificationsend == 0) {
                    sendNotification(UpComingTripList.get(i).getTripname() + " trip ready to start", UpComingTripList.get(i).getTripId());
                    Notificationsend = 1;
                    //  preferences.edit().putString("servicepop", "true").commit();
                    if (isScreenOn == false) {
                        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyLock");
                        wl.acquire(10000);
                        PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
                        wl_cpu.acquire(10000);
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.e("screen on.....", "" + isScreenOn);


            if (gpsTracker.isGPSEnabled) {
                try {
                    double distanceofmarkers = distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(), UpComingTripList.get(i).getSource_latitude(), UpComingTripList.get(i).getSource_longitude());
                    if ((distanceofmarkers * 1000) < 1000 && (mFormatter.parse(currentdate).after(mFormatter.parse(startdate)) && mFormatter.parse(currentdate).before(mFormatter.parse(tripactualDate)))) {
                        starttripdialog(UpComingTripList.get(i).getTripId());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
           /* try {

                if ((mFormatter.parse(UpComingTripList.get(i).getStartdate() + " " + UpComingTripList.get(i).getStarttime() + ":01").after(mFormatter.parse(current_date_before)) || (mFormatter.parse(UpComingTripList.get(i).getStartdate() + " " + UpComingTripList.get(i).getStarttime() + ":01").before(mFormatter.parse(current_date_before)))) && preferences.getString("servicepop", "").equalsIgnoreCase("")) {
                    sendNotification(UpComingTripList.get(i).getTripname() + " trip ready to start", i);
                  //  preferences.edit().putString("servicepop", "true").commit();

                    double distanceofmarkers = distance(gpsTracker.getLatitude(), gpsTracker.getLongitude(), UpComingTripList.get(i).getSource_latitude(), UpComingTripList.get(i).getSource_longitude());
                    if ((distanceofmarkers * 1000) < 1000 && preferences.getString("servicepopup", "").equalsIgnoreCase("")) {
                        starttripdialog(UpComingTripList.get(i).getTripId());
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }*/
        }
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.6;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public void starttripdialog(final int tripid) {
        Intent intent = new Intent(context, StartTripActivity.class);
        preferences.edit().putString("servicepopup", "true").commit();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("tripid", tripid);
        Log.i("system", "service tripid --" + tripid);
        context.startActivity(intent);
    }


    private void sendNotification(String messageBody, Integer tripid) {

        Intent intent = new Intent(context, ExploreActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("trip", "true");
        intent.putExtra("tripid", tripid);

        preferences.edit().putString("servicepopup", "").commit();

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.appicon);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(largeIcon)
                .setContentTitle("Happy Roads")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}
