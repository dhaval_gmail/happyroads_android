package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.GPSTracker;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by ADMIN on 9/3/2016.
 */
public class RoadsideAssistance extends Fragment implements OnMapReadyCallback {
    TextView tv_send_ras,tv_vehicle_name,tv_vehicle_year_color;
    LinearLayout ll_vehicle;
    HorizontalScrollView hsv_rsa;
    LinearLayout ll_rsa;
    int position=-1;
    ImageView iv_left_arrow_hsv_rsa,iv_right_arrow_hsv_rsa;
    RelativeLayout error_layout;
    SessionManager sessionManager;
    OTP otp;
    Dialog Vehicledilog;
    String SelectedService="TOWING";
    private GoogleMap mMap;
    static final LatLng myLocation = new LatLng(23.034469, 72.501988);
    SupportMapFragment supportMapFragment;
    GPSTracker gpsTracker;
    Location location;
    TextView tv_longitude,tv_latitude;
    View v;
    Geocoder geocoder;
    List<Address> addresses;



    ArrayList<GetVehicleDetails> getVehicleList=new ArrayList<>();
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(v==null) {
            v = inflater.inflate(R.layout.road_side_assistance, container, false);
        }
        gpsTracker = new GPSTracker(getActivity());

        geocoder = new Geocoder(getActivity(),Locale.getDefault());



        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Road-side Assistance");
       // ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        hsv_rsa=(HorizontalScrollView)v.findViewById(R.id.hsv_rsa);
        ll_rsa=(LinearLayout)v.findViewById(R.id.ll_rsa);
        iv_right_arrow_hsv_rsa=(ImageView)v.findViewById(R.id.iv_right_arrow_hsv_rsa);
        iv_left_arrow_hsv_rsa=(ImageView)v.findViewById(R.id.iv_left_arrow_hsv_rsa);
        error_layout =(RelativeLayout) v.findViewById(R.id.error_layout);
        tv_vehicle_name=(TextView)v.findViewById(R.id.tv_vehicle_name);
        tv_vehicle_year_color=(TextView)v.findViewById(R.id.tv_vehicle_year_color);
        ll_vehicle=(LinearLayout)v.findViewById(R.id.ll_vehicle);
        tv_latitude=(TextView)v.findViewById(R.id.tv_latitude);
        tv_longitude=(TextView)v.findViewById(R.id.tv_longitude);

      /*  FragmentManager fm = getChildFragmentManager();
        ((SupportMapFragment)fm.findFragmentById(R.id.map)).getMapAsync(this);*/
        if (gpsTracker.isGPSEnabled)
        {
            location = gpsTracker.getLocation();
            tv_longitude.setText(""+location.getLongitude());
            tv_latitude.setText(""+location.getLatitude());
            FragmentManager fm = getChildFragmentManager();
            ((SupportMapFragment)fm.findFragmentById(R.id.map)).getMapAsync(this);
        }
            else {
            gpsTracker.showSettingsAlertDialog();
        }







        // ((SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(RoadsideAssistance.this);



        for(int i=0;i<4;i++) {
            RsaServices(i);
        }

        sessionManager = new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();

        tv_send_ras=(TextView)v.findViewById(R.id.tv_send_ras);
        tv_send_ras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tv_vehicle_name.getId() == -1)
                {
                    Constants.show_error_popup(getActivity(),"Select a vehicle.",error_layout);
                }else if(position==-1)
                {
                    Constants.show_error_popup(getActivity(),"Select a service. ",error_layout);
                }
                else {
                    SendRSA();
                }
                //((ExploreActivity)getActivity()).replace_fragmnet(new EmergencyFragment());
            }
        });
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        iv_left_arrow_hsv_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  hsv.getLeft();
                hsv_rsa.scrollTo((int)hsv_rsa.getScrollX() - 60, (int)hsv_rsa.getScrollY());
            }
        });
        iv_right_arrow_hsv_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // hsv.getRight();
                hsv_rsa.scrollTo((int)hsv_rsa.getScrollX() +60, (int)hsv_rsa.getScrollY());

            }
        });

        GetVehicleList();


        ll_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getVehicleList.size()>0)
                {
                    showPopup(getVehicleList);
                }
            }
        });
        return v;
    }


    private void SendRSA() {


        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
             String json = "[{\"IncVehId\": \"" + tv_vehicle_name.getId()+ "\",\"IncUmId\":\"" + otp.getUmId() + "\",   \"umDeviceType\": \"Android\",\"locationLat\": \"" + location.getLatitude() + "\",   \"locationLong\": \"" + location.getLongitude() + "\",\"jobCode\": \""+SelectedService+"\"}]";


            //String json = "[{\"vehicleUmId\":\""+otp.getUmId()+"\"}]";
            Log.d("System out", "In Get Vehicle List  " + json);

            SendRSA(json);

        }
        else
        {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
        }

    }

    private void SendRSA(final String json) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<OTP>> call = apiService.AddNewIncidents(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                  //  getVehicleList.clear();
                   // getVehicleList= response.body();
                    if(response.body().get(0).getResStatus()==true) {
                        Constants.show_error_popup_success(getActivity(), " " + response.body().get(0).getResDescription(), error_layout);
                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(), " " + response.body().get(0).getResDescription(), error_layout);

                    }




                }


            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }
    private void GetVehicleList() {


        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


            String json = "[{\"vehicleUmId\":\""+otp.getUmId()+"\"}]";
            Log.d("System out", "In Get Vehicle List  " + json);

            GetVehicleListAPI(json,error_layout);

        }
        else
        {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
        }

    }
    private void GetVehicleListAPI(final String json,final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<GetVehicleDetails>> call = apiService.GetVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<GetVehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVehicleDetails>> call, Response<ArrayList<GetVehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    getVehicleList.clear();
                    getVehicleList= response.body();
                    if (getVehicleList.get(0).getResStatus() == true)
                    {
                        //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);
                        tv_vehicle_name.setId(getVehicleList.get(0).getVehicleId());
                        tv_vehicle_name.setText(getVehicleList.get(0).getVehicleMake());
                        tv_vehicle_year_color.setText(getVehicleList.get(0).getVehicleYear()+" "+getVehicleList.get(0).getVehicleColor());

                    }
                    else
                    {
                        ll_vehicle.setVisibility(View.GONE);
                        Constants.show_error_popup(getActivity(), "No vehicle found.", error_layout);
                        tv_vehicle_name.setId(-1);
                        tv_vehicle_name.setText("");
                        tv_vehicle_year_color.setText("");
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<GetVehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }
    private void RsaServices(int i) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.facility_content_bottom, null);
        final SimpleDraweeView fc_cat=(SimpleDraweeView) v.findViewById(R.id.fc_cat);
        final TextView tv_facility_name=(TextView) v.findViewById(R.id.tv_facility_name);

        if(i==0)
        {
            fc_cat.setImageResource(R.drawable.ras_icon1_unselected);
            tv_facility_name.setText(" Locked out ");
            if(position==0)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon1_selected);

            }

        }
        else if(i==1)
        {
            fc_cat.setImageResource(R.drawable.ras_icon2_unselected);
            tv_facility_name.setText(" Need a Tow ");
            if(position==1)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon2_selected);


            }
        }
        else if(i==2)
        {
            fc_cat.setImageResource(R.drawable.ras_icon3_unselected);
            tv_facility_name.setText("  Accident  ");
            if(position==2)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon3_selected);
            }

        }else
        {
            fc_cat.setImageResource(R.drawable.ras_icon4_unselected);
            tv_facility_name.setText("Dead Battery");
            if(position==3)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon4_selected);
            }


        }
        v.setId(i);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  if (tv_facility_name.getText().toString().equalsIgnoreCase("Petrol Pump")) {
                position=v.getId();


                //  fl_pop_up_zoo.setVisibility(View.GONE);
                // tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));


                ll_rsa.removeAllViews();
                for(int i=0;i<4;i++) {
                    RsaServices(i);
                    //  }


                }

            }
        });
        ll_rsa.addView(v);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).tv_text_header.setText("Road-side Assistance");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);


    }

    private void showPopup( ArrayList<GetVehicleDetails> getVehicleList) {

        Vehicledilog = new Dialog(getActivity());
        Vehicledilog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Vehicledilog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Vehicledilog.setContentView(R.layout.subscriptiondialog);
        Vehicledilog.setCanceledOnTouchOutside(false);

        // dialog.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);


        final ListView subcriptionlist = (ListView) Vehicledilog.findViewById(R.id.subcriptionlist);
        final ImageView ImgClosepopup = (ImageView) Vehicledilog.findViewById(R.id.ImgClosepopup);
        final TextView tv_popup_header=(TextView)Vehicledilog.findViewById(R.id.tv_popup_header);

        tv_popup_header.setText("Select Vehicle");

        SubscriptionAdapter adapter = new SubscriptionAdapter(getActivity(),getVehicleList);
        subcriptionlist.setAdapter(adapter);




        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vehicledilog.dismiss();
            }
        });








        Vehicledilog.show();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;
        double lat=location.getLatitude();
        double lon=location.getLongitude();
        LatLng map = new LatLng(lat, lon);
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();


        mMap.addMarker(new MarkerOptions().position(map).title(address+" "+city));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(map));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);


        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.getUiSettings().setScrollGesturesEnabled(false);
            }
        });




    }

    public class SubscriptionAdapter extends BaseAdapter
    {
        Activity activity;
        ArrayList<GetVehicleDetails> getVehicleList;
        LayoutInflater inflater;
        public  SubscriptionAdapter(Activity ac,  ArrayList<GetVehicleDetails> getVehicleList1)
        {
            this.getVehicleList=getVehicleList1;
            this.activity=ac;
        }


        @Override
        public int getCount() {
            return getVehicleList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View v;
            inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.subscriptionplan_adapter,null);
            TextView tv_sub_name=(TextView)v.findViewById(R.id.tv_sub_name);
            TextView tv_sub_des=(TextView)v.findViewById(R.id.tv_sub_des);
            TextView buy_plan=(TextView)v.findViewById(R.id.buy_plan);
            buy_plan.setVisibility(View.GONE);
            tv_sub_name.setText(getVehicleList.get(i).getVehicleMake());
            tv_sub_des.setText(getVehicleList.get(i).getVehicleYear()+"  "+getVehicleList.get(i).getVehicleColor());

            v.setId(i);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Vehicledilog.dismiss();
                    tv_vehicle_name.setId(getVehicleList.get(view.getId()).getVehicleId());
                    tv_vehicle_name.setText(getVehicleList.get(view.getId()).getVehicleMake());
                    tv_vehicle_year_color.setText(getVehicleList.get(view.getId()).getVehicleYear()+" "+getVehicleList.get(view.getId()).getVehicleColor());
                }
            });
          /*  v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(otp.getUmRSAId() == null) {
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json = "[{\"umMobile\": \"" + otp.getUmMobile() + "\",\"umFirstName\":\"" + otp.getUmFirstName() + "\",\"umEmailId\": \"" + otp.getUmEmailId() + "\",\"UmId\": \"" + otp.getUmId() + "\",\"umDeviceType\": \"Android\"}]";


                            Log.d("System out", "json for RSA customer Registration " + json);
                            RSARegistration(json,subsciptionPlansArrayList.get(view.getId()).getId(),subsciptionPlansArrayList.get(view.getId()).getCode());

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.no_network, error_layout);
                        }
                    }

                    else if(getVehicleList.get(vehiclepos).getVehicleRSAId() != null)
                    {

                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            // String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";



                            String json=Constants.BASE_URL+"api/RSA_Ver1/PolicyPurchase?id="+subsciptionPlansArrayList.get(view.getId()).getId()+"&code="+subsciptionPlansArrayList.get(view.getId()).getCode();
                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            GetRSAwebUrl(json);

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.no_network, error_layout);
                        }

                        //  showPopupForWebview(subsciptionPlansArrayList.get(view.getId()).getId(),subsciptionPlansArrayList.get(view.getId()).getCode());
                        //VehicleRegister(json,subsciptionPlansArrayList.get(view.getId()).getId(),subsciptionPlansArrayList.get(view.getId()).getCode());
                    }
                    else
                    {

                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";


                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            VehicleRegister(json,subsciptionPlansArrayList.get(view.getId()).getId(),subsciptionPlansArrayList.get(view.getId()).getCode());

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.no_network, error_layout);
                        }
                    }
                }
            });*/

            return v;
        }
    }


}
