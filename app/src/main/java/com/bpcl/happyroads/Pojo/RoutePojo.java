

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

        import java.util.ArrayList;
        import java.util.List;


        public class RoutePojo {

    @SerializedName("RouteId")
    @Expose
    private Integer routeId;
    @SerializedName("RouteNumber")
    @Expose
    private String routeNumber;
    @SerializedName("RouteOrigin")
    @Expose
    private String routeOrigin;
    @SerializedName("RouteOriginLat")
    @Expose
    private String routeOriginLat;
    @SerializedName("RouteOriginLong")
    @Expose
    private String routeOriginLong;
    @SerializedName("RouteDestination")
    @Expose
    private String routeDestination;
    @SerializedName("RouteDestinationLat")
    @Expose
    private String routeDestinationLat;
    @SerializedName("RouteDestinationLong")
    @Expose
    private String routeDestinationLong;
    @SerializedName("RouteTravelVia")
    @Expose
    private String routeTravelVia;
    @SerializedName("RouteTravelViaLat")
    @Expose
    private String routeTravelViaLat;
    @SerializedName("RouteTravelViaLong")
    @Expose
    private String routeTravelViaLong;
    @SerializedName("RouteDistance")
    @Expose
    private String routeDistance;
    @SerializedName("RouteDistanceTime")
    @Expose
    private String routeDistanceTime;
    @SerializedName("RouteEstCost")
    @Expose
    private String routeEstCost;
    @SerializedName("RouteStatus")
    @Expose
    private String routeStatus;
    @SerializedName("RouteIsActive")
    @Expose
    private Boolean routeIsActive;
    @SerializedName("RouteCreatedDate")
    @Expose
    private String routeCreatedDate;
    @SerializedName("RouteUpdateDate")
    @Expose
    private String routeUpdateDate;
    @SerializedName("RouteHostDetails")
    @Expose
    private String routeHostDetails;
    @SerializedName("RouteAdminId")
    @Expose
    private Integer routeAdminId;
    @SerializedName("RouteWayPoints")
    @Expose
    private String routeWayPoints;
    @SerializedName("MainRouteId")
    @Expose
    private Integer mainRouteId;
    @SerializedName("RouteOriginId")
    @Expose
    private Integer routeOriginId;
    @SerializedName("RouteDesId")
    @Expose
    private Integer routeDesId;
    @SerializedName("RouteDistanceValue")
    @Expose
    private String routeDistanceValue;
    @SerializedName("RouteDurationValue")
    @Expose
    private String routeDurationValue;
    @SerializedName("OnAdminRoutePoiLists")
    @Expose
    private List<OnAdminRoutePoiList> onAdminRoutePoiLists = new ArrayList<OnAdminRoutePoiList>();
    @SerializedName("ImageListByDestinationId")
    @Expose
    private List<ImageListByDestinationId> imageListByDestinationId = new ArrayList<ImageListByDestinationId>();
    @SerializedName("ImageListByOriginId")
    @Expose
    private List<ImageListByOriginId> imageListByOriginId = new ArrayList<ImageListByOriginId>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The routeId
     */
    public Integer getRouteId() {
        return routeId;
    }

    /**
     *
     * @param routeId
     * The RouteId
     */
    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    /**
     *
     * @return
     * The routeNumber
     */
    public String getRouteNumber() {
        return routeNumber;
    }

    /**
     *
     * @param routeNumber
     * The RouteNumber
     */
    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    /**
     *
     * @return
     * The routeOrigin
     */
    public String getRouteOrigin() {
        return routeOrigin;
    }

    /**
     *
     * @param routeOrigin
     * The RouteOrigin
     */
    public void setRouteOrigin(String routeOrigin) {
        this.routeOrigin = routeOrigin;
    }

    /**
     *
     * @return
     * The routeOriginLat
     */
    public String getRouteOriginLat() {
        return routeOriginLat;
    }

    /**
     *
     * @param routeOriginLat
     * The RouteOriginLat
     */
    public void setRouteOriginLat(String routeOriginLat) {
        this.routeOriginLat = routeOriginLat;
    }

    /**
     *
     * @return
     * The routeOriginLong
     */
    public String getRouteOriginLong() {
        return routeOriginLong;
    }

    /**
     *
     * @param routeOriginLong
     * The RouteOriginLong
     */
    public void setRouteOriginLong(String routeOriginLong) {
        this.routeOriginLong = routeOriginLong;
    }

    /**
     *
     * @return
     * The routeDestination
     */
    public String getRouteDestination() {
        return routeDestination;
    }

    /**
     *
     * @param routeDestination
     * The RouteDestination
     */
    public void setRouteDestination(String routeDestination) {
        this.routeDestination = routeDestination;
    }

    /**
     *
     * @return
     * The routeDestinationLat
     */
    public String getRouteDestinationLat() {
        return routeDestinationLat;
    }

    /**
     *
     * @param routeDestinationLat
     * The RouteDestinationLat
     */
    public void setRouteDestinationLat(String routeDestinationLat) {
        this.routeDestinationLat = routeDestinationLat;
    }

    /**
     *
     * @return
     * The routeDestinationLong
     */
    public String getRouteDestinationLong() {
        return routeDestinationLong;
    }

    /**
     *
     * @param routeDestinationLong
     * The RouteDestinationLong
     */
    public void setRouteDestinationLong(String routeDestinationLong) {
        this.routeDestinationLong = routeDestinationLong;
    }

    /**
     *
     * @return
     * The routeTravelVia
     */
    public String getRouteTravelVia() {
        return routeTravelVia;
    }

    /**
     *
     * @param routeTravelVia
     * The RouteTravelVia
     */
    public void setRouteTravelVia(String routeTravelVia) {
        this.routeTravelVia = routeTravelVia;
    }

    /**
     *
     * @return
     * The routeTravelViaLat
     */
    public String getRouteTravelViaLat() {
        return routeTravelViaLat;
    }

    /**
     *
     * @param routeTravelViaLat
     * The RouteTravelViaLat
     */
    public void setRouteTravelViaLat(String routeTravelViaLat) {
        this.routeTravelViaLat = routeTravelViaLat;
    }

    /**
     *
     * @return
     * The routeTravelViaLong
     */
    public String getRouteTravelViaLong() {
        return routeTravelViaLong;
    }

    /**
     *
     * @param routeTravelViaLong
     * The RouteTravelViaLong
     */
    public void setRouteTravelViaLong(String routeTravelViaLong) {
        this.routeTravelViaLong = routeTravelViaLong;
    }

    /**
     *
     * @return
     * The routeDistance
     */
    public String getRouteDistance() {
        return routeDistance;
    }

    /**
     *
     * @param routeDistance
     * The RouteDistance
     */
    public void setRouteDistance(String routeDistance) {
        this.routeDistance = routeDistance;
    }

    /**
     *
     * @return
     * The routeDistanceTime
     */
    public String getRouteDistanceTime() {
        return routeDistanceTime;
    }

    /**
     *
     * @param routeDistanceTime
     * The RouteDistanceTime
     */
    public void setRouteDistanceTime(String routeDistanceTime) {
        this.routeDistanceTime = routeDistanceTime;
    }

    /**
     *
     * @return
     * The routeEstCost
     */
    public String getRouteEstCost() {
        return routeEstCost;
    }

    /**
     *
     * @param routeEstCost
     * The RouteEstCost
     */
    public void setRouteEstCost(String routeEstCost) {
        this.routeEstCost = routeEstCost;
    }

    /**
     *
     * @return
     * The routeStatus
     */
    public String getRouteStatus() {
        return routeStatus;
    }

    /**
     *
     * @param routeStatus
     * The RouteStatus
     */
    public void setRouteStatus(String routeStatus) {
        this.routeStatus = routeStatus;
    }

    /**
     *
     * @return
     * The routeIsActive
     */
    public Boolean getRouteIsActive() {
        return routeIsActive;
    }

    /**
     *
     * @param routeIsActive
     * The RouteIsActive
     */
    public void setRouteIsActive(Boolean routeIsActive) {
        this.routeIsActive = routeIsActive;
    }

    /**
     *
     * @return
     * The routeCreatedDate
     */
    public String getRouteCreatedDate() {
        return routeCreatedDate;
    }

    /**
     *
     * @param routeCreatedDate
     * The RouteCreatedDate
     */
    public void setRouteCreatedDate(String routeCreatedDate) {
        this.routeCreatedDate = routeCreatedDate;
    }

    /**
     *
     * @return
     * The routeUpdateDate
     */
    public String getRouteUpdateDate() {
        return routeUpdateDate;
    }

    /**
     *
     * @param routeUpdateDate
     * The RouteUpdateDate
     */
    public void setRouteUpdateDate(String routeUpdateDate) {
        this.routeUpdateDate = routeUpdateDate;
    }

    /**
     *
     * @return
     * The routeHostDetails
     */
    public String getRouteHostDetails() {
        return routeHostDetails;
    }

    /**
     *
     * @param routeHostDetails
     * The RouteHostDetails
     */
    public void setRouteHostDetails(String routeHostDetails) {
        this.routeHostDetails = routeHostDetails;
    }

    /**
     *
     * @return
     * The routeAdminId
     */
    public Integer getRouteAdminId() {
        return routeAdminId;
    }

    /**
     *
     * @param routeAdminId
     * The RouteAdminId
     */
    public void setRouteAdminId(Integer routeAdminId) {
        this.routeAdminId = routeAdminId;
    }

    /**
     *
     * @return
     * The routeWayPoints
     */
    public String getRouteWayPoints() {
        return routeWayPoints;
    }

    /**
     *
     * @param routeWayPoints
     * The RouteWayPoints
     */
    public void setRouteWayPoints(String routeWayPoints) {
        this.routeWayPoints = routeWayPoints;
    }

    /**
     *
     * @return
     * The mainRouteId
     */
    public Integer getMainRouteId() {
        return mainRouteId;
    }

    /**
     *
     * @param mainRouteId
     * The MainRouteId
     */
    public void setMainRouteId(Integer mainRouteId) {
        this.mainRouteId = mainRouteId;
    }

    /**
     *
     * @return
     * The routeOriginId
     */
    public Integer getRouteOriginId() {
        return routeOriginId;
    }

    /**
     *
     * @param routeOriginId
     * The RouteOriginId
     */
    public void setRouteOriginId(Integer routeOriginId) {
        this.routeOriginId = routeOriginId;
    }

    /**
     *
     * @return
     * The routeDesId
     */
    public Integer getRouteDesId() {
        return routeDesId;
    }

    /**
     *
     * @param routeDesId
     * The RouteDesId
     */
    public void setRouteDesId(Integer routeDesId) {
        this.routeDesId = routeDesId;
    }

    /**
     *
     * @return
     * The routeDistanceValue
     */
    public String getRouteDistanceValue() {
        return routeDistanceValue;
    }

    /**
     *
     * @param routeDistanceValue
     * The RouteDistanceValue
     */
    public void setRouteDistanceValue(String routeDistanceValue) {
        this.routeDistanceValue = routeDistanceValue;
    }

    /**
     *
     * @return
     * The routeDurationValue
     */
    public String getRouteDurationValue() {
        return routeDurationValue;
    }

    /**
     *
     * @param routeDurationValue
     * The RouteDurationValue
     */
    public void setRouteDurationValue(String routeDurationValue) {
        this.routeDurationValue = routeDurationValue;
    }

    /**
     *
     * @return
     * The onAdminRoutePoiLists
     */
    public List<OnAdminRoutePoiList> getOnAdminRoutePoiLists() {
        return onAdminRoutePoiLists;
    }

    /**
     *
     * @param onAdminRoutePoiLists
     * The OnAdminRoutePoiLists
     */
    public void setOnAdminRoutePoiLists(List<OnAdminRoutePoiList> onAdminRoutePoiLists) {
        this.onAdminRoutePoiLists = onAdminRoutePoiLists;
    }

    /**
     *
     * @return
     * The imageListByDestinationId
     */
    public List<ImageListByDestinationId> getImageListByDestinationId() {
        return imageListByDestinationId;
    }

    /**
     *
     * @param imageListByDestinationId
     * The ImageListByDestinationId
     */
    public void setImageListByDestinationId(List<ImageListByDestinationId> imageListByDestinationId) {
        this.imageListByDestinationId = imageListByDestinationId;
    }

    /**
     *
     * @return
     * The imageListByOriginId
     */
    public List<ImageListByOriginId> getImageListByOriginId() {
        return imageListByOriginId;
    }

    /**
     *
     * @param imageListByOriginId
     * The ImageListByOriginId
     */
    public void setImageListByOriginId(List<ImageListByOriginId> imageListByOriginId) {
        this.imageListByOriginId = imageListByOriginId;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }
    public class GeneralFields {

        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("longi")
        @Expose
        private String longi;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("Image")
        @Expose
        private String image;
        @SerializedName("ShortDescription")
        @Expose
        private String shortDescription;

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The Name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         * The longi
         */
        public String getLongi() {
            return longi;
        }

        /**
         *
         * @param longi
         * The longi
         */
        public void setLongi(String longi) {
            this.longi = longi;
        }

        /**
         *
         * @return
         * The lat
         */
        public String getLat() {
            return lat;
        }

        /**
         *
         * @param lat
         * The lat
         */
        public void setLat(String lat) {
            this.lat = lat;
        }

        /**
         *
         * @return
         * The address
         */
        public String getAddress() {
            return address;
        }

        /**
         *
         * @param address
         * The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         *
         * @return
         * The image
         */
        public String getImage() {
            return image;
        }

        /**
         *
         * @param image
         * The Image
         */
        public void setImage(String image) {
            this.image = image;
        }

        /**
         *
         * @return
         * The shortDescription
         */
        public String getShortDescription() {
            return shortDescription;
        }

        /**
         *
         * @param shortDescription
         * The ShortDescription
         */
        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

    }


    public class ImageListByDestinationId {

        @SerializedName("TotalImage")
        @Expose
        private Integer totalImage;
        @SerializedName("ImgId")
        @Expose
        private Integer imgId;
        @SerializedName("ImgType")
        @Expose
        private String imgType;
        @SerializedName("ImgName")
        @Expose
        private String imgName;
        @SerializedName("ImgCreatedDate")
        @Expose
        private String imgCreatedDate;
        @SerializedName("ImgRefId")
        @Expose
        private Integer imgRefId;
        @SerializedName("ImgDescription")
        @Expose
        private String imgDescription;
        @SerializedName("ImgIsActive")
        @Expose
        private Boolean imgIsActive;

        /**
         *
         * @return
         * The totalImage
         */
        public Integer getTotalImage() {
            return totalImage;
        }

        /**
         *
         * @param totalImage
         * The TotalImage
         */
        public void setTotalImage(Integer totalImage) {
            this.totalImage = totalImage;
        }

        /**
         *
         * @return
         * The imgId
         */
        public Integer getImgId() {
            return imgId;
        }

        /**
         *
         * @param imgId
         * The ImgId
         */
        public void setImgId(Integer imgId) {
            this.imgId = imgId;
        }

        /**
         *
         * @return
         * The imgType
         */
        public String getImgType() {
            return imgType;
        }

        /**
         *
         * @param imgType
         * The ImgType
         */
        public void setImgType(String imgType) {
            this.imgType = imgType;
        }

        /**
         *
         * @return
         * The imgName
         */
        public String getImgName() {
            return imgName;
        }

        /**
         *
         * @param imgName
         * The ImgName
         */
        public void setImgName(String imgName) {
            this.imgName = imgName;
        }

        /**
         *
         * @return
         * The imgCreatedDate
         */
        public String getImgCreatedDate() {
            return imgCreatedDate;
        }

        /**
         *
         * @param imgCreatedDate
         * The ImgCreatedDate
         */
        public void setImgCreatedDate(String imgCreatedDate) {
            this.imgCreatedDate = imgCreatedDate;
        }

        /**
         *
         * @return
         * The imgRefId
         */
        public Integer getImgRefId() {
            return imgRefId;
        }

        /**
         *
         * @param imgRefId
         * The ImgRefId
         */
        public void setImgRefId(Integer imgRefId) {
            this.imgRefId = imgRefId;
        }

        /**
         *
         * @return
         * The imgDescription
         */
        public String getImgDescription() {
            return imgDescription;
        }

        /**
         *
         * @param imgDescription
         * The ImgDescription
         */
        public void setImgDescription(String imgDescription) {
            this.imgDescription = imgDescription;
        }

        /**
         *
         * @return
         * The imgIsActive
         */
        public Boolean getImgIsActive() {
            return imgIsActive;
        }

        /**
         *
         * @param imgIsActive
         * The ImgIsActive
         */
        public void setImgIsActive(Boolean imgIsActive) {
            this.imgIsActive = imgIsActive;
        }

    }

    public class ImageListByOriginId {

        @SerializedName("TotalImage")
        @Expose
        private Integer totalImage;
        @SerializedName("ImgId")
        @Expose
        private Integer imgId;
        @SerializedName("ImgType")
        @Expose
        private String imgType;
        @SerializedName("ImgName")
        @Expose
        private String imgName;
        @SerializedName("ImgCreatedDate")
        @Expose
        private String imgCreatedDate;
        @SerializedName("ImgRefId")
        @Expose
        private Integer imgRefId;
        @SerializedName("ImgDescription")
        @Expose
        private String imgDescription;
        @SerializedName("ImgIsActive")
        @Expose
        private Boolean imgIsActive;

        /**
         *
         * @return
         * The totalImage
         */
        public Integer getTotalImage() {
            return totalImage;
        }

        /**
         *
         * @param totalImage
         * The TotalImage
         */
        public void setTotalImage(Integer totalImage) {
            this.totalImage = totalImage;
        }

        /**
         *
         * @return
         * The imgId
         */
        public Integer getImgId() {
            return imgId;
        }

        /**
         *
         * @param imgId
         * The ImgId
         */
        public void setImgId(Integer imgId) {
            this.imgId = imgId;
        }

        /**
         *
         * @return
         * The imgType
         */
        public String getImgType() {
            return imgType;
        }

        /**
         *
         * @param imgType
         * The ImgType
         */
        public void setImgType(String imgType) {
            this.imgType = imgType;
        }

        /**
         *
         * @return
         * The imgName
         */
        public String getImgName() {
            return imgName;
        }

        /**
         *
         * @param imgName
         * The ImgName
         */
        public void setImgName(String imgName) {
            this.imgName = imgName;
        }

        /**
         *
         * @return
         * The imgCreatedDate
         */
        public String getImgCreatedDate() {
            return imgCreatedDate;
        }

        /**
         *
         * @param imgCreatedDate
         * The ImgCreatedDate
         */
        public void setImgCreatedDate(String imgCreatedDate) {
            this.imgCreatedDate = imgCreatedDate;
        }

        /**
         *
         * @return
         * The imgRefId
         */
        public Integer getImgRefId() {
            return imgRefId;
        }

        /**
         *
         * @param imgRefId
         * The ImgRefId
         */
        public void setImgRefId(Integer imgRefId) {
            this.imgRefId = imgRefId;
        }

        /**
         *
         * @return
         * The imgDescription
         */
        public String getImgDescription() {
            return imgDescription;
        }

        /**
         *
         * @param imgDescription
         * The ImgDescription
         */
        public void setImgDescription(String imgDescription) {
            this.imgDescription = imgDescription;
        }

        /**
         *
         * @return
         * The imgIsActive
         */
        public Boolean getImgIsActive() {
            return imgIsActive;
        }

        /**
         *
         * @param imgIsActive
         * The ImgIsActive
         */
        public void setImgIsActive(Boolean imgIsActive) {
            this.imgIsActive = imgIsActive;
        }

    }

    public class OnAdminRoutePoiList {

        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("long")
        @Expose
        private String _long;
        @SerializedName("distanceFromRouteOrigin")
        @Expose
        private Double distanceFromRouteOrigin;
        @SerializedName("durationFromRouteOrigin")
        @Expose
        private Double durationFromRouteOrigin;
        @SerializedName("distanceFromRouteDest")
        @Expose
        private Double distanceFromRouteDest;
        @SerializedName("durationFromRouteDest")
        @Expose
        private Double durationFromRouteDest;
        @SerializedName("distanceFromUser")
        @Expose
        private Double distanceFromUser;
        @SerializedName("durationFromUser")
        @Expose
        private Double durationFromUser;
        @SerializedName("RPId")
        @Expose
        private Integer rPId;
        @SerializedName("RouteId")
        @Expose
        private Integer routeId;
        @SerializedName("RouteRefId")
        @Expose
        private Integer routeRefId;
        @SerializedName("RPIsActive")
        @Expose
        private Boolean rPIsActive;
        @SerializedName("RPType")
        @Expose
        private String rPType;
        @SerializedName("ROMasterId")
        @Expose
        private Integer rOMasterId;
        @SerializedName("ROName")
        @Expose
        private String rOName;
        @SerializedName("ROCCNo")
        @Expose
        private String rOCCNo;
        @SerializedName("ROManagerContactNo")
        @Expose
        private String rOManagerContactNo;
        @SerializedName("ROLocationName")
        @Expose
        private String rOLocationName;
        @SerializedName("ROLatitude")
        @Expose
        private String rOLatitude;
        @SerializedName("ROLongitude")
        @Expose
        private String rOLongitude;
        @SerializedName("ROStateID")
        @Expose
        private Integer rOStateID;
        @SerializedName("ROCityID")
        @Expose
        private Integer rOCityID;
        @SerializedName("ROBusHours")
        @Expose
        private Boolean rOBusHours;
        @SerializedName("ROFrom")
        @Expose
        private String rOFrom;
        @SerializedName("ROTo")
        @Expose
        private String rOTo;
        @SerializedName("ROCatId")
        @Expose
        private Integer rOCatId;
        @SerializedName("ROType")
        @Expose
        private Integer rOType;
        @SerializedName("ROServiceOffered")
        @Expose
        private String rOServiceOffered;
        @SerializedName("ROToilet")
        @Expose
        private String rOToilet;
        @SerializedName("ROFuels")
        @Expose
        private String rOFuels;
        @SerializedName("ROFuelsType")
        @Expose
        private String rOFuelsType;
        @SerializedName("ROHostDetails")
        @Expose
        private String rOHostDetails;
        @SerializedName("ROAdminId")
        @Expose
        private Integer rOAdminId;
        @SerializedName("ROIsActive")
        @Expose
        private Boolean rOIsActive;
        @SerializedName("ROCreatedDate")
        @Expose
        private String rOCreatedDate;
        @SerializedName("ROHighwayType")
        @Expose
        private Integer rOHighwayType;
        @SerializedName("ROSpendTime")
        @Expose
        private String rOSpendTime;
        @SerializedName("ROSide")
        @Expose
        private String rOSide;
        @SerializedName("GeneralFields")
        @Expose
        private GeneralFields generalFields;
        @SerializedName("resStatus")
        @Expose
        private Boolean resStatus;
        @SerializedName("resCallerDetails")
        @Expose
        private String resCallerDetails;
        @SerializedName("PageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("PageSize")
        @Expose
        private Integer pageSize;
        @SerializedName("radius")
        @Expose
        private Integer radius;
        @SerializedName("ROManagerName")
        @Expose
        private String rOManagerName;
        @SerializedName("ROManagerEmailId")
        @Expose
        private String rOManagerEmailId;
        @SerializedName("RPCreateDate")
        @Expose
        private String rPCreateDate;
        @SerializedName("RPUpdateDate")
        @Expose
        private String rPUpdateDate;
        @SerializedName("RPAdminId")
        @Expose
        private Integer rPAdminId;
        @SerializedName("RPHostDetail")
        @Expose
        private String rPHostDetail;
        @SerializedName("POIID")
        @Expose
        private Integer pOIID;
        @SerializedName("POICatId")
        @Expose
        private Integer pOICatId;
        @SerializedName("POITypeId")
        @Expose
        private String pOITypeId;
        @SerializedName("POIImage")
        @Expose
        private String pOIImage;
        @SerializedName("POINearbyDestination")
        @Expose
        private Integer pOINearbyDestination;
        @SerializedName("POINoOfNearByDestination")
        @Expose
        private Integer pOINoOfNearByDestination;
        @SerializedName("POIName")
        @Expose
        private String pOIName;
        @SerializedName("POIContactPerson")
        @Expose
        private String pOIContactPerson;
        @SerializedName("POICustCareNo")
        @Expose
        private String pOICustCareNo;
        @SerializedName("POIEmailId")
        @Expose
        private String pOIEmailId;
        @SerializedName("POIAddress1")
        @Expose
        private String pOIAddress1;
        @SerializedName("POILatitude")
        @Expose
        private String pOILatitude;
        @SerializedName("POILongitude")
        @Expose
        private String pOILongitude;
        @SerializedName("POIState")
        @Expose
        private Integer pOIState;
        @SerializedName("POICity")
        @Expose
        private Integer pOICity;
        @SerializedName("POIZipCode")
        @Expose
        private String pOIZipCode;
        @SerializedName("POIIsActive")
        @Expose
        private Boolean pOIIsActive;
        @SerializedName("POICreateDate")
        @Expose
        private String pOICreateDate;
        @SerializedName("POIUpdateDate")
        @Expose
        private String pOIUpdateDate;
        @SerializedName("POIHostDetails")
        @Expose
        private String pOIHostDetails;
        @SerializedName("POIAdminId")
        @Expose
        private Integer pOIAdminId;
        @SerializedName("POIIsFullTime")
        @Expose
        private Boolean pOIIsFullTime;
        @SerializedName("POIStartTime")
        @Expose
        private String pOIStartTime;
        @SerializedName("POIEndTime")
        @Expose
        private String pOIEndTime;
        @SerializedName("POIBestTimeFrom")
        @Expose
        private String pOIBestTimeFrom;
        @SerializedName("POIBestTimeTo")
        @Expose
        private String pOIBestTimeTo;
        @SerializedName("POIShortDescription")
        @Expose
        private String pOIShortDescription;
        @SerializedName("POILongDescription")
        @Expose
        private String pOILongDescription;
        @SerializedName("POINoofVisited")
        @Expose
        private Integer pOINoofVisited;
        @SerializedName("POIRating")
        @Expose
        private String pOIRating;
        @SerializedName("POISpendTime")
        @Expose
        private String pOISpendTime;
        @SerializedName("POINoOfNearByAttraction")
        @Expose
        private Integer pOINoOfNearByAttraction;
        @SerializedName("desID")
        @Expose
        private Integer desID;
        @SerializedName("desCountry")
        @Expose
        private Integer desCountry;
        @SerializedName("desState")
        @Expose
        private Integer desState;
        @SerializedName("desDistrict")
        @Expose
        private Integer desDistrict;
        @SerializedName("desCity")
        @Expose
        private Integer desCity;
        @SerializedName("desName")
        @Expose
        private String desName;
        @SerializedName("desLatitude")
        @Expose
        private String desLatitude;
        @SerializedName("desLongitude")
        @Expose
        private String desLongitude;
        @SerializedName("desImage")
        @Expose
        private String desImage;
        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;
        @SerializedName("desCreateDate")
        @Expose
        private String desCreateDate;
        @SerializedName("desRating")
        @Expose
        private String desRating;
        @SerializedName("desAddress1")
        @Expose
        private String desAddress1;
        @SerializedName("destBestTimeFrom")
        @Expose
        private String destBestTimeFrom;
        @SerializedName("desBestTimeTo")
        @Expose
        private String desBestTimeTo;
        @SerializedName("desShortDescription")
        @Expose
        private String desShortDescription;
        @SerializedName("desLongDescription")
        @Expose
        private String desLongDescription;
        @SerializedName("desFestival")
        @Expose
        private String desFestival;
        @SerializedName("desNoOfPOI")
        @Expose
        private Integer desNoOfPOI;
        @SerializedName("desNoOfVisited")
        @Expose
        private Integer desNoOfVisited;
        @SerializedName("desHostDetails")
        @Expose
        private String desHostDetails;
        @SerializedName("desNearByDestination")
        @Expose
        private String desNearByDestination;
        @SerializedName("desNoOfNearByDestination")
        @Expose
        private Integer desNoOfNearByDestination;
        @SerializedName("desDistance")
        @Expose
        private String desDistance;
        @SerializedName("desNoOfNearByAttraction")
        @Expose
        private Integer desNoOfNearByAttraction;

        public boolean click_status;
        public boolean isClick_status() {
            return click_status;
        }

        public void setClick_status(boolean click_status) {
            this.click_status = click_status;
        }

        /**
         *
         * @return
         * The lat
         */
        public String getLat() {
            return lat;
        }

        /**
         *
         * @param lat
         * The lat
         */
        public void setLat(String lat) {
            this.lat = lat;
        }

        /**
         *
         * @return
         * The _long
         */
        public String getLong() {
            return _long;
        }

        /**
         *
         * @param _long
         * The long
         */
        public void setLong(String _long) {
            this._long = _long;
        }

        /**
         *
         * @return
         * The distanceFromRouteOrigin
         */
        public Double getDistanceFromRouteOrigin() {
            return distanceFromRouteOrigin;
        }

        /**
         *
         * @param distanceFromRouteOrigin
         * The distanceFromRouteOrigin
         */
        public void setDistanceFromRouteOrigin(Double distanceFromRouteOrigin) {
            this.distanceFromRouteOrigin = distanceFromRouteOrigin;
        }

        /**
         *
         * @return
         * The durationFromRouteOrigin
         */
        public Double getDurationFromRouteOrigin() {
            return durationFromRouteOrigin;
        }

        /**
         *
         * @param durationFromRouteOrigin
         * The durationFromRouteOrigin
         */
        public void setDurationFromRouteOrigin(Double durationFromRouteOrigin) {
            this.durationFromRouteOrigin = durationFromRouteOrigin;
        }

        /**
         *
         * @return
         * The distanceFromRouteDest
         */
        public Double getDistanceFromRouteDest() {
            return distanceFromRouteDest;
        }

        /**
         *
         * @param distanceFromRouteDest
         * The distanceFromRouteDest
         */
        public void setDistanceFromRouteDest(Double distanceFromRouteDest) {
            this.distanceFromRouteDest = distanceFromRouteDest;
        }

        /**
         *
         * @return
         * The durationFromRouteDest
         */
        public Double getDurationFromRouteDest() {
            return durationFromRouteDest;
        }

        /**
         *
         * @param durationFromRouteDest
         * The durationFromRouteDest
         */
        public void setDurationFromRouteDest(Double durationFromRouteDest) {
            this.durationFromRouteDest = durationFromRouteDest;
        }

        /**
         *
         * @return
         * The distanceFromUser
         */
        public Double getDistanceFromUser() {
            return distanceFromUser;
        }

        /**
         *
         * @param distanceFromUser
         * The distanceFromUser
         */
        public void setDistanceFromUser(Double distanceFromUser) {
            this.distanceFromUser = distanceFromUser;
        }

        /**
         *
         * @return
         * The durationFromUser
         */
        public Double getDurationFromUser() {
            return durationFromUser;
        }

        /**
         *
         * @param durationFromUser
         * The durationFromUser
         */
        public void setDurationFromUser(Double durationFromUser) {
            this.durationFromUser = durationFromUser;
        }

        /**
         *
         * @return
         * The rPId
         */
        public Integer getRPId() {
            return rPId;
        }

        /**
         *
         * @param rPId
         * The RPId
         */
        public void setRPId(Integer rPId) {
            this.rPId = rPId;
        }

        /**
         *
         * @return
         * The routeId
         */
        public Integer getRouteId() {
            return routeId;
        }

        /**
         *
         * @param routeId
         * The RouteId
         */
        public void setRouteId(Integer routeId) {
            this.routeId = routeId;
        }

        /**
         *
         * @return
         * The routeRefId
         */
        public Integer getRouteRefId() {
            return routeRefId;
        }

        /**
         *
         * @param routeRefId
         * The RouteRefId
         */
        public void setRouteRefId(Integer routeRefId) {
            this.routeRefId = routeRefId;
        }

        /**
         *
         * @return
         * The rPIsActive
         */
        public Boolean getRPIsActive() {
            return rPIsActive;
        }

        /**
         *
         * @param rPIsActive
         * The RPIsActive
         */
        public void setRPIsActive(Boolean rPIsActive) {
            this.rPIsActive = rPIsActive;
        }

        /**
         *
         * @return
         * The rPType
         */
        public String getRPType() {
            return rPType;
        }

        /**
         *
         * @param rPType
         * The RPType
         */
        public void setRPType(String rPType) {
            this.rPType = rPType;
        }

        /**
         *
         * @return
         * The rOMasterId
         */
        public Integer getROMasterId() {
            return rOMasterId;
        }

        /**
         *
         * @param rOMasterId
         * The ROMasterId
         */
        public void setROMasterId(Integer rOMasterId) {
            this.rOMasterId = rOMasterId;
        }

        /**
         *
         * @return
         * The rOName
         */
        public String getROName() {
            return rOName;
        }

        /**
         *
         * @param rOName
         * The ROName
         */
        public void setROName(String rOName) {
            this.rOName = rOName;
        }

        /**
         *
         * @return
         * The rOCCNo
         */
        public String getROCCNo() {
            return rOCCNo;
        }

        /**
         *
         * @param rOCCNo
         * The ROCCNo
         */
        public void setROCCNo(String rOCCNo) {
            this.rOCCNo = rOCCNo;
        }

        /**
         *
         * @return
         * The rOManagerContactNo
         */
        public String getROManagerContactNo() {
            return rOManagerContactNo;
        }

        /**
         *
         * @param rOManagerContactNo
         * The ROManagerContactNo
         */
        public void setROManagerContactNo(String rOManagerContactNo) {
            this.rOManagerContactNo = rOManagerContactNo;
        }

        /**
         *
         * @return
         * The rOLocationName
         */
        public String getROLocationName() {
            return rOLocationName;
        }

        /**
         *
         * @param rOLocationName
         * The ROLocationName
         */
        public void setROLocationName(String rOLocationName) {
            this.rOLocationName = rOLocationName;
        }

        /**
         *
         * @return
         * The rOLatitude
         */
        public String getROLatitude() {
            return rOLatitude;
        }

        /**
         *
         * @param rOLatitude
         * The ROLatitude
         */
        public void setROLatitude(String rOLatitude) {
            this.rOLatitude = rOLatitude;
        }

        /**
         *
         * @return
         * The rOLongitude
         */
        public String getROLongitude() {
            return rOLongitude;
        }

        /**
         *
         * @param rOLongitude
         * The ROLongitude
         */
        public void setROLongitude(String rOLongitude) {
            this.rOLongitude = rOLongitude;
        }

        /**
         *
         * @return
         * The rOStateID
         */
        public Integer getROStateID() {
            return rOStateID;
        }

        /**
         *
         * @param rOStateID
         * The ROStateID
         */
        public void setROStateID(Integer rOStateID) {
            this.rOStateID = rOStateID;
        }

        /**
         *
         * @return
         * The rOCityID
         */
        public Integer getROCityID() {
            return rOCityID;
        }

        /**
         *
         * @param rOCityID
         * The ROCityID
         */
        public void setROCityID(Integer rOCityID) {
            this.rOCityID = rOCityID;
        }

        /**
         *
         * @return
         * The rOBusHours
         */
        public Boolean getROBusHours() {
            return rOBusHours;
        }

        /**
         *
         * @param rOBusHours
         * The ROBusHours
         */
        public void setROBusHours(Boolean rOBusHours) {
            this.rOBusHours = rOBusHours;
        }

        /**
         *
         * @return
         * The rOFrom
         */
        public String getROFrom() {
            return rOFrom;
        }

        /**
         *
         * @param rOFrom
         * The ROFrom
         */
        public void setROFrom(String rOFrom) {
            this.rOFrom = rOFrom;
        }

        /**
         *
         * @return
         * The rOTo
         */
        public String getROTo() {
            return rOTo;
        }

        /**
         *
         * @param rOTo
         * The ROTo
         */
        public void setROTo(String rOTo) {
            this.rOTo = rOTo;
        }

        /**
         *
         * @return
         * The rOCatId
         */
        public Integer getROCatId() {
            return rOCatId;
        }

        /**
         *
         * @param rOCatId
         * The ROCatId
         */
        public void setROCatId(Integer rOCatId) {
            this.rOCatId = rOCatId;
        }

        /**
         *
         * @return
         * The rOType
         */
        public Integer getROType() {
            return rOType;
        }

        /**
         *
         * @param rOType
         * The ROType
         */
        public void setROType(Integer rOType) {
            this.rOType = rOType;
        }

        /**
         *
         * @return
         * The rOServiceOffered
         */
        public String getROServiceOffered() {
            return rOServiceOffered;
        }

        /**
         *
         * @param rOServiceOffered
         * The ROServiceOffered
         */
        public void setROServiceOffered(String rOServiceOffered) {
            this.rOServiceOffered = rOServiceOffered;
        }

        /**
         *
         * @return
         * The rOToilet
         */
        public String getROToilet() {
            return rOToilet;
        }

        /**
         *
         * @param rOToilet
         * The ROToilet
         */
        public void setROToilet(String rOToilet) {
            this.rOToilet = rOToilet;
        }

        /**
         *
         * @return
         * The rOFuels
         */
        public String getROFuels() {
            return rOFuels;
        }

        /**
         *
         * @param rOFuels
         * The ROFuels
         */
        public void setROFuels(String rOFuels) {
            this.rOFuels = rOFuels;
        }

        /**
         *
         * @return
         * The rOFuelsType
         */
        public String getROFuelsType() {
            return rOFuelsType;
        }

        /**
         *
         * @param rOFuelsType
         * The ROFuelsType
         */
        public void setROFuelsType(String rOFuelsType) {
            this.rOFuelsType = rOFuelsType;
        }

        /**
         *
         * @return
         * The rOHostDetails
         */
        public String getROHostDetails() {
            return rOHostDetails;
        }

        /**
         *
         * @param rOHostDetails
         * The ROHostDetails
         */
        public void setROHostDetails(String rOHostDetails) {
            this.rOHostDetails = rOHostDetails;
        }

        /**
         *
         * @return
         * The rOAdminId
         */
        public Integer getROAdminId() {
            return rOAdminId;
        }

        /**
         *
         * @param rOAdminId
         * The ROAdminId
         */
        public void setROAdminId(Integer rOAdminId) {
            this.rOAdminId = rOAdminId;
        }

        /**
         *
         * @return
         * The rOIsActive
         */
        public Boolean getROIsActive() {
            return rOIsActive;
        }

        /**
         *
         * @param rOIsActive
         * The ROIsActive
         */
        public void setROIsActive(Boolean rOIsActive) {
            this.rOIsActive = rOIsActive;
        }

        /**
         *
         * @return
         * The rOCreatedDate
         */
        public String getROCreatedDate() {
            return rOCreatedDate;
        }

        /**
         *
         * @param rOCreatedDate
         * The ROCreatedDate
         */
        public void setROCreatedDate(String rOCreatedDate) {
            this.rOCreatedDate = rOCreatedDate;
        }

        /**
         *
         * @return
         * The rOHighwayType
         */
        public Integer getROHighwayType() {
            return rOHighwayType;
        }

        /**
         *
         * @param rOHighwayType
         * The ROHighwayType
         */
        public void setROHighwayType(Integer rOHighwayType) {
            this.rOHighwayType = rOHighwayType;
        }

        /**
         *
         * @return
         * The rOSpendTime
         */
        public String getROSpendTime() {
            return rOSpendTime;
        }

        /**
         *
         * @param rOSpendTime
         * The ROSpendTime
         */
        public void setROSpendTime(String rOSpendTime) {
            this.rOSpendTime = rOSpendTime;
        }

        /**
         *
         * @return
         * The rOSide
         */
        public String getROSide() {
            return rOSide;
        }

        /**
         *
         * @param rOSide
         * The ROSide
         */
        public void setROSide(String rOSide) {
            this.rOSide = rOSide;
        }

        /**
         *
         * @return
         * The generalFields
         */
        public GeneralFields getGeneralFields() {
            return generalFields;
        }

        /**
         *
         * @param generalFields
         * The GeneralFields
         */
        public void setGeneralFields(GeneralFields generalFields) {
            this.generalFields = generalFields;
        }

        /**
         *
         * @return
         * The resStatus
         */
        public Boolean getResStatus() {
            return resStatus;
        }

        /**
         *
         * @param resStatus
         * The resStatus
         */
        public void setResStatus(Boolean resStatus) {
            this.resStatus = resStatus;
        }

        /**
         *
         * @return
         * The resCallerDetails
         */
        public String getResCallerDetails() {
            return resCallerDetails;
        }

        /**
         *
         * @param resCallerDetails
         * The resCallerDetails
         */
        public void setResCallerDetails(String resCallerDetails) {
            this.resCallerDetails = resCallerDetails;
        }

        /**
         *
         * @return
         * The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         *
         * @param pageNo
         * The PageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         *
         * @return
         * The pageSize
         */
        public Integer getPageSize() {
            return pageSize;
        }

        /**
         *
         * @param pageSize
         * The PageSize
         */
        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         *
         * @return
         * The radius
         */
        public Integer getRadius() {
            return radius;
        }

        /**
         *
         * @param radius
         * The radius
         */
        public void setRadius(Integer radius) {
            this.radius = radius;
        }

        /**
         *
         * @return
         * The rOManagerName
         */
        public String getROManagerName() {
            return rOManagerName;
        }

        /**
         *
         * @param rOManagerName
         * The ROManagerName
         */
        public void setROManagerName(String rOManagerName) {
            this.rOManagerName = rOManagerName;
        }

        /**
         *
         * @return
         * The rOManagerEmailId
         */
        public String getROManagerEmailId() {
            return rOManagerEmailId;
        }

        /**
         *
         * @param rOManagerEmailId
         * The ROManagerEmailId
         */
        public void setROManagerEmailId(String rOManagerEmailId) {
            this.rOManagerEmailId = rOManagerEmailId;
        }

        /**
         *
         * @return
         * The rPCreateDate
         */
        public String getRPCreateDate() {
            return rPCreateDate;
        }

        /**
         *
         * @param rPCreateDate
         * The RPCreateDate
         */
        public void setRPCreateDate(String rPCreateDate) {
            this.rPCreateDate = rPCreateDate;
        }

        /**
         *
         * @return
         * The rPUpdateDate
         */
        public String getRPUpdateDate() {
            return rPUpdateDate;
        }

        /**
         *
         * @param rPUpdateDate
         * The RPUpdateDate
         */
        public void setRPUpdateDate(String rPUpdateDate) {
            this.rPUpdateDate = rPUpdateDate;
        }

        /**
         *
         * @return
         * The rPAdminId
         */
        public Integer getRPAdminId() {
            return rPAdminId;
        }

        /**
         *
         * @param rPAdminId
         * The RPAdminId
         */
        public void setRPAdminId(Integer rPAdminId) {
            this.rPAdminId = rPAdminId;
        }

        /**
         *
         * @return
         * The rPHostDetail
         */
        public String getRPHostDetail() {
            return rPHostDetail;
        }

        /**
         *
         * @param rPHostDetail
         * The RPHostDetail
         */
        public void setRPHostDetail(String rPHostDetail) {
            this.rPHostDetail = rPHostDetail;
        }

        /**
         *
         * @return
         * The pOIID
         */
        public Integer getPOIID() {
            return pOIID;
        }

        /**
         *
         * @param pOIID
         * The POIID
         */
        public void setPOIID(Integer pOIID) {
            this.pOIID = pOIID;
        }

        /**
         *
         * @return
         * The pOICatId
         */
        public Integer getPOICatId() {
            return pOICatId;
        }

        /**
         *
         * @param pOICatId
         * The POICatId
         */
        public void setPOICatId(Integer pOICatId) {
            this.pOICatId = pOICatId;
        }

        /**
         *
         * @return
         * The pOITypeId
         */
        public String getPOITypeId() {
            return pOITypeId;
        }

        /**
         *
         * @param pOITypeId
         * The POITypeId
         */
        public void setPOITypeId(String pOITypeId) {
            this.pOITypeId = pOITypeId;
        }

        /**
         *
         * @return
         * The pOIImage
         */
        public String getPOIImage() {
            return pOIImage;
        }

        /**
         *
         * @param pOIImage
         * The POIImage
         */
        public void setPOIImage(String pOIImage) {
            this.pOIImage = pOIImage;
        }

        /**
         *
         * @return
         * The pOINearbyDestination
         */
        public Integer getPOINearbyDestination() {
            return pOINearbyDestination;
        }

        /**
         *
         * @param pOINearbyDestination
         * The POINearbyDestination
         */
        public void setPOINearbyDestination(Integer pOINearbyDestination) {
            this.pOINearbyDestination = pOINearbyDestination;
        }

        /**
         *
         * @return
         * The pOINoOfNearByDestination
         */
        public Integer getPOINoOfNearByDestination() {
            return pOINoOfNearByDestination;
        }

        /**
         *
         * @param pOINoOfNearByDestination
         * The POINoOfNearByDestination
         */
        public void setPOINoOfNearByDestination(Integer pOINoOfNearByDestination) {
            this.pOINoOfNearByDestination = pOINoOfNearByDestination;
        }

        /**
         *
         * @return
         * The pOIName
         */
        public String getPOIName() {
            if(pOIName == null)
            {
                return "";
            }
            else
            {
                return pOIName;
            }

        }

        /**
         *
         * @param pOIName
         * The POIName
         */
        public void setPOIName(String pOIName) {
            this.pOIName = pOIName;
        }

        /**
         *
         * @return
         * The pOIContactPerson
         */
        public String getPOIContactPerson() {
            return pOIContactPerson;
        }

        /**
         *
         * @param pOIContactPerson
         * The POIContactPerson
         */
        public void setPOIContactPerson(String pOIContactPerson) {
            this.pOIContactPerson = pOIContactPerson;
        }

        /**
         *
         * @return
         * The pOICustCareNo
         */
        public String getPOICustCareNo() {
            return pOICustCareNo;
        }

        /**
         *
         * @param pOICustCareNo
         * The POICustCareNo
         */
        public void setPOICustCareNo(String pOICustCareNo) {
            this.pOICustCareNo = pOICustCareNo;
        }

        /**
         *
         * @return
         * The pOIEmailId
         */
        public String getPOIEmailId() {
            return pOIEmailId;
        }

        /**
         *
         * @param pOIEmailId
         * The POIEmailId
         */
        public void setPOIEmailId(String pOIEmailId) {
            this.pOIEmailId = pOIEmailId;
        }

        /**
         *
         * @return
         * The pOIAddress1
         */
        public String getPOIAddress1() {
            return pOIAddress1;
        }

        /**
         *
         * @param pOIAddress1
         * The POIAddress1
         */
        public void setPOIAddress1(String pOIAddress1) {
            this.pOIAddress1 = pOIAddress1;
        }

        /**
         *
         * @return
         * The pOILatitude
         */
        public String getPOILatitude() {
            return pOILatitude;
        }

        /**
         *
         * @param pOILatitude
         * The POILatitude
         */
        public void setPOILatitude(String pOILatitude) {
            this.pOILatitude = pOILatitude;
        }

        /**
         *
         * @return
         * The pOILongitude
         */
        public String getPOILongitude() {
            return pOILongitude;
        }

        /**
         *
         * @param pOILongitude
         * The POILongitude
         */
        public void setPOILongitude(String pOILongitude) {
            this.pOILongitude = pOILongitude;
        }

        /**
         *
         * @return
         * The pOIState
         */
        public Integer getPOIState() {
            return pOIState;
        }

        /**
         *
         * @param pOIState
         * The POIState
         */
        public void setPOIState(Integer pOIState) {
            this.pOIState = pOIState;
        }

        /**
         *
         * @return
         * The pOICity
         */
        public Integer getPOICity() {
            return pOICity;
        }

        /**
         *
         * @param pOICity
         * The POICity
         */
        public void setPOICity(Integer pOICity) {
            this.pOICity = pOICity;
        }

        /**
         *
         * @return
         * The pOIZipCode
         */
        public String getPOIZipCode() {
            return pOIZipCode;
        }

        /**
         *
         * @param pOIZipCode
         * The POIZipCode
         */
        public void setPOIZipCode(String pOIZipCode) {
            this.pOIZipCode = pOIZipCode;
        }

        /**
         *
         * @return
         * The pOIIsActive
         */
        public Boolean getPOIIsActive() {
            return pOIIsActive;
        }

        /**
         *
         * @param pOIIsActive
         * The POIIsActive
         */
        public void setPOIIsActive(Boolean pOIIsActive) {
            this.pOIIsActive = pOIIsActive;
        }

        /**
         *
         * @return
         * The pOICreateDate
         */
        public String getPOICreateDate() {
            return pOICreateDate;
        }

        /**
         *
         * @param pOICreateDate
         * The POICreateDate
         */
        public void setPOICreateDate(String pOICreateDate) {
            this.pOICreateDate = pOICreateDate;
        }

        /**
         *
         * @return
         * The pOIUpdateDate
         */
        public String getPOIUpdateDate() {
            return pOIUpdateDate;
        }

        /**
         *
         * @param pOIUpdateDate
         * The POIUpdateDate
         */
        public void setPOIUpdateDate(String pOIUpdateDate) {
            this.pOIUpdateDate = pOIUpdateDate;
        }

        /**
         *
         * @return
         * The pOIHostDetails
         */
        public String getPOIHostDetails() {
            return pOIHostDetails;
        }

        /**
         *
         * @param pOIHostDetails
         * The POIHostDetails
         */
        public void setPOIHostDetails(String pOIHostDetails) {
            this.pOIHostDetails = pOIHostDetails;
        }

        /**
         *
         * @return
         * The pOIAdminId
         */
        public Integer getPOIAdminId() {
            return pOIAdminId;
        }

        /**
         *
         * @param pOIAdminId
         * The POIAdminId
         */
        public void setPOIAdminId(Integer pOIAdminId) {
            this.pOIAdminId = pOIAdminId;
        }

        /**
         *
         * @return
         * The pOIIsFullTime
         */
        public Boolean getPOIIsFullTime() {
            return pOIIsFullTime;
        }

        /**
         *
         * @param pOIIsFullTime
         * The POIIsFullTime
         */
        public void setPOIIsFullTime(Boolean pOIIsFullTime) {
            this.pOIIsFullTime = pOIIsFullTime;
        }

        /**
         *
         * @return
         * The pOIStartTime
         */
        public String getPOIStartTime() {
            return pOIStartTime;
        }

        /**
         *
         * @param pOIStartTime
         * The POIStartTime
         */
        public void setPOIStartTime(String pOIStartTime) {
            this.pOIStartTime = pOIStartTime;
        }

        /**
         *
         * @return
         * The pOIEndTime
         */
        public String getPOIEndTime() {
            return pOIEndTime;
        }

        /**
         *
         * @param pOIEndTime
         * The POIEndTime
         */
        public void setPOIEndTime(String pOIEndTime) {
            this.pOIEndTime = pOIEndTime;
        }

        /**
         *
         * @return
         * The pOIBestTimeFrom
         */
        public String getPOIBestTimeFrom() {
            return pOIBestTimeFrom;
        }

        /**
         *
         * @param pOIBestTimeFrom
         * The POIBestTimeFrom
         */
        public void setPOIBestTimeFrom(String pOIBestTimeFrom) {
            this.pOIBestTimeFrom = pOIBestTimeFrom;
        }

        /**
         *
         * @return
         * The pOIBestTimeTo
         */
        public String getPOIBestTimeTo() {
            return pOIBestTimeTo;
        }

        /**
         *
         * @param pOIBestTimeTo
         * The POIBestTimeTo
         */
        public void setPOIBestTimeTo(String pOIBestTimeTo) {
            this.pOIBestTimeTo = pOIBestTimeTo;
        }

        /**
         *
         * @return
         * The pOIShortDescription
         */
        public String getPOIShortDescription() {
            return pOIShortDescription;
        }

        /**
         *
         * @param pOIShortDescription
         * The POIShortDescription
         */
        public void setPOIShortDescription(String pOIShortDescription) {
            this.pOIShortDescription = pOIShortDescription;
        }

        /**
         *
         * @return
         * The pOILongDescription
         */
        public String getPOILongDescription() {
            return pOILongDescription;
        }

        /**
         *
         * @param pOILongDescription
         * The POILongDescription
         */
        public void setPOILongDescription(String pOILongDescription) {
            this.pOILongDescription = pOILongDescription;
        }

        /**
         *
         * @return
         * The pOINoofVisited
         */
        public Integer getPOINoofVisited() {
            return pOINoofVisited;
        }

        /**
         *
         * @param pOINoofVisited
         * The POINoofVisited
         */
        public void setPOINoofVisited(Integer pOINoofVisited) {
            this.pOINoofVisited = pOINoofVisited;
        }

        /**
         *
         * @return
         * The pOIRating
         */
        public String getPOIRating() {
            return pOIRating;
        }

        /**
         *
         * @param pOIRating
         * The POIRating
         */
        public void setPOIRating(String pOIRating) {
            this.pOIRating = pOIRating;
        }

        /**
         *
         * @return
         * The pOISpendTime
         */
        public String getPOISpendTime() {
            return pOISpendTime;
        }

        /**
         *
         * @param pOISpendTime
         * The POISpendTime
         */
        public void setPOISpendTime(String pOISpendTime) {
            this.pOISpendTime = pOISpendTime;
        }

        /**
         *
         * @return
         * The pOINoOfNearByAttraction
         */
        public Integer getPOINoOfNearByAttraction() {
            return pOINoOfNearByAttraction;
        }

        /**
         *
         * @param pOINoOfNearByAttraction
         * The POINoOfNearByAttraction
         */
        public void setPOINoOfNearByAttraction(Integer pOINoOfNearByAttraction) {
            this.pOINoOfNearByAttraction = pOINoOfNearByAttraction;
        }

        /**
         *
         * @return
         * The desID
         */
        public Integer getDesID() {
            return desID;
        }

        /**
         *
         * @param desID
         * The desID
         */
        public void setDesID(Integer desID) {
            this.desID = desID;
        }

        /**
         *
         * @return
         * The desCountry
         */
        public Integer getDesCountry() {
            return desCountry;
        }

        /**
         *
         * @param desCountry
         * The desCountry
         */
        public void setDesCountry(Integer desCountry) {
            this.desCountry = desCountry;
        }

        /**
         *
         * @return
         * The desState
         */
        public Integer getDesState() {
            return desState;
        }

        /**
         *
         * @param desState
         * The desState
         */
        public void setDesState(Integer desState) {
            this.desState = desState;
        }

        /**
         *
         * @return
         * The desDistrict
         */
        public Integer getDesDistrict() {
            return desDistrict;
        }

        /**
         *
         * @param desDistrict
         * The desDistrict
         */
        public void setDesDistrict(Integer desDistrict) {
            this.desDistrict = desDistrict;
        }

        /**
         *
         * @return
         * The desCity
         */
        public Integer getDesCity() {
            return desCity;
        }

        /**
         *
         * @param desCity
         * The desCity
         */
        public void setDesCity(Integer desCity) {
            this.desCity = desCity;
        }

        /**
         *
         * @return
         * The desName
         */
        public String getDesName() {
            if(desName == null)
            {
                return "";
            }
            else {
                return desName;
            }
        }

        /**
         *
         * @param desName
         * The desName
         */
        public void setDesName(String desName) {
            this.desName = desName;
        }

        /**
         *
         * @return
         * The desLatitude
         */
        public String getDesLatitude() {
            return desLatitude;
        }

        /**
         *
         * @param desLatitude
         * The desLatitude
         */
        public void setDesLatitude(String desLatitude) {
            this.desLatitude = desLatitude;
        }

        /**
         *
         * @return
         * The desLongitude
         */
        public String getDesLongitude() {
            return desLongitude;
        }

        /**
         *
         * @param desLongitude
         * The desLongitude
         */
        public void setDesLongitude(String desLongitude) {
            this.desLongitude = desLongitude;
        }

        /**
         *
         * @return
         * The desImage
         */
        public String getDesImage() {
            return desImage;
        }

        /**
         *
         * @param desImage
         * The desImage
         */
        public void setDesImage(String desImage) {
            this.desImage = desImage;
        }

        /**
         *
         * @return
         * The isActive
         */
        public Boolean getIsActive() {
            return isActive;
        }

        /**
         *
         * @param isActive
         * The IsActive
         */
        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        /**
         *
         * @return
         * The desCreateDate
         */
        public String getDesCreateDate() {
            return desCreateDate;
        }

        /**
         *
         * @param desCreateDate
         * The desCreateDate
         */
        public void setDesCreateDate(String desCreateDate) {
            this.desCreateDate = desCreateDate;
        }

        /**
         *
         * @return
         * The desRating
         */
        public String getDesRating() {
            return desRating;
        }

        /**
         *
         * @param desRating
         * The desRating
         */
        public void setDesRating(String desRating) {
            this.desRating = desRating;
        }

        /**
         *
         * @return
         * The desAddress1
         */
        public String getDesAddress1() {
            return desAddress1;
        }

        /**
         *
         * @param desAddress1
         * The desAddress1
         */
        public void setDesAddress1(String desAddress1) {
            this.desAddress1 = desAddress1;
        }

        /**
         *
         * @return
         * The destBestTimeFrom
         */
        public String getDestBestTimeFrom() {
            return destBestTimeFrom;
        }

        /**
         *
         * @param destBestTimeFrom
         * The destBestTimeFrom
         */
        public void setDestBestTimeFrom(String destBestTimeFrom) {
            this.destBestTimeFrom = destBestTimeFrom;
        }

        /**
         *
         * @return
         * The desBestTimeTo
         */
        public String getDesBestTimeTo() {
            return desBestTimeTo;
        }

        /**
         *
         * @param desBestTimeTo
         * The desBestTimeTo
         */
        public void setDesBestTimeTo(String desBestTimeTo) {
            this.desBestTimeTo = desBestTimeTo;
        }

        /**
         *
         * @return
         * The desShortDescription
         */
        public String getDesShortDescription() {
            return desShortDescription;
        }

        /**
         *
         * @param desShortDescription
         * The desShortDescription
         */
        public void setDesShortDescription(String desShortDescription) {
            this.desShortDescription = desShortDescription;
        }

        /**
         *
         * @return
         * The desLongDescription
         */
        public String getDesLongDescription() {
            return desLongDescription;
        }

        /**
         *
         * @param desLongDescription
         * The desLongDescription
         */
        public void setDesLongDescription(String desLongDescription) {
            this.desLongDescription = desLongDescription;
        }

        /**
         *
         * @return
         * The desFestival
         */
        public String getDesFestival() {
            return desFestival;
        }

        /**
         *
         * @param desFestival
         * The desFestival
         */
        public void setDesFestival(String desFestival) {
            this.desFestival = desFestival;
        }

        /**
         *
         * @return
         * The desNoOfPOI
         */
        public Integer getDesNoOfPOI() {
            return desNoOfPOI;
        }

        /**
         *
         * @param desNoOfPOI
         * The desNoOfPOI
         */
        public void setDesNoOfPOI(Integer desNoOfPOI) {
            this.desNoOfPOI = desNoOfPOI;
        }

        /**
         *
         * @return
         * The desNoOfVisited
         */
        public Integer getDesNoOfVisited() {
            return desNoOfVisited;
        }

        /**
         *
         * @param desNoOfVisited
         * The desNoOfVisited
         */
        public void setDesNoOfVisited(Integer desNoOfVisited) {
            this.desNoOfVisited = desNoOfVisited;
        }

        /**
         *
         * @return
         * The desHostDetails
         */
        public String getDesHostDetails() {
            return desHostDetails;
        }

        /**
         *
         * @param desHostDetails
         * The desHostDetails
         */
        public void setDesHostDetails(String desHostDetails) {
            this.desHostDetails = desHostDetails;
        }

        /**
         *
         * @return
         * The desNearByDestination
         */
        public String getDesNearByDestination() {
            return desNearByDestination;
        }

        /**
         *
         * @param desNearByDestination
         * The desNearByDestination
         */
        public void setDesNearByDestination(String desNearByDestination) {
            this.desNearByDestination = desNearByDestination;
        }

        /**
         *
         * @return
         * The desNoOfNearByDestination
         */
        public Integer getDesNoOfNearByDestination() {
            return desNoOfNearByDestination;
        }

        /**
         *
         * @param desNoOfNearByDestination
         * The desNoOfNearByDestination
         */
        public void setDesNoOfNearByDestination(Integer desNoOfNearByDestination) {
            this.desNoOfNearByDestination = desNoOfNearByDestination;
        }

        /**
         *
         * @return
         * The desDistance
         */
        public String getDesDistance() {
            return desDistance;
        }

        /**
         *
         * @param desDistance
         * The desDistance
         */
        public void setDesDistance(String desDistance) {
            this.desDistance = desDistance;
        }

        /**
         *
         * @return
         * The desNoOfNearByAttraction
         */
        public Integer getDesNoOfNearByAttraction() {
            return desNoOfNearByAttraction;
        }

        /**
         *
         * @param desNoOfNearByAttraction
         * The desNoOfNearByAttraction
         */
        public void setDesNoOfNearByAttraction(Integer desNoOfNearByAttraction) {
            this.desNoOfNearByAttraction = desNoOfNearByAttraction;
        }

    }
}