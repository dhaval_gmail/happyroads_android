package com.bpcl.happyroads.Pojo;

/**
 * Created by admin on 10/11/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TripListforPOIPlanning {

    @SerializedName("TripId")
    @Expose
    private Integer tripId;
    @SerializedName("TripName")
    @Expose
    private String tripName;
    @SerializedName("TripDate")
    @Expose
    private String tripDate;
    @SerializedName("TripGoingTo_Start")
    @Expose
    private Integer tripGoingToStart;
    @SerializedName("TripDestination_End")
    @Expose
    private Integer tripDestinationEnd;
    @SerializedName("TripStatus")
    @Expose
    private String tripStatus;
    @SerializedName("TripIsActive")
    @Expose
    private Boolean tripIsActive;
    @SerializedName("TripIsDeleted")
    @Expose
    private Boolean tripIsDeleted;
    @SerializedName("TripCreatedDate")
    @Expose
    private String tripCreatedDate;
    @SerializedName("TripEndDateTime")
    @Expose
    private String tripEndDateTime;
    @SerializedName("TripUmID")
    @Expose
    private Integer tripUmID;
    @SerializedName("TripGooglePlaceDetails")
    @Expose
    private String tripGooglePlaceDetails;
    @SerializedName("TripReturnDate")
    @Expose
    private String tripReturnDate;
    @SerializedName("TripReturnDateETA")
    @Expose
    private String tripReturnDateETA;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The tripId
     */
    public Integer getTripId() {
        return tripId;
    }

    /**
     *
     * @param tripId
     * The TripId
     */
    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    /**
     *
     * @return
     * The tripName
     */
    public String getTripName() {
        return tripName;
    }

    /**
     *
     * @param tripName
     * The TripName
     */
    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    /**
     *
     * @return
     * The tripDate
     */
    public String getTripDate() {
        return tripDate;
    }

    /**
     *
     * @param tripDate
     * The TripDate
     */
    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    /**
     *
     * @return
     * The tripGoingToStart
     */
    public Integer getTripGoingToStart() {
        return tripGoingToStart;
    }

    /**
     *
     * @param tripGoingToStart
     * The TripGoingTo_Start
     */
    public void setTripGoingToStart(Integer tripGoingToStart) {
        this.tripGoingToStart = tripGoingToStart;
    }

    /**
     *
     * @return
     * The tripDestinationEnd
     */
    public Integer getTripDestinationEnd() {
        return tripDestinationEnd;
    }

    /**
     *
     * @param tripDestinationEnd
     * The TripDestination_End
     */
    public void setTripDestinationEnd(Integer tripDestinationEnd) {
        this.tripDestinationEnd = tripDestinationEnd;
    }

    /**
     *
     * @return
     * The tripStatus
     */
    public String getTripStatus() {
        return tripStatus;
    }

    /**
     *
     * @param tripStatus
     * The TripStatus
     */
    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    /**
     *
     * @return
     * The tripIsActive
     */
    public Boolean getTripIsActive() {
        return tripIsActive;
    }

    /**
     *
     * @param tripIsActive
     * The TripIsActive
     */
    public void setTripIsActive(Boolean tripIsActive) {
        this.tripIsActive = tripIsActive;
    }

    /**
     *
     * @return
     * The tripIsDeleted
     */
    public Boolean getTripIsDeleted() {
        return tripIsDeleted;
    }

    /**
     *
     * @param tripIsDeleted
     * The TripIsDeleted
     */
    public void setTripIsDeleted(Boolean tripIsDeleted) {
        this.tripIsDeleted = tripIsDeleted;
    }

    /**
     *
     * @return
     * The tripCreatedDate
     */
    public String getTripCreatedDate() {
        return tripCreatedDate;
    }

    /**
     *
     * @param tripCreatedDate
     * The TripCreatedDate
     */
    public void setTripCreatedDate(String tripCreatedDate) {
        this.tripCreatedDate = tripCreatedDate;
    }

    /**
     *
     * @return
     * The tripEndDateTime
     */
    public String getTripEndDateTime() {
        return tripEndDateTime;
    }

    /**
     *
     * @param tripEndDateTime
     * The TripEndDateTime
     */
    public void setTripEndDateTime(String tripEndDateTime) {
        this.tripEndDateTime = tripEndDateTime;
    }

    /**
     *
     * @return
     * The tripUmID
     */
    public Integer getTripUmID() {
        return tripUmID;
    }

    /**
     *
     * @param tripUmID
     * The TripUmID
     */
    public void setTripUmID(Integer tripUmID) {
        this.tripUmID = tripUmID;
    }

    /**
     *
     * @return
     * The tripGooglePlaceDetails
     */
    public String getTripGooglePlaceDetails() {
        return tripGooglePlaceDetails;
    }

    /**
     *
     * @param tripGooglePlaceDetails
     * The TripGooglePlaceDetails
     */
    public void setTripGooglePlaceDetails(String tripGooglePlaceDetails) {
        this.tripGooglePlaceDetails = tripGooglePlaceDetails;
    }

    /**
     *
     * @return
     * The tripReturnDate
     */
    public String getTripReturnDate() {
        if(tripReturnDate==null)
        {
            return "";
        }else {
            return tripReturnDate;
        }
    }

    /**
     *
     * @param tripReturnDate
     * The TripReturnDate
     */
    public void setTripReturnDate(String tripReturnDate) {
        this.tripReturnDate = tripReturnDate;
    }

    /**
     *
     * @return
     * The tripReturnDateETA
     */
    public String getTripReturnDateETA() {
        return tripReturnDateETA;
    }

    /**
     *
     * @param tripReturnDateETA
     * The TripReturnDateETA
     */
    public void setTripReturnDateETA(String tripReturnDateETA) {
        this.tripReturnDateETA = tripReturnDateETA;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}