package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 04/02/2017.
 */



public class DeleteNotification {

    @SerializedName("ntfUsrId")
    @Expose
    private Integer ntfUsrId;
    @SerializedName("ntfRefKey")
    @Expose
    private Integer ntfRefKey;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("ntfId")
    @Expose
    private Integer ntfId;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("resStatusDescription")
    @Expose
    private String resStatusDescription;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("IsActiveInt")
    @Expose
    private Integer isActiveInt;

    public Integer getNtfUsrId() {
        return ntfUsrId;
    }

    public void setNtfUsrId(Integer ntfUsrId) {
        this.ntfUsrId = ntfUsrId;
    }

    public Integer getNtfRefKey() {
        return ntfRefKey;
    }

    public void setNtfRefKey(Integer ntfRefKey) {
        this.ntfRefKey = ntfRefKey;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getNtfId() {
        return ntfId;
    }

    public void setNtfId(Integer ntfId) {
        this.ntfId = ntfId;
    }

    public Boolean getResStatus() {
        return resStatus;
    }

    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    public String getResCallerDetails() {
        return resCallerDetails;
    }

    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    public String getResStatusDescription() {
        return resStatusDescription;
    }

    public void setResStatusDescription(String resStatusDescription) {
        this.resStatusDescription = resStatusDescription;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Integer getIsActiveInt() {
        return isActiveInt;
    }

    public void setIsActiveInt(Integer isActiveInt) {
        this.isActiveInt = isActiveInt;
    }

}