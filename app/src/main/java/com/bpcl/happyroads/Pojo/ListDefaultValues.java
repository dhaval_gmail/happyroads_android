

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class ListDefaultValues {

    @SerializedName("OptionId")
    @Expose
    private Integer optionId;
    @SerializedName("keyName")
    @Expose
    private String keyName;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;

    /**
     *
     * @return
     * The optionId
     */
    public Integer getOptionId() {
        return optionId;
    }

    /**
     *
     * @param optionId
     * The OptionId
     */
    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    /**
     *
     * @return
     * The keyName
     */
    public String getKeyName() {
        return keyName;
    }

    /**
     *
     * @param keyName
     * The keyName
     */
    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     *
     * @param createdDate
     * The CreatedDate
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}