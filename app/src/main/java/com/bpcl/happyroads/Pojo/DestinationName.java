package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class DestinationName {

    @SerializedName("desID")
    @Expose
    private Integer desID;
    @SerializedName("desCountry")
    @Expose
    private Integer desCountry;
    @SerializedName("desState")
    @Expose
    private Integer desState;
    @SerializedName("desDistrict")
    @Expose
    private Integer desDistrict;
    @SerializedName("desCity")
    @Expose
    private Integer desCity;
    @SerializedName("desName")
    @Expose
    private String desName;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("umId")
    @Expose
    private Integer umId;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
 @SerializedName("desLatitude")
    @Expose
    private String desLatitude;
 @SerializedName("desLongitude")
    @Expose
    private String desLongitude;

    /**
     *
     * @return
     * The desID
     */
    public Integer getDesID() {
        return desID;
    }

    /**
     *
     * @param desID
     * The desID
     */
    public void setDesID(Integer desID) {
        this.desID = desID;
    }

    /**
     *
     * @return
     * The desCountry
     */
    public Integer getDesCountry() {
        return desCountry;
    }

    /**
     *
     * @param desCountry
     * The desCountry
     */
    public void setDesCountry(Integer desCountry) {
        this.desCountry = desCountry;
    }

    /**
     *
     * @return
     * The desState
     */
    public Integer getDesState() {
        return desState;
    }

    /**
     *
     * @param desState
     * The desState
     */
    public void setDesState(Integer desState) {
        this.desState = desState;
    }

    /**
     *
     * @return
     * The desDistrict
     */
    public Integer getDesDistrict() {
        return desDistrict;
    }

    /**
     *
     * @param desDistrict
     * The desDistrict
     */
    public void setDesDistrict(Integer desDistrict) {
        this.desDistrict = desDistrict;
    }

    /**
     *
     * @return
     * The desCity
     */
    public Integer getDesCity() {
        return desCity;
    }

    /**
     *
     * @param desCity
     * The desCity
     */
    public void setDesCity(Integer desCity) {
        this.desCity = desCity;
    }

    /**
     *
     * @return
     * The desName
     */
    public String getDesName() {
        return desName;
    }

    /**
     *
     * @param desName
     * The desName
     */
    public void setDesName(String desName) {
        this.desName = desName;
    }

    /**
     *
     * @return
     * The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     *
     * @return
     * The umId
     */
    public Integer getUmId() {
        return umId;
    }

    /**
     *
     * @param umId
     * The umId
     */
    public void setUmId(Integer umId) {
        this.umId = umId;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }



    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getDesLatitude() {
        return desLatitude;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setDesLatitude(String desLatitude) {
        this.desLatitude = desLatitude;
    }
 public String getDesLongitude() {
        return desLongitude;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setDesLongitude(String desLongitude) {
        this.desLongitude = desLongitude;
    }

}