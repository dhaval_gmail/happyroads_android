package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 10/15/2016.
 */

public class FavPOIList {

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("desName")
    @Expose
    private String desName;
    @SerializedName("POIID")
    @Expose
    private Integer pOIID;
    @SerializedName("POICatId")
    @Expose
    private Integer pOICatId;
    @SerializedName("POITypeId")
    @Expose
    private String pOITypeId;
    @SerializedName("POIImage")
    @Expose
    private String pOIImage;
    @SerializedName("POINearbyDestination")
    @Expose
    private Integer pOINearbyDestination;
    @SerializedName("POIName")
    @Expose
    private String pOIName;
    @SerializedName("POIAddress1")
    @Expose
    private String pOIAddress1;
    @SerializedName("POILatitude")
    @Expose
    private String pOILatitude;
    @SerializedName("POILongitude")
    @Expose
    private String pOILongitude;
    @SerializedName("POIState")
    @Expose
    private Integer pOIState;
    @SerializedName("POICity")
    @Expose
    private Integer pOICity;
    @SerializedName("POIZipCode")
    @Expose
    private String pOIZipCode;
    @SerializedName("POIIsActive")
    @Expose
    private Boolean pOIIsActive;
    @SerializedName("POICreateDate")
    @Expose
    private String pOICreateDate;
    @SerializedName("POIUpdateDate")
    @Expose
    private String pOIUpdateDate;
    @SerializedName("POIHostDetails")
    @Expose
    private String pOIHostDetails;
    @SerializedName("POIAdminId")
    @Expose
    private Integer pOIAdminId;
    @SerializedName("POIIsFullTime")
    @Expose
    private Boolean pOIIsFullTime;
    @SerializedName("POIStartTime")
    @Expose
    private String pOIStartTime;
    @SerializedName("POIEndTime")
    @Expose
    private String pOIEndTime;
    @SerializedName("POIBestTimeFrom")
    @Expose
    private String pOIBestTimeFrom;
    @SerializedName("POIBestTimeTo")
    @Expose
    private String pOIBestTimeTo;
    @SerializedName("POIShortDescription")
    @Expose
    private String pOIShortDescription;
    @SerializedName("POILongDescription")
    @Expose
    private String pOILongDescription;
    @SerializedName("POINoofVisited")
    @Expose
    private Integer pOINoofVisited;
    @SerializedName("POIRating")
    @Expose
    private String pOIRating;
    @SerializedName("POISpendTime")
    @Expose
    private String pOISpendTime;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("statename")
    @Expose
    private String statename;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The rowNum
     */
    public Integer getRowNum() {
        return rowNum;
    }

    /**
     *
     * @param rowNum
     * The RowNum
     */
    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    /**
     *
     * @return
     * The distance
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     *
     * @return
     * The desName
     */
    public String getDesName() {
        return desName;
    }

    /**
     *
     * @param desName
     * The desName
     */
    public void setDesName(String desName) {
        this.desName = desName;
    }

    /**
     *
     * @return
     * The pOIID
     */
    public Integer getPOIID() {
        return pOIID;
    }

    /**
     *
     * @param pOIID
     * The POIID
     */
    public void setPOIID(Integer pOIID) {
        this.pOIID = pOIID;
    }

    /**
     *
     * @return
     * The pOICatId
     */
    public Integer getPOICatId() {
        return pOICatId;
    }

    /**
     *
     * @param pOICatId
     * The POICatId
     */
    public void setPOICatId(Integer pOICatId) {
        this.pOICatId = pOICatId;
    }

    /**
     *
     * @return
     * The pOITypeId
     */
    public String getPOITypeId() {
        return pOITypeId;
    }

    /**
     *
     * @param pOITypeId
     * The POITypeId
     */
    public void setPOITypeId(String pOITypeId) {
        this.pOITypeId = pOITypeId;
    }

    /**
     *
     * @return
     * The pOIImage
     */
    public String getPOIImage() {
        return pOIImage;
    }

    /**
     *
     * @param pOIImage
     * The POIImage
     */
    public void setPOIImage(String pOIImage) {
        this.pOIImage = pOIImage;
    }

    /**
     *
     * @return
     * The pOINearbyDestination
     */
    public Integer getPOINearbyDestination() {
        return pOINearbyDestination;
    }

    /**
     *
     * @param pOINearbyDestination
     * The POINearbyDestination
     */
    public void setPOINearbyDestination(Integer pOINearbyDestination) {
        this.pOINearbyDestination = pOINearbyDestination;
    }

    /**
     *
     * @return
     * The pOIName
     */
    public String getPOIName() {
        return pOIName;
    }

    /**
     *
     * @param pOIName
     * The POIName
     */
    public void setPOIName(String pOIName) {
        this.pOIName = pOIName;
    }

    /**
     *
     * @return
     * The pOIAddress1
     */
    public String getPOIAddress1() {
        return pOIAddress1;
    }

    /**
     *
     * @param pOIAddress1
     * The POIAddress1
     */
    public void setPOIAddress1(String pOIAddress1) {
        this.pOIAddress1 = pOIAddress1;
    }

    /**
     *
     * @return
     * The pOILatitude
     */
    public String getPOILatitude() {
        return pOILatitude;
    }

    /**
     *
     * @param pOILatitude
     * The POILatitude
     */
    public void setPOILatitude(String pOILatitude) {
        this.pOILatitude = pOILatitude;
    }

    /**
     *
     * @return
     * The pOILongitude
     */
    public String getPOILongitude() {
        return pOILongitude;
    }

    /**
     *
     * @param pOILongitude
     * The POILongitude
     */
    public void setPOILongitude(String pOILongitude) {
        this.pOILongitude = pOILongitude;
    }

    /**
     *
     * @return
     * The pOIState
     */
    public Integer getPOIState() {
        return pOIState;
    }

    /**
     *
     * @param pOIState
     * The POIState
     */
    public void setPOIState(Integer pOIState) {
        this.pOIState = pOIState;
    }

    /**
     *
     * @return
     * The pOICity
     */
    public Integer getPOICity() {
        return pOICity;
    }

    /**
     *
     * @param pOICity
     * The POICity
     */
    public void setPOICity(Integer pOICity) {
        this.pOICity = pOICity;
    }

    /**
     *
     * @return
     * The pOIZipCode
     */
    public String getPOIZipCode() {
        return pOIZipCode;
    }

    /**
     *
     * @param pOIZipCode
     * The POIZipCode
     */
    public void setPOIZipCode(String pOIZipCode) {
        this.pOIZipCode = pOIZipCode;
    }

    /**
     *
     * @return
     * The pOIIsActive
     */
    public Boolean getPOIIsActive() {
        return pOIIsActive;
    }

    /**
     *
     * @param pOIIsActive
     * The POIIsActive
     */
    public void setPOIIsActive(Boolean pOIIsActive) {
        this.pOIIsActive = pOIIsActive;
    }

    /**
     *
     * @return
     * The pOICreateDate
     */
    public String getPOICreateDate() {
        return pOICreateDate;
    }

    /**
     *
     * @param pOICreateDate
     * The POICreateDate
     */
    public void setPOICreateDate(String pOICreateDate) {
        this.pOICreateDate = pOICreateDate;
    }

    /**
     *
     * @return
     * The pOIUpdateDate
     */
    public String getPOIUpdateDate() {
        return pOIUpdateDate;
    }

    /**
     *
     * @param pOIUpdateDate
     * The POIUpdateDate
     */
    public void setPOIUpdateDate(String pOIUpdateDate) {
        this.pOIUpdateDate = pOIUpdateDate;
    }

    /**
     *
     * @return
     * The pOIHostDetails
     */
    public String getPOIHostDetails() {
        return pOIHostDetails;
    }

    /**
     *
     * @param pOIHostDetails
     * The POIHostDetails
     */
    public void setPOIHostDetails(String pOIHostDetails) {
        this.pOIHostDetails = pOIHostDetails;
    }

    /**
     *
     * @return
     * The pOIAdminId
     */
    public Integer getPOIAdminId() {
        return pOIAdminId;
    }

    /**
     *
     * @param pOIAdminId
     * The POIAdminId
     */
    public void setPOIAdminId(Integer pOIAdminId) {
        this.pOIAdminId = pOIAdminId;
    }

    /**
     *
     * @return
     * The pOIIsFullTime
     */
    public Boolean getPOIIsFullTime() {
        return pOIIsFullTime;
    }

    /**
     *
     * @param pOIIsFullTime
     * The POIIsFullTime
     */
    public void setPOIIsFullTime(Boolean pOIIsFullTime) {
        this.pOIIsFullTime = pOIIsFullTime;
    }

    /**
     *
     * @return
     * The pOIStartTime
     */
    public String getPOIStartTime() {
        return pOIStartTime;
    }

    /**
     *
     * @param pOIStartTime
     * The POIStartTime
     */
    public void setPOIStartTime(String pOIStartTime) {
        this.pOIStartTime = pOIStartTime;
    }

    /**
     *
     * @return
     * The pOIEndTime
     */
    public String getPOIEndTime() {
        return pOIEndTime;
    }

    /**
     *
     * @param pOIEndTime
     * The POIEndTime
     */
    public void setPOIEndTime(String pOIEndTime) {
        this.pOIEndTime = pOIEndTime;
    }

    /**
     *
     * @return
     * The pOIBestTimeFrom
     */
    public String getPOIBestTimeFrom() {
        return pOIBestTimeFrom;
    }

    /**
     *
     * @param pOIBestTimeFrom
     * The POIBestTimeFrom
     */
    public void setPOIBestTimeFrom(String pOIBestTimeFrom) {
        this.pOIBestTimeFrom = pOIBestTimeFrom;
    }

    /**
     *
     * @return
     * The pOIBestTimeTo
     */
    public String getPOIBestTimeTo() {
        return pOIBestTimeTo;
    }

    /**
     *
     * @param pOIBestTimeTo
     * The POIBestTimeTo
     */
    public void setPOIBestTimeTo(String pOIBestTimeTo) {
        this.pOIBestTimeTo = pOIBestTimeTo;
    }

    /**
     *
     * @return
     * The pOIShortDescription
     */
    public String getPOIShortDescription() {
        return pOIShortDescription;
    }

    /**
     *
     * @param pOIShortDescription
     * The POIShortDescription
     */
    public void setPOIShortDescription(String pOIShortDescription) {
        this.pOIShortDescription = pOIShortDescription;
    }

    /**
     *
     * @return
     * The pOILongDescription
     */
    public String getPOILongDescription() {
        return pOILongDescription;
    }

    /**
     *
     * @param pOILongDescription
     * The POILongDescription
     */
    public void setPOILongDescription(String pOILongDescription) {
        this.pOILongDescription = pOILongDescription;
    }

    /**
     *
     * @return
     * The pOINoofVisited
     */
    public Integer getPOINoofVisited() {
        return pOINoofVisited;
    }

    /**
     *
     * @param pOINoofVisited
     * The POINoofVisited
     */
    public void setPOINoofVisited(Integer pOINoofVisited) {
        this.pOINoofVisited = pOINoofVisited;
    }

    /**
     *
     * @return
     * The pOIRating
     */
    public String getPOIRating() {
        return pOIRating;
    }

    /**
     *
     * @param pOIRating
     * The POIRating
     */
    public void setPOIRating(String pOIRating) {
        this.pOIRating = pOIRating;
    }

    /**
     *
     * @return
     * The pOISpendTime
     */
    public String getPOISpendTime() {
        return pOISpendTime;
    }

    /**
     *
     * @param pOISpendTime
     * The POISpendTime
     */
    public void setPOISpendTime(String pOISpendTime) {
        this.pOISpendTime = pOISpendTime;
    }

    /**
     *
     * @return
     * The cityname
     */
    public String getCityname() {
        return cityname;
    }

    /**
     *
     * @param cityname
     * The cityname
     */
    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    /**
     *
     * @return
     * The statename
     */
    public String getStatename() {
        return statename;
    }

    /**
     *
     * @param statename
     * The statename
     */
    public void setStatename(String statename) {
        this.statename = statename;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}
