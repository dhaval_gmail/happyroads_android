

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class CalculateDistance {

    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("end_address")
    @Expose
    private String endAddress;
    @SerializedName("start_address")
    @Expose
    private String startAddress;

    /**
     * @return The distance
     */
    public Distance getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    /**
     * @return The duration
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * @return The endAddress
     */
    public String getEndAddress() {
        return endAddress;
    }

    /**
     * @param endAddress The end_address
     */
    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    /**
     * @return The startAddress
     */
    public String getStartAddress() {
        return startAddress;
    }

    /**
     * @param startAddress The start_address
     */
    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }




    @Generated("org.jsonschema2pojo")
    public class Distance {

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("value")
        @Expose
        private Integer value;

        /**
         * @return The text
         */
        public String getText() {
            return text;
        }

        /**
         * @param text The text
         */
        public void setText(String text) {
            this.text = text;
        }

        /**
         * @return The value
         */
        public Integer getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        public void setValue(Integer value) {
            this.value = value;
        }

    }

    @Generated("org.jsonschema2pojo")
    public class Duration {

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("value")
        @Expose
        private Integer value;

        /**
         * @return The text
         */
        public String getText() {
            return text;
        }

        /**
         * @param text The text
         */
        public void setText(String text) {
            this.text = text;
        }

        /**
         * @return The value
         */
        public Integer getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        public void setValue(Integer value) {
            this.value = value;
        }

    }
}