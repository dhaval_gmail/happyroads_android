package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 18/11/2016.
 */
public class LogOut {

    @SerializedName("umId")
    @Expose
    private Integer umId;
    @SerializedName("umCreatedDate")
    @Expose
    private String umCreatedDate;
    @SerializedName("umUpdateDate")
    @Expose
    private String umUpdateDate;
    @SerializedName("umLastLoginDate")
    @Expose
    private String umLastLoginDate;
    @SerializedName("umIsActive")
    @Expose
    private Boolean umIsActive;
    @SerializedName("umIsDeleted")
    @Expose
    private Boolean umIsDeleted;
    @SerializedName("umUdid")
    @Expose
    private String umUdid;
    @SerializedName("umIsPushOn")
    @Expose
    private Boolean umIsPushOn;
    @SerializedName("umDeviceType")
    @Expose
    private String umDeviceType;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The umId
     */
    public Integer getUmId() {
        return umId;
    }

    /**
     *
     * @param umId
     * The umId
     */
    public void setUmId(Integer umId) {
        this.umId = umId;
    }

    /**
     *
     * @return
     * The umCreatedDate
     */
    public String getUmCreatedDate() {
        return umCreatedDate;
    }

    /**
     *
     * @param umCreatedDate
     * The umCreatedDate
     */
    public void setUmCreatedDate(String umCreatedDate) {
        this.umCreatedDate = umCreatedDate;
    }

    /**
     *
     * @return
     * The umUpdateDate
     */
    public String getUmUpdateDate() {
        return umUpdateDate;
    }

    /**
     *
     * @param umUpdateDate
     * The umUpdateDate
     */
    public void setUmUpdateDate(String umUpdateDate) {
        this.umUpdateDate = umUpdateDate;
    }

    /**
     *
     * @return
     * The umLastLoginDate
     */
    public String getUmLastLoginDate() {
        return umLastLoginDate;
    }

    /**
     *
     * @param umLastLoginDate
     * The umLastLoginDate
     */
    public void setUmLastLoginDate(String umLastLoginDate) {
        this.umLastLoginDate = umLastLoginDate;
    }

    /**
     *
     * @return
     * The umIsActive
     */
    public Boolean getUmIsActive() {
        return umIsActive;
    }

    /**
     *
     * @param umIsActive
     * The umIsActive
     */
    public void setUmIsActive(Boolean umIsActive) {
        this.umIsActive = umIsActive;
    }

    /**
     *
     * @return
     * The umIsDeleted
     */
    public Boolean getUmIsDeleted() {
        return umIsDeleted;
    }

    /**
     *
     * @param umIsDeleted
     * The umIsDeleted
     */
    public void setUmIsDeleted(Boolean umIsDeleted) {
        this.umIsDeleted = umIsDeleted;
    }

    /**
     *
     * @return
     * The umUdid
     */
    public String getUmUdid() {
        return umUdid;
    }

    /**
     *
     * @param umUdid
     * The umUdid
     */
    public void setUmUdid(String umUdid) {
        this.umUdid = umUdid;
    }

    /**
     *
     * @return
     * The umIsPushOn
     */
    public Boolean getUmIsPushOn() {
        return umIsPushOn;
    }

    /**
     *
     * @param umIsPushOn
     * The umIsPushOn
     */
    public void setUmIsPushOn(Boolean umIsPushOn) {
        this.umIsPushOn = umIsPushOn;
    }

    /**
     *
     * @return
     * The umDeviceType
     */
    public String getUmDeviceType() {
        return umDeviceType;
    }

    /**
     *
     * @param umDeviceType
     * The umDeviceType
     */
    public void setUmDeviceType(String umDeviceType) {
        this.umDeviceType = umDeviceType;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}
