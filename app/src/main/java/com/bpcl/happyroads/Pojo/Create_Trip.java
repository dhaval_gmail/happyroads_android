package com.bpcl.happyroads.Pojo;

/**
 * Created by ADMIN on 9/30/2016.
 */
public class Create_Trip {

    String source,destination,startdate,enddate,starttime,endtime,tripname,tripstatus,triprating,des_address,return_start_time,return_start_date,return_end_time,return_end_date;
    Double source_latitude;
    Double source_longitude;
    Double destination_latitude;
    Double destination_longitude;

    public Double getTripGooglePlaceLat() {
        return tripGooglePlaceLat;
    }

    public void setTripGooglePlaceLat(Double tripGooglePlaceLat) {
        this.tripGooglePlaceLat = tripGooglePlaceLat;
    }

    public Double getTripGooglePlaceLong() {
        return tripGooglePlaceLong;
    }

    public void setTripGooglePlaceLong(Double tripGooglePlaceLong) {
        this.tripGooglePlaceLong = tripGooglePlaceLong;
    }

    Double tripGooglePlaceLat=0.0;
    Double tripGooglePlaceLong=0.0;;
    byte[] source_image,destination_image;
    Integer Id,tripId,sourceId,destinationId;

    Double KM,returnKM;
    Double time,returnTime;
    Boolean returnstatus,tripIsOneSideCompleted;
    String  source_image_url,destination_image_url;
    Double eta_diff_second,eta_return_diff_second;
    Integer DesNoOfNearByAttraction;


    public Integer getDesNoOfNearByAttraction() {
        return DesNoOfNearByAttraction;
    }

    public void setDesNoOfNearByAttraction(Integer desNoOfNearByAttraction) {
        DesNoOfNearByAttraction = desNoOfNearByAttraction;
    }

    public Double getReturnKM() {
        return returnKM;
    }

    public void setReturnKM(Double returnKM) {
        this.returnKM = returnKM;
    }

    public Double getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Double returnTime) {
        this.returnTime = returnTime;
    }

    public Double getEta_return_diff_second() {
        return eta_return_diff_second;
    }

    public void setEta_return_diff_second(Double eta_return_diff_second) {
        this.eta_return_diff_second = eta_return_diff_second;
    }

    public Double getEta_diff_second() {
        return eta_diff_second;

    }

    public void setEta_diff_second(Double eta_diff_second) {
        this.eta_diff_second = eta_diff_second;
    }
    public String getSource_image_url() {
        return source_image_url;
    }

    public void setSource_image_url(String source_image_url) {
        this.source_image_url = source_image_url;
    }

    public String getDestination_image_url() {
        return destination_image_url;
    }

    public void setDestination_image_url(String destination_image_url) {
        this.destination_image_url = destination_image_url;
    }

    public String getReturn_end_date() {
        return return_end_date;
    }

    public void setReturn_end_date(String return_end_date) {
        this.return_end_date = return_end_date;
    }

    public String getReturn_start_time() {
        return return_start_time;
    }

    public void setReturn_start_time(String return_start_time) {
        this.return_start_time = return_start_time;
    }

    public String getReturn_start_date() {
        if(return_start_date=="")
        {
            return "";

        }else
        {
            return return_start_date;

        }
    }

    public void setReturn_start_date(String return_start_date) {
        this.return_start_date = return_start_date;
    }

    public String getReturn_end_time() {
        return return_end_time;
    }

    public void setReturn_end_time(String return_end_time) {
        this.return_end_time = return_end_time;
    }



    public String getDes_address() {
        return des_address;
    }

    public void setDes_address(String des_address) {
        this.des_address = des_address;
    }

    public Boolean getReturnstatus() {
        return returnstatus;
    }

    public void setReturnstatus(Boolean returnstatus) {
        this.returnstatus = returnstatus;
    }

    public Double getKM() {
        return KM;
    }

    public void setKM(Double KM) {
        this.KM = KM;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {

        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getTripname() {
        return tripname;
    }

    public void setTripname(String tripname) {
        this.tripname = tripname;
    }

    public Double getSource_latitude() {
        return source_latitude;
    }

    public void setSource_latitude(Double source_latitude) {
        this.source_latitude = source_latitude;
    }

    public Double getSource_longitude() {
        return source_longitude;
    }

    public void setSource_longitude(Double source_longitude) {
        this.source_longitude = source_longitude;
    }

    public Double getDestination_latitude() {
        return destination_latitude;
    }

    public void setDestination_latitude(Double destination_latitude) {
        this.destination_latitude = destination_latitude;
    }

    public Double getDestination_longitude() {
        return destination_longitude;
    }

    public void setDestination_longitude(Double destination_longitude) {
        this.destination_longitude = destination_longitude;
    }

    public byte[] getSource_image() {
        return source_image;
    }

    public void setSource_image(byte[] source_image) {
        this.source_image = source_image;
    }

    public byte[] getDestination_image() {
        return destination_image;
    }

    public void setDestination_image(byte[] destination_image) {
        this.destination_image = destination_image;
    }

    public String getTripstatus() {
        return tripstatus;
    }

    public void setTripstatus(String tripstatus) {
        this.tripstatus = tripstatus;
    }

    public String getTriprating() {
        return triprating;
    }

    public void setTriprating(String triprating) {
        this.triprating = triprating;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Boolean getTripIsOneSideCompleted() {
        if(tripIsOneSideCompleted == null)
        {
            return false;
        }
        else {
            return tripIsOneSideCompleted;
        }
    }

    public void setTripIsOneSideCompleted(Boolean tripIsOneSideCompleted) {
        this.tripIsOneSideCompleted = tripIsOneSideCompleted;
    }

}
