package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 10/17/2016.
 */
public class POILocalHolidayMaster {

    @SerializedName("POILHId")
    @Expose
    private Integer pOILHId;
    @SerializedName("POIID")
    @Expose
    private Integer pOIID;
    @SerializedName("POILHDate")
    @Expose
    private String pOILHDate;
    @SerializedName("POILHName")
    @Expose
    private String pOILHName;

    /**
     *
     * @return
     * The pOILHId
     */
    public Integer getPOILHId() {
        return pOILHId;
    }

    /**
     *
     * @param pOILHId
     * The POILHId
     */
    public void setPOILHId(Integer pOILHId) {
        this.pOILHId = pOILHId;
    }

    /**
     *
     * @return
     * The pOIID
     */
    public Integer getPOIID() {
        return pOIID;
    }

    /**
     *
     * @param pOIID
     * The POIID
     */
    public void setPOIID(Integer pOIID) {
        this.pOIID = pOIID;
    }

    /**
     *
     * @return
     * The pOILHDate
     */
    public String getPOILHDate() {
        return pOILHDate;
    }

    /**
     *
     * @param pOILHDate
     * The POILHDate
     */
    public void setPOILHDate(String pOILHDate) {
        this.pOILHDate = pOILHDate;
    }

    /**
     *
     * @return
     * The pOILHName
     */
    public String getPOILHName() {
        return pOILHName;
    }

    /**
     *
     * @param pOILHName
     * The POILHName
     */
    public void setPOILHName(String pOILHName) {
        this.pOILHName = pOILHName;
    }

}
