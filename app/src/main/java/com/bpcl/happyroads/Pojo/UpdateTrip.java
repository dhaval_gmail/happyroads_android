
        package com.bpcl.happyroads.Pojo;

        import javax.annotation.Generated;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class UpdateTrip {

    @SerializedName("TripId")
    @Expose
    private Integer tripId;
    @SerializedName("TripName")
    @Expose
    private String tripName;
    @SerializedName("TripDate")
    @Expose
    private String tripDate;
    @SerializedName("TripGoingTo_Start")
    @Expose
    private Integer tripGoingToStart;
    @SerializedName("TripDestination_End")
    @Expose
    private Integer tripDestinationEnd;
    @SerializedName("TripStatus")
    @Expose
    private String tripStatus;
    @SerializedName("TripEndDateTime")
    @Expose
    private String tripEndDateTime;
    @SerializedName("TripUmID")
    @Expose
    private Integer tripUmID;
    @SerializedName("TripCurrentLatitude")
    @Expose
    private String tripCurrentLatitude;
    @SerializedName("TripCurrentLongitude")
    @Expose
    private String tripCurrentLongitude;
    @SerializedName("TripCurrentLocation")
    @Expose
    private String tripCurrentLocation;
    @SerializedName("TripRoute")
    @Expose
    private Integer tripRoute;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private String resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The tripId
     */
    public Integer getTripId() {
        return tripId;
    }

    /**
     *
     * @param tripId
     * The TripId
     */
    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    /**
     *
     * @return
     * The tripName
     */
    public String getTripName() {
        return tripName;
    }

    /**
     *
     * @param tripName
     * The TripName
     */
    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    /**
     *
     * @return
     * The tripDate
     */
    public String getTripDate() {
        return tripDate;
    }

    /**
     *
     * @param tripDate
     * The TripDate
     */
    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    /**
     *
     * @return
     * The tripGoingToStart
     */
    public Integer getTripGoingToStart() {
        return tripGoingToStart;
    }

    /**
     *
     * @param tripGoingToStart
     * The TripGoingTo_Start
     */
    public void setTripGoingToStart(Integer tripGoingToStart) {
        this.tripGoingToStart = tripGoingToStart;
    }

    /**
     *
     * @return
     * The tripDestinationEnd
     */
    public Integer getTripDestinationEnd() {
        return tripDestinationEnd;
    }

    /**
     *
     * @param tripDestinationEnd
     * The TripDestination_End
     */
    public void setTripDestinationEnd(Integer tripDestinationEnd) {
        this.tripDestinationEnd = tripDestinationEnd;
    }

    /**
     *
     * @return
     * The tripStatus
     */
    public String getTripStatus() {
        return tripStatus;
    }

    /**
     *
     * @param tripStatus
     * The TripStatus
     */
    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    /**
     *
     * @return
     * The tripEndDateTime
     */
    public String getTripEndDateTime() {
        return tripEndDateTime;
    }

    /**
     *
     * @param tripEndDateTime
     * The TripEndDateTime
     */
    public void setTripEndDateTime(String tripEndDateTime) {
        this.tripEndDateTime = tripEndDateTime;
    }

    /**
     *
     * @return
     * The tripUmID
     */
    public Integer getTripUmID() {
        return tripUmID;
    }

    /**
     *
     * @param tripUmID
     * The TripUmID
     */
    public void setTripUmID(Integer tripUmID) {
        this.tripUmID = tripUmID;
    }

    /**
     *
     * @return
     * The tripCurrentLatitude
     */
    public String getTripCurrentLatitude() {
        return tripCurrentLatitude;
    }

    /**
     *
     * @param tripCurrentLatitude
     * The TripCurrentLatitude
     */
    public void setTripCurrentLatitude(String tripCurrentLatitude) {
        this.tripCurrentLatitude = tripCurrentLatitude;
    }

    /**
     *
     * @return
     * The tripCurrentLongitude
     */
    public String getTripCurrentLongitude() {
        return tripCurrentLongitude;
    }

    /**
     *
     * @param tripCurrentLongitude
     * The TripCurrentLongitude
     */
    public void setTripCurrentLongitude(String tripCurrentLongitude) {
        this.tripCurrentLongitude = tripCurrentLongitude;
    }

    /**
     *
     * @return
     * The tripCurrentLocation
     */
    public String getTripCurrentLocation() {
        return tripCurrentLocation;
    }

    /**
     *
     * @param tripCurrentLocation
     * The TripCurrentLocation
     */
    public void setTripCurrentLocation(String tripCurrentLocation) {
        this.tripCurrentLocation = tripCurrentLocation;
    }

    /**
     *
     * @return
     * The tripRoute
     */
    public Integer getTripRoute() {
        return tripRoute;
    }

    /**
     *
     * @param tripRoute
     * The TripRoute
     */
    public void setTripRoute(Integer tripRoute) {
        this.tripRoute = tripRoute;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public String getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(String resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}
