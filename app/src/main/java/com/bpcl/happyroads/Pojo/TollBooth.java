

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class TollBooth {

    @SerializedName("tollID")
    @Expose
    private Integer tollID;
    @SerializedName("TollBoothName")
    @Expose
    private String tollBoothName;
    @SerializedName("Distance")
    @Expose
    private Double distance;
    @SerializedName("LCV")
    @Expose
    private String lCV;
    @SerializedName("tollIsActive")
    @Expose
    private Boolean tollIsActive;
    @SerializedName("tollCreateDate")
    @Expose
    private String tollCreateDate;
    @SerializedName("TollRouteId")
    @Expose
    private Integer tollRouteId;
    @SerializedName("TollRemark")
    @Expose
    private String tollRemark;
    @SerializedName("RouteId")
    @Expose
    private Integer routeId;
    @SerializedName("RouteNumber")
    @Expose
    private String routeNumber;
    @SerializedName("RouteOrigin")
    @Expose
    private String routeOrigin;
    @SerializedName("RouteOriginLat")
    @Expose
    private String routeOriginLat;
    @SerializedName("RouteOriginLong")
    @Expose
    private String routeOriginLong;
    @SerializedName("RouteDestination")
    @Expose
    private String routeDestination;
    @SerializedName("RouteDestinationLat")
    @Expose
    private String routeDestinationLat;
    @SerializedName("RouteDestinationLong")
    @Expose
    private String routeDestinationLong;
    @SerializedName("RouteTravelVia")
    @Expose
    private String routeTravelVia;
    @SerializedName("RouteTravelViaLat")
    @Expose
    private String routeTravelViaLat;
    @SerializedName("RouteTravelViaLong")
    @Expose
    private String routeTravelViaLong;
    @SerializedName("RouteDistance")
    @Expose
    private String routeDistance;
    @SerializedName("RouteDistanceTime")
    @Expose
    private String routeDistanceTime;
    @SerializedName("RouteEstCost")
    @Expose
    private String routeEstCost;
    @SerializedName("RouteStatus")
    @Expose
    private String routeStatus;
    @SerializedName("RouteIsActive")
    @Expose
    private Boolean routeIsActive;
    @SerializedName("RouteCreatedDate")
    @Expose
    private String routeCreatedDate;
    @SerializedName("RouteUpdateDate")
    @Expose
    private String routeUpdateDate;
    @SerializedName("RouteHostDetails")
    @Expose
    private String routeHostDetails;
    @SerializedName("RouteAdminId")
    @Expose
    private Integer routeAdminId;
    @SerializedName("RouteWayPoints")
    @Expose
    private String routeWayPoints;
    @SerializedName("MainRouteId")
    @Expose
    private Integer mainRouteId;
    @SerializedName("RouteOriginId")
    @Expose
    private Integer routeOriginId;
    @SerializedName("RouteDesId")
    @Expose
    private Integer routeDesId;
    @SerializedName("RouteDistanceValue")
    @Expose
    private String routeDistanceValue;
    @SerializedName("RouteDurationValue")
    @Expose
    private String routeDurationValue;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The tollID
     */
    public Integer getTollID() {
        return tollID;
    }

    /**
     *
     * @param tollID
     * The tollID
     */
    public void setTollID(Integer tollID) {
        this.tollID = tollID;
    }

    /**
     *
     * @return
     * The tollBoothName
     */
    public String getTollBoothName() {
        return tollBoothName;
    }

    /**
     *
     * @param tollBoothName
     * The TollBoothName
     */
    public void setTollBoothName(String tollBoothName) {
        this.tollBoothName = tollBoothName;
    }

    /**
     *
     * @return
     * The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The Distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The lCV
     */
    public String getLCV() {
        return lCV;
    }

    /**
     *
     * @param lCV
     * The LCV
     */
    public void setLCV(String lCV) {
        this.lCV = lCV;
    }

    /**
     *
     * @return
     * The tollIsActive
     */
    public Boolean getTollIsActive() {
        return tollIsActive;
    }

    /**
     *
     * @param tollIsActive
     * The tollIsActive
     */
    public void setTollIsActive(Boolean tollIsActive) {
        this.tollIsActive = tollIsActive;
    }

    /**
     *
     * @return
     * The tollCreateDate
     */
    public String getTollCreateDate() {
        return tollCreateDate;
    }

    /**
     *
     * @param tollCreateDate
     * The tollCreateDate
     */
    public void setTollCreateDate(String tollCreateDate) {
        this.tollCreateDate = tollCreateDate;
    }

    /**
     *
     * @return
     * The tollRouteId
     */
    public Integer getTollRouteId() {
        return tollRouteId;
    }

    /**
     *
     * @param tollRouteId
     * The TollRouteId
     */
    public void setTollRouteId(Integer tollRouteId) {
        this.tollRouteId = tollRouteId;
    }

    /**
     *
     * @return
     * The tollRemark
     */
    public String getTollRemark() {
        return tollRemark;
    }

    /**
     *
     * @param tollRemark
     * The TollRemark
     */
    public void setTollRemark(String tollRemark) {
        this.tollRemark = tollRemark;
    }

    /**
     *
     * @return
     * The routeId
     */
    public Integer getRouteId() {
        return routeId;
    }

    /**
     *
     * @param routeId
     * The RouteId
     */
    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    /**
     *
     * @return
     * The routeNumber
     */
    public String getRouteNumber() {
        return routeNumber;
    }

    /**
     *
     * @param routeNumber
     * The RouteNumber
     */
    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    /**
     *
     * @return
     * The routeOrigin
     */
    public String getRouteOrigin() {
        return routeOrigin;
    }

    /**
     *
     * @param routeOrigin
     * The RouteOrigin
     */
    public void setRouteOrigin(String routeOrigin) {
        this.routeOrigin = routeOrigin;
    }

    /**
     *
     * @return
     * The routeOriginLat
     */
    public String getRouteOriginLat() {
        return routeOriginLat;
    }

    /**
     *
     * @param routeOriginLat
     * The RouteOriginLat
     */
    public void setRouteOriginLat(String routeOriginLat) {
        this.routeOriginLat = routeOriginLat;
    }

    /**
     *
     * @return
     * The routeOriginLong
     */
    public String getRouteOriginLong() {
        return routeOriginLong;
    }

    /**
     *
     * @param routeOriginLong
     * The RouteOriginLong
     */
    public void setRouteOriginLong(String routeOriginLong) {
        this.routeOriginLong = routeOriginLong;
    }

    /**
     *
     * @return
     * The routeDestination
     */
    public String getRouteDestination() {
        return routeDestination;
    }

    /**
     *
     * @param routeDestination
     * The RouteDestination
     */
    public void setRouteDestination(String routeDestination) {
        this.routeDestination = routeDestination;
    }

    /**
     *
     * @return
     * The routeDestinationLat
     */
    public String getRouteDestinationLat() {
        return routeDestinationLat;
    }

    /**
     *
     * @param routeDestinationLat
     * The RouteDestinationLat
     */
    public void setRouteDestinationLat(String routeDestinationLat) {
        this.routeDestinationLat = routeDestinationLat;
    }

    /**
     *
     * @return
     * The routeDestinationLong
     */
    public String getRouteDestinationLong() {
        return routeDestinationLong;
    }

    /**
     *
     * @param routeDestinationLong
     * The RouteDestinationLong
     */
    public void setRouteDestinationLong(String routeDestinationLong) {
        this.routeDestinationLong = routeDestinationLong;
    }

    /**
     *
     * @return
     * The routeTravelVia
     */
    public String getRouteTravelVia() {
        return routeTravelVia;
    }

    /**
     *
     * @param routeTravelVia
     * The RouteTravelVia
     */
    public void setRouteTravelVia(String routeTravelVia) {
        this.routeTravelVia = routeTravelVia;
    }

    /**
     *
     * @return
     * The routeTravelViaLat
     */
    public String getRouteTravelViaLat() {
        return routeTravelViaLat;
    }

    /**
     *
     * @param routeTravelViaLat
     * The RouteTravelViaLat
     */
    public void setRouteTravelViaLat(String routeTravelViaLat) {
        this.routeTravelViaLat = routeTravelViaLat;
    }

    /**
     *
     * @return
     * The routeTravelViaLong
     */
    public String getRouteTravelViaLong() {
        return routeTravelViaLong;
    }

    /**
     *
     * @param routeTravelViaLong
     * The RouteTravelViaLong
     */
    public void setRouteTravelViaLong(String routeTravelViaLong) {
        this.routeTravelViaLong = routeTravelViaLong;
    }

    /**
     *
     * @return
     * The routeDistance
     */
    public String getRouteDistance() {
        return routeDistance;
    }

    /**
     *
     * @param routeDistance
     * The RouteDistance
     */
    public void setRouteDistance(String routeDistance) {
        this.routeDistance = routeDistance;
    }

    /**
     *
     * @return
     * The routeDistanceTime
     */
    public String getRouteDistanceTime() {
        return routeDistanceTime;
    }

    /**
     *
     * @param routeDistanceTime
     * The RouteDistanceTime
     */
    public void setRouteDistanceTime(String routeDistanceTime) {
        this.routeDistanceTime = routeDistanceTime;
    }

    /**
     *
     * @return
     * The routeEstCost
     */
    public String getRouteEstCost() {
        return routeEstCost;
    }

    /**
     *
     * @param routeEstCost
     * The RouteEstCost
     */
    public void setRouteEstCost(String routeEstCost) {
        this.routeEstCost = routeEstCost;
    }

    /**
     *
     * @return
     * The routeStatus
     */
    public String getRouteStatus() {
        return routeStatus;
    }

    /**
     *
     * @param routeStatus
     * The RouteStatus
     */
    public void setRouteStatus(String routeStatus) {
        this.routeStatus = routeStatus;
    }

    /**
     *
     * @return
     * The routeIsActive
     */
    public Boolean getRouteIsActive() {
        return routeIsActive;
    }

    /**
     *
     * @param routeIsActive
     * The RouteIsActive
     */
    public void setRouteIsActive(Boolean routeIsActive) {
        this.routeIsActive = routeIsActive;
    }

    /**
     *
     * @return
     * The routeCreatedDate
     */
    public String getRouteCreatedDate() {
        return routeCreatedDate;
    }

    /**
     *
     * @param routeCreatedDate
     * The RouteCreatedDate
     */
    public void setRouteCreatedDate(String routeCreatedDate) {
        this.routeCreatedDate = routeCreatedDate;
    }

    /**
     *
     * @return
     * The routeUpdateDate
     */
    public String getRouteUpdateDate() {
        return routeUpdateDate;
    }

    /**
     *
     * @param routeUpdateDate
     * The RouteUpdateDate
     */
    public void setRouteUpdateDate(String routeUpdateDate) {
        this.routeUpdateDate = routeUpdateDate;
    }

    /**
     *
     * @return
     * The routeHostDetails
     */
    public String getRouteHostDetails() {
        return routeHostDetails;
    }

    /**
     *
     * @param routeHostDetails
     * The RouteHostDetails
     */
    public void setRouteHostDetails(String routeHostDetails) {
        this.routeHostDetails = routeHostDetails;
    }

    /**
     *
     * @return
     * The routeAdminId
     */
    public Integer getRouteAdminId() {
        return routeAdminId;
    }

    /**
     *
     * @param routeAdminId
     * The RouteAdminId
     */
    public void setRouteAdminId(Integer routeAdminId) {
        this.routeAdminId = routeAdminId;
    }

    /**
     *
     * @return
     * The routeWayPoints
     */
    public String getRouteWayPoints() {
        return routeWayPoints;
    }

    /**
     *
     * @param routeWayPoints
     * The RouteWayPoints
     */
    public void setRouteWayPoints(String routeWayPoints) {
        this.routeWayPoints = routeWayPoints;
    }

    /**
     *
     * @return
     * The mainRouteId
     */
    public Integer getMainRouteId() {
        return mainRouteId;
    }

    /**
     *
     * @param mainRouteId
     * The MainRouteId
     */
    public void setMainRouteId(Integer mainRouteId) {
        this.mainRouteId = mainRouteId;
    }

    /**
     *
     * @return
     * The routeOriginId
     */
    public Integer getRouteOriginId() {
        return routeOriginId;
    }

    /**
     *
     * @param routeOriginId
     * The RouteOriginId
     */
    public void setRouteOriginId(Integer routeOriginId) {
        this.routeOriginId = routeOriginId;
    }

    /**
     *
     * @return
     * The routeDesId
     */
    public Integer getRouteDesId() {
        return routeDesId;
    }

    /**
     *
     * @param routeDesId
     * The RouteDesId
     */
    public void setRouteDesId(Integer routeDesId) {
        this.routeDesId = routeDesId;
    }

    /**
     *
     * @return
     * The routeDistanceValue
     */
    public String getRouteDistanceValue() {
        return routeDistanceValue;
    }

    /**
     *
     * @param routeDistanceValue
     * The RouteDistanceValue
     */
    public void setRouteDistanceValue(String routeDistanceValue) {
        this.routeDistanceValue = routeDistanceValue;
    }

    /**
     *
     * @return
     * The routeDurationValue
     */
    public String getRouteDurationValue() {
        return routeDurationValue;
    }

    /**
     *
     * @param routeDurationValue
     * The RouteDurationValue
     */
    public void setRouteDurationValue(String routeDurationValue) {
        this.routeDurationValue = routeDurationValue;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}