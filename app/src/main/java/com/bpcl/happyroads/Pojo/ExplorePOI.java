

        package com.bpcl.happyroads.Pojo;

        import java.util.ArrayList;
        import java.util.List;
        import javax.annotation.Generated;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ExplorePOI {

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("FavId")
    @Expose
    private Integer favId;
    @SerializedName("desName")
    @Expose
    private String desName;
    @SerializedName("POIID")
    @Expose
    private Integer pOIID;
    @SerializedName("POICatId")
    @Expose
    private Integer pOICatId;
    @SerializedName("POITypeId")
    @Expose
    private String pOITypeId;
    @SerializedName("POIImage")
    @Expose
    private String pOIImage;
    @SerializedName("POINearbyDestination")
    @Expose
    private Integer pOINearbyDestination;
    @SerializedName("POINoOfNearByDestination")
    @Expose
    private Integer pOINoOfNearByDestination;

    public Integer getPOINoOfNearByAttraction() {
        if(POINoOfNearByAttraction == null)
        {
            return 0;
        }
        else {
            return POINoOfNearByAttraction;
        }
    }

    public void setPOINoOfNearByAttraction(Integer POINoOfNearByAttraction) {
        this.POINoOfNearByAttraction = POINoOfNearByAttraction;
    }

    @SerializedName("POINoOfNearByAttraction")
    @Expose

    private Integer POINoOfNearByAttraction;
    @SerializedName("POIName")
    @Expose
    private String pOIName;
    @SerializedName("POIContactPerson")
    @Expose
    private String pOIContactPerson;
    @SerializedName("POICustCareNo")
    @Expose
    private String pOICustCareNo;
    @SerializedName("POIEmailId")
    @Expose
    private Object pOIEmailId;
    @SerializedName("POIAddress1")
    @Expose
    private String pOIAddress1;
    @SerializedName("POIAddress2")
    @Expose
    private Object pOIAddress2;
    @SerializedName("POILatitude")
    @Expose
    private String pOILatitude;
    @SerializedName("POILongitude")
    @Expose
    private String pOILongitude;
    @SerializedName("POISide")
    @Expose
    private Object pOISide;
    @SerializedName("POIState")
    @Expose
    private Integer pOIState;
    @SerializedName("POICity")
    @Expose
    private Integer pOICity;
    @SerializedName("POIZipCode")
    @Expose
    private String pOIZipCode;
    @SerializedName("POITerritory")
    @Expose
    private Object pOITerritory;
    @SerializedName("POIIsActive")
    @Expose
    private Boolean pOIIsActive;
    @SerializedName("POICreateDate")
    @Expose
    private String pOICreateDate;
    @SerializedName("POIUpdateDate")
    @Expose
    private String pOIUpdateDate;
    @SerializedName("POIVicinity")
    @Expose
    private Object pOIVicinity;
    @SerializedName("POIGoogleResponse")
    @Expose
    private Object pOIGoogleResponse;
    @SerializedName("POIHostDetails")
    @Expose
    private String pOIHostDetails;
    @SerializedName("POIAdminId")
    @Expose
    private Integer pOIAdminId;
    @SerializedName("POIGooglePlaceId")
    @Expose
    private Object pOIGooglePlaceId;
    @SerializedName("POIGoogleRating")
    @Expose
    private Object pOIGoogleRating;
    @SerializedName("POIIcon")
    @Expose
    private Object pOIIcon;
    @SerializedName("POIIsFullTime")
    @Expose
    private Boolean pOIIsFullTime;
    @SerializedName("POIStartTime")
    @Expose
    private String pOIStartTime;
    @SerializedName("POIEndTime")
    @Expose
    private String pOIEndTime;
    @SerializedName("POIBestTimeFrom")
    @Expose
    private String pOIBestTimeFrom;
    @SerializedName("POIBestTimeTo")
    @Expose
    private String pOIBestTimeTo;
    @SerializedName("POIShortDescription")
    @Expose
    private String pOIShortDescription;
    @SerializedName("POILongDescription")
    @Expose
    private String pOILongDescription;
    @SerializedName("POINoofVisited")
    @Expose
    private Integer pOINoofVisited;
    @SerializedName("POIRating")
    @Expose
    private String pOIRating;
    @SerializedName("POISpendTime")
    @Expose
    private String pOISpendTime;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("statename")
    @Expose
    private String statename;
    @SerializedName("TravelTypelList")
    @Expose
    private List<TravelTypelList> travelTypelList = new ArrayList<TravelTypelList>();
    @SerializedName("ImageList")
    @Expose
    private List<ImageList> imageList = new ArrayList<ImageList>();
    @SerializedName("CategoryName")
    @Expose
    private Object categoryName;
    @SerializedName("PoiUmId")
    @Expose
    private Object poiUmId;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private Object resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("resHttpStatus")
    @Expose
    private Object resHttpStatus;
    @SerializedName("resStatusDescription")
    @Expose
    private Object resStatusDescription;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("Search")
    @Expose
    private Object search;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("CurrentLat")
    @Expose
    private Object currentLat;
    @SerializedName("CurrentLong")
    @Expose
    private Object currentLong;

    public boolean click_status;

    public boolean isClick_status() {
        return click_status;
    }

    public void setClick_status(boolean click_status) {
        this.click_status = click_status;
    }


    /**
     * @return The rowNum
     */
    public Integer getRowNum() {
        return rowNum;
    }

    /**
     * @param rowNum The RowNum
     */
    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    /**
     * @return The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     * @return The duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * @return The favId
     */
    public Integer getFavId() {
        if(favId==null)
        {
            return 0;
        }else {
            return favId;
        }
    }

    /**
     * @param favId The FavId
     */
    public void setFavId(Integer favId) {
        this.favId = favId;
    }

    /**
     * @return The desName
     */
    public String getDesName() {
        return desName;
    }

    /**
     * @param desName The desName
     */
    public void setDesName(String desName) {
        this.desName = desName;
    }

    /**
     * @return The pOIID
     */
    public Integer getPOIID() {
        return pOIID;
    }

    /**
     * @param pOIID The POIID
     */
    public void setPOIID(Integer pOIID) {
        this.pOIID = pOIID;
    }

    /**
     * @return The pOICatId
     */
    public Integer getPOICatId() {
        return pOICatId;
    }

    /**
     * @param pOICatId The POICatId
     */
    public void setPOICatId(Integer pOICatId) {
        this.pOICatId = pOICatId;
    }

    /**
     * @return The pOITypeId
     */
    public String getPOITypeId() {
        return pOITypeId;
    }

    /**
     * @param pOITypeId The POITypeId
     */
    public void setPOITypeId(String pOITypeId) {
        this.pOITypeId = pOITypeId;
    }

    /**
     * @return The pOIImage
     */
    public String getPOIImage() {
        return pOIImage;
    }

    /**
     * @param pOIImage The POIImage
     */
    public void setPOIImage(String pOIImage) {
        this.pOIImage = pOIImage;
    }

    /**
     * @return The pOINearbyDestination
     */
    public Integer getPOINearbyDestination() {
        return pOINearbyDestination;
    }

    /**
     * @param pOINearbyDestination The POINearbyDestination
     */
    public void setPOINearbyDestination(Integer pOINearbyDestination) {
        this.pOINearbyDestination = pOINearbyDestination;
    }

    /**
     * @return The pOINoOfNearByDestination
     */
    public Integer getPOINoOfNearByDestination() {
        return pOINoOfNearByDestination;
    }

    /**
     * @param pOINoOfNearByDestination The POINoOfNearByDestination
     */
    public void setPOINoOfNearByDestination(Integer pOINoOfNearByDestination) {
        this.pOINoOfNearByDestination = pOINoOfNearByDestination;
    }

    /**
     * @return The pOIName
     */
    public String getPOIName() {
        return pOIName;
    }

    /**
     * @param pOIName The POIName
     */
    public void setPOIName(String pOIName) {
        this.pOIName = pOIName;
    }

    /**
     * @return The pOIContactPerson
     */
    public String getPOIContactPerson() {
        return pOIContactPerson;
    }

    /**
     * @param pOIContactPerson The POIContactPerson
     */
    public void setPOIContactPerson(String pOIContactPerson) {
        this.pOIContactPerson = pOIContactPerson;
    }

    /**
     * @return The pOICustCareNo
     */
    public String getPOICustCareNo() {
        if(pOICustCareNo==null)
        {
            return  "";
        }else {
            return pOICustCareNo;
        }
    }

    /**
     * @param pOICustCareNo The POICustCareNo
     */
    public void setPOICustCareNo(String pOICustCareNo) {
        this.pOICustCareNo = pOICustCareNo;
    }

    /**
     * @return The pOIEmailId
     */
    public Object getPOIEmailId() {
        return pOIEmailId;
    }

    /**
     * @param pOIEmailId The POIEmailId
     */
    public void setPOIEmailId(Object pOIEmailId) {
        this.pOIEmailId = pOIEmailId;
    }

    /**
     * @return The pOIAddress1
     */
    public String getPOIAddress1() {
        if(pOIAddress1==null)
        {
            return "";
        }else {
            return pOIAddress1;
        }
    }

    /**
     * @param pOIAddress1 The POIAddress1
     */
    public void setPOIAddress1(String pOIAddress1) {
        this.pOIAddress1 = pOIAddress1;
    }

    /**
     * @return The pOIAddress2
     */
    public Object getPOIAddress2() {
        return pOIAddress2;
    }

    /**
     * @param pOIAddress2 The POIAddress2
     */
    public void setPOIAddress2(Object pOIAddress2) {
        this.pOIAddress2 = pOIAddress2;
    }

    /**
     * @return The pOILatitude
     */
    public String getPOILatitude() {
        return pOILatitude;
    }

    /**
     * @param pOILatitude The POILatitude
     */
    public void setPOILatitude(String pOILatitude) {
        this.pOILatitude = pOILatitude;
    }

    /**
     * @return The pOILongitude
     */
    public String getPOILongitude() {
        return pOILongitude;
    }

    /**
     * @param pOILongitude The POILongitude
     */
    public void setPOILongitude(String pOILongitude) {
        this.pOILongitude = pOILongitude;
    }

    /**
     * @return The pOISide
     */
    public Object getPOISide() {
        return pOISide;
    }

    /**
     * @param pOISide The POISide
     */
    public void setPOISide(Object pOISide) {
        this.pOISide = pOISide;
    }

    /**
     * @return The pOIState
     */
    public Integer getPOIState() {
        return pOIState;
    }

    /**
     * @param pOIState The POIState
     */
    public void setPOIState(Integer pOIState) {
        this.pOIState = pOIState;
    }

    /**
     * @return The pOICity
     */
    public Integer getPOICity() {
        return pOICity;
    }

    /**
     * @param pOICity The POICity
     */
    public void setPOICity(Integer pOICity) {
        this.pOICity = pOICity;
    }

    /**
     * @return The pOIZipCode
     */
    public String getPOIZipCode() {
        return pOIZipCode;
    }

    /**
     * @param pOIZipCode The POIZipCode
     */
    public void setPOIZipCode(String pOIZipCode) {
        this.pOIZipCode = pOIZipCode;
    }

    /**
     * @return The pOITerritory
     */
    public Object getPOITerritory() {
        return pOITerritory;
    }

    /**
     * @param pOITerritory The POITerritory
     */
    public void setPOITerritory(Object pOITerritory) {
        this.pOITerritory = pOITerritory;
    }

    /**
     * @return The pOIIsActive
     */
    public Boolean getPOIIsActive() {
        return pOIIsActive;
    }

    /**
     * @param pOIIsActive The POIIsActive
     */
    public void setPOIIsActive(Boolean pOIIsActive) {
        this.pOIIsActive = pOIIsActive;
    }

    /**
     * @return The pOICreateDate
     */
    public String getPOICreateDate() {
        return pOICreateDate;
    }

    /**
     * @param pOICreateDate The POICreateDate
     */
    public void setPOICreateDate(String pOICreateDate) {
        this.pOICreateDate = pOICreateDate;
    }

    /**
     * @return The pOIUpdateDate
     */
    public String getPOIUpdateDate() {
        return pOIUpdateDate;
    }

    /**
     * @param pOIUpdateDate The POIUpdateDate
     */
    public void setPOIUpdateDate(String pOIUpdateDate) {
        this.pOIUpdateDate = pOIUpdateDate;
    }

    /**
     * @return The pOIVicinity
     */
    public Object getPOIVicinity() {
        return pOIVicinity;
    }

    /**
     * @param pOIVicinity The POIVicinity
     */
    public void setPOIVicinity(Object pOIVicinity) {
        this.pOIVicinity = pOIVicinity;
    }

    /**
     * @return The pOIGoogleResponse
     */
    public Object getPOIGoogleResponse() {
        return pOIGoogleResponse;
    }

    /**
     * @param pOIGoogleResponse The POIGoogleResponse
     */
    public void setPOIGoogleResponse(Object pOIGoogleResponse) {
        this.pOIGoogleResponse = pOIGoogleResponse;
    }

    /**
     * @return The pOIHostDetails
     */
    public String getPOIHostDetails() {
        return pOIHostDetails;
    }

    /**
     * @param pOIHostDetails The POIHostDetails
     */
    public void setPOIHostDetails(String pOIHostDetails) {
        this.pOIHostDetails = pOIHostDetails;
    }

    /**
     * @return The pOIAdminId
     */
    public Integer getPOIAdminId() {
        return pOIAdminId;
    }

    /**
     * @param pOIAdminId The POIAdminId
     */
    public void setPOIAdminId(Integer pOIAdminId) {
        this.pOIAdminId = pOIAdminId;
    }

    /**
     * @return The pOIGooglePlaceId
     */
    public Object getPOIGooglePlaceId() {
        return pOIGooglePlaceId;
    }

    /**
     * @param pOIGooglePlaceId The POIGooglePlaceId
     */
    public void setPOIGooglePlaceId(Object pOIGooglePlaceId) {
        this.pOIGooglePlaceId = pOIGooglePlaceId;
    }

    /**
     * @return The pOIGoogleRating
     */
    public Object getPOIGoogleRating() {
        return pOIGoogleRating;
    }

    /**
     * @param pOIGoogleRating The POIGoogleRating
     */
    public void setPOIGoogleRating(Object pOIGoogleRating) {
        this.pOIGoogleRating = pOIGoogleRating;
    }

    /**
     * @return The pOIIcon
     */
    public Object getPOIIcon() {
        return pOIIcon;
    }

    /**
     * @param pOIIcon The POIIcon
     */
    public void setPOIIcon(Object pOIIcon) {
        this.pOIIcon = pOIIcon;
    }

    /**
     * @return The pOIIsFullTime
     */
    public Boolean getPOIIsFullTime() {
        return pOIIsFullTime;
    }

    /**
     * @param pOIIsFullTime The POIIsFullTime
     */
    public void setPOIIsFullTime(Boolean pOIIsFullTime) {
        this.pOIIsFullTime = pOIIsFullTime;
    }

    /**
     * @return The pOIStartTime
     */
    public String getPOIStartTime() {
        return pOIStartTime;
    }

    /**
     * @param pOIStartTime The POIStartTime
     */
    public void setPOIStartTime(String pOIStartTime) {
        this.pOIStartTime = pOIStartTime;
    }

    /**
     * @return The pOIEndTime
     */
    public String getPOIEndTime() {
        return pOIEndTime;
    }

    /**
     * @param pOIEndTime The POIEndTime
     */
    public void setPOIEndTime(String pOIEndTime) {
        this.pOIEndTime = pOIEndTime;
    }

    /**
     * @return The pOIBestTimeFrom
     */
    public String getPOIBestTimeFrom() {
        return pOIBestTimeFrom;
    }

    /**
     * @param pOIBestTimeFrom The POIBestTimeFrom
     */
    public void setPOIBestTimeFrom(String pOIBestTimeFrom) {
        this.pOIBestTimeFrom = pOIBestTimeFrom;
    }

    /**
     * @return The pOIBestTimeTo
     */
    public String getPOIBestTimeTo() {
        return pOIBestTimeTo;
    }

    /**
     * @param pOIBestTimeTo The POIBestTimeTo
     */
    public void setPOIBestTimeTo(String pOIBestTimeTo) {
        this.pOIBestTimeTo = pOIBestTimeTo;
    }

    /**
     * @return The pOIShortDescription
     */
    public String getPOIShortDescription() {
        if(pOIShortDescription==null)
        {
            return "";
        }else {
            return pOIShortDescription;
        }
    }

    /**
     * @param pOIShortDescription The POIShortDescription
     */
    public void setPOIShortDescription(String pOIShortDescription) {
        this.pOIShortDescription = pOIShortDescription;
    }

    /**
     * @return The pOILongDescription
     */
    public String getPOILongDescription() {
        pOILongDescription.replace ("\\r\\n", "<br>").replace ("\\n", "<br>");

        if(pOILongDescription==null)
        {
            return "";

        }else
        {
            return pOILongDescription;
        }

    }

    /**
     * @param pOILongDescription The POILongDescription
     */
    public void setPOILongDescription(String pOILongDescription) {
        this.pOILongDescription = pOILongDescription;
    }

    /**
     * @return The pOINoofVisited
     */
    public Integer getPOINoofVisited() {
        return pOINoofVisited;
    }

    /**
     * @param pOINoofVisited The POINoofVisited
     */
    public void setPOINoofVisited(Integer pOINoofVisited) {
        this.pOINoofVisited = pOINoofVisited;
    }

    /**
     * @return The pOIRating
     */
    public String getPOIRating() {
        return pOIRating;
    }

    /**
     * @param pOIRating The POIRating
     */
    public void setPOIRating(String pOIRating) {
        this.pOIRating = pOIRating;
    }

    /**
     * @return The pOISpendTime
     */
    public String getPOISpendTime() {
        return pOISpendTime;
    }

    /**
     * @param pOISpendTime The POISpendTime
     */
    public void setPOISpendTime(String pOISpendTime) {
        this.pOISpendTime = pOISpendTime;
    }

    /**
     * @return The cityname
     */
    public String getCityname() {
        if(cityname==null)
        {
            return "";
        }else {
            return cityname;
        }
    }

    /**
     * @param cityname The cityname
     */
    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    /**
     * @return The statename
     */
    public String getStatename() {
        if(statename==null)
        {
            return "";
        }else {
            return statename;
        }
    }

    /**
     * @param statename The statename
     */
    public void setStatename(String statename) {
        this.statename = statename;
    }

    /**
     * @return The travelTypelList
     */
    public List<TravelTypelList> getTravelTypelList() {
        return travelTypelList;
    }

    /**
     * @param travelTypelList The TravelTypelList
     */
    public void setTravelTypelList(List<TravelTypelList> travelTypelList) {
        this.travelTypelList = travelTypelList;
    }

    /**
     * @return The imageList
     */
    public List<ImageList> getImageList() {
        return imageList;
    }

    /**
     * @param imageList The ImageList
     */
    public void setImageList(List<ImageList> imageList) {
        this.imageList = imageList;
    }

    /**
     * @return The categoryName
     */
    public Object getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName The CategoryName
     */
    public void setCategoryName(Object categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return The poiUmId
     */
    public Object getPoiUmId() {
        return poiUmId;
    }

    /**
     * @param poiUmId The PoiUmId
     */
    public void setPoiUmId(Object poiUmId) {
        this.poiUmId = poiUmId;
    }

    /**
     * @return The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     * @param resStatus The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     * @return The resDescription
     */
    public Object getResDescription() {
        return resDescription;
    }

    /**
     * @param resDescription The resDescription
     */
    public void setResDescription(Object resDescription) {
        this.resDescription = resDescription;
    }

    /**
     * @return The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     * @param resCallerDetails The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     * @return The resHttpStatus
     */
    public Object getResHttpStatus() {
        return resHttpStatus;
    }

    /**
     * @param resHttpStatus The resHttpStatus
     */
    public void setResHttpStatus(Object resHttpStatus) {
        this.resHttpStatus = resHttpStatus;
    }

    /**
     * @return The resStatusDescription
     */
    public Object getResStatusDescription() {
        return resStatusDescription;
    }

    /**
     * @param resStatusDescription The resStatusDescription
     */
    public void setResStatusDescription(Object resStatusDescription) {
        this.resStatusDescription = resStatusDescription;
    }

    /**
     * @return The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * @return The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return The search
     */
    public Object getSearch() {
        return search;
    }

    /**
     * @param search The Search
     */
    public void setSearch(Object search) {
        this.search = search;
    }

    /**
     * @return The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     * @param radius The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    /**
     * @return The currentLat
     */
    public Object getCurrentLat() {
        return currentLat;
    }

    /**
     * @param currentLat The CurrentLat
     */
    public void setCurrentLat(Object currentLat) {
        this.currentLat = currentLat;
    }

    /**
     * @return The currentLong
     */
    public Object getCurrentLong() {
        return currentLong;
    }

    /**
     * @param currentLong The CurrentLong
     */
    public void setCurrentLong(Object currentLong) {
        this.currentLong = currentLong;
    }



    /*@Generated("org.jsonschema2pojo")
    public class ImageList {

        @SerializedName("TotalImage")
        @Expose
        private Integer totalImage;
        @SerializedName("ImgId")
        @Expose
        private Integer imgId;
        @SerializedName("ImgType")
        @Expose
        private String imgType;
        @SerializedName("ImgName")
        @Expose
        private String imgName;
        @SerializedName("ImgCreatedDate")
        @Expose
        private String imgCreatedDate;
        @SerializedName("ImgRefId")
        @Expose
        private Integer imgRefId;
        @SerializedName("ImgDescription")
        @Expose
        private String imgDescription;
        @SerializedName("ImgIsActive")
        @Expose
        private Boolean imgIsActive;
        @SerializedName("ImgUpdateDate")
        @Expose
        private Object imgUpdateDate;

        @SerializedName("ImgCredits")
        @Expose
        private String ImgCredits;

        @SerializedName("ImgPhotoLink")
        @Expose
        private String ImgPhotoLink;

        @SerializedName("ImgLicenseCCBY")
        @Expose
        private String ImgLicenseCCBY;

        public String getImgLicenseCCSA() {
            return ImgLicenseCCSA;
        }

        public void setImgLicenseCCSA(String imgLicenseCCSA) {
            ImgLicenseCCSA = imgLicenseCCSA;
        }

        public String getImgCredits() {
            if(ImgCredits==null)
            {
                return "";
            }else
            {
                return ImgCredits;

            }
        }

        public void setImgCredits(String imgCredits) {
            ImgCredits = imgCredits;
        }

        public String getImgPhotoLink() {
            return ImgPhotoLink;
        }

        public void setImgPhotoLink(String imgPhotoLink) {
            ImgPhotoLink = imgPhotoLink;
        }

        public String getImgLicenseCCBY() {
            return ImgLicenseCCBY;
        }

        public void setImgLicenseCCBY(String imgLicenseCCBY) {
            ImgLicenseCCBY = imgLicenseCCBY;
        }

        @SerializedName("ImgLicenseCCSA")
        @Expose
        private String ImgLicenseCCSA;

        *//**
         * @return The totalImage
         *//*
        public Integer getTotalImage() {
            return totalImage;
        }

        *//**
         * @param totalImage The TotalImage
         *//*
        public void setTotalImage(Integer totalImage) {
            this.totalImage = totalImage;
        }

        *//**
         * @return The imgId
         *//*
        public Integer getImgId() {
            return imgId;
        }

        *//**
         * @param imgId The ImgId
         *//*
        public void setImgId(Integer imgId) {
            this.imgId = imgId;
        }

        *//**
         * @return The imgType
         *//*
        public String getImgType() {
            return imgType;
        }

        *//**
         * @param imgType The ImgType
         *//*
        public void setImgType(String imgType) {
            this.imgType = imgType;
        }

        *//**
         * @return The imgName
         *//*
        public String getImgName() {
            return imgName;
        }

        *//**
         * @param imgName The ImgName
         *//*
        public void setImgName(String imgName) {
            this.imgName = imgName;
        }

        *//**
         * @return The imgCreatedDate
         *//*
        public String getImgCreatedDate() {
            return imgCreatedDate;
        }

        *//**
         * @param imgCreatedDate The ImgCreatedDate
         *//*
        public void setImgCreatedDate(String imgCreatedDate) {
            this.imgCreatedDate = imgCreatedDate;
        }

        *//**
         * @return The imgRefId
         *//*
        public Integer getImgRefId() {
            return imgRefId;
        }

        *//**
         * @param imgRefId The ImgRefId
         *//*
        public void setImgRefId(Integer imgRefId) {
            this.imgRefId = imgRefId;
        }

        *//**
         * @return The imgDescription
         *//*
        public String getImgDescription() {
            return imgDescription;
        }

        *//**
         * @param imgDescription The ImgDescription
         *//*
        public void setImgDescription(String imgDescription) {
            this.imgDescription = imgDescription;
        }

        *//**
         * @return The imgIsActive
         *//*
        public Boolean getImgIsActive() {
            return imgIsActive;
        }

        *//**
         * @param imgIsActive The ImgIsActive
         *//*
        public void setImgIsActive(Boolean imgIsActive) {
            this.imgIsActive = imgIsActive;
        }

        *//**
         * @return The imgUpdateDate
         *//*
        public Object getImgUpdateDate() {
            return imgUpdateDate;
        }

        *//**
         * @param imgUpdateDate The ImgUpdateDate
         *//*
        public void setImgUpdateDate(Object imgUpdateDate) {
            this.imgUpdateDate = imgUpdateDate;
        }

    }*/

    @Generated("org.jsonschema2pojo")
    public class TravelTypelList {

        @SerializedName("TTypeId")
        @Expose
        private Integer tTypeId;
        @SerializedName("TTypeName")
        @Expose
        private String tTypeName;
        @SerializedName("TTypeDescription")
        @Expose
        private String tTypeDescription;
        @SerializedName("TTypeImage")
        @Expose
        private String tTypeImage;
        @SerializedName("TTypeCreateDate")
        @Expose
        private String tTypeCreateDate;
        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;
        @SerializedName("TTypeUpdateDate")
        @Expose
        private Object tTypeUpdateDate;
        @SerializedName("TTypeIPAddress")
        @Expose
        private Object tTypeIPAddress;
        @SerializedName("TTypeAdminUserId")
        @Expose
        private Object tTypeAdminUserId;
        @SerializedName("TTSequenceId")
        @Expose
        private Integer tTSequenceId;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private Object name;
        @SerializedName("ImageMaster")
        @Expose
        private Object imageMaster;
        @SerializedName("BannerImage")
        @Expose
        private Object bannerImage;
        @SerializedName("resStatus")
        @Expose
        private Boolean resStatus;
        @SerializedName("resDescription")
        @Expose
        private Object resDescription;
        @SerializedName("resCallerDetails")
        @Expose
        private String resCallerDetails;
        @SerializedName("resHttpStatus")
        @Expose
        private Object resHttpStatus;
        @SerializedName("resStatusDescription")
        @Expose
        private Object resStatusDescription;
        @SerializedName("PageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("PageSize")
        @Expose
        private Integer pageSize;
        @SerializedName("Search")
        @Expose
        private Object search;
        @SerializedName("radius")
        @Expose
        private Integer radius;
        @SerializedName("CurrentLat")
        @Expose
        private Object currentLat;
        @SerializedName("CurrentLong")
        @Expose
        private Object currentLong;

        /**
         * @return The tTypeId
         */
        public Integer getTTypeId() {
            return tTypeId;
        }

        /**
         * @param tTypeId The TTypeId
         */
        public void setTTypeId(Integer tTypeId) {
            this.tTypeId = tTypeId;
        }

        /**
         * @return The tTypeName
         */
        public String getTTypeName() {
            return tTypeName;
        }

        /**
         * @param tTypeName The TTypeName
         */
        public void setTTypeName(String tTypeName) {
            this.tTypeName = tTypeName;
        }

        /**
         * @return The tTypeDescription
         */
        public String getTTypeDescription() {
            return tTypeDescription;
        }

        /**
         * @param tTypeDescription The TTypeDescription
         */
        public void setTTypeDescription(String tTypeDescription) {
            this.tTypeDescription = tTypeDescription;
        }

        /**
         * @return The tTypeImage
         */
        public String getTTypeImage() {
            return tTypeImage;
        }

        /**
         * @param tTypeImage The TTypeImage
         */
        public void setTTypeImage(String tTypeImage) {
            this.tTypeImage = tTypeImage;
        }

        /**
         * @return The tTypeCreateDate
         */
        public String getTTypeCreateDate() {
            return tTypeCreateDate;
        }

        /**
         * @param tTypeCreateDate The TTypeCreateDate
         */
        public void setTTypeCreateDate(String tTypeCreateDate) {
            this.tTypeCreateDate = tTypeCreateDate;
        }

        /**
         * @return The isActive
         */
        public Boolean getIsActive() {
            return isActive;
        }

        /**
         * @param isActive The IsActive
         */
        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        /**
         * @return The tTypeUpdateDate
         */
        public Object getTTypeUpdateDate() {
            return tTypeUpdateDate;
        }

        /**
         * @param tTypeUpdateDate The TTypeUpdateDate
         */
        public void setTTypeUpdateDate(Object tTypeUpdateDate) {
            this.tTypeUpdateDate = tTypeUpdateDate;
        }

        /**
         * @return The tTypeIPAddress
         */
        public Object getTTypeIPAddress() {
            return tTypeIPAddress;
        }

        /**
         * @param tTypeIPAddress The TTypeIPAddress
         */
        public void setTTypeIPAddress(Object tTypeIPAddress) {
            this.tTypeIPAddress = tTypeIPAddress;
        }

        /**
         * @return The tTypeAdminUserId
         */
        public Object getTTypeAdminUserId() {
            return tTypeAdminUserId;
        }

        /**
         * @param tTypeAdminUserId The TTypeAdminUserId
         */
        public void setTTypeAdminUserId(Object tTypeAdminUserId) {
            this.tTypeAdminUserId = tTypeAdminUserId;
        }

        /**
         * @return The tTSequenceId
         */
        public Integer getTTSequenceId() {
            return tTSequenceId;
        }

        /**
         * @param tTSequenceId The TTSequenceId
         */
        public void setTTSequenceId(Integer tTSequenceId) {
            this.tTSequenceId = tTSequenceId;
        }

        /**
         * @return The id
         */
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        public Object getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(Object name) {
            this.name = name;
        }

        /**
         * @return The imageMaster
         */
        public Object getImageMaster() {
            return imageMaster;
        }

        /**
         * @param imageMaster The ImageMaster
         */
        public void setImageMaster(Object imageMaster) {
            this.imageMaster = imageMaster;
        }

        /**
         * @return The bannerImage
         */
        public Object getBannerImage() {
            return bannerImage;
        }

        /**
         * @param bannerImage The BannerImage
         */
        public void setBannerImage(Object bannerImage) {
            this.bannerImage = bannerImage;
        }

        /**
         * @return The resStatus
         */
        public Boolean getResStatus() {
            return resStatus;
        }

        /**
         * @param resStatus The resStatus
         */
        public void setResStatus(Boolean resStatus) {
            this.resStatus = resStatus;
        }

        /**
         * @return The resDescription
         */
        public Object getResDescription() {
            return resDescription;
        }

        /**
         * @param resDescription The resDescription
         */
        public void setResDescription(Object resDescription) {
            this.resDescription = resDescription;
        }

        /**
         * @return The resCallerDetails
         */
        public String getResCallerDetails() {
            return resCallerDetails;
        }

        /**
         * @param resCallerDetails The resCallerDetails
         */
        public void setResCallerDetails(String resCallerDetails) {
            this.resCallerDetails = resCallerDetails;
        }

        /**
         * @return The resHttpStatus
         */
        public Object getResHttpStatus() {
            return resHttpStatus;
        }

        /**
         * @param resHttpStatus The resHttpStatus
         */
        public void setResHttpStatus(Object resHttpStatus) {
            this.resHttpStatus = resHttpStatus;
        }

        /**
         * @return The resStatusDescription
         */
        public Object getResStatusDescription() {
            return resStatusDescription;
        }

        /**
         * @param resStatusDescription The resStatusDescription
         */
        public void setResStatusDescription(Object resStatusDescription) {
            this.resStatusDescription = resStatusDescription;
        }

        /**
         * @return The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The PageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The pageSize
         */
        public Integer getPageSize() {
            return pageSize;
        }

        /**
         * @param pageSize The PageSize
         */
        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         * @return The search
         */
        public Object getSearch() {
            return search;
        }

        /**
         * @param search The Search
         */
        public void setSearch(Object search) {
            this.search = search;
        }

        /**
         * @return The radius
         */
        public Integer getRadius() {
            return radius;
        }

        /**
         * @param radius The radius
         */
        public void setRadius(Integer radius) {
            this.radius = radius;
        }

        /**
         * @return The currentLat
         */
        public Object getCurrentLat() {
            return currentLat;
        }

        /**
         * @param currentLat The CurrentLat
         */
        public void setCurrentLat(Object currentLat) {
            this.currentLat = currentLat;
        }

        /**
         * @return The currentLong
         */
        public Object getCurrentLong() {
            return currentLong;
        }

        /**
         * @param currentLong The CurrentLong
         */
        public void setCurrentLong(Object currentLong) {
            this.currentLong = currentLong;
        }

    }
}