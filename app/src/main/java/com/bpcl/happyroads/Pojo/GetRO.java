

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class GetRO {

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("duration")
    @Expose
    private Double duration;
    @SerializedName("ROMasterId")
    @Expose
    private Integer rOMasterId;
    @SerializedName("ROName")
    @Expose
    private String rOName;
    @SerializedName("ROManagerName")
    @Expose
    private String ROManagerName;

    public Boolean getRoHasAdditionalService() {
        return IsRoHasAdditionalService;
    }

    public void setRoHasAdditionalService(Boolean roHasAdditionalService) {
        IsRoHasAdditionalService = roHasAdditionalService;
    }

    @SerializedName("IsRoHasAdditionalService")
    @Expose
    private Boolean IsRoHasAdditionalService;
    public List<ToiletTypesAll> getToiletTypesAllArrayList() {
        return toiletTypesAllArrayList;
    }

    public void setToiletTypesAllArrayList(List<ToiletTypesAll> toiletTypesAllArrayList) {
        this.toiletTypesAllArrayList = toiletTypesAllArrayList;
    }

    @SerializedName("GetToiletTypesLists")

    @Expose
    private List<ToiletTypesAll> toiletTypesAllArrayList = new ArrayList<ToiletTypesAll>();

    public String getROManagerEmailId() {
        if(ROManagerEmailId==null)
        {
            return "";

        }else
        {
            return ROManagerEmailId;

        }
    }

    public void setROManagerEmailId(String ROManagerEmailId) {
        this.ROManagerEmailId = ROManagerEmailId;
    }

    public String getROManagerName() {
        if(ROManagerName==null)
        {
            return "";
        }
        else {
            return ROManagerName;
        }
    }

    public void setROManagerName(String ROManagerName) {
        this.ROManagerName = ROManagerName;
    }

    @SerializedName("ROManagerEmailId")
    @Expose

    private String ROManagerEmailId;
    @SerializedName("ROCCNo")
    @Expose
    private String rOCCNo;
    @SerializedName("ROManagerContactNo")
    @Expose
    private String rOManagerContactNo;
    @SerializedName("ROLocationName")
    @Expose
    private String rOLocationName;
    @SerializedName("ROLatitude")
    @Expose
    private String rOLatitude;
    @SerializedName("ROLongitude")
    @Expose
    private String rOLongitude;
    @SerializedName("ROStateID")
    @Expose
    private Integer rOStateID;
    @SerializedName("ROCityID")
    @Expose
    private Integer rOCityID;
    @SerializedName("ROBusHours")
    @Expose
    private Boolean rOBusHours;
    @SerializedName("ROFrom")
    @Expose
    private String rOFrom;
    @SerializedName("ROTo")
    @Expose
    private String rOTo;
    @SerializedName("ROCatId")
    @Expose
    private Integer rOCatId;
    @SerializedName("ROType")
    @Expose
    private Integer rOType;
    @SerializedName("ROServiceOffered")
    @Expose
    private String rOServiceOffered;
    @SerializedName("ROToilet")
    @Expose
    private String rOToilet;
    @SerializedName("ROFuels")
    @Expose
    private String rOFuels;
    @SerializedName("ROFuelsType")
    @Expose
    private String rOFuelsType;
    @SerializedName("ROHostDetails")
    @Expose
    private String rOHostDetails;
    @SerializedName("ROAdminId")
    @Expose
    private Integer rOAdminId;
    @SerializedName("ROIsActive")
    @Expose
    private Boolean rOIsActive;
    @SerializedName("ROCreatedDate")
    @Expose
    private String rOCreatedDate;
    @SerializedName("ROHighwayType")
    @Expose
    private Integer rOHighwayType;
    @SerializedName("ROSpendTime")
    @Expose
    private String rOSpendTime;
    @SerializedName("ROSide")
    @Expose
    private String rOSide;
    @SerializedName("CityName")
    @Expose
    private String cityName;
    @SerializedName("StateName")
    @Expose
    private String stateName;
    @SerializedName("ROHighwayTypeAll")
    @Expose
    private List<ROHighwayTypeAll> rOHighwayTypeAll = new ArrayList<ROHighwayTypeAll>();
    @SerializedName("ROAdditionalService")
    @Expose
    private List<ROAdditionalService> rOAdditionalService = new ArrayList<ROAdditionalService>();
    @SerializedName("retailOutletTypes")
    @Expose
    private List<RetailOutletType> retailOutletTypes = new ArrayList<RetailOutletType>();
    @SerializedName("ROCategoriesLists")
    @Expose
    private List<ROCategoriesList> rOCategoriesLists = new ArrayList<ROCategoriesList>();
    @SerializedName("GetRoBasicServiceDetails")
    @Expose
    private List<GetRoBasicServiceDetail> getRoBasicServiceDetails = new ArrayList<GetRoBasicServiceDetail>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    public boolean click_status;
    public boolean isClick_status() {
        return click_status;
    }

    public void setClick_status(boolean click_status) {
        this.click_status = click_status;
    }
    /**
     *
     * @return
     * The rowNum
     */
    public Integer getRowNum() {
        return rowNum;
    }

    /**
     *
     * @param rowNum
     * The RowNum
     */
    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    /**
     *
     * @return
     * The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The duration
     */
    public Double getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(Double duration) {
        this.duration = duration;
    }

    /**
     *
     * @return
     * The rOMasterId
     */
    public Integer getROMasterId() {
        return rOMasterId;
    }

    /**
     *
     * @param rOMasterId
     * The ROMasterId
     */
    public void setROMasterId(Integer rOMasterId) {
        this.rOMasterId = rOMasterId;
    }

    /**
     *
     * @return
     * The rOName
     */
    public String getROName() {
        return rOName;
    }

    /**
     *
     * @param rOName
     * The ROName
     */
    public void setROName(String rOName) {
        this.rOName = rOName;
    }

    /**
     *
     * @return
     * The rOCCNo
     */
    public String getROCCNo() {
        return rOCCNo;
    }

    /**
     *
     * @param rOCCNo
     * The ROCCNo
     */
    public void setROCCNo(String rOCCNo) {
        this.rOCCNo = rOCCNo;
    }

    /**
     *
     * @return
     * The rOManagerContactNo
     */
    public String getROManagerContactNo() {
        if(rOManagerContactNo==null)
        {
            return  "";
        }else {
            return rOManagerContactNo;
        }
    }

    /**
     *
     * @param rOManagerContactNo
     * The ROManagerContactNo
     */
    public void setROManagerContactNo(String rOManagerContactNo) {
        this.rOManagerContactNo = rOManagerContactNo;
    }

    /**
     *
     * @return
     * The rOLocationName
     */
    public String getROLocationName() {
        return rOLocationName;
    }

    /**
     *
     * @param rOLocationName
     * The ROLocationName
     */
    public void setROLocationName(String rOLocationName) {
        this.rOLocationName = rOLocationName;
    }

    /**
     *
     * @return
     * The rOLatitude
     */
    public String getROLatitude() {
        return rOLatitude;
    }

    /**
     *
     * @param rOLatitude
     * The ROLatitude
     */
    public void setROLatitude(String rOLatitude) {
        this.rOLatitude = rOLatitude;
    }

    /**
     *
     * @return
     * The rOLongitude
     */
    public String getROLongitude() {
        return rOLongitude;
    }

    /**
     *
     * @param rOLongitude
     * The ROLongitude
     */
    public void setROLongitude(String rOLongitude) {
        this.rOLongitude = rOLongitude;
    }

    /**
     *
     * @return
     * The rOStateID
     */
    public Integer getROStateID() {
        return rOStateID;
    }

    /**
     *
     * @param rOStateID
     * The ROStateID
     */
    public void setROStateID(Integer rOStateID) {
        this.rOStateID = rOStateID;
    }

    /**
     *
     * @return
     * The rOCityID
     */
    public Integer getROCityID() {
        return rOCityID;
    }

    /**
     *
     * @param rOCityID
     * The ROCityID
     */
    public void setROCityID(Integer rOCityID) {
        this.rOCityID = rOCityID;
    }

    /**
     *
     * @return
     * The rOBusHours
     */
    public Boolean getROBusHours() {
        return rOBusHours;
    }

    /**
     *
     * @param rOBusHours
     * The ROBusHours
     */
    public void setROBusHours(Boolean rOBusHours) {
        this.rOBusHours = rOBusHours;
    }

    /**
     *
     * @return
     * The rOFrom
     */
    public String getROFrom() {
        return rOFrom;
    }

    /**
     *
     * @param rOFrom
     * The ROFrom
     */
    public void setROFrom(String rOFrom) {
        this.rOFrom = rOFrom;
    }

    /**
     *
     * @return
     * The rOTo
     */
    public String getROTo() {
        return rOTo;
    }

    /**
     *
     * @param rOTo
     * The ROTo
     */
    public void setROTo(String rOTo) {
        this.rOTo = rOTo;
    }

    /**
     *
     * @return
     * The rOCatId
     */
    public Integer getROCatId() {
        return rOCatId;
    }

    /**
     *
     * @param rOCatId
     * The ROCatId
     */
    public void setROCatId(Integer rOCatId) {
        this.rOCatId = rOCatId;
    }

    /**
     *
     * @return
     * The rOType
     */
    public Integer getROType() {
        return rOType;
    }

    /**
     *
     * @param rOType
     * The ROType
     */
    public void setROType(Integer rOType) {
        this.rOType = rOType;
    }

    /**
     *
     * @return
     * The rOServiceOffered
     */
    public String getROServiceOffered() {
        return rOServiceOffered;
    }

    /**
     *
     * @param rOServiceOffered
     * The ROServiceOffered
     */
    public void setROServiceOffered(String rOServiceOffered) {
        this.rOServiceOffered = rOServiceOffered;
    }

    /**
     *
     * @return
     * The rOToilet
     */
    public String getROToilet() {
        return rOToilet;
    }

    /**
     *
     * @param rOToilet
     * The ROToilet
     */
    public void setROToilet(String rOToilet) {
        this.rOToilet = rOToilet;
    }

    /**
     *
     * @return
     * The rOFuels
     */
    public String getROFuels() {
        return rOFuels;
    }

    /**
     *
     * @param rOFuels
     * The ROFuels
     */
    public void setROFuels(String rOFuels) {
        this.rOFuels = rOFuels;
    }

    /**
     *
     * @return
     * The rOFuelsType
     */
    public String getROFuelsType() {
        return rOFuelsType;
    }

    /**
     *
     * @param rOFuelsType
     * The ROFuelsType
     */
    public void setROFuelsType(String rOFuelsType) {
        this.rOFuelsType = rOFuelsType;
    }

    /**
     *
     * @return
     * The rOHostDetails
     */
    public String getROHostDetails() {
        return rOHostDetails;
    }

    /**
     *
     * @param rOHostDetails
     * The ROHostDetails
     */
    public void setROHostDetails(String rOHostDetails) {
        this.rOHostDetails = rOHostDetails;
    }

    /**
     *
     * @return
     * The rOAdminId
     */
    public Integer getROAdminId() {
        return rOAdminId;
    }

    /**
     *
     * @param rOAdminId
     * The ROAdminId
     */
    public void setROAdminId(Integer rOAdminId) {
        this.rOAdminId = rOAdminId;
    }

    /**
     *
     * @return
     * The rOIsActive
     */
    public Boolean getROIsActive() {
        return rOIsActive;
    }

    /**
     *
     * @param rOIsActive
     * The ROIsActive
     */
    public void setROIsActive(Boolean rOIsActive) {
        this.rOIsActive = rOIsActive;
    }

    /**
     *
     * @return
     * The rOCreatedDate
     */
    public String getROCreatedDate() {
        return rOCreatedDate;
    }

    /**
     *
     * @param rOCreatedDate
     * The ROCreatedDate
     */
    public void setROCreatedDate(String rOCreatedDate) {
        this.rOCreatedDate = rOCreatedDate;
    }

    /**
     *
     * @return
     * The rOHighwayType
     */
    public Integer getROHighwayType() {
        return rOHighwayType;
    }

    /**
     *
     * @param rOHighwayType
     * The ROHighwayType
     */
    public void setROHighwayType(Integer rOHighwayType) {
        this.rOHighwayType = rOHighwayType;
    }

    /**
     *
     * @return
     * The rOSpendTime
     */
    public String getROSpendTime() {
        return rOSpendTime;
    }

    /**
     *
     * @param rOSpendTime
     * The ROSpendTime
     */
    public void setROSpendTime(String rOSpendTime) {
        this.rOSpendTime = rOSpendTime;
    }

    /**
     *
     * @return
     * The rOSide
     */
    public String getROSide() {
        return rOSide;
    }

    /**
     *
     * @param rOSide
     * The ROSide
     */
    public void setROSide(String rOSide) {
        this.rOSide = rOSide;
    }

    /**
     *
     * @return
     * The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     *
     * @param cityName
     * The CityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     *
     * @return
     * The stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     *
     * @param stateName
     * The StateName
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     *
     * @return
     * The rOHighwayTypeAll
     */
    public List<ROHighwayTypeAll> getROHighwayTypeAll() {
        return rOHighwayTypeAll;
    }

    /**
     *
     * @param rOHighwayTypeAll
     * The ROHighwayTypeAll
     */
    public void setROHighwayTypeAll(List<ROHighwayTypeAll> rOHighwayTypeAll) {
        this.rOHighwayTypeAll = rOHighwayTypeAll;
    }

    /**
     *
     * @return
     * The rOAdditionalService
     */
    public List<ROAdditionalService> getROAdditionalService() {
        return rOAdditionalService;
    }

    /**
     *
     * @param rOAdditionalService
     * The ROAdditionalService
     */
    public void setROAdditionalService(List<ROAdditionalService> rOAdditionalService) {
        this.rOAdditionalService = rOAdditionalService;
    }

    /**
     *
     * @return
     * The retailOutletTypes
     */
    public List<RetailOutletType> getRetailOutletTypes() {
        return retailOutletTypes;
    }

    /**
     *
     * @param retailOutletTypes
     * The retailOutletTypes
     */
    public void setRetailOutletTypes(List<RetailOutletType> retailOutletTypes) {
        this.retailOutletTypes = retailOutletTypes;
    }

    /**
     *
     * @return
     * The rOCategoriesLists
     */
    public List<ROCategoriesList> getROCategoriesLists() {
        return rOCategoriesLists;
    }

    /**
     *
     * @param rOCategoriesLists
     * The ROCategoriesLists
     */
    public void setROCategoriesLists(List<ROCategoriesList> rOCategoriesLists) {
        this.rOCategoriesLists = rOCategoriesLists;
    }

    /**
     *
     * @return
     * The getRoBasicServiceDetails
     */
    public List<GetRoBasicServiceDetail> getGetRoBasicServiceDetails() {
        return getRoBasicServiceDetails;
    }

    /**
     *
     * @param getRoBasicServiceDetails
     * The GetRoBasicServiceDetails
     */
    public void setGetRoBasicServiceDetails(List<GetRoBasicServiceDetail> getRoBasicServiceDetails) {
        this.getRoBasicServiceDetails = getRoBasicServiceDetails;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }




@Generated("org.jsonschema2pojo")
public static class GetRoBasicServiceDetail {

    @SerializedName("ROBasicSerID")
    @Expose
    private Integer rOBasicSerID;
    @SerializedName("ROBasicSerName")
    @Expose
    private String rOBasicSerName;
    @SerializedName("ROBasicSerImage")
    @Expose
    private String rOBasicSerImage;
    @SerializedName("ROBasicSerIsActive")
    @Expose
    private Boolean rOBasicSerIsActive;

    /**
     *
     * @return
     * The rOBasicSerID
     */
    public Integer getROBasicSerID() {
        return rOBasicSerID;
    }

    /**
     *
     * @param rOBasicSerID
     * The ROBasicSerID
     */
    public void setROBasicSerID(Integer rOBasicSerID) {
        this.rOBasicSerID = rOBasicSerID;
    }

    /**
     *
     * @return
     * The rOBasicSerName
     */
    public String getROBasicSerName() {
        return rOBasicSerName;
    }

    /**
     *
     * @param rOBasicSerName
     * The ROBasicSerName
     */
    public void setROBasicSerName(String rOBasicSerName) {
        this.rOBasicSerName = rOBasicSerName;
    }

    /**
     *
     * @return
     * The rOBasicSerImage
     */
    public String getROBasicSerImage() {
        return rOBasicSerImage;
    }

    /**
     *
     * @param rOBasicSerImage
     * The ROBasicSerImage
     */
    public void setROBasicSerImage(String rOBasicSerImage) {
        this.rOBasicSerImage = rOBasicSerImage;
    }

    /**
     *
     * @return
     * The rOBasicSerIsActive
     */
    public Boolean getROBasicSerIsActive() {
        return rOBasicSerIsActive;
    }

    /**
     *
     * @param rOBasicSerIsActive
     * The ROBasicSerIsActive
     */
    public void setROBasicSerIsActive(Boolean rOBasicSerIsActive) {
        this.rOBasicSerIsActive = rOBasicSerIsActive;
    }

}



    public class ROAdditionalService {

        @SerializedName("ROSerID")
        @Expose
        private Integer rOSerID;
        @SerializedName("ROMasterID")
        @Expose
        private Integer rOMasterID;
        @SerializedName("ROServiceID")
        @Expose
        private Integer rOServiceID;
        @SerializedName("ROSerDescription")
        @Expose
        private String rOSerDescription;
        @SerializedName("CreateDate")
        @Expose
        private String createDate;
        @SerializedName("SerCatIcon")
        @Expose
        private String serCatIcon;
        @SerializedName("SerCatName")
        @Expose
        private String serCatName;

        public Integer getROSerID() {
            return rOSerID;
        }

        public void setROSerID(Integer rOSerID) {
            this.rOSerID = rOSerID;
        }

        public Integer getROMasterID() {
            return rOMasterID;
        }

        public void setROMasterID(Integer rOMasterID) {
            this.rOMasterID = rOMasterID;
        }

        public Integer getROServiceID() {
            return rOServiceID;
        }

        public void setROServiceID(Integer rOServiceID) {
            this.rOServiceID = rOServiceID;
        }

        public String getROSerDescription() {
            return rOSerDescription;
        }

        public void setROSerDescription(String rOSerDescription) {
            this.rOSerDescription = rOSerDescription;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getSerCatIcon() {
            return serCatIcon;
        }

        public void setSerCatIcon(String serCatIcon) {
            this.serCatIcon = serCatIcon;
        }

        public String getSerCatName() {
            return serCatName;
        }

        public void setSerCatName(String serCatName) {
            this.serCatName = serCatName;
        }

    }

@Generated("org.jsonschema2pojo")
public class ROCategoriesList {

    @SerializedName("ROID")
    @Expose
    private Integer rOID;
    @SerializedName("ROCategoryName")
    @Expose
    private String rOCategoryName;
    @SerializedName("ROCategoryDesc")
    @Expose
    private String rOCategoryDesc;
    @SerializedName("ROIsActive")
    @Expose
    private Boolean rOIsActive;
    @SerializedName("ROCreateDate")
    @Expose
    private String rOCreateDate;

    /**
     *
     * @return
     * The rOID
     */
    public Integer getROID() {
        return rOID;
    }

    /**
     *
     * @param rOID
     * The ROID
     */
    public void setROID(Integer rOID) {
        this.rOID = rOID;
    }

    /**
     *
     * @return
     * The rOCategoryName
     */
    public String getROCategoryName() {
        return rOCategoryName;
    }

    /**
     *
     * @param rOCategoryName
     * The ROCategoryName
     */
    public void setROCategoryName(String rOCategoryName) {
        this.rOCategoryName = rOCategoryName;
    }

    /**
     *
     * @return
     * The rOCategoryDesc
     */
    public String getROCategoryDesc() {
        return rOCategoryDesc;
    }

    /**
     *
     * @param rOCategoryDesc
     * The ROCategoryDesc
     */
    public void setROCategoryDesc(String rOCategoryDesc) {
        this.rOCategoryDesc = rOCategoryDesc;
    }

    /**
     *
     * @return
     * The rOIsActive
     */
    public Boolean getROIsActive() {
        return rOIsActive;
    }

    /**
     *
     * @param rOIsActive
     * The ROIsActive
     */
    public void setROIsActive(Boolean rOIsActive) {
        this.rOIsActive = rOIsActive;
    }

    /**
     *
     * @return
     * The rOCreateDate
     */
    public String getROCreateDate() {
        return rOCreateDate;
    }

    /**
     *
     * @param rOCreateDate
     * The ROCreateDate
     */
    public void setROCreateDate(String rOCreateDate) {
        this.rOCreateDate = rOCreateDate;
    }

}


@Generated("org.jsonschema2pojo")
public class ROHighwayTypeAll {

    @SerializedName("HighwayTypeID")
    @Expose
    private Integer highwayTypeID;
    @SerializedName("HighwayTypeName")
    @Expose
    private String highwayTypeName;

    /**
     *
     * @return
     * The highwayTypeID
     */
    public Integer getHighwayTypeID() {
        return highwayTypeID;
    }

    /**
     *
     * @param highwayTypeID
     * The HighwayTypeID
     */
    public void setHighwayTypeID(Integer highwayTypeID) {
        this.highwayTypeID = highwayTypeID;
    }

    /**
     *
     * @return
     * The highwayTypeName
     */
    public String getHighwayTypeName() {
        return highwayTypeName;
    }

    /**
     *
     * @param highwayTypeName
     * The HighwayTypeName
     */
    public void setHighwayTypeName(String highwayTypeName) {
        this.highwayTypeName = highwayTypeName;
    }

}

@Generated("org.jsonschema2pojo")
public class RetailOutletType {

    @SerializedName("rTypeID")
    @Expose
    private Integer rTypeID;
    @SerializedName("roTypeName")
    @Expose
    private String roTypeName;
    @SerializedName("roTypeDesc")
    @Expose
    private String roTypeDesc;
    @SerializedName("roTypeIsActive")
    @Expose
    private Boolean roTypeIsActive;
    @SerializedName("roTypeCreateDate")
    @Expose
    private String roTypeCreateDate;

    /**
     *
     * @return
     * The rTypeID
     */
    public Integer getRTypeID() {
        return rTypeID;
    }

    /**
     *
     * @param rTypeID
     * The rTypeID
     */
    public void setRTypeID(Integer rTypeID) {
        this.rTypeID = rTypeID;
    }

    /**
     *
     * @return
     * The roTypeName
     */
    public String getRoTypeName() {
        return roTypeName;
    }

    /**
     *
     * @param roTypeName
     * The roTypeName
     */
    public void setRoTypeName(String roTypeName) {
        this.roTypeName = roTypeName;
    }

    /**
     *
     * @return
     * The roTypeDesc
     */
    public String getRoTypeDesc() {
        return roTypeDesc;
    }

    /**
     *
     * @param roTypeDesc
     * The roTypeDesc
     */
    public void setRoTypeDesc(String roTypeDesc) {
        this.roTypeDesc = roTypeDesc;
    }

    /**
     *
     * @return
     * The roTypeIsActive
     */
    public Boolean getRoTypeIsActive() {
        return roTypeIsActive;
    }

    /**
     *
     * @param roTypeIsActive
     * The roTypeIsActive
     */
    public void setRoTypeIsActive(Boolean roTypeIsActive) {
        this.roTypeIsActive = roTypeIsActive;
    }

    /**
     *
     * @return
     * The roTypeCreateDate
     */
    public String getRoTypeCreateDate() {
        return roTypeCreateDate;
    }

    /**
     *
     * @param roTypeCreateDate
     * The roTypeCreateDate
     */
    public void setRoTypeCreateDate(String roTypeCreateDate) {
        this.roTypeCreateDate = roTypeCreateDate;
    }

}

@Generated("org.jsonschema2pojo")
public class ServiceListObj {

    @SerializedName("SerCatId")
    @Expose
    private Integer serCatId;
    @SerializedName("SerCatImage")
    @Expose
    private String serCatImage;
    @SerializedName("SerCatIcon")
    @Expose
    private String serCatIcon;
    @SerializedName("SerCatIsActive")
    @Expose
    private Boolean serCatIsActive;
    @SerializedName("SerCatHostDetails")
    @Expose
    private String serCatHostDetails;
    @SerializedName("SerCatAdminId")
    @Expose
    private Integer serCatAdminId;
    @SerializedName("SerCatName")
    @Expose
    private String serCatName;
    @SerializedName("SerCatFooterImage")
    @Expose
    private String serCatFooterImage;
    @SerializedName("SerCatFooterImageSelected")
    @Expose
    private String serCatFooterImageSelected;
    @SerializedName("SerSeqId")
    @Expose
    private Integer serSeqId;
    @SerializedName("SerIsRo")
    @Expose
    private Boolean serIsRo;
    @SerializedName("DefaultSpendTime")
    @Expose
    private String defaultSpendTime;
    @SerializedName("SerIsMapOn")
    @Expose
    private Boolean serIsMapOn;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The serCatId
     */
    public Integer getSerCatId() {
        return serCatId;
    }

    /**
     *
     * @param serCatId
     * The SerCatId
     */
    public void setSerCatId(Integer serCatId) {
        this.serCatId = serCatId;
    }

    /**
     *
     * @return
     * The serCatImage
     */
    public String getSerCatImage() {
        return serCatImage;
    }

    /**
     *
     * @param serCatImage
     * The SerCatImage
     */
    public void setSerCatImage(String serCatImage) {
        this.serCatImage = serCatImage;
    }

    /**
     *
     * @return
     * The serCatIcon
     */
    public String getSerCatIcon() {
        return serCatIcon;
    }

    /**
     *
     * @param serCatIcon
     * The SerCatIcon
     */
    public void setSerCatIcon(String serCatIcon) {
        this.serCatIcon = serCatIcon;
    }

    /**
     *
     * @return
     * The serCatIsActive
     */
    public Boolean getSerCatIsActive() {
        return serCatIsActive;
    }

    /**
     *
     * @param serCatIsActive
     * The SerCatIsActive
     */
    public void setSerCatIsActive(Boolean serCatIsActive) {
        this.serCatIsActive = serCatIsActive;
    }

    /**
     *
     * @return
     * The serCatHostDetails
     */
    public String getSerCatHostDetails() {
        return serCatHostDetails;
    }

    /**
     *
     * @param serCatHostDetails
     * The SerCatHostDetails
     */
    public void setSerCatHostDetails(String serCatHostDetails) {
        this.serCatHostDetails = serCatHostDetails;
    }

    /**
     *
     * @return
     * The serCatAdminId
     */
    public Integer getSerCatAdminId() {
        return serCatAdminId;
    }

    /**
     *
     * @param serCatAdminId
     * The SerCatAdminId
     */
    public void setSerCatAdminId(Integer serCatAdminId) {
        this.serCatAdminId = serCatAdminId;
    }

    /**
     *
     * @return
     * The serCatName
     */
    public String getSerCatName() {
        return serCatName;
    }

    /**
     *
     * @param serCatName
     * The SerCatName
     */
    public void setSerCatName(String serCatName) {
        this.serCatName = serCatName;
    }

    /**
     *
     * @return
     * The serCatFooterImage
     */
    public String getSerCatFooterImage() {
        return serCatFooterImage;
    }

    /**
     *
     * @param serCatFooterImage
     * The SerCatFooterImage
     */
    public void setSerCatFooterImage(String serCatFooterImage) {
        this.serCatFooterImage = serCatFooterImage;
    }

    /**
     *
     * @return
     * The serCatFooterImageSelected
     */
    public String getSerCatFooterImageSelected() {
        return serCatFooterImageSelected;
    }

    /**
     *
     * @param serCatFooterImageSelected
     * The SerCatFooterImageSelected
     */
    public void setSerCatFooterImageSelected(String serCatFooterImageSelected) {
        this.serCatFooterImageSelected = serCatFooterImageSelected;
    }

    /**
     *
     * @return
     * The serSeqId
     */
    public Integer getSerSeqId() {
        return serSeqId;
    }

    /**
     *
     * @param serSeqId
     * The SerSeqId
     */
    public void setSerSeqId(Integer serSeqId) {
        this.serSeqId = serSeqId;
    }

    /**
     *
     * @return
     * The serIsRo
     */
    public Boolean getSerIsRo() {
        return serIsRo;
    }

    /**
     *
     * @param serIsRo
     * The SerIsRo
     */
    public void setSerIsRo(Boolean serIsRo) {
        this.serIsRo = serIsRo;
    }

    /**
     *
     * @return
     * The defaultSpendTime
     */
    public String getDefaultSpendTime() {
        return defaultSpendTime;
    }

    /**
     *
     * @param defaultSpendTime
     * The DefaultSpendTime
     */
    public void setDefaultSpendTime(String defaultSpendTime) {
        this.defaultSpendTime = defaultSpendTime;
    }

    /**
     *
     * @return
     * The serIsMapOn
     */
    public Boolean getSerIsMapOn() {
        return serIsMapOn;
    }

    /**
     *
     * @param serIsMapOn
     * The SerIsMapOn
     */
    public void setSerIsMapOn(Boolean serIsMapOn) {
        this.serIsMapOn = serIsMapOn;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}
    @Generated("org.jsonschema2pojo")
    public class ToiletTypesAll {

        @SerializedName("ToiletID")
        @Expose
        private Integer toiletID;
        @SerializedName("ToiletName")
        @Expose
        private String toiletName;

        /**
         *
         * @return
         * The toiletID
         */
        public Integer getToiletID() {
            return toiletID;
        }

        /**
         *
         * @param toiletID
         * The ToiletID
         */
        public void setToiletID(Integer toiletID) {
            this.toiletID = toiletID;
        }

        /**
         *
         * @return
         * The toiletName
         */
        public String getToiletName() {
            return toiletName;
        }

        /**
         *
         * @param toiletName
         * The ToiletName
         */
        public void setToiletName(String toiletName) {
            this.toiletName = toiletName;
        }

    }
}