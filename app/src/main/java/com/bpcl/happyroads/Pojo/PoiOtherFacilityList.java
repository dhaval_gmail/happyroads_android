
        package com.bpcl.happyroads.Pojo;

        import javax.annotation.Generated;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PoiOtherFacilityList {

    @SerializedName("PoiAdvOtherId")
    @Expose
    private Integer poiAdvOtherId;
    @SerializedName("PoiAdvOtherName")
    @Expose
    private String poiAdvOtherName;
    @SerializedName("PoiAdvOtherIcon")
    @Expose
    private String poiAdvOtherIcon;
    @SerializedName("PoiAdvOtherImage")
    @Expose
    private String poiAdvOtherImage;
    @SerializedName("PoiAdvOtherIsActive")
    @Expose
    private Boolean poiAdvOtherIsActive;
    @SerializedName("PoiAdvOtherHostDetails")
    @Expose
    private Object poiAdvOtherHostDetails;
    @SerializedName("PoiAdvOtherAdminId")
    @Expose
    private Object poiAdvOtherAdminId;
    @SerializedName("PoiAdvOtherCreateDate")
    @Expose
    private String poiAdvOtherCreateDate;
    @SerializedName("PoiAdvOtherUpdateDate")
    @Expose
    private String poiAdvOtherUpdateDate;

    /**
     *
     * @return
     * The poiAdvOtherId
     */
    public Integer getPoiAdvOtherId() {
        return poiAdvOtherId;
    }

    /**
     *
     * @param poiAdvOtherId
     * The PoiAdvOtherId
     */
    public void setPoiAdvOtherId(Integer poiAdvOtherId) {
        this.poiAdvOtherId = poiAdvOtherId;
    }

    /**
     *
     * @return
     * The poiAdvOtherName
     */
    public String getPoiAdvOtherName() {
        return poiAdvOtherName;
    }

    /**
     *
     * @param poiAdvOtherName
     * The PoiAdvOtherName
     */
    public void setPoiAdvOtherName(String poiAdvOtherName) {
        this.poiAdvOtherName = poiAdvOtherName;
    }

    /**
     *
     * @return
     * The poiAdvOtherIcon
     */
    public String getPoiAdvOtherIcon() {
        return poiAdvOtherIcon;
    }

    /**
     *
     * @param poiAdvOtherIcon
     * The PoiAdvOtherIcon
     */
    public void setPoiAdvOtherIcon(String poiAdvOtherIcon) {
        this.poiAdvOtherIcon = poiAdvOtherIcon;
    }

    /**
     *
     * @return
     * The poiAdvOtherImage
     */
    public String getPoiAdvOtherImage() {
        return poiAdvOtherImage;
    }

    /**
     *
     * @param poiAdvOtherImage
     * The PoiAdvOtherImage
     */
    public void setPoiAdvOtherImage(String poiAdvOtherImage) {
        this.poiAdvOtherImage = poiAdvOtherImage;
    }

    /**
     *
     * @return
     * The poiAdvOtherIsActive
     */
    public Boolean getPoiAdvOtherIsActive() {
        return poiAdvOtherIsActive;
    }

    /**
     *
     * @param poiAdvOtherIsActive
     * The PoiAdvOtherIsActive
     */
    public void setPoiAdvOtherIsActive(Boolean poiAdvOtherIsActive) {
        this.poiAdvOtherIsActive = poiAdvOtherIsActive;
    }

    /**
     *
     * @return
     * The poiAdvOtherHostDetails
     */
    public Object getPoiAdvOtherHostDetails() {
        return poiAdvOtherHostDetails;
    }

    /**
     *
     * @param poiAdvOtherHostDetails
     * The PoiAdvOtherHostDetails
     */
    public void setPoiAdvOtherHostDetails(Object poiAdvOtherHostDetails) {
        this.poiAdvOtherHostDetails = poiAdvOtherHostDetails;
    }

    /**
     *
     * @return
     * The poiAdvOtherAdminId
     */
    public Object getPoiAdvOtherAdminId() {
        return poiAdvOtherAdminId;
    }

    /**
     *
     * @param poiAdvOtherAdminId
     * The PoiAdvOtherAdminId
     */
    public void setPoiAdvOtherAdminId(Object poiAdvOtherAdminId) {
        this.poiAdvOtherAdminId = poiAdvOtherAdminId;
    }

    /**
     *
     * @return
     * The poiAdvOtherCreateDate
     */
    public String getPoiAdvOtherCreateDate() {
        return poiAdvOtherCreateDate;
    }

    /**
     *
     * @param poiAdvOtherCreateDate
     * The PoiAdvOtherCreateDate
     */
    public void setPoiAdvOtherCreateDate(String poiAdvOtherCreateDate) {
        this.poiAdvOtherCreateDate = poiAdvOtherCreateDate;
    }

    /**
     *
     * @return
     * The poiAdvOtherUpdateDate
     */
    public String getPoiAdvOtherUpdateDate() {
        return poiAdvOtherUpdateDate;
    }

    /**
     *
     * @param poiAdvOtherUpdateDate
     * The PoiAdvOtherUpdateDate
     */
    public void setPoiAdvOtherUpdateDate(String poiAdvOtherUpdateDate) {
        this.poiAdvOtherUpdateDate = poiAdvOtherUpdateDate;
    }

}