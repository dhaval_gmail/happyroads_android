package com.bpcl.happyroads.Pojo;

/**
 * Created by admin on 12/12/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class push {

    @SerializedName("ntfContent")
    @Expose
    private String ntfContent;
    @SerializedName("userFullName")
    @Expose
    private String userFullName;
    @SerializedName("ntfUsrId")
    @Expose
    private String ntfUsrId;
    @SerializedName("notificationType")
    @Expose
    private String notificationType;
    @SerializedName("ntfRefKey")
    @Expose
    private String ntfRefKey;
    @SerializedName("iconIdentifier")
    @Expose
    private String iconIdentifier;

    /**
     *
     * @return
     * The ntfContent
     */
    public String getNtfContent() {
        return ntfContent;
    }

    /**
     *
     * @param ntfContent
     * The ntfContent
     */
    public void setNtfContent(String ntfContent) {
        this.ntfContent = ntfContent;
    }

    /**
     *
     * @return
     * The userFullName
     */
    public String getUserFullName() {
        return userFullName;
    }

    /**
     *
     * @param userFullName
     * The userFullName
     */
    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    /**
     *
     * @return
     * The ntfUsrId
     */
    public String getNtfUsrId() {
        return ntfUsrId;
    }

    /**
     *
     * @param ntfUsrId
     * The ntfUsrId
     */
    public void setNtfUsrId(String ntfUsrId) {
        this.ntfUsrId = ntfUsrId;
    }

    /**
     *
     * @return
     * The notificationType
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     *
     * @param notificationType
     * The notificationType
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     *
     * @return
     * The ntfRefKey
     */
    public String getNtfRefKey() {
        return ntfRefKey;
    }

    /**
     *
     * @param ntfRefKey
     * The ntfRefKey
     */
    public void setNtfRefKey(String ntfRefKey) {
        this.ntfRefKey = ntfRefKey;
    }

    /**
     *
     * @return
     * The iconIdentifier
     */
    public String getIconIdentifier() {
        return iconIdentifier;
    }

    /**
     *
     * @param iconIdentifier
     * The iconIdentifier
     */
    public void setIconIdentifier(String iconIdentifier) {
        this.iconIdentifier = iconIdentifier;
    }

}