package com.bpcl.happyroads.Pojo;

/**
 * Created by admin on 07/11/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;



@Generated("org.jsonschema2pojo")
public class SubsciptionPlans implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("defaultDurationDays")
    @Expose
    private Integer defaultDurationDays;
    @SerializedName("defaultCoolOffDays")
    @Expose
    private Integer defaultCoolOffDays;
    @SerializedName("currentDefaultSalePrice")
    @Expose
    private String currentDefaultSalePrice;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("__version")
    @Expose
    private String version;
    @SerializedName("__createdAt")
    @Expose
    private String createdAt;
    @SerializedName("__updatedAt")
    @Expose
    private String updatedAt;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The defaultDurationDays
     */
    public Integer getDefaultDurationDays() {
        return defaultDurationDays;
    }

    /**
     *
     * @param defaultDurationDays
     * The defaultDurationDays
     */
    public void setDefaultDurationDays(Integer defaultDurationDays) {
        this.defaultDurationDays = defaultDurationDays;
    }

    /**
     *
     * @return
     * The defaultCoolOffDays
     */
    public Integer getDefaultCoolOffDays() {
        return defaultCoolOffDays;
    }

    /**
     *
     * @param defaultCoolOffDays
     * The defaultCoolOffDays
     */
    public void setDefaultCoolOffDays(Integer defaultCoolOffDays) {
        this.defaultCoolOffDays = defaultCoolOffDays;
    }

    /**
     *
     * @return
     * The currentDefaultSalePrice
     */
    public String getCurrentDefaultSalePrice() {
        return currentDefaultSalePrice;
    }

    /**
     *
     * @param currentDefaultSalePrice
     * The currentDefaultSalePrice
     */
    public void setCurrentDefaultSalePrice(String currentDefaultSalePrice) {
        this.currentDefaultSalePrice = currentDefaultSalePrice;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The version
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     * The __version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The __createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The __updatedAt
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
