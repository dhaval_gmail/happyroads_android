package com.bpcl.happyroads.Pojo;

/**
 * Created by dhavalkaka on 06/02/2017.
 */

public class RouteLegStep {
    public Double currentDistance;
    public Integer currentStep;
    public Integer currentLeg;

    public Double getCurrentDistancemeter() {
        return currentDistance;
    }

    public void setCurrentDistancemeter(Double currentDistancemeter) {
        this.currentDistance = currentDistancemeter;
    }

    public Integer getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(Integer currentStep) {
        this.currentStep = currentStep;
    }

    public Integer getCurrentLeg() {
        return currentLeg;
    }

    public void setCurrentLeg(Integer currentLeg) {
        this.currentLeg = currentLeg;
    }

}
