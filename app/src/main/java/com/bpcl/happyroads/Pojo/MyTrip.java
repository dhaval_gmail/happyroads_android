
package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;






@Generated("org.jsonschema2pojo")
public class MyTrip {

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;
    @SerializedName("TripId")
    @Expose
    private Integer tripId;
    @SerializedName("TripName")
    @Expose
    private String tripName;
    @SerializedName("TripDate")
    @Expose
    private String tripDate;
    @SerializedName("TripGoingTo_Start")
    @Expose
    private Integer tripGoingToStart;
    @Expose
    private String tripReturnKm;

    public String getTripReturnTime() {
        if(tripReturnTime==null || tripReturnTime.equalsIgnoreCase("null"))
        {
            return null;
        }else {
            return tripReturnTime;

        }
    }

    public void setTripReturnTime(String tripReturnTime) {
        this.tripReturnTime = tripReturnTime;
    }

    public String getTripReturnKm() {
        if(tripReturnKm==null || tripReturnKm.equalsIgnoreCase("null"))
        {
            return null;
        }else {
            return tripReturnKm;
        }
    }

    public void setTripReturnKm(String tripReturnKm) {
        this.tripReturnKm = tripReturnKm;
    }

    @SerializedName("TripReturnTime")
    @Expose
    private String tripReturnTime;

    public Integer getEnddesNoOfNearByAttraction() {
        if(enddesNoOfNearByAttraction==null)
        {
            return 0;
        }else {
            return enddesNoOfNearByAttraction;
        }
    }

    public void setEnddesNoOfNearByAttraction(Integer enddesNoOfNearByAttraction) {
        this.enddesNoOfNearByAttraction = enddesNoOfNearByAttraction;
    }

    @SerializedName("enddesNoOfNearByAttraction")
    @Expose
    private Integer enddesNoOfNearByAttraction;


    @SerializedName("TripDestination_End")
    @Expose
    private Integer tripDestinationEnd;
    @SerializedName("TripStatus")
    @Expose
    private String tripStatus;
    @SerializedName("TripIsActive")
    @Expose
    private Boolean tripIsActive;
    @SerializedName("TripIsDeleted")
    @Expose
    private Boolean tripIsDeleted;
    @SerializedName("TripCreatedDate")
    @Expose
    private String tripCreatedDate;
    @SerializedName("TripEndDateTime")
    @Expose
    private String tripEndDateTime;
    @SerializedName("TripUmID")
    @Expose
    private Integer tripUmID;
    @SerializedName("TripGooglePlaceDetails")
    @Expose
    private String tripGooglePlaceDetails;

    public boolean getTripIsDestinationPlanned() {
        return TripIsDestinationPlanned;
    }

    public void setTripIsDestinationPlanned(boolean tripIsDestinationPlanned) {
        TripIsDestinationPlanned = tripIsDestinationPlanned;
    }

    @SerializedName("TripIsDestinationPlanned")
    @Expose
    private boolean TripIsDestinationPlanned;
    @SerializedName("TripReturnDate")
    @Expose
    private String tripReturnDate;
    @SerializedName("TripReturnDateETA")
    @Expose
    private String tripReturnDateETA;
    @SerializedName("umId")
    @Expose
    private Integer umId;
    @SerializedName("umFirstName")
    @Expose
    private String umFirstName;
    @SerializedName("umLastName")
    @Expose
    private String umLastName;
    @SerializedName("umMobile")
    @Expose
    private String umMobile;
    @SerializedName("umEmailId")
    @Expose
    private String umEmailId;
    @SerializedName("umTitle")
    @Expose
    private String umTitle;
    @SerializedName("umDOB")
    @Expose
    private String umDOB;
    @SerializedName("umPetroCard")
    @Expose
    private String umPetroCard;
    @SerializedName("umCreatedDate")
    @Expose
    private String umCreatedDate;
    @SerializedName("umUpdateDate")
    @Expose
    private String umUpdateDate;
    @SerializedName("umLastLoginDate")
    @Expose
    private String umLastLoginDate;
    @SerializedName("umIsActive")
    @Expose
    private Boolean umIsActive;
    @SerializedName("umIsDeleted")
    @Expose
    private Boolean umIsDeleted;
    @SerializedName("umPassword")
    @Expose
    private String umPassword;
    @SerializedName("umOTP")
    @Expose
    private String umOTP;
    @SerializedName("umLat")
    @Expose
    private String umLat;
    @SerializedName("umLong")
    @Expose
    private String umLong;
    @SerializedName("umUdid")
    @Expose
    private String umUdid;
    @SerializedName("umIsPushOn")
    @Expose
    private Boolean umIsPushOn;
    @SerializedName("umReferralCode")
    @Expose
    private String umReferralCode;
    @SerializedName("umDeviceType")
    @Expose
    private String umDeviceType;
    @SerializedName("umProfilePhoto")
    @Expose
    private String umProfilePhoto;
    @SerializedName("umFacebookToken")
    @Expose
    private String umFacebookToken;
    @SerializedName("umGoogleToken")
    @Expose
    private String umGoogleToken;
    @SerializedName("umDescription")
    @Expose
    private String umDescription;
    @SerializedName("umLocation")
    @Expose
    private String umLocation;
    @SerializedName("umEmrgncyName")
    @Expose
    private String umEmrgncyName;
    @SerializedName("umEmrgncyNumber")
    @Expose
    private String umEmrgncyNumber;
    @SerializedName("umMyUniqueReferralCode")
    @Expose
    private String umMyUniqueReferralCode;
    @SerializedName("umCoTraveller")
    @Expose
    private String umCoTraveller;
    @SerializedName("originDesName")
    @Expose
    private String originDesName;
    @SerializedName("originLat")
    @Expose
    private String originLat;
    @SerializedName("originLong")
    @Expose
    private String originLong;
    @SerializedName("origindesImage")
    @Expose
    private String origindesImage;
    @SerializedName("endDesName")
    @Expose
    private String endDesName;
    @SerializedName("endDeslat")
    @Expose
    private String endDeslat;
    @SerializedName("endDesLong")
    @Expose
    private String endDesLong;
    @SerializedName("endDesImage")
    @Expose
    private String endDesImage;
    @SerializedName("Distance")
    @Expose
    private Double distance;
    @SerializedName("duration")
    @Expose
    private Double duration;
    @SerializedName("GetTripPlans")
    @Expose
    private List<GetTripPlan> getTripPlans = new ArrayList<GetTripPlan>();
    @SerializedName("ImageListByOriginId")
    @Expose
    private List<ImageListByOriginId> imageListByOriginId = new ArrayList<ImageListByOriginId>();
    @SerializedName("ImageListByDestinationId")
    @Expose
    private List<ImageListByDestinationId> imageListByDestinationId = new ArrayList<ImageListByDestinationId>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    @SerializedName("TripIsOneSideCompleted")
    @Expose
    Boolean tripIsOneSideCompleted;
    public Boolean getTripIsOneSideCompleted() {
        if(tripIsOneSideCompleted == null)
        {
            return false;
        }
        else {
            return tripIsOneSideCompleted;
        }
    }

    public void setTripIsOneSideCompleted(Boolean tripIsOneSideCompleted) {
        this.tripIsOneSideCompleted = tripIsOneSideCompleted;
    }

    /**
     *
     * @return
     * The rowNum
     */
    public Integer getRowNum() {
        return rowNum;
    }

    /**
     *
     * @param rowNum
     * The RowNum
     */
    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    /**
     *
     * @return
     * The tripId
     */
    public Integer getTripId() {
        return tripId;
    }

    /**
     *
     * @param tripId
     * The TripId
     */
    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    /**
     *
     * @return
     * The tripName
     */
    public String getTripName() {
        return tripName;
    }

    /**
     *
     * @param tripName
     * The TripName
     */
    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    /**
     *
     * @return
     * The tripDate
     */
    public String getTripDate() {
        return tripDate;
    }

    /**
     *
     * @param tripDate
     * The TripDate
     */
    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    /**
     *
     * @return
     * The tripGoingToStart
     */
    public Integer getTripGoingToStart() {
        return tripGoingToStart;
    }

    /**
     *
     * @param tripGoingToStart
     * The TripGoingTo_Start
     */
    public void setTripGoingToStart(Integer tripGoingToStart) {
        this.tripGoingToStart = tripGoingToStart;
    }

    /**
     *
     * @return
     * The tripDestinationEnd
     */
    public Integer getTripDestinationEnd() {
        return tripDestinationEnd;
    }

    /**
     *
     * @param tripDestinationEnd
     * The TripDestination_End
     */
    public void setTripDestinationEnd(Integer tripDestinationEnd) {
        this.tripDestinationEnd = tripDestinationEnd;
    }

    /**
     *
     * @return
     * The tripStatus
     */
    public String getTripStatus() {
        return tripStatus;
    }

    /**
     *
     * @param tripStatus
     * The TripStatus
     */
    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    /**
     *
     * @return
     * The tripIsActive
     */
    public Boolean getTripIsActive() {
        return tripIsActive;
    }

    /**
     *
     * @param tripIsActive
     * The TripIsActive
     */
    public void setTripIsActive(Boolean tripIsActive) {
        this.tripIsActive = tripIsActive;
    }

    /**
     *
     * @return
     * The tripIsDeleted
     */
    public Boolean getTripIsDeleted() {
        return tripIsDeleted;
    }

    /**
     *
     * @param tripIsDeleted
     * The TripIsDeleted
     */
    public void setTripIsDeleted(Boolean tripIsDeleted) {
        this.tripIsDeleted = tripIsDeleted;
    }

    /**
     *
     * @return
     * The tripCreatedDate
     */
    public String getTripCreatedDate() {
        return tripCreatedDate;
    }

    /**
     *
     * @param tripCreatedDate
     * The TripCreatedDate
     */
    public void setTripCreatedDate(String tripCreatedDate) {
        this.tripCreatedDate = tripCreatedDate;
    }

    /**
     *
     * @return
     * The tripEndDateTime
     */
    public String getTripEndDateTime() {
        return tripEndDateTime;
    }

    /**
     *
     * @param tripEndDateTime
     * The TripEndDateTime
     */
    public void setTripEndDateTime(String tripEndDateTime) {
        this.tripEndDateTime = tripEndDateTime;
    }

    /**
     *
     * @return
     * The tripUmID
     */
    public Integer getTripUmID() {
        return tripUmID;
    }

    /**
     *
     * @param tripUmID
     * The TripUmID
     */
    public void setTripUmID(Integer tripUmID) {
        this.tripUmID = tripUmID;
    }

    /**
     *
     * @return
     * The tripGooglePlaceDetails
     */
    public String getTripGooglePlaceDetails() {
        return tripGooglePlaceDetails;
    }

    /**
     *
     * @param tripGooglePlaceDetails
     * The TripGooglePlaceDetails
     */
    public void setTripGooglePlaceDetails(String tripGooglePlaceDetails) {
        this.tripGooglePlaceDetails = tripGooglePlaceDetails;
    }

    /**
     *
     * @return
     * The tripReturnDate
     */
    public String getTripReturnDate() {
        return tripReturnDate;
    }

    /**
     *
     * @param tripReturnDate
     * The TripReturnDate
     */
    public void setTripReturnDate(String tripReturnDate) {
        this.tripReturnDate = tripReturnDate;
    }

    /**
     *
     * @return
     * The tripReturnDateETA
     */
    public String getTripReturnDateETA() {
        return tripReturnDateETA;
    }

    /**
     *
     * @param tripReturnDateETA
     * The TripReturnDateETA
     */
    public void setTripReturnDateETA(String tripReturnDateETA) {
        this.tripReturnDateETA = tripReturnDateETA;
    }

    /**
     *
     * @return
     * The umId
     */
    public Integer getUmId() {
        return umId;
    }

    /**
     *
     * @param umId
     * The umId
     */
    public void setUmId(Integer umId) {
        this.umId = umId;
    }

    /**
     *
     * @return
     * The umFirstName
     */
    public String getUmFirstName() {
        return umFirstName;
    }

    /**
     *
     * @param umFirstName
     * The umFirstName
     */
    public void setUmFirstName(String umFirstName) {
        this.umFirstName = umFirstName;
    }

    /**
     *
     * @return
     * The umLastName
     */
    public String getUmLastName() {
        return umLastName;
    }

    /**
     *
     * @param umLastName
     * The umLastName
     */
    public void setUmLastName(String umLastName) {
        this.umLastName = umLastName;
    }

    /**
     *
     * @return
     * The umMobile
     */
    public String getUmMobile() {
        return umMobile;
    }

    /**
     *
     * @param umMobile
     * The umMobile
     */
    public void setUmMobile(String umMobile) {
        this.umMobile = umMobile;
    }

    /**
     *
     * @return
     * The umEmailId
     */
    public String getUmEmailId() {
        return umEmailId;
    }

    /**
     *
     * @param umEmailId
     * The umEmailId
     */
    public void setUmEmailId(String umEmailId) {
        this.umEmailId = umEmailId;
    }

    /**
     *
     * @return
     * The umTitle
     */
    public String getUmTitle() {
        return umTitle;
    }

    /**
     *
     * @param umTitle
     * The umTitle
     */
    public void setUmTitle(String umTitle) {
        this.umTitle = umTitle;
    }

    /**
     *
     * @return
     * The umDOB
     */
    public String getUmDOB() {
        return umDOB;
    }

    /**
     *
     * @param umDOB
     * The umDOB
     */
    public void setUmDOB(String umDOB) {
        this.umDOB = umDOB;
    }

    /**
     *
     * @return
     * The umPetroCard
     */
    public String getUmPetroCard() {
        return umPetroCard;
    }

    /**
     *
     * @param umPetroCard
     * The umPetroCard
     */
    public void setUmPetroCard(String umPetroCard) {
        this.umPetroCard = umPetroCard;
    }

    /**
     *
     * @return
     * The umCreatedDate
     */
    public String getUmCreatedDate() {
        return umCreatedDate;
    }

    /**
     *
     * @param umCreatedDate
     * The umCreatedDate
     */
    public void setUmCreatedDate(String umCreatedDate) {
        this.umCreatedDate = umCreatedDate;
    }

    /**
     *
     * @return
     * The umUpdateDate
     */
    public String getUmUpdateDate() {
        return umUpdateDate;
    }

    /**
     *
     * @param umUpdateDate
     * The umUpdateDate
     */
    public void setUmUpdateDate(String umUpdateDate) {
        this.umUpdateDate = umUpdateDate;
    }

    /**
     *
     * @return
     * The umLastLoginDate
     */
    public String getUmLastLoginDate() {
        return umLastLoginDate;
    }

    /**
     *
     * @param umLastLoginDate
     * The umLastLoginDate
     */
    public void setUmLastLoginDate(String umLastLoginDate) {
        this.umLastLoginDate = umLastLoginDate;
    }

    /**
     *
     * @return
     * The umIsActive
     */
    public Boolean getUmIsActive() {
        return umIsActive;
    }

    /**
     *
     * @param umIsActive
     * The umIsActive
     */
    public void setUmIsActive(Boolean umIsActive) {
        this.umIsActive = umIsActive;
    }

    /**
     *
     * @return
     * The umIsDeleted
     */
    public Boolean getUmIsDeleted() {
        return umIsDeleted;
    }

    /**
     *
     * @param umIsDeleted
     * The umIsDeleted
     */
    public void setUmIsDeleted(Boolean umIsDeleted) {
        this.umIsDeleted = umIsDeleted;
    }

    /**
     *
     * @return
     * The umPassword
     */
    public String getUmPassword() {
        return umPassword;
    }

    /**
     *
     * @param umPassword
     * The umPassword
     */
    public void setUmPassword(String umPassword) {
        this.umPassword = umPassword;
    }

    /**
     *
     * @return
     * The umOTP
     */
    public String getUmOTP() {
        return umOTP;
    }

    /**
     *
     * @param umOTP
     * The umOTP
     */
    public void setUmOTP(String umOTP) {
        this.umOTP = umOTP;
    }

    /**
     *
     * @return
     * The umLat
     */
    public String getUmLat() {
        return umLat;
    }

    /**
     *
     * @param umLat
     * The umLat
     */
    public void setUmLat(String umLat) {
        this.umLat = umLat;
    }

    /**
     *
     * @return
     * The umLong
     */
    public String getUmLong() {
        return umLong;
    }

    /**
     *
     * @param umLong
     * The umLong
     */
    public void setUmLong(String umLong) {
        this.umLong = umLong;
    }

    /**
     *
     * @return
     * The umUdid
     */
    public String getUmUdid() {
        return umUdid;
    }

    /**
     *
     * @param umUdid
     * The umUdid
     */
    public void setUmUdid(String umUdid) {
        this.umUdid = umUdid;
    }

    /**
     *
     * @return
     * The umIsPushOn
     */
    public Boolean getUmIsPushOn() {
        return umIsPushOn;
    }

    /**
     *
     * @param umIsPushOn
     * The umIsPushOn
     */
    public void setUmIsPushOn(Boolean umIsPushOn) {
        this.umIsPushOn = umIsPushOn;
    }

    /**
     *
     * @return
     * The umReferralCode
     */
    public String getUmReferralCode() {
        return umReferralCode;
    }

    /**
     *
     * @param umReferralCode
     * The umReferralCode
     */
    public void setUmReferralCode(String umReferralCode) {
        this.umReferralCode = umReferralCode;
    }

    /**
     *
     * @return
     * The umDeviceType
     */
    public String getUmDeviceType() {
        return umDeviceType;
    }

    /**
     *
     * @param umDeviceType
     * The umDeviceType
     */
    public void setUmDeviceType(String umDeviceType) {
        this.umDeviceType = umDeviceType;
    }

    /**
     *
     * @return
     * The umProfilePhoto
     */
    public String getUmProfilePhoto() {
        return umProfilePhoto;
    }

    /**
     *
     * @param umProfilePhoto
     * The umProfilePhoto
     */
    public void setUmProfilePhoto(String umProfilePhoto) {
        this.umProfilePhoto = umProfilePhoto;
    }

    /**
     *
     * @return
     * The umFacebookToken
     */
    public String getUmFacebookToken() {
        return umFacebookToken;
    }

    /**
     *
     * @param umFacebookToken
     * The umFacebookToken
     */
    public void setUmFacebookToken(String umFacebookToken) {
        this.umFacebookToken = umFacebookToken;
    }

    /**
     *
     * @return
     * The umGoogleToken
     */
    public String getUmGoogleToken() {
        return umGoogleToken;
    }

    /**
     *
     * @param umGoogleToken
     * The umGoogleToken
     */
    public void setUmGoogleToken(String umGoogleToken) {
        this.umGoogleToken = umGoogleToken;
    }

    /**
     *
     * @return
     * The umDescription
     */
    public String getUmDescription() {
        return umDescription;
    }

    /**
     *
     * @param umDescription
     * The umDescription
     */
    public void setUmDescription(String umDescription) {
        this.umDescription = umDescription;
    }

    /**
     *
     * @return
     * The umLocation
     */
    public String getUmLocation() {
        return umLocation;
    }

    /**
     *
     * @param umLocation
     * The umLocation
     */
    public void setUmLocation(String umLocation) {
        this.umLocation = umLocation;
    }

    /**
     *
     * @return
     * The umEmrgncyName
     */
    public String getUmEmrgncyName() {
        return umEmrgncyName;
    }

    /**
     *
     * @param umEmrgncyName
     * The umEmrgncyName
     */
    public void setUmEmrgncyName(String umEmrgncyName) {
        this.umEmrgncyName = umEmrgncyName;
    }

    /**
     *
     * @return
     * The umEmrgncyNumber
     */
    public String getUmEmrgncyNumber() {
        return umEmrgncyNumber;
    }

    /**
     *
     * @param umEmrgncyNumber
     * The umEmrgncyNumber
     */
    public void setUmEmrgncyNumber(String umEmrgncyNumber) {
        this.umEmrgncyNumber = umEmrgncyNumber;
    }

    /**
     *
     * @return
     * The umMyUniqueReferralCode
     */
    public String getUmMyUniqueReferralCode() {
        return umMyUniqueReferralCode;
    }

    /**
     *
     * @param umMyUniqueReferralCode
     * The umMyUniqueReferralCode
     */
    public void setUmMyUniqueReferralCode(String umMyUniqueReferralCode) {
        this.umMyUniqueReferralCode = umMyUniqueReferralCode;
    }

    /**
     *
     * @return
     * The umCoTraveller
     */
    public String getUmCoTraveller() {
        return umCoTraveller;
    }

    /**
     *
     * @param umCoTraveller
     * The umCoTraveller
     */
    public void setUmCoTraveller(String umCoTraveller) {
        this.umCoTraveller = umCoTraveller;
    }

    /**
     *
     * @return
     * The originDesName
     */
    public String getOriginDesName() {
        return originDesName;
    }

    /**
     *
     * @param originDesName
     * The originDesName
     */
    public void setOriginDesName(String originDesName) {
        this.originDesName = originDesName;
    }

    /**
     *
     * @return
     * The originLat
     */
    public String getOriginLat() {
        return originLat;
    }

    /**
     *
     * @param originLat
     * The originLat
     */
    public void setOriginLat(String originLat) {
        this.originLat = originLat;
    }

    /**
     *
     * @return
     * The originLong
     */
    public String getOriginLong() {
        return originLong;
    }

    /**
     *
     * @param originLong
     * The originLong
     */
    public void setOriginLong(String originLong) {
        this.originLong = originLong;
    }

    /**
     *
     * @return
     * The origindesImage
     */
    public String getOrigindesImage() {
        return origindesImage;
    }

    /**
     *
     * @param origindesImage
     * The origindesImage
     */
    public void setOrigindesImage(String origindesImage) {
        this.origindesImage = origindesImage;
    }

    /**
     *
     * @return
     * The endDesName
     */
    public String getEndDesName() {
        return endDesName;
    }

    /**
     *
     * @param endDesName
     * The endDesName
     */
    public void setEndDesName(String endDesName) {
        this.endDesName = endDesName;
    }

    /**
     *
     * @return
     * The endDeslat
     */
    public String getEndDeslat() {
        return endDeslat;
    }

    /**
     *
     * @param endDeslat
     * The endDeslat
     */
    public void setEndDeslat(String endDeslat) {
        this.endDeslat = endDeslat;
    }

    /**
     *
     * @return
     * The endDesLong
     */
    public String getEndDesLong() {
        return endDesLong;
    }

    /**
     *
     * @param endDesLong
     * The endDesLong
     */
    public void setEndDesLong(String endDesLong) {
        this.endDesLong = endDesLong;
    }

    /**
     *
     * @return
     * The endDesImage
     */
    public String getEndDesImage() {
        return endDesImage;
    }

    /**
     *
     * @param endDesImage
     * The endDesImage
     */
    public void setEndDesImage(String endDesImage) {
        this.endDesImage = endDesImage;
    }

    /**
     *
     * @return
     * The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The Distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The duration
     */
    public Double getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(Double duration) {
        this.duration = duration;
    }

    /**
     *
     * @return
     * The getTripPlans
     */
    public List<GetTripPlan> getGetTripPlans() {
        return getTripPlans;
    }

    /**
     *
     * @param getTripPlans
     * The GetTripPlans
     */
    public void setGetTripPlans(List<GetTripPlan> getTripPlans) {
        this.getTripPlans = getTripPlans;
    }

    /**
     *
     * @return
     * The imageListByOriginId
     */
    public List<ImageListByOriginId> getImageListByOriginId() {
        return imageListByOriginId;
    }

    /**
     *
     * @param imageListByOriginId
     * The ImageListByOriginId
     */
    public void setImageListByOriginId(List<ImageListByOriginId> imageListByOriginId) {
        this.imageListByOriginId = imageListByOriginId;
    }

    /**
     *
     * @return
     * The imageListByDestinationId
     */
    public List<ImageListByDestinationId> getImageListByDestinationId() {
        return imageListByDestinationId;
    }

    /**
     *
     * @param imageListByDestinationId
     * The ImageListByDestinationId
     */
    public void setImageListByDestinationId(List<ImageListByDestinationId> imageListByDestinationId) {
        this.imageListByDestinationId = imageListByDestinationId;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }
    @Generated("org.jsonschema2pojo")
    public class DestinatinObject {

        @SerializedName("desID")
        @Expose
        private Integer desID;
        @SerializedName("desCountry")
        @Expose
        private Integer desCountry;
        @SerializedName("desState")
        @Expose
        private Integer desState;
        @SerializedName("desDistrict")
        @Expose
        private Integer desDistrict;
        @SerializedName("desCity")
        @Expose
        private Integer desCity;
        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;
        @SerializedName("umId")
        @Expose
        private Integer umId;
        @SerializedName("resStatus")
        @Expose
        private Boolean resStatus;
        @SerializedName("resCallerDetails")
        @Expose
        private String resCallerDetails;
        @SerializedName("PageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("PageSize")
        @Expose
        private Integer pageSize;
        @SerializedName("radius")
        @Expose
        private Integer radius;

        /**
         *
         * @return
         * The desID
         */
        public Integer getDesID() {
            return desID;
        }

        /**
         *
         * @param desID
         * The desID
         */
        public void setDesID(Integer desID) {
            this.desID = desID;
        }

        /**
         *
         * @return
         * The desCountry
         */
        public Integer getDesCountry() {
            return desCountry;
        }

        /**
         *
         * @param desCountry
         * The desCountry
         */
        public void setDesCountry(Integer desCountry) {
            this.desCountry = desCountry;
        }

        /**
         *
         * @return
         * The desState
         */
        public Integer getDesState() {
            return desState;
        }

        /**
         *
         * @param desState
         * The desState
         */
        public void setDesState(Integer desState) {
            this.desState = desState;
        }

        /**
         *
         * @return
         * The desDistrict
         */
        public Integer getDesDistrict() {
            return desDistrict;
        }

        /**
         *
         * @param desDistrict
         * The desDistrict
         */
        public void setDesDistrict(Integer desDistrict) {
            this.desDistrict = desDistrict;
        }

        /**
         *
         * @return
         * The desCity
         */
        public Integer getDesCity() {
            return desCity;
        }

        /**
         *
         * @param desCity
         * The desCity
         */
        public void setDesCity(Integer desCity) {
            this.desCity = desCity;
        }

        /**
         *
         * @return
         * The isActive
         */
        public Boolean getIsActive() {
            return isActive;
        }

        /**
         *
         * @param isActive
         * The IsActive
         */
        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        /**
         *
         * @return
         * The umId
         */
        public Integer getUmId() {
            return umId;
        }

        /**
         *
         * @param umId
         * The umId
         */
        public void setUmId(Integer umId) {
            this.umId = umId;
        }

        /**
         *
         * @return
         * The resStatus
         */
        public Boolean getResStatus() {
            return resStatus;
        }

        /**
         *
         * @param resStatus
         * The resStatus
         */
        public void setResStatus(Boolean resStatus) {
            this.resStatus = resStatus;
        }

        /**
         *
         * @return
         * The resCallerDetails
         */
        public String getResCallerDetails() {
            return resCallerDetails;
        }

        /**
         *
         * @param resCallerDetails
         * The resCallerDetails
         */
        public void setResCallerDetails(String resCallerDetails) {
            this.resCallerDetails = resCallerDetails;
        }

        /**
         *
         * @return
         * The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         *
         * @param pageNo
         * The PageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         *
         * @return
         * The pageSize
         */
        public Integer getPageSize() {
            return pageSize;
        }

        /**
         *
         * @param pageSize
         * The PageSize
         */
        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         *
         * @return
         * The radius
         */
        public Integer getRadius() {
            return radius;
        }

        /**
         *
         * @param radius
         * The radius
         */
        public void setRadius(Integer radius) {
            this.radius = radius;
        }

    }

    @Generated("org.jsonschema2pojo")
    public class GeneralFields {


        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("longi")
        @Expose
        private String longi;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("Image")
        @Expose
        private String image;
        @SerializedName("Side")
        @Expose
        private String side;
        @SerializedName("ShortDescription")
        @Expose
        private String shortDescription;
        @SerializedName("POIEndTime")
        @Expose
        private String pOIEndTime;
        @SerializedName("POIStartTime")
        @Expose
        private String pOIStartTime;
        @SerializedName("POIIsFullTime")
        @Expose
        private Boolean pOIIsFullTime;
        @SerializedName("SerCatFooterImageSelected")
        @Expose
        private String serCatFooterImageSelected;

        public String getSerCatFooterImageSelected() {
            return serCatFooterImageSelected;
        }

        public void setSerCatFooterImageSelected(String serCatFooterImageSelected) {
            this.serCatFooterImageSelected = serCatFooterImageSelected;
        }

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The Name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         * The longi
         */
        public String getLongi() {
            return longi;
        }

        /**
         *
         * @param longi
         * The longi
         */
        public void setLongi(String longi) {
            this.longi = longi;
        }

        /**
         *
         * @return
         * The lat
         */
        public String getLat() {
            return lat;
        }

        /**
         *
         * @param lat
         * The lat
         */
        public void setLat(String lat) {
            this.lat = lat;
        }

        /**
         *
         * @return
         * The image
         */
        public String getImage() {
            if(image == null)
            {
                return "";
            }
            else {
                return image;
            }
        }

        /**
         *
         * @param image
         * The Image
         */
        public void setImage(String image) {
            this.image = image;
        }

        /**
         *
         * @return
         * The side
         */
        public String getSide() {
            return side;
        }

        /**
         *
         * @param side
         * The Side
         */
        public void setSide(String side) {
            this.side = side;
        }

        /**
         *
         * @return
         * The shortDescription
         */
        public String getShortDescription() {
            return shortDescription;
        }

        /**
         *
         * @param shortDescription
         * The ShortDescription
         */
        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }
        /**
         *
         * @return
         * The pOIEndTime
         */
        public String getPOIEndTime() {
            return pOIEndTime;
        }

        /**
         *
         * @param pOIEndTime
         * The POIEndTime
         */
        public void setPOIEndTime(String pOIEndTime) {
            this.pOIEndTime = pOIEndTime;
        }

        /**
         *
         * @return
         * The pOIStartTime
         */
        public String getPOIStartTime() {
            return pOIStartTime;
        }

        /**
         *
         * @param pOIStartTime
         * The POIStartTime
         */
        public void setPOIStartTime(String pOIStartTime) {
            this.pOIStartTime = pOIStartTime;
        }

        /**
         *
         * @return
         * The pOIIsFullTime
         */
        public Boolean getPOIIsFullTime() {
            return pOIIsFullTime;
        }

        /**
         *
         * @param pOIIsFullTime
         * The POIIsFullTime
         */
        public void setPOIIsFullTime(Boolean pOIIsFullTime) {
            this.pOIIsFullTime = pOIIsFullTime;
        }
    }


    @Generated("org.jsonschema2pojo")
    public class GetTripPlan {

        @SerializedName("TripWayPointId")
        @Expose
        private Integer tripWayPointId;
        @SerializedName("TripWayType")
        @Expose
        private String tripWayType;
        @SerializedName("TripWayRefId")
        @Expose
        private Integer tripWayRefId;
        @SerializedName("TripWayCreatedDate")
        @Expose
        private String tripWayCreatedDate;
        @SerializedName("TripWayUpdateDate")
        @Expose
        private String tripWayUpdateDate;
        @SerializedName("TripWayTripid")
        @Expose
        private Integer tripWayTripid;
        @SerializedName("TripWayUmId")
        @Expose
        private Integer tripWayUmId;
        @SerializedName("TripWayComments")
        @Expose
        private String tripWayComments;
        @SerializedName("TripWayIsActive")
        @Expose
        private Boolean tripWayIsActive;
        @SerializedName("TripExtraKeys")
        @Expose
        private String tripExtraKeys;
        @SerializedName("TripEstInDateTime")
        @Expose
        private String tripEstInDateTime;
        @SerializedName("TripEstOutTime")
        @Expose
        private String tripEstOutTime;
        @SerializedName("TripDesId")
        @Expose
        private Integer tripDesId;
        @SerializedName("TripIsPause")
        @Expose
        private Boolean tripIsPause;
        @SerializedName("TripDistanceInKm")
        @Expose
        private Double tripDistanceInKm;
        @SerializedName("TripDistanceInMinute")
        @Expose
        private Double tripDistanceInMinute;
        @SerializedName("TripSpendTime")
        @Expose
        private Double tripSpendTime;
        @SerializedName("TripIsNightStayOn")
        @Expose
        private Boolean tripIsNightStayOn;
        @SerializedName("TripWayIsReturnPlanned")
        @Expose
        private Boolean tripWayIsReturnPlanned;
        @SerializedName("desID")
        @Expose
        private Integer desID;
        @SerializedName("desCountry")
        @Expose
        private Integer desCountry;
        @SerializedName("desState")
        @Expose
        private Integer desState;
        @SerializedName("desDistrict")
        @Expose
        private Integer desDistrict;
        @SerializedName("desCity")
        @Expose
        private Integer desCity;
        @SerializedName("desName")
        @Expose
        private String desName;
        @SerializedName("desLatitude")
        @Expose
        private String desLatitude;
        @SerializedName("desLongitude")
        @Expose
        private String desLongitude;
        @SerializedName("desImage")
        @Expose
        private String desImage;
        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;
        @SerializedName("desCreateDate")
        @Expose
        private String desCreateDate;
        @SerializedName("desRating")
        @Expose
        private String desRating;
        @SerializedName("desAddress1")
        @Expose
        private String desAddress1;
        @SerializedName("destBestTimeFrom")
        @Expose
        private String destBestTimeFrom;
        @SerializedName("desBestTimeTo")
        @Expose
        private String desBestTimeTo;
        @SerializedName("desShortDescription")
        @Expose
        private String desShortDescription;
        @SerializedName("desLongDescription")
        @Expose
        private String desLongDescription;
        @SerializedName("desFestival")
        @Expose
        private String desFestival;
        @SerializedName("desNoOfPOI")
        @Expose
        private Integer desNoOfPOI;
        @SerializedName("desNoOfVisited")
        @Expose
        private Integer desNoOfVisited;
        @SerializedName("desHostDetails")
        @Expose
        private String desHostDetails;
        @SerializedName("desNearByDestination")
        @Expose
        private String desNearByDestination;
        @SerializedName("desDistance")
        @Expose
        private String desDistance;
        @SerializedName("DestinatinObject")
        @Expose
        private DestinatinObject destinatinObject;
        @SerializedName("GeneralFields")
        @Expose
        private GeneralFields generalFields;
        @SerializedName("resStatus")
        @Expose
        private Boolean resStatus;
        @SerializedName("resCallerDetails")
        @Expose
        private String resCallerDetails;
        @SerializedName("PageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("PageSize")
        @Expose
        private Integer pageSize;
        @SerializedName("radius")
        @Expose
        private Integer radius;
        @SerializedName("TripResumeDateTime")
        @Expose
        private String tripResumeDateTime;
        @SerializedName("SerCategoryId")
        @Expose
        private Integer serCategoryId;

        @SerializedName("desNoOfNearByAttraction")
        @Expose
        private Integer desNoOfNearByAttraction;

        public String getTripLegNo() {
            return tripLegNo;
        }

        public void setTripLegNo(String tripLegNo) {
            this.tripLegNo = tripLegNo;
        }

        @SerializedName("TripLegNo")
        @Expose
        private String tripLegNo;

        public Integer getDesNoOfNearByAttraction() {
            return desNoOfNearByAttraction;
        }

        public void setDesNoOfNearByAttraction(Integer desNoOfNearByAttraction) {
            this.desNoOfNearByAttraction = desNoOfNearByAttraction;
        }

        /**
         *
         * @return
         * The serCategoryId
         */
        public Integer getSerCategoryId() {
            return serCategoryId;
        }

        /**
         *
         * @param serCategoryId
         * The SerCategoryId
         */
        public void setSerCategoryId(Integer serCategoryId) {
            this.serCategoryId = serCategoryId;
        }
        /**
         *
         * @return
         * The tripResumeDateTime
         */
        public String getTripResumeDateTime() {
            return tripResumeDateTime;
        }

        /**
         *
         * @param tripResumeDateTime
         * The TripResumeDateTime
         */
        public void setTripResumeDateTime(String tripResumeDateTime) {
            this.tripResumeDateTime = tripResumeDateTime;
        }

        /**
         *
         * @return
         * The tripWayPointId
         */
        public Integer getTripWayPointId() {
            return tripWayPointId;
        }

        /**
         *
         * @param tripWayPointId
         * The TripWayPointId
         */
        public void setTripWayPointId(Integer tripWayPointId) {
            this.tripWayPointId = tripWayPointId;
        }

        /**
         *
         * @return
         * The tripWayType
         */
        public String getTripWayType() {
            if(tripWayType==null)
            {
                return "";
            }else {
                return tripWayType;
            }
        }

        /**
         *
         * @param tripWayType
         * The TripWayType
         */
        public void setTripWayType(String tripWayType) {
            this.tripWayType = tripWayType;
        }

        /**
         *
         * @return
         * The tripWayRefId
         */
        public Integer getTripWayRefId() {
            return tripWayRefId;
        }

        /**
         *
         * @param tripWayRefId
         * The TripWayRefId
         */
        public void setTripWayRefId(Integer tripWayRefId) {
            this.tripWayRefId = tripWayRefId;
        }

        /**
         *
         * @return
         * The tripWayCreatedDate
         */
        public String getTripWayCreatedDate() {
            return tripWayCreatedDate;
        }

        /**
         *
         * @param tripWayCreatedDate
         * The TripWayCreatedDate
         */
        public void setTripWayCreatedDate(String tripWayCreatedDate) {
            this.tripWayCreatedDate = tripWayCreatedDate;
        }

        /**
         *
         * @return
         * The tripWayUpdateDate
         */
        public String getTripWayUpdateDate() {
            return tripWayUpdateDate;
        }

        /**
         *
         * @param tripWayUpdateDate
         * The TripWayUpdateDate
         */
        public void setTripWayUpdateDate(String tripWayUpdateDate) {
            this.tripWayUpdateDate = tripWayUpdateDate;
        }

        /**
         *
         * @return
         * The tripWayTripid
         */
        public Integer getTripWayTripid() {
            return tripWayTripid;
        }

        /**
         *
         * @param tripWayTripid
         * The TripWayTripid
         */
        public void setTripWayTripid(Integer tripWayTripid) {
            this.tripWayTripid = tripWayTripid;
        }

        /**
         *
         * @return
         * The tripWayUmId
         */
        public Integer getTripWayUmId() {
            return tripWayUmId;
        }

        /**
         *
         * @param tripWayUmId
         * The TripWayUmId
         */
        public void setTripWayUmId(Integer tripWayUmId) {
            this.tripWayUmId = tripWayUmId;
        }

        /**
         *
         * @return
         * The tripWayComments
         */
        public String getTripWayComments() {
            return tripWayComments;
        }

        /**
         *
         * @param tripWayComments
         * The TripWayComments
         */
        public void setTripWayComments(String tripWayComments) {
            this.tripWayComments = tripWayComments;
        }

        /**
         *
         * @return
         * The tripWayIsActive
         */
        public Boolean getTripWayIsActive() {
            return tripWayIsActive;
        }

        /**
         *
         * @param tripWayIsActive
         * The TripWayIsActive
         */
        public void setTripWayIsActive(Boolean tripWayIsActive) {
            this.tripWayIsActive = tripWayIsActive;
        }

        /**
         *
         * @return
         * The tripExtraKeys
         */
        public String getTripExtraKeys() {
            return tripExtraKeys;
        }

        /**
         *
         * @param tripExtraKeys
         * The TripExtraKeys
         */
        public void setTripExtraKeys(String tripExtraKeys) {
            this.tripExtraKeys = tripExtraKeys;
        }

        /**
         *
         * @return
         * The tripEstInDateTime
         */
        public String getTripEstInDateTime() {
            return tripEstInDateTime;
        }

        /**
         *
         * @param tripEstInDateTime
         * The TripEstInDateTime
         */
        public void setTripEstInDateTime(String tripEstInDateTime) {
            this.tripEstInDateTime = tripEstInDateTime;
        }

        /**
         *
         * @return
         * The tripEstOutTime
         */
        public String getTripEstOutTime() {
            return tripEstOutTime;
        }

        /**
         *
         * @param tripEstOutTime
         * The TripEstOutTime
         */
        public void setTripEstOutTime(String tripEstOutTime) {
            this.tripEstOutTime = tripEstOutTime;
        }

        /**
         *
         * @return
         * The tripDesId
         */
        public Integer getTripDesId() {
            return tripDesId;
        }

        /**
         *
         * @param tripDesId
         * The TripDesId
         */
        public void setTripDesId(Integer tripDesId) {
            this.tripDesId = tripDesId;
        }

        /**
         *
         * @return
         * The tripIsPause
         */
        public Boolean getTripIsPause() {
            return tripIsPause;
        }

        /**
         *
         * @param tripIsPause
         * The TripIsPause
         */
        public void setTripIsPause(Boolean tripIsPause) {
            this.tripIsPause = tripIsPause;
        }

        /**
         *
         * @return
         * The tripDistanceInKm
         */
        public Double getTripDistanceInKm() {
            return tripDistanceInKm;
        }

        /**
         *
         * @param tripDistanceInKm
         * The TripDistanceInKm
         */
        public void setTripDistanceInKm(Double tripDistanceInKm) {
            this.tripDistanceInKm = tripDistanceInKm;
        }

        /**
         *
         * @return
         * The tripDistanceInMinute
         */
        public Double getTripDistanceInMinute() {
            return tripDistanceInMinute;
        }

        /**
         *
         * @param tripDistanceInMinute
         * The TripDistanceInMinute
         */
        public void setTripDistanceInMinute(Double tripDistanceInMinute) {
            this.tripDistanceInMinute = tripDistanceInMinute;
        }

        /**
         *
         * @return
         * The tripSpendTime
         */
        public Double getTripSpendTime() {
            return tripSpendTime;
        }

        /**
         *
         * @param tripSpendTime
         * The TripSpendTime
         */
        public void setTripSpendTime(Double tripSpendTime) {
            this.tripSpendTime = tripSpendTime;
        }

        /**
         *
         * @return
         * The tripIsNightStayOn
         */
        public Boolean getTripIsNightStayOn() {
            return tripIsNightStayOn;
        }

        /**
         *
         * @param tripIsNightStayOn
         * The TripIsNightStayOn
         */
        public void setTripIsNightStayOn(Boolean tripIsNightStayOn) {
            this.tripIsNightStayOn = tripIsNightStayOn;
        }

        /**
         *
         * @return
         * The tripWayIsReturnPlanned
         */
        public Boolean getTripWayIsReturnPlanned() {
            return tripWayIsReturnPlanned;
        }

        /**
         *
         * @param tripWayIsReturnPlanned
         * The TripWayIsReturnPlanned
         */
        public void setTripWayIsReturnPlanned(Boolean tripWayIsReturnPlanned) {
            this.tripWayIsReturnPlanned = tripWayIsReturnPlanned;
        }

        /**
         *
         * @return
         * The desID
         */
        public Integer getDesID() {
            return desID;
        }

        /**
         *
         * @param desID
         * The desID
         */
        public void setDesID(Integer desID) {
            this.desID = desID;
        }

        /**
         *
         * @return
         * The desCountry
         */
        public Integer getDesCountry() {
            return desCountry;
        }

        /**
         *
         * @param desCountry
         * The desCountry
         */
        public void setDesCountry(Integer desCountry) {
            this.desCountry = desCountry;
        }

        /**
         *
         * @return
         * The desState
         */
        public Integer getDesState() {
            return desState;
        }

        /**
         *
         * @param desState
         * The desState
         */
        public void setDesState(Integer desState) {
            this.desState = desState;
        }

        /**
         *
         * @return
         * The desDistrict
         */
        public Integer getDesDistrict() {
            return desDistrict;
        }

        /**
         *
         * @param desDistrict
         * The desDistrict
         */
        public void setDesDistrict(Integer desDistrict) {
            this.desDistrict = desDistrict;
        }

        /**
         *
         * @return
         * The desCity
         */
        public Integer getDesCity() {
            return desCity;
        }

        /**
         *
         * @param desCity
         * The desCity
         */
        public void setDesCity(Integer desCity) {
            this.desCity = desCity;
        }

        /**
         *
         * @return
         * The desName
         */
        public String getDesName() {
            return desName;
        }

        /**
         *
         * @param desName
         * The desName
         */
        public void setDesName(String desName) {
            this.desName = desName;
        }

        /**
         *
         * @return
         * The desLatitude
         */
        public String getDesLatitude() {
            return desLatitude;
        }

        /**
         *
         * @param desLatitude
         * The desLatitude
         */
        public void setDesLatitude(String desLatitude) {
            this.desLatitude = desLatitude;
        }

        /**
         *
         * @return
         * The desLongitude
         */
        public String getDesLongitude() {
            return desLongitude;
        }

        /**
         *
         * @param desLongitude
         * The desLongitude
         */
        public void setDesLongitude(String desLongitude) {
            this.desLongitude = desLongitude;
        }

        /**
         *
         * @return
         * The desImage
         */
        public String getDesImage() {
            return desImage;
        }

        /**
         *
         * @param desImage
         * The desImage
         */
        public void setDesImage(String desImage) {
            this.desImage = desImage;
        }

        /**
         *
         * @return
         * The isActive
         */
        public Boolean getIsActive() {
            return isActive;
        }

        /**
         *
         * @param isActive
         * The IsActive
         */
        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        /**
         *
         * @return
         * The desCreateDate
         */
        public String getDesCreateDate() {
            return desCreateDate;
        }

        /**
         *
         * @param desCreateDate
         * The desCreateDate
         */
        public void setDesCreateDate(String desCreateDate) {
            this.desCreateDate = desCreateDate;
        }

        /**
         *
         * @return
         * The desRating
         */
        public String getDesRating() {
            return desRating;
        }

        /**
         *
         * @param desRating
         * The desRating
         */
        public void setDesRating(String desRating) {
            this.desRating = desRating;
        }

        /**
         *
         * @return
         * The desAddress1
         */
        public String getDesAddress1() {
            return desAddress1;
        }

        /**
         *
         * @param desAddress1
         * The desAddress1
         */
        public void setDesAddress1(String desAddress1) {
            this.desAddress1 = desAddress1;
        }

        /**
         *
         * @return
         * The destBestTimeFrom
         */
        public String getDestBestTimeFrom() {
            return destBestTimeFrom;
        }

        /**
         *
         * @param destBestTimeFrom
         * The destBestTimeFrom
         */
        public void setDestBestTimeFrom(String destBestTimeFrom) {
            this.destBestTimeFrom = destBestTimeFrom;
        }

        /**
         *
         * @return
         * The desBestTimeTo
         */
        public String getDesBestTimeTo() {
            return desBestTimeTo;
        }

        /**
         *
         * @param desBestTimeTo
         * The desBestTimeTo
         */
        public void setDesBestTimeTo(String desBestTimeTo) {
            this.desBestTimeTo = desBestTimeTo;
        }

        /**
         *
         * @return
         * The desShortDescription
         */
        public String getDesShortDescription() {
            return desShortDescription;
        }

        /**
         *
         * @param desShortDescription
         * The desShortDescription
         */
        public void setDesShortDescription(String desShortDescription) {
            this.desShortDescription = desShortDescription;
        }

        /**
         *
         * @return
         * The desLongDescription
         */
        public String getDesLongDescription() {
            return desLongDescription;
        }

        /**
         *
         * @param desLongDescription
         * The desLongDescription
         */
        public void setDesLongDescription(String desLongDescription) {
            this.desLongDescription = desLongDescription;
        }

        /**
         *
         * @return
         * The desFestival
         */
        public String getDesFestival() {
            return desFestival;
        }

        /**
         *
         * @param desFestival
         * The desFestival
         */
        public void setDesFestival(String desFestival) {
            this.desFestival = desFestival;
        }

        /**
         *
         * @return
         * The desNoOfPOI
         */
        public Integer getDesNoOfPOI() {
            return desNoOfPOI;
        }

        /**
         *
         * @param desNoOfPOI
         * The desNoOfPOI
         */
        public void setDesNoOfPOI(Integer desNoOfPOI) {
            this.desNoOfPOI = desNoOfPOI;
        }

        /**
         *
         * @return
         * The desNoOfVisited
         */
        public Integer getDesNoOfVisited() {
            return desNoOfVisited;
        }

        /**
         *
         * @param desNoOfVisited
         * The desNoOfVisited
         */
        public void setDesNoOfVisited(Integer desNoOfVisited) {
            this.desNoOfVisited = desNoOfVisited;
        }

        /**
         *
         * @return
         * The desHostDetails
         */
        public String getDesHostDetails() {
            return desHostDetails;
        }

        /**
         *
         * @param desHostDetails
         * The desHostDetails
         */
        public void setDesHostDetails(String desHostDetails) {
            this.desHostDetails = desHostDetails;
        }

        /**
         *
         * @return
         * The desNearByDestination
         */
        public String getDesNearByDestination() {
            return desNearByDestination;
        }

        /**
         *
         * @param desNearByDestination
         * The desNearByDestination
         */
        public void setDesNearByDestination(String desNearByDestination) {
            this.desNearByDestination = desNearByDestination;
        }

        /**
         *
         * @return
         * The desDistance
         */
        public String getDesDistance() {
            return desDistance;
        }

        /**
         *
         * @param desDistance
         * The desDistance
         */
        public void setDesDistance(String desDistance) {
            this.desDistance = desDistance;
        }

        /**
         *
         * @return
         * The destinatinObject
         */
        public DestinatinObject getDestinatinObject() {
            return destinatinObject;
        }

        /**
         *
         * @param destinatinObject
         * The DestinatinObject
         */
        public void setDestinatinObject(DestinatinObject destinatinObject) {
            this.destinatinObject = destinatinObject;
        }

        /**
         *
         * @return
         * The generalFields
         */
        public GeneralFields getGeneralFields() {
            return generalFields;
        }

        /**
         *
         * @param generalFields
         * The GeneralFields
         */
        public void setGeneralFields(GeneralFields generalFields) {
            this.generalFields = generalFields;
        }

        /**
         *
         * @return
         * The resStatus
         */
        public Boolean getResStatus() {
            return resStatus;
        }

        /**
         *
         * @param resStatus
         * The resStatus
         */
        public void setResStatus(Boolean resStatus) {
            this.resStatus = resStatus;
        }

        /**
         *
         * @return
         * The resCallerDetails
         */
        public String getResCallerDetails() {
            return resCallerDetails;
        }

        /**
         *
         * @param resCallerDetails
         * The resCallerDetails
         */
        public void setResCallerDetails(String resCallerDetails) {
            this.resCallerDetails = resCallerDetails;
        }

        /**
         *
         * @return
         * The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         *
         * @param pageNo
         * The PageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         *
         * @return
         * The pageSize
         */
        public Integer getPageSize() {
            return pageSize;
        }

        /**
         *
         * @param pageSize
         * The PageSize
         */
        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         *
         * @return
         * The radius
         */
        public Integer getRadius() {
            return radius;
        }

        /**
         *
         * @param radius
         * The radius
         */
        public void setRadius(Integer radius) {
            this.radius = radius;
        }

    }

    public class ImageListByDestinationId {

        @SerializedName("TotalImage")
        @Expose
        private Integer totalImage;
        @SerializedName("ImgId")
        @Expose
        private Integer imgId;
        @SerializedName("ImgType")
        @Expose
        private String imgType;
        @SerializedName("ImgName")
        @Expose
        private String imgName;
        @SerializedName("ImgCreatedDate")
        @Expose
        private String imgCreatedDate;
        @SerializedName("ImgRefId")
        @Expose
        private Integer imgRefId;
        @SerializedName("ImgDescription")
        @Expose
        private String imgDescription;
        @SerializedName("ImgIsActive")
        @Expose
        private Boolean imgIsActive;

        /**
         *
         * @return
         * The totalImage
         */
        public Integer getTotalImage() {
            return totalImage;
        }

        /**
         *
         * @param totalImage
         * The TotalImage
         */
        public void setTotalImage(Integer totalImage) {
            this.totalImage = totalImage;
        }

        /**
         *
         * @return
         * The imgId
         */
        public Integer getImgId() {
            return imgId;
        }

        /**
         *
         * @param imgId
         * The ImgId
         */
        public void setImgId(Integer imgId) {
            this.imgId = imgId;
        }

        /**
         *
         * @return
         * The imgType
         */
        public String getImgType() {
            return imgType;
        }

        /**
         *
         * @param imgType
         * The ImgType
         */
        public void setImgType(String imgType) {
            this.imgType = imgType;
        }

        /**
         *
         * @return
         * The imgName
         */
        public String getImgName() {
            return imgName;
        }

        /**
         *
         * @param imgName
         * The ImgName
         */
        public void setImgName(String imgName) {
            this.imgName = imgName;
        }

        /**
         *
         * @return
         * The imgCreatedDate
         */
        public String getImgCreatedDate() {
            return imgCreatedDate;
        }

        /**
         *
         * @param imgCreatedDate
         * The ImgCreatedDate
         */
        public void setImgCreatedDate(String imgCreatedDate) {
            this.imgCreatedDate = imgCreatedDate;
        }

        /**
         *
         * @return
         * The imgRefId
         */
        public Integer getImgRefId() {
            return imgRefId;
        }

        /**
         *
         * @param imgRefId
         * The ImgRefId
         */
        public void setImgRefId(Integer imgRefId) {
            this.imgRefId = imgRefId;
        }

        /**
         *
         * @return
         * The imgDescription
         */
        public String getImgDescription() {
            return imgDescription;
        }

        /**
         *
         * @param imgDescription
         * The ImgDescription
         */
        public void setImgDescription(String imgDescription) {
            this.imgDescription = imgDescription;
        }

        /**
         *
         * @return
         * The imgIsActive
         */
        public Boolean getImgIsActive() {
            return imgIsActive;
        }

        /**
         *
         * @param imgIsActive
         * The ImgIsActive
         */
        public void setImgIsActive(Boolean imgIsActive) {
            this.imgIsActive = imgIsActive;
        }

    }

    @Generated("org.jsonschema2pojo")
    public class ImageListByOriginId {

        @SerializedName("TotalImage")
        @Expose
        private Integer totalImage;
        @SerializedName("ImgId")
        @Expose
        private Integer imgId;
        @SerializedName("ImgType")
        @Expose
        private String imgType;
        @SerializedName("ImgName")
        @Expose
        private String imgName;
        @SerializedName("ImgCreatedDate")
        @Expose
        private String imgCreatedDate;
        @SerializedName("ImgRefId")
        @Expose
        private Integer imgRefId;
        @SerializedName("ImgDescription")
        @Expose
        private String imgDescription;
        @SerializedName("ImgIsActive")
        @Expose
        private Boolean imgIsActive;



        /**
         *
         * @return
         * The totalImage
         */
        public Integer getTotalImage() {
            return totalImage;
        }

        /**
         *
         * @param totalImage
         * The TotalImage
         */
        public void setTotalImage(Integer totalImage) {
            this.totalImage = totalImage;
        }

        /**
         *
         * @return
         * The imgId
         */
        public Integer getImgId() {
            return imgId;
        }

        /**
         *
         * @param imgId
         * The ImgId
         */
        public void setImgId(Integer imgId) {
            this.imgId = imgId;
        }

        /**
         *
         * @return
         * The imgType
         */
        public String getImgType() {
            return imgType;
        }

        /**
         *
         * @param imgType
         * The ImgType
         */
        public void setImgType(String imgType) {
            this.imgType = imgType;
        }

        /**
         *
         * @return
         * The imgName
         */
        public String getImgName() {
            return imgName;
        }

        /**
         *
         * @param imgName
         * The ImgName
         */
        public void setImgName(String imgName) {
            this.imgName = imgName;
        }

        /**
         *
         * @return
         * The imgCreatedDate
         */
        public String getImgCreatedDate() {
            return imgCreatedDate;
        }

        /**
         *
         * @param imgCreatedDate
         * The ImgCreatedDate
         */
        public void setImgCreatedDate(String imgCreatedDate) {
            this.imgCreatedDate = imgCreatedDate;
        }

        /**
         *
         * @return
         * The imgRefId
         */
        public Integer getImgRefId() {
            return imgRefId;
        }

        /**
         *
         * @param imgRefId
         * The ImgRefId
         */
        public void setImgRefId(Integer imgRefId) {
            this.imgRefId = imgRefId;
        }

        /**
         *
         * @return
         * The imgDescription
         */
        public String getImgDescription() {
            return imgDescription;
        }

        /**
         *
         * @param imgDescription
         * The ImgDescription
         */
        public void setImgDescription(String imgDescription) {
            this.imgDescription = imgDescription;
        }

        /**
         *
         * @return
         * The imgIsActive
         */
        public Boolean getImgIsActive() {
            return imgIsActive;
        }

        /**
         *
         * @param imgIsActive
         * The ImgIsActive
         */
        public void setImgIsActive(Boolean imgIsActive) {
            this.imgIsActive = imgIsActive;
        }

    }
}