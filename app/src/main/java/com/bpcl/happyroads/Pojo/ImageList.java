package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ADMIN on 9/29/2016.
 */
public class ImageList implements Serializable{

    @SerializedName("TotalImage")
    @Expose
    private Integer totalImage;
    @SerializedName("ImgId")
    @Expose
    private Integer imgId;
    @SerializedName("ImgType")
    @Expose
    private String imgType;
    @SerializedName("ImgCreatedDate")
    @Expose
    private String imgCreatedDate;
    @SerializedName("ImgRefId")
    @Expose
    private Integer imgRefId;
    @SerializedName("ImgDescription")
    @Expose
    private String imgDescription;
    @SerializedName("ImgIsActive")
    @Expose
    private Boolean imgIsActive;
    @SerializedName("ImgName")
    @Expose
    private String imgName;

    @SerializedName("ImgCredits")
    @Expose
    private String ImgCredits;

    @SerializedName("ImgPhotoLink")
    @Expose
    private String ImgPhotoLink;

    @SerializedName("ImgLicenseCCBY")
    @Expose
    private String ImgLicenseCCBY;

    @SerializedName("ImgLicenseCCSA")
    @Expose
    private String ImgLicenseCCSA;

    public String getImgLicenseCCSA() {
        if(ImgLicenseCCSA==null)
        {
            return "";
        }else {
            return ImgLicenseCCSA;
        }
    }

    public void setImgLicenseCCSA(String imgLicenseCCSA) {
        ImgLicenseCCSA = imgLicenseCCSA;
    }

    public String getImgCredits() {
        if(ImgCredits==null)
        {
            return "";
        }else {
            return ImgCredits;
        }
    }

    public void setImgCredits(String imgCredits) {
        ImgCredits = imgCredits;
    }

    public String getImgPhotoLink() {
        return ImgPhotoLink;
    }

    public void setImgPhotoLink(String imgPhotoLink) {
        ImgPhotoLink = imgPhotoLink;
    }

    public String getImgLicenseCCBY() {
        if(ImgLicenseCCBY==null)
        {
            return "";
        }else {
            return ImgLicenseCCBY;
        }
    }

    public void setImgLicenseCCBY(String imgLicenseCCBY) {
        ImgLicenseCCBY = imgLicenseCCBY;
    }



    /**
     *
     * @return
     * The totalImage
     */
    public Integer getTotalImage() {
        return totalImage;
    }

    /**
     *
     * @param totalImage
     * The TotalImage
     */
    public void setTotalImage(Integer totalImage) {
        this.totalImage = totalImage;
    }

    /**
     *
     * @return
     * The imgId
     */
    public Integer getImgId() {
        return imgId;
    }

    /**
     *
     * @param imgId
     * The ImgId
     */
    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    /**
     *
     * @return
     * The imgType
     */
    public String getImgType() {
        return imgType;
    }

    /**
     *
     * @param imgType
     * The ImgType
     */
    public void setImgType(String imgType) {
        this.imgType = imgType;
    }

    /**
     *
     * @return
     * The imgCreatedDate
     */
    public String getImgCreatedDate() {
        return imgCreatedDate;
    }

    /**
     *
     * @param imgCreatedDate
     * The ImgCreatedDate
     */
    public void setImgCreatedDate(String imgCreatedDate) {
        this.imgCreatedDate = imgCreatedDate;
    }

    /**
     *
     * @return
     * The imgRefId
     */
    public Integer getImgRefId() {
        return imgRefId;
    }

    /**
     *
     * @param imgRefId
     * The ImgRefId
     */
    public void setImgRefId(Integer imgRefId) {
        this.imgRefId = imgRefId;
    }

    /**
     *
     * @return
     * The imgDescription
     */
    public String getImgDescription() {
        return imgDescription;
    }

    /**
     *
     * @param imgDescription
     * The ImgDescription
     */
    public void setImgDescription(String imgDescription) {
        this.imgDescription = imgDescription;
    }

    /**
     *
     * @return
     * The imgIsActive
     */
    public Boolean getImgIsActive() {
        return imgIsActive;
    }

    /**
     *
     * @param imgIsActive
     * The ImgIsActive
     */
    public void setImgIsActive(Boolean imgIsActive) {
        this.imgIsActive = imgIsActive;
    }

    /**
     *
     * @return
     * The imgName
     */
    public String getImgName() {
        return imgName;
    }

    /**
     *
     * @param imgName
     * The ImgName
     */
    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

}