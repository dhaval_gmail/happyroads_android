package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADMIN on 9/28/2016.
 */
public class ExploreDestination implements Serializable{

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;

    public Integer getDesNoOfNearByAttraction() {
        if(desNoOfNearByAttraction==null)
        {
            return 0;
        }else {
            return desNoOfNearByAttraction;
        }
    }

    public void setDesNoOfNearByAttraction(Integer desNoOfNearByAttraction) {
        this.desNoOfNearByAttraction = desNoOfNearByAttraction;
    }

    @SerializedName("desNoOfNearByAttraction")
    @Expose
    private Integer desNoOfNearByAttraction;
    @SerializedName("distance")
    @Expose
    private double distance;
    @SerializedName("desID")
    @Expose
    private Integer desID;
    @SerializedName("desCountry")
    @Expose
    private Integer desCountry;
    @SerializedName("desState")
    @Expose
    private Integer desState;
    @SerializedName("desDistrict")
    @Expose
    private Integer desDistrict;
    @SerializedName("desCity")
    @Expose
    private Integer desCity;
    @SerializedName("desName")
    @Expose
    private String desName;
    @SerializedName("desLatitude")
    @Expose
    private String desLatitude;
    @SerializedName("desLongitude")
    @Expose
    private String desLongitude;
    @SerializedName("desImage")
    @Expose
    private String desImage;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("desCreateDate")
    @Expose
    private String desCreateDate;
    @SerializedName("desRating")
    @Expose
    private String desRating;
    @SerializedName("desAddress1")
    @Expose
    private String desAddress1;
    @SerializedName("desAddress2")
    @Expose
    private String desAddress2;
    @SerializedName("destBestTimeFrom")
    @Expose
    private String destBestTimeFrom;
    @SerializedName("desBestTimeTo")
    @Expose
    private String desBestTimeTo;
    @SerializedName("desShortDescription")
    @Expose
    private String desShortDescription;
    @SerializedName("desLongDescription")
    @Expose
    private String desLongDescription;
    @SerializedName("desFestival")
    @Expose
    private String desFestival;
    @SerializedName("desNoOfPOI")
    @Expose
    private Integer desNoOfPOI;
    @SerializedName("desNoOfVisited")
    @Expose
    private Integer desNoOfVisited;
    @SerializedName("desHostDetails")
    @Expose
    private String desHostDetails;
    @SerializedName("desNearByDestination")
    @Expose
    private String desNearByDestination;
    @SerializedName("desNoOfNearByDestination")
    @Expose
    private Integer desNoOfNearByDestination;
    @SerializedName("desDistance")
    @Expose
    private String desDistance;
    @SerializedName("ImageListJson")
    @Expose
    private String imageListJson;
    @SerializedName("CityName")
    @Expose
    private String cityName;
    @SerializedName("ImageList")
    @Expose
    private List<ImageList> imageList = new ArrayList<ImageList>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("resDescription")
    @Expose
    private String resDescription;

    public Integer getFavId() {
        if(FavId==null)
        {
            return 0;
        }else {
            return FavId;
        }



    }

    public void setFavId(Integer favId) {
        FavId = favId;
    }

    @SerializedName("FavId")
    @Expose
    private Integer FavId;

    @SerializedName("duration")
    @Expose
    private Long duration;

    @SerializedName("TravelTypelList")
    @Expose
    private List<TravelTypelList> travelTypelList = new ArrayList<TravelTypelList>();

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<TravelTypelList> getTravelTypelList() {
        return travelTypelList;
    }

    public void setTravelTypelList(List<TravelTypelList> travelTypelList) {
        this.travelTypelList = travelTypelList;
    }

    /**
     *
     * @return
     * The rowNum
     */
    public Integer getRowNum() {
        return rowNum;
    }

    /**
     *
     * @param rowNum
     * The RowNum
     */
    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    /**
     *
     * @return
     * The distance
     */
    public double getDistance() {

        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The desID
     */
    public Integer getDesID() {
        return desID;
    }

    /**
     *
     * @param desID
     * The desID
     */
    public void setDesID(Integer desID) {
        this.desID = desID;
    }

    /**
     *
     * @return
     * The desCountry
     */
    public Integer getDesCountry() {
        return desCountry;
    }

    /**
     *
     * @param desCountry
     * The desCountry
     */
    public void setDesCountry(Integer desCountry) {
        this.desCountry = desCountry;
    }

    /**
     *
     * @return
     * The desState
     */
    public Integer getDesState() {
        return desState;
    }

    /**
     *
     * @param desState
     * The desState
     */
    public void setDesState(Integer desState) {
        this.desState = desState;
    }

    /**
     *
     * @return
     * The desDistrict
     */
    public Integer getDesDistrict() {
        return desDistrict;
    }

    /**
     *
     * @param desDistrict
     * The desDistrict
     */
    public void setDesDistrict(Integer desDistrict) {
        this.desDistrict = desDistrict;
    }

    /**
     *
     * @return
     * The desCity
     */
    public Integer getDesCity() {
        return desCity;
    }

    /**
     *
     * @param desCity
     * The desCity
     */
    public void setDesCity(Integer desCity) {
        this.desCity = desCity;
    }

    /**
     *
     * @return
     * The desName
     */
    public String getDesName() {
        return desName;
    }

    /**
     *
     * @param desName
     * The desName
     */
    public void setDesName(String desName) {
        this.desName = desName;
    }

    /**
     *
     * @return
     * The desLatitude
     */
    public String getDesLatitude() {
        return desLatitude;
    }

    /**
     *
     * @param desLatitude
     * The desLatitude
     */
    public void setDesLatitude(String desLatitude) {
        this.desLatitude = desLatitude;
    }

    /**
     *
     * @return
     * The desLongitude
     */
    public String getDesLongitude() {
        return desLongitude;
    }

    /**
     *
     * @param desLongitude
     * The desLongitude
     */
    public void setDesLongitude(String desLongitude) {
        this.desLongitude = desLongitude;
    }

    /**
     *
     * @return
     * The desImage
     */
    public String getDesImage() {
        return desImage;
    }

    /**
     *
     * @param desImage
     * The desImage
     */
    public void setDesImage(String desImage) {
        this.desImage = desImage;
    }

    /**
     *
     * @return
     * The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     *
     * @return
     * The desCreateDate
     */
    public String getDesCreateDate() {
        return desCreateDate;
    }

    /**
     *
     * @param desCreateDate
     * The desCreateDate
     */
    public void setDesCreateDate(String desCreateDate) {
        this.desCreateDate = desCreateDate;
    }

    /**
     *
     * @return
     * The desRating
     */
    public String getDesRating() {
        return desRating;
    }

    /**
     *
     * @param desRating
     * The desRating
     */
    public void setDesRating(String desRating) {
        this.desRating = desRating;
    }

    /**
     *
     * @return
     * The desAddress1
     */
    public String getDesAddress1() {
        return desAddress1;
    }

    /**
     *
     * @param desAddress1
     * The desAddress1
     */
    public void setDesAddress1(String desAddress1) {
        this.desAddress1 = desAddress1;
    }

    /**
     *
     * @return
     * The desAddress2
     */
    public String getDesAddress2() {
        return desAddress2;
    }

    /**
     *
     * @param desAddress2
     * The desAddress2
     */
    public void setDesAddress2(String desAddress2) {
        this.desAddress2 = desAddress2;
    }

    /**
     *
     * @return
     * The destBestTimeFrom
     */
    public String getDestBestTimeFrom() {
        return destBestTimeFrom;
    }

    /**
     *
     * @param destBestTimeFrom
     * The destBestTimeFrom
     */
    public void setDestBestTimeFrom(String destBestTimeFrom) {
        this.destBestTimeFrom = destBestTimeFrom;
    }

    /**
     *
     * @return
     * The desBestTimeTo
     */
    public String getDesBestTimeTo() {
        return desBestTimeTo;
    }

    /**
     *
     * @param desBestTimeTo
     * The desBestTimeTo
     */
    public void setDesBestTimeTo(String desBestTimeTo) {
        this.desBestTimeTo = desBestTimeTo;
    }

    /**
     *
     * @return
     * The desShortDescription
     */
    public String getDesShortDescription() {
        return desShortDescription;
    }

    /**
     *
     * @param desShortDescription
     * The desShortDescription
     */
    public void setDesShortDescription(String desShortDescription) {
        this.desShortDescription = desShortDescription;
    }

    /**
     *
     * @return
     * The desLongDescription
     */
    public String getDesLongDescription() {
        if(desLongDescription==null)
        {
            return "";

        }else
        {
            return desLongDescription;
        }
    }

    /**
     *
     * @param desLongDescription
     * The desLongDescription
     */
    public void setDesLongDescription(String desLongDescription) {
        this.desLongDescription = desLongDescription;
    }

    /**
     *
     * @return
     * The desFestival
     */
    public String getDesFestival() {
        if(desFestival == null)
        {
         return "";
        }
        else {
            return desFestival;
        }
    }

    /**
     *
     * @param desFestival
     * The desFestival
     */
    public void setDesFestival(String desFestival) {
        this.desFestival = desFestival;
    }

    /**
     *
     * @return
     * The desNoOfPOI
     */
    public Integer getDesNoOfPOI() {
        if(desNoOfPOI==null)
        {
            desNoOfPOI=0;

        }else
        {
            return desNoOfPOI;
        }
        return desNoOfPOI;

    }

    /**
     *
     * @param desNoOfPOI
     * The desNoOfPOI
     */
    public void setDesNoOfPOI(Integer desNoOfPOI) {
        this.desNoOfPOI = desNoOfPOI;
    }

    /**
     *
     * @return
     * The desNoOfVisited
     */
    public Integer getDesNoOfVisited() {
        return desNoOfVisited;
    }

    /**
     *
     * @param desNoOfVisited
     * The desNoOfVisited
     */
    public void setDesNoOfVisited(Integer desNoOfVisited) {
        this.desNoOfVisited = desNoOfVisited;
    }

    /**
     *
     * @return
     * The desHostDetails
     */
    public String getDesHostDetails() {
        return desHostDetails;
    }

    /**
     *
     * @param desHostDetails
     * The desHostDetails
     */
    public void setDesHostDetails(String desHostDetails) {
        this.desHostDetails = desHostDetails;
    }

    /**
     *
     * @return
     * The desNearByDestination
     */
    public String getDesNearByDestination() {
        return desNearByDestination;
    }

    /**
     *
     * @param desNearByDestination
     * The desNearByDestination
     */
    public void setDesNearByDestination(String desNearByDestination) {
        this.desNearByDestination = desNearByDestination;
    }

    /**
     *
     * @return
     * The desNoOfNearByDestination
     */
    public Integer getDesNoOfNearByDestination() {
        if(desNoOfNearByDestination==null)
        {
            return 0;
        }else {
            return desNoOfNearByDestination;
        }
    }

    /**
     *
     * @param desNoOfNearByDestination
     * The desNoOfNearByDestination
     */
    public void setDesNoOfNearByDestination(Integer desNoOfNearByDestination) {
        this.desNoOfNearByDestination = desNoOfNearByDestination;
    }

    /**
     *
     * @return
     * The desDistance
     */
    public String getDesDistance() {
        if(desDistance.equalsIgnoreCase("0") || desDistance.equalsIgnoreCase("")||desDistance==null)
        {
            return "0.0";
        }else {
            return desDistance;
        }
    }

    /**
     *
     * @param desDistance
     * The desDistance
     */
    public void setDesDistance(String desDistance) {
        this.desDistance = desDistance;
    }

    /**
     *
     * @return
     * The imageListJson
     */
    public String getImageListJson() {
        return imageListJson;
    }

    /**
     *
     * @param imageListJson
     * The ImageListJson
     */
    public void setImageListJson(String imageListJson) {
        this.imageListJson = imageListJson;
    }

    /**
     *
     * @return
     * The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     *
     * @param cityName
     * The CityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     *
     * @return
     * The imageList
     */
    public List<ImageList> getImageList() {
        return imageList;
    }

    /**
     *
     * @param imageList
     * The ImageList
     */
    public void setImageList(List<ImageList> imageList) {
        this.imageList = imageList;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }
    public String getResDescription() {
        return resDescription;
    }

    public void setResDescription(String resDescription) {
        this.resDescription = resDescription;
    }
}



