package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ADMIN on 9/29/2016.
 */
public class TravelTypelList implements Serializable{

    @SerializedName("TTypeId")
    @Expose
    private Integer tTypeId;
    @SerializedName("TTypeName")
    @Expose
    private String tTypeName;
    @SerializedName("TTypeDescription")
    @Expose
    private String tTypeDescription;
    @SerializedName("TTypeImage")
    @Expose
    private String tTypeImage;
    @SerializedName("TTypeCreateDate")
    @Expose
    private String tTypeCreateDate;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("TTSequenceId")
    @Expose
    private Integer tTSequenceId;

    /**
     *
     * @return
     * The tTypeId
     */
    public Integer getTTypeId() {
        return tTypeId;
    }

    /**
     *
     * @param tTypeId
     * The TTypeId
     */
    public void setTTypeId(Integer tTypeId) {
        this.tTypeId = tTypeId;
    }

    /**
     *
     * @return
     * The tTypeName
     */
    public String getTTypeName() {
        return tTypeName;
    }

    /**
     *
     * @param tTypeName
     * The TTypeName
     */
    public void setTTypeName(String tTypeName) {
        this.tTypeName = tTypeName;
    }

    /**
     *
     * @return
     * The tTypeDescription
     */
    public String getTTypeDescription() {
        return tTypeDescription;
    }

    /**
     *
     * @param tTypeDescription
     * The TTypeDescription
     */
    public void setTTypeDescription(String tTypeDescription) {
        this.tTypeDescription = tTypeDescription;
    }

    /**
     *
     * @return
     * The tTypeImage
     */
    public String getTTypeImage() {
        return tTypeImage;
    }

    /**
     *
     * @param tTypeImage
     * The TTypeImage
     */
    public void setTTypeImage(String tTypeImage) {
        this.tTypeImage = tTypeImage;
    }

    /**
     *
     * @return
     * The tTypeCreateDate
     */
    public String getTTypeCreateDate() {
        return tTypeCreateDate;
    }

    /**
     *
     * @param tTypeCreateDate
     * The TTypeCreateDate
     */
    public void setTTypeCreateDate(String tTypeCreateDate) {
        this.tTypeCreateDate = tTypeCreateDate;
    }

    /**
     *
     * @return
     * The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     *
     * @return
     * The tTSequenceId
     */
    public Integer getTTSequenceId() {
        return tTSequenceId;
    }

    /**
     *
     * @param tTSequenceId
     * The TTSequenceId
     */
    public void setTTSequenceId(Integer tTSequenceId) {
        this.tTSequenceId = tTSequenceId;
    }

}
