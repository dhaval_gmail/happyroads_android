package com.bpcl.happyroads.Pojo;

/**
 * Created by dhavalkaka on 08/11/2016.
 */
public class DestinationPlan {

    Integer id,tripId,desId,WayPointId,poiDesID,poiServerId;
    String poiName,poiDate,poitype,poidescription;
            Double starttime,endtime;
    Double stayTime,poiLatitude,poiLongitude,arrivalTime,departureTime;
    byte[] poiImage;
    int pos;





    Double KM,time,reqtime;


    String imagename;

    Integer day;
    Double daystartime;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public Double getDaystartime() {
        return daystartime;
    }

    public void setDaystartime(Double daystartime) {
        this.daystartime = daystartime;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public Integer getDesId() {
        return desId;
    }

    public void setDesId(Integer desId) {
        this.desId = desId;
    }

    public Integer getPoiDesID() {
        return poiDesID;
    }

    public void setPoiDesID(Integer poiDesID) {
        this.poiDesID = poiDesID;
    }

    public Integer getWayPointId() {
        return WayPointId;
    }

    public void setWayPointId(Integer wayPointId) {
        WayPointId = wayPointId;
    }

    public Integer getPoiServerId() {
        return poiServerId;
    }

    public void setPoiServerId(Integer poiServerId) {
        this.poiServerId = poiServerId;
    }

    public String getPoiName() {
        return poiName;
    }

    public void setPoiName(String poiName) {
        this.poiName = poiName;
    }

    public String getPoitype() {
        return poitype;
    }

    public void setPoitype(String poitype) {
        this.poitype = poitype;
    }

    public String getPoiDate() {
        return poiDate;
    }

    public void setPoiDate(String poiDate) {
        this.poiDate = poiDate;
    }

    public String getPoidescription() {
        return poidescription;
    }

    public void setPoidescription(String poidescription) {
        this.poidescription = poidescription;
    }

    public Double getStarttime() {
        return starttime;
    }

    public void setStarttime(Double starttime) {
        this.starttime = starttime;
    }

    public Double getStayTime() {
        return stayTime;
    }

    public void setStayTime(Double stayTime) {
        this.stayTime = stayTime;
    }

    public Double getEndtime() {
        return endtime;
    }

    public void setEndtime(Double endtime) {
        this.endtime = endtime;
    }

    public Double getPoiLongitude() {
        return poiLongitude;
    }

    public void setPoiLongitude(Double poiLongitude) {
        this.poiLongitude = poiLongitude;
    }

    public Double getPoiLatitude() {
        return poiLatitude;
    }

    public void setPoiLatitude(Double poiLatitude) {
        this.poiLatitude = poiLatitude;
    }

    public Double getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Double getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Double departureTime) {
        this.departureTime = departureTime;
    }

    public byte[] getPoiImage() {
        return poiImage;
    }

    public void setPoiImage(byte[] poiImage) {
        this.poiImage = poiImage;
    }

    public Double getKM() {
        return KM;
    }

    public void setKM(Double KM) {
        this.KM = KM;
    }

    public Double getReqtime() {
        return reqtime;
    }

    public void setReqtime(Double reqtime) {
        this.reqtime = reqtime;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }



    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }
}
