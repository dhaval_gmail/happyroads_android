package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 10/13/2016.
 */
public class DeleteVehicle {

    @SerializedName("vehicleId")
    @Expose
    private Integer vehicleId;
    @SerializedName("vehicleRegNumber")
    @Expose
    private String vehicleRegNumber;
    @SerializedName("vehicleMake")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicleModel")
    @Expose
    private String vehicleModel;
    @SerializedName("vehicleYear")
    @Expose
    private String vehicleYear;
    @SerializedName("vehicleColor")
    @Expose
    private String vehicleColor;
    @SerializedName("vehicleHasPUC")
    @Expose
    private Boolean vehicleHasPUC;
    @SerializedName("vehicleHasRSA")
    @Expose
    private Boolean vehicleHasRSA;
    @SerializedName("vehicleUmId")
    @Expose
    private Integer vehicleUmId;
    @SerializedName("vehicleCreatedDate")
    @Expose
    private String vehicleCreatedDate;
    @SerializedName("vehicleUpdateDate")
    @Expose
    private String vehicleUpdateDate;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private String resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The vehicleId
     */
    public Integer getVehicleId() {
        return vehicleId;
    }

    /**
     *
     * @param vehicleId
     * The vehicleId
     */
    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    /**
     *
     * @return
     * The vehicleRegNumber
     */
    public String getVehicleRegNumber() {
        return vehicleRegNumber;
    }

    /**
     *
     * @param vehicleRegNumber
     * The vehicleRegNumber
     */
    public void setVehicleRegNumber(String vehicleRegNumber) {
        this.vehicleRegNumber = vehicleRegNumber;
    }

    /**
     *
     * @return
     * The vehicleMake
     */
    public String getVehicleMake() {
        return vehicleMake;
    }

    /**
     *
     * @param vehicleMake
     * The vehicleMake
     */
    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    /**
     *
     * @return
     * The vehicleModel
     */
    public String getVehicleModel() {
        return vehicleModel;
    }

    /**
     *
     * @param vehicleModel
     * The vehicleModel
     */
    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    /**
     *
     * @return
     * The vehicleYear
     */
    public String getVehicleYear() {
        return vehicleYear;
    }

    /**
     *
     * @param vehicleYear
     * The vehicleYear
     */
    public void setVehicleYear(String vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    /**
     *
     * @return
     * The vehicleColor
     */
    public String getVehicleColor() {
        return vehicleColor;
    }

    /**
     *
     * @param vehicleColor
     * The vehicleColor
     */
    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    /**
     *
     * @return
     * The vehicleHasPUC
     */
    public Boolean getVehicleHasPUC() {
        return vehicleHasPUC;
    }

    /**
     *
     * @param vehicleHasPUC
     * The vehicleHasPUC
     */
    public void setVehicleHasPUC(Boolean vehicleHasPUC) {
        this.vehicleHasPUC = vehicleHasPUC;
    }

    /**
     *
     * @return
     * The vehicleHasRSA
     */
    public Boolean getVehicleHasRSA() {
        return vehicleHasRSA;
    }

    /**
     *
     * @param vehicleHasRSA
     * The vehicleHasRSA
     */
    public void setVehicleHasRSA(Boolean vehicleHasRSA) {
        this.vehicleHasRSA = vehicleHasRSA;
    }

    /**
     *
     * @return
     * The vehicleUmId
     */
    public Integer getVehicleUmId() {
        return vehicleUmId;
    }

    /**
     *
     * @param vehicleUmId
     * The vehicleUmId
     */
    public void setVehicleUmId(Integer vehicleUmId) {
        this.vehicleUmId = vehicleUmId;
    }

    /**
     *
     * @return
     * The vehicleCreatedDate
     */
    public String getVehicleCreatedDate() {
        return vehicleCreatedDate;
    }

    /**
     *
     * @param vehicleCreatedDate
     * The vehicleCreatedDate
     */
    public void setVehicleCreatedDate(String vehicleCreatedDate) {
        this.vehicleCreatedDate = vehicleCreatedDate;
    }

    /**
     *
     * @return
     * The vehicleUpdateDate
     */
    public String getVehicleUpdateDate() {
        return vehicleUpdateDate;
    }

    /**
     *
     * @param vehicleUpdateDate
     * The vehicleUpdateDate
     */
    public void setVehicleUpdateDate(String vehicleUpdateDate) {
        this.vehicleUpdateDate = vehicleUpdateDate;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public String getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(String resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}
