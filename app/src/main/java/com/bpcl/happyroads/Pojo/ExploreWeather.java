package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 16/11/2016.
 */

public class ExploreWeather {

    @SerializedName("WeatherId")
    @Expose
    private Integer weatherId;
    @SerializedName("Weather_Temp_day")
    @Expose
    private Object weatherTempDay;
    @SerializedName("Weather_Temp_night")
    @Expose
    private Object weatherTempNight;
    @SerializedName("Weather_Temp_min")
    @Expose
    private Double weatherTempMin;
    @SerializedName("Weather_Temp_max")
    @Expose
    private Double weatherTempMax;
    @SerializedName("Weather_Temp_eve")
    @Expose
    private Object weatherTempEve;
    @SerializedName("Weather_pressure")
    @Expose
    private Double weatherPressure;
    @SerializedName("Weather_Temp_morn")
    @Expose
    private Object weatherTempMorn;
    @SerializedName("Weather_description")
    @Expose
    private String weatherDescription;
    @SerializedName("Weather_icon")
    @Expose
    private String weatherIcon;
    @SerializedName("Weather_Weather_id")
    @Expose
    private Object weatherWeatherId;
    @SerializedName("Weather_main")
    @Expose
    private Object weatherMain;
    @SerializedName("Weather_humidity")
    @Expose
    private Integer weatherHumidity;
    @SerializedName("Weath_speed")
    @Expose
    private Double weathSpeed;
    @SerializedName("Weath_rain")
    @Expose
    private Integer weathRain;
    @SerializedName("Weath_message")
    @Expose
    private Object weathMessage;
    @SerializedName("Weather_deg")
    @Expose
    private Object weatherDeg;
    @SerializedName("Weather_clouds")
    @Expose
    private Object weatherClouds;
    @SerializedName("Weather_cod")
    @Expose
    private Object weatherCod;
    @SerializedName("Weather_Cnt")
    @Expose
    private Object weatherCnt;
    @SerializedName("Weather_PTO_RefId")
    @Expose
    private Integer weatherPTORefId;
    @SerializedName("Weather_PTO_Type")
    @Expose
    private Integer weatherPTOType;
    @SerializedName("Weather_sea_level")
    @Expose
    private Double weatherSeaLevel;
    @SerializedName("Weather_grnd_level")
    @Expose
    private Double weatherGrndLevel;
    @SerializedName("Weather_Date")
    @Expose
    private String weatherDate;
    @SerializedName("Weather_Date2")
    @Expose
    private Object weatherDate2;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private Object resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("resHttpStatus")
    @Expose
    private Object resHttpStatus;
    @SerializedName("resStatusDescription")
    @Expose
    private Object resStatusDescription;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("Search")
    @Expose
    private Object search;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("CurrentLat")
    @Expose
    private Object currentLat;
    @SerializedName("CurrentLong")
    @Expose
    private Object currentLong;
    public String getWeatherDay() {
        return weatherDay;
    }

    public void setWeatherDay(String weatherDay) {
        this.weatherDay = weatherDay;
    }

    @SerializedName("Weather_Day")
    @Expose
    private String weatherDay;

    /**
     *
     * @return
     * The weatherId
     */
    public Integer getWeatherId() {
        return weatherId;
    }

    /**
     *
     * @param weatherId
     * The WeatherId
     */
    public void setWeatherId(Integer weatherId) {
        this.weatherId = weatherId;
    }

    /**
     *
     * @return
     * The weatherTempDay
     */
    public Object getWeatherTempDay() {
        return weatherTempDay;
    }

    /**
     *
     * @param weatherTempDay
     * The Weather_Temp_day
     */
    public void setWeatherTempDay(Object weatherTempDay) {
        this.weatherTempDay = weatherTempDay;
    }

    /**
     *
     * @return
     * The weatherTempNight
     */
    public Object getWeatherTempNight() {
        return weatherTempNight;
    }

    /**
     *
     * @param weatherTempNight
     * The Weather_Temp_night
     */
    public void setWeatherTempNight(Object weatherTempNight) {
        this.weatherTempNight = weatherTempNight;
    }

    /**
     *
     * @return
     * The weatherTempMin
     */
    public Double getWeatherTempMin() {
        return weatherTempMin;
    }

    /**
     *
     * @param weatherTempMin
     * The Weather_Temp_min
     */
    public void setWeatherTempMin(Double weatherTempMin) {
        this.weatherTempMin = weatherTempMin;
    }

    /**
     *
     * @return
     * The weatherTempMax
     */
    public Double getWeatherTempMax() {
        return weatherTempMax;
    }

    /**
     *
     * @param weatherTempMax
     * The Weather_Temp_max
     */
    public void setWeatherTempMax(Double weatherTempMax) {
        this.weatherTempMax = weatherTempMax;
    }

    /**
     *
     * @return
     * The weatherTempEve
     */
    public Object getWeatherTempEve() {
        return weatherTempEve;
    }

    /**
     *
     * @param weatherTempEve
     * The Weather_Temp_eve
     */
    public void setWeatherTempEve(Object weatherTempEve) {
        this.weatherTempEve = weatherTempEve;
    }

    /**
     *
     * @return
     * The weatherPressure
     */
    public Double getWeatherPressure() {
        return weatherPressure;
    }

    /**
     *
     * @param weatherPressure
     * The Weather_pressure
     */
    public void setWeatherPressure(Double weatherPressure) {
        this.weatherPressure = weatherPressure;
    }

    /**
     *
     * @return
     * The weatherTempMorn
     */
    public Object getWeatherTempMorn() {
        return weatherTempMorn;
    }

    /**
     *
     * @param weatherTempMorn
     * The Weather_Temp_morn
     */
    public void setWeatherTempMorn(Object weatherTempMorn) {
        this.weatherTempMorn = weatherTempMorn;
    }

    /**
     *
     * @return
     * The weatherDescription
     */
    public String getWeatherDescription() {
        return weatherDescription;
    }

    /**
     *
     * @param weatherDescription
     * The Weather_description
     */
    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    /**
     *
     * @return
     * The weatherIcon
     */
    public String getWeatherIcon() {
        return weatherIcon;
    }

    /**
     *
     * @param weatherIcon
     * The Weather_icon
     */
    public void setWeatherIcon(String weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    /**
     *
     * @return
     * The weatherWeatherId
     */
    public Object getWeatherWeatherId() {
        return weatherWeatherId;
    }

    /**
     *
     * @param weatherWeatherId
     * The Weather_Weather_id
     */
    public void setWeatherWeatherId(Object weatherWeatherId) {
        this.weatherWeatherId = weatherWeatherId;
    }

    /**
     *
     * @return
     * The weatherMain
     */
    public Object getWeatherMain() {
        return weatherMain;
    }

    /**
     *
     * @param weatherMain
     * The Weather_main
     */
    public void setWeatherMain(Object weatherMain) {
        this.weatherMain = weatherMain;
    }

    /**
     *
     * @return
     * The weatherHumidity
     */
    public Integer getWeatherHumidity() {
        return weatherHumidity;
    }

    /**
     *
     * @param weatherHumidity
     * The Weather_humidity
     */
    public void setWeatherHumidity(Integer weatherHumidity) {
        this.weatherHumidity = weatherHumidity;
    }

    /**
     *
     * @return
     * The weathSpeed
     */
    public Double getWeathSpeed() {
        return weathSpeed;
    }

    /**
     *
     * @param weathSpeed
     * The Weath_speed
     */
    public void setWeathSpeed(Double weathSpeed) {
        this.weathSpeed = weathSpeed;
    }

    /**
     *
     * @return
     * The weathRain
     */
    public Integer getWeathRain() {
        return weathRain;
    }

    /**
     *
     * @param weathRain
     * The Weath_rain
     */
    public void setWeathRain(Integer weathRain) {
        this.weathRain = weathRain;
    }

    /**
     *
     * @return
     * The weathMessage
     */
    public Object getWeathMessage() {
        return weathMessage;
    }

    /**
     *
     * @param weathMessage
     * The Weath_message
     */
    public void setWeathMessage(Object weathMessage) {
        this.weathMessage = weathMessage;
    }

    /**
     *
     * @return
     * The weatherDeg
     */
    public Object getWeatherDeg() {
        return weatherDeg;
    }

    /**
     *
     * @param weatherDeg
     * The Weather_deg
     */
    public void setWeatherDeg(Object weatherDeg) {
        this.weatherDeg = weatherDeg;
    }

    /**
     *
     * @return
     * The weatherClouds
     */
    public Object getWeatherClouds() {
        return weatherClouds;
    }

    /**
     *
     * @param weatherClouds
     * The Weather_clouds
     */
    public void setWeatherClouds(Object weatherClouds) {
        this.weatherClouds = weatherClouds;
    }

    /**
     *
     * @return
     * The weatherCod
     */
    public Object getWeatherCod() {
        return weatherCod;
    }

    /**
     *
     * @param weatherCod
     * The Weather_cod
     */
    public void setWeatherCod(Object weatherCod) {
        this.weatherCod = weatherCod;
    }

    /**
     *
     * @return
     * The weatherCnt
     */
    public Object getWeatherCnt() {
        return weatherCnt;
    }

    /**
     *
     * @param weatherCnt
     * The Weather_Cnt
     */
    public void setWeatherCnt(Object weatherCnt) {
        this.weatherCnt = weatherCnt;
    }

    /**
     *
     * @return
     * The weatherPTORefId
     */
    public Integer getWeatherPTORefId() {
        return weatherPTORefId;
    }

    /**
     *
     * @param weatherPTORefId
     * The Weather_PTO_RefId
     */
    public void setWeatherPTORefId(Integer weatherPTORefId) {
        this.weatherPTORefId = weatherPTORefId;
    }

    /**
     *
     * @return
     * The weatherPTOType
     */
    public Integer getWeatherPTOType() {
        return weatherPTOType;
    }

    /**
     *
     * @param weatherPTOType
     * The Weather_PTO_Type
     */
    public void setWeatherPTOType(Integer weatherPTOType) {
        this.weatherPTOType = weatherPTOType;
    }

    /**
     *
     * @return
     * The weatherSeaLevel
     */
    public Double getWeatherSeaLevel() {
        return weatherSeaLevel;
    }

    /**
     *
     * @param weatherSeaLevel
     * The Weather_sea_level
     */
    public void setWeatherSeaLevel(Double weatherSeaLevel) {
        this.weatherSeaLevel = weatherSeaLevel;
    }

    /**
     *
     * @return
     * The weatherGrndLevel
     */
    public Double getWeatherGrndLevel() {
        return weatherGrndLevel;
    }

    /**
     *
     * @param weatherGrndLevel
     * The Weather_grnd_level
     */
    public void setWeatherGrndLevel(Double weatherGrndLevel) {
        this.weatherGrndLevel = weatherGrndLevel;
    }

    /**
     *
     * @return
     * The weatherDate
     */
    public String getWeatherDate() {
        return weatherDate;
    }

    /**
     *
     * @param weatherDate
     * The Weather_Date
     */
    public void setWeatherDate(String weatherDate) {
        this.weatherDate = weatherDate;
    }

    /**
     *
     * @return
     * The weatherDate2
     */
    public Object getWeatherDate2() {
        return weatherDate2;
    }

    /**
     *
     * @param weatherDate2
     * The Weather_Date2
     */
    public void setWeatherDate2(Object weatherDate2) {
        this.weatherDate2 = weatherDate2;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public Object getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(Object resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The resHttpStatus
     */
    public Object getResHttpStatus() {
        return resHttpStatus;
    }

    /**
     *
     * @param resHttpStatus
     * The resHttpStatus
     */
    public void setResHttpStatus(Object resHttpStatus) {
        this.resHttpStatus = resHttpStatus;
    }

    /**
     *
     * @return
     * The resStatusDescription
     */
    public Object getResStatusDescription() {
        return resStatusDescription;
    }

    /**
     *
     * @param resStatusDescription
     * The resStatusDescription
     */
    public void setResStatusDescription(Object resStatusDescription) {
        this.resStatusDescription = resStatusDescription;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The search
     */
    public Object getSearch() {
        return search;
    }

    /**
     *
     * @param search
     * The Search
     */
    public void setSearch(Object search) {
        this.search = search;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    /**
     *
     * @return
     * The currentLat
     */
    public Object getCurrentLat() {
        return currentLat;
    }

    /**
     *
     * @param currentLat
     * The CurrentLat
     */
    public void setCurrentLat(Object currentLat) {
        this.currentLat = currentLat;
    }

    /**
     *
     * @return
     * The currentLong
     */
    public Object getCurrentLong() {
        return currentLong;
    }

    /**
     *
     * @param currentLong
     * The CurrentLong
     */
    public void setCurrentLong(Object currentLong) {
        this.currentLong = currentLong;
    }

}

