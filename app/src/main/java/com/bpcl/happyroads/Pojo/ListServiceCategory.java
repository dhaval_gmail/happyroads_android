

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class ListServiceCategory {

    @SerializedName("SerCatId")
    @Expose
    private Integer serCatId;
    @SerializedName("SerCatImage")
    @Expose
    private String serCatImage;
    @SerializedName("SerCatIcon")
    @Expose
    private String serCatIcon;
    @SerializedName("SerCatIsActive")
    @Expose
    private Boolean serCatIsActive;
    @SerializedName("SerCatHostDetails")
    @Expose
    private String serCatHostDetails;
    @SerializedName("SerCatAdminId")
    @Expose
    private Integer serCatAdminId;
    @SerializedName("SerCatName")
    @Expose
    private String serCatName;
    @SerializedName("SerCatFooterImage")
    @Expose
    private String serCatFooterImage;
    @SerializedName("SerCatFooterImageSelected")
    @Expose
    private String serCatFooterImageSelected;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("SerDefaultImage")
    @Expose
    private String serDefaultImage;
    @SerializedName("SerMapSelectedImage")
    @Expose
    private String serMapSelectedImage;

    public String getSerMapSelectedImage() {
        return serMapSelectedImage;
    }

    public void setSerMapSelectedImage(String serMapSelectedImage) {
        this.serMapSelectedImage = serMapSelectedImage;
    }

    public String getSerDefaultImage() {
        return serDefaultImage;
    }

    public void setSerDefaultImage(String serDefaultImage) {
        this.serDefaultImage = serDefaultImage;
    }

    public boolean status;



    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean marker_flag;

    public boolean isMarker_flag() {
        return marker_flag;
    }

    public void setMarker_flag(boolean marker_flag) {
        this.marker_flag = marker_flag;
    }

    /**
     *
     * @return
     * The serCatId
     */
    public Integer getSerCatId() {
        return serCatId;
    }

    /**
     *
     * @param serCatId
     * The SerCatId
     */
    public void setSerCatId(Integer serCatId) {
        this.serCatId = serCatId;
    }

    /**
     *
     * @return
     * The serCatImage
     */
    public String getSerCatImage() {
        return serCatImage;
    }

    /**
     *
     * @param serCatImage
     * The SerCatImage
     */
    public void setSerCatImage(String serCatImage) {
        this.serCatImage = serCatImage;
    }

    /**
     *
     * @return
     * The serCatIcon
     */
    public String getSerCatIcon() {
        return serCatIcon;
    }

    /**
     *
     * @param serCatIcon
     * The SerCatIcon
     */
    public void setSerCatIcon(String serCatIcon) {
        this.serCatIcon = serCatIcon;
    }

    /**
     *
     * @return
     * The serCatIsActive
     */
    public Boolean getSerCatIsActive() {
        return serCatIsActive;
    }

    /**
     *
     * @param serCatIsActive
     * The SerCatIsActive
     */
    public void setSerCatIsActive(Boolean serCatIsActive) {
        this.serCatIsActive = serCatIsActive;
    }

    /**
     *
     * @return
     * The serCatHostDetails
     */
    public String getSerCatHostDetails() {
        return serCatHostDetails;
    }

    /**
     *
     * @param serCatHostDetails
     * The SerCatHostDetails
     */
    public void setSerCatHostDetails(String serCatHostDetails) {
        this.serCatHostDetails = serCatHostDetails;
    }

    /**
     *
     * @return
     * The serCatAdminId
     */
    public Integer getSerCatAdminId() {
        return serCatAdminId;
    }

    /**
     *
     * @param serCatAdminId
     * The SerCatAdminId
     */
    public void setSerCatAdminId(Integer serCatAdminId) {
        this.serCatAdminId = serCatAdminId;
    }

    /**
     *
     * @return
     * The serCatName
     */
    public String getSerCatName() {
        return serCatName;
    }

    /**
     *
     * @param serCatName
     * The SerCatName
     */
    public void setSerCatName(String serCatName) {
        this.serCatName = serCatName;
    }

    /**
     *
     * @return
     * The serCatFooterImage
     */
    public String getSerCatFooterImage() {
        return serCatFooterImage;
    }

    /**
     *
     * @param serCatFooterImage
     * The SerCatFooterImage
     */
    public void setSerCatFooterImage(String serCatFooterImage) {
        this.serCatFooterImage = serCatFooterImage;
    }

    /**
     *
     * @return
     * The serCatFooterImageSelected
     */
    public String getSerCatFooterImageSelected() {
        return serCatFooterImageSelected;
    }

    /**
     *
     * @param serCatFooterImageSelected
     * The SerCatFooterImageSelected
     */
    public void setSerCatFooterImageSelected(String serCatFooterImageSelected) {
        this.serCatFooterImageSelected = serCatFooterImageSelected;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}