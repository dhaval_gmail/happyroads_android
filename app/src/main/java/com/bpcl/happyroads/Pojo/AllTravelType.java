package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADMIN on 9/27/2016.
 */
public class AllTravelType  {

    @SerializedName("ImageListJson")
    @Expose
    private String imageListJson;
    @SerializedName("TTypeId")
    @Expose
    private Integer tTypeId;
    @SerializedName("TTypeName")
    @Expose
    private String tTypeName;
    @SerializedName("TTypeDescription")
    @Expose
    private String tTypeDescription;
    @SerializedName("TTypeImage")
    @Expose
    private String tTypeImage;
    @SerializedName("TTypeCreateDate")
    @Expose
    private String tTypeCreateDate;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("ImageListJson2")
    @Expose
    private List<Object> imageListJson2 = new ArrayList<Object>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;

    /**
     *
     * @return
     * The imageListJson
     */
    public String getImageListJson() {
        return imageListJson;
    }

    /**
     *
     * @param imageListJson
     * The ImageListJson
     */
    public void setImageListJson(String imageListJson) {
        this.imageListJson = imageListJson;
    }

    /**
     *
     * @return
     * The tTypeId
     */
    public Integer getTTypeId() {
        return tTypeId;
    }

    /**
     *
     * @param tTypeId
     * The TTypeId
     */
    public void setTTypeId(Integer tTypeId) {
        this.tTypeId = tTypeId;
    }

    /**
     *
     * @return
     * The tTypeName
     */
    public String getTTypeName() {
        return tTypeName;
    }

    /**
     *
     * @param tTypeName
     * The TTypeName
     */
    public void setTTypeName(String tTypeName) {
        this.tTypeName = tTypeName;
    }

    /**
     *
     * @return
     * The tTypeDescription
     */
    public String getTTypeDescription() {
        return tTypeDescription;
    }

    /**
     *
     * @param tTypeDescription
     * The TTypeDescription
     */
    public void setTTypeDescription(String tTypeDescription) {
        this.tTypeDescription = tTypeDescription;
    }

    /**
     *
     * @return
     * The tTypeImage
     */
    public String getTTypeImage() {
        return tTypeImage;
    }

    /**
     *
     * @param tTypeImage
     * The TTypeImage
     */
    public void setTTypeImage(String tTypeImage) {
        this.tTypeImage = tTypeImage;
    }

    /**
     *
     * @return
     * The tTypeCreateDate
     */
    public String getTTypeCreateDate() {
        return tTypeCreateDate;
    }

    /**
     *
     * @param tTypeCreateDate
     * The TTypeCreateDate
     */
    public void setTTypeCreateDate(String tTypeCreateDate) {
        this.tTypeCreateDate = tTypeCreateDate;
    }

    /**
     *
     * @return
     * The isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The IsActive
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     *
     * @return
     * The imageListJson2
     */
    public List<Object> getImageListJson2() {
        return imageListJson2;
    }

    /**
     *
     * @param imageListJson2
     * The ImageListJson2
     */
    public void setImageListJson2(List<Object> imageListJson2) {
        this.imageListJson2 = imageListJson2;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
