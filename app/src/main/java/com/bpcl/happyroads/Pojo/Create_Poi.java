package com.bpcl.happyroads.Pojo;

import java.text.DecimalFormat;

/**
 * Created by ADMIN on 9/30/2016.
 */
public class Create_Poi {
    Integer tripId,PoiId,WayPointId,poiDesID,poiServerId,legNo,stepNo;
    String poiName,resumeDate,resumeTime,poiDate,poitype,poidescription,actual_arrival_date_time,actual_dep_date_time;
    Double stayTime,poiLatitude,poiLongitude,arrivalTime,departureTime;
    byte[] poiImage;
    Boolean poiStay,flag;

    Double KM,time,reqtime, actual_Km,actual_Time;
    Integer attractionCount;

    Boolean returnstatus;

    String imagename;
    Double poi_diff_seconds;
    Boolean poi_track_flag;
    String poiside;

    public String getPoiside() {
        if(poiside==null)
            return "";
        else
            return poiside;
    }

    public void setPoiside(String poiside) {
        this.poiside = poiside;
    }

    public Boolean getPoi_track_flag() {
        return poi_track_flag;
    }

    public void setPoi_track_flag(Boolean poi_track_flag) {
        this.poi_track_flag = poi_track_flag;
    }

    public Double getActual_Km() {
        return actual_Km;
    }

    public void setActual_Km(Double actual_Km) {
        this.actual_Km = actual_Km;
    }

    public Double getActual_Time() {
        return actual_Time;
    }

    public void setActual_Time(Double actual_Time) {
        this.actual_Time = actual_Time;
    }

    public Double getPoi_diff_seconds() {
        return poi_diff_seconds;
    }

    public void setPoi_diff_seconds(Double poi_diff_seconds) {
        this.poi_diff_seconds = poi_diff_seconds;
    }

    public Integer getLegNo() {
        return legNo;
    }

    public void setLegNo(Integer legNo) {
        this.legNo = legNo;
    }

    public Integer getStepNo() {
        return stepNo;
    }

    public void setStepNo(Integer stepNo) {
        this.stepNo = stepNo;
    }

    public String getActual_dep_date_time() {
        return actual_dep_date_time;
    }

    public void setActual_dep_date_time(String actual_dep_date_time) {
        this.actual_dep_date_time = actual_dep_date_time;
    }

    public String getActual_arrival_date_time() {
        return actual_arrival_date_time;
    }

    public void setActual_arrival_date_time(String actual_arrival_date_time) {
        this.actual_arrival_date_time = actual_arrival_date_time;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }



    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }

    public String getPoidescription() {
        return poidescription;
    }

    public void setPoidescription(String poidescription) {
        this.poidescription = poidescription;
    }

    public Boolean getReturnstatus() {
        return returnstatus;
    }

    public void setReturnstatus(Boolean returnstatus) {
        this.returnstatus = returnstatus;
    }

    public Integer getAttractionCount() {
        return attractionCount;
    }

    public void setAttractionCount(Integer attractionCount) {
        this.attractionCount = attractionCount;
    }

    public Integer getPoiServerId() {
        return poiServerId;
    }

    public void setPoiServerId(Integer poiServerId) {
        this.poiServerId = poiServerId;
    }

    public Integer getPoiDesID() {
        return poiDesID;
    }

    public void setPoiDesID(Integer poiDesID) {
        this.poiDesID = poiDesID;
    }

    public Double getReqtime() {
        return reqtime;
    }

    public void setReqtime(Double reqtime) {
        this.reqtime = reqtime;
    }

    public Double getKM() {
        return KM;
    }

    public void setKM(Double KM) {
        this.KM = KM;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public Integer getPoiId() {
        return PoiId;
    }

    public void setPoiId(Integer poiId) {
        PoiId = poiId;
    }

    public String getPoiName() {
        return poiName;
    }

    public void setPoiName(String poiName) {
        this.poiName = poiName;
    }

    public String getResumeDate() {
        return resumeDate;
    }

    public void setResumeDate(String resumeDate) {
        this.resumeDate = resumeDate;
    }

    public String getResumeTime() {
        return resumeTime;
    }

    public void setResumeTime(String resumeTime) {
        this.resumeTime = resumeTime;
    }

    public Double getStayTime() {
        return stayTime;
    }

    public void setStayTime(Double stayTime) {
        this.stayTime = stayTime;
    }

    public Double getPoiLatitude() {
        return poiLatitude;
    }

    public void setPoiLatitude(Double poiLatitude) {
        this.poiLatitude = poiLatitude;
    }

    public Double getPoiLongitude() {
        return poiLongitude;
    }

    public void setPoiLongitude(Double poiLongitude) {
        this.poiLongitude = poiLongitude;
    }

    public byte[] getPoiImage() {
        return poiImage;
    }

    public void setPoiImage(byte[] poiImage) {
        this.poiImage = poiImage;
    }

    public Boolean getPoiStay() {
        return poiStay;
    }

    public void setPoiStay(Boolean poiStay) {
        this.poiStay = poiStay;
    }

    public String getPoiDate() {
        return poiDate;
    }

    public void setPoiDate(String poiDate) {
        this.poiDate = poiDate;
    }

    public Double getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Double getDepartureTime() {

        DecimalFormat df = new DecimalFormat("#.00");

        return Double.valueOf(df.format(departureTime));
    }

    public void setDepartureTime(Double departureTime) {
        this.departureTime = departureTime;
    }

    public Integer getWayPointId() {
        return WayPointId;
    }

    public void setWayPointId(Integer wayPointId) {
        WayPointId = wayPointId;
    }

    public String getPoitype() {
        return poitype;
    }

    public void setPoitype(String poitype) {
        this.poitype = poitype;
    }
    public boolean click_status;
    public boolean isClick_status() {
        return click_status;
    }

    public void setClick_status(boolean click_status) {
        this.click_status = click_status;
    }
}
