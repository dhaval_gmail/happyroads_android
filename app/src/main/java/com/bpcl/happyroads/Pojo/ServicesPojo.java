

        package com.bpcl.happyroads.Pojo;

        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

        import java.util.ArrayList;
        import java.util.List;

        import javax.annotation.Generated;


        @Generated("org.jsonschema2pojo")
public class ServicesPojo {

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("duration")
    @Expose
    private Double duration;
    @SerializedName("SerCatId")
    @Expose
    private Integer serCatId;
    @SerializedName("SerCatImage")
    @Expose
    private String serCatImage;
    @SerializedName("SerCatIcon")
    @Expose
    private String serCatIcon;
    @SerializedName("SerCatIsActive")
    @Expose
    private Boolean serCatIsActive;
    @SerializedName("SerCatHostDetails")
    @Expose
    private String serCatHostDetails;

            public String getToiFeature() {
                return ToiFeature;
            }

            public void setToiFeature(String toiFeature) {
                ToiFeature = toiFeature;
            }

            @SerializedName("ToiFeature")
    @Expose
    private String ToiFeature;

            @SerializedName("SerMapSelectedImage")
            @Expose
            private String serMapSelectedImage;

            public String getSerMapSelectedImage() {
                return serMapSelectedImage;
            }

            public void setSerMapSelectedImage(String serMapSelectedImage) {
                this.serMapSelectedImage = serMapSelectedImage;
            }

            @SerializedName("AmbulanceContactNo")
    @Expose
    private String AmbulanceContactNo;

            public String getAmbulanceContactNo() {
                if(AmbulanceContactNo==null) {
                    return "";
                }
                else {
                    return AmbulanceContactNo;
                }
            }

            public void setAmbulanceContactNo(String ambulanceContactNo) {
                AmbulanceContactNo = ambulanceContactNo;
            }

            public String getEmergencyContactNo() {
                if(EmergencyContactNo==null) {
                    return "";
                }
                else {
                    return EmergencyContactNo;
                }

            }

            public void setEmergencyContactNo(String emergencyContactNo) {
                EmergencyContactNo = emergencyContactNo;
            }

            @SerializedName("EmergencyContactNo")
    @Expose
    private String EmergencyContactNo;

    @SerializedName("SerCatAdminId")
    @Expose
    private Integer serCatAdminId;
    @SerializedName("SerCatName")
    @Expose
    private String serCatName;

            public String getSerDefaultImage() {
                return SerDefaultImage;
            }

            public void setSerDefaultImage(String serDefaultImage) {
                SerDefaultImage = serDefaultImage;
            }

            @SerializedName("SerDefaultImage")
            @Expose
            private String SerDefaultImage;
    @SerializedName("SerCatFooterImage")
    @Expose
    private String serCatFooterImage;
    @SerializedName("SerCatFooterImageSelected")
    @Expose
    private String serCatFooterImageSelected;
    @SerializedName("SerSeqId")
    @Expose
    private Integer serSeqId;
    @SerializedName("SerIsRo")
    @Expose
    private Boolean serIsRo;
    @SerializedName("DefaultSpendTime")
    @Expose
    private String defaultSpendTime;
    @SerializedName("SerIsMapOn")
    @Expose
    private Boolean serIsMapOn;
    @SerializedName("SerId")
    @Expose
    private Integer serId;
    @SerializedName("SerCategoryId")
    @Expose
    private Integer serCategoryId;
    @SerializedName("SerName")
    @Expose
    private String serName;
    @SerializedName("SerCCNo")
    @Expose
    private String serCCNo;
    @SerializedName("SerNearByDestination")
    @Expose
    private String serNearByDestination;
    @SerializedName("SerAddress1")
    @Expose
    private String serAddress1;
    @SerializedName("SerLatitude")
    @Expose
    private String serLatitude;
    @SerializedName("SerLongitude")
    @Expose
    private String serLongitude;
    @SerializedName("SerSide")
    @Expose
    private String serSide;
    @SerializedName("SerStateId")
    @Expose
    private Integer serStateId;
    @SerializedName("SerCityId")
    @Expose
    private Integer serCityId;
    @SerializedName("SerIsFullTime")
    @Expose
    private Boolean serIsFullTime;
    @SerializedName("SerStartTime")
    @Expose
    private String serStartTime;
    @SerializedName("SerEndTime")
    @Expose
    private String serEndTime;
    @SerializedName("SetIsActive")
    @Expose
    private Boolean setIsActive;
    @SerializedName("SerCreateDate")
    @Expose
    private String serCreateDate;
    @SerializedName("SerUpdateDate")
    @Expose
    private String serUpdateDate;
    @SerializedName("SerHostDetail")
    @Expose
    private String serHostDetail;
    @SerializedName("SerAdminId")
    @Expose
    private Integer serAdminId;
    @SerializedName("SerServiceType")
    @Expose
    private String serServiceType;
    @SerializedName("SerSpendTime")
    @Expose
    private String serSpendTime;
    @SerializedName("SerOtherFacility")
    @Expose
    private String serOtherFacility;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("statename")
    @Expose
    private String statename;
            public String getGarageWheelerType() {
                if(garageWheelerType==null)
                {
                    return "";
                }else {
                    return garageWheelerType;
                }
            }

            public void setGarageWheelerType(String garageWheelerType) {
                this.garageWheelerType = garageWheelerType;
            }

            @SerializedName("garageWheelerType")
            @Expose
            private String garageWheelerType;
    @SerializedName("SerOtherFacilityLists")
    @Expose
    private List<SerOtherFacilityList> serOtherFacilityLists = new ArrayList<SerOtherFacilityList>();

            public List<GetResAdditionalFacilitiesList> getGetResAdditionalFacilitiesList() {
                return getResAdditionalFacilitiesList;
            }

            public void setGetResAdditionalFacilitiesList(List<GetResAdditionalFacilitiesList> getResAdditionalFacilitiesList) {
                this.getResAdditionalFacilitiesList = getResAdditionalFacilitiesList;
            }

            public List<GetACServiceLists> getGetACServiceLists() {
                return getACServiceLists;
            }

            public void setGetACServiceLists(List<GetACServiceLists> getACServiceLists) {
                this.getACServiceLists = getACServiceLists;
            }

            public List<GetCuisineLists> getGetCuisineLists() {
                return getCuisineLists;
            }

            public void setGetCuisineLists(List<GetCuisineLists> getCuisineLists) {
                this.getCuisineLists = getCuisineLists;
            }

            public List<GetFoodTypeLists> getGetFoodTypeLists() {
                return getFoodTypeLists;
            }

            public void setGetFoodTypeLists(List<GetFoodTypeLists> getFoodTypeLists) {
                this.getFoodTypeLists = getFoodTypeLists;
            }

            public List<GetToiletTypesList> getGetToiletTypesList() {
                return getToiletTypesList;
            }

            public void setGetToiletTypesList(List<GetToiletTypesList> getToiletTypesList) {
                this.getToiletTypesList = getToiletTypesList;
            }

            public List<GetFuelsList> getGetFuelsList() {
                return getFuelsList;
            }

            public void setGetFuelsList(List<GetFuelsList> getFuelsList) {
                this.getFuelsList = getFuelsList;
            }

            public List<GetFuelsTypeList> getGetFuelsTypeList() {
                return getFuelsTypeList;
            }

            public void setGetFuelsTypeList(List<GetFuelsTypeList> getFuelsTypeList) {
                this.getFuelsTypeList = getFuelsTypeList;
            }

            public List<GetResSeatingFacilitiesList> getGetResSeatingFacilitiesList() {
                return getResSeatingFacilitiesList;
            }

            public void setGetResSeatingFacilitiesList(List<GetResSeatingFacilitiesList> getResSeatingFacilitiesList) {
                this.getResSeatingFacilitiesList = getResSeatingFacilitiesList;
            }

            @SerializedName("GetACServiceLists")
            @Expose
            private List<GetACServiceLists> getACServiceLists = new ArrayList<GetACServiceLists>();
            @SerializedName("GetCuisineLists")
            @Expose
            private List<GetCuisineLists> getCuisineLists = new ArrayList<GetCuisineLists>();

            @SerializedName("GetFoodTypeLists")
            @Expose
            private List<GetFoodTypeLists> getFoodTypeLists = new ArrayList<GetFoodTypeLists>();

            @SerializedName("GetToiletTypesList")
            @Expose
            private List<GetToiletTypesList> getToiletTypesList = new ArrayList<GetToiletTypesList>();


            @SerializedName("GetFuelsList")
            @Expose
            private List<GetFuelsList> getFuelsList = new ArrayList<GetFuelsList>();

            @SerializedName("GetFuelsTypeList")
            @Expose
            private List<GetFuelsTypeList> getFuelsTypeList = new ArrayList<GetFuelsTypeList>();


            @SerializedName("GetResSeatingFacilitiesList")
            @Expose
            private List<GetResSeatingFacilitiesList> getResSeatingFacilitiesList = new ArrayList<GetResSeatingFacilitiesList>();


            @SerializedName("GetResAdditionalFacilitiesList")
            @Expose
            private List<GetResAdditionalFacilitiesList> getResAdditionalFacilitiesList = new ArrayList<GetResAdditionalFacilitiesList>();

            public List<GarageWheelerTypesList> getGarageWheelerTypesLists() {
                return garageWheelerTypesLists;
            }

            public void setGarageWheelerTypesLists(List<GarageWheelerTypesList> garageWheelerTypesLists) {
                this.garageWheelerTypesLists = garageWheelerTypesLists;
            }

            @SerializedName("GetGarageWheelerTypesList")
            @Expose
            private List<GarageWheelerTypesList> garageWheelerTypesLists = new ArrayList<GarageWheelerTypesList>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("RestType")
    @Expose
    private String restType;
    @SerializedName("RestFoodType")
    @Expose
    private String restFoodType;
    @SerializedName("RestCuisine")
    @Expose
    private String restCuisine;
    @SerializedName("RestACService")
    @Expose
    private String restACService;
    @SerializedName("RestAdditionalFacilities")
    @Expose
    private String restAdditionalFacilities;
    @SerializedName("RestSeatingFacility")
    @Expose
    private String restSeatingFacility;
    @SerializedName("RestToilet")
    @Expose
    private String restToilet;
    @SerializedName("RestCreateDate")
    @Expose
    private String restCreateDate;
    @SerializedName("RestUpdateDate")
    @Expose
    private String restUpdateDate;
    @SerializedName("RestHostDetail")
    @Expose
    private String restHostDetail;
    @SerializedName("RestAdminId")
    @Expose
    private Integer restAdminId;
    @SerializedName("RestIsactive")
    @Expose
    private Boolean restIsactive;
    @SerializedName("SerContactPerson")
    @Expose
    private String serContactPerson;
            @SerializedName("SerEmailId")
            @Expose
            private String SerEmailId;
            /**
             *
             * @return
             * The SerEmailId
             */
            public String getSerEmailId() {
                if(SerEmailId==null)
                {
                    return "";
                }else {
                    return SerEmailId;
                }
            }

            /**
             *
             * @param SerEmailId
             * The SerEmailId
             */
            public void setSerEmailId(String SerEmailId1) {
                this.SerEmailId = SerEmailId1;
            }
    @SerializedName("SerCustCareNo")
    @Expose
    private String serCustCareNo;

    @SerializedName("SerPostalCode")
    @Expose
    private String serPostalCode;


            public boolean click_status;
            public boolean isClick_status() {
                return click_status;
            }

            public void setClick_status(boolean click_status) {
                this.click_status = click_status;
            }

    /**
     *
     * @return
     * The rowNum
     */
    public Integer getRowNum() {
        return rowNum;
    }

    /**
     *
     * @param rowNum
     * The RowNum
     */
    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    /**
     *
     * @return
     * The distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     *
     * @param distance
     * The distance
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     *
     * @return
     * The duration
     */
    public Double getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     * The duration
     */
    public void setDuration(Double duration) {
        this.duration = duration;
    }

    /**
     *
     * @return
     * The serCatId
     */
    public Integer getSerCatId() {
        return serCatId;
    }

    /**
     *
     * @param serCatId
     * The SerCatId
     */
    public void setSerCatId(Integer serCatId) {
        this.serCatId = serCatId;
    }

    /**
     *
     * @return
     * The serCatImage
     */
    public String getSerCatImage() {
        return serCatImage;
    }

    /**
     *
     * @param serCatImage
     * The SerCatImage
     */
    public void setSerCatImage(String serCatImage) {
        this.serCatImage = serCatImage;
    }

    /**
     *
     * @return
     * The serCatIcon
     */
    public String getSerCatIcon() {
        return serCatIcon;
    }

    /**
     *
     * @param serCatIcon
     * The SerCatIcon
     */
    public void setSerCatIcon(String serCatIcon) {
        this.serCatIcon = serCatIcon;
    }

    /**
     *
     * @return
     * The serCatIsActive
     */
    public Boolean getSerCatIsActive() {
        return serCatIsActive;
    }

    /**
     *
     * @param serCatIsActive
     * The SerCatIsActive
     */
    public void setSerCatIsActive(Boolean serCatIsActive) {
        this.serCatIsActive = serCatIsActive;
    }

    /**
     *
     * @return
     * The serCatHostDetails
     */
    public String getSerCatHostDetails() {
        return serCatHostDetails;
    }

    /**
     *
     * @param serCatHostDetails
     * The SerCatHostDetails
     */
    public void setSerCatHostDetails(String serCatHostDetails) {
        this.serCatHostDetails = serCatHostDetails;
    }

    /**
     *
     * @return
     * The serCatAdminId
     */
    public Integer getSerCatAdminId() {
        return serCatAdminId;
    }

    /**
     *
     * @param serCatAdminId
     * The SerCatAdminId
     */
    public void setSerCatAdminId(Integer serCatAdminId) {
        this.serCatAdminId = serCatAdminId;
    }

    /**
     *
     * @return
     * The serCatName
     */
    public String getSerCatName() {
        return serCatName;
    }

    /**
     *
     * @param serCatName
     * The SerCatName
     */
    public void setSerCatName(String serCatName) {
        this.serCatName = serCatName;
    }

    /**
     *
     * @return
     * The serCatFooterImage
     */
    public String getSerCatFooterImage() {
        return serCatFooterImage;
    }

    /**
     *
     * @param serCatFooterImage
     * The SerCatFooterImage
     */
    public void setSerCatFooterImage(String serCatFooterImage) {
        this.serCatFooterImage = serCatFooterImage;
    }

    /**
     *
     * @return
     * The serCatFooterImageSelected
     */
    public String getSerCatFooterImageSelected() {
        return serCatFooterImageSelected;
    }

    /**
     *
     * @param serCatFooterImageSelected
     * The SerCatFooterImageSelected
     */
    public void setSerCatFooterImageSelected(String serCatFooterImageSelected) {
        this.serCatFooterImageSelected = serCatFooterImageSelected;
    }

    /**
     *
     * @return
     * The serSeqId
     */
    public Integer getSerSeqId() {
        return serSeqId;
    }

    /**
     *
     * @param serSeqId
     * The SerSeqId
     */
    public void setSerSeqId(Integer serSeqId) {
        this.serSeqId = serSeqId;
    }

    /**
     *
     * @return
     * The serIsRo
     */
    public Boolean getSerIsRo() {
        return serIsRo;
    }

    /**
     *
     * @param serIsRo
     * The SerIsRo
     */
    public void setSerIsRo(Boolean serIsRo) {
        this.serIsRo = serIsRo;
    }

    /**
     *
     * @return
     * The defaultSpendTime
     */
    public String getDefaultSpendTime() {
        return defaultSpendTime;
    }

    /**
     *
     * @param defaultSpendTime
     * The DefaultSpendTime
     */
    public void setDefaultSpendTime(String defaultSpendTime) {
        this.defaultSpendTime = defaultSpendTime;
    }

    /**
     *
     * @return
     * The serIsMapOn
     */
    public Boolean getSerIsMapOn() {
        return serIsMapOn;
    }

    /**
     *
     * @param serIsMapOn
     * The SerIsMapOn
     */
    public void setSerIsMapOn(Boolean serIsMapOn) {
        this.serIsMapOn = serIsMapOn;
    }

    /**
     *
     * @return
     * The serId
     */
    public Integer getSerId() {
        return serId;
    }

    /**
     *
     * @param serId
     * The SerId
     */
    public void setSerId(Integer serId) {
        this.serId = serId;
    }

    /**
     *
     * @return
     * The serCategoryId
     */
    public Integer getSerCategoryId() {
        return serCategoryId;
    }

    /**
     *
     * @param serCategoryId
     * The SerCategoryId
     */
    public void setSerCategoryId(Integer serCategoryId) {
        this.serCategoryId = serCategoryId;
    }

    /**
     *
     * @return
     * The serName
     */
    public String getSerName() {
        return serName;
    }

    /**
     *
     * @param serName
     * The SerName
     */
    public void setSerName(String serName) {
        this.serName = serName;
    }

    /**
     *
     * @return
     * The serCCNo
     */
    public String getSerCCNo() {
        return serCCNo;
    }

    /**
     *
     * @param serCCNo
     * The SerCCNo
     */
    public void setSerCCNo(String serCCNo) {
        this.serCCNo = serCCNo;
    }

    /**
     *
     * @return
     * The serNearByDestination
     */
    public String getSerNearByDestination() {
        return serNearByDestination;
    }

    /**
     *
     * @param serNearByDestination
     * The SerNearByDestination
     */
    public void setSerNearByDestination(String serNearByDestination) {
        this.serNearByDestination = serNearByDestination;
    }

    /**
     *
     * @return
     * The serAddress1
     */
    public String getSerAddress1() {
        if(serAddress1==null)
        {
            return "";
        }else {
            return serAddress1;
        }
    }

    /**
     *
     * @param serAddress1
     * The SerAddress1
     */
    public void setSerAddress1(String serAddress1) {
        this.serAddress1 = serAddress1;
    }

    /**
     *
     * @return
     * The serLatitude
     */
    public String getSerLatitude() {
        return serLatitude;
    }

    /**
     *
     * @param serLatitude
     * The SerLatitude
     */
    public void setSerLatitude(String serLatitude) {
        this.serLatitude = serLatitude;
    }

    /**
     *
     * @return
     * The serLongitude
     */
    public String getSerLongitude() {
        return serLongitude;
    }

    /**
     *
     * @param serLongitude
     * The SerLongitude
     */
    public void setSerLongitude(String serLongitude) {
        this.serLongitude = serLongitude;
    }

    /**
     *
     * @return
     * The serSide
     */
    public String getSerSide() {

        return serSide;
    }

    /**
     *
     * @param serSide
     * The SerSide
     */
    public void setSerSide(String serSide) {
        this.serSide = serSide;
    }

    /**
     *
     * @return
     * The serStateId
     */
    public Integer getSerStateId() {
        return serStateId;
    }

    /**
     *
     * @param serStateId
     * The SerStateId
     */
    public void setSerStateId(Integer serStateId) {
        this.serStateId = serStateId;
    }

    /**
     *
     * @return
     * The serCityId
     */
    public Integer getSerCityId() {
        return serCityId;
    }

    /**
     *
     * @param serCityId
     * The SerCityId
     */
    public void setSerCityId(Integer serCityId) {
        this.serCityId = serCityId;
    }

    /**
     *
     * @return
     * The serIsFullTime
     */
    public Boolean getSerIsFullTime() {
        return serIsFullTime;
    }

    /**
     *
     * @param serIsFullTime
     * The SerIsFullTime
     */
    public void setSerIsFullTime(Boolean serIsFullTime) {
        this.serIsFullTime = serIsFullTime;
    }

    /**
     *
     * @return
     * The serStartTime
     */
    public String getSerStartTime() {
        return serStartTime;
    }

    /**
     *
     * @param serStartTime
     * The SerStartTime
     */
    public void setSerStartTime(String serStartTime) {
        this.serStartTime = serStartTime;
    }

    /**
     *
     * @return
     * The serEndTime
     */
    public String getSerEndTime() {
        return serEndTime;
    }

    /**
     *
     * @param serEndTime
     * The SerEndTime
     */
    public void setSerEndTime(String serEndTime) {
        this.serEndTime = serEndTime;
    }

    /**
     *
     * @return
     * The setIsActive
     */
    public Boolean getSetIsActive() {
        return setIsActive;
    }

    /**
     *
     * @param setIsActive
     * The SetIsActive
     */
    public void setSetIsActive(Boolean setIsActive) {
        this.setIsActive = setIsActive;
    }

    /**
     *
     * @return
     * The serCreateDate
     */
    public String getSerCreateDate() {
        return serCreateDate;
    }

    /**
     *
     * @param serCreateDate
     * The SerCreateDate
     */
    public void setSerCreateDate(String serCreateDate) {
        this.serCreateDate = serCreateDate;
    }

    /**
     *
     * @return
     * The serUpdateDate
     */
    public String getSerUpdateDate() {
        return serUpdateDate;
    }

    /**
     *
     * @param serUpdateDate
     * The SerUpdateDate
     */
    public void setSerUpdateDate(String serUpdateDate) {
        this.serUpdateDate = serUpdateDate;
    }

    /**
     *
     * @return
     * The serHostDetail
     */
    public String getSerHostDetail() {
        return serHostDetail;
    }

    /**
     *
     * @param serHostDetail
     * The SerHostDetail
     */
    public void setSerHostDetail(String serHostDetail) {
        this.serHostDetail = serHostDetail;
    }

    /**
     *
     * @return
     * The serAdminId
     */
    public Integer getSerAdminId() {
        return serAdminId;
    }

    /**
     *
     * @param serAdminId
     * The SerAdminId
     */
    public void setSerAdminId(Integer serAdminId) {
        this.serAdminId = serAdminId;
    }

    /**
     *
     * @return
     * The serServiceType
     */
    public String getSerServiceType() {
        return serServiceType;
    }

    /**
     *
     * @param serServiceType
     * The SerServiceType
     */
    public void setSerServiceType(String serServiceType) {
        this.serServiceType = serServiceType;
    }

    /**
     *
     * @return
     * The serSpendTime
     */
    public String getSerSpendTime() {
        return serSpendTime;
    }

    /**
     *
     * @param serSpendTime
     * The SerSpendTime
     */
    public void setSerSpendTime(String serSpendTime) {
        this.serSpendTime = serSpendTime;
    }

    /**
     *
     * @return
     * The serOtherFacility
     */
    public String getSerOtherFacility() {
        if(serOtherFacility == null)
        {
            return "";
        }
        else {
            return serOtherFacility;
        }
    }

    /**
     *
     * @param serOtherFacility
     * The SerOtherFacility
     */
    public void setSerOtherFacility(String serOtherFacility) {
        this.serOtherFacility = serOtherFacility;
    }

    /**
     *
     * @return
     * The cityname
     */
    public String getCityname() {
        if(cityname==null)
        {
            return "";
        }else {
            return cityname;
        }
    }

    /**
     *
     * @param cityname
     * The cityname
     */
    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    /**
     *
     * @return
     * The statename
     */
    public String getStatename() {
        return statename;
    }

    /**
     *
     * @param statename
     * The statename
     */
    public void setStatename(String statename) {
        this.statename = statename;
    }

    /**
     *
     * @return
     * The serOtherFacilityLists
     */
    public List<SerOtherFacilityList> getSerOtherFacilityLists() {
        return serOtherFacilityLists;
    }

    /**
     *
     * @param serOtherFacilityLists
     * The SerOtherFacilityLists
     */
    public void setSerOtherFacilityLists(List<SerOtherFacilityList> serOtherFacilityLists) {
        this.serOtherFacilityLists = serOtherFacilityLists;
    }




    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    /**
     *
     * @return
     * The restType
     */
    public String getRestType() {
        return restType;
    }

    /**
     *
     * @param restType
     * The RestType
     */
    public void setRestType(String restType) {
        this.restType = restType;
    }

    /**
     *
     * @return
     * The restFoodType
     */
    public String getRestFoodType() {
        if(restFoodType == null)
        {
            return "";
        }
        else {
            return restFoodType;
        }
    }

    /**
     *
     * @param restFoodType
     * The RestFoodType
     */
    public void setRestFoodType(String restFoodType) {
        this.restFoodType = restFoodType;
    }

    /**
     *
     * @return
     * The restCuisine
     */
    public String getRestCuisine() {
        if(restCuisine == null)
        {
            return  "";
        }
        else {
            return restCuisine;
        }
    }

    /**
     *
     * @param restCuisine
     * The RestCuisine
     */
    public void setRestCuisine(String restCuisine) {
        this.restCuisine = restCuisine;
    }

    /**
     *
     * @return
     * The restACService
     */
    public String getRestACService() {
        if(restACService == null)
        {
            return "";
        }
        else {
            return restACService;
        }
    }

    /**
     *
     * @param restACService
     * The RestACService
     */
    public void setRestACService(String restACService) {
        this.restACService = restACService;
    }

    /**
     *
     * @return
     * The restAdditionalFacilities
     */
    public String getRestAdditionalFacilities() {
        return restAdditionalFacilities;
    }

    /**
     *
     * @param restAdditionalFacilities
     * The RestAdditionalFacilities
     */
    public void setRestAdditionalFacilities(String restAdditionalFacilities) {
        this.restAdditionalFacilities = restAdditionalFacilities;
    }

    /**
     *
     * @return
     * The restSeatingFacility
     */
    public String getRestSeatingFacility() {
        if(restSeatingFacility == null)
        {
            return "";
        }
        else {
            return restSeatingFacility;
        }
    }

    /**
     *
     * @param restSeatingFacility
     * The RestSeatingFacility
     */
    public void setRestSeatingFacility(String restSeatingFacility) {
        this.restSeatingFacility = restSeatingFacility;
    }

    /**
     *
     * @return
     * The restToilet
     */
    public String getRestToilet() {
        return restToilet;
    }

    /**
     *
     * @param restToilet
     * The RestToilet
     */
    public void setRestToilet(String restToilet) {
        this.restToilet = restToilet;
    }

    /**
     *
     * @return
     * The restCreateDate
     */
    public String getRestCreateDate() {
        return restCreateDate;
    }

    /**
     *
     * @param restCreateDate
     * The RestCreateDate
     */
    public void setRestCreateDate(String restCreateDate) {
        this.restCreateDate = restCreateDate;
    }

    /**
     *
     * @return
     * The restUpdateDate
     */
    public String getRestUpdateDate() {
        return restUpdateDate;
    }

    /**
     *
     * @param restUpdateDate
     * The RestUpdateDate
     */
    public void setRestUpdateDate(String restUpdateDate) {
        this.restUpdateDate = restUpdateDate;
    }

    /**
     *
     * @return
     * The restHostDetail
     */
    public String getRestHostDetail() {
        return restHostDetail;
    }

    /**
     *
     * @param restHostDetail
     * The RestHostDetail
     */
    public void setRestHostDetail(String restHostDetail) {
        this.restHostDetail = restHostDetail;
    }

    /**
     *
     * @return
     * The restAdminId
     */
    public Integer getRestAdminId() {
        return restAdminId;
    }

    /**
     *
     * @param restAdminId
     * The RestAdminId
     */
    public void setRestAdminId(Integer restAdminId) {
        this.restAdminId = restAdminId;
    }

    /**
     *
     * @return
     * The restIsactive
     */
    public Boolean getRestIsactive() {
        return restIsactive;
    }

    /**
     *
     * @param restIsactive
     * The RestIsactive
     */
    public void setRestIsactive(Boolean restIsactive) {
        this.restIsactive = restIsactive;
    }

    /**
     *
     * @return
     * The serContactPerson
     */
    public String getSerContactPerson() {
        if(serContactPerson==null)
        {
            return  "";
        }else {
            return serContactPerson;
        }
    }

    /**
     *
     * @param serContactPerson
     * The SerContactPerson
     */
    public void setSerContactPerson(String serContactPerson) {
        this.serContactPerson = serContactPerson;
    }

    /**
     *
     * @return
     * The serCustCareNo
     */
    public String getSerCustCareNo() {
        if(serCustCareNo==null)
        {
            return  "";
        }else {
            return serCustCareNo;
        }
    }

    /**
     *
     * @param serCustCareNo
     * The SerCustCareNo
     */
    public void setSerCustCareNo(String serCustCareNo) {
        this.serCustCareNo = serCustCareNo;
    }

    /**
     *
     * @return
     * The serPostalCode
     */
    public String getSerPostalCode() {
        return serPostalCode;
    }

    /**
     *
     * @param serPostalCode
     * The SerPostalCode
     */
    public void setSerPostalCode(String serPostalCode) {
        this.serPostalCode = serPostalCode;
    }
    @Generated("org.jsonschema2pojo")
    public class SerOtherFacilityList {

        @SerializedName("FacSerId")
        @Expose
        private Integer facSerId;
        @SerializedName("FacSerName")
        @Expose
        private String facSerName;
        @SerializedName("FacSerCategoryNames")
        @Expose
        private String facSerCategoryNames;
        @SerializedName("FacSerSelectedIcon")
        @Expose
        private String facSerSelectedIcon;
        @SerializedName("FacSerUnSelectedIcon")
        @Expose
        private String facSerUnSelectedIcon;
        @SerializedName("FacSerIsActive")
        @Expose
        private Boolean facSerIsActive;
        @SerializedName("FacSerCreateDate")
        @Expose
        private String facSerCreateDate;
        @SerializedName("FacSerUpdateDate")
        @Expose
        private String facSerUpdateDate;

        /**
         *
         * @return
         * The facSerId
         */
        public Integer getFacSerId() {
            return facSerId;
        }

        /**
         *
         * @param facSerId
         * The FacSerId
         */
        public void setFacSerId(Integer facSerId) {
            this.facSerId = facSerId;
        }

        /**
         *
         * @return
         * The facSerName
         */
        public String getFacSerName() {
            return facSerName;
        }

        /**
         *
         * @param facSerName
         * The FacSerName
         */
        public void setFacSerName(String facSerName) {
            this.facSerName = facSerName;
        }

        /**
         *
         * @return
         * The facSerCategoryNames
         */
        public String getFacSerCategoryNames() {
            return facSerCategoryNames;
        }

        /**
         *
         * @param facSerCategoryNames
         * The FacSerCategoryNames
         */
        public void setFacSerCategoryNames(String facSerCategoryNames) {
            this.facSerCategoryNames = facSerCategoryNames;
        }

        /**
         *
         * @return
         * The facSerSelectedIcon
         */
        public String getFacSerSelectedIcon() {
            return facSerSelectedIcon;
        }

        /**
         *
         * @param facSerSelectedIcon
         * The FacSerSelectedIcon
         */
        public void setFacSerSelectedIcon(String facSerSelectedIcon) {
            this.facSerSelectedIcon = facSerSelectedIcon;
        }

        /**
         *
         * @return
         * The facSerUnSelectedIcon
         */
        public String getFacSerUnSelectedIcon() {
            return facSerUnSelectedIcon;
        }

        /**
         *
         * @param facSerUnSelectedIcon
         * The FacSerUnSelectedIcon
         */
        public void setFacSerUnSelectedIcon(String facSerUnSelectedIcon) {
            this.facSerUnSelectedIcon = facSerUnSelectedIcon;
        }

        /**
         *
         * @return
         * The facSerIsActive
         */
        public Boolean getFacSerIsActive() {
            return facSerIsActive;
        }

        /**
         *
         * @param facSerIsActive
         * The FacSerIsActive
         */
        public void setFacSerIsActive(Boolean facSerIsActive) {
            this.facSerIsActive = facSerIsActive;
        }

        /**
         *
         * @return
         * The facSerCreateDate
         */
        public String getFacSerCreateDate() {
            return facSerCreateDate;
        }

        /**
         *
         * @param facSerCreateDate
         * The FacSerCreateDate
         */
        public void setFacSerCreateDate(String facSerCreateDate) {
            this.facSerCreateDate = facSerCreateDate;
        }

        /**
         *
         * @return
         * The facSerUpdateDate
         */
        public String getFacSerUpdateDate() {
            return facSerUpdateDate;
        }

        /**
         *
         * @param facSerUpdateDate
         * The FacSerUpdateDate
         */
        public void setFacSerUpdateDate(String facSerUpdateDate) {
            this.facSerUpdateDate = facSerUpdateDate;
        }



    }



            @Generated("org.jsonschema2pojo")
            public class GetACServiceLists {

                @SerializedName("Id")
                @Expose
                private Integer id;
                @SerializedName("Name")
                @Expose
                private String name;

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The Id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The name
                 */
                public String getName() {
                    return name;
                }

                /**
                 *
                 * @param name
                 * The Name
                 */
                public void setName(String name) {
                    this.name = name;
                }

            }


            @Generated("org.jsonschema2pojo")
            public class GetCuisineLists {

                @SerializedName("CuisineId")
                @Expose
                private Integer cuisineId;
                @SerializedName("CuisineName")
                @Expose
                private String cuisineName;

                /**
                 *
                 * @return
                 * The cuisineId
                 */
                public Integer getCuisineId() {
                    return cuisineId;
                }

                /**
                 *
                 * @param cuisineId
                 * The CuisineId
                 */
                public void setCuisineId(Integer cuisineId) {
                    this.cuisineId = cuisineId;
                }

                /**
                 *
                 * @return
                 * The cuisineName
                 */
                public String getCuisineName() {
                    return cuisineName;
                }

                /**
                 *
                 * @param cuisineName
                 * The CuisineName
                 */
                public void setCuisineName(String cuisineName) {
                    this.cuisineName = cuisineName;
                }

            }


            @Generated("org.jsonschema2pojo")
            public class GetFoodTypeLists {

                @SerializedName("FoodId")
                @Expose
                private Integer foodId;
                @SerializedName("FoodTypeName")
                @Expose
                private String foodTypeName;

                /**
                 *
                 * @return
                 * The foodId
                 */
                public Integer getFoodId() {
                    return foodId;
                }

                /**
                 *
                 * @param foodId
                 * The FoodId
                 */
                public void setFoodId(Integer foodId) {
                    this.foodId = foodId;
                }

                /**
                 *
                 * @return
                 * The foodTypeName
                 */
                public String getFoodTypeName() {
                    return foodTypeName;
                }

                /**
                 *
                 * @param foodTypeName
                 * The FoodTypeName
                 */
                public void setFoodTypeName(String foodTypeName) {
                    this.foodTypeName = foodTypeName;
                }

            }




            @Generated("org.jsonschema2pojo")
            public class GetToiletTypesList {

                @SerializedName("ToiletID")
                @Expose
                private Integer toiletID;
                @SerializedName("ToiletName")
                @Expose
                private String toiletName;

                /**
                 *
                 * @return
                 * The toiletID
                 */
                public Integer getToiletID() {
                    return toiletID;
                }

                /**
                 *
                 * @param toiletID
                 * The ToiletID
                 */
                public void setToiletID(Integer toiletID) {
                    this.toiletID = toiletID;
                }

                /**
                 *
                 * @return
                 * The toiletName
                 */
                public String getToiletName() {
                    return toiletName;
                }

                /**
                 *
                 * @param toiletName
                 * The ToiletName
                 */
                public void setToiletName(String toiletName) {
                    this.toiletName = toiletName;
                }

            }

            @Generated("org.jsonschema2pojo")
            public class GetFuelsList {

                @SerializedName("FuelID")
                @Expose
                private Integer fuelID;
                @SerializedName("FuelName")
                @Expose
                private String fuelName;

                /**
                 *
                 * @return
                 * The fuelID
                 */
                public Integer getFuelID() {
                    return fuelID;
                }

                /**
                 *
                 * @param fuelID
                 * The FuelID
                 */
                public void setFuelID(Integer fuelID) {
                    this.fuelID = fuelID;
                }

                /**
                 *
                 * @return
                 * The fuelName
                 */
                public String getFuelName() {
                    return fuelName;
                }

                /**
                 *
                 * @param fuelName
                 * The FuelName
                 */
                public void setFuelName(String fuelName) {
                    this.fuelName = fuelName;
                }

            }




            @Generated("org.jsonschema2pojo")
            public class GetFuelsTypeList {

                @SerializedName("FuelTypeID")
                @Expose
                private Integer fuelTypeID;
                @SerializedName("FuelTypeName")
                @Expose
                private String fuelTypeName;

                /**
                 *
                 * @return
                 * The fuelTypeID
                 */
                public Integer getFuelTypeID() {
                    return fuelTypeID;
                }

                /**
                 *
                 * @param fuelTypeID
                 * The FuelTypeID
                 */
                public void setFuelTypeID(Integer fuelTypeID) {
                    this.fuelTypeID = fuelTypeID;
                }

                /**
                 *
                 * @return
                 * The fuelTypeName
                 */
                public String getFuelTypeName() {
                    return fuelTypeName;
                }

                /**
                 *
                 * @param fuelTypeName
                 * The FuelTypeName
                 */
                public void setFuelTypeName(String fuelTypeName) {
                    this.fuelTypeName = fuelTypeName;
                }

            }




            @Generated("org.jsonschema2pojo")
            public class GetResSeatingFacilitiesList {

                @SerializedName("Id")
                @Expose
                private Integer id;
                @SerializedName("values")
                @Expose
                private String values;

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The Id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The values
                 */
                public String getValues() {
                    return values;
                }

                /**
                 *
                 * @param values
                 * The values
                 */
                public void setValues(String values) {
                    this.values = values;
                }

            }



            @Generated("org.jsonschema2pojo")
            public class GetResAdditionalFacilitiesList {

                @SerializedName("Id")
                @Expose
                private Integer id;
                @SerializedName("values")
                @Expose
                private String values;

                /**
                 *
                 * @return
                 * The id
                 */
                public Integer getId() {
                    return id;
                }

                /**
                 *
                 * @param id
                 * The Id
                 */
                public void setId(Integer id) {
                    this.id = id;
                }

                /**
                 *
                 * @return
                 * The values
                 */
                public String getValues() {
                    return values;
                }

                /**
                 *
                 * @param values
                 * The values
                 */
                public void setValues(String values) {
                    this.values = values;
                }

            }




            @Generated("org.jsonschema2pojo")
            public class GarageWheelerTypesList {

                @SerializedName("GarageWheelerTypeID")
                @Expose
                private Integer garageWheelerTypeID;
                @SerializedName("GarageWheelerTypeName")
                @Expose
                private String garageWheelerTypeName;

                /**
                 *
                 * @return
                 * The garageWheelerTypeID
                 */
                public Integer getGarageWheelerTypeID() {
                    return garageWheelerTypeID;
                }

                /**
                 *
                 * @param garageWheelerTypeID
                 * The GarageWheelerTypeID
                 */
                public void setGarageWheelerTypeID(Integer garageWheelerTypeID) {
                    this.garageWheelerTypeID = garageWheelerTypeID;
                }

                /**
                 *
                 * @return
                 * The garageWheelerTypeName
                 */
                public String getGarageWheelerTypeName() {
                    return garageWheelerTypeName;
                }

                /**
                 *
                 * @param garageWheelerTypeName
                 * The GarageWheelerTypeName
                 */
                public void setGarageWheelerTypeName(String garageWheelerTypeName) {
                    this.garageWheelerTypeName = garageWheelerTypeName;
                }

            }
}