package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by ADMIN on 10/3/2016.
 */

@Generated("org.jsonschema2pojo")
public class OTP {

    @SerializedName("umId")
    @Expose
    private Integer umId;
    @SerializedName("umFirstName")
    @Expose
    private String umFirstName;
    @SerializedName("umMyUniqueReferralCode")
    @Expose
    private String umMyUniqueReferralCode;
    @SerializedName("umLastName")
    @Expose
    private String umLastName;
    @SerializedName("umMobile")
    @Expose
    private String umMobile;
    @SerializedName("umEmailId")
    @Expose
    private String umEmailId;
    @SerializedName("umTitle")
    @Expose
    private String umTitle;
    @SerializedName("umDOB")
    @Expose
    private String umDOB;
    @SerializedName("umPetroCard")
    @Expose
    private String umPetroCard;
    @SerializedName("umCreatedDate")
    @Expose
    private String umCreatedDate;
    @SerializedName("umUpdateDate")
    @Expose
    private String umUpdateDate;
    @SerializedName("umLastLoginDate")
    @Expose
    private String umLastLoginDate;
    @SerializedName("umIsActive")
    @Expose
    private Boolean umIsActive;
    @SerializedName("umIsDeleted")
    @Expose
    private Boolean umIsDeleted;
    @SerializedName("umPassword")
    @Expose
    private String umPassword;
    @SerializedName("umOTP")
    @Expose
    private String umOTP;
    @SerializedName("umLat")
    @Expose
    private String umLat;
    @SerializedName("umLong")
    @Expose
    private String umLong;
    @SerializedName("umUdid")
    @Expose
    private String umUdid;
    @SerializedName("umIsPushOn")
    @Expose
    private Boolean umIsPushOn;
    @SerializedName("umReferralCode")
    @Expose
    private String umReferralCode;
    @SerializedName("umDeviceType")
    @Expose
    private String umDeviceType;
    @SerializedName("umProfilePhoto")
    @Expose
    private String umProfilePhoto;
    @SerializedName("umFacebookToken")
    @Expose
    private String umFacebookToken;
    @SerializedName("umGoogleToken")
    @Expose
    private String umGoogleToken;
    @SerializedName("umDescription")
    @Expose
    private String umDescription;
    @SerializedName("umLocation")
    @Expose
    private String umLocation;
    @SerializedName("umEmrgncyName")
    @Expose
    private String umEmrgncyName;
    @SerializedName("umEmrgncyNumber")
    @Expose
    private String umEmrgncyNumber;
    @SerializedName("umCoTraveller")
    @Expose
    private String umCoTraveller;
    @SerializedName("umPreferredType")
    @Expose
    private String umPreferredType;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private String resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;

    public String getUmRSAId() {
        return umRSAId;
    }

    public void setUmRSAId(String umRSAId) {
        this.umRSAId = umRSAId;
    }

    @SerializedName("umRSAId")
    @Expose
    private String umRSAId;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The umId
     */
    public Integer getUmId() {
        return umId;
    }

    /**
     *
     * @param umId
     * The umId
     */
    public void setUmId(Integer umId) {
        this.umId = umId;
    }

    /**
     *
     * @return
     * The umFirstName
     */
    public String getUmFirstName() {
        return umFirstName;
    }

    /**
     *
     * @param umFirstName
     * The umFirstName
     */
    public void setUmFirstName(String umFirstName) {
        this.umFirstName = umFirstName;
    }

    /**
     *
     * @return
     * The umLastName
     */
    public String getUmLastName() {
        return umLastName;
    }

    /**
     *
     * @param umLastName
     * The umLastName
     */
    public void setUmLastName(String umLastName) {
        this.umLastName = umLastName;
    }

    /**
     *
     * @return
     * The umMobile
     */
    public String getUmMobile() {
        return umMobile;
    }

    /**
     *
     * @param umMobile
     * The umMobile
     */
    public void setUmMobile(String umMobile) {
        this.umMobile = umMobile;
    }

    /**
     *
     * @return
     * The umEmailId
     */
    public String getUmEmailId() {
        return umEmailId;
    }

    /**
     *
     * @param umEmailId
     * The umEmailId
     */
    public void setUmEmailId(String umEmailId) {
        this.umEmailId = umEmailId;
    }

    /**
     *
     * @return
     * The umTitle
     */
    public String getUmTitle() {
        return umTitle;
    }

    /**
     *
     * @param umTitle
     * The umTitle
     */
    public void setUmTitle(String umTitle) {
        this.umTitle = umTitle;
    }

    /**
     *
     * @return
     * The umDOB
     */
    public String getUmDOB() {
        return umDOB;
    }

    /**
     *
     * @param umDOB
     * The umDOB
     */
    public void setUmDOB(String umDOB) {
        this.umDOB = umDOB;
    }

    /**
     *
     * @return
     * The umPetroCard
     */
    public String getUmPetroCard() {
        if(umPetroCard==null)
        {
            return "";
        }else {
            return umPetroCard;
        }
    }

    /**
     *
     * @param umPetroCard
     * The umPetroCard
     */
    public void setUmPetroCard(String umPetroCard) {
        this.umPetroCard = umPetroCard;
    }

    /**
     *
     * @return
     * The umCreatedDate
     */
    public String getUmCreatedDate() {
        return umCreatedDate;
    }

    /**
     *
     * @param umCreatedDate
     * The umCreatedDate
     */
    public void setUmCreatedDate(String umCreatedDate) {
        this.umCreatedDate = umCreatedDate;
    }

    /**
     *
     * @return
     * The umUpdateDate
     */
    public String getUmUpdateDate() {
        return umUpdateDate;
    }

    /**
     *
     * @param umUpdateDate
     * The umUpdateDate
     */
    public void setUmUpdateDate(String umUpdateDate) {
        this.umUpdateDate = umUpdateDate;
    }

    /**
     *
     * @return
     * The umLastLoginDate
     */
    public String getUmLastLoginDate() {
        return umLastLoginDate;
    }

    /**
     *
     * @param umLastLoginDate
     * The umLastLoginDate
     */
    public void setUmLastLoginDate(String umLastLoginDate) {
        this.umLastLoginDate = umLastLoginDate;
    }

    /**
     *
     * @return
     * The umIsActive
     */
    public Boolean getUmIsActive() {
        return umIsActive;
    }

    /**
     *
     * @param umIsActive
     * The umIsActive
     */
    public void setUmIsActive(Boolean umIsActive) {
        this.umIsActive = umIsActive;
    }

    /**
     *
     * @return
     * The umIsDeleted
     */
    public Boolean getUmIsDeleted() {
        return umIsDeleted;
    }

    /**
     *
     * @param umIsDeleted
     * The umIsDeleted
     */
    public void setUmIsDeleted(Boolean umIsDeleted) {
        this.umIsDeleted = umIsDeleted;
    }

    /**
     *
     * @return
     * The umPassword
     */
    public String getUmPassword() {
        return umPassword;
    }

    /**
     *
     * @param umPassword
     * The umPassword
     */
    public void setUmPassword(String umPassword) {
        this.umPassword = umPassword;
    }

    /**
     *
     * @return
     * The umOTP
     */
    public String getUmOTP() {
        return umOTP;
    }

    /**
     *
     * @param umOTP
     * The umOTP
     */
    public void setUmOTP(String umOTP) {
        this.umOTP = umOTP;
    }

    /**
     *
     * @return
     * The umLat
     */
    public String getUmLat() {
        if(umLat==null) {
            return "";
        }else
        {
            return umLat;

        }
    }

    /**
     *
     * @param umLat
     * The umLat
     */
    public void setUmLat(String umLat) {
        this.umLat = umLat;
    }

    /**
     *
     * @return
     * The umLong
     */
    public String getUmLong() {
        if(umLong==null)
        {
            return "";
        }else {
            return umLong;
        }
    }

    /**
     *
     * @param umLong
     * The umLong
     */
    public void setUmLong(String umLong) {
        this.umLong = umLong;
    }

    /**
     *
     * @return
     * The umUdid
     */
    public String getUmUdid() {
        return umUdid;
    }

    /**
     *
     * @param umUdid
     * The umUdid
     */
    public void setUmUdid(String umUdid) {
        this.umUdid = umUdid;
    }

    /**
     *
     * @return
     * The umIsPushOn
     */
    public Boolean getUmIsPushOn() {
        return umIsPushOn;
    }

    /**
     *
     * @param umIsPushOn
     * The umIsPushOn
     */
    public void setUmIsPushOn(Boolean umIsPushOn) {
        this.umIsPushOn = umIsPushOn;
    }

    /**
     *
     * @return
     * The umReferralCode
     */
    public String getUmReferralCode() {
        return umReferralCode;
    }

    /**
     *
     * @param umReferralCode
     * The umReferralCode
     */
    public void setUmReferralCode(String umReferralCode) {
        this.umReferralCode = umReferralCode;
    }

    /**
     *
     * @return
     * The umDeviceType
     */
    public String getUmDeviceType() {
        return umDeviceType;
    }

    /**
     *
     * @param umDeviceType
     * The umDeviceType
     */
    public void setUmDeviceType(String umDeviceType) {
        this.umDeviceType = umDeviceType;
    }

    /**
     *
     * @return
     * The umProfilePhoto
     */
    public String getUmProfilePhoto() {
        return umProfilePhoto;
    }

    /**
     *
     * @param umProfilePhoto
     * The umProfilePhoto
     */
    public void setUmProfilePhoto(String umProfilePhoto) {
        this.umProfilePhoto = umProfilePhoto;
    }

    /**
     *
     * @return
     * The umFacebookToken
     */
    public String getUmFacebookToken() {
        return umFacebookToken;
    }

    /**
     *
     * @param umFacebookToken
     * The umFacebookToken
     */
    public void setUmFacebookToken(String umFacebookToken) {
        this.umFacebookToken = umFacebookToken;
    }

    /**
     *
     * @return
     * The umGoogleToken
     */
    public String getUmGoogleToken() {
        return umGoogleToken;
    }

    /**
     *
     * @param umGoogleToken
     * The umGoogleToken
     */
    public void setUmGoogleToken(String umGoogleToken) {
        this.umGoogleToken = umGoogleToken;
    }

    /**
     *
     * @return
     * The umDescription
     */
    public String getUmDescription() {
        return umDescription;
    }

    /**
     *
     * @param umDescription
     * The umDescription
     */
    public void setUmDescription(String umDescription) {
        this.umDescription = umDescription;
    }

    /**
     *
     * @return
     * The umLocation
     */
    public String getUmLocation() {
        return umLocation;
    }

    /**
     *
     * @param umLocation
     * The umLocation
     */
    public void setUmLocation(String umLocation) {
        this.umLocation = umLocation;
    }

    /**
     *
     * @return
     * The umEmrgncyName
     */
    public String getUmEmrgncyName() {
        return umEmrgncyName;
    }

    /**
     *
     * @param umEmrgncyName
     * The umEmrgncyName
     */
    public void setUmEmrgncyName(String umEmrgncyName) {
        this.umEmrgncyName = umEmrgncyName;
    }

    /**
     *
     * @return
     * The umEmrgncyNumber
     */
    public String getUmEmrgncyNumber() {
        return umEmrgncyNumber;
    }

    /**
     *
     * @param umEmrgncyNumber
     * The umEmrgncyNumber
     */
    public void setUmEmrgncyNumber(String umEmrgncyNumber) {
        this.umEmrgncyNumber = umEmrgncyNumber;
    }

    /**
     *
     * @return
     * The umCoTraveller
     */
    public String getUmCoTraveller() {
        if(umCoTraveller == null)
        {
            return "";
        }
        else
        {
            return umCoTraveller;
        }

    }

    /**
     *
     * @param umCoTraveller
     * The umCoTraveller
     */
    public void setUmCoTraveller(String umCoTraveller) {
        this.umCoTraveller = umCoTraveller;
    }


    /**
     *
     * @return
     * The umPreferredType
     */
    public String getUmPreferredType() {

        if(umPreferredType == null)
        {
            return "";
        }
        else
        {
            return umPreferredType;
        }

    }

    /**
     *
     * @param umPreferredType
     * The umPreferredType
     */
    public void setUmPreferredType(String umPreferredType) {

        this.umPreferredType = umPreferredType;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public String getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(String resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }
    /**
     *
     * @return
     * The umMyUniqueReferralCode
     */
    public String getumMyUniqueReferralCode() {
        return umMyUniqueReferralCode;
    }

    /**
     *
     * @param umMyUniqueReferralCode
     * The umMyUniqueReferralCode
     */
    public void setumMyUniqueReferralCode(String umMyUniqueReferralCode) {
        this.umMyUniqueReferralCode = umMyUniqueReferralCode;
    }

}