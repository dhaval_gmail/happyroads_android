package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by admin on 21/11/2016.
 */


@Generated("org.jsonschema2pojo")
public class InviteMyFrined {

    @SerializedName("dsId")
    @Expose
    private Integer dsId;
    @SerializedName("dsStringName")
    @Expose
    private String dsStringName;
    @SerializedName("dsStringValue")
    @Expose
    private String dsStringValue;
    @SerializedName("dsIsActive")
    @Expose
    private Boolean dsIsActive;
    @SerializedName("dsIsDelete")
    @Expose
    private Boolean dsIsDelete;
    @SerializedName("dsCreatedDate")
    @Expose
    private String dsCreatedDate;
    @SerializedName("dsUpdateDate")
    @Expose
    private String dsUpdateDate;

    /**
     *
     * @return
     * The dsId
     */
    public Integer getDsId() {
        return dsId;
    }

    /**
     *
     * @param dsId
     * The dsId
     */
    public void setDsId(Integer dsId) {
        this.dsId = dsId;
    }

    /**
     *
     * @return
     * The dsStringName
     */
    public String getDsStringName() {
        return dsStringName;
    }

    /**
     *
     * @param dsStringName
     * The dsStringName
     */
    public void setDsStringName(String dsStringName) {
        this.dsStringName = dsStringName;
    }

    /**
     *
     * @return
     * The dsStringValue
     */
    public String getDsStringValue() {
        return dsStringValue;
    }

    /**
     *
     * @param dsStringValue
     * The dsStringValue
     */
    public void setDsStringValue(String dsStringValue) {
        this.dsStringValue = dsStringValue;
    }

    /**
     *
     * @return
     * The dsIsActive
     */
    public Boolean getDsIsActive() {
        return dsIsActive;
    }

    /**
     *
     * @param dsIsActive
     * The dsIsActive
     */
    public void setDsIsActive(Boolean dsIsActive) {
        this.dsIsActive = dsIsActive;
    }

    /**
     *
     * @return
     * The dsIsDelete
     */
    public Boolean getDsIsDelete() {
        return dsIsDelete;
    }

    /**
     *
     * @param dsIsDelete
     * The dsIsDelete
     */
    public void setDsIsDelete(Boolean dsIsDelete) {
        this.dsIsDelete = dsIsDelete;
    }

    /**
     *
     * @return
     * The dsCreatedDate
     */
    public String getDsCreatedDate() {
        return dsCreatedDate;
    }

    /**
     *
     * @param dsCreatedDate
     * The dsCreatedDate
     */
    public void setDsCreatedDate(String dsCreatedDate) {
        this.dsCreatedDate = dsCreatedDate;
    }

    /**
     *
     * @return
     * The dsUpdateDate
     */
    public String getDsUpdateDate() {
        return dsUpdateDate;
    }

    /**
     *
     * @param dsUpdateDate
     * The dsUpdateDate
     */
    public void setDsUpdateDate(String dsUpdateDate) {
        this.dsUpdateDate = dsUpdateDate;
    }

}