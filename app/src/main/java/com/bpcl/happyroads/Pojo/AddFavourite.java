package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 10/15/2016.
 */
public class AddFavourite {

    @SerializedName("FavId")
    @Expose
    private Integer favId;
    @SerializedName("FavRefId")
    @Expose
    private Integer favRefId;
    @SerializedName("FavUmId")
    @Expose
    private Integer favUmId;
    @SerializedName("FavCreatedDate")
    @Expose
    private String favCreatedDate;
    @SerializedName("FavType")
    @Expose
    private String favType;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private String resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
       @SerializedName("radius")
    @Expose
    private Integer radius;



    /**
     *
     * @return
     * The favId
     */
    public Integer getFavId() {
        return favId;
    }

    /**
     *
     * @param favId
     * The FavId
     */
    public void setFavId(Integer favId) {
        this.favId = favId;
    }

    /**
     *
     * @return
     * The favRefId
     */
    public Integer getFavRefId() {
        return favRefId;
    }

    /**
     *
     * @param favRefId
     * The FavRefId
     */
    public void setFavRefId(Integer favRefId) {
        this.favRefId = favRefId;
    }

    /**
     *
     * @return
     * The favUmId
     */
    public Integer getFavUmId() {
        return favUmId;
    }

    /**
     *
     * @param favUmId
     * The FavUmId
     */
    public void setFavUmId(Integer favUmId) {
        this.favUmId = favUmId;
    }

    /**
     *
     * @return
     * The favCreatedDate
     */
    public String getFavCreatedDate() {
        return favCreatedDate;
    }

    /**
     *
     * @param favCreatedDate
     * The FavCreatedDate
     */
    public void setFavCreatedDate(String favCreatedDate) {
        this.favCreatedDate = favCreatedDate;
    }

    /**
     *
     * @return
     * The favType
     */
    public String getFavType() {
        return favType;
    }

    /**
     *
     * @param favType
     * The FavType
     */
    public void setFavType(String favType) {
        this.favType = favType;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public String getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(String resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}
