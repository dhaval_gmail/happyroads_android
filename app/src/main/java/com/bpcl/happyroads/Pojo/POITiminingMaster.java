package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 10/20/2016.
 */
public class POITiminingMaster {

    @SerializedName("TimingId")
    @Expose
    private Integer timingId;
    @SerializedName("POITiminingId")
    @Expose
    private Integer pOITiminingId;
    @SerializedName("POITimingType")
    @Expose
    private String pOITimingType;
    @SerializedName("POIMorningFrom")
    @Expose
    private String pOIMorningFrom;
    @SerializedName("POIMorningTo")
    @Expose
    private String pOIMorningTo;
    @SerializedName("POIEveningFrom")
    @Expose
    private String pOIEveningFrom;
    @SerializedName("POIEveningTo")
    @Expose
    private String pOIEveningTo;
    @SerializedName("MorningFrom")
    @Expose
    private Object morningFrom;
    @SerializedName("MorningTo")
    @Expose
    private Object morningTo;
    @SerializedName("EveningFrom")
    @Expose
    private Object eveningFrom;
    @SerializedName("EveningTo")
    @Expose
    private Object eveningTo;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private Object resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("resHttpStatus")
    @Expose
    private Object resHttpStatus;
    @SerializedName("resStatusDescription")
    @Expose
    private Object resStatusDescription;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("Search")
    @Expose
    private Object search;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("CurrentLat")
    @Expose
    private Object currentLat;
    @SerializedName("CurrentLong")
    @Expose
    private Object currentLong;

    /**
     *
     * @return
     * The timingId
     */
    public Integer getTimingId() {
        return timingId;
    }

    /**
     *
     * @param timingId
     * The TimingId
     */
    public void setTimingId(Integer timingId) {
        this.timingId = timingId;
    }

    /**
     *
     * @return
     * The pOITiminingId
     */
    public Integer getPOITiminingId() {
        return pOITiminingId;
    }

    /**
     *
     * @param pOITiminingId
     * The POITiminingId
     */
    public void setPOITiminingId(Integer pOITiminingId) {
        this.pOITiminingId = pOITiminingId;
    }

    /**
     *
     * @return
     * The pOITimingType
     */
    public String getPOITimingType() {
        return pOITimingType;
    }

    /**
     *
     * @param pOITimingType
     * The POITimingType
     */
    public void setPOITimingType(String pOITimingType) {
        this.pOITimingType = pOITimingType;
    }

    /**
     *
     * @return
     * The pOIMorningFrom
     */
    public String getPOIMorningFrom() {
        return pOIMorningFrom;
    }

    /**
     *
     * @param pOIMorningFrom
     * The POIMorningFrom
     */
    public void setPOIMorningFrom(String pOIMorningFrom) {
        this.pOIMorningFrom = pOIMorningFrom;
    }

    /**
     *
     * @return
     * The pOIMorningTo
     */
    public String getPOIMorningTo() {
        return pOIMorningTo;
    }

    /**
     *
     * @param pOIMorningTo
     * The POIMorningTo
     */
    public void setPOIMorningTo(String pOIMorningTo) {
        this.pOIMorningTo = pOIMorningTo;
    }

    /**
     *
     * @return
     * The pOIEveningFrom
     */
    public String getPOIEveningFrom() {
        return pOIEveningFrom;
    }

    /**
     *
     * @param pOIEveningFrom
     * The POIEveningFrom
     */
    public void setPOIEveningFrom(String pOIEveningFrom) {
        this.pOIEveningFrom = pOIEveningFrom;
    }

    /**
     *
     * @return
     * The pOIEveningTo
     */
    public String getPOIEveningTo() {
        return pOIEveningTo;
    }

    /**
     *
     * @param pOIEveningTo
     * The POIEveningTo
     */
    public void setPOIEveningTo(String pOIEveningTo) {
        this.pOIEveningTo = pOIEveningTo;
    }

    /**
     *
     * @return
     * The morningFrom
     */
    public Object getMorningFrom() {
        return morningFrom;
    }

    /**
     *
     * @param morningFrom
     * The MorningFrom
     */
    public void setMorningFrom(Object morningFrom) {
        this.morningFrom = morningFrom;
    }

    /**
     *
     * @return
     * The morningTo
     */
    public Object getMorningTo() {
        return morningTo;
    }

    /**
     *
     * @param morningTo
     * The MorningTo
     */
    public void setMorningTo(Object morningTo) {
        this.morningTo = morningTo;
    }

    /**
     *
     * @return
     * The eveningFrom
     */
    public Object getEveningFrom() {
        return eveningFrom;
    }

    /**
     *
     * @param eveningFrom
     * The EveningFrom
     */
    public void setEveningFrom(Object eveningFrom) {
        this.eveningFrom = eveningFrom;
    }

    /**
     *
     * @return
     * The eveningTo
     */
    public Object getEveningTo() {
        return eveningTo;
    }

    /**
     *
     * @param eveningTo
     * The EveningTo
     */
    public void setEveningTo(Object eveningTo) {
        this.eveningTo = eveningTo;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public Object getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(Object resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The resHttpStatus
     */
    public Object getResHttpStatus() {
        return resHttpStatus;
    }

    /**
     *
     * @param resHttpStatus
     * The resHttpStatus
     */
    public void setResHttpStatus(Object resHttpStatus) {
        this.resHttpStatus = resHttpStatus;
    }

    /**
     *
     * @return
     * The resStatusDescription
     */
    public Object getResStatusDescription() {
        return resStatusDescription;
    }

    /**
     *
     * @param resStatusDescription
     * The resStatusDescription
     */
    public void setResStatusDescription(Object resStatusDescription) {
        this.resStatusDescription = resStatusDescription;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The search
     */
    public Object getSearch() {
        return search;
    }

    /**
     *
     * @param search
     * The Search
     */
    public void setSearch(Object search) {
        this.search = search;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    /**
     *
     * @return
     * The currentLat
     */
    public Object getCurrentLat() {
        return currentLat;
    }

    /**
     *
     * @param currentLat
     * The CurrentLat
     */
    public void setCurrentLat(Object currentLat) {
        this.currentLat = currentLat;
    }

    /**
     *
     * @return
     * The currentLong
     */
    public Object getCurrentLong() {
        return currentLong;
    }

    /**
     *
     * @param currentLong
     * The CurrentLong
     */
    public void setCurrentLong(Object currentLong) {
        this.currentLong = currentLong;
    }

}

