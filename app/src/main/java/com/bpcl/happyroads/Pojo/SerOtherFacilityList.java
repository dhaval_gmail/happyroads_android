package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 10/25/2016.
 */
public class SerOtherFacilityList {

    @SerializedName("FacSerId")
    @Expose
    private Integer facSerId;
    @SerializedName("FacSerName")
    @Expose
    private String facSerName;
    @SerializedName("FacSerCategoryNames")
    @Expose
    private String facSerCategoryNames;
    @SerializedName("FacSerSelectedIcon")
    @Expose
    private String facSerSelectedIcon;
    @SerializedName("FacSerUnSelectedIcon")
    @Expose
    private String facSerUnSelectedIcon;
    @SerializedName("FacSerIsActive")
    @Expose
    private Boolean facSerIsActive;
    @SerializedName("FacSerCreateDate")
    @Expose
    private String facSerCreateDate;
    @SerializedName("FacSerUpdateDate")
    @Expose
    private String facSerUpdateDate;

    /**
     *
     * @return
     * The facSerId
     */
    public Integer getFacSerId() {
        return facSerId;
    }

    /**
     *
     * @param facSerId
     * The FacSerId
     */
    public void setFacSerId(Integer facSerId) {
        this.facSerId = facSerId;
    }

    /**
     *
     * @return
     * The facSerName
     */
    public String getFacSerName() {
        return facSerName;
    }

    /**
     *
     * @param facSerName
     * The FacSerName
     */
    public void setFacSerName(String facSerName) {
        this.facSerName = facSerName;
    }

    /**
     *
     * @return
     * The facSerCategoryNames
     */
    public String getFacSerCategoryNames() {
        return facSerCategoryNames;
    }

    /**
     *
     * @param facSerCategoryNames
     * The FacSerCategoryNames
     */
    public void setFacSerCategoryNames(String facSerCategoryNames) {
        this.facSerCategoryNames = facSerCategoryNames;
    }

    /**
     *
     * @return
     * The facSerSelectedIcon
     */
    public String getFacSerSelectedIcon() {
        return facSerSelectedIcon;
    }

    /**
     *
     * @param facSerSelectedIcon
     * The FacSerSelectedIcon
     */
    public void setFacSerSelectedIcon(String facSerSelectedIcon) {
        this.facSerSelectedIcon = facSerSelectedIcon;
    }

    /**
     *
     * @return
     * The facSerUnSelectedIcon
     */
    public String getFacSerUnSelectedIcon() {
        return facSerUnSelectedIcon;
    }

    /**
     *
     * @param facSerUnSelectedIcon
     * The FacSerUnSelectedIcon
     */
    public void setFacSerUnSelectedIcon(String facSerUnSelectedIcon) {
        this.facSerUnSelectedIcon = facSerUnSelectedIcon;
    }

    /**
     *
     * @return
     * The facSerIsActive
     */
    public Boolean getFacSerIsActive() {
        return facSerIsActive;
    }

    /**
     *
     * @param facSerIsActive
     * The FacSerIsActive
     */
    public void setFacSerIsActive(Boolean facSerIsActive) {
        this.facSerIsActive = facSerIsActive;
    }

    /**
     *
     * @return
     * The facSerCreateDate
     */
    public String getFacSerCreateDate() {
        return facSerCreateDate;
    }

    /**
     *
     * @param facSerCreateDate
     * The FacSerCreateDate
     */
    public void setFacSerCreateDate(String facSerCreateDate) {
        this.facSerCreateDate = facSerCreateDate;
    }

    /**
     *
     * @return
     * The facSerUpdateDate
     */
    public String getFacSerUpdateDate() {
        return facSerUpdateDate;
    }

    /**
     *
     * @param facSerUpdateDate
     * The FacSerUpdateDate
     */
    public void setFacSerUpdateDate(String facSerUpdateDate) {
        this.facSerUpdateDate = facSerUpdateDate;
    }

}
