package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;



@Generated("org.jsonschema2pojo")
public class ServiceImage {

    @SerializedName("keyName")
    @Expose
    private String keyName;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     *
     * @return
     * The keyName
     */
    public String getKeyName() {
        return keyName;
    }

    /**
     *
     * @param keyName
     * The keyName
     */
    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    /**
     *
     * @return
     * The value
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(String value) {
        this.value = value;
    }

}