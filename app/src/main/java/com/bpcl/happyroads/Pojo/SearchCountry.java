package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 9/27/2016.
 */
public class SearchCountry {

    @SerializedName("CityID")
    @Expose
    private Integer cityID;
    @SerializedName("CityName")
    @Expose
    private String cityName;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;

    /**
     *
     * @return
     * The cityID
     */
    public Integer getCityID() {
        return cityID;
    }

    /**
     *
     * @param cityID
     * The CityID
     */
    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    /**
     *
     * @return
     * The cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     *
     * @param cityName
     * The CityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}

