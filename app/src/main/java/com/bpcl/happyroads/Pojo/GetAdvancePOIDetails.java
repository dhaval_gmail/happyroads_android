package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by ADMIN on 10/17/2016.
 */
public class GetAdvancePOIDetails {

    @SerializedName("AdvId")
    @Expose
    private Integer advId;
    @SerializedName("AdvPOIId")
    @Expose
    private Integer advPOIId;
    @SerializedName("POIEntryFeeCategory")
    @Expose
    private String pOIEntryFeeCategory;
    @SerializedName("POIEntryFeeRegularRates")
    @Expose
    private String pOIEntryFeeRegularRates;
    @SerializedName("POIEntryFeeSeasonRates")
    @Expose
    private String pOIEntryFeeSeasonRates;
    @SerializedName("POIIsPhotoGraphyAllow")
    @Expose
    private Boolean pOIIsPhotoGraphyAllow;
    @SerializedName("POIPhotoGraphyRate")
    @Expose
    private Double pOIPhotoGraphyRate;
    @SerializedName("POIIsParkingFacility")
    @Expose
    private Boolean pOIIsParkingFacility;
    @SerializedName("POIOtherFacility")
    @Expose
    private String pOIOtherFacility;
    @SerializedName("POIToiletFacility")
    @Expose
    private String pOIToiletFacility;
    @SerializedName("AdvPoiCreateDate")
    @Expose
    private String advPoiCreateDate;
    @SerializedName("AdvPoiUpdateDate")
    @Expose
    private String advPoiUpdateDate;
    @SerializedName("AdvPoiHostDetail")
    @Expose
    private String advPoiHostDetail;
    @SerializedName("AdvPoiAdminId")
    @Expose
    private Integer advPoiAdminId;
    @SerializedName("AdvPoiIsActive")
    @Expose
    private Boolean advPoiIsActive;
    @SerializedName("POIIsEntryFee")
    @Expose
    private Boolean pOIIsEntryFee;
    @SerializedName("POIHolidayTypes")
    @Expose
    private String pOIHolidayTypes;
    @SerializedName("POIStateHoliDays")
    @Expose
    private String pOIStateHoliDays;
    @SerializedName("POINationalHoliDays")
    @Expose
    private String pOINationalHoliDays;
    @SerializedName("POIEntryFeeCategoryName")
    @Expose
    private String pOIEntryFeeCategoryName;
    @SerializedName("POILocalHolidayMaster")
    @Expose
    private ArrayList<POILocalHolidayMaster> pOILocalHolidayMaster = new ArrayList<POILocalHolidayMaster>();

    @SerializedName("GetToiletTypesAll")
    @Expose
    private ArrayList<ToiletTypesAll> toiletTypesAllArrayList = new ArrayList<ToiletTypesAll>();


    public ArrayList<ToiletServiceCategoryDetails> getToiletServiceCategoryDetailses() {
        return toiletServiceCategoryDetailses;
    }

    public void setToiletServiceCategoryDetailses(ArrayList<ToiletServiceCategoryDetails> toiletServiceCategoryDetailses) {
        this.toiletServiceCategoryDetailses = toiletServiceCategoryDetailses;
    }

    public ArrayList<ToiletTypesAll> getToiletTypesAllArrayList() {

        return toiletTypesAllArrayList;
    }

    public void setToiletTypesAllArrayList(ArrayList<ToiletTypesAll> toiletTypesAllArrayList) {
        this.toiletTypesAllArrayList = toiletTypesAllArrayList;
    }

    @SerializedName("GetToiletServiceCategoryDetails")
    @Expose
    private ArrayList<ToiletServiceCategoryDetails> toiletServiceCategoryDetailses = new ArrayList<ToiletServiceCategoryDetails>();


    public ArrayList<PoiOtherFacilityList> getPoiOtherFacilityLists() {
        return poiOtherFacilityLists;
    }

    public void setPoiOtherFacilityLists(ArrayList<PoiOtherFacilityList> poiOtherFacilityLists) {
        this.poiOtherFacilityLists = poiOtherFacilityLists;
    }

    @SerializedName("POITiminingMaster")
    @Expose
    private ArrayList<POITiminingMaster> poiTimingPoiTiminingMasters = new ArrayList<POITiminingMaster>();


    @SerializedName("PoiOtherFacilityList")
    @Expose
    private ArrayList<PoiOtherFacilityList> poiOtherFacilityLists = new ArrayList<PoiOtherFacilityList>();
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private Object resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("resHttpStatus")
    @Expose
    private Object resHttpStatus;
    @SerializedName("resStatusDescription")
    @Expose
    private Object resStatusDescription;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("Search")
    @Expose
    private Object search;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("CurrentLat")
    @Expose
    private Object currentLat;
    @SerializedName("CurrentLong")
    @Expose
    private Object currentLong;

    /**
     *
     * @return
     * The advId
     */
    public Integer getAdvId() {
        return advId;
    }

    /**
     *
     * @param advId
     * The AdvId
     */
    public void setAdvId(Integer advId) {
        this.advId = advId;
    }

    /**
     *
     * @return
     * The advPOIId
     */
    public Integer getAdvPOIId() {
        return advPOIId;
    }

    /**
     *
     * @param advPOIId
     * The AdvPOIId
     */
    public void setAdvPOIId(Integer advPOIId) {
        this.advPOIId = advPOIId;
    }

    /**
     *
     * @return
     * The pOIEntryFeeCategory
     */
    public String getPOIEntryFeeCategory() {
        return pOIEntryFeeCategory;
    }

    /**
     *
     * @param pOIEntryFeeCategory
     * The POIEntryFeeCategory
     */
    public void setPOIEntryFeeCategory(String pOIEntryFeeCategory) {
        this.pOIEntryFeeCategory = pOIEntryFeeCategory;
    }

    /**
     *
     * @return
     * The pOIEntryFeeRegularRates
     */
    public String getPOIEntryFeeRegularRates() {
        if(pOIEntryFeeRegularRates==null)
        {
            return "-";
        }else {
            return pOIEntryFeeRegularRates;
        }
    }

    /**
     *
     * @param pOIEntryFeeRegularRates
     * The POIEntryFeeRegularRates
     */
    public void setPOIEntryFeeRegularRates(String pOIEntryFeeRegularRates) {
        this.pOIEntryFeeRegularRates = pOIEntryFeeRegularRates;
    }

    /**
     *
     * @return
     * The pOIEntryFeeSeasonRates
     */
    public String getPOIEntryFeeSeasonRates() {
        if(pOIEntryFeeSeasonRates==null)
        {
            return "-";
        }else {
            return pOIEntryFeeSeasonRates;
        }
    }

    /**
     *
     * @param pOIEntryFeeSeasonRates
     * The POIEntryFeeSeasonRates
     */
    public void setPOIEntryFeeSeasonRates(String pOIEntryFeeSeasonRates) {
        this.pOIEntryFeeSeasonRates = pOIEntryFeeSeasonRates;
    }

    /**
     *
     * @return
     * The pOIIsPhotoGraphyAllow
     */
    public Boolean getPOIIsPhotoGraphyAllow() {
        if(pOIIsPhotoGraphyAllow==null)
        {
            return pOIIsPhotoGraphyAllow=false;
        }else {
            return pOIIsPhotoGraphyAllow;
        }
    }

    /**
     *
     * @param pOIIsPhotoGraphyAllow
     * The POIIsPhotoGraphyAllow
     */
    public void setPOIIsPhotoGraphyAllow(Boolean pOIIsPhotoGraphyAllow) {
        this.pOIIsPhotoGraphyAllow = pOIIsPhotoGraphyAllow;
    }

    /**
     *
     * @return
     * The pOIPhotoGraphyRate
     */
    public Double getPOIPhotoGraphyRate() {
        return pOIPhotoGraphyRate;
    }

    /**
     *
     * @param pOIPhotoGraphyRate
     * The POIPhotoGraphyRate
     */
    public void setPOIPhotoGraphyRate(Double pOIPhotoGraphyRate) {
        this.pOIPhotoGraphyRate = pOIPhotoGraphyRate;
    }

    /**
     *
     * @return
     * The pOIIsParkingFacility
     */
    public Boolean getPOIIsParkingFacility() {
        if(pOIIsParkingFacility==null)
        {
            return pOIIsParkingFacility=false;

        }else {
            return pOIIsParkingFacility;
        }
    }

    /**
     *
     * @param pOIIsParkingFacility
     * The POIIsParkingFacility
     */
    public void setPOIIsParkingFacility(Boolean pOIIsParkingFacility) {
        this.pOIIsParkingFacility = pOIIsParkingFacility;
    }

    /**
     *
     * @return
     * The pOIOtherFacility
     */
    public String getPOIOtherFacility() {
        if(pOIOtherFacility == null)
        {
            return  "";
        }
        else {
            return pOIOtherFacility;
        }
    }

    /**
     *
     * @param pOIOtherFacility
     * The POIOtherFacility
     */
    public void setPOIOtherFacility(String pOIOtherFacility) {
        this.pOIOtherFacility = pOIOtherFacility;
    }

    /**
     *
     * @return
     * The pOIToiletFacility
     */
    public String getPOIToiletFacility() {
        return pOIToiletFacility;
    }

    /**
     *
     * @param pOIToiletFacility
     * The POIToiletFacility
     */
    public void setPOIToiletFacility(String pOIToiletFacility) {
        this.pOIToiletFacility = pOIToiletFacility;
    }

    /**
     *
     * @return
     * The advPoiCreateDate
     */
    public String getAdvPoiCreateDate() {
        return advPoiCreateDate;
    }

    /**
     *
     * @param advPoiCreateDate
     * The AdvPoiCreateDate
     */
    public void setAdvPoiCreateDate(String advPoiCreateDate) {
        this.advPoiCreateDate = advPoiCreateDate;
    }

    /**
     *
     * @return
     * The advPoiUpdateDate
     */
    public String getAdvPoiUpdateDate() {
        return advPoiUpdateDate;
    }

    /**
     *
     * @param advPoiUpdateDate
     * The AdvPoiUpdateDate
     */
    public void setAdvPoiUpdateDate(String advPoiUpdateDate) {
        this.advPoiUpdateDate = advPoiUpdateDate;
    }

    /**
     *
     * @return
     * The advPoiHostDetail
     */
    public String getAdvPoiHostDetail() {
        return advPoiHostDetail;
    }

    /**
     *
     * @param advPoiHostDetail
     * The AdvPoiHostDetail
     */
    public void setAdvPoiHostDetail(String advPoiHostDetail) {
        this.advPoiHostDetail = advPoiHostDetail;
    }

    /**
     *
     * @return
     * The advPoiAdminId
     */
    public Integer getAdvPoiAdminId() {
        return advPoiAdminId;
    }

    /**
     *
     * @param advPoiAdminId
     * The AdvPoiAdminId
     */
    public void setAdvPoiAdminId(Integer advPoiAdminId) {
        this.advPoiAdminId = advPoiAdminId;
    }

    /**
     *
     * @return
     * The advPoiIsActive
     */
    public Boolean getAdvPoiIsActive() {
        return advPoiIsActive;
    }

    /**
     *
     * @param advPoiIsActive
     * The AdvPoiIsActive
     */
    public void setAdvPoiIsActive(Boolean advPoiIsActive) {
        this.advPoiIsActive = advPoiIsActive;
    }

    /**
     *
     * @return
     * The pOIIsEntryFee
     */
    public Boolean getPOIIsEntryFee() {
        if(pOIIsEntryFee==null)
        {
            return false;
        }else {
            return pOIIsEntryFee;
        }
    }

    /**
     *
     * @param pOIIsEntryFee
     * The POIIsEntryFee
     */
    public void setPOIIsEntryFee(Boolean pOIIsEntryFee) {
        this.pOIIsEntryFee = pOIIsEntryFee;
    }

    /**
     *
     * @return
     * The pOIHolidayTypes
     */
    public String getPOIHolidayTypes() {
        if(pOIHolidayTypes==null)
        {
            return  pOIHolidayTypes="";
        }
        else
        {
            return pOIHolidayTypes;
        }
    }

    /**
     *
     * @param pOIHolidayTypes
     * The POIHolidayTypes
     */
    public void setPOIHolidayTypes(String pOIHolidayTypes) {
        this.pOIHolidayTypes = pOIHolidayTypes;
    }

    /**
     *
     * @return
     * The pOIStateHoliDays
     */
    public String getPOIStateHoliDays() {
        if(pOIStateHoliDays==null)
        {
            return  pOIStateHoliDays="";
        }
        else
        {
            return pOIStateHoliDays;
        }

    }

    /**
     *
     * @param pOIStateHoliDays
     * The POIStateHoliDays
     */
    public void setPOIStateHoliDays(String pOIStateHoliDays) {
        this.pOIStateHoliDays = pOIStateHoliDays;
    }

    /**
     *
     * @return
     * The pOINationalHoliDays
     */
    public String getPOINationalHoliDays() {
        if(pOINationalHoliDays==null)
        {
            return  pOINationalHoliDays="";
        }
        else
        {
            return pOINationalHoliDays;
        }
    }

    /**
     *
     * @param pOINationalHoliDays
     * The POINationalHoliDays
     */
    public void setPOINationalHoliDays(String pOINationalHoliDays) {
        this.pOINationalHoliDays = pOINationalHoliDays;
    }

    /**
     *
     * @return
     * The pOIEntryFeeCategoryName
     */
    public String getPOIEntryFeeCategoryName() {
        if(pOIEntryFeeCategoryName==null)
        {
            return "-";
        }else {
            return pOIEntryFeeCategoryName;
        }
    }

    /**
     *
     * @param pOIEntryFeeCategoryName
     * The POIEntryFeeCategoryName
     */
    public void setPOIEntryFeeCategoryName(String pOIEntryFeeCategoryName) {
        this.pOIEntryFeeCategoryName = pOIEntryFeeCategoryName;
    }

    /**
     *
     * @return
     * The pOILocalHolidayMaster
     */
    public ArrayList<POILocalHolidayMaster> getPOILocalHolidayMaster() {

        return pOILocalHolidayMaster;
    }

    /**
     *
     * @param pOILocalHolidayMaster
     * The POILocalHolidayMaster
     */
    public void setPOILocalHolidayMaster(ArrayList<POILocalHolidayMaster> pOILocalHolidayMaster) {
        this.pOILocalHolidayMaster = pOILocalHolidayMaster;
    }
 /**
     *
     * @return
     * The POITiminingMaster
     */
    public ArrayList<POITiminingMaster> getPOITiminingMaster() {

        return poiTimingPoiTiminingMasters;
    }

    /**
     *
     * @param poiTimingPoiTiminingMasters
     * The POITiminingMaster
     */
    public void setPOITiminingMaster(ArrayList<POITiminingMaster> poiTimingPoiTiminingMasters) {
        this.poiTimingPoiTiminingMasters = poiTimingPoiTiminingMasters;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public Object getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(Object resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The resHttpStatus
     */
    public Object getResHttpStatus() {
        return resHttpStatus;
    }

    /**
     *
     * @param resHttpStatus
     * The resHttpStatus
     */
    public void setResHttpStatus(Object resHttpStatus) {
        this.resHttpStatus = resHttpStatus;
    }

    /**
     *
     * @return
     * The resStatusDescription
     */
    public Object getResStatusDescription() {
        return resStatusDescription;
    }

    /**
     *
     * @param resStatusDescription
     * The resStatusDescription
     */
    public void setResStatusDescription(Object resStatusDescription) {
        this.resStatusDescription = resStatusDescription;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The search
     */
    public Object getSearch() {
        return search;
    }

    /**
     *
     * @param search
     * The Search
     */
    public void setSearch(Object search) {
        this.search = search;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    /**
     *
     * @return
     * The currentLat
     */
    public Object getCurrentLat() {
        return currentLat;
    }

    /**
     *
     * @param currentLat
     * The CurrentLat
     */
    public void setCurrentLat(Object currentLat) {
        this.currentLat = currentLat;
    }

    /**
     *
     * @return
     * The currentLong
     */
    public Object getCurrentLong() {
        return currentLong;
    }

    /**
     *
     * @param currentLong
     * The CurrentLong
     */
    public void setCurrentLong(Object currentLong) {
        this.currentLong = currentLong;
    }




    @Generated("org.jsonschema2pojo")
    public class ToiletTypesAll {

        @SerializedName("ToiletID")
        @Expose
        private Integer toiletID;
        @SerializedName("ToiletName")
        @Expose
        private String toiletName;

        /**
         *
         * @return
         * The toiletID
         */
        public Integer getToiletID() {
            return toiletID;
        }

        /**
         *
         * @param toiletID
         * The ToiletID
         */
        public void setToiletID(Integer toiletID) {
            this.toiletID = toiletID;
        }

        /**
         *
         * @return
         * The toiletName
         */
        public String getToiletName() {
            return toiletName;
        }

        /**
         *
         * @param toiletName
         * The ToiletName
         */
        public void setToiletName(String toiletName) {
            this.toiletName = toiletName;
        }

    }

    @Generated("org.jsonschema2pojo")
    public class ToiletServiceCategoryDetails {

        @SerializedName("SerCatId")
        @Expose
        private Integer serCatId;
        @SerializedName("SerCatImage")
        @Expose
        private String serCatImage;
        @SerializedName("SerCatIcon")
        @Expose
        private String serCatIcon;
        @SerializedName("SerCatIsActive")
        @Expose
        private Boolean serCatIsActive;
        @SerializedName("SerCatHostDetails")
        @Expose
        private String serCatHostDetails;
        @SerializedName("SerCatAdminId")
        @Expose
        private Integer serCatAdminId;
        @SerializedName("SerCatName")
        @Expose
        private String serCatName;
        @SerializedName("SerCatFooterImage")
        @Expose
        private String serCatFooterImage;
        @SerializedName("SerCatFooterImageSelected")
        @Expose
        private String serCatFooterImageSelected;
        @SerializedName("SerSeqId")
        @Expose
        private Integer serSeqId;
        @SerializedName("SerIsRo")
        @Expose
        private Boolean serIsRo;
        @SerializedName("SerIsPoi")
        @Expose
        private Object serIsPoi;
        @SerializedName("DefaultSpendTime")
        @Expose
        private String defaultSpendTime;
        @SerializedName("SerIsMapOn")
        @Expose
        private Boolean serIsMapOn;
        @SerializedName("SerDefaultImage")
        @Expose
        private String serDefaultImage;
        @SerializedName("SerIsSerVisible")
        @Expose
        private Boolean serIsSerVisible;
        @SerializedName("resStatus")
        @Expose
        private Boolean resStatus;
        @SerializedName("resDescription")
        @Expose
        private Object resDescription;
        @SerializedName("resCallerDetails")
        @Expose
        private String resCallerDetails;
        @SerializedName("resHttpStatus")
        @Expose
        private Object resHttpStatus;
        @SerializedName("resStatusDescription")
        @Expose
        private Object resStatusDescription;
        @SerializedName("PageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("PageSize")
        @Expose
        private Integer pageSize;
        @SerializedName("Search")
        @Expose
        private Object search;
        @SerializedName("radius")
        @Expose
        private Integer radius;
        @SerializedName("CurrentLat")
        @Expose
        private Object currentLat;
        @SerializedName("CurrentLong")
        @Expose
        private Object currentLong;

        /**
         *
         * @return
         * The serCatId
         */
        public Integer getSerCatId() {
            return serCatId;
        }

        /**
         *
         * @param serCatId
         * The SerCatId
         */
        public void setSerCatId(Integer serCatId) {
            this.serCatId = serCatId;
        }

        /**
         *
         * @return
         * The serCatImage
         */
        public String getSerCatImage() {
            return serCatImage;
        }

        /**
         *
         * @param serCatImage
         * The SerCatImage
         */
        public void setSerCatImage(String serCatImage) {
            this.serCatImage = serCatImage;
        }

        /**
         *
         * @return
         * The serCatIcon
         */
        public String getSerCatIcon() {
            return serCatIcon;
        }

        /**
         *
         * @param serCatIcon
         * The SerCatIcon
         */
        public void setSerCatIcon(String serCatIcon) {
            this.serCatIcon = serCatIcon;
        }

        /**
         *
         * @return
         * The serCatIsActive
         */
        public Boolean getSerCatIsActive() {
            return serCatIsActive;
        }

        /**
         *
         * @param serCatIsActive
         * The SerCatIsActive
         */
        public void setSerCatIsActive(Boolean serCatIsActive) {
            this.serCatIsActive = serCatIsActive;
        }

        /**
         *
         * @return
         * The serCatHostDetails
         */
        public String getSerCatHostDetails() {
            return serCatHostDetails;
        }

        /**
         *
         * @param serCatHostDetails
         * The SerCatHostDetails
         */
        public void setSerCatHostDetails(String serCatHostDetails) {
            this.serCatHostDetails = serCatHostDetails;
        }

        /**
         *
         * @return
         * The serCatAdminId
         */
        public Integer getSerCatAdminId() {
            return serCatAdminId;
        }

        /**
         *
         * @param serCatAdminId
         * The SerCatAdminId
         */
        public void setSerCatAdminId(Integer serCatAdminId) {
            this.serCatAdminId = serCatAdminId;
        }

        /**
         *
         * @return
         * The serCatName
         */
        public String getSerCatName() {
            return serCatName;
        }

        /**
         *
         * @param serCatName
         * The SerCatName
         */
        public void setSerCatName(String serCatName) {
            this.serCatName = serCatName;
        }

        /**
         *
         * @return
         * The serCatFooterImage
         */
        public String getSerCatFooterImage() {
            return serCatFooterImage;
        }

        /**
         *
         * @param serCatFooterImage
         * The SerCatFooterImage
         */
        public void setSerCatFooterImage(String serCatFooterImage) {
            this.serCatFooterImage = serCatFooterImage;
        }

        /**
         *
         * @return
         * The serCatFooterImageSelected
         */
        public String getSerCatFooterImageSelected() {
            return serCatFooterImageSelected;
        }

        /**
         *
         * @param serCatFooterImageSelected
         * The SerCatFooterImageSelected
         */
        public void setSerCatFooterImageSelected(String serCatFooterImageSelected) {
            this.serCatFooterImageSelected = serCatFooterImageSelected;
        }

        /**
         *
         * @return
         * The serSeqId
         */
        public Integer getSerSeqId() {
            return serSeqId;
        }

        /**
         *
         * @param serSeqId
         * The SerSeqId
         */
        public void setSerSeqId(Integer serSeqId) {
            this.serSeqId = serSeqId;
        }

        /**
         *
         * @return
         * The serIsRo
         */
        public Boolean getSerIsRo() {
            return serIsRo;
        }

        /**
         *
         * @param serIsRo
         * The SerIsRo
         */
        public void setSerIsRo(Boolean serIsRo) {
            this.serIsRo = serIsRo;
        }

        /**
         *
         * @return
         * The serIsPoi
         */
        public Object getSerIsPoi() {
            return serIsPoi;
        }

        /**
         *
         * @param serIsPoi
         * The SerIsPoi
         */
        public void setSerIsPoi(Object serIsPoi) {
            this.serIsPoi = serIsPoi;
        }

        /**
         *
         * @return
         * The defaultSpendTime
         */
        public String getDefaultSpendTime() {
            return defaultSpendTime;
        }

        /**
         *
         * @param defaultSpendTime
         * The DefaultSpendTime
         */
        public void setDefaultSpendTime(String defaultSpendTime) {
            this.defaultSpendTime = defaultSpendTime;
        }

        /**
         *
         * @return
         * The serIsMapOn
         */
        public Boolean getSerIsMapOn() {
            return serIsMapOn;
        }

        /**
         *
         * @param serIsMapOn
         * The SerIsMapOn
         */
        public void setSerIsMapOn(Boolean serIsMapOn) {
            this.serIsMapOn = serIsMapOn;
        }

        /**
         *
         * @return
         * The serDefaultImage
         */
        public String getSerDefaultImage() {
            return serDefaultImage;
        }

        /**
         *
         * @param serDefaultImage
         * The SerDefaultImage
         */
        public void setSerDefaultImage(String serDefaultImage) {
            this.serDefaultImage = serDefaultImage;
        }

        /**
         *
         * @return
         * The serIsSerVisible
         */
        public Boolean getSerIsSerVisible() {
            return serIsSerVisible;
        }

        /**
         *
         * @param serIsSerVisible
         * The SerIsSerVisible
         */
        public void setSerIsSerVisible(Boolean serIsSerVisible) {
            this.serIsSerVisible = serIsSerVisible;
        }

        /**
         *
         * @return
         * The resStatus
         */
        public Boolean getResStatus() {
            return resStatus;
        }

        /**
         *
         * @param resStatus
         * The resStatus
         */
        public void setResStatus(Boolean resStatus) {
            this.resStatus = resStatus;
        }

        /**
         *
         * @return
         * The resDescription
         */
        public Object getResDescription() {
            return resDescription;
        }

        /**
         *
         * @param resDescription
         * The resDescription
         */
        public void setResDescription(Object resDescription) {
            this.resDescription = resDescription;
        }

        /**
         *
         * @return
         * The resCallerDetails
         */
        public String getResCallerDetails() {
            return resCallerDetails;
        }

        /**
         *
         * @param resCallerDetails
         * The resCallerDetails
         */
        public void setResCallerDetails(String resCallerDetails) {
            this.resCallerDetails = resCallerDetails;
        }

        /**
         *
         * @return
         * The resHttpStatus
         */
        public Object getResHttpStatus() {
            return resHttpStatus;
        }

        /**
         *
         * @param resHttpStatus
         * The resHttpStatus
         */
        public void setResHttpStatus(Object resHttpStatus) {
            this.resHttpStatus = resHttpStatus;
        }

        /**
         *
         * @return
         * The resStatusDescription
         */
        public Object getResStatusDescription() {
            return resStatusDescription;
        }

        /**
         *
         * @param resStatusDescription
         * The resStatusDescription
         */
        public void setResStatusDescription(Object resStatusDescription) {
            this.resStatusDescription = resStatusDescription;
        }

        /**
         *
         * @return
         * The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         *
         * @param pageNo
         * The PageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         *
         * @return
         * The pageSize
         */
        public Integer getPageSize() {
            return pageSize;
        }

        /**
         *
         * @param pageSize
         * The PageSize
         */
        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         *
         * @return
         * The search
         */
        public Object getSearch() {
            return search;
        }

        /**
         *
         * @param search
         * The Search
         */
        public void setSearch(Object search) {
            this.search = search;
        }

        /**
         *
         * @return
         * The radius
         */
        public Integer getRadius() {
            return radius;
        }

        /**
         *
         * @param radius
         * The radius
         */
        public void setRadius(Integer radius) {
            this.radius = radius;
        }

        /**
         *
         * @return
         * The currentLat
         */
        public Object getCurrentLat() {
            return currentLat;
        }

        /**
         *
         * @param currentLat
         * The CurrentLat
         */
        public void setCurrentLat(Object currentLat) {
            this.currentLat = currentLat;
        }

        /**
         *
         * @return
         * The currentLong
         */
        public Object getCurrentLong() {
            return currentLong;
        }

        /**
         *
         * @param currentLong
         * The CurrentLong
         */
        public void setCurrentLong(Object currentLong) {
            this.currentLong = currentLong;
        }

    }

}

