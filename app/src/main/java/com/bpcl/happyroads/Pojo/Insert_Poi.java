

        package com.bpcl.happyroads.Pojo;

        import javax.annotation.Generated;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Insert_Poi {

    @SerializedName("TripWayPointId")
    @Expose
    private Integer tripWayPointId;
    @SerializedName("TripWayType")
    @Expose
    private String tripWayType;
    @SerializedName("TripWayRefId")
    @Expose
    private Integer tripWayRefId;
    @SerializedName("TripWayTripid")
    @Expose
    private Integer tripWayTripid;
    @SerializedName("TripWayUmId")
    @Expose
    private Integer tripWayUmId;
    @SerializedName("TripWayComments")
    @Expose
    private String tripWayComments;
    @SerializedName("TripExtraKeys")
    @Expose
    private String tripExtraKeys;
    @SerializedName("TripDesId")
    @Expose
    private Integer tripDesId;
    @SerializedName("TripIsPause")
    @Expose
    private Boolean tripIsPause;
    @SerializedName("TripDistanceInKm")
    @Expose
    private String tripDistanceInKm;
    @SerializedName("TripDistanceInMinute")
    @Expose
    private String tripDistanceInMinute;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resDescription")
    @Expose
    private String resDescription;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;

    /**
     *
     * @return
     * The tripWayPointId
     */
    public Integer getTripWayPointId() {
        return tripWayPointId;
    }

    /**
     *
     * @param tripWayPointId
     * The TripWayPointId
     */
    public void setTripWayPointId(Integer tripWayPointId) {
        this.tripWayPointId = tripWayPointId;
    }

    /**
     *
     * @return
     * The tripWayType
     */
    public String getTripWayType() {
        return tripWayType;
    }

    /**
     *
     * @param tripWayType
     * The TripWayType
     */
    public void setTripWayType(String tripWayType) {
        this.tripWayType = tripWayType;
    }

    /**
     *
     * @return
     * The tripWayRefId
     */
    public Integer getTripWayRefId() {
        return tripWayRefId;
    }

    /**
     *
     * @param tripWayRefId
     * The TripWayRefId
     */
    public void setTripWayRefId(Integer tripWayRefId) {
        this.tripWayRefId = tripWayRefId;
    }

    /**
     *
     * @return
     * The tripWayTripid
     */
    public Integer getTripWayTripid() {
        return tripWayTripid;
    }

    /**
     *
     * @param tripWayTripid
     * The TripWayTripid
     */
    public void setTripWayTripid(Integer tripWayTripid) {
        this.tripWayTripid = tripWayTripid;
    }

    /**
     *
     * @return
     * The tripWayUmId
     */
    public Integer getTripWayUmId() {
        return tripWayUmId;
    }

    /**
     *
     * @param tripWayUmId
     * The TripWayUmId
     */
    public void setTripWayUmId(Integer tripWayUmId) {
        this.tripWayUmId = tripWayUmId;
    }

    /**
     *
     * @return
     * The tripWayComments
     */
    public String getTripWayComments() {
        return tripWayComments;
    }

    /**
     *
     * @param tripWayComments
     * The TripWayComments
     */
    public void setTripWayComments(String tripWayComments) {
        this.tripWayComments = tripWayComments;
    }

    /**
     *
     * @return
     * The tripExtraKeys
     */
    public String getTripExtraKeys() {
        return tripExtraKeys;
    }

    /**
     *
     * @param tripExtraKeys
     * The TripExtraKeys
     */
    public void setTripExtraKeys(String tripExtraKeys) {
        this.tripExtraKeys = tripExtraKeys;
    }

    /**
     *
     * @return
     * The tripDesId
     */
    public Integer getTripDesId() {
        return tripDesId;
    }

    /**
     *
     * @param tripDesId
     * The TripDesId
     */
    public void setTripDesId(Integer tripDesId) {
        this.tripDesId = tripDesId;
    }

    /**
     *
     * @return
     * The tripIsPause
     */
    public Boolean getTripIsPause() {
        return tripIsPause;
    }

    /**
     *
     * @param tripIsPause
     * The TripIsPause
     */
    public void setTripIsPause(Boolean tripIsPause) {
        this.tripIsPause = tripIsPause;
    }

    /**
     *
     * @return
     * The tripDistanceInKm
     */
    public String getTripDistanceInKm() {
        return tripDistanceInKm;
    }

    /**
     *
     * @param tripDistanceInKm
     * The TripDistanceInKm
     */
    public void setTripDistanceInKm(String tripDistanceInKm) {
        this.tripDistanceInKm = tripDistanceInKm;
    }

    /**
     *
     * @return
     * The tripDistanceInMinute
     */
    public String getTripDistanceInMinute() {
        return tripDistanceInMinute;
    }

    /**
     *
     * @param tripDistanceInMinute
     * The TripDistanceInMinute
     */
    public void setTripDistanceInMinute(String tripDistanceInMinute) {
        this.tripDistanceInMinute = tripDistanceInMinute;
    }

    /**
     *
     * @return
     * The resStatus
     */
    public Boolean getResStatus() {
        return resStatus;
    }

    /**
     *
     * @param resStatus
     * The resStatus
     */
    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    /**
     *
     * @return
     * The resDescription
     */
    public String getResDescription() {
        return resDescription;
    }

    /**
     *
     * @param resDescription
     * The resDescription
     */
    public void setResDescription(String resDescription) {
        this.resDescription = resDescription;
    }

    /**
     *
     * @return
     * The resCallerDetails
     */
    public String getResCallerDetails() {
        return resCallerDetails;
    }

    /**
     *
     * @param resCallerDetails
     * The resCallerDetails
     */
    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    /**
     *
     * @return
     * The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     *
     * @param pageNo
     * The PageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     *
     * @return
     * The pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     *
     * @param pageSize
     * The PageSize
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     *
     * @return
     * The radius
     */
    public Integer getRadius() {
        return radius;
    }

    /**
     *
     * @param radius
     * The radius
     */
    public void setRadius(Integer radius) {
        this.radius = radius;
    }

}