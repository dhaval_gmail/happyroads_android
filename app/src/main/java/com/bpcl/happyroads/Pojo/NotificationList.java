package com.bpcl.happyroads.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ADMIN on 04/02/2017.
 */



public class NotificationList {

    @SerializedName("RowNum")
    @Expose
    private Integer rowNum;
    @SerializedName("Msgtext")
    @Expose
    private String msgtext;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ntfUsrId")
    @Expose
    private Integer ntfUsrId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Rcvr_ProfilePhoto")
    @Expose
    private String rcvrProfilePhoto;
    @SerializedName("Rcvr_umFirstName")
    @Expose
    private String rcvrUmFirstName;
    @SerializedName("Rcvr_umLastName")
    @Expose
    private String rcvrUmLastName;
    @SerializedName("Rcvr_umid")
    @Expose
    private Integer rcvrUmid;
    @SerializedName("ntfType")
    @Expose
    private String ntfType;
    @SerializedName("ntfRefKey")
    @Expose
    private Integer ntfRefKey;
    @SerializedName("ntfContent")
    @Expose
    private String ntfContent;
    @SerializedName("ntfMsgText")
    @Expose
    private String ntfMsgText;
    @SerializedName("mintuesago")
    @Expose
    private Integer mintuesago;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("ntfId")
    @Expose
    private Integer ntfId;
    @SerializedName("resStatus")
    @Expose
    private Boolean resStatus;
    @SerializedName("resCallerDetails")
    @Expose
    private String resCallerDetails;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("IsActiveInt")
    @Expose
    private Integer isActiveInt;

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum(Integer rowNum) {
        this.rowNum = rowNum;
    }

    public String getMsgtext() {
        return msgtext;
    }

    public void setMsgtext(String msgtext) {
        this.msgtext = msgtext;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNtfUsrId() {
        return ntfUsrId;
    }

    public void setNtfUsrId(Integer ntfUsrId) {
        this.ntfUsrId = ntfUsrId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRcvrProfilePhoto() {
        return rcvrProfilePhoto;
    }

    public void setRcvrProfilePhoto(String rcvrProfilePhoto) {
        this.rcvrProfilePhoto = rcvrProfilePhoto;
    }

    public String getRcvrUmFirstName() {
        return rcvrUmFirstName;
    }

    public void setRcvrUmFirstName(String rcvrUmFirstName) {
        this.rcvrUmFirstName = rcvrUmFirstName;
    }

    public String getRcvrUmLastName() {
        return rcvrUmLastName;
    }

    public void setRcvrUmLastName(String rcvrUmLastName) {
        this.rcvrUmLastName = rcvrUmLastName;
    }

    public Integer getRcvrUmid() {
        return rcvrUmid;
    }

    public void setRcvrUmid(Integer rcvrUmid) {
        this.rcvrUmid = rcvrUmid;
    }

    public String getNtfType() {
        return ntfType;
    }

    public void setNtfType(String ntfType) {
        this.ntfType = ntfType;
    }

    public Integer getNtfRefKey() {
        return ntfRefKey;
    }

    public void setNtfRefKey(Integer ntfRefKey) {
        this.ntfRefKey = ntfRefKey;
    }

    public String getNtfContent() {
        return ntfContent;
    }

    public void setNtfContent(String ntfContent) {
        this.ntfContent = ntfContent;
    }

    public String getNtfMsgText() {
        return ntfMsgText;
    }

    public void setNtfMsgText(String ntfMsgText) {
        this.ntfMsgText = ntfMsgText;
    }

    public Integer getMintuesago() {
        return mintuesago;
    }

    public void setMintuesago(Integer mintuesago) {
        this.mintuesago = mintuesago;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getNtfId() {
        return ntfId;
    }

    public void setNtfId(Integer ntfId) {
        this.ntfId = ntfId;
    }

    public Boolean getResStatus() {
        return resStatus;
    }

    public void setResStatus(Boolean resStatus) {
        this.resStatus = resStatus;
    }

    public String getResCallerDetails() {
        return resCallerDetails;
    }

    public void setResCallerDetails(String resCallerDetails) {
        this.resCallerDetails = resCallerDetails;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Integer getIsActiveInt() {
        return isActiveInt;
    }

    public void setIsActiveInt(Integer isActiveInt) {
        this.isActiveInt = isActiveInt;
    }

}
