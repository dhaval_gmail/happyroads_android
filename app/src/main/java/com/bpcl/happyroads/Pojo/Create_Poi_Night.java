package com.bpcl.happyroads.Pojo;

/**
 * Created by ADMIN on 9/30/2016.
 */
public class Create_Poi_Night {
    Integer tripId,PoiId,WayPointId,poiDesID,poiServerId;
    String poiName,resumeDate,resumeTime,poiDate,poitype,poidescription;
    Double stayTime,poiLatitude,poiLongitude,arrivalTime,departureTime;
    byte[] poiImage;
    Boolean poiStay;

    Double KM,time,reqtime;

    Boolean returnstatus;
    String imagename;

    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }

    public String getPoidescription() {
        return poidescription;
    }

    public void setPoidescription(String poidescription) {
        this.poidescription = poidescription;
    }


    public Boolean getReturnstatus() {
        return returnstatus;
    }

    public void setReturnstatus(Boolean returnstatus) {
        this.returnstatus = returnstatus;
    }


    public Integer getPoiServerId() {
        return poiServerId;
    }

    public void setPoiServerId(Integer poiServerId) {
        this.poiServerId = poiServerId;
    }

    public Integer getPoiDesID() {
        return poiDesID;
    }

    public void setPoiDesID(Integer poiDesID) {
        this.poiDesID = poiDesID;
    }

    public Double getReqtime() {
        return reqtime;
    }

    public void setReqtime(Double reqtime) {
        this.reqtime = reqtime;
    }

    public Double getKM() {
        return KM;
    }

    public void setKM(Double KM) {
        this.KM = KM;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public Integer getPoiId() {
        return PoiId;
    }

    public void setPoiId(Integer poiId) {
        PoiId = poiId;
    }

    public String getPoiName() {
        return poiName;
    }

    public void setPoiName(String poiName) {
        this.poiName = poiName;
    }

    public String getResumeDate() {
        return resumeDate;
    }

    public void setResumeDate(String resumeDate) {
        this.resumeDate = resumeDate;
    }

    public String getResumeTime() {
        return resumeTime;
    }

    public void setResumeTime(String resumeTime) {
        this.resumeTime = resumeTime;
    }

    public Double getStayTime() {
        return stayTime;
    }

    public void setStayTime(Double stayTime) {
        this.stayTime = stayTime;
    }

    public Double getPoiLatitude() {
        return poiLatitude;
    }

    public void setPoiLatitude(Double poiLatitude) {
        this.poiLatitude = poiLatitude;
    }

    public Double getPoiLongitude() {
        return poiLongitude;
    }

    public void setPoiLongitude(Double poiLongitude) {
        this.poiLongitude = poiLongitude;
    }

    public byte[] getPoiImage() {
        return poiImage;
    }

    public void setPoiImage(byte[] poiImage) {
        this.poiImage = poiImage;
    }

    public Boolean getPoiStay() {
        return poiStay;
    }

    public void setPoiStay(Boolean poiStay) {
        this.poiStay = poiStay;
    }

    public String getPoiDate() {
        return poiDate;
    }

    public void setPoiDate(String poiDate) {
        this.poiDate = poiDate;
    }

    public Double getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Double arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Double getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Double departureTime) {
        this.departureTime = departureTime;
    }

    public Integer getWayPointId() {
        return WayPointId;
    }

    public void setWayPointId(Integer wayPointId) {
        WayPointId = wayPointId;
    }

    public String getPoitype() {
        return poitype;
    }

    public void setPoitype(String poitype) {
        this.poitype = poitype;
    }
}
