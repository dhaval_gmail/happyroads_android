package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Pojo.CalculateDistance;
import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.Insert_Poi;
import com.bpcl.happyroads.Pojo.RemoveTripPoi;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.Utils.CircleImageView;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.ImageSaver;
import com.bpcl.happyroads.Utils.MyTextView;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/30/2016.
 */
public class MyPlanFragment extends Fragment implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {
    LinearLayout ll_day_wise_plan, ll_add_places_to_plan;
    int i;
    ImageView iv_dotted_line_bottom, iv_location_my_plan, iv_edit_my_plan,iv_add_poi;
    TextView tv_km_time_plan, tv_day, tv_save_my_plan,tv_save_my_plan1;
    LinearLayout ll_plan_my_route;
    TextView tv_add_poi,tv_edit_my_plan,tv_location_my_plan;

    boolean editIconUnselected = false;
    boolean tripFlag = false;
    String date;
    TextView tv_start_date_my_trip, tv_return_date_my_trip;
    Calendar now;
    DatePickerDialog dpd;
    TimePickerDialog tpd,tpd1;
    String selected = "";
    String date1 = null;
    TextView tv_source, tv_destination, tv_trip_start_date, tv_trip_end_date, tv_no_days;

    List<Create_Trip> createTripList = new ArrayList<>();
    List<DestinationPlan> destinationplanList = new ArrayList<>();
    DBHandler dbHandler;

    Integer tripid = 0;
    Dialog pb_dialog;
    Double dayhours = 8.0;
    Double starttime=7.0;
    ArrayList<Integer> noday;
    ArrayList<DestinationPlan> tempdestiDestinationPlanlist;
    CircleImageView circleivsource, circleivdes;
    TextView tv_arrival_time;
    String time_start_trip;
    SessionManager sessionManager;
    TextView tv_start_date_my_return_trip, tv_start_time_my_return_trip;
    String excepted_date="", excepted_time = "",return_excepted_date="",return_excepted_time="";
    String oldvalue="";

    ListView list_minutes,list_hrs;
    TextView tv_cancel,tv_ok;
    String[]  hrsvalues;
    String[] minsvalues;
    MinsAdapter minsArrayAdapter;
    Hrsadapter hrsaArrayAdapter;
    String hrs,min;
    Dialog  myTripDialog;
    EditText et_trip_name;
    AutoCompleteTextView av_tripname;
    String select="";
    TextView tv_save_my_trip,tv_text_trip_name;
    String start_date="",start_time="",end_time="",end_date="";
    String select_dpd_type="";
    private Boolean return_trip_plan=false;

    private int main_pos;
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM ,yyyy");
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    private TextView tv_plan_my_return_trip;
    List<Create_Poi> count_create_poi_list_S_D,count_create_poi_list_D_S;
    String temptime="19:00";
    int a,b;
    String frommytrips = "";


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_plan_fragment, container, false);


        pb_dialog = new Dialog(getActivity());
        tv_source = (TextView) v.findViewById(R.id.tv_source);
        tv_destination = (TextView) v.findViewById(R.id.tv_destination);
        tv_trip_start_date = (TextView) v.findViewById(R.id.tv_trip_start_date);
        tv_trip_end_date = (TextView) v.findViewById(R.id.tv_trip_end_date);
        tv_no_days = (TextView) v.findViewById(R.id.tv_no_days);
        circleivsource = (CircleImageView) v.findViewById(R.id.fc_poi_image_source);
        circleivdes = (CircleImageView) v.findViewById(R.id.fc_poi_image_des);
        iv_edit_my_plan = (ImageView) v.findViewById(R.id.iv_edit_my_plan);
        tv_add_poi = (TextView) v.findViewById(R.id.tv_add_poi);
        tv_location_my_plan = (TextView) v.findViewById(R.id.tv_location_my_plan);
        tv_edit_my_plan = (TextView) v.findViewById(R.id.tv_edit_my_plan);

        ll_day_wise_plan = (LinearLayout) v.findViewById(R.id.ll_day_wise_plan);
        ll_plan_my_route = (LinearLayout) v.findViewById(R.id.ll_plan_my_route);
        tv_save_my_plan = (TextView) v.findViewById(R.id.tv_save_my_plan);
        tv_save_my_plan1 = (TextView) v.findViewById(R.id.tv_save_my_plan1);

        sessionManager=new SessionManager(getActivity());



        for(int i=0;i<Constants.listDefaultValuesArrayList.size();i++)
        {
            if(Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
            if(Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

        }




        if (getArguments() != null)
        {
            tripid = getArguments().getInt("tripId", 0);
            frommytrips = getArguments().getString("from", "");

            dbHandler = new DBHandler(getActivity());
            createTripList = dbHandler.get_TRIP_By_TripId(tripid);
            destinationplanList = dbHandler.get_DestinationList(tripid);
            count_create_poi_list_S_D = dbHandler.get_POI_TRIP(tripid, 0);
            count_create_poi_list_D_S = dbHandler.get_POI_TRIP(tripid, 1);



            if (createTripList.size() > 0) {

                String trip_start_time = "", trip_end_time = "";
                Log.d("System out","Start date____ "+createTripList.get(0).getStartdate());
                /*if(createTripList.get(0).getStartdate().equalsIgnoreCase("")) {
                    tv_save_my_plan.setVisibility(View.VISIBLE);
                    tv_save_my_plan.setText("Plan My Route");
                }
                else if(createTripList.get(0).getStartdate().length()>1 && createTripList.get(0).getReturn_start_date().equalsIgnoreCase(""))
                {
                    tv_save_my_plan.setVisibility(View.VISIBLE);

                    tv_save_my_plan.setText("Plan " + createTripList.get(0).getDestination() + " - " + createTripList.get(0).getSource() + " Route");

                }
                else
                {
                    tv_save_my_plan.setVisibility(View.GONE);
                }*/
               /* if (createTripList.get(0).getStartdate().length()>0) {

                    count_create_poi_list_D_S = dbHandler.get_POI_TRIP(tripid, 1);
                    if (createTripList.get(0).getReturn_start_date().length()>0) {

                        ll_plan_my_route.setVisibility(View.GONE);
                        tv_save_my_plan.setVisibility(View.GONE);
                        tv_save_my_plan1.setVisibility(View.GONE);

                    } else {

                            tv_save_my_plan.setVisibility(View.VISIBLE);
                            tv_save_my_plan1.setVisibility(View.VISIBLE);
                            tv_save_my_plan.setText("Plan My Return Route ");
                            // tv_save_my_plan1.setText("(" + createTripList.get(0).getDestination() + " to " + createTripList.get(0).getSource() + ")");
                            tv_save_my_plan1.setText("(" + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + " to " + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + ")");

                    }

                } else {*/
                if(((ExploreActivity)getActivity()).whichActivity==true)
                {
                    ll_plan_my_route.setVisibility(View.GONE);
                }
                else
                {
                    ll_plan_my_route.setVisibility(View.VISIBLE);

                    tv_save_my_plan.setVisibility(View.VISIBLE);
                    tv_save_my_plan1.setVisibility(View.VISIBLE);
                    tv_save_my_plan.setText("Plan My Route");
                    //   tv_save_my_plan1.setText("("+createTripList.get(0).getSource()+" to "+createTripList.get(0).getDestination()+")");
                    tv_save_my_plan1.setText("(" + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + " to " + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + ")");
                }

                //   }
                try {
                    trip_start_time = Constants.formatDate(createTripList.get(0).getStarttime(), "HH:mm", "hh:mm a");
                    trip_end_time = Constants.formatDate(createTripList.get(0).getEndtime(), "HH:mm", "hh:mm a");
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String tripdate = createTripList.get(0).getStartdate() + " " + trip_start_time;
                String tripenddate = createTripList.get(0).getEnddate() + " " + trip_end_time;
                excepted_date=createTripList.get(0).getReturn_start_date();
                excepted_time=createTripList.get(0).getReturn_start_time();

                tv_source.setText(createTripList.get(0).getSource());
                tv_destination.setText(createTripList.get(0).getDestination());
                tv_trip_start_date.setText(tripdate);
                tv_trip_end_date.setText(tripenddate);
                circleivsource.setImageBitmap(Constants.getImage(createTripList.get(0).getSource_image()));
                circleivdes.setImageBitmap(Constants.getImage(createTripList.get(0).getDestination_image()));
            }
        }
        else
        {

        }

        ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* ((ExploreActivity)getActivity()).replace_fragmnet(new GallaryFragment());
                ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);*/
               /* Intent intent=new Intent(getContext(),SubListingFragment.class);
                startActivity(intent);*/
                ((ExploreActivity) getActivity()).onBackPressed();
            }
        });
        now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        tpd = TimePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );
        tpd1 = TimePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE),true
        );


        tv_edit_my_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editIconUnselected == false) {
                    iv_edit_my_plan.setImageResource(R.drawable.edit_icon_selected_my_plan);
                    tv_edit_my_plan.setTextColor(getResources().getColor(R.color.textYellow));
                    editIconUnselected = true;
                    ll_plan_my_route.setVisibility(View.VISIBLE);
                    tv_save_my_plan.setText("Update");
                    tv_save_my_plan.setVisibility(View.VISIBLE);
                    tv_save_my_plan1.setVisibility(View.GONE);
                } else {
                    iv_edit_my_plan.setImageResource(R.drawable.edit_icon_unselected_my_plan);
                    tv_edit_my_plan.setTextColor(getResources().getColor(R.color.colorWhite));

                    editIconUnselected = false;
                   /* if(count_create_poi_list_S_D.size()<=0) {


                            tv_save_my_plan.setVisibility(View.VISIBLE);
                            tv_save_my_plan1.setVisibility(View.VISIBLE);
                            // tv_save_my_plan.setText("Plan My Route");
                            tv_save_my_plan.setText("Plan My Route ");
//                        tv_save_my_plan1.setText("("+createTripList.get(0).getSource()+" to "+createTripList.get(0).getDestination()+")");
                            tv_save_my_plan1.setText("(" + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + " to " + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + ")");



                    }
                    else if((count_create_poi_list_D_S.size()<=0))
                    {
                        tv_save_my_plan.setVisibility(View.VISIBLE);
                        tv_save_my_plan1.setVisibility(View.VISIBLE);

                        tv_save_my_plan.setText("Plan My Return Route ");
                       // tv_save_my_plan1.setText("(" + tv_destination.getText().toString() + " to " + tv_source.getText().toString() + " Route"+")");
                        tv_save_my_plan1.setText("("+(tv_destination.getText().toString().contains(" ")?tv_destination.getText().toString().substring(0,tv_destination.getText().toString().indexOf(" ")):tv_destination.getText().toString())+" to "+(tv_source.getText().toString().contains(" ")?tv_source.getText().toString().substring(0,tv_source.getText().toString().indexOf(" ")):tv_source.getText().toString())+")");

                    }*/

                  /*  if (createTripList.get(0).getStartdate().length()>0) {

                        count_create_poi_list_D_S = dbHandler.get_POI_TRIP(tripid, 1);
                        if (createTripList.get(0).getReturn_start_date().length()>0) {

                            ll_plan_my_route.setVisibility(View.GONE);
                            tv_save_my_plan.setVisibility(View.GONE);
                            tv_save_my_plan1.setVisibility(View.GONE);

                        } else {

                            tv_save_my_plan.setVisibility(View.VISIBLE);
                            tv_save_my_plan1.setVisibility(View.VISIBLE);
                            tv_save_my_plan.setText("Plan My Return Route ");
                            // tv_save_my_plan1.setText("(" + createTripList.get(0).getDestination() + " to " + createTripList.get(0).getSource() + ")");
                            tv_save_my_plan1.setText("(" + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + " to " + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + ")");

                        }

                    } else {*/
                    if(((ExploreActivity)getActivity()).whichActivity==true)
                    {
                        ll_plan_my_route.setVisibility(View.GONE);
                    }else {

                        tv_save_my_plan.setVisibility(View.VISIBLE);
                        tv_save_my_plan1.setVisibility(View.VISIBLE);
                        tv_save_my_plan.setText("Plan My Route ");
                        //   tv_save_my_plan1.setText("("+createTripList.get(0).getSource()+" to "+createTripList.get(0).getDestination()+")");
                        tv_save_my_plan1.setText("(" + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + " to " + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + ")");
                    }
                    //  }
                }
                ll_day_wise_plan.removeAllViewsInLayout();
                set_destination_layout();
            }
        });

        tv_add_poi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExploreActivity)getActivity()).iv_home_icon_header.performClick();
                ExplorePlacesFragment explorePlacesFragment = new ExplorePlacesFragment();
                android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                // ((ExploreActivity)getActivity()).setHeader("Change Password");
                changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                changeTransaction.replace(R.id.frame, explorePlacesFragment,"ExplorePlacesFragment");

                changeTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("placeName",tv_destination.getText().toString());
                bundle.putString("position","");
                bundle.putString("destID",createTripList.get(0).getDestinationId()+"");
                bundle.putString("destLat",createTripList.get(0).getDestination_latitude()+"");
                bundle.putString("destLong",createTripList.get(0).getDestination_longitude()+"");
                bundle.putInt("tripId", tripid);

                //   ExploreActivity.destattractionimages.clear();
                bundle.putString("from","explore from myplan");
                explorePlacesFragment.setArguments(bundle);
                changeTransaction.commit();
            }
        });
        tv_location_my_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   ((ExploreActivity)getActivity()).replace_fragmnet(new RoutePlanningFragment());

                Bundle b = new Bundle();
                b.putInt("tripId", tripid);
                b.putString("plantype","DES");
                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);


            }
        });
        ll_plan_my_route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  ((ExploreActivity)getActivity()).replace_fragmnet(new RoutePlanningFragment());
                if (tv_save_my_plan.getText().toString().equalsIgnoreCase("Update")) {
                    Constants.show_error_popup_success(getActivity(), "Trip updated successfully.", tv_save_my_plan);

                    iv_edit_my_plan.setImageResource(R.drawable.edit_icon_unselected_my_plan);
                    tv_edit_my_plan.setTextColor(getResources().getColor(R.color.colorWhite));
                    //    ll_edit_stay.setBackground(null);
                    editIconUnselected = false;
                   /* if((count_create_poi_list_S_D.size()<=0)) {
                        tv_save_my_plan.setVisibility(View.VISIBLE);
                        tv_save_my_plan1.setVisibility(View.VISIBLE);
                       // tv_save_my_plan.setText("Plan My Route");
                        tv_save_my_plan.setText("Plan My Route ");
                        tv_save_my_plan1.setText("("+createTripList.get(0).getSource()+" to "+createTripList.get(0).getDestination()+")");

                    }
                    else if((count_create_poi_list_D_S.size()<=0))
                    {
                        tv_save_my_plan.setVisibility(View.VISIBLE);
                        tv_save_my_plan1.setVisibility(View.VISIBLE);
                        tv_save_my_plan.setText("Plan My Return Route ");
                        tv_save_my_plan1.setText("(" + tv_destination.getText().toString() + " to " + tv_source.getText().toString() + ")");

                    }
                    else
                    {
                        tv_save_my_plan.setVisibility(View.GONE);
                        tv_save_my_plan1.setVisibility(View.GONE);
                    }*/
                  /*  if (createTripList.get(0).getStartdate().length()>0) {

                        count_create_poi_list_D_S = dbHandler.get_POI_TRIP(tripid, 1);
                        if (createTripList.get(0).getReturn_start_date().length()>0) {

                            ll_plan_my_route.setVisibility(View.GONE);
                            tv_save_my_plan.setVisibility(View.GONE);
                            tv_save_my_plan1.setVisibility(View.GONE);

                        } else {

                            tv_save_my_plan.setVisibility(View.VISIBLE);
                            tv_save_my_plan1.setVisibility(View.VISIBLE);
                            tv_save_my_plan.setText("Plan My Return Route ");
                            // tv_save_my_plan1.setText("(" + createTripList.get(0).getDestination() + " to " + createTripList.get(0).getSource() + ")");
                            tv_save_my_plan1.setText("(" + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + " to " + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + ")");

                        }

                    } else {
*/
                    if(((ExploreActivity)getActivity()).whichActivity==true)
                    {
                        ll_plan_my_route.setVisibility(View.GONE);
                    }else {

                        tv_save_my_plan.setVisibility(View.VISIBLE);
                        tv_save_my_plan1.setVisibility(View.VISIBLE);
                        tv_save_my_plan.setText("Plan My Route ");
                        //   tv_save_my_plan1.setText("("+createTripList.get(0).getSource()+" to "+createTripList.get(0).getDestination()+")");
                        tv_save_my_plan1.setText("(" + (createTripList.get(0).getSource().contains(" ") ? createTripList.get(0).getSource().substring(0, createTripList.get(0).getSource().indexOf(" ")) : createTripList.get(0).getSource()) + " to " + (createTripList.get(0).getDestination().contains(" ") ? createTripList.get(0).getDestination().substring(0, createTripList.get(0).getDestination().indexOf(" ")) : createTripList.get(0).getDestination()) + ")");
                    }
                    //  }
                 /*   final Dialog myTripDialog = new Dialog(getActivity());

                    Constants.show_error_popup_success(getActivity(), "Trip updated successfully.", tv_save_my_plan);
                    tv_save_my_plan.setText("Plan My Route");
                    myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    myTripDialog.setContentView(R.layout.pop_up_my_trips);
                    final TextView tv_for_better_dates = (TextView) myTripDialog.findViewById(R.id.tv_for_better_dates);
                    final ImageView iv_swt_off_my_trip = (ImageView) myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
                    final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
                    final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
                    tv_start_date_my_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_trip);
                    tv_return_date_my_trip = (TextView) myTripDialog.findViewById(R.id.tv_return_date_my_trip);
                    iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (tripFlag == false) {
                                iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                                tv_for_better_dates.setVisibility(View.GONE);
                                ll_start_date_my_trip.setAlpha(1);
                                ll_return_date_my_trip.setAlpha(1);
                                tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        selected = "start";
                                        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                                    }


                                });
                                tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        selected = "return";
                                        dpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                                    }
                                });


                                tripFlag = true;
                            } else {
                                iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                                tv_for_better_dates.setVisibility(View.VISIBLE);
                                ll_start_date_my_trip.setAlpha((float) 0.6);
                                ll_return_date_my_trip.setAlpha((float) 0.6);
                                tripFlag = false;
                            }

                        }
                    });
                    TextView tv_save_my_trip = (TextView) myTripDialog.findViewById(R.id.tv_save_my_trip);
                    tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myTripDialog.dismiss();
                            // ((ExploreActivity)getActivity()).replace_fragmnet(new ListingDetailsFragment());        String time = hourOfDay+":"+minute+"";
                            tv_save_my_plan.setText("Plan My Route");


                        }
                    });
                    ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);
                    //  myTripDialog.show();
                    ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myTripDialog.dismiss();
                        }
                    });*/
                } else if (tv_save_my_plan.getText().toString().contains("Plan My Route")) {
                   /* if(createTripList.get(0).getStartdate().equalsIgnoreCase("")) {
                        Mytrip_Dialog(getActivity());
                    }else
                    {*/
                    Bundle b = new Bundle();
                    b.putInt("tripId", tripid);
                    b.putString("plantype","STOD");
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                    // }

                    //  Constants.frommapplan = "planmyroutefrommap";
                    // ((ExploreActivity) getActivity()).replace_fragmnet(new RoutePlanningFragment());

                   /* RoutePlanningFragment routePlanningFragment = new RoutePlanningFragment();
                    android.support.v4.app.FragmentTransaction routePlanningTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // ((ExploreActivity)getActivity()).setHeader("Change Password");

                    routePlanningTransaction.addToBackStack(null);
                    final String fromMyTrip = "START_TRACKING_PLAN_MY_ROUTE";
                    Bundle routeBundle = new Bundle();
                    routeBundle.putString("fromMyTrip", fromMyTrip);

                    routeBundle.putInt("tripId", tripid);

                    routePlanningFragment.setArguments(routeBundle);
                    routePlanningTransaction.replace(R.id.frame, routePlanningFragment);
                    routePlanningTransaction.commit();*/




                }/*else if(tv_save_my_plan.getText().toString().contains("Plan My Return Route"))
                {
                    if(!createTripList.get(0).getStartdate().equalsIgnoreCase("") &&createTripList.get(0).getReturn_start_date().equalsIgnoreCase(""))
                {
                    return_trip_dialog();
                }else
                    {
                        Bundle b = new Bundle();
                        b.putInt("tripId", tripid);
                        b.putString("plantype","DTOS");
                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                    }

                }*/
                set_destination_layout();
            }
        });


        for (int i = 1; i < destinationplanList.size(); i++) {
            getdistancetime(i);
        }


        if (destinationplanList.size() == 1) {
            set_destination_layout();
        }

        noday = new ArrayList<>();
        Log.d("noofdaysize", String.valueOf(destinationplanList.size()));

        return v;
    }


    public void set_destination_layout()
    {
        if (destinationplanList.size() > 0) {
            ll_day_wise_plan.removeAllViewsInLayout();
            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

            {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

            }




            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime());
                Date date1 = format.parse(createTripList.get(0).getEnddate()+" "+temptime);
                Date firstPoiDate=format.parse(createTripList.get(0).getEnddate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.toString());
                Log.d("System out","End time of day "+firstPoiDate.toString());
                if(c.getTimeInMillis()<=date1.getTime())
                {
                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));

                    }
                    else {

                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(0);
                }
                else
                {
                    if (destinationplanList.get(0).getStarttime() == 24.00) {
                        destinationplanList.get(0).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                destinationplanList.get(0).setDay(1);
                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

            }

            Log.d("System out","Destination poi list size "+destinationplanList.size());
           /* if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(0);*/


            Double tempDepart=destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++)
            {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out","required time  "+reqtime);
                Log.d("System out","depature time  "+destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours)
                {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(j).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart=destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out","required time  "+ destinationplanList.get(j).getDay());


            }



            noday = new ArrayList<>();
            noday.clear();

            //  noday.add(1);

            Integer day = 0;
       /* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*/
            if(destinationplanList.get(0).getDay() == 0)
            {
                day = destinationplanList.get(0).getDay();
                noday.add(destinationplanList.get(0).getDay());
            }
            for (int i = 0; i < destinationplanList.size(); i++) {

                destinationplanList.get(i).setPos(i);
                try {
                    if (createTripList.get(0).getEnddate().length() > 0) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                        try {
                            Date date = format.parse(createTripList.get(0).getEnddate());
                            c.setTime(date);
                            c.add(Calendar.DATE, day);
                            destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();

                        }

                    }
                } catch (Exception e) {
                }

                Log.d("days","pos"+day+"v2"+destinationplanList.get(i).getDay());

                if (day != destinationplanList.get(i).getDay() ) {
                    day = destinationplanList.get(i).getDay();
                    noday.add(destinationplanList.get(i).getDay());

                }


            }

            Log.d("System out","Reponse of set time of days count "+noday.size());
            for (int j = 0; j < noday.size(); j++) {
                AddDayWisePlan(j);
            }

            if (noday.size() > 0)
                if (noday.size() == 1) {
                    tv_no_days.setText(noday.size() + " Day");
                } else {
                    tv_no_days.setText(noday.size() + " Days");
                }
            else
                tv_no_days.setText("");
            pb_dialog.dismiss();

        }
    }


    public void AddDayWisePlan(int pos) {


        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.add_day_wise_plan_layout, null);

        TextView tv_day = (TextView) v.findViewById(R.id.tv_day);
        TextView tv_des_date = (TextView) v.findViewById(R.id.tv_des_date);
        tv_des_date.setText("");
        try {

            if (createTripList.get(0).getEnddate().length() > 0) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                try {
                    Date date = format.parse(createTripList.get(0).getEnddate());
                    c.setTime(date);
                    c.add(Calendar.DATE, noday.get(pos));
                    tv_des_date.setText(format.format(c.getTime()));
                    return_excepted_date=format.format(c.getTime());

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tv_des_date.setText("");
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
            tv_des_date.setText("");

        }
        Log.d("System out","day pos "+noday.get(pos));
        if (noday.size() > 0) {
            if (noday.get(0).intValue() == 0)
                tv_day.setText("Day " + (noday.get(pos) + 1));
            else
                tv_day.setText("Day " + noday.get(pos));
        }
        else
        {
            tv_day.setText("Day " + noday.get(pos));
        }
        ll_add_places_to_plan = (LinearLayout) v.findViewById(R.id.ll_add_places_to_plan);

        //  ll_day_wise_plan.removeAllViews();
        //  ll_add_places_to_plan.removeAllViews();

        //set time


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == noday.get(pos)) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        if (tempdestiDestinationPlanlist.size() > 0) {
            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            //  SimpleDateFormat format = new SimpleDateFormat("HH:mm");

            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                // Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime());
                Date date1 = format.parse(createTripList.get(0).getEnddate()+" "+temptime);
                Date firstPoiDate=format.parse(createTripList.get(0).getEnddate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime() && pos==0)
                {

                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY) + "." + ((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                    }
                    else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }



                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                }
                else
                {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                tv_des_date.setText("");
            }

            Double tempDepart=tempdestiDestinationPlanlist.get(0).getArrivalTime();


            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempDepart) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    //      dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                    if (tempdestiDestinationPlanlist.get(j).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(07.00);
                    } else {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(destinationplanList.get(j).getStarttime());
                    }
                    tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());

                    tempDepart=tempdestiDestinationPlanlist.get(j).getArrivalTime();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }

        }

        ll_add_places_to_plan.removeAllViewsInLayout();
        for (int i = 0; i < tempdestiDestinationPlanlist.size(); i++) {

            AddPlacesToPlan(i);
        }
        ll_day_wise_plan.addView(v);
    }
    public void AddPlacesToPlan(final int pos) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v;
        v = inflater.inflate(R.layout.add_places_to_plan_layout, null);
        ImageView iv_dotted_line = (ImageView) v.findViewById(R.id.iv_dotted_line);
        iv_dotted_line_bottom = (ImageView) v.findViewById(R.id.iv_dotted_line_bottom);
        tv_km_time_plan = (TextView) v.findViewById(R.id.tv_km_time_plan);
        final TextView  et_stay_time = (TextView) v.findViewById(R.id.et_stay_time);
        final TextView tv_hrs=(TextView)v.findViewById(R.id.tv_hrs);
        LinearLayout   ll_edit_stay = (LinearLayout) v.findViewById(R.id.ll_bg_edit);
        LinearLayout ll_edit_start_time = (LinearLayout) v.findViewById(R.id.ll_bg_edit_starttime);
        TextView tv_start_end_time = (TextView) v.findViewById(R.id.tv_start_end_time);
        TextView tv_des_name = (TextView) v.findViewById(R.id.tv_des_name);
        final CircleImageView fc_poi_image = (CircleImageView) v.findViewById(R.id.fc_poi_image);
        final ImageView iv_poi_delete = (ImageView) v.findViewById(R.id.iv_poi_delete);
        tv_arrival_time = (TextView) v.findViewById(R.id.tv_arrival_time);
        TextView tv_km_time = (TextView) v.findViewById(R.id.tv_km_time_plan);

        et_stay_time.setId(tempdestiDestinationPlanlist.get(pos).getPos());
        et_stay_time.setTag(tempdestiDestinationPlanlist.get(pos).getPos());
        Log.d("edittext_pos",String.valueOf(tempdestiDestinationPlanlist.get(pos).getPos()));



        if (editIconUnselected) {
            iv_poi_delete.setVisibility(View.VISIBLE);
            ll_edit_stay.setBackground(getResources().getDrawable(R.drawable.textbox_edit));



            //   et_stay_time.setEnabled(true);

            if (pos == 0) {
                //  ll_edit_start_time.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
                tv_arrival_time.setEnabled(false);
                ll_edit_start_time.setBackground(null);

            }
            else {
                ll_edit_start_time.setBackground(null);
                tv_arrival_time.setEnabled(false);
            }
        } else {
            iv_poi_delete.setVisibility(View.GONE);
            ll_edit_stay.setBackground(null);
            ll_edit_start_time.setBackground(null);
            //  et_stay_time.setEnabled(false);
        }


        ll_edit_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_stay_time.performClick();
            }
        });
        et_stay_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //select="timerDuration";

                //ll_edit_stay.getId();


                if(editIconUnselected) {


                    main_pos = view.getId();

                    BigDecimal bd = new BigDecimal((destinationplanList.get(main_pos).getStayTime() - Math.floor(destinationplanList.get(main_pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int amin = bd.intValue();
                    Log.d("System out","stay__________"+destinationplanList.get(main_pos).getStayTime().intValue()+"  "+amin);


                    final Dialog timerDialog = new Dialog(getActivity());

                    timerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    timerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    timerDialog.setContentView(R.layout.timer_dialog);
                    timerDialog.show();
                    NumberPicker np = (NumberPicker)timerDialog. findViewById(R.id.np);
                    NumberPicker np1 = (NumberPicker)timerDialog. findViewById(R.id.np1);
                    ImageView ImgClosepopup=(ImageView)timerDialog.findViewById(R.id.ImgClosepopup);
                    ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            timerDialog.dismiss();
                        }
                    });
                    Button btn_ok =(Button)timerDialog.findViewById(R.id.btn_ok);
                    final String[] hrs= new String[]{"00", "01", "02", "03","04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                    final String[] mins= new String[]{"00", "15", "30", "45"};

                    np.setMinValue(0);
                    //Specify the maximum value/number of NumberPicker
                    np.setMaxValue(hrs.length-1);
                    np1.setMinValue(0);
                    np1.setMaxValue(mins.length-1);

                    //Gets whether the selector wheel wraps when reaching the min/max value.
                    np.setWrapSelectorWheel(true);
                    np1.setWrapSelectorWheel(true);

                    np.setDisplayedValues(hrs);
                    np1.setDisplayedValues(mins);
                    a=destinationplanList.get(main_pos).getStayTime().intValue();
                    b=amin;


                    np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                    np1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                    np.setValue(destinationplanList.get(main_pos).getStayTime().intValue()); // example value

                    if(amin==0)
                    {
                        np1.setValue(0);
                    }
                    else if(amin<=15)
                    {
                        np1.setValue(1);
                    }
                    else if(amin<=30)
                    {
                        np1.setValue(2);
                    }
                    else if(amin>31)
                    {
                        np1.setValue(3);
                    }

                    np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                        @Override
                        public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                            a=Integer.valueOf(hrs[newVal].toString());
                        }
                    });
                    np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                        @Override
                        public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                            b=Integer.valueOf(mins[newVal].toString());
                        }
                    });



                   /* tpd1 = TimePickerDialog.newInstance(
                            MyPlanFragment.this,
                            destinationplanList.get(main_pos).getStayTime().intValue(),
                            amin, true
                    );

                    tpd1.show(getActivity().getFragmentManager(), "Timepickerdialog1");
*/

                   btn_ok.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {
                           timerDialog.dismiss();
                            String timertime = "";

                                timertime = a+"."+b;

                           String total_time=timertime;
                           et_stay_time.setText(timertime);
                            int pos1=Integer.valueOf(main_pos);

                            Log.d("System out","timettime___"+timertime+" pos "+pos1);



                            if(pos1>=0) {

                                if (!excepted_date.equalsIgnoreCase(""))
                                {
                                    oldvalue = destinationplanList.get(pos1).getStayTime().toString();
                                    Date expecteddate = null;
                                    Date returndatenew = null;
                                    try {

                                        DecimalFormat f = new DecimalFormat("##.00");
                                        Log.d("System out", "last date " + return_excepted_date + "Time  " + Constants.formatDate(f.format(destinationplanList.get(destinationplanList.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
                                        Log.d("System out", "return start time " + excepted_date + "Time  " + excepted_time);


                                        String date_excepted_date = excepted_date + " " + Constants.formatDate(excepted_time, "HH:mm", "hh:mm a");
                                        String date_return_excepted_date = return_excepted_date + " " + Constants.formatDate(f.format(destinationplanList.get(destinationplanList.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

                                        SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");

                                        expecteddate = format.parse(date_excepted_date);
                                        returndatenew = format.parse(date_return_excepted_date);


                                     //   System.out.println(date);
                                    } catch (ParseException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(returndatenew);
                                    DecimalFormat f = new DecimalFormat("##.00");
                                    //  calendar.add(Calendar.HOUR,Integer.parseInt(et_stay_time.getText().toString()));
                                    if (et_stay_time.getText().toString().contains(".")) {
                                        calendar.add(Calendar.HOUR, Integer.parseInt(et_stay_time.getText().toString().substring(0, et_stay_time.getText().toString().indexOf("."))));
                                    } else {
                                        calendar.add(Calendar.HOUR, Integer.parseInt(et_stay_time.getText().toString()));
                                    }
                                    Log.d("System out", "return start time " + expecteddate.toString());
                                    Log.d("System out", "new saved time " + calendar.getTime());
                                    Log.d("System out", "new saved time " + calendar.toString());
                                    Log.d("System out", "new saved time check condition " + calendar.getTime().after(expecteddate));

                                    Log.d("timebetween", destinationplanList.get(pos1).getStarttime().toString() + "end" + destinationplanList.get(pos1).getEndtime() + "timestay " + et_stay_time.getText().toString());
                                    if ((destinationplanList.get(pos1).getEndtime() - destinationplanList.get(pos1).getStarttime()) < Double.valueOf(et_stay_time.getText().toString())) {
                                        Constants.show_error_popup(getActivity(), "Our system is limited to 7 hours to visit attractions. We are working to improve it.", tv_save_my_plan);
                                        et_stay_time.setText(oldvalue);
                                    }
                                    else    if (calendar.getTime().after(expecteddate))
                                    {
                                        //   Constants.show_error_popup(getActivity(), "", tv_save_my_plan);
                                        et_stay_time.setText(oldvalue);
                                        setReturnDate(expecteddate.toString(), calendar.getTime().toString());
                                    }
                                    else if ((destinationplanList.get(pos1).getEndtime() - destinationplanList.get(pos1).getStarttime()) >= Double.valueOf(et_stay_time.getText().toString())) {
                                        destinationplanList.get(pos1).setStayTime(Double.valueOf(et_stay_time.getText().toString()));
                                        dbHandler.update_stay_time_destination_plan(destinationplanList.get(pos1).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                        ll_day_wise_plan.removeAllViewsInLayout();
                                        destinationplanList = dbHandler.get_DestinationList(tripid);
                                        set_destination_layout();
                                    } else {
                                        et_stay_time.setText(oldvalue);
                                        //setReturnDate();
                                        Constants.show_error_popup(getActivity(), "No time for additional attractions. ", tv_save_my_plan);
                                    }
                                } else {
                                    if ((destinationplanList.get(pos1).getEndtime() - destinationplanList.get(pos1).getStarttime()) >= Double.valueOf(et_stay_time.getText().toString())) {
                                        destinationplanList.get(pos1).setStayTime(Double.valueOf(et_stay_time.getText().toString()));
                                        dbHandler.update_stay_time_destination_plan(destinationplanList.get(pos1).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                        ll_day_wise_plan.removeAllViewsInLayout();
                                        destinationplanList = dbHandler.get_DestinationList(tripid);
                                        et_stay_time.setText(timertime);
                                        set_destination_layout();
                                    } else {
                                        et_stay_time.setText(oldvalue);
                                        Constants.show_error_popup(getActivity(), "Our system is limited to 7 hours to visit attractions. We are working to improve it.", tv_save_my_plan);
                                    }
                                }
                            }
                        }
                    });



                }

                // et_stay_time.requestFocus();
              /*  final Dialog timer = new Dialog(getActivity());

                timer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                timer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timer.setContentView(R.layout.time_select_popup);
                list_hrs=(ListView)timer.findViewById(R.id.list_hrs);
                list_minutes=(ListView)timer.findViewById(R.id.list_minutes);
                tv_cancel=(TextView)timer.findViewById(R.id.tv_cancel);
                tv_ok=(TextView)timer.findViewById(R.id.tv_ok);
                timer.show();
                hrsvalues= new String[] { "01","02","03","04","05","06","08","09","10"};
                minsvalues= new String[] { "00","10","20","30","40","50"};

                hrsaArrayAdapter = new Hrsadapter(getActivity(), hrsvalues);
                list_hrs.setAdapter(hrsaArrayAdapter);

                minsArrayAdapter = new MinsAdapter(getActivity(),minsvalues);
                list_minutes.setAdapter(minsArrayAdapter);

                list_hrs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                       *//* Log.d("System out","hrs before____"+hrs);

                        hrs=String.valueOf(list_hrs.getItemAtPosition(i));
                        Log.d("System out","hrs____"+hrs);*//*


                    }
                });
                list_minutes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        *//* min=String.valueOf(list_minutes.getItemAtPosition(i));
                        Log.d("System out","mins___"+min);*//*


                    }
                });
                tv_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timer.dismiss();
                        String total_time=hrs+"."+min;
                        et_stay_time.setText(total_time);
                    int pos1=Integer.valueOf(et_stay_time.getTag().toString());




                    if(pos1>=0)
                    {

                        if(!excepted_date.equalsIgnoreCase("")) {
                            oldvalue=destinationplanList.get(pos1).getStayTime().toString();
                            Date expecteddate = null;
                            Date returndatenew = null;
                            try {

                                DecimalFormat f = new DecimalFormat("##.00");
                                Log.d("System out", "last date " + return_excepted_date + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
                                Log.d("System out", "return start time " + excepted_date + "Time  " + excepted_time);


                                String date_excepted_date = excepted_date + " " + Constants.formatDate(excepted_time, "HH:mm", "hh:mm a");
                                String date_return_excepted_date = return_excepted_date + " " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

                                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");

                                expecteddate = format.parse(date_excepted_date);
                                returndatenew = format.parse(date_return_excepted_date);


                                System.out.println(date);
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(returndatenew);
                            DecimalFormat f = new DecimalFormat("##.00");
                            //  calendar.add(Calendar.HOUR,Integer.parseInt(et_stay_time.getText().toString()));
                            if (et_stay_time.getText().toString().contains(".")) {
                                calendar.add(Calendar.HOUR, Integer.parseInt(et_stay_time.getText().toString().substring(0, et_stay_time.getText().toString().indexOf("."))));
                            } else {
                                calendar.add(Calendar.HOUR, Integer.parseInt(et_stay_time.getText().toString()));
                            }
                            Log.d("System out", "return start time " + expecteddate.toString());
                            Log.d("System out", "new saved time " + calendar.getTime());
                            Log.d("System out", "new saved time " + calendar.toString());
                            Log.d("System out", "new saved time check condition " + calendar.getTime().after(expecteddate));
                            Log.d("timebetween", destinationplanList.get(pos1).getStarttime().toString() + "end" + destinationplanList.get(pos1).getEndtime() + "timestay " + et_stay_time.getText().toString());
                            if (calendar.getTime().after(expecteddate)) {
                                Constants.show_error_popup(getActivity(), "", tv_save_my_plan);
                                et_stay_time.setText(oldvalue);
                                setReturnDate(expecteddate.toString(), calendar.getTime().toString());
                            } else if ((destinationplanList.get(pos1).getEndtime() - destinationplanList.get(pos1).getStarttime()) >= Double.valueOf(et_stay_time.getText().toString())) {
                                destinationplanList.get(pos1).setStayTime(Double.valueOf(et_stay_time.getText().toString()));
                                dbHandler.update_stay_time_destination_plan(destinationplanList.get(pos1).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                ll_day_wise_plan.removeAllViewsInLayout();
                                set_destination_layout();
                            } else {
                                et_stay_time.setText(oldvalue);
                                //setReturnDate();
                                Constants.show_error_popup(getActivity(), "No time for additional attractions. ", tv_save_my_plan);
                            }
                        }else
                        {
                            if ((destinationplanList.get(pos1).getEndtime() - destinationplanList.get(pos1).getStarttime()) >= Double.valueOf(et_stay_time.getText().toString())) {
                                destinationplanList.get(pos1).setStayTime(Double.valueOf(et_stay_time.getText().toString()));
                                dbHandler.update_stay_time_destination_plan(destinationplanList.get(pos1).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                ll_day_wise_plan.removeAllViewsInLayout();
                                set_destination_layout();
                            } else {

                                Constants.show_error_popup(getActivity(), "No time for additional attractions. ", tv_save_my_plan);
                            }
                        }


                    }

     }
                });
                tv_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timer.dismiss();
                    }
                });

                Log.d("System out","old date and time "+oldvalue);*/
                //  Constants.showKeyboard(getActivity(),ll_edit_stay);
            }
        });
       /* ll_edit_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // et_stay_time.performClick();
                main_pos=view.getId();
                tpd1.show(getActivity().getFragmentManager(), "Timepickerdialog1");

                //et_stay_time=(EditText)v.findViewById(tempdestiDestinationPlanlist.get(pos).getPos());
               // setValue.performClick();

            }
        });*/














        double roundOff1 = Math.round(tempdestiDestinationPlanlist.get(pos).getKM() / 1000 * 100.0) / 100.0;
        // tv_km_time.setText(roundOff1 + " Kms / " + Constants.convert_minute_hrs_both(tempdestiDestinationPlanlist.get(pos).getTime()));
        tv_km_time.setText(roundOff1 + " Kms ");


        DecimalFormat f = new DecimalFormat("##.00");
        try {
            tv_arrival_time.setText(Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
        } catch (ParseException e) {
            e.printStackTrace();

        }
        et_stay_time.setText(tempdestiDestinationPlanlist.get(pos).getStayTime().toString());


        tv_des_name.setText(tempdestiDestinationPlanlist.get(pos).getPoiName());


        // Log.d("starttime","name"+destinationplanList.get(pos).getPoiName()+"time"+f.format(destinationplanList.get(pos).getStarttime()));


        if (tempdestiDestinationPlanlist.get(pos).getStarttime() == 24.00)
        {
            tv_start_end_time.setText("24 hrs");

        }
        else {
            String starttime = "0:0", endtime = "0:0";

            try {

                starttime = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getStarttime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");
                endtime = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getEndtime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            } catch (ParseException e) {
                e.printStackTrace();
                starttime = "0:0";
                endtime = "0:0";
            }

            tv_start_end_time.setText(starttime + " - " + endtime);


        }

        Log.d("imageName",tempdestiDestinationPlanlist.get(pos).getImagename());
        Bitmap bitmap = new ImageSaver(getActivity()).
                setFileName(tempdestiDestinationPlanlist.get(pos).getImagename()).
                setExternal(true).
                load();

        if (bitmap != null)
            fc_poi_image.setImageBitmap(bitmap);


        if (pos > 0) {
            iv_dotted_line.setVisibility(View.VISIBLE);
        }
        if (pos == tempdestiDestinationPlanlist.size() - 1) {
            iv_dotted_line_bottom.setVisibility(View.INVISIBLE);
            tv_km_time_plan.setVisibility(View.INVISIBLE);
        }
        iv_poi_delete.setTag(tempdestiDestinationPlanlist.get(pos).getWayPointId());
        iv_poi_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Delete_Poi_Api(Integer.parseInt(iv_poi_delete.getTag().toString()));
            }
        });

        tv_arrival_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tpd = TimePickerDialog.newInstance(
                        MyPlanFragment.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE), true
                );
                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
            }
        });

        //update destination trip
        int pos1=Integer.valueOf(et_stay_time.getTag().toString());
        update_destination_plan(pos1);

        ll_add_places_to_plan.addView(v);
    }



    public void setReturnDate(String expected,String newtime)
    {

        final Dialog myTripDialog = new Dialog(getActivity());

        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.plan_trip_from_to_popup);
        final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        tv_start_date_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_return_trip);
        tv_start_time_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_time_my_return_trip);
        TextView tv_error_msg = (TextView)myTripDialog.findViewById(R.id.tv_error_msg);
        tv_error_msg.setVisibility(View.VISIBLE);
       // tv_error_msg.setText("Please update return date and time! after added this return date: "+newtime + "but actual Return date is :"+expected);
        tv_error_msg.setText("Return date & time changed. Confirm. "+expected);
        TextView tv_source = (TextView) myTripDialog.findViewById(R.id.tv_source);
        TextView tv_des = (TextView) myTripDialog.findViewById(R.id.tv_des);

        String date = "", time = "";


        dpd = DatePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );


        tpd = TimePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );


        tv_source.setText(createTripList.get(0).getDestination());
        tv_des.setText(createTripList.get(0).getSource());

        date = excepted_date;
        time = excepted_time;
        tv_start_date_my_return_trip.setText(excepted_date);


        try {
            tv_start_time_my_return_trip.setText(Constants.formatDate(String.valueOf(excepted_time), "HH:mm", "hh:mm a"));
        } catch (ParseException e) {
            e.printStackTrace();

        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");
        Date date1 = null;
        try {
            date1 = sdf1.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);

            dpd.setMinDate(calendar);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");


        createTripList.clear();
        createTripList = dbHandler.get_TRIP_By_TripId(tripid);


        tv_start_date_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //select="start";
                selected="returnchnage";
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


            }

        });

        tv_start_time_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  select="stop";
                selected="returnchnage";
                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");


            }
        });

        tv_plan_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_plan_my_return_trip);
        tv_plan_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String start_date = "", end_time = "", start_date1 = "", end_time1 = "";

                try {
                    start_date = tv_start_date_my_return_trip.getText().toString();
                    end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Log.d("System out","date and time before add deffence "+start_date+" "+end_time);
                Date expecteddate = null;
                Date returndatenew = null;
                Date returndatelatest = null;
                try {

                    DecimalFormat f = new DecimalFormat("##.00");
                    Log.d("System out", "last date " + return_excepted_date + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
                    Log.d("System out", "return start time " + excepted_date + "Time  " + excepted_time);


                    String date_excepted_date=excepted_date +" "+Constants.formatDate(excepted_time, "HH:mm", "hh:mm a");
                    String date_return_excepted_date=createTripList.get(0).getReturn_end_date() +" "+Constants.formatDate(createTripList.get(0).getReturn_end_time(), "HH:mm", "hh:mm a");
                    String date_return_latestend=start_date +" "+Constants.formatDate(end_time, "HH:mm", "hh:mm a");

                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");

                    expecteddate = format.parse(date_excepted_date);
                    returndatenew= format.parse(date_return_excepted_date);
                    returndatelatest=format.parse(date_return_latestend);
                    long diff = returndatenew.getTime() - expecteddate.getTime();

                    long seconds = diff / 1000 % 60;
                    long minutes = diff / (60 * 1000) % 60;
                    long hours = diff / (60 * 60 * 1000) % 24;
                    long days = diff / (24 * 60 * 60 * 1000);

                    Log.d("System out", "difference days "+days+" hourse "+ + hours + "minutes  " + minutes);


                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(returndatelatest);
                    calendar.add(Calendar.DATE, (int) days);
                    calendar.add(Calendar.HOUR, (int) hours);
                    calendar.add(Calendar.MINUTE, (int) minutes);
                    calendar.add(Calendar.SECOND, (int) seconds);

                    SimpleDateFormat format1 = new SimpleDateFormat("dd MMM ,yyyy");
                    SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
                    start_date1=format1.format(calendar.getTime());
                    end_time1=format2.format(calendar.getTime());

                    Log.d("System out","date and time after addd deffence "+start_date+" "+end_time);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }




                if(CheckDates(excepted_date+" "+excepted_time,start_date+" "+end_time)) {
                    dbHandler.update_Return_trip_status(tripid);
                    dbHandler.update_Return_trip_date_time_ETA(tripid, start_date, end_time,start_date1,end_time1);
                    update_trip();
                    myTripDialog.dismiss();
                }
                else
                {

                    Constants.show_error_popup(getActivity(), "Please change trip return date time.",tv_plan_my_return_trip);

                }

            }
        });
        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });
    }

    public  void update_trip()
    {
        try {
            createTripList.clear();
            createTripList=dbHandler.get_TRIP_By_TripId(tripid);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String tripdate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime() + ":00";
            String tripenddate = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime() + ":00";

            String returndate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time() + ":00";
            excepted_date=createTripList.get(0).getReturn_start_date();
            excepted_time=createTripList.get(0).getReturn_start_time();

            String returndatetimeeta=createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time() + ":00";
            Log.d("start_date", returndate);
            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            try {
                returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                returndate = "";
            }

            try {
                returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                returndatetimeeta = "";

            }

            String tripstatus=createTripList.get(0).getTripstatus();
            if(tripdate.length()>0&&createTripList.get(0).getTripstatus().contains("Saved"))
                tripstatus=  "UpComing";

            String json = "[{\"TripName\": \"" + createTripList.get(0).getTripname() + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(0).getSourceId() + ",\"TripDestination_End\":" + createTripList.get(0).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + createTripList.get(0).getTripId()  +  "\",\"TripStatus\": \"" + tripstatus +"\",\"TripGooglePlaceDetails\":\"" + createTripList.get(0).getDes_address() + "\"}]";
            Log.d("json_CreateTrip1", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<UpdateTrip> call = apiService.updateTrip(json);
            call.enqueue(new Callback<UpdateTrip>() {
                @Override
                public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                    if (response.body() != null) {
                        Bundle b = new Bundle();
                        b.putInt("tripId", tripid);
                        b.putString("plantype","DTOS");
                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);


                    }
                }

                @Override
                public void onFailure(Call<UpdateTrip> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    pb_dialog.dismiss();

                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                    }

                }
            });
        }
        catch (Exception e){}
    }
    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).tv_text_header.setText("My Itinerary at "+createTripList.get(0).getDestination());
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);


    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + " " + (++monthOfYear) + "," + year;
        //  dateTextView.setText(date);
        if(selected.equalsIgnoreCase("returnchnage"))
        {
            try {
                tv_start_date_my_return_trip.setText(Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy"));
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }else if(selected.equalsIgnoreCase("return_trip"))
        {
            try {
                tv_start_date_my_return_trip.setText(Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy"));
                tpd.show(getActivity().getFragmentManager(), "TimePickerDialog");

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        else
        {
            tpd.show(getActivity().getFragmentManager(), "TimePickerDialog");
        }



    }

    @Override
    public void onTimeSet(RadialPickerLayout view1, int hourOfDay, int minute, int second) {

        String time = hourOfDay + ":" + minute + "";




        try {

            if (selected.equalsIgnoreCase("start"))
            {
                date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM,yyyy hh:mm a");
                tv_start_date_my_trip.setText(date1);
            }
            else if (selected.equalsIgnoreCase("return"))
            {
                date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM,yyyy hh:mm a");
                tv_return_date_my_trip.setText(date1);
            }
            else if(selected.equalsIgnoreCase("returnchnage"))
            {
                //  date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM,yyyy hh:mm a");
                tv_start_time_my_return_trip.setText(Constants.formatDate(time, "HH:mm", "hh:mm a"));
            }
            else if(selected.equalsIgnoreCase("return_trip"))
            {
                Date expecteddate = null;
                Date returndatenew = null;
                Date stayTime=null;
                try {

                    DecimalFormat f = new DecimalFormat("##.00");
                    //    Log.d("System out", "last date " + return_excepted_date + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
                    Log.d("System out", "return start time " + date + "Time  " + time);


                    String date_excepted_date = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM ,yyyy hh:mm a");
                    // String stay_time = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getStayTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");
                    //   String date_return_excepted_date = return_excepted_date + " " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");
                    SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm a");


                    // Calendar calendar = Calendar.getInstance();
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Double[] des_plan_date_time_eta=get_no_days_destination_plan();

                    calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

                    calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
                    calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

                    //  arrival_date=sdf.format(calendar.getTime());

                    expecteddate = format.parse(date_excepted_date);
                    returndatenew = format.parse(format.format(calendar.getTime()));
                    //stayTime=dateformat.parse(stay_time);

                    Log.d("System out", "return start time after add stay time " + returndatenew.toString());

                    System.out.println(date);


                    if (expecteddate.before(returndatenew)) {
                        // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                        Constants.show_error_popup(getActivity(), "Invalid Return date and time ", tv_start_time_my_return_trip);
                        tv_start_date_my_return_trip.setText("");
                        tv_start_time_my_return_trip.setText("");

                    } else {
                        tv_start_time_my_return_trip.setText(Constants.formatDate(time, "HH:mm", "hh:mm a"));

                    }

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            else if(selected.equalsIgnoreCase("Addstart"))
            {

               /* start_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                start_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");*/
                date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM, yyyy hh:mm a");
                Log.d("System out","date1___"+date1);
                String tripdate=start_date+" "+start_time;
                String returndate=end_date+" "+end_time;
                Log.d("System out","tripdate___"+tripdate);
                Log.d("System out","returndate___"+returndate);
                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date1);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf1.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf1.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);
                //calendar1.add(Calendar.HOUR, 2);
                calendar1.add(Calendar.HOUR, 0);
                Log.d("System out","datenew___"+datenew);
                Log.d("System out","current "+datecurrent);
                Log.d("System out","ctime___"+calendar1.getTime());
                Log.d("System out","ctime___"+calendar1.getTime().after(datenew));
                Log.d("System out","ctime___"+datecurrent.before(datenew));

                //  currentDateandTime.set
                if (calendar1.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                    //  Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", tv_save_my_trip);


                }else  if (datenew.before(datecurrent))
                {
                    // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", tv_save_my_trip);

                }
                else {

                    start_date = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
                    start_time = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "HH:mm");
                    date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM,yyyy hh:mm a");
                    tv_start_date_my_trip.setText(date1);
                }
            }

            else if(selected.equalsIgnoreCase("Addstop"))
            {
                end_date = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
                end_time = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "HH:mm");
                date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM,yyyy hh:mm a");
                tv_return_date_my_trip.setText(date1);
            }

            else
            {
                date1 = Constants.formatDate(time + " " + time, "HH:mm", "hh:mm a");



            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    private void Delete_Poi_Api(final Integer waypointid) {
        // get & set progressbar dialog

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{     \"TripWayPointId\": \"" + waypointid + "\"}]";

        Log.d("json_CreateTrip", json);

        Call<ArrayList<RemoveTripPoi>> call = apiService.RemovePoi(json);
        call.enqueue(new Callback<ArrayList<RemoveTripPoi>>() {
            @Override
            public void onResponse(Call<ArrayList<RemoveTripPoi>> call, final Response<ArrayList<RemoveTripPoi>> response) {
                if (response.body() != null) {


                    if (response.body().get(0).getResStatus() == true) {

                        pb_dialog.dismiss();
                        dbHandler.delete_Destination_ById(waypointid);
                        //     AddDayWisePlan();
                        ll_day_wise_plan.removeAllViews();
                        destinationplanList = dbHandler.get_DestinationList(tripid);

                        for (int i = 1; i < destinationplanList.size(); i++) {
                            getdistancetime(i);
                        }

                        if (destinationplanList.size() == 1) {
                            set_destination_layout();
                        }
                        //set_destination_layout();


                    } else {
                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().get(0).getResDescription(), tv_save_my_plan);
                    }
                } else {
                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), tv_save_my_plan);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RemoveTripPoi>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), tv_save_my_plan);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), tv_save_my_plan);
                }

            }
        });


    }


    private void getdistancetime(final int pos) {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        //  pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CalculateDistance>> call = apiService.GetDistanceTimebylatlong("api/userMobileApi/GetDistanceDurationFromGoogle?lat1=" + destinationplanList.get(pos).getPoiLatitude() + "&lon1=" + destinationplanList.get(pos).getPoiLongitude() + "&lat2=" + destinationplanList.get(pos - 1).getPoiLatitude() + "&lon2=" + destinationplanList.get(pos - 1).getPoiLongitude() + "&apiKey=chndroid");
        call.enqueue(new Callback<ArrayList<CalculateDistance>>() {
            @Override
            public void onResponse(Call<ArrayList<CalculateDistance>> call, Response<ArrayList<CalculateDistance>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();


                    if (response.body().get(0).getDistance().getText() != null) {
                        destinationplanList.get(pos - 1).setTime(Double.valueOf(response.body().get(0).getDuration().getText()));
                        destinationplanList.get(pos - 1).setKM(Double.valueOf(response.body().get(0).getDistance().getText()));
                        dbHandler.update_destination_plan_km_time(destinationplanList.get(pos-1).getTripId(),destinationplanList.get(pos-1).getKM(),destinationplanList.get(pos-1).getTime());
                    }

                    if (pos == destinationplanList.size() - 1) {
                        set_destination_layout();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<CalculateDistance>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });

    }

    public void update_destination_plan(int pos) {

        try {




            String triparrival = destinationplanList.get(pos).getPoiDate() + " " + destinationplanList.get(pos).getArrivalTime().toString().replace(".", ":") + ":00";
            String tripdeparture = destinationplanList.get(pos).getPoiDate() + " " + destinationplanList.get(pos).getDepartureTime().toString().replace(".", ":") + ":00";


            try {
                triparrival = Constants.formatDate(triparrival, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                Log.d("resumedate1", triparrival);
                triparrival = "";

            }

            try {
                tripdeparture = Constants.formatDate(tripdeparture, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripdeparture = "";
            }



            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String json ="";
            if(triparrival.length()>0)
                json = "[{     \"TripWayType\": \"" + destinationplanList.get(pos).getPoitype() + "\",\"TripWayRefId\": \"" + destinationplanList.get(pos).getPoiServerId() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"" + triparrival + "\" ,\"TripEstOutTime\":\"" + tripdeparture + "\",\"TripDesId\":" + destinationplanList.get(pos).getPoiDesID() + ", \"TripIsPause\":\"false\" ,\"TripDistanceInKm\":" + destinationplanList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + destinationplanList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + false + ",\"TripSpendTime\":" + destinationplanList.get(pos).getStayTime() + ",\"TripWayPointId\":" + destinationplanList.get(pos).getWayPointId() +  ",\"TripWayDayNo\":"+destinationplanList.get(pos).getDay() +",\"TripIsNightStayOn\":" + false + "}]";
            else
                json = "[{     \"TripWayType\": \"" + destinationplanList.get(pos).getPoitype() + "\",\"TripWayRefId\": \"" + destinationplanList.get(pos).getPoiServerId() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripDesId\":" + destinationplanList.get(pos).getPoiDesID() + ", \"TripIsPause\":\"false\" ,\"TripDistanceInKm\":" + destinationplanList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + destinationplanList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + false + ",\"TripSpendTime\":" + destinationplanList.get(pos).getStayTime() + ",\"TripWayPointId\":" + destinationplanList.get(pos).getWayPointId() +  ",\"TripWayDayNo\":"+destinationplanList.get(pos).getDay() +",\"TripIsNightStayOn\":" + false + "}]";

            Log.d("json_update_poi_plan1", json);

            Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
            call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
                @Override
                public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {


                }

                @Override
                public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();


                }
            });
        }
        catch (Exception e){}

    }

    public static boolean CheckDates(String startDate, String endDate) {



        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = false;  // If two dates are equal.
            } else {
                b = false;
                // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare",String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare",e.toString());
        }
        return b;
    }
    public void Mytrip_Dialog(final Activity activity)
    {
        final SessionManager sessionManager1 = new SessionManager(activity);
        myTripDialog=new Dialog(activity);
        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_my_trips);
        tv_start_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_start_date_my_trip);
        tv_return_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_return_date_my_trip);
        final TextView tv_for_better_dates=(TextView)myTripDialog.findViewById(R.id.tv_for_better_dates);
        final ImageView iv_swt_off_my_trip=(ImageView)myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
        final LinearLayout ll_start_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        LinearLayout ll_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_trip);
        et_trip_name=(EditText)myTripDialog.findViewById(R.id.et_trip_name);
        av_tripname= (AutoCompleteTextView) myTripDialog.findViewById(R.id.av_tripname);
        tv_save_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_save_my_trip);

        et_trip_name.setVisibility(View.GONE);
        av_tripname.setVisibility(View.VISIBLE);
        ll_trip.setVisibility(View.INVISIBLE);
        av_tripname.setText(createTripList.get(0).getTripname());
        et_trip_name.setText(createTripList.get(0).getTripname());

          /*  av_tripname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<myTripArrayList.size();i++)
                    {
                       if(av_tripname.getText().toString().equalsIgnoreCase(myTripArrayList.get(i).getTripName()))
                       {
                           try {
                               tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                               tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));

                           } catch (ParseException e) {
                               e.printStackTrace();
                           }
                       }
                    }

                }
            });*/

           /* av_tripname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                        tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));

                        start_date=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");
                        Log.d("System out","tripdate___"+start_date +" "+start_time);
                        start_time=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","HH:mm");
                        Log.d("System out","tripdate___"+start_date +" "+start_time);
                        end_date  = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM ,yyyy");
                        Log.d("System out","returndate___"+end_date+"  "+end_time);
                        end_time = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm");

                        Log.d("System out","returndate___"+end_date+"  "+end_time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });*/
        Calendar now = Calendar.getInstance();


        dpd = DatePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf.format(new Date());

        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 0);
        dpd.setMinDate(calendar);
        tpd = TimePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),false
        );


        //  tpd.setMinTime(calendar.getTime().getHours(),calendar.getTime().getMinutes(),calendar.getTime().getSeconds());

        tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if(tripFlag==true) {
                selected = "Addstart";
                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                //  }


            }

        });

        tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //    if(tripFlag==true) {
                selected = "Addstop";


                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                //  }
                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            }
        });
/*
        iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tripFlag==false)
                {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                    tv_for_better_dates.setVisibility(View.GONE);
                    ll_start_date_my_trip.setAlpha(1);
                    ll_return_date_my_trip.setAlpha(1);
                    tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tripFlag==true) {
                                selected = "Addstart";
                                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                            }


                        }

                    });

                    tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tripFlag==true) {
                                selected = "Addstop";


                                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                            }
                            //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                        }
                    });
                    tripFlag=true;



                }
                else
                {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                    tv_for_better_dates.setVisibility(View.VISIBLE);
                    ll_start_date_my_trip.setAlpha((float) 0.6);
                    ll_return_date_my_trip.setAlpha((float) 0.6);
                    tripFlag=false;
                }

            }
        });
*/

        tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System out","save1 click___"+et_trip_name.getText().toString());
                if (et_trip_name.getText().toString().length() > 0)
                {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {

                        Constants.show_error_popup(getActivity(), "Select your trip dates.",tv_save_my_trip);
                    }
                    else if(tv_start_date_my_trip.getText().length() == 0)
                    {
                        myTripDialog.dismiss();
                        Bundle b = new Bundle();
                        b.putInt("tripId", tripid);
                        b.putString("plantype","STOD");
                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                        // Constants.show_error_popup(getActivity(), "Please select Trip start date", tv_save_my_trip);
                    }

                    else
                    {

                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;




                        if(returndate.length()>2)
                        {
                            Log.d("System out","return date___________"+returndate);

                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;

                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(createTripList.get(0).getTime().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);

                                    tripdate=sdf.format(calendar.getTime());
                                    Log.d("trip_Date_eta_c",tripdate.toString());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate))
                                {

                                    update_trip(0,et_trip_name.getText().toString());
                                }
                                else {
                                    try {
                                        //Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(createTripList.get(0).getTime().doubleValue()) + " reach at destination",tv_save_my_trip);
                                        Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule.",tv_save_my_trip);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. ", getActivity().findViewById(R.id.error_layout));
                                    }
                                }
                            }
                            else
                            {
                                //  Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(createTripList.get(0).getTime().doubleValue()) + " reach at destination",tv_save_my_trip);
                                Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule.",tv_save_my_trip);
                            }
                        }
                        else {
                            Log.d("System out","return date else___________"+returndate);

                            update_trip(0,et_trip_name.getText().toString());
                        }
                    }
                } else

                {
                    Constants.show_error_popup(getActivity(), "Please Enter Trip Name", getActivity().findViewById(R.id.error_layout));
                }
            }

        });


        /*tv_save_my_trip1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tripFlag==true && tv_start_date_my_trip.getText().length() == 0 ) {
                    Constants.show_error_popup(activity, "Please select Trip start date", activity.findViewById(R.id.error_layout));

                }else if (av_tripname.getText().toString().length() > 0) {

                    Log.d("Sizemytrip","trip name size"+searchtripesname.size());
                    String flag = "";
                    for (int i = 0; i < searchtripesname.size(); i++) {
                        Log.d("Sizemytrip","trip name size in for "+searchtripesname.get(i)+"   "+av_tripname.getText().toString().trim());
                        if (searchtripesname.get(i).equalsIgnoreCase(av_tripname.getText().toString().trim())) {
                            String tripdate=tv_start_date_my_trip.getText().toString();
                            String returndate=tv_return_date_my_trip.getText().toString()
                            if(returndate.length()>2)
                            {
                                if(CheckDates(tripdate,returndate))
                                {
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                    Date date = null;
                                    int pos=Integer.parseInt(Constants.selectedPosition);
                                    try {
                                        date = sdf.parse(tripdate);
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(date);
                                        int[] convertime = splitToComponentTimes(exploreDestList.get(pos).getDuration().doubleValue());
                                        calendar.add(Calendar.HOUR, convertime[0]);
                                        calendar.add(Calendar.MINUTE, convertime[1]);
                                        Log.d("trip_Date_eta",date.toString());
                                        tripdate=sdf.format(calendar.getTime());
                                        //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                    } catch (ParseException e) {
                                        e.printStackTrace();

                                    }
                                    if(CheckDates(tripdate,returndate)) {
                                        //  CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                        // searchtripesnameby.add(searchtripesname.get(i));
                                        Log.d("Sizemytrip","trip name id "+searchtripesids.get(i));
                                        int check_trip_in_db = dbHandler.check_trip_in_local_db(Integer.parseInt(searchtripesids.get(i)));

                                        Log.d("Sizemytrip",check_trip_in_db+"");
                                        tripid=Integer.parseInt(searchtripesids.get(i));
                                        myTripDialog.dismiss();
                                        if (check_trip_in_db >= 1) {

                                            Integer pos1 = Integer.valueOf(posi);

                                            if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)
                                                if(!excepted_date.trim().equalsIgnoreCase("")) {
                                                    addData();
                                                }
                                                else {
                                                    Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                                }

                                            else {

                                                Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                            }
                                        } else {
                                            getMyTrip(tripid);
                                            // add_trip_in_to_database(position);
                                        }
                                        flag+="true";
                                    }
                                    else {
                                        try {
                                            Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                        }
                                    }
                                }
                                else
                                {
                                    int pos=Integer.parseInt(Constants.selectedPosition);
                                    Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                }
                            }
                            else {
                                // Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                                int pos=Integer.parseInt(Constants.selectedPosition);
                                //CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                // searchtripesnameby.add(searchtripesname.get(i));
                                Log.d("Sizemytrip","trip name id "+searchtripesids.get(i));
                                int check_trip_in_db = dbHandler.check_trip_in_local_db(Integer.parseInt(searchtripesids.get(i)));

                                Log.d("Sizemytrip",check_trip_in_db+"");
                                tripid=Integer.parseInt(searchtripesids.get(i));
                                myTripDialog.dismiss();
                                if (check_trip_in_db >= 1) {

                                    Integer pos1 = Integer.valueOf(posi);

                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)
                                        if(!excepted_date.trim().equalsIgnoreCase("")) {
                                            addData();
                                        }
                                        else {
                                            Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                        }

                                    else {
                                        Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                    }
                                } else {
                                    getMyTrip(tripid);
                                    // add_trip_in_to_database(position);
                                }
                                flag+="true";
                            }
                        }
                        else
                        {
                            flag+="false";

                        }
                    }

                    if(!flag.contains("true"))
                    {

                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;

                        if(returndate.length()>2 && tripFlag == true)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;
                                int pos=Integer.parseInt(Constants.selectedPosition);
                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDestList.get(pos).getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta",date.toString());
                                    tripdate=sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate)) {
                                    CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                }
                                else {
                                    try {
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                    }
                                }
                            }
                            else
                            {
                                int pos=Integer.parseInt(Constants.selectedPosition);
                                Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                            }
                        }
                        else {
                            // Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                            int pos=Integer.parseInt(Constants.selectedPosition);
                            CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                        }

                    }
                }
                else
                {
                    Constants.show_error_popup(activity, "Please Enter Trip Name", activity.findViewById(R.id.error_layout));
                }
            }

        });*/
        ImageView ImgClosepopup=(ImageView)myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

    }

    public  void update_trip(final Integer pos,String tripname)
    {
        try {
            pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(false);
            pb_dialog.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String tripdate = start_date + " " + start_time + ":00";
            String tripenddate = "";
            String returndate = end_date + " " + end_time + ":00";
            String returndatetimeeta="";

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm:ss");
            Date date = null;

            try {
                date = sdf.parse(tripdate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(createTripList.get(0).getTime());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);
                Log.d("trip_Date_eta", date.toString());
                tripenddate = sdf.format(calendar.getTime());
                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {
                e.printStackTrace();
                tripenddate="";

            }


            try {
                date = sdf.parse(returndate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(createTripList.get(0).getTime());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);
                Log.d("trip_Date_eta", date.toString());
                returndatetimeeta = sdf.format(calendar.getTime());
                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {
                e.printStackTrace();
                returndatetimeeta="";

            }



            Log.d("start_date", returndate);
            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            try {
                returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                returndate = "";
            }

            try {
                returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                returndatetimeeta = "";

            }

            String json = "[{ \"TripName\": \"" + tripname + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(pos).getSourceId() + ",\"TripDestination_End\":" +createTripList.get(pos).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + createTripList.get(pos).getTripId() +  "\",\"TripStatus\": \"" + "UpComing" +"\",\"TripGooglePlaceDetails\":\"" + "" + "\"}]";
            Log.d("json_CreateTrip1", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<UpdateTrip> call = apiService.updateTrip(json);
            final String finalReturndate = returndate;
            final String finalReturndatetimeeta = returndatetimeeta;
            call.enqueue(new Callback<UpdateTrip>() {
                @Override
                public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                    if (response.body() != null) {

                        pb_dialog.dismiss();
                        if (response.body().getResStatus() == true) {
                            dbHandler.update_Start_trip_date_time(createTripList.get(pos).getTripId(), start_date, start_time);
                            dbHandler.update_Return_trip_date_time(createTripList.get(pos).getTripId(), finalReturndate, finalReturndatetimeeta, true);
                            if (myTripDialog != null)
                                myTripDialog.dismiss();
                            String trip_start_time = "", trip_end_time = "";
                            createTripList.clear();
                            createTripList = dbHandler.get_TRIP_By_TripId(tripid);
                            try {
                                trip_start_time = Constants.formatDate(createTripList.get(0).getStarttime(), "HH:mm", "hh:mm a");
                                trip_end_time = Constants.formatDate(createTripList.get(0).getEndtime(), "HH:mm", "hh:mm a");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }



                            String tripdate = createTripList.get(0).getStartdate() + " " + trip_start_time;
                            String tripenddate = createTripList.get(0).getEnddate() + " " + trip_end_time;
                            excepted_date=createTripList.get(0).getReturn_start_date();
                            excepted_time=createTripList.get(0).getReturn_start_time();

                            tv_source.setText(createTripList.get(0).getSource());
                            tv_destination.setText(createTripList.get(0).getDestination());
                            ((ExploreActivity) getActivity()).tv_text_header.setText("My Itinerary at "+createTripList.get(0).getDestination());

                            tv_trip_start_date.setText(tripdate);
                            tv_trip_end_date.setText(tripenddate);
                            circleivsource.setImageBitmap(Constants.getImage(createTripList.get(0).getSource_image()));
                            circleivdes.setImageBitmap(Constants.getImage(createTripList.get(0).getDestination_image()));


                            Bundle b = new Bundle();
                            b.putInt("tripId", tripid);
                            b.putString("plantype","STOD");
                            ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                            // UpdateTrip_status("UpComing", pos);
                            // getMyTrip();
                        }
                        else
                        {
                            Constants.show_error_popup(getActivity(),""+response.body().getResDescription(), tv_save_my_trip);

                            // Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                            //   Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                        }
                    }
                }

                @Override
                public void onFailure(Call<UpdateTrip> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    pb_dialog.dismiss();

                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                    }

                }
            });
        }
        catch (Exception e){}
    }


    private class Hrsadapter extends BaseAdapter {
        Activity activity;
        private String[] valuesHrs1;

        Hrsadapter(Activity activity,String[] valuesHrs) {
            this.activity = activity;
            this.valuesHrs1 = valuesHrs;

        }

        @Override
        public int getCount() {
            return valuesHrs1.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater)getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.cell, null);
            final MyTextView tv_text_timer = (MyTextView) v.findViewById(R.id.tv_text_timer);
            // final MyTextView statename = (MyTextView) v.findViewById(R.id.tvstatenamepopup);

            //  statename.setText(cityListpopup.get(position).get("State"));
            tv_text_timer.setText(valuesHrs1[position]);
            v.setId(position);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("System out","hrs before____"+hrs);

                    hrs=String.valueOf(valuesHrs1[view.getId()]);
                    Log.d("System out","hrs____"+hrs);
                }
            });


            return v;

        }


       /* public void filter(String charText) {
            charText = charText.toString();
            cityListOfpopup.clear();
            if (charText.length() == 0) {
                cityListpopup.addAll(arraylist);

            } else {
                for (HashMap<String, String> salesHis : arraylist) {
                    Log.d("System out", "msg text " + charText);
                    if (salesHis.get("City").toLowerCase()
                            .contains(charText.toLowerCase())) {
                        cityListpopup.add(salesHis);
                    }
                }
            }
            cityAdapter.notifyDataSetChanged();
        }*/
    }

    private class MinsAdapter extends BaseAdapter {
        Activity activity;
        private String[] valuesMins1;

        MinsAdapter(Activity activity,String[] valuesMins) {
            this.activity = activity;
            this.valuesMins1 = valuesMins;

        }

        @Override
        public int getCount() {
            return valuesMins1.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater)getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.cell, null);
            final MyTextView tv_text_timer = (MyTextView) v.findViewById(R.id.tv_text_timer);
            // final MyTextView statename = (MyTextView) v.findViewById(R.id.tvstatenamepopup);

            //  statename.setText(cityListpopup.get(position).get("State"));
            v.setId(position);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    min=String.valueOf(valuesMins1[view.getId()]);
                    Log.d("System out","mins___"+min);
                }
            });

            tv_text_timer.setText(valuesMins1[position]);

            return v;

        }


       /* public void filter(String charText) {
            charText = charText.toString();
            cityListOfpopup.clear();
            if (charText.length() == 0) {
                cityListpopup.addAll(arraylist);

            } else {
                for (HashMap<String, String> salesHis : arraylist) {
                    Log.d("System out", "msg text " + charText);
                    if (salesHis.get("City").toLowerCase()
                            .contains(charText.toLowerCase())) {
                        cityListpopup.add(salesHis);
                    }
                }
            }
            cityAdapter.notifyDataSetChanged();
        }*/
    }
    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }
    public void return_trip_dialog(){


        final Dialog myTripDialog = new Dialog(getActivity());

        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.plan_trip_from_to_popup);
        final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        tv_start_date_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_return_trip);
        tv_start_time_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_time_my_return_trip);

        TextView tv_source = (TextView) myTripDialog.findViewById(R.id.tv_source);
        TextView tv_des = (TextView) myTripDialog.findViewById(R.id.tv_des);

        String date = "", time = "";


        try {
            DecimalFormat f = new DecimalFormat("##.00");
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Double[] des_plan_date_time_eta=get_no_days_destination_plan();

            calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

            calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
            calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());


            dpd = DatePickerDialog.newInstance(
                    MyPlanFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)

            );
            dpd.setMinDate(calendar);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        tpd = TimePickerDialog.newInstance(
                MyPlanFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );


        tv_source.setText(createTripList.get(0).getDestination());
        tv_des.setText(createTripList.get(0).getSource());



        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");


        createTripList.clear();
        createTripList = dbHandler.get_TRIP_By_TripId(tripid);

        String arrival_date = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime();


        if (destinationplanList.size() > 0)
        {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Double[] des_plan_date_time_eta=get_no_days_destination_plan();

            calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

            calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
            calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

            arrival_date=sdf.format(calendar.getTime());


            Log.d("resumetimedparrival_hrs", des_plan_date_time_eta[1].intValue()+" min "+des_plan_date_time_eta[2].intValue());


        }

        Log.d("resumetimedparrival", arrival_date);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(arrival_date));
            calendar.add(Calendar.MINUTE,5);
            arrival_date=sdf.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            tv_start_date_my_return_trip.setText(Constants.formatDate(String.valueOf(arrival_date), "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy"));
            tv_start_time_my_return_trip.setText(Constants.formatDate(String.valueOf(arrival_date), "dd MMM ,yyyy HH:mm", "hh:mm a"));
        } catch (ParseException e) {
            e.printStackTrace();

        }


        tv_start_date_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //select="start";
                selected = "return_trip";
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


            }

        });



        tv_start_time_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  select="stop";
                selected = "return_trip";

                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");

            }
        });

        final TextView tv_plan_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_plan_my_return_trip);
        final String finalArrival_date = arrival_date;
        tv_plan_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_start_date_my_return_trip.getText().toString().equalsIgnoreCase(""))

                {
                    Constants.show_error_popup(getActivity(), "Select your trip dates.", tv_plan_my_return_trip);
                } else if (tv_start_time_my_return_trip.getText().toString().equalsIgnoreCase("")) {
                    Constants.show_error_popup(getActivity(), "Please select trip start  time.", tv_plan_my_return_trip);
                } else {

                    String start_date = "", end_time = "";

                    try {
                        start_date = tv_start_date_my_return_trip.getText().toString();
                        end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.d("start_date____", start_date+"end_time____"+end_time);



                    Log.d("getenddate____", createTripList.get(0).getEnddate());
                    Log.d("resumetimedparrival", finalArrival_date);


                    if (CheckDates(finalArrival_date, start_date + " " + end_time)) {

                        dbHandler.update_Return_trip_status(tripid);

                        dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, true);


                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


                        long return_date_diff[] = get_diff_between_date_hours(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time(), createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time());


                        try {


                            Calendar calendar2 = Calendar.getInstance();
                            //     hours=get_diff_between_date_hours(createTripList.get(0).getReturn_start_date()+" "+createTripList.get(0).getReturn_start_time(),createTripList.get(0).getReturn_end_date()+" "+createTripList.get(0).getReturn_end_time());
                            calendar2.setTime(dateFormat.parse(start_date + " " + end_time));
                            calendar2.add(Calendar.DATE, (int) return_date_diff[0]);
                            calendar2.add(Calendar.HOUR, (int) return_date_diff[1]);
                            calendar2.add(Calendar.MINUTE, (int) return_date_diff[2]);

                            String return_end_date_time = dateFormat.format(calendar2.getTime());

                            String return_end_date = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy");
                            String return_end_time = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "HH:mm");
                            dbHandler.update_return_end_trip_date_time(tripid, return_end_date, return_end_time);
                        } catch (Exception e) {
                        }


                        update_trip();
                        myTripDialog.dismiss();


                    } else {
                        Constants.show_error_popup(getActivity(), "Please change trip return date time.", tv_plan_my_return_trip);
                    }
                }
            }

        });
        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

    }
    public long[] get_diff_between_date_hours(String formdate, String todate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        long diff = 0;
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = dateFormat.parse(formdate);
            d2 = dateFormat.parse(todate);

            diff = d2.getTime() - d1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //   Log.d("datediff","time"+dateFormat.format(fromCalender.getTime())+"to"+dateFormat.format(toCalender.getTime()));
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long[] datediffvalue = new long[3];
        datediffvalue[0] = diffDays;
        datediffvalue[1] = diffHours;
        datediffvalue[2] = diffMinutes;
        return datediffvalue;

    }

    public Double[] get_no_days_destination_plan()
    {
        Double[] destination_plan={0.0,0.0,0.0};
        Double total_time_stay_last=0.0;

        for(int i=0;i<Constants.listDefaultValuesArrayList.size();i++)
        {
            if(Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
            if(Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

        }
        if (destinationplanList.size() > 0) {
      /*      if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(1);*/
            try {

                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEndtime());
                Date date1 = format.parse(temptime);
                Date firstPoiDate=format.parse(destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime())
                {
                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                    }
                    else {

                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }





                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(0);
                }
                else
                {
                    if (destinationplanList.get(0).getStarttime() == 24.00) {
                        destinationplanList.get(0).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                destinationplanList.get(0).setDay(1);
                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

            }

            for (int j = 1; j < destinationplanList.size(); j++) {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());


                if ((destinationplanList.get(j).getDepartureTime() - destinationplanList.get(0).getArrivalTime()) > dayhours) {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);
                    dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }


            }

        }

        noday = new ArrayList<>();
        noday.clear();

        //  noday.add(1);

        Integer day = 0;


        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);

            // Log.d("days","pos"+String.valueOf(j)+"value"+noday.get(j).toString()+"v2"+String.valueOf(destinationplanList.get(i).getDay()));
            if (day != destinationplanList.get(i).getDay()) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());
            }

        }

        Integer last_day=0;

        if(noday.size()>0)
            last_day=noday.get(noday.size()-1);


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == last_day) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }

        if (tempdestiDestinationPlanlist.size() > 0) {
           /* if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
            } else {
                tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
            }


            tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
            tempdestiDestinationPlanlist.get(0).setDay(1);*/

            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            SimpleDateFormat format = new SimpleDateFormat("HH:mm");

            try {
                Date date = format.parse(createTripList.get(0).getEndtime());
                Date date1 = format.parse(temptime);
                Date firstPoiDate=format.parse(tempdestiDestinationPlanlist.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime())
                {

                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY) + "." + ((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                    }
                    else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }



                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                }
                else
                {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempdestiDestinationPlanlist.get(0).getArrivalTime()) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }
            total_time_stay_last=tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size()-1).getDepartureTime();

            Log.d("total_time_stay_last",String.valueOf(total_time_stay_last));
        }

        destination_plan[0]=Double.valueOf(noday.size()); //day
        //int[] convertime = splitToComponentTimes(total_time_stay_last);
        destination_plan[1]=Double.valueOf(total_time_stay_last.intValue()); //hours
        BigDecimal bd = new BigDecimal((total_time_stay_last - Math.floor(total_time_stay_last)) * 100);
        bd = bd.setScale(4, RoundingMode.HALF_DOWN);

        destination_plan[2]=Double.valueOf(bd.doubleValue()); //minute



        return destination_plan;


    }


}
