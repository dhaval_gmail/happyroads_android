package com.bpcl.happyroads;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.MyBearingTracking;
import com.mapbox.mapboxsdk.constants.MyLocationTracking;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationListener;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.widgets.MyLocationViewSettings;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Poi_Night;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.Direction;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.GetRO;
import com.bpcl.happyroads.Pojo.Insert_Poi;
import com.bpcl.happyroads.Pojo.ListDefaultValues;
import com.bpcl.happyroads.Pojo.ListServiceCategory;
import com.bpcl.happyroads.Pojo.Place;
import com.bpcl.happyroads.Pojo.PushNotification;
import com.bpcl.happyroads.Pojo.RouteLegStep;
import com.bpcl.happyroads.Pojo.RoutePojo;
import com.bpcl.happyroads.Pojo.ServicesPojo;
import com.bpcl.happyroads.Pojo.TollBooth;
import com.bpcl.happyroads.Pojo.TripCompleted;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.Pojo.latlong;
import com.bpcl.happyroads.Utils.CircleImageView;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DirectionsJSONParser;
import com.bpcl.happyroads.Utils.GPSTracker;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.Utils.ImageSaver;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/30/2016.
 */
public class TrackRoutePlanningFragment extends Fragment implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    static final LatLng source = new LatLng();
    static final LatLng destination = new LatLng();
    private static final String TAG = "LocationPickerActivity";


    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 125;

    public String select_dpd_type = "";
    public Integer avg_par_km = 1;
    public Integer total_avg_par_km = 0;
    public Integer radius_meter = 200;
    public Direction route_direction;

    public Integer offroute_radius_meter = 1000;
    public Integer push_radius_meter = 1500;
    public Integer complete_stay_radius_meter = 1000;

    String temptime = "19:00";
    NavigationView left_navView;
    DrawerLayout drawer_layout_services;
    ImageView iv_bottom_up_arrow, iv_dotted_line_bottom, iv_directions;
    TextView  iv_left_arrow_map, iv_left_arrow_map_shut;
    LinearLayout ll_bottom_up, ll_places_between_S_to_D_on_trip_tracking, ll_left_arrow_map, ll_new_trip, ll_on_tracking;
    LinearLayout ll_places_between_S_to_D, ll_S_to_D_on_trip_tracking, ll_plan_at_D_on_trip_tracking, ll_D_to_S_on_trip_tracking;
    TextView tv_S_to_D_tab_line, tv_S_to_D, tv_plan_at_D, tv_plan_at_D_tab_line, tv_D_to_S, tv_D_to_S_tab_line;
    Gson gson = new Gson();
    String name = "";
    ImageView iv_map_my_plan;

    ImageView iv_left_arrow_hsv, iv_right_arrow_hsv;
    ViewGroup parent;
    TextView tv_D_to_S_from_left_drawer;
    int i = 1;



    ExploreActivity exploreActivity = null;
    FrameLayout fl_pop_up_zoo;
    TextView tv_explore_from_map;

    int position = 3;

    //Trip Plan
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    Calendar now;
    String date;
    String date1 = null;

    LinearLayout ll_route_plan, ll_close_pop_up_map,ll_no_plan;
    TextView tv_start_date_my_return_trip, tv_start_time_my_return_trip;
    TextView tv_origin, tv_destination, tv_start_date, tv_start_time, tv_end_date, tv_end_time, tv_trip_distance_time, tv_add_poi, tv_services_details;
    AutoCompleteTextView tv_place_of_des;
    DBHandler dbHandler;
    List<Create_Trip> createTripList = new ArrayList<>();
    List<Create_Poi> createPoiList = new ArrayList<>();
    List<Create_Trip> createTripList_display = new ArrayList<>();
    List<Create_Poi> createPoiList_display = new ArrayList<>();

    Integer avgSpeed = 60;
    CircleImageView iv_circle_source, iv_circle_destination;
    Integer tripid = -1;
    Dialog pb_dialog;
    MapboxMap map = null;
    MapView mapView;
    LatLngBounds.Builder b1;


    GPSTracker gpsTracker;
    TextView tv_poi_map_name, tv_poi_map_des, tv_poi_map_distance;
    SimpleDraweeView fc_poi_map_image;
    ImageView iv_close_poi_popup;
    Integer orginId, destinationId;
    SessionManager sessionManager;

    List<ListServiceCategory> listServiceCategoryList;
    List<List<HashMap<String, String>>> routelineresult;
    LinearLayout llserviceslayout;
    TextView tv_services_location, tv_services_side;
    Integer selected_poi_pos = -1;
    Integer selected_ro_pos = -1;
    Integer selected_ser_pos = -1;
    Integer selected_des_pos = -1;
    RecyclerView recycleview_cat_list;
    Service_List_Adapter serviceListAdapter;
    LinearLayoutManager horizontalLayoutManagaer;
    TollBoothAdapter toolBooth_list_adapter;

    Double orginlat = 0.0, originlong = 0.0, deslat = 0.0, deslong = 0.0;
    ArrayAdapter<String> adapter;
    ArrayList<String> placeList;
    ArrayList<ExplorePOI> explorePoiList;
    Integer category_select_pos = -1;

    TextView tv_resume_date_my_trip, tv_resume_time_my_trip;
    Integer night_poi_id = -1;
    List<Create_Poi_Night> create_poi_nightsList;
    Boolean return_trip_plan = false;
    Integer return_plan = 0;
    RelativeLayout error_layout;
    TextView  iv_tollbooth;
    String from = "";
    LinearLayout ll_top_header_plan;
    ImageView iv_edit_stay_time;
    Boolean edit_stay_time = false;
    Integer S_To_D_Plan = 0;
    String excepted_date = "", excepted_time = "", return_excepted_date = "", return_excepted_time = "";
    ArrayList<TollBooth> tollBoothArrayList = new ArrayList<>();
    ImageView iv_map_call;
    LinearLayout ll_des_plan;
    List<DestinationPlan> destinationplanList = new ArrayList<>();
    Double dayhours = 8.0;
    Double starttime = 7.0;
    ArrayList<Integer> noday;
    ArrayList<DestinationPlan> tempdestiDestinationPlanlist;
    boolean editIconUnselected = false;
    LinearLayout ll_add_places_to_plan;
    boolean desplan = false;
    String plantype = "";
    String default_EmergencyNumber = "";
    //offline track
    SharedPreferences preferences;
    List<LatLng> points = null;
    List<PushNotification> pushNotificationList;
    PolylineOptions lineOptions = null;

    FrameLayout frame_push_popup;
    CircleImageView circleImageView_push;
    TextView tv_push_msg, tv_push_msg1;
    ImageView iv_push_close;
    LinearLayout ll_push_pop,ll_D_to_S_from_left_drawer;

    ImageView iv_map_center;
    PowerManager.WakeLock wl = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    SimpleDateFormat sdf_second = new SimpleDateFormat("dd MMM ,yyyy HH:mm:ss");
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM ,yyyy");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

    Handler handler;
    Runnable runnable;
    DecimalFormat df = new DecimalFormat("#.00");

    Double remaining_km = 0.0;
    Double remaining_time = 0.0;
    ImageView iv_voice;
    Boolean voice_flag = true;
    int max_zoom = 17;

    private ArrayList<RoutePojo> routepojo = null;
    private List<ServicesPojo> services = null;
    private List<RoutePojo.OnAdminRoutePoiList> onAdminRoutePoiLists = null;
    private List<RoutePojo.OnAdminRoutePoiList> onAdminRouteRoLists = null;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private int zoomlevel = 1;
    private TextToSpeech ttobj;
    private boolean  complatepopup = false;

    private List<Create_Poi> tempcreatePoiList;
    private double current_distance = 0.0, current_distance_start = 0.0;

    private LocationServices locationServices;



    public String current_time_offRoute="";
    private Dialog myTripDialog;
    private Dialog myTripDialog1;
    TextView tv_tripCompleted,tv_add,tv_edit;



    public static boolean CheckDates(String startDate, String endDate) {


        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = false;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare", String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }

    public static boolean CheckDates1(String startDate, String endDate) {


        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = false;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = true; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare", String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.track_route_planning_fragment, container, false);
        ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).toolbar.setBackgroundResource(R.drawable.black_bg_home);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_menu_header.setVisibility(View.INVISIBLE);
        ((ExploreActivity) getActivity()).ll_top_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);

        //  mLocationProvider = new LocationProvider(getActivity(), TrackRoutePlanningFragment.this);


        if (getArguments() != null) {
            plantype = getArguments().getString("plantype", "");
        }
        dbHandler = new DBHandler(getActivity());

        tollBoothArrayList = new ArrayList<>();

        if (getArguments() != null)
            tripid = getArguments().getInt("tripId", -1);


        if (tripid == -1) {
            return null;

        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            name = getArguments().getString("from", "");
        } else {
            name = "";
        }

        sessionManager = new SessionManager(getActivity());


        left_navView = (NavigationView) v.findViewById(R.id.left_navView);
        fl_pop_up_zoo = (FrameLayout) v.findViewById(R.id.fl_pop_up_zoo);
        //  iv_poi_unselected=(ImageView)v.findViewById(R.id.iv_poi_unselected);
        iv_map_my_plan = (ImageView) v.findViewById(R.id.iv_map_my_plan);
        iv_directions = (ImageView) v.findViewById(R.id.iv_directions);
        //  tv_poi_unselected=(TextView)v.findViewById(R.id.tv_poi_unselected);
        tv_explore_from_map = (TextView) v.findViewById(R.id.tv_explore_from_map);
        tv_explore_from_map.setVisibility(View.GONE);

        drawer_layout_services = (DrawerLayout) v.findViewById(R.id.drawer_layout_route);
        ll_bottom_up = (LinearLayout) v.findViewById(R.id.ll_bottom_up);
        ll_places_between_S_to_D = (LinearLayout) v.findViewById(R.id.ll_places_between_S_to_D);
        ll_left_arrow_map = (LinearLayout) v.findViewById(R.id.ll_left_arrow_map);
        ll_new_trip = (LinearLayout) v.findViewById(R.id.ll_new_trip);
        ll_route_plan = (LinearLayout) v.findViewById(R.id.ll_route_plan);
        ll_close_pop_up_map = (LinearLayout) v.findViewById(R.id.ll_close_pop_up_map);
        ll_no_plan = (LinearLayout) v.findViewById(R.id.ll_no_plan);
        // ll_places_between_S_to_D_on_trip_tracking=(LinearLayout)v.findViewById(R.id.ll_places_between_S_to_D_start_tracking);
        ll_S_to_D_on_trip_tracking = (LinearLayout) v.findViewById(R.id.ll_S_to_D_on_trip_tracking);
        ll_D_to_S_on_trip_tracking = (LinearLayout) v.findViewById(R.id.ll_D_to_S_on_trip_tracking);
        ll_plan_at_D_on_trip_tracking = (LinearLayout) v.findViewById(R.id.ll_plan_at_D_on_trip_tracking);
        tv_S_to_D_tab_line = (TextView) v.findViewById(R.id.tv_S_to_D_tab_line);
        tv_S_to_D = (TextView) v.findViewById(R.id.tv_S_to_D);
        tv_plan_at_D = (TextView) v.findViewById(R.id.tv_plan_at_D);
        tv_plan_at_D_tab_line = (TextView) v.findViewById(R.id.tv_plan_at_D_tab_line);
        tv_D_to_S = (TextView) v.findViewById(R.id.tv_D_to_S);
        tv_D_to_S_tab_line = (TextView) v.findViewById(R.id.tv_D_to_S_tab_line);
        tv_D_to_S_from_left_drawer = (TextView) v.findViewById(R.id.tv_D_to_S_from_left_drawer);
        tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
        iv_left_arrow_hsv = (ImageView) v.findViewById(R.id.iv_left_arrow_hsv);
        iv_right_arrow_hsv = (ImageView) v.findViewById(R.id.iv_right_arrow_hsv);
        llserviceslayout = (LinearLayout) v.findViewById(R.id.ll_services);
        tv_services_location = (TextView) v.findViewById(R.id.tv_service_location);
        tv_services_side = (TextView) v.findViewById(R.id.tv_service_side);
        error_layout = (RelativeLayout) v.findViewById(R.id.error_layout);
        tv_start_date = (TextView) v.findViewById(R.id.tv_start_date);
        tv_start_time = (TextView) v.findViewById(R.id.tv_start_time);
        tv_end_date = (TextView) v.findViewById(R.id.tv_end_date);
        tv_end_time = (TextView) v.findViewById(R.id.tv_end_time);
        tv_origin = (TextView) v.findViewById(R.id.tv_origin);
        tv_destination = (TextView) v.findViewById(R.id.tv_destination);
        tv_trip_distance_time = (TextView) v.findViewById(R.id.tv_trip_distance_time);
        tv_add_poi = (TextView) v.findViewById(R.id.add_poi);
        tv_services_details = (TextView) v.findViewById(R.id.tv_services_details);
        iv_circle_source = (CircleImageView) v.findViewById(R.id.iv_circle_source);
        iv_circle_destination = (CircleImageView) v.findViewById(R.id.iv_circle_destination);
        tv_place_of_des = (AutoCompleteTextView) v.findViewById(R.id.tv_place_of_des);
        iv_tollbooth = (TextView) v.findViewById(R.id.iv_tollbooth);
        tv_add=(TextView)v.findViewById(R.id.tv_add);
        tv_edit=(TextView)v.findViewById(R.id.tv_edit);
        tv_add.setVisibility(View.GONE);
        tv_edit.setVisibility(View.GONE);
        ll_des_plan = (LinearLayout) v.findViewById(R.id.ll_des_plan);
        FragmentManager myFM = getActivity().getSupportFragmentManager();

        LinearLayout topLinearLayout = new LinearLayout(getActivity());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        fc_poi_map_image = (SimpleDraweeView) v.findViewById(R.id.fc_poi);
        tv_poi_map_name = (TextView) v.findViewById(R.id.tv_poi_name);
        tv_poi_map_des = (TextView) v.findViewById(R.id.tv_poi_des);
        tv_poi_map_distance = (TextView) v.findViewById(R.id.tv_distance_time);
        iv_close_poi_popup = (ImageView) v.findViewById(R.id.iv_close_poi_popup);
        recycleview_cat_list = (RecyclerView) v.findViewById(R.id.recycleview_cat_list);

        ll_top_header_plan = (LinearLayout) v.findViewById(R.id.ll_top_header_plan);
        iv_edit_stay_time = (ImageView) v.findViewById(R.id.iv_edit_stay_time);
        iv_map_call = (ImageView) v.findViewById(R.id.iv_map_call);

        //push popup

        frame_push_popup = (FrameLayout) v.findViewById(R.id.frame_push_popup);
        tv_push_msg = (TextView) v.findViewById(R.id.tv_push_msg);
        iv_push_close = (ImageView) v.findViewById(R.id.iv_close_push_popup);
        circleImageView_push = (CircleImageView) v.findViewById(R.id.push_image);
        ll_push_pop = (LinearLayout) v.findViewById(R.id.ll_push_pop);
        ll_D_to_S_from_left_drawer = (LinearLayout) v.findViewById(R.id.ll_D_to_S_from_left_drawer);
        ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        tv_push_msg1 = (TextView) v.findViewById(R.id.tv_push_msg1);
        iv_map_center = (ImageView) v.findViewById(R.id.iv_map_center);
        iv_voice = (ImageView) v.findViewById(R.id.iv_voice);
        tv_tripCompleted=(TextView)v.findViewById(R.id.tv_tripCompleted);

        frame_push_popup.setVisibility(View.GONE);


        serviceListAdapter = new Service_List_Adapter();
        //((ExploreActivity) getActivity()).ll_header_main.setEnabled(false);

        Constants.executeLogcat(getActivity());

        MapboxAccountManager.start(getActivity(), getString(R.string.accessToken));

        mapView = (MapView) v.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);


        iv_edit_stay_time.setVisibility(View.GONE);
        iv_bottom_up_arrow = (ImageView) v.findViewById(R.id.iv_bottom_up_arrow);
        iv_left_arrow_map_shut = (TextView) v.findViewById(R.id.iv_left_arrow_map_shut);

        iv_left_arrow_map = (TextView) v.findViewById(R.id.iv_left_arrow_map);


        drawer_layout_services.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, left_navView);
        //  drawer_layout_services.setDrawerLockMode (DrawerLayout.LOCK_MODE_LOCKED_OPEN,((ExploreActivity)getActivity()).navigationView);


        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        tv_explore_from_map.setVisibility(View.GONE);
        pb_dialog = new Dialog(getActivity());
        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        b1 = new LatLngBounds.Builder();
        lineOptions = new PolylineOptions();
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        locationServices = LocationServices.getLocationServices(getActivity());
        locationServices.toggleGPS(true);
        preferences = getActivity().getSharedPreferences(Constants.TripTrackPREFERENCES, 0);

      /*  preferences.edit().putBoolean("trip_start_tracking", true).commit();
        preferences.edit().putInt("tripId", tripid).commit();*/

        listServiceCategoryList = new ArrayList<>();
        destinationplanList = dbHandler.get_DestinationList(tripid);



        route_direction = new Direction();

        tempcreatePoiList = new ArrayList<>();
        edit_stay_time = false;
        now = Calendar.getInstance();

        PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();


        tv_D_to_S_from_left_drawer.setVisibility(View.GONE);



        iv_left_arrow_map_shut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.hideSoftKeyBoardOnTabClicked(getActivity(), v);
                drawer_layout_services.closeDrawer(left_navView);
                //  iv_left_arrow_map_shut.setImageResource(R.drawable.map_arrow_right_slider);
            }
        });
        iv_left_arrow_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (drawer_layout_services.isDrawerOpen(left_navView))
                {

                   // iv_left_arrow_map.setImageResource(R.drawable.map_arrow_left_slider);
                    iv_left_arrow_map.setText("C\nL\nO\nS\nE");

                    Constants.hideSoftKeyBoardOnTabClicked(getActivity(), iv_left_arrow_map);
                    drawer_layout_services.closeDrawer(left_navView);
                    ll_bottom_up.setVisibility(View.VISIBLE);
                    if (fl_pop_up_zoo.getVisibility() == View.VISIBLE) {
                        Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_down);
                        fl_pop_up_zoo.startAnimation(bottomUpDown);
                        fl_pop_up_zoo.setVisibility(View.GONE);
                    }
                    //((ExploreActivity)getActivity()).drawerLayout.closeDrawer(((ExploreActivity)getActivity()).navigationView);
                }
                else {
                    Constants.hideSoftKeyBoardOnTabClicked(getActivity(), iv_left_arrow_map);
//                    iv_left_arrow_map.setImageResource(R.drawable.map_arrow_right_slider);
                    iv_left_arrow_map.setText("O\nP\nE\nN");

                    drawer_layout_services.openDrawer(left_navView);
                    ll_bottom_up.setVisibility(View.GONE);
                    if (fl_pop_up_zoo.getVisibility() == View.VISIBLE) {
                        Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_down);
                        fl_pop_up_zoo.startAnimation(bottomUpDown);
                        fl_pop_up_zoo.setVisibility(View.GONE);
                    }

                }


            }
        });


        iv_bottom_up_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_bottom_up.getVisibility() == View.GONE) {
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_up);
                    iv_bottom_up_arrow.startAnimation(bottomUp);
                    ll_bottom_up.startAnimation(bottomUp);
                    ll_bottom_up.setVisibility(View.VISIBLE);
                    iv_bottom_up_arrow.setImageResource(R.drawable.map_arrow_down);
                } else {
                    Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_down);
                    ll_bottom_up.startAnimation(bottomUpDown);
                    iv_bottom_up_arrow.startAnimation(bottomUpDown);
                    ll_bottom_up.setVisibility(View.GONE);
                    iv_bottom_up_arrow.setImageResource(R.drawable.map_arrow_up);

                }


            }
        });

        iv_left_arrow_hsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int positionView = ((LinearLayoutManager) recycleview_cat_list.getLayoutManager()).findFirstVisibleItemPosition();
                Log.d("recycleviewposleft", String.valueOf(positionView));
                recycleview_cat_list.getLayoutManager().scrollToPosition(positionView - 1);

            }
        });
        iv_right_arrow_hsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int positionView = ((LinearLayoutManager) recycleview_cat_list.getLayoutManager()).findLastVisibleItemPosition();
                recycleview_cat_list.getLayoutManager().scrollToPosition(positionView + 1);
                Log.d("recycleviewposright", String.valueOf(positionView));

            }
        });


        //offline

        pushNotificationList = new ArrayList<>();


        ttobj = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

                if (status == TextToSpeech.SUCCESS) {
                    int result = ttobj.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA ||
                            result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("error", "This Language is not supported");
                        iv_voice.setVisibility(View.GONE);
                    }

                } else {
                    iv_voice.setVisibility(View.GONE);
                }

            }
        });

        tv_explore_from_map.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (createTripList.size() > 0) {
                    Constants.frommapplan = "explorefrommap";

                    Bundle b = new Bundle();
                    b.putInt("tripId", tripid);
                    b.putString("from", "routeplan");
                    b.putString("destID", createTripList.get(0).getDestinationId().toString());
                    b.putString("destLat", createTripList.get(0).getDestination_latitude().toString());
                    b.putString("destLong", createTripList.get(0).getDestination_longitude().toString());
                    b.putString("placeName", createTripList.get(0).getDestination().toString());
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new ExplorePlacesFragment(), b);
                }
            }
        });

        ll_close_pop_up_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                fl_pop_up_zoo.startAnimation(bottomUpDown);
                fl_pop_up_zoo.setVisibility(View.GONE);
            }
        });


        iv_tollbooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog myTollBoothDialog = new Dialog(getActivity());

                myTollBoothDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myTollBoothDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myTollBoothDialog.setContentView(R.layout.pop_up_toolbooth);

                final ListView tollboothlistview = (ListView) myTollBoothDialog.findViewById(R.id.tollboothlist);

                TextView tv_no_tollbooth = (TextView) myTollBoothDialog.findViewById(R.id.tv_no_tollbooth);

                toolBooth_list_adapter = new TollBoothAdapter(getActivity(), tollBoothArrayList);

                tollboothlistview.setAdapter(toolBooth_list_adapter);
                final ImageView imgClosepopup = (ImageView) myTollBoothDialog.findViewById(R.id.ImgClosepopup);

                if (tollBoothArrayList.size() == 0) {
                    tollboothlistview.setVisibility(View.GONE);
                    tv_no_tollbooth.setVisibility(View.VISIBLE);
                }
                myTollBoothDialog.show();

                imgClosepopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myTollBoothDialog.dismiss();
                    }
                });


            }
        });

        ll_S_to_D_on_trip_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                S_To_D_Plan = 0;
                ll_no_plan.setVisibility(View.GONE);

                tv_S_to_D.setTextColor(getResources().getColor(R.color.textYellow));
                tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
                tv_plan_at_D.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tv_D_to_S.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tv_D_to_S_from_left_drawer.setVisibility(View.GONE);


                ll_route_plan.setVisibility(View.VISIBLE);
                iv_tollbooth.setVisibility(View.VISIBLE);
                ll_des_plan.setVisibility(View.GONE);
                if (destinationplanList.size() > 0) {
                    tv_explore_from_map.setVisibility(View.GONE);
                } else {
                    tv_explore_from_map.setVisibility(View.GONE);
                }

                selected_poi_pos = -1;
                selected_ro_pos = -1;
                selected_ser_pos = -1;
                selected_des_pos = -1;
                for (int i = 0; i < listServiceCategoryList.size(); i++) {
                    listServiceCategoryList.get(i).setStatus(false);
                }
                if (listServiceCategoryList.size() > 0)
                    listServiceCategoryList.get(0).setStatus(true);

                serviceListAdapter.notifyDataSetChanged();


                if (return_plan == 0) {

                    get_my_trip();
                    tv_tripCompleted.setVisibility(View.VISIBLE);
                }
                else {
                    get_my_trip_display(0);
                    tv_tripCompleted.setVisibility(View.GONE);

                }


                tv_place_of_des.setVisibility(View.VISIBLE);
                ll_bottom_up.setVisibility(View.VISIBLE);
                iv_bottom_up_arrow.setVisibility(View.VISIBLE);
                iv_edit_stay_time.setVisibility(View.GONE);



            }
        });
        ll_plan_at_D_on_trip_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // planAtDest();
                tv_tripCompleted.setVisibility(View.GONE);

                if (destinationplanList.size() > 0) {
                    tv_explore_from_map.setVisibility(View.GONE);
                } else {
                    tv_explore_from_map.setVisibility(View.GONE);
                }
                llserviceslayout.setVisibility(View.GONE);

                if (destinationplanList.size() > 0) {
                    ll_no_plan.setVisibility(View.GONE);
                    edit_stay_time = false;
                    iv_edit_stay_time.setImageResource(R.drawable.edit_icon_unselected_my_plan);

                    editIconUnselected = edit_stay_time;
                    tv_D_to_S_from_left_drawer.setVisibility(View.GONE);

                    ll_route_plan.setVisibility(View.GONE);
                    iv_tollbooth.setVisibility(View.GONE);
                    ll_des_plan.setVisibility(View.VISIBLE);

                    tv_S_to_D.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_plan_at_D.setTextColor(getResources().getColor(R.color.textYellow));
                    tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
                    tv_D_to_S.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));


                    tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
                    selected_poi_pos = -1;
                    selected_ro_pos = -1;
                    selected_ser_pos = -1;
                    selected_des_pos = -1;
                    for (int i = 0; i < listServiceCategoryList.size(); i++) {
                        listServiceCategoryList.get(i).setStatus(false);
                    }
                    if (listServiceCategoryList.size() > 0)
                        listServiceCategoryList.get(0).setStatus(true);
                    serviceListAdapter.notifyDataSetChanged();


                    set_destination_layout();


                    // getDestinationPOIAPI(createTripList.get(0).getDestinationId(), createTripList.get(0).getDestination_latitude(), createTripList.get(0).getDestination_longitude());
                    ll_bottom_up.setVisibility(View.GONE);
                    iv_bottom_up_arrow.setVisibility(View.GONE);
                    iv_edit_stay_time.setVisibility(View.GONE);
                }else
                {
                    ll_des_plan.setVisibility(View.GONE);
                    tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
                    ll_route_plan.setVisibility(View.GONE);
                    iv_tollbooth.setVisibility(View.GONE);
                    ll_des_plan.setVisibility(View.VISIBLE);
                    tv_S_to_D.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_plan_at_D.setTextColor(getResources().getColor(R.color.textYellow));
                    tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
                    tv_D_to_S.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
                    ll_bottom_up.setVisibility(View.GONE);
                    iv_bottom_up_arrow.setVisibility(View.GONE);
                    iv_edit_stay_time.setVisibility(View.GONE);
                    ll_no_plan.setVisibility(View.VISIBLE);
                    ll_no_plan.removeAllViews();
                    LayoutInflater inflater1 = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View v1;
                    v1 = inflater1.inflate(R.layout.first_ride_trip, null);
                    TextView tv_discoverbtn=(TextView)v1.findViewById(R.id.tv_discoverbtn);
                    TextView tv_text=(TextView)v1.findViewById(R.id.tv_text);
                    ((TextView) v1.findViewById(R.id.tv_title)).setVisibility(View.GONE);
                    tv_text.setText("No plan has been made");
                    tv_discoverbtn.setVisibility(View.GONE);
                    ll_no_plan.addView(v1);
                }
            }
        });

        ll_D_to_S_on_trip_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                createTripList = dbHandler.get_TRIP_By_TripId(tripid);
                llserviceslayout.setVisibility(View.VISIBLE);
                if (createTripList.get(0).getReturnstatus()) {
                    ll_no_plan.setVisibility(View.GONE);

                    S_To_D_Plan = 1;
                    edit_stay_time = false;
                    iv_edit_stay_time.setImageResource(R.drawable.edit_icon_unselected_my_plan);
                    tv_explore_from_map.setVisibility(View.GONE);


                    tv_S_to_D.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_plan_at_D.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_D_to_S.setTextColor(getResources().getColor(R.color.textYellow));
                    tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));


                    ll_route_plan.setVisibility(View.VISIBLE);
                    iv_tollbooth.setVisibility(View.VISIBLE);
                    ll_des_plan.setVisibility(View.GONE);


                    tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
                    tv_place_of_des.setVisibility(View.GONE);
                    selected_poi_pos = -1;
                    selected_ro_pos = -1;
                    selected_ser_pos = -1;
                    selected_des_pos = -1;

                    for (int i = 0; i < listServiceCategoryList.size(); i++) {
                        listServiceCategoryList.get(i).setStatus(false);
                    }

                    if (listServiceCategoryList.size() > 0)
                        listServiceCategoryList.get(0).setStatus(true);
                    serviceListAdapter.notifyDataSetChanged();

                    ll_bottom_up.setVisibility(View.VISIBLE);
                    iv_bottom_up_arrow.setVisibility(View.VISIBLE);
                    iv_edit_stay_time.setVisibility(View.GONE);

                    //  getRouteOfPOI(destinationId, orginId);

                    if (return_plan == 1) {
                        get_my_trip();
                        tv_tripCompleted.setVisibility(View.VISIBLE);
                    }

                    else {
                        get_my_trip_display(1);
                        tv_tripCompleted.setVisibility(View.GONE);

                    }

                }else
                {
                    ll_route_plan.setVisibility(View.GONE);


                    tv_explore_from_map.setVisibility(View.GONE);
                    tv_S_to_D.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_plan_at_D.setTextColor(getResources().getColor(R.color.colorWhite));
                    tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    tv_D_to_S.setTextColor(getResources().getColor(R.color.textYellow));
                    tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
                    iv_tollbooth.setVisibility(View.GONE);
                    ll_des_plan.setVisibility(View.GONE);
                    tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
                    tv_place_of_des.setVisibility(View.GONE);
                    ll_no_plan.removeAllViews();
                    ll_no_plan.setVisibility(View.VISIBLE);
                    LayoutInflater inflater1 = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View v1;
                    v1 = inflater1.inflate(R.layout.first_ride_trip, null);
                    TextView tv_discoverbtn=(TextView)v1.findViewById(R.id.tv_discoverbtn);
                    TextView tv_text=(TextView)v1.findViewById(R.id.tv_text);
                    ((TextView) v1.findViewById(R.id.tv_title)).setVisibility(View.GONE);
                    tv_text.setText("No plan has been made");
                    tv_discoverbtn.setVisibility(View.GONE);
                    ll_no_plan.addView(v1);

                }
            }
        });

        onAdminRouteRoLists = new ArrayList<>();
        onAdminRoutePoiLists = new ArrayList<>();
        services = new ArrayList<>();
        routepojo = new ArrayList<>();

        desplan = false;


        serviceListAdapter = new Service_List_Adapter();
        horizontalLayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycleview_cat_list.setHasFixedSize(true);
        recycleview_cat_list.setLayoutManager(horizontalLayoutManagaer);

        recycleview_cat_list.setAdapter(serviceListAdapter);


        placeList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, placeList);
        tv_place_of_des.setThreshold(1);
        tv_place_of_des.setAdapter(adapter);
        tv_place_of_des.setEnabled(false);

        // call to emergency call number
        iv_map_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:" + default_EmergencyNumber));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                } else {     //have got permission
                    try {
                        startActivity(callIntent);  //call activity and make phone call
                    } catch (android.content.ActivityNotFoundException ex) {
                        //  Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });


        gpsTracker = new GPSTracker(getActivity());

        //  getListServices_Category();


        tv_services_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer pos = Integer.parseInt(tv_add_poi.getTag().toString().substring(1));
                Bundle b = new Bundle();
                if (tv_add_poi.getTag().toString().startsWith("r")) {


                    getRo(onAdminRouteRoLists.get(pos).getROLatitude(), onAdminRouteRoLists.get(pos).getROLongitude(), onAdminRouteRoLists.get(pos).getROMasterId());
                } else if (tv_add_poi.getTag().toString().startsWith("s")) {

                    b.putString("facilitytype", "s");
                    String json = gson.toJson(services.get(pos));
                    b.putString("servicelist", json);
                    // b.putString("position",position);
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new FacilitiesFragment(), b);
                } else if (tv_add_poi.getTag().toString().startsWith("d")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("position", String.valueOf(pos));
                    bundle.putString("placeName", explorePoiList.get(pos).getPOIName());
                    bundle.putInt("tripId", tripid);
                    bundle.putString("destId", createTripList.get(0).getDestinationId().toString());
                    bundle.putString("DestName", ((ExploreActivity) getActivity()).tv_text_header.getText().toString());
                    gson = new Gson();
                    String json = gson.toJson(explorePoiList);
                    editor.putString("POIList", json);
                    editor.commit();

                    ExploreActivity.destattractionimages.clear();
                    ((ExploreActivity) getActivity()).setpagerdata(false);
                    for (int i = 0; i < explorePoiList.get(0).getImageList().size(); i++) {
                        if (explorePoiList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                            ExploreActivity.destattractionimages.add(explorePoiList.get(0).getImageList().get(i).getImgName());
                        }
                    }

                    //  bundle=getArguments();

                    ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();


                    // bundle.putAll(getArguments());
                    exploreAttractionFragment.setArguments(bundle);
                    changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                    changeTransaction.addToBackStack(null);
                    changeTransaction.commit();

                }


            }
        });
        iv_map_center.setImageResource(R.drawable.navigation);
        iv_map_center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (map != null)
                    {
                        if (zoomlevel == 0)
                        {
                            zoomlevel = 1;
                            iv_map_center.setImageResource(R.drawable.navigation);


                        }
                        else {
                            zoomlevel = 0;
                            iv_map_center.setImageResource(R.drawable.location);
                            map.getTrackingSettings().setDismissAllTrackingOnGesture(true);
                        }


                        set_map_position();
                    }
                }

                catch (Exception e) {
                }
            }
        });

        iv_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (voice_flag) {
                    iv_voice.setImageResource(R.drawable.sound_icon_map_off);
                    voice_flag = false;
                } else {
                    iv_voice.setImageResource(R.drawable.sound_icon_map);
                    voice_flag = true;
                }
            }
        });


        createTripList = new ArrayList<>();

        createTripList = dbHandler.get_TRIP_By_TripId(tripid);


        if (createTripList.size() > 0) {
            orginId = createTripList.get(0).getSourceId();
            destinationId = createTripList.get(0).getDestinationId();
            orginlat = createTripList.get(0).getSource_latitude();
            originlong = createTripList.get(0).getSource_longitude();
            deslat = createTripList.get(0).getDestination_latitude();
            deslong = createTripList.get(0).getDestination_longitude();
            tv_S_to_D.setText(createTripList.get(0).getSource() + "-" + createTripList.get(0).getDestination());
            tv_plan_at_D.setText("Itinerary at " + createTripList.get(0).getDestination());
            tv_D_to_S.setText(createTripList.get(0).getDestination() + "-" + createTripList.get(0).getSource());
            total_avg_par_km = (createTripList.get(0).getKM().intValue() / 1000) / avg_par_km;


        } else {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
        }


        if (createTripList.size() > 0)
        {
            if (createTripList.get(0).getTripIsOneSideCompleted())
            {
                if(createTripList.get(0).getReturnstatus() && createTripList.get(0).getReturn_start_date().length()>1) {
                    return_plan = 1;

                    ((ExploreActivity) getActivity()).tv_origin_city_header.setText(tv_origin.getText().toString());
                    ((ExploreActivity) getActivity()).tv_dest_city_header.setText(tv_destination.getText().toString());
                    ((ExploreActivity) getActivity()).tv_origin_date_header.setText(createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime());
                    ((ExploreActivity) getActivity()).tv_dest_date_header.setText(createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime());

                    ll_D_to_S_on_trip_tracking.performClick();
                }
                else
                {
                    preferences.edit().putBoolean("trip_start_tracking", false).commit();
                    preferences.edit().putInt("tripId", 0).commit();
                    preferences.edit().putString("DirectionJson", "").commit();
                    ((ExploreActivity) getActivity()).replace_fragmnet(new DiscoverFragment());

                }
            } else {
                return_plan = 0;

                ((ExploreActivity) getActivity()).tv_origin_city_header.setText(tv_origin.getText().toString());
                ((ExploreActivity) getActivity()).tv_dest_city_header.setText(tv_destination.getText().toString());
                ((ExploreActivity) getActivity()).tv_origin_date_header.setText(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time());
                ((ExploreActivity) getActivity()).tv_dest_date_header.setText(createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time());

                ll_S_to_D_on_trip_tracking.performClick();
            }
        }

        if (Constants.listDefaultValuesArrayList.size() > 0)
        {
            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

            {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_EmergencyNumber"))
                    default_EmergencyNumber = Constants.listDefaultValuesArrayList.get(i).getValue().toString();

            }
        } else {
            getListDefaultvalues();
        }
        if (Build.VERSION.SDK_INT >= 23) {
            check_permission_marshmallow();
        } else {
            ViewMap();
        }
        locationServices.addLocationListener(new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    // Move the map camera to where the user location is and then remove the
                    // listener so the camera isn't constantly updating when the user location
                    // changes. When the user disables and then enables the location again, this
                    // listener is registered again and will adjust the camera once again.
                    if(zoomlevel==0)
                  setMapZoom();
                }
                else
                {
                    try {
                        Constants.showSnackMsg(getActivity(), "Searching GPS location.", iv_bottom_up_arrow);
                    }catch (Exception e){}

                }
            }
        });

        tv_tripCompleted.setVisibility(View.VISIBLE);

        tv_tripCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myTripDialog1 = new Dialog(getActivity());

                myTripDialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myTripDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myTripDialog1.setContentView(R.layout.pop_up_start_tracking);
                myTripDialog1.setCancelable(false);

                TextView tv_complete_msg = (TextView) myTripDialog1.findViewById(R.id.tv_trip_start_text);

                tv_complete_msg.setText("Are you sure you want to exit navigation?");

                TextView tv_yes = (TextView) myTripDialog1.findViewById(R.id.tv_start_tracking);
                tv_yes.setText("Yes");
                //tv_yes.setText("Yes");
                tv_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        myTripDialog1.dismiss();
                        NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                        nMgr.cancelAll();
                        if (Constants.isInternetAvailable(getActivity())) {
                            update_trip_one_way_completed(createTripList.get(0).getTripId(), true);
                        } else {
                            showStayDialog();
                        }
                        complatepopup = true;




                        //android.os.Process.killProcess(android.os.Process.myPid());
                    }
                });

                ImageView ImgClosepopup = (ImageView) myTripDialog1.findViewById(R.id.ImgClosepopup);

                ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        myTripDialog1.dismiss();
                    }
                });

                myTripDialog1.show();



            }
        });

    }

    private void check_permission_marshmallow() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Access Content");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                });
                return;

            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);

            return;
        }
        ViewMap();
        //else open ur
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private void PlacesBetweenStoD(final int pos) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v;

        v = inflater.inflate(R.layout.places_between_src_to_dest, null);

        ImageView iv_dotted_line = (ImageView) v.findViewById(R.id.iv_dotted_line);
        iv_dotted_line_bottom = (ImageView) v.findViewById(R.id.iv_dotted_line_bottom);
        TextView tv_poi_name = (TextView) v.findViewById(R.id.tv_poi_name);
        //    TextView tv_poi_hrs=(TextView)v.findViewById(R.id.tv_poi_hrs);
        final TextView et_stay_time = (TextView) v.findViewById(R.id.et_stay_time);
        TextView tv_departure_time = (TextView) v.findViewById(R.id.tv_departure_time);
        TextView tv_arrival_time = (TextView) v.findViewById(R.id.tv_arrival_time);
        ImageView iv_poi_delete = (ImageView) v.findViewById(R.id.iv_poi_delete);
        TextView tv_poi_distance_time = (TextView) v.findViewById(R.id.tv_poi_distance_time);
        CircleImageView fc_poi_image = (CircleImageView) v.findViewById(R.id.fc_poi_image);
        ImageView iv_poi_night_stay = (ImageView) v.findViewById(R.id.iv_poi_night_stay);
        ImageView iv_poi_map = (ImageView) v.findViewById(R.id.iv_poi_map);
        final ImageView iv_poi_up_arrow = (ImageView) v.findViewById(R.id.iv_poi_up_arrow);

        LinearLayout ll_stay_time_layout = (LinearLayout) v.findViewById(R.id.ll_stay_time_layout);
        LinearLayout ll_edit_stay_time = (LinearLayout) v.findViewById(R.id.ll_edit_stay_time);

        final LinearLayout ll_places_between_N_to_D = (LinearLayout) v.findViewById(R.id.ll_places_between_N_to_D);

        tv_poi_name.setText(createPoiList.get(pos).getPoiName());
        et_stay_time.setText(createPoiList.get(pos).getStayTime().toString());


        et_stay_time.setEnabled(false);
        ll_edit_stay_time.setBackground(null);
        iv_poi_delete.setVisibility(View.GONE);


        Bitmap bitmap;
        try {

            bitmap = new ImageSaver(getActivity()).
                    setFileName(createPoiList.get(pos).getImagename()).

                    setExternal(true).
                    load();
            fc_poi_image.setImageBitmap(bitmap);
        } catch (Exception e) {

        }
        double roundOff1 = Math.round(createPoiList.get(0).getKM() / 1000 * 100.0) / 100.0;
        tv_trip_distance_time.setText(roundOff1 + " Kms / " + Constants.convert_minute_hrs_both(createPoiList.get(0).getTime() + createPoiList.get(0).getPoi_diff_seconds()));


        if (createPoiList.get(pos).getPoitype().contains("DES")) {


            if (createPoiList.get(pos).getPoiStay() == true) {
                iv_poi_night_stay.setImageResource(R.drawable.night_halt_icon_map_slider);
                iv_poi_up_arrow.setVisibility(View.GONE);
                iv_poi_map.setVisibility(View.VISIBLE);
                iv_poi_night_stay.setVisibility(View.VISIBLE);
                iv_poi_delete.setVisibility(View.VISIBLE);
                ll_stay_time_layout.setVisibility(View.GONE);


            } else {
                iv_poi_night_stay.setImageResource(R.drawable.night_halt_icon_map_slider_1);
                iv_poi_up_arrow.setVisibility(View.GONE);
                iv_poi_map.setVisibility(View.GONE);
                iv_poi_night_stay.setVisibility(View.VISIBLE);
                iv_poi_delete.setVisibility(View.VISIBLE);
                ll_stay_time_layout.setVisibility(View.VISIBLE);

            }
            if (createPoiList.get(pos).getAttractionCount() > 0 && createPoiList.get(pos).getPoiStay()) {
                iv_poi_map.setVisibility(View.VISIBLE);
            } else {
                iv_poi_map.setVisibility(View.GONE);
            }


            create_poi_nightsList = new ArrayList<>();
            create_poi_nightsList.clear();

            create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(createPoiList.get(pos).getTripId(), createPoiList.get(pos).getPoiDesID(), return_plan);

            Log.d("sizeofnight", String.valueOf(create_poi_nightsList.size()));


            if (create_poi_nightsList.size() <= 0) {
                iv_poi_up_arrow.setVisibility(View.GONE);

                if (edit_stay_time) {
                    iv_poi_delete.setVisibility(View.VISIBLE);
                } else {
                    iv_poi_delete.setVisibility(View.GONE);
                }


            } else {

                iv_poi_up_arrow.setVisibility(View.VISIBLE);
                //tv_poi_distance_time.setVisibility(View.GONE);

                iv_poi_delete.setVisibility(View.GONE);


                for (int j = 0; j < create_poi_nightsList.size(); j++) {
                    LayoutInflater inflater1 = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v1;

                    v1 = inflater1.inflate(R.layout.places_between_night_to_dest, null);
                    TextView tv_poi_name_n = (TextView) v1.findViewById(R.id.tv_poi_name);
                    //    TextView tv_poi_hrs=(TextView)v.findViewById(R.id.tv_poi_hrs);
                    final EditText et_stay_time_n = (EditText) v1.findViewById(R.id.et_stay_time);


                    final ImageView iv_poi_delete_n = (ImageView) v1.findViewById(R.id.iv_poi_delete);
                    TextView tv_poi_distance_time_n = (TextView) v1.findViewById(R.id.tv_poi_distance_time);
                    CircleImageView fc_poi_image_n = (CircleImageView) v1.findViewById(R.id.fc_poi_image);
                    final View view_bottom_line = v1.findViewById(R.id.view_line);
                    LinearLayout ll_edit_night_stay = (LinearLayout) v1.findViewById(R.id.ll_edit_night_stay);

                    tv_poi_name_n.setText(create_poi_nightsList.get(j).getPoiName());
                    et_stay_time_n.setText(create_poi_nightsList.get(j).getStayTime().toString());
                    if (edit_stay_time) {
                        et_stay_time_n.setEnabled(true);
                        ll_edit_night_stay.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
                        iv_poi_delete_n.setVisibility(View.VISIBLE);
                    } else {
                        et_stay_time_n.setEnabled(false);
                        ll_edit_night_stay.setBackground(null);
                        iv_poi_delete_n.setVisibility(View.GONE);
                    }

                    double roundOff = Math.round((create_poi_nightsList.get(j).getKM() / 1000) * 100.0) / 100.0;

                    if (j == 0)
                        tv_poi_distance_time_n.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(create_poi_nightsList.get(j).getTime()));
                    else {

                        int previouspos = j - 1;
                        Double distance_between = distance(create_poi_nightsList.get(j).getPoiLatitude(), create_poi_nightsList.get(j).getPoiLongitude(), create_poi_nightsList.get(previouspos).getPoiLatitude(), create_poi_nightsList.get(previouspos).getPoiLongitude());
                        double roundOff2 = Math.round(distance_between * 100.0) / 100.0;


                        //Double time = get_time_based_on_KM(distance_between);
                        Double time = roundOff2 * 60;
                        time = Math.round(time * 100.0) / 100.0;
                        Log.d("distime1", String.valueOf(distance_between) + "time" + String.valueOf(time));

                        tv_poi_distance_time_n.setText(roundOff2 + " Kms / " + Constants.convert_minute_hrs_both(Double.valueOf(time)));
                    }


                    /*if (create_poi_nightsList.get(j).getPoiImage() != null) {
                        byte[] bytes = create_poi_nightsList.get(j).getPoiImage();
                        fc_poi_image_n.setImageBitmap(Constants.getImage(bytes));


                    }*/

                    bitmap = new ImageSaver(getActivity()).
                            setFileName(create_poi_nightsList.get(j).getImagename()).

                            setExternal(true).
                            load();
                    if (bitmap != null)
                        fc_poi_image_n.setImageBitmap(bitmap);

                    iv_poi_delete_n.setTag(j);
                    et_stay_time_n.setTag(j);


                    if (j == create_poi_nightsList.size() - 1) {
                        view_bottom_line.setVisibility(View.GONE);
                    }


                    iv_poi_up_arrow.setTag(1);
                    iv_poi_up_arrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if ((int) iv_poi_up_arrow.getTag() == 1) {
                                ll_places_between_N_to_D.setVisibility(View.GONE);
                                iv_poi_up_arrow.setImageResource(R.drawable.arrow_down_map_slider);
                                iv_poi_up_arrow.setTag(0);
                            } else {

                                ll_places_between_N_to_D.setVisibility(View.VISIBLE);
                                iv_poi_up_arrow.setImageResource(R.drawable.arrow_up_map_slider);
                                iv_poi_up_arrow.setTag(1);
                            }

                        }
                    });

                    ll_places_between_N_to_D.addView(v1);


                }
            }


        } else {
            iv_poi_night_stay.setVisibility(View.GONE);
            iv_poi_delete.setVisibility(View.VISIBLE);
            ll_stay_time_layout.setVisibility(View.VISIBLE);
        }
        if (edit_stay_time) {
            iv_poi_delete.setVisibility(View.VISIBLE);
        } else {
            iv_poi_delete.setVisibility(View.GONE);
        }


        if (pos == 0) {

            String tripStartDate = "";
            if (return_plan == 0)
                tripStartDate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime();
            else
                tripStartDate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time();

            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(createPoiList.get(0).getTime() + createPoiList.get(0).getPoi_diff_seconds());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);

                tv_arrival_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));


                createPoiList.get(pos).setActual_arrival_date_time(sdf_second.format(calendar.getTime()));


                if (createPoiList.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((createPoiList.get(pos).getStayTime() - Math.floor(createPoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());
                    calendar.add(Calendar.HOUR, createPoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tv_departure_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
                    createPoiList.get(pos).setActual_dep_date_time(sdf_second.format(calendar.getTime()));


                } else {
                    date = sdf.parse(createPoiList.get(pos).getResumeDate() + " " + createPoiList.get(pos).getResumeTime());
                    calendar.setTime(date);
                    tv_departure_time.setText(createPoiList.get(pos).getResumeDate() + "\n" + convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList.get(pos).setActual_dep_date_time(sdf_second.format(calendar.getTime()));


                }

            } catch (ParseException e) {
                e.printStackTrace();

            }


        }
        //Log.d("arrivalTime","ar"+createPoiList.get(pos).getArrivalTime()+"de"+createPoiList.get(pos).getDepartureTime()+"re"+createPoiList.get(pos).getReqtime());

        if (pos >= 1 && (pos <= createPoiList.size() - 1)) {

            int position = pos - 1;

            String tripStartDate = createPoiList.get(position).getActual_arrival_date_time();

            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                Double requriedtime = createPoiList.get(position).getReqtime();

                int[] convert_h_m_s = splitToComponentTimes(requriedtime);
                calendar.add(Calendar.HOUR, convert_h_m_s[0]);
                calendar.add(Calendar.MINUTE, convert_h_m_s[1]);
                calendar.add(Calendar.SECOND, convert_h_m_s[2]);

                tv_arrival_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));

                createPoiList.get(pos).setActual_arrival_date_time(sdf_second.format(calendar.getTime()));

                if (createPoiList.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((createPoiList.get(pos).getStayTime() - Math.floor(createPoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, createPoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tv_departure_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
                    createPoiList.get(pos).setActual_dep_date_time(sdf_second.format(calendar.getTime()));

                } else {
                    date = sdf.parse(createPoiList.get(pos).getResumeDate() + " " + createPoiList.get(pos).getResumeTime());

                    calendar.setTime(date);


                    tv_departure_time.setText(createPoiList.get(pos).getResumeDate() + "\n" + convert_time_12_format(sdfTime.format(calendar.getTime())));


                    createPoiList.get(pos).setActual_dep_date_time(sdf_second.format(calendar.getTime()));


                }

            } catch (ParseException e) {
                e.printStackTrace();

            }

        }


        if (createPoiList.size() == pos + 1) {


            if (return_plan == 0) {


                double roundOff = Math.round((createTripList.get(0).getKM()) / 1000 * 100.0) / 100.0;


                tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getTime() + createTripList.get(0).getEta_diff_second()));

                createPoiList.get(pos).setReqtime(createTripList.get(0).getTime() + createTripList.get(0).getEta_diff_second());
            } else {
                double roundOff = Math.round((createTripList.get(0).getReturnKM()) / 1000 * 100.0) / 100.0;


                tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getReturnTime() + createTripList.get(0).getEta_return_diff_second()));

                createPoiList.get(pos).setReqtime(createTripList.get(0).getReturnTime() + createTripList.get(0).getEta_return_diff_second());
            }


            add_hours_minute_last(createPoiList.get(pos).getActual_dep_date_time(), createPoiList.get(pos).getReqtime());

        } else {
            double roundOff = Math.round(createPoiList.get(pos + 1).getKM() / 1000 * 100.0) / 100.0;

            tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createPoiList.get(pos + 1).getTime() + createPoiList.get(pos + 1).getPoi_diff_seconds()));

            createPoiList.get(pos).setReqtime(createPoiList.get(pos + 1).getTime() + createPoiList.get(pos + 1).getPoi_diff_seconds());


        }


        iv_poi_night_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!createTripList.get(0).getTripstatus().contains("Saved") && createPoiList.get(pos).getPoiDate() != null) {

//                Log.d("poidate", createPoiList.get(pos).getPoiDate() );
                    if (edit_stay_time) {

                        final SessionManager sessionManager1 = new SessionManager(getActivity());
                        final Dialog myTripResumeDialog = new Dialog(getActivity());
                        myTripResumeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        myTripResumeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        myTripResumeDialog.setContentView(R.layout.resume_trip_from_place_popup);
                        tv_resume_date_my_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_date);
                        tv_resume_time_my_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_time);
                        final ImageView imgClosepopup = (ImageView) myTripResumeDialog.findViewById(R.id.ImgClosepopup);
                        TextView tv_resume_destination = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_destination);

                        TextView tv_save_resume_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_save_resume_trip);


                        Calendar now = Calendar.getInstance();


                        dpd = DatePickerDialog.newInstance(
                                TrackRoutePlanningFragment.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );

                        tv_resume_destination.setText("Resume Trip From " + createPoiList.get(pos).getPoiName());

                        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
                        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");

                        tv_resume_date_my_trip.setText(createTripList.get(0).getStartdate());


                        try {
                            tv_resume_time_my_trip.setText(Constants.formatDate(String.valueOf(createPoiList.get(pos).getDepartureTime()).replace(".", ":"), "HH:mm", "hh:mm a"));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        Date date = null;
                        try {
                            date = sdf1.parse(createPoiList.get(pos).getPoiDate());
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);

                            dpd.setMinDate(calendar);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        tpd = TimePickerDialog.newInstance(
                                TrackRoutePlanningFragment.this,
                                now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE), true
                        );

                        imgClosepopup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                myTripResumeDialog.dismiss();
                            }
                        });

                        tv_resume_time_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                select_dpd_type = "resume";
                                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                            }
                        });

                        tv_resume_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                select_dpd_type = "resume";
                                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

                            }
                        });
                        tv_save_resume_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                String resume_time = "";

                                try {
                                    resume_time = Constants.formatDate(tv_resume_time_my_trip.getText().toString(), "hh:mm a", "HH:mm");


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String arrival_date = createPoiList.get(0).getPoiDate() + " " + createPoiList.get(0).getArrivalTime().toString().replace(".", ":");
                                Log.d("resumetimedparrival", arrival_date);
                                if (CheckDates(arrival_date, tv_resume_date_my_trip.getText().toString() + " " + resume_time)) {

                                    dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time);

                                    Log.d("resumetimedp", String.valueOf(dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time)));
                                    myTripResumeDialog.dismiss();
                                    get_my_trip();
                                } else {
                                    Constants.show_error_popup(getActivity(), "Please change trip resume date.", iv_bottom_up_arrow);
                                }


                            }
                        });

                        myTripResumeDialog.show();


                    }
                } else {
                    Constants.show_error_popup(getActivity(), "Please enter trip start date.", iv_bottom_up_arrow);
                }

            }
        });


        iv_poi_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.hideSoftKeyBoardOnTabClicked(getActivity(), v);
                drawer_layout_services.closeDrawer(left_navView);
                ll_bottom_up.setVisibility(View.GONE);
                iv_bottom_up_arrow.setVisibility(View.GONE);
                night_poi_id = createPoiList.get(pos).getPoiDesID();
                getPOIAPI(createPoiList.get(pos).getPoiDesID(), createPoiList.get(pos).getPoiLatitude(), createPoiList.get(pos).getPoiLongitude());
            }
        });

        update_poi_plan(pos);

        ll_places_between_S_to_D.addView(v);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        exploreActivity = (ExploreActivity) activity;
    }

    @Override
    public void onDestroy() {
        if (wl != null) {
            Log.v(TAG, "Releasing wakelock");
            try {
                wl.release();
            } catch (Throwable th) {
                // ignoring this exception, probably wakeLock was already released
            }
        } else {
            // should never happen during normal workflow
            Log.e(TAG, "Wakelock reference is null");
        }
        try {
            if (handler != null)
                handler.removeCallbacks(runnable);


            ((ExploreActivity) getActivity()).ll_menu_header.setVisibility(View.VISIBLE);


        } catch (Exception e) {
        }


        Log.d("Syatem out", "onDestroyCalled________");

        //   mLocationProvider.disconnect();

        super.onDestroy();
        // ((ExploreActivity)getActivity()).setmenutonavigationdrawer();
        //  ((ExploreActivity)getActivity()).drawerLayout.openDrawer(((ExploreActivity)getActivity()).navigationView);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((ExploreActivity) getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_menu_header.setVisibility(View.INVISIBLE);

        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        try {
            if (map != null) {
                map.setMyLocationEnabled(true);
           //  setTrackingMode();
            }
        } catch (Exception e) {
        }

        if( preferences.getBoolean("trip_start_tracking", false)==false)
        {
            ((ExploreActivity) getActivity()).replace_fragmnet(new DiscoverFragment());
        }

        else {
            if (handler != null) {
                running_car_point1();
            }
        }


        NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        nMgr.cancelAll();

        //  mLocationProvider.connect();
        Log.d("retrun_plan","return_value "+return_plan);



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.GONE);
        //  ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));

        //  ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + " " + (++monthOfYear) + "," + year;


        try {
            date1 = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
            if (select_dpd_type.contains("resume")) {
                tv_resume_date_my_trip.setText(date1.toString());
            } else {
                tv_start_date_my_return_trip.setText(date1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String time = hourOfDay + ":" + minute + "";

        try {
            date1 = Constants.formatDate(time, "HH:mm", "hh:mm a");

            if (select_dpd_type.contains("resume")) {
                tv_resume_time_my_trip.setText(date1.toString());
            } else {

                tv_start_time_my_return_trip.setText(date1);
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void get_my_trip() {
        if (S_To_D_Plan == return_plan)
        {
            iv_edit_stay_time.setVisibility(View.GONE);
            createTripList = new ArrayList<>();
            createPoiList = new ArrayList<>();

            createTripList = dbHandler.get_TRIP_By_TripId(tripid);


            if (createTripList.size() > 0) {


                if (return_plan == 0) {
                    tv_origin.setText(createTripList.get(0).getSource());
                    tv_destination.setText(createTripList.get(0).getDestination());

                    tv_start_time.setText(convert_time_12_format(createTripList.get(0).getStarttime()));

                    tv_start_date.setText(createTripList.get(0).getStartdate());


                    DecimalFormat df = new DecimalFormat("#.##");
                    double distance = Double.valueOf(df.format(createTripList.get(0).getKM() / 1000));

                    Log.d("eta_diff_second", "eta_diff " + createTripList.get(0).getEta_diff_second() + " time" + createTripList.get(0).getTime());
                    tv_trip_distance_time.setText(distance + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getTime() + createTripList.get(0).getEta_diff_second()));


                    iv_circle_source.setImageBitmap(Constants.getImage(createTripList.get(0).getSource_image()));

                    iv_circle_destination.setImageBitmap(Constants.getImage(createTripList.get(0).getDestination_image()));

                    if(createTripList.get(0).getDes_address().length()>1)
                        tv_place_of_des.setText(createTripList.get(0).getDes_address());

                    add_hours_minute(createTripList.get(0).getStartdate(), createTripList.get(0).getStarttime(), createTripList.get(0).getTime() + createTripList.get(0).getEta_diff_second());


                } else {
                    tv_origin.setText(createTripList.get(0).getDestination());
                    tv_destination.setText(createTripList.get(0).getSource());

                    tv_start_time.setText(convert_time_12_format(createTripList.get(0).getReturn_start_time()));

                    tv_start_date.setText(createTripList.get(0).getReturn_start_date());


                    DecimalFormat df = new DecimalFormat("#.##");
                    double distance = Double.valueOf(df.format(createTripList.get(0).getReturnKM() / 1000));


                    tv_trip_distance_time.setText(distance + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getReturnTime() + createTripList.get(0).getEta_return_diff_second()));


                    iv_circle_source.setImageBitmap(Constants.getImage(createTripList.get(0).getDestination_image()));

                    iv_circle_destination.setImageBitmap(Constants.getImage(createTripList.get(0).getSource_image()));

                    add_hours_minute(createTripList.get(0).getReturn_start_date(), createTripList.get(0).getReturn_start_time(), createTripList.get(0).getReturnTime());
                }
                ll_places_between_S_to_D.removeAllViews();

                get_poi_list(return_plan);
            }
        } else {
            get_my_trip_display(S_To_D_Plan);
        }

    }

    public void get_poi_list(Integer returnstatus) {

        createPoiList = dbHandler.get_POI_TRIP(createTripList.get(0).getTripId(), returnstatus);


        for (int i = 0; i < createPoiList.size(); i++) {

            PlacesBetweenStoD(i);
        }

        if(preferences.getBoolean("trip_start_tracking",false)==true)
            update_trip();
    }

    public String convert_time_12_format(String time) {
        String timein12 = "";
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
        try {
            Date _24HourDt = _24HourSDF.parse(time);
            timein12 = _12HourSDF.format(_24HourDt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timein12;
    }

    public String add_hours_minute(String startdate, String starttime, Double time_seconds) {


        String tripStartDate = startdate + " " + starttime;


        //  Log.d("startPoi",currentDateandTime);
        Date date = null;
        try {
            date = sdf.parse(tripStartDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            int[] converttime = splitToComponentTimes(time_seconds);


            calendar.add(Calendar.HOUR, converttime[0]);
            calendar.add(Calendar.MINUTE, converttime[1]);
            tv_end_date.setText(sdfDate.format(calendar.getTime()));
            tv_end_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
            if (return_plan==0) {
                excepted_date = tv_end_date.getText().toString();
                excepted_time = Constants.formatDate(tv_end_time.getText().toString(), "hh:mm a", "HH:mm");
                dbHandler.update_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
            } else {
                return_excepted_date = tv_end_date.getText().toString();
                return_excepted_time = Constants.formatDate(tv_end_time.getText().toString(), "hh:mm a", "HH:mm");
                //  dbHandler.update_Return_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()), return_trip_plan);
                dbHandler.update_return_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
            }


        } catch (ParseException e) {
            e.printStackTrace();
            tv_end_date.setText("");
        }

        set_header_origin_destination();


        return "";
    }

    public String add_hours_minute_last(String startdate, Double requriedtime) {
        try {


            String tripStartDate = startdate;


            //  Log.d("startPoi",currentDateandTime);
            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                BigDecimal bd = new BigDecimal((requriedtime - Math.floor(requriedtime)) * 100);
                bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                int amin = bd.intValue();


                int[] convert_h_m_s = splitToComponentTimes(requriedtime);
                calendar.add(Calendar.HOUR, convert_h_m_s[0]);
                calendar.add(Calendar.MINUTE, convert_h_m_s[1]);
                calendar.add(Calendar.SECOND, convert_h_m_s[2]);

                tv_end_date.setText(sdfDate.format(calendar.getTime()));
                tv_end_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
                if (return_plan==0) {
                    excepted_date = tv_end_date.getText().toString();
                    excepted_time = Constants.formatDate(tv_end_time.getText().toString(), "hh:mm a", "HH:mm");
                    dbHandler.update_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
                } else {
                    return_excepted_date = tv_end_date.getText().toString();
                    return_excepted_time = Constants.formatDate(tv_end_time.getText().toString(), "hh:mm a", "HH:mm");
                    dbHandler.update_return_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
                }


            } catch (ParseException e) {
                e.printStackTrace();
                tv_end_date.setText("");
            }

            set_header_origin_destination();
        } catch (Exception e) {
        }

        return "";
    }

    public void set_header_origin_destination()
    {
        ((ExploreActivity) getActivity()).tv_origin_city_header.setText(tv_origin.getText().toString());
        ((ExploreActivity) getActivity()).tv_dest_city_header.setText(tv_destination.getText().toString());
        ((ExploreActivity) getActivity()).tv_origin_date_header.setText(tv_start_date.getText().toString() + " " + tv_start_time.getText().toString());
        ((ExploreActivity) getActivity()).tv_dest_date_header.setText(tv_end_date.getText().toString() + " " + tv_end_time.getText().toString());

        tv_explore_from_map.setText("Explore " + tv_destination.getText().toString());
        tv_D_to_S_from_left_drawer.setText("Plan " + tv_destination.getText().toString() + " -" + tv_origin.getText().toString() + " Route");
        tv_place_of_des.setHint("Place of Stay at " + tv_destination.getText().toString());


        if(createTripList.size()>0)
        {
            if(createTripList.get(0).getDes_address().length()>1)
                tv_place_of_des.setText(createTripList.get(0).getDes_address());
        }



    }

    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

    public void ViewMap() {
        if (gpsTracker.isGPSEnabled) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    map = mapboxMap;


                    map.setMyLocationEnabled(true);

                    locationServices.toggleGPS(true);


                    setTrackingMode();
                    map.getUiSettings().setCompassEnabled(false);

                    // map.getUiSettings().setRotateGesturesEnabled(false);
//                    map.getMyLocationViewSettings().setForegroundDrawable(getResources().getDrawable(R.drawable.car_map_transparant), null);
                    MyLocationViewSettings myLocationViewSettings = mapboxMap.getMyLocationViewSettings();
                    myLocationViewSettings.setAccuracyAlpha(0);
                     /* myLocationViewSettings.setForegroundTintColor(ContextCompat.getColor(getActivity(), R.color.transparent1));
                    myLocationViewSettings.setBackgroundTintColor(ContextCompat.getColor(getActivity(), R.color.transparent1));
                   */
                    map.getMyLocationViewSettings().setForegroundDrawable(getResources().getDrawable(R.drawable.car_map), getResources().getDrawable(R.drawable.car_map));

                  // map.getMyLocationViewSettings().setPadding(0, mapView.getHeight() / 4, 0, 0);
                    map.setMaxZoom(max_zoom);

                    if (Constants.isInternetAvailable(getActivity()))
                    {
                        getListServices_Category();
                        //  get_my_trip();

                        if (preferences.getString("DirectionJson", "").length() > 1)
                        {
                            getdata_form_offline();
                        }
                        else
                            getRouteOfPOI(orginId, destinationId);


                    } else
                    {
                        getdata_form_offline();
                    }


                    map.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(@NonNull com.mapbox.mapboxsdk.annotations.Marker marker) {

                            //    map.getInfoWindowAdapter().getInfoWindow(marker).setVisibility(View.GONE);

                            marker.hideInfoWindow();
                            tv_add_poi.setVisibility(View.GONE);
                            tv_services_details.setVisibility(View.GONE);
                            if (marker.getSnippet().contains("point")) {

                            } else {


                                if (marker.getSnippet().contains("POI"))
                                {
                                    String pos = marker.getSnippet().substring(4);
                                    //      Toast.makeText(getActivity(), marker.getSnippet()+pos, Toast.LENGTH_SHORT).show();//

                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);

                                    fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + createPoiList.get(Integer.valueOf(pos)).getImagename() + "&width=" + 150 + "&height=" + 150);
                                    fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                    tv_poi_map_name.setText(createPoiList.get(Integer.valueOf(pos)).getPoiName());
                                    tv_poi_map_des.setText(Html.fromHtml(createPoiList.get(Integer.valueOf(pos)).getPoidescription()));

                                    tv_poi_map_distance.setText((int) Math.ceil(createPoiList.get(Integer.valueOf(pos)).getKM() / 1000) + " Kms [" + Constants.convert_minute_hrs(createPoiList.get(Integer.valueOf(pos)).getTime().doubleValue()) + "]");


                                    llserviceslayout.setVisibility(View.GONE);
                                    tv_poi_map_des.setVisibility(View.VISIBLE);


                                    tv_add_poi.setTag("p" + pos);

                                    if (selected_poi_pos > -1)
                                        createPoiList.get(selected_poi_pos).setClick_status(false);

                                    selected_poi_pos = Integer.valueOf(pos);

                                    createPoiList.get(Integer.valueOf(pos)).setClick_status(true);

                                    //  add_marker_poi(return_plan);
                                    add_marker_plan(return_plan);

                                } else if (marker.getSnippet().contains("Services"))
                                {
                                    String pos = marker.getSnippet().substring(9);
                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);

                                    if (!new ImageSaver(getActivity()).check_file_exists(createPoiList.get(Integer.valueOf(pos)).getImagename())) {
                                        fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + createPoiList.get(Integer.valueOf(pos)).getImagename() + "&width=" + 150 + "&height=" + 150);
                                    }
                                    else {
                                        Bitmap bitmap = new ImageSaver(getActivity()).
                                                setFileName(createPoiList.get(Integer.valueOf(pos)).getImagename()).
                                                setExternal(true).
                                                load();
                                        fc_poi_map_image.setImageBitmap(bitmap);
                                    }

                                    //fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                    tv_poi_map_name.setText(createPoiList.get(Integer.valueOf(pos)).getPoiName());

                                    // tv_poi_map_des.setText(Html.fromHtml(routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIShortDescription()));

                                    tv_poi_map_distance.setText((int) Math.ceil(createPoiList.get(Integer.valueOf(pos)).getKM() / 1000) + " Kms [" + Constants.convert_minute_hrs(createPoiList.get(Integer.valueOf(pos)).getTime().doubleValue()) + "]");
                                    tv_services_location.setText(Html.fromHtml(createPoiList.get(Integer.valueOf(pos)).getPoidescription()));
                                    tv_services_side.setText(createPoiList.get(Integer.valueOf(pos)).getPoiside());

                                    llserviceslayout.setVisibility(View.VISIBLE);
                                    tv_poi_map_des.setVisibility(View.GONE);
                                    tv_add_poi.setTag("s" + pos);
                                    if (selected_poi_pos > -1)
                                        createPoiList.get(selected_poi_pos).setClick_status(false);

                                    selected_poi_pos = Integer.valueOf(pos);

                                    createPoiList.get(Integer.valueOf(pos)).setClick_status(true);

                                    // add_marker_services(createPoiList.get(selected_poi_pos).getPoiServerId());
                                    add_marker_plan(return_plan);

                                }


                                else if (marker.getSnippet().equals("Roplan")) {
                                    String pos = marker.getSnippet().substring(7);
                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);

                                    fc_poi_map_image.setImageResource(R.drawable.map_slider_icon4_selected);
                                    tv_poi_map_name.setText(createPoiList.get(Integer.valueOf(pos)).getPoiName());

                                    // tv_poi_map_des.setText(Html.fromHtml(routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIShortDescription()));

                                    tv_poi_map_distance.setText((int) Math.ceil(createPoiList.get(Integer.valueOf(pos)).getKM() / 1000) + " Kms [" + Constants.convert_minute_hrs(createPoiList.get(Integer.valueOf(pos)).getTime().doubleValue()) + "]");
                                    tv_services_location.setText(Html.fromHtml(createPoiList.get(Integer.valueOf(pos)).getPoidescription()));
                                    tv_services_side.setText(createPoiList.get(Integer.valueOf(pos)).getPoiside());

                                    llserviceslayout.setVisibility(View.VISIBLE);
                                    tv_poi_map_des.setVisibility(View.GONE);

                                    if (selected_poi_pos > -1)
                                        createPoiList.get(selected_poi_pos).setClick_status(false);

                                    selected_poi_pos = Integer.valueOf(pos);

                                    createPoiList.get(Integer.valueOf(pos)).setClick_status(true);

                                    // add_marker_services(createPoiList.get(selected_poi_pos).getPoiServerId());
                                    add_marker_plan(return_plan);

                                }


                                else if (marker.getSnippet().contains("RO")) {
                                    String pos = marker.getSnippet().substring(3);
                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);

                                    fc_poi_map_image.setImageResource(R.drawable.map_slider_icon4_selected);

                                    //fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                    tv_poi_map_name.setText(onAdminRouteRoLists.get(Integer.valueOf(pos)).getROName());

                                    // tv_poi_map_des.setText(Html.fromHtml(routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIShortDescription()));

                                    tv_poi_map_distance.setText((int) Math.ceil(onAdminRouteRoLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms [" + Constants.convert_minute_hrs(onAdminRouteRoLists.get(Integer.valueOf(pos)).getDurationFromRouteOrigin().doubleValue()) + "]");
                                    tv_services_location.setText(onAdminRouteRoLists.get(Integer.valueOf(pos)).getROLocationName());
                                    tv_services_side.setText(onAdminRouteRoLists.get(Integer.valueOf(pos)).getROSide());

                                    llserviceslayout.setVisibility(View.VISIBLE);
                                    tv_poi_map_des.setVisibility(View.GONE);
                                    tv_add_poi.setTag("r" + pos);

                                    if (selected_ro_pos > -1)
                                        onAdminRouteRoLists.get(selected_ro_pos).setClick_status(false);

                                    selected_ro_pos = Integer.valueOf(pos);

                                    onAdminRouteRoLists.get(Integer.valueOf(pos)).setClick_status(true);
                                    add_marker_ro();


                                } else if (marker.getSnippet().contains("PoiNight")) {
                                    String pos = marker.getSnippet().substring(9);
                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);


                                    fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + createPoiList.get(Integer.valueOf(pos)).getImagename() + "&width=" + 150 + "&height=" + 150);
                                    fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                    tv_poi_map_name.setText(createPoiList.get(Integer.valueOf(pos)).getPoiName());
                                    tv_poi_map_des.setText(Html.fromHtml(createPoiList.get(Integer.valueOf(pos)).getPoidescription()));

                                    tv_poi_map_distance.setText((int) Math.ceil(createPoiList.get(Integer.valueOf(pos)).getKM()) + " Kms [" + Constants.convert_minute_hrs(createPoiList.get(Integer.valueOf(pos)).getTime().doubleValue()) + "]");


                                    llserviceslayout.setVisibility(View.GONE);
                                    tv_poi_map_des.setVisibility(View.VISIBLE);


                                    tv_add_poi.setTag("n" + pos);


                                    add_marker_Night_destination();

                                } else if (marker.getSnippet().contains("allservice")) {
                                    String pos = marker.getSnippet().substring(11);
                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);

                                    if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(Integer.valueOf(pos)).getSerId(), "Service", return_plan) != 0) {
                                        tv_add_poi.setVisibility(View.GONE);
                                    }

                                    fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + services.get(Integer.valueOf(pos)).getSerCatFooterImageSelected() + "&width=" + 150 + "&height=" + 150);
                                    fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                    tv_poi_map_name.setText(services.get(Integer.valueOf(pos)).getSerName());

                                    // tv_poi_map_des.setText(Html.fromHtml(routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIShortDescription()));

                                    tv_poi_map_distance.setText((int) Math.ceil(services.get(Integer.valueOf(pos)).getDistance()) + " Kms [" + Constants.convert_minute_hrs(services.get(Integer.valueOf(pos)).getDuration().doubleValue()) + "]");
                                    tv_services_location.setText(services.get(Integer.valueOf(pos)).getSerAddress1());
                                    tv_services_side.setText(services.get(Integer.valueOf(pos)).getSerSide());

                                    llserviceslayout.setVisibility(View.VISIBLE);
                                    tv_poi_map_des.setVisibility(View.GONE);
                                    tv_add_poi.setTag("s" + pos);

                                    if (selected_ser_pos > -1)
                                        services.get(selected_ser_pos).setClick_status(false);

                                    selected_ser_pos = Integer.valueOf(pos);

                                    services.get(Integer.valueOf(pos)).setClick_status(true);
                                    add_marker_services();

                                } else if (marker.getSnippet().contains("allpoi")) {
                                    String pos = marker.getSnippet().substring(7);
                                    //      Toast.makeText(getActivity(), marker.getSnippet()+pos, Toast.LENGTH_SHORT).show();//

                                    fl_pop_up_zoo.setVisibility(View.VISIBLE);
                                    if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(Integer.valueOf(pos)).getRouteRefId(), "POI", return_plan) != 0) {
                                        tv_add_poi.setVisibility(View.GONE);
                                    }
                                    if (onAdminRoutePoiLists.get(Integer.valueOf(pos)).getRPType().contains("POI")) {


                                        // Log.d("poiImagepath", Constants.TimbThumb_ImagePath + routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIImage());


                                        fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIImage() + "&width=" + 150 + "&height=" + 150);
                                        fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                        tv_poi_map_name.setText(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIName());
                                        tv_poi_map_des.setText(Html.fromHtml(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIShortDescription()));

                                        tv_poi_map_distance.setText((int) Math.ceil(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms [" + Constants.convert_minute_hrs(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDurationFromRouteOrigin().doubleValue()) + "]");

                                    } else {
                                        fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDesImage() + "&width=" + 150 + "&height=" + 150);
                                        fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                                        tv_poi_map_name.setText(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDesName());
                                        tv_poi_map_des.setText(Html.fromHtml(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDesShortDescription()));

                                        tv_poi_map_distance.setText((int) Math.ceil(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms [" + Constants.convert_minute_hrs(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDurationFromRouteOrigin().doubleValue()) + "]");

                                    }


                                    llserviceslayout.setVisibility(View.GONE);
                                    tv_poi_map_des.setVisibility(View.VISIBLE);


                                    tv_add_poi.setTag("p" + pos);


                                    if (selected_poi_pos > -1)
                                        onAdminRoutePoiLists.get(Integer.valueOf(selected_poi_pos)).setClick_status(false);

                                    selected_poi_pos = Integer.valueOf(pos);

                                    onAdminRoutePoiLists.get(Integer.valueOf(pos)).setClick_status(true);
                                    add_marker_poi();
                                }
                            }


                            return true;
                        }
                    });


// Execute the request
                }
            });

            mapView.onResume();
        } else {
            gpsTracker.showSettingsAlert();
            //  ttobj.speak("Location is not enable.", TextToSpeech.QUEUE_FLUSH, null);
        }

    }


    private void getRouteOfPOI(Integer oId, Integer dId) {

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();


        if (return_plan == 1) {
            int temp = oId;
            oId = dId;
            dId = temp;
        }

        if (Constants.isInternetAvailable(getActivity())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            String Json = "[{\"RouteOriginId\": \"" + oId + "\",\"RouteDesId\":\"" + dId + "\",\"CurrentLat\":\"23.0605914\", \"CurrentLong\":\"72.52878059999999\"   }]";

            Log.d("Routejson", Json);
            Call<ArrayList<RoutePojo>> call = apiService.RouteExplore(Json);
            call.enqueue(new Callback<ArrayList<RoutePojo>>() {
                @Override
                public void onResponse(Call<ArrayList<RoutePojo>> call, Response<ArrayList<RoutePojo>> response) {
                    routepojo = new ArrayList<RoutePojo>();
                    routepojo = response.body();

                    if (routepojo.get(0).getResStatus() == true) {

                        if (return_plan == 1) {



                            if (map.getMyLocation() != null)
                            {
                                source.setLatitude(map.getMyLocation().getLatitude());
                                source.setLongitude(map.getMyLocation().getLongitude());
                            } else if (gpsTracker.getLocation() != null) {
                                source.setLatitude(gpsTracker.getLatitude());
                                source.setLongitude(gpsTracker.getLongitude());
                            } else {
                                source.setLatitude(createTripList.get(0).getDestination_latitude());
                                source.setLongitude(createTripList.get(0).getDestination_longitude());
                            }
                           /* source.setLatitude(createTripList.get(0).getDestination_latitude());
                            source.setLongitude(createTripList.get(0).getDestination_longitude());
*/

                            destination.setLatitude(createTripList.get(0).getSource_latitude());
                            destination.setLongitude(createTripList.get(0).getSource_longitude());

                        }
                        else {

                          if (map.getMyLocation() != null)
                            {
                                source.setLatitude(map.getMyLocation().getLatitude());
                                source.setLongitude(map.getMyLocation().getLongitude());
                            }
                            else if (gpsTracker.getLocation() != null) {
                                source.setLatitude(gpsTracker.getLatitude());
                                source.setLongitude(gpsTracker.getLongitude());
                            } else {
                                source.setLatitude(createTripList.get(0).getSource_latitude());
                                source.setLongitude(createTripList.get(0).getSource_longitude());
                            }



                          /*  source.setLatitude(createTripList.get(0).getSource_latitude());
                            source.setLongitude(createTripList.get(0).getSource_longitude());*/

                            if(createTripList.get(0).getTripGooglePlaceLat()>0 && createTripList.get(0).getDes_address().length()>1)
                            {
                                destination.setLatitude(createTripList.get(0).getTripGooglePlaceLat());
                                destination.setLongitude(createTripList.get(0).getTripGooglePlaceLong());
                                complete_stay_radius_meter=40;
                            }
                            else {
                                destination.setLatitude(createTripList.get(0).getDestination_latitude());
                                destination.setLongitude(createTripList.get(0).getDestination_longitude());

                            }
                        }
                        preferences.edit().putString("source_lat",String.valueOf(source.getLatitude()));
                        preferences.edit().putString("source_long",String.valueOf(source.getLongitude()));



                        //dbHandler.update_source_Latlong(createTripList.get(0).getTripId(), Double.valueOf(source.getLatitude()), Double.valueOf(source.getLongitude()),return_plan);



                        String wayPoints = "";

                        String url = null;


                        try {
                            JSONArray jsonArray = new JSONArray(routepojo.get(0).getRouteWayPoints());
                            List<Create_Poi> create_poiList1 = new ArrayList<>();
                            create_poiList1 = dbHandler.get_POI_Into_Cart_By_TripId(tripid, return_plan);

                            int count = create_poiList1.size() >= 6 ? create_poiList1.size() : create_poiList1.size();
                            for (int i = 0; i < count; i++) {

                                wayPoints += create_poiList1.get(i).getPoiLatitude() + "," + create_poiList1.get(i).getPoiLongitude() + "|";
                            }
                            if (jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject explrObject = jsonArray.getJSONObject(i);
                                    wayPoints += explrObject.get("lat") + "," + explrObject.get("lng") + "|";
                                    url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, wayPoints);
                                }
                                Log.i("system", "Url of Map if--" + url);
                            } else {
                                url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, wayPoints);
                                Log.i("system", "Url of Map else--" + url);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("routeurl", e.toString());

                        }

                        url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, wayPoints);



                        onAdminRoutePoiLists = new ArrayList<RoutePojo.OnAdminRoutePoiList>();
                        onAdminRouteRoLists = new ArrayList<RoutePojo.OnAdminRoutePoiList>();

                        IconFactory iconFactory = IconFactory.getInstance(getActivity());
                        Drawable iconDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.round_blue_map);
                        Icon icon = iconFactory.fromDrawable(iconDrawable);

                        Drawable iconDrawable_des = ContextCompat.getDrawable(getActivity(), R.drawable.round_red_map);
                        Icon icon_des = iconFactory.fromDrawable(iconDrawable_des);



                        if (routepojo.get(0).getOnAdminRoutePoiLists().get(0).getResStatus() == true)
                        {
                            if (routepojo.get(0).getOnAdminRoutePoiLists().size() > 0) {
                                for (int i = 0; i < routepojo.get(0).getOnAdminRoutePoiLists().size(); i++) {

                                    if (routepojo.get(0).getOnAdminRoutePoiLists().get(i).getRPType().contains("RO"))
                                        onAdminRouteRoLists.add(routepojo.get(0).getOnAdminRoutePoiLists().get(i));
                                    else
                                        onAdminRoutePoiLists.add(routepojo.get(0).getOnAdminRoutePoiLists().get(i));
                                }
                            }


                        }


                        String json = gson.toJson(onAdminRouteRoLists);
                        preferences.edit().putString("rolist", json).commit();

                        json = gson.toJson(onAdminRoutePoiLists);
                        preferences.edit().putString("poilist", json).commit();


                        if (createTripList.get(0).getTripIsOneSideCompleted()) {
                            add_data_for_push_notification_for_Ro(1);
                        } else {
                            add_data_for_push_notification_for_Ro(0);
                        }
                        getRoute(url);
                        add_marker_plan(return_plan);
                        getTollbooth();

                    }
                    pb_dialog.dismiss();

                }

                @Override
                public void onFailure(Call<ArrayList<RoutePojo>> call, Throwable t) {
                    // Log error here since request failed
                    t.printStackTrace();
                    pb_dialog.dismiss();
                }
            });
        } else {
            pb_dialog.dismiss();
        }
    }


    @Override
    public void onPause() {
        super.onPause();

        if(createTripList.size()>0 && preferences.getBoolean("trip_start_tracking", false))
        {
            sendNotification_onResume();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            mapView.onDestroy();
        } catch (Exception e) {
        }

    }

    private void getMarkerBitmapFromView(final int position, final String type, final Double lat, final Double longi, int imagepos) {


        final View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        final CircleImageView markerImageView = (CircleImageView) customMarkerView.findViewById(R.id.fc_poi);


        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
        String  path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();

        String imagename=listServiceCategoryList.get(imagepos).getSerCatImage().toString();

        if (type.equals("Services") )
        {

            if (createPoiList.get(position).isClick_status() )
            {
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }
            path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();

            imagename=listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();


        }
        else if( type.equals("allservice"))
        {
            if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(position).getSerId(), "Service", return_plan) != 0)
            {
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();

                imagename=listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
            }



            if (services.get(position).isClick_status())
            {
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }
        }
        else if (type.equals("POI") || type.equals("Roplan") )
        {
            if (createPoiList.get(position).isClick_status()) {

                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }
            path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();

            imagename=listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();

        }
        else if ( type.equals("allpoi"))
        {
            if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(position).getRouteRefId(), "POI", return_plan) != 0) {

                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
            }
            if (onAdminRoutePoiLists.get(position).isClick_status()) {

                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }

        }
        else if (type.equals("RO"))
        {
            if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRouteRoLists.get(Integer.valueOf(position)).getROMasterId(), "RO", return_plan) != 0) {
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
            }
            if (onAdminRouteRoLists.get(position).isClick_status())
            {
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }


        }
        else if (type.contains("PoiNight"))
        {

            if (dbHandler.check_poi_night_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
            }
            if (explorePoiList.get(position).isClick_status())
            {
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }
        }
        else if (type.contains("DesPlan"))
        {
            if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
            }
            if (explorePoiList.get(position).isClick_status())
            {
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics());
            }
        }

        FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
        markerImageView.setLayoutParams(parms);

        final String finalPath = path;
        final String finalImagename=imagename;
        Log.d("check_image","path "+path +" name "+ finalImagename +" value "+new ImageSaver(getActivity()).check_file_exists(finalImagename));


        if (!new ImageSaver(getActivity()).check_file_exists(imagename))
        {

            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(Uri.parse(finalPath))
                    .setAutoRotateEnabled(true)
                    .build();

            ImagePipeline imagePipeline = Fresco.getImagePipeline();
            final DataSource<CloseableReference<CloseableImage>>
                    dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


            dataSource.subscribe(new BaseBitmapDataSubscriber()
            {

                @Override
                public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                    if (bitmap != null)
                    {


                        //stuff that updates ui

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                markerImageView.setImageBitmap(bitmap);
                                //   markerImageView.setImageResource(resId);
                                customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                                customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                                //  customMarkerView.buildDrawingCache();
                                Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                        Bitmap.Config.ARGB_8888);

                                Canvas canvas = new Canvas(returnedBitmap);
                                canvas.drawColor(Color.BLUE, PorterDuff.Mode.SRC_IN);
                                Drawable drawable = customMarkerView.getBackground();

                                if (drawable != null)
                                    drawable.draw(canvas);

                                customMarkerView.draw(canvas);
                                IconFactory iconFactory = IconFactory.getInstance(getActivity());
                                Icon marker_icon = iconFactory.fromBitmap(returnedBitmap);

                                map.addMarker(new MarkerOptions().snippet(type + "," + String.valueOf(position)).position(new LatLng(lat, longi)).icon(marker_icon));

                                new ImageSaver(getActivity()).
                                        setFileName(finalImagename).

                                        setExternal(true).
                                        save(bitmap);


                            }
                        });

                        dataSource.close();

                    }
                }

                @Override
                public void onFailureImpl(DataSource dataSource) {
                    if (dataSource != null) {
                        dataSource.close();
                    }


                }
            }, CallerThreadExecutor.getInstance());
        }
        else
        {
            Bitmap bitmap = new ImageSaver(getActivity()).
                    setFileName(imagename).
                    setExternal(true).
                    load();

            markerImageView.setImageBitmap(bitmap);

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());

            Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(returnedBitmap);
            canvas.drawColor(Color.BLUE, PorterDuff.Mode.SRC_IN);
            Drawable drawable = customMarkerView.getBackground();

            if (drawable != null)
                drawable.draw(canvas);

            customMarkerView.draw(canvas);
            IconFactory iconFactory = IconFactory.getInstance(getActivity());
            Icon marker_icon = iconFactory.fromBitmap(returnedBitmap);

            map.addMarker(new MarkerOptions().setSnippet(type + "," + String.valueOf(position)).position(new LatLng(lat, longi)).icon(marker_icon));

        }


    }





    private void add_marker_ro() {
        map.removeAnnotations();
        draw_route();
        if (onAdminRouteRoLists.size() > 0) {
            try {



                for (int i = 0; i < onAdminRouteRoLists.size(); i++) {
                    //   map.addMarker(new MarkerOptions().title(onAdminRouteRoLists.get(i).getROName()).snippet("RO" + "," + String.valueOf(i)).position(new LatLng(Double.parseDouble(onAdminRouteRoLists.get(i).getROLatitude()), Double.parseDouble(onAdminRouteRoLists.get(i).getROLongitude()))).icon(icon1));

                    getMarkerBitmapFromView(i, "RO", Double.parseDouble(onAdminRouteRoLists.get(i).getROLatitude()), Double.parseDouble(onAdminRouteRoLists.get(i).getROLongitude()), 0);
                }
            } catch (Exception e) {
            }
        } else {
            // Constants.show_error_popup(getActivity(), "No Ro found!", iv_bottom_up_arrow);
        }
    }

    private void add_marker_services() {

        map.clear();
        draw_route();
        if (services.size() > 0) {
            try {


                for (int i = 0; i < services.size(); i++) {


                    getMarkerBitmapFromView(i, "allservice", Double.parseDouble(services.get(i).getSerLatitude()), Double.parseDouble(services.get(i).getSerLongitude()), category_select_pos);
                }
            } catch (Exception e) {
            }
        } else {
            map.clear();

            Constants.show_error_popup(getActivity(), "No service found.", iv_bottom_up_arrow);
        }

    }

    private void add_marker_poi() {

        try {
            map.clear();
            draw_route();
            for (int i = 0; i < onAdminRoutePoiLists.size(); i++) {

                if (onAdminRoutePoiLists.get(i).getResStatus() == true) {
                    if (onAdminRoutePoiLists.get(i).getRPType().contains("POI")) {
                        getMarkerBitmapFromView(i, "allpoi", Double.parseDouble(onAdminRoutePoiLists.get(i).getPOILatitude()), Double.parseDouble(onAdminRoutePoiLists.get(i).getPOILongitude()), 1);

                    } else {
                        getMarkerBitmapFromView(i, "allpoi", Double.parseDouble(onAdminRoutePoiLists.get(i).getDesLatitude()), Double.parseDouble(onAdminRoutePoiLists.get(i).getDesLongitude()), 1);

                    }
                } else {

                    Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", iv_bottom_up_arrow);

                }
            }
        } catch (Exception e) {
        }

    }

    private void add_marker_Night_destination() {

        if (explorePoiList.size() > 0) {
            try {
                map.removeAnnotations();
                map.moveCamera(CameraUpdateFactory.newCameraPosition(
                        new CameraPosition.Builder()
                                .target(new LatLng(Double.parseDouble(explorePoiList.get(0).getPOILatitude()), Double.parseDouble(explorePoiList.get(0).getPOILongitude()))) // set the camera's center position
                                .zoom(10)  // set the camera's zoom level
                                // set the camera's tilt
                                .build()));
                map.setMyLocationEnabled(true);
                map.animateCamera(CameraUpdateFactory.zoomIn());
                //draw_route();
                for (int i = 0; i < explorePoiList.size(); i++) {


                    getMarkerBitmapFromView(i, "PoiNight", Double.parseDouble(explorePoiList.get(i).getPOILatitude()), Double.parseDouble(explorePoiList.get(i).getPOILongitude()), 1);
                }
            } catch (Exception e) {
            }
        } else {

            Constants.show_error_popup(getActivity(), "No attraction found.", iv_bottom_up_arrow);

        }
    }

    private void add_marker_destination_plan() {

        if (explorePoiList.size() > 0) {
            try {
                map.removeAnnotations();
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(explorePoiList.get(0).getPOILatitude()), Double.valueOf(explorePoiList.get(0).getPOILongitude())), 10));
                map.setMyLocationEnabled(false);

                map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);


                //     map.addPolygon(new PolygonOptions().addAll(polygonCircleForCoordinate(new LatLng(Double.valueOf(explorePoiList.get(0).getPOILatitude()), Double.valueOf(explorePoiList.get(0).getPOILongitude())), 25000.0)).fillColor(Color.parseColor("#12121212")));
                // Enable user tracking to show the padding affect.
               /* Icon icon = IconFactory.getInstance(getActivity())
                        .fromResource(R.drawable.map_circle);

                MarkerView marker = map.addMarker(new MarkerViewOptions()
                        .position(new com.mapbox.mapboxsdk.geometry.LatLng(createTripList.get(0).getDestination_latitude(), createTripList.get(0).getDestination_longitude()))
                        .icon(icon)
                        .flat(true));

              View  markerView = map.getMarkerViewManager().getView(marker);*/

                //draw_route();
                for (int i = 0; i < explorePoiList.size(); i++) {


                    getMarkerBitmapFromView(i, "DesPlan", Double.parseDouble(explorePoiList.get(i).getPOILatitude()), Double.parseDouble(explorePoiList.get(i).getPOILongitude()), 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Constants.show_error_popup(getActivity(), "No attraction found. ", iv_bottom_up_arrow);

        }
    }


    public void draw_route() {
        if (routelineresult != null && map != null)

        {

            IconFactory iconFactory = IconFactory.getInstance(getActivity());
            Drawable iconDrawable = ContextCompat.getDrawable(getActivity(), R.drawable.round_blue_map);
            Icon icon = iconFactory.fromDrawable(iconDrawable);

            Drawable iconDrawable_des = ContextCompat.getDrawable(getActivity(), R.drawable.round_red_map);
            Icon icon_des = iconFactory.fromDrawable(iconDrawable_des);



            map.addMarker(new MarkerOptions().title(createTripList.get(0).getSource().toString()).snippet("point").position(new LatLng(source.getLatitude(), source.getLongitude())).icon(icon));
            map.addMarker(new MarkerOptions().title(createTripList.get(0).getDestination().toString()).snippet("point").position(new LatLng(destination.getLatitude(), destination.getLongitude())).icon(icon_des));




            if(lineOptions.getPoints().size()==0) {
                // Traversing through all the routes

                for (int i = 0; i < routelineresult.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = routelineresult.get(i);


                    for (int j = 0; j < path.size(); j++) {
                        latlong latlong = new latlong();
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        b1.include(position);
                        points.add(position);




                    }
                    lineOptions.addAll(points);
                    lineOptions.width(12);


                    lineOptions.color(getActivity().getResources().getColor(R.color.colorAccent));
                }
            }

            //polyline draw when trip start


            map.addPolyline(lineOptions);


            LatLngBounds bounds = b1.build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));







            /*String polyline_encode="}{qkColoyLc@hJO~BOA`AmRb@mIf@iM\\\\eGb@{HLcB_MqE{CoAeI}CeCaAoCcAuF_Co@@mDsAsZkLKEBK`@cA^m@lDqEpBwB^g@Xo@h@wBbBkI?C?EBC@AdAwErBmJp@mC_LsEcE_B_HwCyBcAQx@UhAaBvNMdAe@dCkAlEoBvGk@bEKlCE~D?z@?j@uK}C{G}A{HyA}UuEiIcBkAWOOCC}Bg@gL}BmMeCcKmB_LyB_\\\\kGaL}BEXoAU";
            points.clear();
            points = decode(polyline_encode);*/


            // draw_polygon();
        }
    }


    private void getListServices_Category() {
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String Json = "[{     \"SerCatId\": \"0\"}]";

        Call<ArrayList<ListServiceCategory>> call = apiService.ListServiceCategory(Json);
        call.enqueue(new Callback<ArrayList<ListServiceCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ListServiceCategory>> call, Response<ArrayList<ListServiceCategory>> response) {
                if (response.body().get(0).getResStatus() == true) {
                    listServiceCategoryList = new ArrayList<ListServiceCategory>();
                    listServiceCategoryList.clear();
                    listServiceCategoryList.addAll(response.body());
                    for (int i = 0; i < listServiceCategoryList.size(); i++) {

                        listServiceCategoryList.get(i).setStatus(false);
                        listServiceCategoryList.get(i).setMarker_flag(false);
                      /*  store_image(listServiceCategoryList.get(i).getSerCatFooterImage());
                        store_image(listServiceCategoryList.get(i).getSerCatFooterImageSelected());
                        store_image(listServiceCategoryList.get(i).getSerCatImage());*/
                    }
                    add_marker_plan(return_plan);
                    serviceListAdapter.notifyDataSetChanged();


                    String json = gson.toJson(listServiceCategoryList);
                    preferences.edit().putString("listofservices", json).commit();


                    pb_dialog.dismiss();
                    //   FacilityContent();

                } else {
                    pb_dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListServiceCategory>> call, Throwable t) {
                // Log error here since request failed
                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }
            }
        });
    }

    private void getPlaces(String search) {


        ApiInterface apiService = ApiClient.get_retrofit_client_google_autocomplete().create(ApiInterface.class);
        Call<Place> call = apiService.GetPlaces("json?input=" + search + "," + createTripList.get(0).getDestination() + "&sensor=false&key=AIzaSyCd1xkg-MlZlS-2MBGAFWBp5wcWEVoYidk");
        call.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {
                if (response.body() != null) {

                    placeList = new ArrayList<String>();
                    placeList.clear();

                    for (int i = 0; i < response.body().getPredictions().size(); i++) {
                        placeList.add(response.body().getPredictions().get(i).getDescription());
                    }

                    adapter.notifyDataSetChanged();


                }


            }

            @Override
            public void onFailure(Call<Place> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });

    }

    private void getPOIAPI(Integer destId, Double destLat, Double destLong) {
        // get & set progressbar dialog

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "";

        json = "[{\"PoiUmId\": \"0\",\"POIID\":\"0\" ,\"radius\":\"100\"   , \"POINearbyDestination\": \"" + destId + "\",  \"PageNo\":1,\"PageSize\":100, \"POILongitude\":\"" + destLat + "\",\"POILatitude\":\"" + destLong + "\",\"CategoryName\":\"Attraction\", \"POIRating\":0  }]";

        Log.d("System out", "Explore POI____" + json);


        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {


                pb_dialog.dismiss();

                if (response.body() != null) {


                    explorePoiList = new ArrayList<ExplorePOI>();
                    explorePoiList.clear();
                    explorePoiList.addAll(response.body());
                    if (explorePoiList.size() > 0) {
                        if (explorePoiList.get(0).getResStatus() == true) {

                            add_marker_Night_destination();

                            //  Collections.reverse(exploreDestList);


                        }

                    } else {
                        pb_dialog.dismiss();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {


        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.6;

        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }



    private void getTollbooth() {

        if (routepojo.size() > 0) {
            Integer routeid = routepojo.get(0).getRouteId();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String Json = "[{\"RouteId\":\"" + routeid + "\"}]";

            Log.d("tollboothjson", Json);


            Call<ArrayList<TollBooth>> call = apiService.GetToolBooth(Json);
            call.enqueue(new Callback<ArrayList<TollBooth>>() {
                @Override
                public void onResponse(Call<ArrayList<TollBooth>> call, Response<ArrayList<TollBooth>> response) {
                    if (response.body() != null && response.body().size() > 0) {

                        if (response.body().get(0).getResStatus() == true) {
//                    dbHandler.deleteAllService();
                            tollBoothArrayList = new ArrayList<TollBooth>();
                            tollBoothArrayList.clear();
                            tollBoothArrayList.addAll(response.body());
                            toolBooth_list_adapter = new TollBoothAdapter(getActivity(), tollBoothArrayList);
                            toolBooth_list_adapter.notifyDataSetChanged();
                           /* String json = gson.toJson(tollBoothArrayList);
                            preferences.edit().putString("tollBooth",json).commit();
                              */
                        }
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<TollBooth>> call, Throwable t) {
                    // Log error here since request failed
                    t.printStackTrace();
                }
            });
        }
    }

    public void update_poi_plan(int pos) {

        try {


            String triparrival = createPoiList.get(pos).getActual_arrival_date_time();
            String tripdeparture = createPoiList.get(pos).getActual_dep_date_time();
            String resumedate = "";
            if (createPoiList.get(pos).getResumeDate() != null)

                resumedate = createPoiList.get(pos).getResumeDate() + " " + createPoiList.get(pos).getResumeTime().toString().replace(".", ":") + ":00";

            try {
                triparrival = Constants.formatDate(triparrival, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                Log.d("resumedate1", triparrival);
                triparrival = "";

            }

            try {
                tripdeparture = Constants.formatDate(tripdeparture, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripdeparture = "";
            }

            try {
                resumedate = Constants.formatDate(resumedate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                resumedate = "";

            }

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            String json = "[{     \"TripWayType\": \"" + createPoiList.get(pos).getPoitype() + "\",     \"TripWayRefId\": \"" + createPoiList.get(pos).getPoiDesID() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"" + triparrival + "\" ,\"TripEstOutTime\":\"" + tripdeparture + "\",\"TripDesId\":" + createPoiList.get(pos).getPoiDesID() + ", \"TripIsPause\":\"false\" ,\"TripResumeDateTime\":\"" + resumedate + "\",\"TripDistanceInKm\":" + createPoiList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + createPoiList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + return_trip_plan + ",\"TripSpendTime\":" + createPoiList.get(pos).getStayTime() + ",\"TripWayPointId\":" + createPoiList.get(pos).getWayPointId() + ",\"TripIsNightStayOn\":" + createPoiList.get(pos).getPoiStay() + "}]";

            Log.d("json_update_poi_plan1", json);

            Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
            call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
                @Override
                public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {


                }

                @Override
                public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();


                }
            });
        } catch (Exception e) {
        }

    }


    public void update_trip() {
        try {
            createTripList = dbHandler.get_TRIP_By_TripId(tripid);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String tripdate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime() + ":00";
            String tripenddate = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime() + ":00";

            String returndate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time() + ":00";


            String returndatetimeeta = createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time() + ":00";
            Log.d("start_date", returndate);
            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            try {
                returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                returndate = "";
            }

            try {
                returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {

                returndatetimeeta = "";

            }

            String tripstatus = createTripList.get(0).getTripstatus();

            String json = "[{     \"TripName\": \"" + createTripList.get(0).getTripname() + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(0).getSourceId() + ",\"TripDestination_End\":" + createTripList.get(0).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + createTripList.get(0).getTripId() + "\",\"TripStatus\": \"" + tripstatus + "\",\"TripGooglePlaceDetails\":\"" + createTripList.get(0).getDes_address() +"\",\"TripKm\": \""+createTripList.get(0).getKM()+"\" ,\"TripTime\":\""+createTripList.get(0).getTime()+"\",\"TripReturnKm\":\""+createTripList.get(0).getReturnKM()+"\",\"TripReturnTime\":\""+createTripList.get(0).getReturnTime()+"\",\"TripGooglePlaceLat\":\""+createTripList.get(0).getTripGooglePlaceLat().toString()+"\",\"TripGooglePlaceLong\":\""+createTripList.get(0).getTripGooglePlaceLong().toString()+"\" }]";
            Log.d("json_CreateTrip1", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<UpdateTrip> call = apiService.updateTrip(json);
            call.enqueue(new Callback<UpdateTrip>() {
                @Override
                public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                    if (response.body() != null) {


                    }
                }

                @Override
                public void onFailure(Call<UpdateTrip> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    pb_dialog.dismiss();

                    /*if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                    }*/

                }
            });
        } catch (Exception e) {
        }
    }

    public void getListDefaultvalues() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = null;


        json = "[{     \"keyName\": \"default\"    }]";


        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<ListDefaultValues>> call = apiService.GetDefaultValues(json);
        call.enqueue(new Callback<ArrayList<ListDefaultValues>>() {
            @Override
            public void onResponse(Call<ArrayList<ListDefaultValues>> call, Response<ArrayList<ListDefaultValues>> response) {


                if (response.body() != null) {

                    Constants.listDefaultValuesArrayList = new ArrayList<>();


                    if (response.body().size() > 0) {

                        Constants.listDefaultValuesArrayList.addAll(response.body());

                        for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

                        {
                            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_EmergencyNumber"))
                                default_EmergencyNumber = Constants.listDefaultValuesArrayList.get(i).getValue().toString();

                        }


                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ListDefaultValues>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();


            }
        });
    }

   /* public void set_destination_layout() {

        ll_des_plan.removeAllViews();
        if (destinationplanList.size() > 0) {


            try {

                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEndtime());
                Date date1 = format.parse(temptime);
                Date firstPoiDate = format.parse(destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out", "End time of trip " + c.getTime());
                Log.d("System out", "End time of day " + date1.getTime());
                if (c.getTimeInMillis() <= date1.getTime()) {
                    if (firstPoiDate.getTime() <= c.getTimeInMillis()) {
                        destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));

                    } else {

                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(0);
                } else {
                    if (destinationplanList.get(0).getStarttime() == 24.00) {
                        destinationplanList.get(0).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                destinationplanList.get(0).setDay(1);
                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

            }


          *//*
            if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(1);*//*

            for (int j = 1; j < destinationplanList.size(); j++) {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());


                if ((destinationplanList.get(j).getDepartureTime() - destinationplanList.get(0).getArrivalTime()) > dayhours) {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);
                    dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }


            }

        }

        noday = new ArrayList<>();
        noday.clear();

        //   noday.add(1);

        Integer day = 0;

        if(destinationplanList.get(0).getDay() == 0)
        {
            day = destinationplanList.get(0).getDay();
            noday.add(destinationplanList.get(0).getDay());
        }
        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);
            try {
                if (createTripList.get(0).getEnddate().length() > 0) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                    try {
                        Date date = format.parse(createTripList.get(0).getEnddate());
                        c.setTime(date);
                        c.add(Calendar.DATE, day);
                        destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }

                }
            } catch (Exception e) {
            }

            // Log.d("days","pos"+String.valueOf(j)+"value"+noday.get(j).toString()+"v2"+String.valueOf(destinationplanList.get(i).getDay()));
            if (day != destinationplanList.get(i).getDay()) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());
            }

        }


        for (int j = 0; j < noday.size(); j++) {
            AddDayWisePlan(j);
        }


        desplan = true;
        // tv_no_days.setText(noday.size()+" Days");

    }


    public void AddDayWisePlan(int pos) {


        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.add_day_wise_plan_layout, null);

        TextView tv_day = (TextView) v.findViewById(R.id.tv_day);
        TextView tv_des_date = (TextView) v.findViewById(R.id.tv_des_date);
        tv_des_date.setText("");
        try {

            if (createTripList.get(0).getEnddate().length() > 0) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                try {
                    Date date = format.parse(createTripList.get(0).getEnddate());
                    c.setTime(date);
                    c.add(Calendar.DATE, noday.get(pos));
                    tv_des_date.setText(format.format(c.getTime()));


                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tv_des_date.setText("");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            tv_des_date.setText("");

        }
        Log.d("System out", "day pos " + noday.get(pos));
        if (noday.size() > 0) {
            if (noday.get(0).intValue() == 0)
                tv_day.setText("Day " + (noday.get(pos) + 1));
            else
                tv_day.setText("Day " + noday.get(pos));
        } else {
            tv_day.setText("Day " + noday.get(pos));
        }
        ll_add_places_to_plan = (LinearLayout) v.findViewById(R.id.ll_add_places_to_plan);


        //  ll_add_places_to_plan.removeAllViews();

        //set time


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == noday.get(pos)) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        if (tempdestiDestinationPlanlist.size() > 0) {
            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            SimpleDateFormat format = new SimpleDateFormat("HH:mm");

            try {
                Date date = format.parse(createTripList.get(0).getEndtime());
                Date date1 = format.parse(temptime);
                Date firstPoiDate = format.parse(tempdestiDestinationPlanlist.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out", "End time of trip " + c.getTime());
                Log.d("System out", "End time of day " + date1.getTime());
                if (c.getTimeInMillis() <= date1.getTime() && pos == 0) {

                    if (firstPoiDate.getTime() <= c.getTimeInMillis()) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));

                    } else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                } else {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                tv_des_date.setText("");
            }


            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempdestiDestinationPlanlist.get(0).getArrivalTime()) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }

        }
        ll_add_places_to_plan.removeAllViewsInLayout();
        for (int i = 0; i < tempdestiDestinationPlanlist.size(); i++) {

            AddPlacesToPlan(i);
        }

        ll_des_plan.addView(v);

    }

    public void AddPlacesToPlan(final int pos) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.ad_places_to_plan_route, null);
        ImageView iv_dotted_line = (ImageView) v.findViewById(R.id.iv_dotted_line);
        iv_dotted_line_bottom = (ImageView) v.findViewById(R.id.iv_dotted_line_bottom);
        TextView tv_km_time_plan = (TextView) v.findViewById(R.id.tv_km_time_plan);
        final TextView et_stay_time = (TextView) v.findViewById(R.id.et_stay_time);
        LinearLayout ll_edit_stay = (LinearLayout) v.findViewById(R.id.ll_bg_edit);
        LinearLayout ll_edit_start_time = (LinearLayout) v.findViewById(R.id.ll_bg_edit_starttime);
        TextView tv_start_end_time = (TextView) v.findViewById(R.id.tv_start_end_time);
        LinearLayout ll_name_poi = (LinearLayout) v.findViewById(R.id.ll_name_poi);
        TextView tv_des_name = (TextView) v.findViewById(R.id.tv_des_name);
        CircleImageView fc_poi_image = (CircleImageView) v.findViewById(R.id.fc_poi_image);
        final ImageView iv_poi_delete = (ImageView) v.findViewById(R.id.iv_poi_delete);
        final ImageView iv_map_view = (ImageView) v.findViewById(R.id.iv_map_view);
        TextView tv_arrival_time = (TextView) v.findViewById(R.id.tv_arrival_time);
        TextView tv_km_time = (TextView) v.findViewById(R.id.tv_km_time_plan);
        iv_map_view.setVisibility(View.GONE);
        ll_name_poi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Constants.isInternetAvailable(getActivity())) {

                    try {
                        if (handler != null)
                            handler.removeCallbacks(runnable);



                    } catch (Exception e) {
                    }

                    Bundle b = new Bundle();


                    //    Intent intent = new Intent(VehicleDetailsRSA.this, ExploreActivity.class);
                    //   b.putString("destID", explorePOIList.get(0).getpo().toString());
                    b.putString("poiLat", tempdestiDestinationPlanlist.get(pos).getPoiLatitude().toString());
                    b.putString("poiLong", tempdestiDestinationPlanlist.get(pos).getPoiLongitude().toString());
                    b.putString("name", tempdestiDestinationPlanlist.get(pos).getPoiName().toString());
                    Intent i=new Intent(getActivity(),MapviewOfPlace.class);
                    i.putExtras(b);
                    startActivity(i);
                    //((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MapviewOfPlace(), b);
                }
                else
                {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                }
            }
        });

        editIconUnselected = false;
        if (editIconUnselected) {
            iv_poi_delete.setVisibility(View.VISIBLE);
         //   ll_edit_stay.setBackground(getResources().getDrawable(R.drawable.textbox_edit));


            et_stay_time.setEnabled(true);

            if (pos == 0) {
                //  ll_edit_start_time.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
                tv_arrival_time.setEnabled(false);
                ll_edit_start_time.setBackground(null);
            } else {
                ll_edit_start_time.setBackground(null);
                tv_arrival_time.setEnabled(false);
            }
        } else {
            iv_poi_delete.setVisibility(View.GONE);
          //  ll_edit_stay.setBackground(null);
            ll_edit_start_time.setBackground(null);
            et_stay_time.setEnabled(false);
        }

      *//*  ll_edit_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_stay_time.requestFocus();
            }
        });*//*

        et_stay_time.setTag(tempdestiDestinationPlanlist.get(pos).getPos());


        et_stay_time.setOnEditorActionListener(new TextView.OnEditorActionListener() {


            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                Constants.hidekeyboard(getActivity(), et_stay_time);
                if (actionId == EditorInfo.IME_ACTION_DONE)

                {


                    int pos1 = Integer.valueOf(et_stay_time.getTag().toString());

                    if (pos1 >= 0) {


                        Log.d("timebetween", destinationplanList.get(pos1).getStarttime().toString() + "end" + destinationplanList.get(pos1).getEndtime() + "timestay " + et_stay_time.getText().toString());
                        if ((destinationplanList.get(pos1).getEndtime() - destinationplanList.get(pos1).getStarttime()) >= Double.valueOf(et_stay_time.getText().toString())) {
                            destinationplanList.get(pos1).setStayTime(Double.valueOf(et_stay_time.getText().toString()));
                            dbHandler.update_stay_time_destination_plan(destinationplanList.get(pos1).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));

                            set_destination_layout();
                        } else {
                            Constants.show_error_popup(getActivity(), "Our system is limited to 7 hours to visit attractions. We are working to improve it.", iv_bottom_up_arrow);
                        }
                    }
                    return true;
                }
                return false;
            }
        });


        double roundOff1 = Math.round(tempdestiDestinationPlanlist.get(pos).getKM() / 1000 * 100.0) / 100.0;
        tv_km_time.setText(roundOff1 + " Kms / " + Constants.convert_minute_hrs_both(tempdestiDestinationPlanlist.get(pos).getTime()));


        DecimalFormat f = new DecimalFormat("##.00");
        try {
            tv_arrival_time.setText(Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
        } catch (ParseException e) {
            e.printStackTrace();

        }
        et_stay_time.setText(tempdestiDestinationPlanlist.get(pos).getStayTime().toString());

        tv_des_name.setText(tempdestiDestinationPlanlist.get(pos).getPoiName());


        // Log.d("starttime","name"+destinationplanList.get(pos).getPoiName()+"time"+f.format(destinationplanList.get(pos).getStarttime()));


        if (tempdestiDestinationPlanlist.get(pos).getStarttime() == 24.00) {
            tv_start_end_time.setText("24 hrs");

        } else {
            String starttime = "0:0", endtime = "0:0";
            try {
                starttime = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getStarttime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");
                endtime = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getEndtime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            } catch (ParseException e) {
                e.printStackTrace();
                starttime = "0:0";
                endtime = "0:0";
            }

            tv_start_end_time.setText(starttime + " - " + endtime);


        }

        Bitmap bitmap = new ImageSaver(getActivity()).
                setFileName(tempdestiDestinationPlanlist.get(pos).getImagename()).
                setExternal(true).
                load();
        if (bitmap != null)
            fc_poi_image.setImageBitmap(bitmap);
        else
            fc_poi_image.setImageBitmap(null);


        if (pos > 0) {

            iv_dotted_line.setVisibility(View.VISIBLE);
        }
        if (pos == tempdestiDestinationPlanlist.size() - 1) {
            iv_dotted_line_bottom.setVisibility(View.INVISIBLE);
            tv_km_time_plan.setVisibility(View.INVISIBLE);
        }


        iv_poi_delete.setTag(tempdestiDestinationPlanlist.get(pos).getWayPointId());
        *//*iv_poi_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Delete_Poi_Api(Integer.parseInt(iv_poi_delete.getTag().toString()), "DesPlan");
            }
        });*//*

        tv_arrival_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        //update destination trip
        int pos1 = Integer.valueOf(et_stay_time.getTag().toString());

        update_destination_plan(pos1);
        ll_add_places_to_plan.setVisibility(View.VISIBLE);
        ll_add_places_to_plan.addView(v);
    }*/


    public void set_destination_layout()
    {
        if (destinationplanList.size() > 0) {
            ll_des_plan.removeAllViews();
            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

            {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

            }




            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime());
                Date date1 = format.parse(createTripList.get(0).getEnddate()+" "+temptime);
                Date firstPoiDate=format.parse(createTripList.get(0).getEnddate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.toString());
                Log.d("System out","End time of day "+firstPoiDate.toString());
                if(c.getTimeInMillis()<=date1.getTime())
                {
                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));

                    }
                    else {

                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(0);
                }
                else
                {
                    if (destinationplanList.get(0).getStarttime() == 24.00) {
                        destinationplanList.get(0).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                destinationplanList.get(0).setDay(1);
                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

            }

            Log.d("System out","Destination poi list size "+destinationplanList.size());
           /* if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(0);*/


            Double tempDepart=destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++)
            {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out","required time  "+reqtime);
                Log.d("System out","depature time  "+destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours)
                {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(j).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart=destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out","required time  "+ destinationplanList.get(j).getDay());


            }



            noday = new ArrayList<>();
            noday.clear();

            //  noday.add(1);

            Integer day = 0;
       /* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*/
            if(destinationplanList.get(0).getDay() == 0)
            {
                day = destinationplanList.get(0).getDay();
                noday.add(destinationplanList.get(0).getDay());
            }
            for (int i = 0; i < destinationplanList.size(); i++) {

                destinationplanList.get(i).setPos(i);
                try {
                    if (createTripList.get(0).getEnddate().length() > 0) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                        try {
                            Date date = format.parse(createTripList.get(0).getEnddate());
                            c.setTime(date);
                            c.add(Calendar.DATE, day);
                            destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();

                        }

                    }
                } catch (Exception e) {
                }

                Log.d("days","pos"+day+"v2"+destinationplanList.get(i).getDay());

                if (day != destinationplanList.get(i).getDay() ) {
                    day = destinationplanList.get(i).getDay();
                    noday.add(destinationplanList.get(i).getDay());

                }


            }

            Log.d("System out","Reponse of set time of days count "+noday.size());
            for (int j = 0; j < noday.size(); j++) {
                AddDayWisePlan(j);
            }

         /*   if (noday.size() > 0)
                if (noday.size() == 1) {
                    tv_no_days.setText(noday.size() + " Day");
                } else {
                    tv_no_days.setText(noday.size() + " Days");
                }
            else
                tv_no_days.setText("");*/
            pb_dialog.dismiss();

        }
    }


    public void AddDayWisePlan(int pos) {


        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.add_day_wise_plan_layout, null);

        TextView tv_day = (TextView) v.findViewById(R.id.tv_day);
        TextView tv_des_date = (TextView) v.findViewById(R.id.tv_des_date);
        tv_des_date.setText("");
        try {

            if (createTripList.get(0).getEnddate().length() > 0) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                try {
                    Date date = format.parse(createTripList.get(0).getEnddate());
                    c.setTime(date);
                    c.add(Calendar.DATE, noday.get(pos));
                    tv_des_date.setText(format.format(c.getTime()));
                    return_excepted_date=format.format(c.getTime());

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tv_des_date.setText("");
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
            tv_des_date.setText("");

        }
        Log.d("System out","day pos "+noday.get(pos));
        if (noday.size() > 0) {
            if (noday.get(0).intValue() == 0)
                tv_day.setText("Day " + (noday.get(pos) + 1));
            else
                tv_day.setText("Day " + noday.get(pos));
        }
        else
        {
            tv_day.setText("Day " + noday.get(pos));
        }
        ll_add_places_to_plan = (LinearLayout) v.findViewById(R.id.ll_add_places_to_plan);

        //  ll_day_wise_plan.removeAllViews();
        //  ll_add_places_to_plan.removeAllViews();

        //set time


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == noday.get(pos)) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        if (tempdestiDestinationPlanlist.size() > 0) {
            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            //  SimpleDateFormat format = new SimpleDateFormat("HH:mm");

            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                // Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime());
                Date date1 = format.parse(createTripList.get(0).getEnddate()+" "+temptime);
                Date firstPoiDate=format.parse(createTripList.get(0).getEnddate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime() && pos==0)
                {

                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY) + "." + ((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                    }
                    else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }



                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                }
                else
                {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                tv_des_date.setText("");
            }

            Double tempDepart=tempdestiDestinationPlanlist.get(0).getArrivalTime();


            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempDepart) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    //      dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                    if (tempdestiDestinationPlanlist.get(j).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(07.00);
                    } else {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(destinationplanList.get(j).getStarttime());
                    }
                    tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());

                    tempDepart=tempdestiDestinationPlanlist.get(j).getArrivalTime();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }

        }

        ll_add_places_to_plan.removeAllViewsInLayout();
        for (int i = 0; i < tempdestiDestinationPlanlist.size(); i++) {

            AddPlacesToPlan(i);
        }
        ll_des_plan.addView(v);
    }
    public void AddPlacesToPlan(final int pos) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v;
        v = inflater.inflate(R.layout.add_places_to_plan_layout, null);
        ImageView iv_dotted_line = (ImageView) v.findViewById(R.id.iv_dotted_line);
        iv_dotted_line_bottom = (ImageView) v.findViewById(R.id.iv_dotted_line_bottom);
       TextView tv_km_time_plan = (TextView) v.findViewById(R.id.tv_km_time_plan);
        final TextView  et_stay_time = (TextView) v.findViewById(R.id.et_stay_time);
        final TextView tv_hrs=(TextView)v.findViewById(R.id.tv_hrs);
        LinearLayout   ll_edit_stay = (LinearLayout) v.findViewById(R.id.ll_bg_edit);
        LinearLayout ll_edit_start_time = (LinearLayout) v.findViewById(R.id.ll_bg_edit_starttime);
        TextView tv_start_end_time = (TextView) v.findViewById(R.id.tv_start_end_time);
        TextView tv_des_name = (TextView) v.findViewById(R.id.tv_des_name);
        final CircleImageView fc_poi_image = (CircleImageView) v.findViewById(R.id.fc_poi_image);
        final ImageView iv_poi_delete = (ImageView) v.findViewById(R.id.iv_poi_delete);
       TextView tv_arrival_time = (TextView) v.findViewById(R.id.tv_arrival_time);
        TextView tv_km_time = (TextView) v.findViewById(R.id.tv_km_time_plan);

        et_stay_time.setId(tempdestiDestinationPlanlist.get(pos).getPos());
        et_stay_time.setTag(tempdestiDestinationPlanlist.get(pos).getPos());
        Log.d("edittext_pos",String.valueOf(tempdestiDestinationPlanlist.get(pos).getPos()));



        if (editIconUnselected) {
            iv_poi_delete.setVisibility(View.VISIBLE);
            ll_edit_stay.setBackground(getResources().getDrawable(R.drawable.textbox_edit));



            //   et_stay_time.setEnabled(true);

            if (pos == 0) {
                //  ll_edit_start_time.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
                tv_arrival_time.setEnabled(false);
                ll_edit_start_time.setBackground(null);

            }
            else {
                ll_edit_start_time.setBackground(null);
                tv_arrival_time.setEnabled(false);
            }
        } else {
            iv_poi_delete.setVisibility(View.GONE);
            ll_edit_stay.setBackground(null);
            ll_edit_start_time.setBackground(null);
            //  et_stay_time.setEnabled(false);
        }


        ll_edit_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_stay_time.performClick();
            }
        });
       /* ll_edit_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // et_stay_time.performClick();
                main_pos=view.getId();
                tpd1.show(getActivity().getFragmentManager(), "Timepickerdialog1");

                //et_stay_time=(EditText)v.findViewById(tempdestiDestinationPlanlist.get(pos).getPos());
               // setValue.performClick();

            }
        });*/














        double roundOff1 = Math.round(tempdestiDestinationPlanlist.get(pos).getKM() / 1000 * 100.0) / 100.0;
        // tv_km_time.setText(roundOff1 + " Kms / " + Constants.convert_minute_hrs_both(tempdestiDestinationPlanlist.get(pos).getTime()));
        tv_km_time.setText(roundOff1 + " Kms ");


        DecimalFormat f = new DecimalFormat("##.00");
        try {
            tv_arrival_time.setText(Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
        } catch (ParseException e) {
            e.printStackTrace();

        }
        et_stay_time.setText(tempdestiDestinationPlanlist.get(pos).getStayTime().toString());


        tv_des_name.setText(tempdestiDestinationPlanlist.get(pos).getPoiName());


        // Log.d("starttime","name"+destinationplanList.get(pos).getPoiName()+"time"+f.format(destinationplanList.get(pos).getStarttime()));


        if (tempdestiDestinationPlanlist.get(pos).getStarttime() == 24.00)
        {
            tv_start_end_time.setText("24 hrs");

        }
        else {
            String starttime = "0:0", endtime = "0:0";

            try {

                starttime = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getStarttime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");
                endtime = Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(pos).getEndtime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            } catch (ParseException e) {
                e.printStackTrace();
                starttime = "0:0";
                endtime = "0:0";
            }

            tv_start_end_time.setText(starttime + " - " + endtime);


        }

        Log.d("imageName",tempdestiDestinationPlanlist.get(pos).getImagename());
        Bitmap bitmap = new ImageSaver(getActivity()).
                setFileName(tempdestiDestinationPlanlist.get(pos).getImagename()).
                setExternal(true).
                load();

        if (bitmap != null)
            fc_poi_image.setImageBitmap(bitmap);


        if (pos > 0) {
            iv_dotted_line.setVisibility(View.VISIBLE);
        }
        if (pos == tempdestiDestinationPlanlist.size() - 1) {
            iv_dotted_line_bottom.setVisibility(View.INVISIBLE);
            tv_km_time_plan.setVisibility(View.INVISIBLE);
        }
        iv_poi_delete.setTag(tempdestiDestinationPlanlist.get(pos).getWayPointId());




        //update destination trip
        int pos1=Integer.valueOf(et_stay_time.getTag().toString());
        update_destination_plan(pos1);

        ll_add_places_to_plan.addView(v);
    }




    public void update_destination_plan(int pos) {

        try {


            String triparrival = destinationplanList.get(pos).getPoiDate() + " " + destinationplanList.get(pos).getArrivalTime().toString().replace(".", ":") + ":00";
            String tripdeparture = destinationplanList.get(pos).getPoiDate() + " " + destinationplanList.get(pos).getDepartureTime().toString().replace(".", ":") + ":00";


            try {
                triparrival = Constants.formatDate(triparrival, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                Log.d("resumedate1", triparrival);
                triparrival = "";

            }

            try {
                tripdeparture = Constants.formatDate(tripdeparture, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripdeparture = "";
            }


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String json = "";
            if (triparrival.length() > 0)
                json = "[{     \"TripWayType\": \"" + destinationplanList.get(pos).getPoitype() + "\",     \"TripWayRefId\": \"" + destinationplanList.get(pos).getPoiServerId() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"" + triparrival + "\" ,\"TripEstOutTime\":\"" + tripdeparture + "\",\"TripDesId\":" + destinationplanList.get(pos).getPoiServerId() + ", \"TripIsPause\":\"false\" ,\"TripDistanceInKm\":" + destinationplanList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + destinationplanList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + false + ",\"TripSpendTime\":" + destinationplanList.get(pos).getStayTime() + ",\"TripWayPointId\":" + destinationplanList.get(pos).getWayPointId() + ",\"TripWayDayNo\":" + destinationplanList.get(pos).getDay() + ",\"TripIsNightStayOn\":" + false + "}]";
            else
                json = "[{     \"TripWayType\": \"" + destinationplanList.get(pos).getPoitype() + "\",     \"TripWayRefId\": \"" + destinationplanList.get(pos).getPoiServerId() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripDesId\":" + destinationplanList.get(pos).getPoiServerId() + ", \"TripIsPause\":\"false\" ,\"TripDistanceInKm\":" + destinationplanList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + destinationplanList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + false + ",\"TripSpendTime\":" + destinationplanList.get(pos).getStayTime() + ",\"TripWayPointId\":" + destinationplanList.get(pos).getWayPointId() + ",\"TripWayDayNo\":" + destinationplanList.get(pos).getDay() + ",\"TripIsNightStayOn\":" + false + "}]";

            Log.d("json_update_poi_plan1", json);

            Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
            call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
                @Override
                public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {


                }

                @Override
                public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();


                }
            });
        } catch (Exception e) {
        }

    }

    private void getRo(String latitude, String longitude, Integer Roid) {


        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String Json = "[{     \"ROMasterId\": \"0\", \"radius\": \"999999\",  \"PageNo\":1,\"PageSize\":100 ,  \"ROLatitude\":\"" + latitude + "\", \"ROLongitude\":\"" + longitude + "\"}]";
        Log.d("Rojson", Json);
        Call<ArrayList<GetRO>> call = apiService.ListRo(Json);
        call.enqueue(new Callback<ArrayList<GetRO>>() {
            @Override
            public void onResponse(Call<ArrayList<GetRO>> call, Response<ArrayList<GetRO>> response) {

                pb_dialog.dismiss();

                if (response.body().get(0).getResStatus() == true) {

                    ArrayList<GetRO> rolist = new ArrayList<GetRO>();
                    rolist.clear();
                    rolist.addAll(response.body());
                    Bundle b = new Bundle();
                    b.putString("facilitytype", "r");
                    String json = gson.toJson(rolist.get(0));
                    b.putString("rolist", json);
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new FacilitiesFragment(), b);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetRO>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
            }
        });
    }




    public void add_data_for_push_notification_for_Ro(Integer return_trip_plan) {
        dbHandler.delete_All_Push();

        for (int i = 0; i < onAdminRouteRoLists.size(); i++) {
            PushNotification pushNotification = new PushNotification();
            pushNotification.setName(onAdminRouteRoLists.get(i).getROName());
            pushNotification.setType("RO");
            pushNotification.setFlag(false);
            pushNotification.setLatitude(Double.valueOf(onAdminRouteRoLists.get(i).getROLatitude()));
            pushNotification.setLongitude(Double.valueOf(onAdminRouteRoLists.get(i).getROLongitude()));

            dbHandler.add_Notification_into_Table(pushNotification);
        }

        List<Create_Poi> create_poiList1 = new ArrayList<>();
        create_poiList1 = dbHandler.get_POI_Into_Cart_By_TripId(createTripList.get(0).getTripId(), return_trip_plan);
        for (int i = 0; i < create_poiList1.size(); i++) {

            PushNotification pushNotification = new PushNotification();
            pushNotification.setName(create_poiList1.get(i).getPoiName());
            pushNotification.setType(create_poiList1.get(i).getPoitype());
            pushNotification.setFlag(false);
            pushNotification.setLatitude(Double.valueOf(create_poiList1.get(i).getPoiLatitude()));
            pushNotification.setLongitude(Double.valueOf(create_poiList1.get(i).getPoiLongitude()));

            dbHandler.add_Notification_into_Table(pushNotification);
        }
        pushNotificationList = new ArrayList<>();
        pushNotificationList = dbHandler.get_push_notification_list();

    }

    public void Onrouteornot()
    {



        Boolean flag=false;
        Calendar calendar1=Calendar.getInstance();
        String current_time=sdf.format(calendar1.getTime());
        if(current_time_offRoute.length()==0)
        {
            flag=true;
        }
        else if(current_time_offRoute.length()>1)
        {
            Calendar calendar=Calendar.getInstance();
            try {
                calendar.setTime(sdf.parse(current_time_offRoute));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.add(Calendar.MINUTE,30);


            flag=CheckDates1(current_time,sdf.format(calendar.getTime()));
        }



        if(flag)
        {
          /*  int[] car_pos_step = get_Step_current_pos(car.getPosition().getLatitude(), car.getPosition().getLongitude(), offroute_radius_meter);

            if (car_pos_step[0] == -1 && car_pos_step[1] == -1)
            {*/
            iv_directions.setVisibility(View.GONE);
            ForgroundPush("Oops! Please return to the planned route, so that Happy Roads can assist you better.", "route");
            if (voice_flag) {
                PushvoiceMsg("Oops!you are on the wrong route!");
            }

            current_time_offRoute=current_time;
           /* }*/
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(getActivity(), ExploreActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        RemoteViews expandedView = new RemoteViews(getActivity().getPackageName(),
                R.layout.custom_notification);
        expandedView.setTextViewText(R.id.tv_trip_name, "Happy Roads");
        expandedView.setTextViewText(R.id.tv_trip_date, messageBody);

        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(largeIcon)
                .setContentTitle("Happy Roads")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setCustomBigContentView(expandedView)
                .setContentText(messageBody)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    public void sendPoiPush() {


        if (map.getMyLocation() != null)
        {
            for (int i = 0; i < pushNotificationList.size(); i++) {
                //     double distanceofmarkers = distance(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), pushNotificationList.get(i).getLatitude(), pushNotificationList.get(i).getLongitude());
                double distanceofmarkers = distance(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), pushNotificationList.get(i).getLatitude(), pushNotificationList.get(i).getLongitude());

                if ((distanceofmarkers * 1000) < push_radius_meter && !pushNotificationList.get(i).getFlag()) {
                    DecimalFormat df = new DecimalFormat("#.##");
                    String pushdistance = df.format(distanceofmarkers);
                    Log.d("pushdistance", String.valueOf(distanceofmarkers) + "value " + distanceofmarkers + "dis" + pushdistance);

                    if (voice_flag) {
                        PushvoiceMsg("Yippi! " + pushNotificationList.get(i).getName() + " is just " + pushdistance + " Kms away");
                    }
                    ForgroundPush("Yippi! " + pushNotificationList.get(i).getName() + " is just " + pushdistance + " Kms away", pushNotificationList.get(i).getType());
                    sendNotification("Yippi! " + pushNotificationList.get(i).getName() + " is just " + pushdistance + " Kms away");
                    dbHandler.update_push_status(pushNotificationList.get(i).getId());
                    pushNotificationList.remove(i);
                    break;
                }

            }

        }


    }


    private void SetDirectionImage(String maneuver) {

        Log.d("meneuever", "directionicon " + maneuver);
        try {
            iv_directions.setVisibility(View.VISIBLE);
            if (maneuver.length() == 0)
            {
                iv_directions.setVisibility(View.GONE);
            }
            else if (maneuver.equalsIgnoreCase("turn-sharp-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_sharp_left));
            } else if (maneuver.equalsIgnoreCase("uturn-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_sharp_right));
            } else if (maneuver.equalsIgnoreCase("turn-slight-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_slight_right));
            } else if (maneuver.equalsIgnoreCase("roundabout-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_traffic_circle_left));
            } else if (maneuver.equalsIgnoreCase("roundabout-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_traffic_circle_right));
            } else if (maneuver.equalsIgnoreCase("uturn-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_sharp_left));
            } else if (maneuver.equalsIgnoreCase("turn-slight-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_slight_left));
            } else if (maneuver.equalsIgnoreCase("turn-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_left));
            } else if (maneuver.equalsIgnoreCase("ramp-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_ramp));
            } else if (maneuver.equalsIgnoreCase("turn-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_right));
            } else if (maneuver.equalsIgnoreCase("fork-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_fork_right));
            } else if (maneuver.equalsIgnoreCase("straight")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_new_name_straight));
            } else if (maneuver.equalsIgnoreCase("fork-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_fork_left));
            } else if (maneuver.equalsIgnoreCase("turn-sharp-right")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_turn_sharp_right));
            } else if (maneuver.equalsIgnoreCase("ramp-left")) {
                iv_directions.setImageDrawable(getResources().getDrawable(R.drawable.directions_ramp));
            } else {
                iv_directions.setVisibility(View.GONE);
                iv_directions.setImageResource(0);
            }
        } catch (Resources.NotFoundException e) {
            iv_directions.setVisibility(View.GONE);
        }


    }


    public void showStayDialog() {

        if(handler!=null) {
            handler.removeCallbacks(runnable);
        }

        String tripstatus = "";
        if (createTripList.get(0).getReturnstatus() && return_plan==0)
        {
            //   dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), "UpComing");
            tripstatus = "OnGoing";
        } else {
            //  dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), "Completed");
            tripstatus = "Completed";
        }


        dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), tripstatus,1);



        preferences.edit().putBoolean("trip_start_tracking", false).commit();
        preferences.edit().putString("DirectionJson", "").commit();

        preferences.edit().putInt("tripid", 0).commit();

        NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();




        myTripDialog = new Dialog(getActivity());

        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_start_tracking);
        myTripDialog.setCancelable(false);
        TextView tv_yes = (TextView) myTripDialog.findViewById(R.id.tv_start_tracking);


        TextView tv_complete_msg = (TextView) myTripDialog.findViewById(R.id.tv_trip_start_text);
        if(return_plan==0) {
            if(createTripList.get(0).getReturnstatus()) {
                tv_complete_msg.setText("Navigation completed. Have a nice day");
                tv_yes.setText("View my trip");
            }else
            {
                tv_complete_msg.setText("Navigation completed. Thanks for using Happy Roads.");
                tv_yes.setText("Exit");
            }

        }else
        {
            tv_complete_msg.setText("Navigation completed. Thanks for using Happy Roads.");
            tv_yes.setText("Exit");
        }
        //tv_yes.setText("Yes");
        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myTripDialog.dismiss();

             /*   Intent i = new Intent(getActivity(), ExploreActivity.class);
                startActivity(i);
                getActivity().finish();
             */


                NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                nMgr.cancelAll();
                if(return_plan==1)
                    ((ExploreActivity) getActivity()).replace_fragmnet(new DiscoverFragment());
                else
                {
                    if(createTripList.get(0).getReturnstatus())
                    {
                        Bundle b = new Bundle();
                        b.putInt("tab_pos",1);
                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyTripWithSwipableFragment(),b);

                    }
                    else
                    {
                        ((ExploreActivity) getActivity()).replace_fragmnet(new DiscoverFragment());
                    }

                }



                //android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);

        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
                NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                nMgr.cancelAll();
                if(return_plan==1)
                {
                    Bundle b = new Bundle();
                    b.putInt("tab_pos", 2);
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyTripWithSwipableFragment(),b);
                }
                else
                {
                    if(createTripList.get(0).getReturnstatus())
                    {
                        Bundle b = new Bundle();
                        b.putInt("tab_pos", 1);
                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyTripWithSwipableFragment(),b);

                    }
                    else
                    {
                        Bundle b = new Bundle();
                        b.putInt("tab_pos", 2);
                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyTripWithSwipableFragment(),b);                    }

                }
            }
        });

        myTripDialog.show();


    }

    public void ForgroundPush(String Msg, String type) {

        frame_push_popup.setVisibility(View.VISIBLE);
        ll_push_pop.setBackgroundColor(getResources().getColor(R.color.yellow));
        tv_push_msg1.setVisibility(View.GONE);
        if (type.contains("POI") || type.contains("DES")) {
            ll_push_pop.setBackgroundColor(getResources().getColor(R.color.yellow));
            circleImageView_push.setImageResource(R.drawable.poi_icon_map_slider);
            tv_push_msg.setText(Msg);
            tv_push_msg.setTextColor(getResources().getColor(R.color.black));
            iv_push_close.setImageResource(R.drawable.close_icon_map_popup);
        } else if (type.equalsIgnoreCase("RO")) {
            ll_push_pop.setBackgroundColor(getResources().getColor(R.color.yellow));
            circleImageView_push.setImageResource(R.drawable.map_slider_icon4_selected);
            tv_push_msg.setText(Msg);
            tv_push_msg.setTextColor(getResources().getColor(R.color.black));
            iv_push_close.setImageResource(R.drawable.close_dropdown_destination);

        } else if (type.contains("route")) {
            ll_push_pop.setBackgroundColor(getResources().getColor(R.color.pink));
            circleImageView_push.setImageResource(R.drawable.alert_icon);
            tv_push_msg.setText(Msg);
            tv_push_msg.setTextColor(getResources().getColor(R.color.colorWhite));
            iv_push_close.setImageResource(R.drawable.close_btn_popup);
          //  tv_push_msg1.setVisibility(View.VISIBLE);

        } else {
            ll_push_pop.setBackgroundColor(getResources().getColor(R.color.yellow));
            circleImageView_push.setImageResource(R.drawable.poi_icon_map_slider);
            tv_push_msg.setText(Msg);
            tv_push_msg.setTextColor(getResources().getColor(R.color.black));
            iv_push_close.setImageResource(R.drawable.close_icon_map_popup);
        }

        iv_push_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                frame_push_popup.startAnimation(bottomUpDown);
                frame_push_popup.setVisibility(View.GONE);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (frame_push_popup.getVisibility() == View.VISIBLE) {
                    try {
                        iv_push_close.performClick();
                    } catch (Exception e) {
                    }
                }
            }
        }, 20000);

    }


    public void PushvoiceMsg(String str) {
        Log.d("directionmsg","msg "+str);
        ttobj.speak(str, TextToSpeech.QUEUE_FLUSH, null);
    }



    public void getdata_form_offline() {


        if (preferences.getString("DirectionJson", "").length() > 1)

        {

            try {
                onAdminRouteRoLists = new ArrayList<RoutePojo.OnAdminRoutePoiList>();

                String jsonOutput = preferences.getString("rolist", "");

                Type RolistType = new TypeToken<ArrayList<RoutePojo.OnAdminRoutePoiList>>() {
                }.getType();

                onAdminRouteRoLists = gson.fromJson(jsonOutput, RolistType);

                onAdminRoutePoiLists = new ArrayList<RoutePojo.OnAdminRoutePoiList>();


                jsonOutput = preferences.getString("poilist", "");
                RolistType = new TypeToken<ArrayList<RoutePojo.OnAdminRoutePoiList>>() {
                }.getType();

                onAdminRoutePoiLists = gson.fromJson(jsonOutput, RolistType);
            }catch (Exception e){}

            String routeresponse = preferences.getString("DirectionJson", null);
            route_direction = new Direction();
            route_direction = gson.fromJson(routeresponse, Direction.class);

            setDecodeLatLongList();

            DirectionsJSONParser parser = new DirectionsJSONParser();

            // Starts parsing data
            Gson gson = new GsonBuilder().create();

            try {
                routelineresult = parser.parse(new JSONObject(routeresponse));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (return_plan == 1)
            {


                source.setLatitude(createTripList.get(0).getDestination_latitude());
                source.setLongitude(createTripList.get(0).getDestination_longitude());
                destination.setLatitude(createTripList.get(0).getSource_latitude());
                destination.setLongitude(createTripList.get(0).getSource_longitude());


            } else {


                source.setLatitude(createTripList.get(0).getSource_latitude());
                source.setLongitude(createTripList.get(0).getSource_longitude());

                if(createTripList.get(0).getTripGooglePlaceLat()>0 && createTripList.get(0).getDes_address().length()>1)
                {
                    destination.setLatitude(createTripList.get(0).getTripGooglePlaceLat());
                    destination.setLongitude(createTripList.get(0).getTripGooglePlaceLong());
                    complete_stay_radius_meter=40;
                }
                else {
                    destination.setLatitude(createTripList.get(0).getDestination_latitude());
                    destination.setLongitude(createTripList.get(0).getDestination_longitude());

                }
            }
            try {
                if (preferences.getString("source_lat", "").toString().length() > 1) {
                    source.setLatitude(Double.valueOf(preferences.getString("source_lat", "0.0").toString()));
                    source.setLongitude(Double.valueOf(preferences.getString("source_long", "0.0").toString()));
                }
            }catch (Exception e){Log.d("source_lat_long_error",e.toString());}


            //add_data_for_push_notification_for_Ro(return_plan);


            set_arrival_date_time_step();

            running_car_point1();



            listServiceCategoryList = new ArrayList<>();

            String jsonOutput1 = preferences.getString("listofservices", "");
            Type listofservices = new TypeToken<ArrayList<ListServiceCategory>>() {
            }.getType();
            listServiceCategoryList = gson.fromJson(jsonOutput1, listofservices);

            for (int i = 0; i < listServiceCategoryList.size(); i++)
            {
                listServiceCategoryList.get(i).setStatus(false);
                listServiceCategoryList.get(i).setMarker_flag(false);
            }

            serviceListAdapter.notifyDataSetChanged();
            add_marker_plan(return_plan);
            set_map_position();
       //     setTrackingMode();


        } else {
            Constants.show_error_popup(getActivity(), "Check internet connection. ", iv_bottom_up_arrow);
        }


    }


    public void update_trip_one_way_completed(Integer tripid, Boolean tripisonesidecompleted) {
        try {
            String tripstatus = "";
            if (createTripList.get(0).getReturnstatus() && return_plan==0)
            {
                //   dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), "UpComing");
                tripstatus = "OnGoing";
            } else {
                //  dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), "Completed");
                tripstatus = "Completed";
            }

            String json = "";

            json = "[{     \"TripId\": \"" + tripid + "\", \"TripIsOneSideCompleted\":" + tripisonesidecompleted + ",\"TripStatus\":\"" + tripstatus + "\"}]";

            Log.d("oneside_trip_completed", json);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ArrayList<TripCompleted>> call = apiService.UpdateTripToOneSideCompleted(json);
            final String finalTripstatus = tripstatus;
            call.enqueue(new Callback<ArrayList<TripCompleted>>() {
                @Override
                public void onResponse(Call<ArrayList<TripCompleted>> call, final Response<ArrayList<TripCompleted>> response) {

                    showStayDialog();

                }

                @Override
                public void onFailure(Call<ArrayList<TripCompleted>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    showStayDialog();


                }
            });
        } catch (Exception e) {
        }
    }


    private void getRoute(String url) {

        ApiInterface apiService = ApiClient.get_retrofit_client_google().create(ApiInterface.class);
        Call<Direction> call = apiService.GetRoute(url);
        call.enqueue(new Callback<Direction>() {
            @Override
            public void onResponse(Call<Direction> call, Response<Direction> response) {
                if (response.body() != null) {


                    try {
                        route_direction = new Direction();
                        route_direction = response.body();

                        DirectionsJSONParser parser = new DirectionsJSONParser();

                        // Starts parsing data
                        Gson gson = new GsonBuilder().create();
                        String routeresponse = gson.toJson(route_direction);
                        routelineresult = parser.parse(new JSONObject(routeresponse));
                        preferences.edit().putString("DirectionJson", routeresponse).commit();
                        draw_route();
                        setDecodeLatLongList();
                        set_arrival_date_time_step();
                        add_marker_step();

                        running_car_point1();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<Direction> call, Throwable t) {
                // Log error here since request failed

                try {
                    t.printStackTrace();

                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                    }
                } catch (Exception e) {
                }

            }
        });

    }

    public void set_eta_diff() {


        if (map.getMyLocation()!=null)
        {
            //  set_map_position();
            current_distance = 0;
            current_distance_start = 0;

            int[] car_pos_step = get_Step_current_pos(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), 25);


            if(car_pos_step[0]==-1 && car_pos_step[1]==-1)
            {
                Onrouteornot();
            }

            else

            {


                Calendar c=Calendar.getInstance();
                Log.d("car_pos_Step", car_pos_step[0] + " step " + car_pos_step[1]+" time "+sdf.format(c.getTime()));

                for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++) {


                    for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                        if (car_pos_step[2] == m && car_pos_step[3] == k)
                        {




                            double distanceof_poi_end = distance(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getEndLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getEndLocation().getLng());

                            //     double distanceof_poi_start = distance(car.getPosition().getLatitude(), car.getPosition().getLongitude(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLng());


                            try {


                                // direction msg set icon

                                Integer distance_check_end_step = 500;


                                String nextMsg = String.valueOf(Html.fromHtml(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getHtmlInstructions()));


                                if (route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getManeuver().toString().length() == 0 && voice_flag && route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getVoice_flag() == false) {


                                    Log.d("car_voice_Step", m + " step " + k+" time "+sdf.format(c.getTime()));

                                    PushvoiceMsg(nextMsg);
                                    route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).setVoice_flag(true);


                                }
                                if (k + 1 < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size()) {
                                    if (route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k + 1).getVoice_flag() == false && voice_flag && distanceof_poi_end * 1000 <= distance_check_end_step) {

                                        PushvoiceMsg(nextMsg);
                                        route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k + 1).setVoice_flag(true);
                                    }
                                }


                                if (k + 1 < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size()) {
                                    if (route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k + 1).getManeuver().toString().length() > 1 && distanceof_poi_end * 1000 <= distance_check_end_step) {

                                        SetDirectionImage(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k + 1).getManeuver());
                                    } else {
                                        SetDirectionImage("");
                                    }

                                } else {
                                    SetDirectionImage("");
                                }


                            } catch (Exception e) {

                                e.printStackTrace();
                            }

                        }


                        if (car_pos_step[0] == m && car_pos_step[1] == k)
                        {


                            Boolean update_ETA = true;
                            Date d1 = null;
                            Date d2 = null;
                            long diff_seconds = 0;

                            double distanceof_poi_end = distance(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getEndLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getEndLocation().getLng());

                            //     double distanceof_poi_start = distance(car.getPosition().getLatitude(), car.getPosition().getLongitude(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLng());

                            // find difference current position

                            current_distance_start = distance(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLng());

                            current_distance = current_distance + current_distance_start;

                            Double remaining_km = route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDistance().getValue() - current_distance_start * 1000;
                            double remaining_duration = (route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDuration().getValue() * remaining_km) / route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDistance().getValue();


                            Calendar arraival_Cal = Calendar.getInstance();
                            try {
                                d1 = sdf_second.parse(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getArrivalDate());

                                arraival_Cal.setTime(d1);
                                arraival_Cal.add(Calendar.SECOND, route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDuration().getValue().intValue());

                                d1 = sdf_second.parse(sdf_second.format(arraival_Cal.getTime()));
                                Calendar calendar = Calendar.getInstance();
                                calendar.add(Calendar.SECOND, (int) remaining_duration);
                                d2 = sdf_second.parse(sdf_second.format(calendar.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            // Get msec from each, and subtract.
                            long diff = d1.getTime() - d2.getTime();

                            diff_seconds = TimeUnit.MILLISECONDS.toSeconds(diff);


                            for (int j = 0; j < tempcreatePoiList.size(); j++)
                            {


                                Log.d("distance", "car_array leg->" + car_pos_step[0] + " step-> " + car_pos_step[1] + " poi leg " + tempcreatePoiList.get(j).getLegNo() + " step" + tempcreatePoiList.get(j).getStepNo() + " distance-> " + distanceof_poi_end * 1000);

                                //   double distanceof_poi_car = distance(car.getPosition().getLatitude(), car.getPosition().getLongitude(), tempcreatePoiList.get(j).getPoiLatitude(), tempcreatePoiList.get(j).getPoiLongitude());

                                if ((tempcreatePoiList.get(j).getLegNo() == car_pos_step[0] && tempcreatePoiList.get(j).getStepNo() == car_pos_step[1])) {

                                    if ((distanceof_poi_end * 1000) < 500) {

                                        try {
                                            d1 = sdf_second.parse(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getArrivalDate());
                                            arraival_Cal = Calendar.getInstance();
                                            arraival_Cal.setTime(d1);
                                            double diff_minute1 = diff_seconds > 0 ? (double) -diff_seconds : (double) diff_seconds;
                                            arraival_Cal.add(Calendar.SECOND, (int) diff_minute1);
                                            d1 = sdf_second.parse(sdf_second.format(arraival_Cal.getTime()));
                                            Calendar calendar = Calendar.getInstance();
                                            d2 = sdf_second.parse(sdf_second.format(calendar.getTime()));
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        // Get msec from each, and subtract.
                                        long diff1 = d1.getTime() - d2.getTime();

                                        long diff_seconds1 = TimeUnit.MILLISECONDS.toSeconds(diff1);
                                        double stay_hrs = (double) diff_seconds1 / 3600;
                                        stay_hrs = stay_hrs > 0 ? stay_hrs : 0.0;

                                        dbHandler.update_stay_km_by_PoiId(tempcreatePoiList.get(j).getWayPointId(), Double.valueOf(df.format(stay_hrs)));
                                        dbHandler.update_poi_track_flag(tempcreatePoiList.get(j).getWayPointId(), true);

                                        Log.i("ok", "stay_hrs" + tempcreatePoiList.get(j).getPoiName() + "distance " + df.format(stay_hrs) + "stay hrs" + stay_hrs + "arrival" + sdf_second.format(d1.getTime()) + " ctime " + sdf_second.format(d2.getTime()));
                                        set_arrival_date_time_step();
                                        get_my_trip();
                                        update_ETA = false;
                                        return;
                                    }

                                }


                                if (tempcreatePoiList.get(j).getLegNo() >= car_pos_step[0] && tempcreatePoiList.get(j).getStepNo() >= car_pos_step[1]) {

                                    double diff_minute1 = diff_seconds > 0 ? (double) -diff_seconds : Math.abs((double) diff_seconds);
                                    tempcreatePoiList.get(j).setPoi_diff_seconds(diff_minute1);
                                    dbHandler.update_trip_poi_eta_diff_seconds(tempcreatePoiList.get(j).getWayPointId(), diff_minute1);
                                    get_my_trip();
                                    update_ETA = false;

                                    return;
                                }

                                if (tempcreatePoiList.get(j).getLegNo() < car_pos_step[0] && tempcreatePoiList.get(j).getPoi_track_flag() == false) {

                                    dbHandler.update_poi_track_flag(tempcreatePoiList.get(j).getWayPointId(), true);
                                    dbHandler.update_stay_km_by_PoiId(tempcreatePoiList.get(j).getWayPointId(), 0.0);
                                    dbHandler.update_trip_poi_eta_diff_seconds(tempcreatePoiList.get(j).getWayPointId(), 0.0);
                                    tempcreatePoiList.get(j).setPoi_diff_seconds(0.0);

                                    set_arrival_date_time_step();
                                    get_my_trip();
                                    update_ETA = false;


                                    return;
                                }


                            }


                            route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).setCurrentDistance(current_distance_start);


                            //destination
                            if (update_ETA)
                                update_eta_Date((double) diff_seconds);

                        }


                    }
                }
            }
        }
        else {
            Constants.show_error_popup(getActivity(), "To continue, turn on GPS. ", iv_bottom_up_arrow);

        }


    }

    public void update_eta_Date(Double seconds) {

        seconds = seconds >= 0 ? -seconds : Math.abs(seconds);

        dbHandler.update_trip_eta_diff_seconds(tripid, seconds, return_plan);
        get_my_trip();
    }

    private void PlacesBetweenStoD1(final int pos) {

        //set Arrival departure date and time pos =0
        if (pos == 0) {


            String tripStartDate = "";

            if (return_plan == 0)
                tripStartDate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime();
            else
                tripStartDate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time();


            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(tempcreatePoiList.get(0).getTime() + tempcreatePoiList.get(0).getPoi_diff_seconds());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);
                calendar.add(Calendar.SECOND, convertime[2]);

                tempcreatePoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));
                tempcreatePoiList.get(pos).setArrivalTime(Double.valueOf(sdfTime.format(calendar.getTime()).replace(":", ".")));
                tempcreatePoiList.get(pos).setPoiDate(sdfDate.format(calendar.getTime()));

                if (tempcreatePoiList.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((tempcreatePoiList.get(pos).getStayTime() - Math.floor(tempcreatePoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, tempcreatePoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);


                    tempcreatePoiList.get(pos).setPoiDate(sdfDate.format(calendar.getTime()));
                    tempcreatePoiList.get(pos).setDepartureTime(Double.valueOf(sdfTime.format(calendar.getTime()).replace(":", ".")));
                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdfDate.format(calendar.getTime()) + " " + sdfTime.format(calendar.getTime()));
                } else {
                    date = sdf.parse(tempcreatePoiList.get(pos).getResumeDate() + " " + tempcreatePoiList.get(pos).getResumeTime());

                    calendar.setTime(date);


                    tempcreatePoiList.get(pos).setPoiDate(sdfDate.format(calendar.getTime()));
                    tempcreatePoiList.get(pos).setDepartureTime(Double.valueOf(sdfTime.format(calendar.getTime()).replace(":", ".")));
                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdfDate.format(calendar.getTime()) + " " + sdfTime.format(calendar.getTime()));


                }

            } catch (ParseException e) {
                e.printStackTrace();

            }
        }


        //Log.d("arrivalTime","ar"+createPoiList.get(pos).getArrivalTime()+"de"+createPoiList.get(pos).getDepartureTime()+"re"+createPoiList.get(pos).getReqtime());
        //set arrival date and time
        if (pos >= 1 && (pos <= tempcreatePoiList.size() - 1)) {


            int position = pos - 1;

            String tripStartDate = tempcreatePoiList.get(position).getPoiDate() + " " + String.valueOf(tempcreatePoiList.get(position).getDepartureTime()).replace(".", ":");


            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                // int[] convertime=  splitToComponentTimes(createPoiList.get(position).getTime());
                Double requriedtime = tempcreatePoiList.get(position).getReqtime();
                int[] convert_h_m_s = splitToComponentTimes(requriedtime);
                calendar.add(Calendar.HOUR, convert_h_m_s[0]);
                calendar.add(Calendar.MINUTE, convert_h_m_s[1]);
                calendar.add(Calendar.SECOND, convert_h_m_s[2]);

                tempcreatePoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));

                tempcreatePoiList.get(pos).setArrivalTime(Double.valueOf(sdfTime.format(calendar.getTime()).replace(":", ".")));

                if (tempcreatePoiList.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((tempcreatePoiList.get(pos).getStayTime() - Math.floor(tempcreatePoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, tempcreatePoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);


                    tempcreatePoiList.get(pos).setPoiDate(sdfDate.format(calendar.getTime()));
                    tempcreatePoiList.get(pos).setDepartureTime(Double.valueOf(sdfTime.format(calendar.getTime()).replace(":", ".")));
                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdfDate.format(calendar.getTime()) + " " + sdfTime.format(calendar.getTime()));
                } else {
                    date = sdf.parse(tempcreatePoiList.get(pos).getResumeDate() + " " + tempcreatePoiList.get(pos).getResumeTime());

                    calendar.setTime(date);


                    tempcreatePoiList.get(pos).setPoiDate(sdfDate.format(calendar.getTime()));
                    tempcreatePoiList.get(pos).setDepartureTime(Double.valueOf(sdfTime.format(calendar.getTime()).replace(":", ".")));
                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdfDate.format(calendar.getTime()) + " " + sdfTime.format(calendar.getTime()));

                }

            } catch (ParseException e) {
                e.printStackTrace();

            }

        }

        if (tempcreatePoiList.size() == pos + 1) {

            tempcreatePoiList.get(pos).setReqtime(createTripList.get(0).getTime() + createTripList.get(0).getEta_diff_second());


        } else {

            tempcreatePoiList.get(pos).setReqtime(tempcreatePoiList.get(pos + 1).getTime() + tempcreatePoiList.get(pos + 1).getPoi_diff_seconds());

        }


    }


    // get legs & step  of current position
    public int[] get_Step_current_pos(Double currentLatitude, Double currentLongitude, Integer radius_meter)
    {

        int[] step = {-1, -1,-1,-1}; //0 for legs and 1 for step 2




        /*for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++)
        {


            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                List<LatLng> latLngList = new ArrayList<>();
                latLngList = route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDecodelatlonglist();

                if(latLngList.size()==0)
                {
                    step[0] = -2;
                    step[1] = -2;

                }

                for (int i = 0; i < latLngList.size(); i++)
                {


                    double distance1 = distance(currentLatitude, currentLongitude, latLngList.get(i).getLatitude(), latLngList.get(i).getLongitude());

                    if (distance1 * 1000 <= radius_meter)
                    {

                        step[0] = m;
                        step[1] = k;


                        break;
                    }
                }


            }
        }*/

        step=get_Step_current_pos_voice(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude());









        return step;
    }

    public int[] get_Step_current_pos_voice(Double currentLatitude, Double currentLongitude)
    {

        long startTime = System.currentTimeMillis();
        int[] step = {-2, -2,-2,-2}; //0 for legs and 1 for step 2

        List<RouteLegStep> routeLegStepList=new ArrayList<>();


        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++)
        {


            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {



                for (int i = 0; i < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDecodelatlonglist().size(); i++)
                {

                    LatLng latLng=route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDecodelatlonglist().get(i);
                    double distance1 = distance(currentLatitude, currentLongitude, latLng.getLatitude(), latLng.getLongitude());

                    RouteLegStep routeLegStep=new RouteLegStep();

                    routeLegStep.setCurrentStep(k);
                    routeLegStep.setCurrentLeg(m);
                    routeLegStep.setCurrentDistancemeter(distance1*1000);
                    routeLegStepList.add(routeLegStep);

                }


            }
        }


        Log.d("latlongsize","size->"+routeLegStepList.size());
        Comparator<RouteLegStep> comparator = new Comparator<RouteLegStep>() {
            @Override
            public int compare(RouteLegStep lhs, RouteLegStep rhs) {


                return lhs.getCurrentDistancemeter().compareTo(rhs.getCurrentDistancemeter());

            }
        };

        Collections.sort(routeLegStepList,comparator);




        if(routeLegStepList.size()>0)
        {
            Log.d("route_distance","d-> "+routeLegStepList.get(0).getCurrentDistancemeter());

            if(routeLegStepList.get(0).getCurrentDistancemeter()>=offroute_radius_meter)
            {
                step[0] = -1;
                step[1] = -1;
            }

            if(routeLegStepList.get(0).getCurrentDistancemeter()<offroute_radius_meter)
            {
                step[0] = routeLegStepList.get(0).getCurrentLeg();
                step[1] = routeLegStepList.get(0).getCurrentStep();
                Log.d("route_distance","l-> "+routeLegStepList.get(0).getCurrentLeg()+" s->"+routeLegStepList.get(0).getCurrentStep());
            }
            if(routeLegStepList.get(0).getCurrentDistancemeter()<=40)
            {
                step[2] = routeLegStepList.get(0).getCurrentLeg();
                step[3] = routeLegStepList.get(0).getCurrentStep();
            }
            Log.d("route_distance","l-> "+routeLegStepList.get(0).getCurrentLeg()+" s->"+routeLegStepList.get(0).getCurrentStep());
        }
        else
        {
            step[0]=-2;
            step[1]=-2;
            step[2]=-2;
            step[3]=-2;

        }

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        Log.d("Time_Requried","time -->"+TimeUnit.MILLISECONDS.toSeconds(elapsedTime)+" elapsed time "+elapsedTime);
        return step;
    }



    // get legs & step  of current position




    public void get_set_Step_poi() {


        remaining_km = 0.0;
        remaining_time = 0.0;

        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++) {

            remaining_km = remaining_km + route_direction.getRoutes().get(0).getLegs().get(m).getDistance().getValue();
            remaining_time = remaining_time + route_direction.getRoutes().get(0).getLegs().get(m).getDuration().getValue();

            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                for (int j = 0; j < tempcreatePoiList.size(); j++) {


                    if (tempcreatePoiList.get(j).getFlag() == false)

                    {


                        List<LatLng> latLngList = new ArrayList<>();
                        latLngList = route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDecodelatlonglist();

                        for (int i = 0; i < latLngList.size(); i++) {


                            double distance1 = distance(tempcreatePoiList.get(j).getPoiLatitude(), tempcreatePoiList.get(j).getPoiLongitude(), latLngList.get(i).getLatitude(), latLngList.get(i).getLongitude());
                            Log.d("distance_point", " dis " + distance1 * 1000);
                            if (distance1 * 1000 <= radius_meter) {


                                if (!tempcreatePoiList.get(j).getFlag()) {
                                    tempcreatePoiList.get(j).setStepNo(k);
                                    tempcreatePoiList.get(j).setLegNo(m);
                                    tempcreatePoiList.get(j).setFlag(true);
                                    dbHandler.update_trip_poi_track_time_km_google(tempcreatePoiList.get(j).getWayPointId(), remaining_km, remaining_time, m, k);
                                    remaining_km = 0.0;
                                    remaining_time = 0.0;

                                }

                                break;
                            }
                        }


                    }
                }
            }
        }
        dbHandler.update_KM_Time_Trip(tripid, remaining_km, remaining_time, return_plan);
        get_my_trip();

    }

    /**
     * Decodes an encoded path string into a sequence of LatLngs.
     */
    public List<LatLng> decode(String encodedPath) {


        int len = encodedPath.length();

        // For speed we preallocate to an upper bound on the final length, then
        // truncate the array before returning.
        final List<LatLng> path = new ArrayList<LatLng>();
        int index = 0;
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int result = 1;
            int shift = 0;
            int b;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lat += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            result = 1;
            shift = 0;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lng += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            path.add(new LatLng(lat * 1e-5, lng * 1e-5));
        }

        return path;
    }

    public void set_arrival_date_time_step() {


        tempcreatePoiList = new ArrayList<>();
        tempcreatePoiList = dbHandler.get_POI_Into_Cart_By_TripId(createTripList.get(0).getTripId(), return_plan);


        String temp_trip_start_date = "";

        if (return_plan == 0)
            temp_trip_start_date = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime() + ":00";
        else
            temp_trip_start_date = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time() + ":00";


        //using google waypoint

        get_set_Step_poi();

        if (return_plan == 0) {
            createTripList.get(0).setKM(remaining_km);
            createTripList.get(0).setTime(remaining_time);

        } else {
            createTripList.get(0).setReturnKM(remaining_km);
            createTripList.get(0).setReturnTime(remaining_time);
        }

        dbHandler.update_KM_Time_Trip(tripid, remaining_km, remaining_time, return_plan);
        tempcreatePoiList = new ArrayList<>();
        tempcreatePoiList = dbHandler.get_POI_TRIP(createTripList.get(0).getTripId(), return_plan);
        for (int i = 0; i < tempcreatePoiList.size(); i++) {
            PlacesBetweenStoD1(i);
            tempcreatePoiList.get(i).setFlag(false);


        }

        for (int i = 0; i < tempcreatePoiList.size(); i++) {
            tempcreatePoiList.get(i).setFlag(false);
            Log.d("tracking_poi", "pos " + i + " step " + tempcreatePoiList.get(i).getStepNo() + " legs" + tempcreatePoiList.get(i).getLegNo());
        }


        //set arrival date and time each step
        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++) {


            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                for (int j = 0; j < tempcreatePoiList.size(); j++) {


                    if (tempcreatePoiList.get(j).getFlag() == false)

                    {


                        try {


                            Log.d("checkdate", "temp_date " + temp_trip_start_date + " poi arrivaldate " + tempcreatePoiList.get(j).getActual_arrival_date_time() + " value " + CheckDates1(sdf.format(sdf_second.parse(temp_trip_start_date)), tempcreatePoiList.get(j).getActual_arrival_date_time()));
                            if (tempcreatePoiList.get(j).getLegNo() + 1 == m && k == 0) {

                                temp_trip_start_date = tempcreatePoiList.get(j).getActual_dep_date_time() + ":00";
                                tempcreatePoiList.get(j).setFlag(true);


                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }
                }


                int seconds = route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getDuration().getValue().intValue();


                Calendar calendar = Calendar.getInstance();
                try {
                    calendar.setTime(sdf_second.parse(temp_trip_start_date));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //   calendar.add(Calendar.MINUTE, minute);
                calendar.add(Calendar.SECOND, seconds);


                route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).setArrivalDate(temp_trip_start_date);


                temp_trip_start_date = sdf_second.format(calendar.getTime());

            }

        }
        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++) {


            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {

                Log.d("route", "leg " + m + " step " + k + " arrival date" + route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getArrivalDate());
            }
        }

    }


    public void running_car_point1() {

/*
  try {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {

                    if (i < points.size()) {

                                              getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            LatLng newPt = points.get(i);
                                                            LatLng oldPt = points.get(i - 1);

                                                            Location location = new Location("");
                                                            location.setLatitude(newPt.getLatitude());
                                                            location.setLongitude(newPt.getLongitude());

                                                            Location location1 = new Location("");
                                                            location1.setLatitude(oldPt.getLatitude());
                                                            location1.setLongitude(oldPt.getLongitude());

//                                                            Double   bearing =  angleBteweenCoordinate(location1.getLatitude(),location1.getLongitude(),location.getLatitude(),location.getLongitude());
                                                            float bearing = getBearing(new LatLng(location1.getLatitude(), location1.getLongitude()), new LatLng(location.getLatitude(), location.getLongitude()));

                                                            float b = location1.bearingTo(location);
                                                            float b1 = location.bearingTo(location1);
                                                            Log.d(TAG, "bearing---" + bearing + " bearing b oldPt-->" + b1 + " bearing b newPt-->" + b);

                                                            // Add the car marker to the map.
                                                            if (car != null) {
                                                                car.setPosition(newPt);
                                                            } else
                                                            {
                                                                car = map.addMarker(new MarkerViewOptions()
                                                                        .position(newPt)
                                                                        .anchor(0.5f, 0.5f)
                                                                        .flat(false)
                                                                        .snippet("point")
                                                                        .icon(car_icon));

                                                            }

                                                            if(zoomlevel==0)
                                                            {
                                                                CameraPosition position = new CameraPosition.Builder(map.getCameraPosition())
                                                                        .target(new LatLng(car.getPosition().getLatitude(), car.getPosition().getLongitude())) // Sets the new camera position
                                                                        .bearing(bearing)
                                                                        .tilt(60) // Set the camera tilt
                                                                        .build(); // Creates a CameraPosition from the builder

                                                                map.setCameraPosition(position);
                                                            }
//
                                                            float toRotation = (float) bearingBetweenLocations(newPt, oldPt);


                                                            //   rotateMarker(car, bearing);

                                                            double distanceofmarkers = distance(car.getPosition().getLatitude(), car.getPosition().getLongitude(), destination.getLatitude(), destination.getLongitude());
                                                            if ((distanceofmarkers * 1000) < radius_meter && complatepopup == false) {

                                                                preferences.edit().putBoolean("trip_start_tracking", false).commit();
                                                                preferences.edit().putString("DirectionJson", "").commit();

                                                                preferences.edit().putInt("tripid", 0).commit();
                                                                if (Constants.isInternetAvailable(getActivity())) {
                                                                    update_trip_one_way_completed(createTripList.get(0).getTripId(), true);
                                                                } else {
                                                                    showStayDialog();
                                                                }
                                                                complatepopup = true;
                                                            }

                                                            Log.i("system", "car handler count" + i);

                                                        }
                                                    }


                        );
                        i++;
                        if(car !=null) {
                            set_eta_diff();
                            sendPoiPush();

                        }

                        handler.postDelayed(runnable, 5000);
                    }
                }

            };

            handler.postDelayed(runnable, 0);

        } catch (Exception e) {
        }
*/

        running_car_point();
    }

    public void running_car_point() {


        try {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {

                    getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {


                                                        try {
                                                            if (map.getMyLocation() != null) {
                                                                double distanceofmarkers = distance(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude(), destination.getLatitude(), destination.getLongitude());
                                                                if (distanceofmarkers * 1000 < complete_stay_radius_meter && complatepopup == false) {

                                                                    preferences.edit().putBoolean("trip_start_tracking", false).commit();
                                                                    preferences.edit().putString("DirectionJson", "").commit();

                                                                    preferences.edit().putInt("tripid", 0).commit();


                                                                    if (Constants.isInternetAvailable(getActivity())) {
                                                                        update_trip_one_way_completed(createTripList.get(0).getTripId(), true);
                                                                    } else {
                                                                        showStayDialog();
                                                                    }
                                                                    complatepopup = true;
                                                                }

                                                            }
                                                        } catch (Exception e) {
                                                        }
                                                        Log.i("system", "car handler count" + i);

                                                    }
                                                }


                    );

                   /* if (preferences.getBoolean("trip_start_tracking", true) && current_distance > push_radius_meter) {
                        Onrouteornot();
                    }*/
               if (map != null) {
                    map.setMyLocationEnabled(true);
                }


                    GPSTracker gpsTracker=new GPSTracker(getActivity());
                    if(!gpsTracker.isGPSEnabled  )
                    {
                        try {

                            Constants.showSnackMsg(getActivity(), "To continue, turn on GPS. ", iv_bottom_up_arrow);
                        }
                        catch (Exception e){}
                    }
                    else  if (map.getMyLocation()!= null)
                    {

                        if(zoomlevel==0)
                        {
                            setMapZoom();
                        }

                        set_eta_diff();
                        sendPoiPush();
                        //    Onrouteornot();
                        if(isApplicationBroughtToBackground(getActivity()))
                        {
                            if( preferences.getBoolean("trip_start_tracking", false)) {
                                sendNotification_onResume();
                            }
                        }

                    }
                    else
                    {

                        if(gpsTracker.isGPSEnabled)
                        {
                            Constants.showSnackMsg(getActivity(), "Searching gps location.", iv_bottom_up_arrow);

                        }
                        else {

                            Constants.showSnackMsg(getActivity(), "To continue, turn on GPS. ", iv_bottom_up_arrow);
                        }

                    }


                    handler.postDelayed(runnable, 5000);
                }

            };

            handler.postDelayed(runnable, 0);






           /* dRouteHandler =new Handler();
            dRouteRunnable=new Runnable() {
                @Override
                public void run() {

                    dRouteHandler.postDelayed(dRouteRunnable,dRouteTime);
                }
            };

            dRouteHandler.postDelayed(dRouteRunnable,dRouteTime);
*/



        } catch (Exception e) {
        }


    }


    public void set_map_position() {
        try {
            if (zoomlevel == 0)
            {

                if (map != null)

                {
                    setTrackingMode();
                    if (map.getMyLocation() != null)
                    {
                        CameraPosition position = new CameraPosition.Builder()
                                .target(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude())) // Sets the new camera position
                                .zoom(max_zoom) // Sets the zoom
                                .tilt(30)
                                .build(); // Creates a CameraPosition from the builder

                        map.animateCamera(CameraUpdateFactory
                                .newCameraPosition(position), 2000);

                    }

                }
            }
            else {

                if (b1 != null && map != null) {

                    LatLngBounds bounds = b1.build();
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 250);

                    map.moveCamera(cu, null);



                    CameraPosition position = new CameraPosition.Builder()
                            .target(midPoint(source.getLatitude(), source.getLongitude(), destination.getLatitude(), destination.getLongitude())) // Sets the new camera position
                            .zoom(12)// Sets the zoom
                            .build(); // Creates a CameraPosition from the builder
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(position));


                    stopTrackingMode();
                }

            }

        } catch (NullPointerException e) {
        }
    }

    public void get_my_trip_display(Integer return_plan) {
        iv_edit_stay_time.setVisibility(View.GONE);
        createTripList_display = new ArrayList<>();
        createPoiList_display = new ArrayList<>();
        createTripList_display = dbHandler.get_TRIP_By_TripId(tripid);


        if (createTripList_display.size() > 0) {


            if (return_plan == 0) {
                tv_origin.setText(createTripList_display.get(0).getSource());
                tv_destination.setText(createTripList_display.get(0).getDestination());
                tv_start_time.setText(convert_time_12_format(createTripList_display.get(0).getStarttime()));
                tv_start_date.setText(createTripList_display.get(0).getStartdate());

                DecimalFormat df = new DecimalFormat("#.##");
                double distance = Double.valueOf(df.format(createTripList_display.get(0).getKM() / 1000));

                tv_trip_distance_time.setText(distance + " Kms / " + Constants.convert_minute_hrs_both(createTripList_display.get(0).getTime() + createTripList_display.get(0).getEta_diff_second()));

                iv_circle_source.setImageBitmap(Constants.getImage(createTripList_display.get(0).getSource_image()));

                iv_circle_destination.setImageBitmap(Constants.getImage(createTripList_display.get(0).getDestination_image()));
                add_hours_minute_display(createTripList_display.get(0).getStartdate(), createTripList_display.get(0).getStarttime(), createTripList_display.get(0).getTime() + createTripList_display.get(0).getEta_diff_second());

            } else {
                tv_origin.setText(createTripList_display.get(0).getDestination());
                tv_destination.setText(createTripList_display.get(0).getSource());

                tv_start_time.setText(convert_time_12_format(createTripList_display.get(0).getReturn_start_time()));

                tv_start_date.setText(createTripList_display.get(0).getReturn_start_date());


                DecimalFormat df = new DecimalFormat("#.##");
                double distance = Double.valueOf(df.format(createTripList_display.get(0).getKM() / 1000));


                tv_trip_distance_time.setText(distance + " Kms / " + Constants.convert_minute_hrs_both(createTripList_display.get(0).getReturnTime() + createTripList_display.get(0).getEta_return_diff_second()));


                iv_circle_source.setImageBitmap(Constants.getImage(createTripList_display.get(0).getDestination_image()));

                iv_circle_destination.setImageBitmap(Constants.getImage(createTripList_display.get(0).getSource_image()));

                add_hours_minute_display(createTripList_display.get(0).getReturn_start_date(), createTripList_display.get(0).getReturn_start_time(), createTripList_display.get(0).getReturnTime() + createTripList_display.get(0).getEta_return_diff_second());
            }

            ll_places_between_S_to_D.removeAllViews();

            createPoiList_display = dbHandler.get_POI_TRIP(createTripList.get(0).getTripId(), return_plan);


            for (int i = 0; i < createPoiList_display.size(); i++) {

                PlacesBetweenStoD_Display(i, return_plan);
            }


        }
    }

    private void PlacesBetweenStoD_Display(final int pos, final Integer return_plan) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v;

        v = inflater.inflate(R.layout.places_between_src_to_dest, null);

        ImageView iv_dotted_line = (ImageView) v.findViewById(R.id.iv_dotted_line);
        iv_dotted_line_bottom = (ImageView) v.findViewById(R.id.iv_dotted_line_bottom);
        TextView tv_poi_name = (TextView) v.findViewById(R.id.tv_poi_name);
        //    TextView tv_poi_hrs=(TextView)v.findViewById(R.id.tv_poi_hrs);
        final TextView et_stay_time = (TextView) v.findViewById(R.id.et_stay_time);
        TextView tv_departure_time = (TextView) v.findViewById(R.id.tv_departure_time);
        TextView tv_arrival_time = (TextView) v.findViewById(R.id.tv_arrival_time);
        ImageView iv_poi_delete = (ImageView) v.findViewById(R.id.iv_poi_delete);
        TextView tv_poi_distance_time = (TextView) v.findViewById(R.id.tv_poi_distance_time);
        CircleImageView fc_poi_image = (CircleImageView) v.findViewById(R.id.fc_poi_image);
        ImageView iv_poi_night_stay = (ImageView) v.findViewById(R.id.iv_poi_night_stay);
        ImageView iv_poi_map = (ImageView) v.findViewById(R.id.iv_poi_map);
        final ImageView iv_poi_up_arrow = (ImageView) v.findViewById(R.id.iv_poi_up_arrow);

        LinearLayout ll_stay_time_layout = (LinearLayout) v.findViewById(R.id.ll_stay_time_layout);
        LinearLayout ll_edit_stay_time = (LinearLayout) v.findViewById(R.id.ll_edit_stay_time);

        final LinearLayout ll_places_between_N_to_D = (LinearLayout) v.findViewById(R.id.ll_places_between_N_to_D);

        tv_poi_name.setText(createPoiList_display.get(pos).getPoiName());
        et_stay_time.setText(createPoiList_display.get(pos).getStayTime().toString());


        et_stay_time.setEnabled(false);
        ll_edit_stay_time.setBackground(null);
        iv_poi_delete.setVisibility(View.GONE);


        Bitmap bitmap;
        try {

            bitmap = new ImageSaver(getActivity()).
                    setFileName(createPoiList_display.get(pos).getImagename()).

                    setExternal(true).
                    load();
            fc_poi_image.setImageBitmap(bitmap);
        } catch (Exception e) {

        }
        double roundOff1 = Math.round(createPoiList_display.get(0).getKM() / 1000 * 100.0) / 100.0;
        tv_trip_distance_time.setText(roundOff1 + " Kms / " + Constants.convert_minute_hrs_both(createPoiList_display.get(0).getTime() + createPoiList_display.get(0).getPoi_diff_seconds()));


        if (createPoiList_display.get(pos).getPoitype().contains("DES")) {


            if (createPoiList_display.get(pos).getPoiStay() == true) {
                iv_poi_night_stay.setImageResource(R.drawable.night_halt_icon_map_slider);
                iv_poi_up_arrow.setVisibility(View.GONE);
                iv_poi_map.setVisibility(View.VISIBLE);
                iv_poi_night_stay.setVisibility(View.VISIBLE);
                iv_poi_delete.setVisibility(View.VISIBLE);
                ll_stay_time_layout.setVisibility(View.GONE);


            } else {
                iv_poi_night_stay.setImageResource(R.drawable.night_halt_icon_map_slider_1);
                iv_poi_up_arrow.setVisibility(View.GONE);
                iv_poi_map.setVisibility(View.GONE);
                iv_poi_night_stay.setVisibility(View.VISIBLE);
                iv_poi_delete.setVisibility(View.VISIBLE);
                ll_stay_time_layout.setVisibility(View.VISIBLE);

            }
            if (createPoiList_display.get(pos).getAttractionCount() > 0 && createPoiList_display.get(pos).getPoiStay()) {
                iv_poi_map.setVisibility(View.VISIBLE);
            } else {
                iv_poi_map.setVisibility(View.GONE);
            }


            create_poi_nightsList = new ArrayList<>();
            create_poi_nightsList.clear();

            create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(createPoiList_display.get(pos).getTripId(), createPoiList_display.get(pos).getPoiDesID(), return_plan);

            Log.d("sizeofnight", String.valueOf(create_poi_nightsList.size()));


            if (create_poi_nightsList.size() <= 0) {
                iv_poi_up_arrow.setVisibility(View.GONE);

                if (edit_stay_time) {
                    iv_poi_delete.setVisibility(View.VISIBLE);
                } else {
                    iv_poi_delete.setVisibility(View.GONE);
                }


            } else {

                iv_poi_up_arrow.setVisibility(View.VISIBLE);
                //tv_poi_distance_time.setVisibility(View.GONE);

                iv_poi_delete.setVisibility(View.GONE);


                for (int j = 0; j < create_poi_nightsList.size(); j++) {
                    LayoutInflater inflater1 = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v1;

                    v1 = inflater1.inflate(R.layout.places_between_night_to_dest, null);
                    TextView tv_poi_name_n = (TextView) v1.findViewById(R.id.tv_poi_name);
                    //    TextView tv_poi_hrs=(TextView)v.findViewById(R.id.tv_poi_hrs);
                    final EditText et_stay_time_n = (EditText) v1.findViewById(R.id.et_stay_time);


                    final ImageView iv_poi_delete_n = (ImageView) v1.findViewById(R.id.iv_poi_delete);
                    TextView tv_poi_distance_time_n = (TextView) v1.findViewById(R.id.tv_poi_distance_time);
                    CircleImageView fc_poi_image_n = (CircleImageView) v1.findViewById(R.id.fc_poi_image);
                    final View view_bottom_line = v1.findViewById(R.id.view_line);
                    LinearLayout ll_edit_night_stay = (LinearLayout) v1.findViewById(R.id.ll_edit_night_stay);

                    tv_poi_name_n.setText(create_poi_nightsList.get(j).getPoiName());
                    et_stay_time_n.setText(create_poi_nightsList.get(j).getStayTime().toString());
                    if (edit_stay_time) {
                        et_stay_time_n.setEnabled(true);
                        ll_edit_night_stay.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
                        iv_poi_delete_n.setVisibility(View.VISIBLE);
                    } else {
                        et_stay_time_n.setEnabled(false);
                        ll_edit_night_stay.setBackground(null);
                        iv_poi_delete_n.setVisibility(View.GONE);
                    }

                    double roundOff = Math.round((create_poi_nightsList.get(j).getKM() / 1000) * 100.0) / 100.0;

                    if (j == 0)
                        tv_poi_distance_time_n.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(create_poi_nightsList.get(j).getTime()));
                    else {

                        int previouspos = j - 1;
                        Double distance_between = distance(create_poi_nightsList.get(j).getPoiLatitude(), create_poi_nightsList.get(j).getPoiLongitude(), create_poi_nightsList.get(previouspos).getPoiLatitude(), create_poi_nightsList.get(previouspos).getPoiLongitude());
                        double roundOff2 = Math.round(distance_between * 100.0) / 100.0;


                        //Double time = get_time_based_on_KM(distance_between);
                        Double time = roundOff2 * 60;
                        time = Math.round(time * 100.0) / 100.0;
                        Log.d("distime1", String.valueOf(distance_between) + "time" + String.valueOf(time));

                        tv_poi_distance_time_n.setText(roundOff2 + " Kms / " + Constants.convert_minute_hrs_both(Double.valueOf(time)));
                    }


                    /*if (create_poi_nightsList.get(j).getPoiImage() != null) {
                        byte[] bytes = create_poi_nightsList.get(j).getPoiImage();
                        fc_poi_image_n.setImageBitmap(Constants.getImage(bytes));


                    }*/

                    bitmap = new ImageSaver(getActivity()).
                            setFileName(create_poi_nightsList.get(j).getImagename()).
                            setExternal(true).
                            load();
                    if (bitmap != null)
                        fc_poi_image_n.setImageBitmap(bitmap);

                    iv_poi_delete_n.setTag(j);
                    et_stay_time_n.setTag(j);


                    if (j == create_poi_nightsList.size() - 1) {
                        view_bottom_line.setVisibility(View.GONE);
                    }


                    iv_poi_up_arrow.setTag(1);
                    iv_poi_up_arrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if ((int) iv_poi_up_arrow.getTag() == 1) {
                                ll_places_between_N_to_D.setVisibility(View.GONE);
                                iv_poi_up_arrow.setImageResource(R.drawable.arrow_down_map_slider);
                                iv_poi_up_arrow.setTag(0);
                            } else {

                                ll_places_between_N_to_D.setVisibility(View.VISIBLE);
                                iv_poi_up_arrow.setImageResource(R.drawable.arrow_up_map_slider);
                                iv_poi_up_arrow.setTag(1);
                            }

                        }
                    });

                    ll_places_between_N_to_D.addView(v1);


                }
            }


        } else {
            iv_poi_night_stay.setVisibility(View.GONE);
            iv_poi_delete.setVisibility(View.VISIBLE);
            ll_stay_time_layout.setVisibility(View.VISIBLE);
        }
        if (edit_stay_time) {
            iv_poi_delete.setVisibility(View.VISIBLE);
        } else {
            iv_poi_delete.setVisibility(View.GONE);
        }


        if (pos == 0) {

            String tripStartDate = "";
            if (return_plan == 0)
                tripStartDate = createTripList_display.get(0).getStartdate() + " " + createTripList_display.get(0).getStarttime();
            else
                tripStartDate = createTripList_display.get(0).getReturn_start_date() + " " + createTripList_display.get(0).getReturn_start_time();

            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(createPoiList_display.get(0).getTime() + createPoiList_display.get(0).getPoi_diff_seconds());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);

                tv_arrival_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
                createPoiList_display.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));


                if (createPoiList_display.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((createPoiList_display.get(pos).getStayTime() - Math.floor(createPoiList_display.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());
                    calendar.add(Calendar.HOUR, createPoiList_display.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tv_departure_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
                    createPoiList_display.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));

                } else {
                    date = sdf.parse(createPoiList_display.get(pos).getResumeDate() + " " + createPoiList_display.get(pos).getResumeTime());
                    calendar.setTime(date);
                    tv_departure_time.setText(createPoiList_display.get(pos).getResumeDate() + "\n" + convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList_display.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));

                }

            } catch (ParseException e) {
                e.printStackTrace();

            }


        }
        //Log.d("arrivalTime","ar"+createPoiList_display.get(pos).getArrivalTime()+"de"+createPoiList.get(pos).getDepartureTime()+"re"+createPoiList.get(pos).getReqtime());

        if (pos >= 1 && (pos <= createPoiList_display.size() - 1)) {

            int position = pos - 1;

            String tripStartDate = createPoiList_display.get(position).getActual_arrival_date_time();

            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                int requriedtime[] = splitToComponentTimes(createPoiList_display.get(position).getReqtime());
                calendar.add(Calendar.HOUR, requriedtime[0]);


                calendar.add(Calendar.MINUTE, requriedtime[1]);

                tv_arrival_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));

                createPoiList_display.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));
                if (createPoiList_display.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((createPoiList_display.get(pos).getStayTime() - Math.floor(createPoiList_display.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, createPoiList_display.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tv_departure_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList_display.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                } else {
                    date = sdf.parse(createPoiList_display.get(pos).getResumeDate() + " " + createPoiList_display.get(pos).getResumeTime());

                    calendar.setTime(date);


                    tv_departure_time.setText(createPoiList_display.get(pos).getResumeDate() + "\n" + convert_time_12_format(sdfTime.format(calendar.getTime())));
                    createPoiList_display.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));


                }

            } catch (ParseException e) {
                e.printStackTrace();

            }

        }


        if (createPoiList_display.size() == pos + 1) {
            try {
                double roundOff = 0.0;

                if (return_plan == 0)
                {
                    roundOff = Math.round((createTripList_display.get(0).getKM()) / 1000 * 100.0) / 100.0;

                    createPoiList_display.get(pos).setReqtime(Double.valueOf(Constants.convert_minute_hrs1(createTripList_display.get(0).getTime() + createTripList_display.get(0).getEta_diff_second())));
                    tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createTripList_display.get(0).getTime() + createTripList_display.get(0).getEta_diff_second()));
                } else {
                    roundOff = Math.round((createTripList_display.get(0).getReturnKM()) / 1000 * 100.0) / 100.0;

                    createPoiList_display.get(pos).setReqtime(createTripList_display.get(0).getReturnTime() + createTripList_display.get(0).getEta_return_diff_second());

                    Log.d("retruntimereq","rettime "+createTripList_display.get(0).getReturnTime()+" eta "+ createTripList_display.get(0).getEta_return_diff_second());
                    tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createTripList_display.get(0).getReturnTime() + createTripList_display.get(0).getEta_return_diff_second()));
                }


                add_hours_minute_last_display(createPoiList_display.get(pos).getActual_dep_date_time(), createPoiList_display.get(pos).getReqtime());
            } catch (Exception e) {
                e.printStackTrace();

            }


        }
        else
        {
            double roundOff = Math.round((createPoiList_display.get(pos + 1).getKM()) / 1000 * 100.0) / 100.0;

            tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createPoiList_display.get(pos + 1).getTime() + createPoiList_display.get(pos + 1).getPoi_diff_seconds()));

            createPoiList_display.get(pos).setReqtime(createPoiList_display.get(pos + 1).getTime() + createPoiList_display.get(pos + 1).getPoi_diff_seconds());


        }


        iv_poi_night_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!createTripList_display.get(0).getTripstatus().contains("Saved") && createPoiList_display.get(pos).getPoiDate() != null) {

//                Log.d("poidate", createPoiList.get(pos).getPoiDate() );
                    if (edit_stay_time) {

                        final SessionManager sessionManager1 = new SessionManager(getActivity());
                        final Dialog myTripResumeDialog = new Dialog(getActivity());
                        myTripResumeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        myTripResumeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        myTripResumeDialog.setContentView(R.layout.resume_trip_from_place_popup);
                        tv_resume_date_my_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_date);
                        tv_resume_time_my_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_time);
                        final ImageView imgClosepopup = (ImageView) myTripResumeDialog.findViewById(R.id.ImgClosepopup);
                        TextView tv_resume_destination = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_destination);

                        TextView tv_save_resume_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_save_resume_trip);


                        Calendar now = Calendar.getInstance();


                        dpd = DatePickerDialog.newInstance(
                                TrackRoutePlanningFragment.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );

                        tv_resume_destination.setText("Resume Trip From " + createPoiList_display.get(pos).getPoiName());

                        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
                        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");

                        tv_resume_date_my_trip.setText(createTripList_display.get(0).getStartdate());


                        try {
                            tv_resume_time_my_trip.setText(Constants.formatDate(String.valueOf(createPoiList_display.get(pos).getDepartureTime()).replace(".", ":"), "HH:mm", "hh:mm a"));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        Date date = null;
                        try {
                            date = sdf1.parse(createPoiList_display.get(pos).getPoiDate());
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);

                            dpd.setMinDate(calendar);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        tpd = TimePickerDialog.newInstance(
                                TrackRoutePlanningFragment.this,
                                now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE), true
                        );

                        imgClosepopup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                myTripResumeDialog.dismiss();
                            }
                        });

                        tv_resume_time_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                select_dpd_type = "resume";
                                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                            }
                        });

                        tv_resume_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                select_dpd_type = "resume";
                                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

                            }
                        });
                        tv_save_resume_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                String resume_time = "";

                                try {
                                    resume_time = Constants.formatDate(tv_resume_time_my_trip.getText().toString(), "hh:mm a", "HH:mm");


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                String arrival_date = createPoiList_display.get(0).getPoiDate() + " " + createPoiList_display.get(0).getArrivalTime().toString().replace(".", ":");
                                Log.d("resumetimedparrival", arrival_date);
                                if (CheckDates(arrival_date, tv_resume_date_my_trip.getText().toString() + " " + resume_time)) {

                                    dbHandler.update_resumedate_time_by_PoiId(createPoiList_display.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time);

                                    Log.d("resumetimedp", String.valueOf(dbHandler.update_resumedate_time_by_PoiId(createPoiList_display.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time)));
                                    myTripResumeDialog.dismiss();
                                    get_my_trip_display(return_plan);
                                } else {
                                    Constants.show_error_popup(getActivity(), "Please change trip resume date.", iv_bottom_up_arrow);
                                }


                            }
                        });

                        myTripResumeDialog.show();


                    }
                } else {
                    Constants.show_error_popup(getActivity(), "Please enter trip start date.", iv_bottom_up_arrow);
                }

            }
        });


        ll_places_between_S_to_D.addView(v);


    }

    public String add_hours_minute_display(String startdate, String starttime, Double time_seconds) {


        String tripStartDate = startdate + " " + starttime;


        //  Log.d("startPoi",currentDateandTime);
        Date date = null;
        try {
            date = sdf.parse(tripStartDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            time_seconds = time_seconds;
            int[] converttime = splitToComponentTimes(time_seconds);


            calendar.add(Calendar.HOUR, converttime[0]);
            calendar.add(Calendar.MINUTE, converttime[1]);
            tv_end_date.setText(sdfDate.format(calendar.getTime()));
            tv_end_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));


        } catch (ParseException e) {
            e.printStackTrace();
            tv_end_date.setText("");
            tv_end_time.setText("");
        }

        set_header_origin_destination();


        return "";
    }

    public String add_hours_minute_last_display(String startdate, Double requriedtime) {
        try {


            String tripStartDate = startdate;

            Log.d("retrundate",tripStartDate+" reqtime"+requriedtime);

            //  Log.d("startPoi",currentDateandTime);
            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                int[] converttime = splitToComponentTimes(requriedtime);


                calendar.add(Calendar.HOUR, converttime[0]);
                calendar.add(Calendar.MINUTE, converttime[1]);
                tv_end_date.setText(sdfDate.format(calendar.getTime()));
                tv_end_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));


            } catch (ParseException e) {
                e.printStackTrace();
                tv_end_date.setText("");
                tv_end_time.setText("");
            }

            set_header_origin_destination();
        } catch (Exception e) {
        }

        return "";
    }

    private LatLng midPoint(double lat1, double long1, double lat2, double long2) {

        return new LatLng((lat1 + lat2) / 2, (long1 + long2) / 2);

    }






    private void add_marker_plan(Integer returnstatus) {

        try {


            map.removeAnnotations();
            draw_route();
            createPoiList = dbHandler.get_POI_TRIP(tripid, return_plan);
            if (selected_poi_pos > -1 && createPoiList.size() > 0) {
                createPoiList.get(selected_poi_pos).setClick_status(false);
            }

            for (int i = 0; i < createPoiList.size(); i++) {


                if ((createPoiList.get(i).getPoitype().contains("POI") || createPoiList.get(i).getPoitype().contains("DES")))
                    getMarkerBitmapFromView(i, "POI", createPoiList.get(i).getPoiLatitude(), createPoiList.get(i).getPoiLongitude(), 1);

                if (createPoiList.get(i).getPoitype().contains("Services")) {

                    for (int j = 0; j < listServiceCategoryList.size(); j++) {
                        if (listServiceCategoryList.get(j).getSerCatId() == createPoiList.get(i).getPoiServerId()) {
                            category_select_pos = j;
                        }

                    }
                    getMarkerBitmapFromView(i, "Services", createPoiList.get(i).getPoiLatitude(), createPoiList.get(i).getPoiLongitude(), category_select_pos);

                }
                if (createPoiList.get(i).getPoitype().contains("RO"))
                {
                    getMarkerBitmapFromView(i, "Roplan", createPoiList.get(i).getPoiLatitude(), createPoiList.get(i).getPoiLongitude(), 0);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

/*    private void setCarLocation1() {
        // If we have the last location of the user, we can move the camera to that position.

        try {



            LatLng newPt = new LatLng(Latitute, Longitute);

            if (car != null)
            {
                car.setVisible(true);
                car.setPosition(newPt);
            } else {

                car = map.addMarker(new MarkerViewOptions()
                        .position(newPt)
                        .anchor(0.5f, 0.5f)
                        .snippet("point")
                        .icon(car_icon));

            }

            if (zoomlevel == 0) {
                CameraPosition position1 = new CameraPosition.Builder()
                        .target(new LatLng(car.getPosition().getLatitude(), car.getPosition().getLongitude())) // Sets the new camera position
                        .bearing(bearing)
                        .zoom(max_zoom)
                        .build(); // Creates a CameraPosition from the builder
                map.moveCamera(CameraUpdateFactory
                        .newCameraPosition(position1));

            }




        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void setCarLocation() {
        // If we have the last location of the user, we can move the camera to that position.

        try {





            if(map.getMyLocation()!=null)

                if (zoomlevel == 0 && map.getMyLocation().hasBearing()) {
                    CameraPosition position1 = new CameraPosition.Builder()
                            .bearing(map.getMyLocation().getBearing())
                            .zoom(max_zoom)
                            .build(); // Creates a CameraPosition from the builder
                    map.moveCamera(CameraUpdateFactory
                            .newCameraPosition(position1));

                }




        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getServices(Double latitude, Double longitude, final String servicetype, Integer routeid) {

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        pb_dialog.show();
        pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String Json = "[{    \"SerId\": \"0\",\"PageNo\": \"1\",\"PageSize\": \"30\", \"SerLatitude\":\"" + latitude + "\",\"SerLongitude\":\"" + longitude + "\",\"radius\":\"999999999\", \"RouteId\": \"" + routeid + "\",  \"SerServiceType\": \"" + servicetype + "\"}]";

        Log.d("servicejson", Json);


        Call<ArrayList<ServicesPojo>> call = apiService.servicesReponse(Json);
        call.enqueue(new Callback<ArrayList<ServicesPojo>>() {
            @Override
            public void onResponse(Call<ArrayList<ServicesPojo>> call, Response<ArrayList<ServicesPojo>> response) {
                pb_dialog.dismiss();
                if (response.body() != null && response.body().size() > 0) {
                    services = new ArrayList<ServicesPojo>();
                    services.clear();
                    services.addAll(response.body());
                    if (services.get(0).getResStatus() == true) {
//                    dbHandler.deleteAllService();
                        map.clear();
                        add_marker_services();

                    } else {
                        map.clear();
                        draw_route();
                        Constants.show_error_popup(getActivity(), "No " + servicetype + " services found.", iv_bottom_up_arrow);
                    }


                }

            }

            @Override
            public void onFailure(Call<ArrayList<ServicesPojo>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
            }
        });
    }


    // adapter for horizontal cart item
    public class Service_List_Adapter extends RecyclerView.Adapter<Service_List_Adapter.ViewHolder> {

        public int lastPosition = -1;


        public Service_List_Adapter() {

        }

        // Create new views (invoked by the layout manager)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            // create a new view
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.facility_content_bottom, null);

            // create ViewHolder

            ViewHolder viewHolder = new ViewHolder(itemLayoutView);
            return viewHolder;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

            // - get data from your itemsData at this position
            // - replace the contents of the view with that itemsData

            if (listServiceCategoryList.get(position).isStatus()) {
                viewHolder.tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));

                fl_pop_up_zoo.setVisibility(View.GONE);
                viewHolder.iv_facility_icon.setImageURI(Constants.ImagePath + listServiceCategoryList.get(position).getSerCatFooterImageSelected());
            } else {
                viewHolder.iv_facility_icon.setImageURI(Constants.ImagePath + listServiceCategoryList.get(position).getSerCatFooterImage());
                viewHolder.tv_facility_name.setTextColor(getResources().getColor(R.color.colorWhite));
            }
            viewHolder.tv_facility_name.setText(listServiceCategoryList.get(position).getSerCatName());
            //setAnimation(viewHolder.itemView, position);

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Dialog   pb_dialog1 = new Dialog(getActivity());
                    pb_dialog1 = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);

                    pb_dialog1.show();

                    category_select_pos = position;
                    for (int i = 0; i < listServiceCategoryList.size(); i++)
                    {
                        listServiceCategoryList.get(i).setStatus(false);
                    }
                    listServiceCategoryList.get(position).setStatus(true);


                    for (int i = 0; i < listServiceCategoryList.size(); i++) {
                        if (i != position)
                            listServiceCategoryList.get(i).setMarker_flag(false);
                    }


                    if (position == 0)
                    {


                        if (selected_ro_pos > -1 && onAdminRouteRoLists.size() > 0)
                        {
                            onAdminRouteRoLists.get(selected_ro_pos).setClick_status(false);
                        }


                        if (listServiceCategoryList.get(position).isMarker_flag() == false)
                        {

                            add_marker_ro();

                            listServiceCategoryList.get(position).setMarker_flag(true);
                            listServiceCategoryList.get(position).setStatus(true);

                        }
                        else {

                            add_marker_plan(return_plan);

                            listServiceCategoryList.get(position).setMarker_flag(false);
                            listServiceCategoryList.get(position).setStatus(false);
                        }


                    } else if (position == 1)
                    {
                        if (selected_poi_pos > -1 && createPoiList.size() > 0) {
                            createPoiList.get(selected_poi_pos).setClick_status(false);
                        }
                        if (listServiceCategoryList.get(position).isMarker_flag() == false)
                        {

                            add_marker_poi();

                            listServiceCategoryList.get(position).setMarker_flag(true);
                            listServiceCategoryList.get(position).setStatus(true);

                        } else {

                            add_marker_plan(return_plan);

                            listServiceCategoryList.get(position).setMarker_flag(false);
                            listServiceCategoryList.get(position).setStatus(false);

                        }


                    } else {

                        if (listServiceCategoryList.get(position).isMarker_flag() == false)
                        {
                            if (Constants.isInternetAvailable(getActivity()))
                            {
                                if (selected_poi_pos > -1 && createPoiList.size() > 0)
                                {
                                    createPoiList.get(selected_poi_pos).setClick_status(false);
                                }
                                selected_ser_pos = -1;
                                if (routepojo.size() > 0)
                                {
                                    if (return_plan == 0)
                                        getServices(orginlat, originlong, listServiceCategoryList.get(position).getSerCatName(), routepojo.get(0).getRouteId());
                                    else
                                        getServices(deslat, deslong, listServiceCategoryList.get(position).getSerCatName(), routepojo.get(0).getRouteId());

                                    listServiceCategoryList.get(position).setMarker_flag(true);
                                    listServiceCategoryList.get(position).setStatus(true);
                                }


                            }
                            else {
                                pb_dialog1.dismiss();

                                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                            }
                        }
                        else {


                            add_marker_plan(return_plan);
                            listServiceCategoryList.get(position).setMarker_flag(false);
                            listServiceCategoryList.get(position).setStatus(false);

                        }


                    }


                    serviceListAdapter.notifyItemRangeChanged(0, listServiceCategoryList.size());
                    pb_dialog1.dismiss();


                }
            });

        }

        // Return the size of your itemsData (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return listServiceCategoryList.size();
        }

        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
                viewToAnimate.startAnimation(anim);
                lastPosition = position;
            }
        }

        // inner class to hold a reference to each item of RecyclerView
        public class ViewHolder extends RecyclerView.ViewHolder {


            SimpleDraweeView iv_facility_icon;
            TextView tv_facility_name;

            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                iv_facility_icon = (SimpleDraweeView) itemLayoutView.findViewById(R.id.fc_cat);
                tv_facility_name = (TextView) itemLayoutView.findViewById(R.id.tv_facility_name);

            }
        }


    }

    private class TollBoothAdapter extends BaseAdapter {
        Activity activity;
        private ArrayList<TollBooth> arraylist;

        TollBoothAdapter(Activity activity, ArrayList<TollBooth> ExpertsexpertData11) {
            this.activity = activity;
            this.arraylist = ExpertsexpertData11;

        }

        @Override
        public int getCount() {
            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.custom_tollbooth_item, null);

            TextView tv_name, tv_distance, tv_remark;

            tv_name = (TextView) v.findViewById(R.id.tv_toolbooth_name);
            tv_distance = (TextView) v.findViewById(R.id.tv_distance);
            tv_remark = (TextView) v.findViewById(R.id.tv_toolbooth_remarks);
            tv_name.setText(arraylist.get(position).getTollBoothName());
            tv_distance.setText(arraylist.get(position).getDistance().toString() + " Kms");
            tv_remark.setText(arraylist.get(position).getTollRemark());

            return v;
        }
    }


    public  void add_marker_step()
    {
      /* for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++)
        {


            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {

                if(map !=null)
                {
                    String name="step:"+k+" v: "+ Html.fromHtml(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getHtmlInstructions());
                    IconGenerator iconFactory = new IconGenerator(getActivity());
                    Bitmap b= iconFactory.makeIcon(name);
                    Drawable background = new BitmapDrawable(b);
                    IconFactory iconFactory1 = IconFactory.getInstance(getActivity());
                    Icon icon = iconFactory1.fromDrawable(background);

                   map.addMarker(new MarkerOptions().snippet("point").position(new LatLng(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getStartLocation().getLng())).icon(icon));
                }

            }
        }*/
    }


    public void stop_live_tracking()
    {
        Date dateReturnend= null;
        Date dateTripend=null;
        createTripList = dbHandler.get_TRIP_By_TripId(tripid);

        try {

            if( return_plan==1)
            {

                dateReturnend = sdf.parse(createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time());
            }
            else
                dateTripend = sdf.parse(createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime());

        } catch (ParseException e)
        {
            e.printStackTrace();
        }


        //one way trip end

        if(dateTripend !=null)
        {
            Calendar thirtyminbefore = Calendar.getInstance();

            thirtyminbefore.setTime(dateTripend);
            thirtyminbefore.add(Calendar.MINUTE, 5);

            String thiryminafterdate = sdf.format(thirtyminbefore.getTime());

            Calendar calendar = Calendar.getInstance();

            String currentdate = sdf.format(calendar.getTime());

            if (CheckDates1(currentdate, thiryminafterdate))
            {

                preferences.edit().putBoolean("trip_start_tracking", false).commit();
                preferences.edit().putString("DirectionJson", "").commit();
                NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                nMgr.cancelAll();

                showStayDialog();
            }
        }
        //Return way trip end
        if(dateReturnend !=null)
        {
            Calendar thirtyminbefore1 = Calendar.getInstance();
            thirtyminbefore1.setTime(dateReturnend);
            thirtyminbefore1.add(Calendar.MINUTE, 5);

            String thiryminafterdate = sdf.format(thirtyminbefore1.getTime());

            Calendar calendar = Calendar.getInstance();

            String  currentdate = sdf.format(calendar.getTime());

            if (CheckDates1(currentdate, thiryminafterdate))
            {

                preferences.edit().putBoolean("trip_start_tracking", false).commit();
                preferences.edit().putString("DirectionJson", "").commit();
                NotificationManager nMgr = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                nMgr.cancelAll();

                showStayDialog();

            }
        }
    }

    // decode polyline and set latlong list
    public void setDecodeLatLongList()
    {



        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++) {


            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                List<LatLng> latLngList = new ArrayList<>();
                latLngList = decode(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getPolyline().getPoints());

                route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).setDecodelatlonglist(latLngList);


            }
        }


    }


    private void sendNotification_onResume() {

        String messageBody="";
        String title="";
        if(return_plan==0) {
            try {
                messageBody = createTripList.get(0).getEnddate() + " " + Constants.formatDate(createTripList.get(0).getEndtime(),  "HH:mm","hh:mm a") + " ETA";
            } catch (ParseException e) {
                e.printStackTrace();
            }
            title=createTripList.get(0).getSource()+"-"+createTripList.get(0).getDestination();
        }
        else {
            try {
                messageBody = createTripList.get(0).getReturn_end_date() + " " + Constants.formatDate(createTripList.get(0).getReturn_end_time(),  "HH:mm","hh:mm a") + " ETA";
            } catch (ParseException e) {
                e.printStackTrace();
            }
            title=createTripList.get(0).getDestination()+"-"+createTripList.get(0).getSource();
        }


        // Creates an explicit intent for an ResultActivity to receive.
        Intent intent = new Intent(getActivity(), ExploreActivity.class);
        intent.putExtra("tripid",createTripList.get(0).getTripId());

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // This ensures that the back button follows the recommended
        // convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());

        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ExploreActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create remote view and set bigContentView.
        RemoteViews expandedView = new RemoteViews(getActivity().getPackageName(),
                R.layout.custom_notification);
        expandedView.setTextViewText(R.id.tv_trip_name, title);
        expandedView.setTextViewText(R.id.tv_trip_date, messageBody);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);

        Notification notification = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setCustomBigContentView(expandedView)
                .setContentText(messageBody)
                .setContentTitle(createTripList.get(0).getTripname()+" Tracking On").build();

        NotificationManager notificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notification);
    }


    public void setMapZoom()
    {
       if(map !=null)
        {
               if(map.getMyLocation()!=null) {

                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(map.getMyLocation()), max_zoom));
                   /* CameraPosition position1 = new CameraPosition.Builder()
                            .target(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude())) // Sets the new camera position
                            .bearing(map.getMyLocation().getBearing())
                            .zoom(max_zoom)
                            .tilt(30)
                            .build(); // Creates a CameraPosition from the builder
                    map.moveCamera(CameraUpdateFactory
                            .newCameraPosition(position1));*/
                }



        }
    }
    private boolean isApplicationBroughtToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);

        if (!tasks.isEmpty()) {

            ComponentName topActivity = tasks.get(0).topActivity;

            String ActivityName = topActivity.getClassName().toString();
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }

        }
        return false;
    }

    public  void setTrackingMode()

    {

        locationServices.toggleGPS(true);
            map.getTrackingSettings().setDismissAllTrackingOnGesture(false);
            // Enable location and bearing tracking
            map.getTrackingSettings().setMyLocationTrackingMode(MyLocationTracking.TRACKING_FOLLOW);
            map.getTrackingSettings().setMyBearingTrackingMode(MyBearingTracking.GPS);

    }

    public  void stopTrackingMode()

    {

        map.getTrackingSettings().setDismissAllTrackingOnGesture(false);
        // Enable location and bearing tracking
        map.getTrackingSettings().setMyLocationTrackingMode(MyLocationTracking.TRACKING_NONE);
        map.getTrackingSettings().setMyBearingTrackingMode(MyBearingTracking.NONE);
        map.setMyLocationEnabled(true);



    }

}



