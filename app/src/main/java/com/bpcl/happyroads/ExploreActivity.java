package com.bpcl.happyroads;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.bpcl.happyroads.CustomeClass.CirclePageIndicator;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Pojo.AllTravelType;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationName;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.ListDefaultValues;
import com.bpcl.happyroads.Pojo.LogOut;
import com.bpcl.happyroads.Pojo.MyTrip;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.UpdateDeviceToken;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.Services.AlarmReceiver;
import com.bpcl.happyroads.Utils.CircularImageView;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.LoginModule;
import com.bpcl.happyroads.Utils.MyTextView;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExploreActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    public Boolean onactivityfb = false;
    ImageView iv_setting_header;
    TextView tv_origin_city_header, tv_origin_date_header, tv_dest_city_header, tv_dest_date_header;
    LinearLayout ll_img_type_bottom;
    LinearLayout ll_gallary_count, ll_close_header, ll_menu_header;
    ImageView iv_origin_city_header, iv_dest_city_header, iv_close_header;
    int desStart = -1, desEnd = -1, poiStart = -1, poiEnd = -1;
    static int width;
    int height;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options;
   public static Boolean drawerleft=false;
    @Bind(R.id.navView_ll)
    LinearLayout navViewLl;
    @Bind(R.id.iv_dofilter)
    ImageView ivDofilter;
    @Bind(R.id.tv_clear_filter)
    TextView tvClearFilter;
    @Bind(R.id.tv_category_title)
    TextView tvCategoryTitle;
    @Bind(R.id.gv_category_list)
    GridView gvCategoryList;
    @Bind(R.id.category_content_ll)
    LinearLayout categoryContentLl;
    @Bind(R.id.category_ll)
    LinearLayout categoryLl;
    @Bind(R.id.tv_rating_title)
    TextView tvRatingTitle;
    @Bind(R.id.rb_filter_by_rating)
    RatingBar rbFilterByRating;
    @Bind(R.id.ll_rating_content)
    LinearLayout llRatingContent;

    @Bind(R.id.tv_progress_filter)
    MyTextView tvProgressFilter;

    @Bind(R.id.filter_rating_ll)
    LinearLayout filterRatingLl;
    @Bind(R.id.sb_travel_time)
    SeekBar sbTravelTime;
    @Bind(R.id.tv_best_visittime_title)
    TextView tvBestVisittimeTitle;
    @Bind(R.id.gv_best_visittime_list)
    GridView gvBestVisittimeList;
    @Bind(R.id.best_visittime_content_ll)
    LinearLayout bestVisittimeContentLl;
    LinearLayout ll_edit_date_header;
    ArrayList<LogOut> logOutArrayList = new ArrayList<>();
    String selectedmomths = "";
    public Boolean fromsignUp = false;
    TextView tv_apply_filter;
    private FragmentManager fm;
    SlidingPaneLayout layout;

    ArrayList<String> Stringsss = new ArrayList<>();
    ArrayList<String> Stringsssids = new ArrayList<>();

    ArrayList<String> poiStringsss = new ArrayList<>();
    ArrayList<String> poiStringsssids = new ArrayList<>();
    String rate = "0", poirate = "0";
    String[] months = new String[]{"Jan", "Feb",
            "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    SessionManager sessionManager;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    boolean isselected[];
    boolean[] monthisselected = new boolean[months.length];
    ;
    boolean poiisselected[];
    boolean poimonthisselected[];
    MonthListAdapter monthListAdapter;
    @Bind(R.id.exploreheadersearch)
    LinearLayout exploreheadersearch;
    @Bind(R.id.iv_menu_header)
    ImageView ivMenuHeader;
    TextView tv_text_header;
    LinearLayout ll_text_header;
    TextView tv_gallary_total, tv_gallary;
    RelativeLayout rl_main_heder;
    ImageView iv_home_icon_header;
    ImageView iv_down_arrow_city;
    NavigationView navView;
    CirclePageIndicator img_circle_indicator;
    ArrayList<String> AddtoMyPlanList = new ArrayList<>();
    Dialog pb_dialog;
    public static ArrayList<String> destattractionimages = new ArrayList<>();
    Dialog dialog;
    DrawerLayout drawerLayout;
    FrameLayout frame;
    private ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolbar;
    NavigationView navigationView;
    View view;
    ArrayList<DestinationName> cityListOfpopup = new ArrayList<DestinationName>();
    CityAdapter cityAdapter;
    public CircularImageView iv_profile_img;
    ImageView iv_settings;
    ImageView iv_filter_header;
    LinearLayout ll_img_type, ll_km_time, ll_filter_header, ll_main_filter, ll_apply_filter;
    TextView tv_img_type, tv_nearby_places;
    ImageView iv_arrow_down_img_type;
    CollapsingToolbarLayout collapsing_toolbar;
    AppBarLayout app_bar_layout;
    ViewPager pager;
    SimpleDraweeView iv_ro_detail;
    ImageView iv_gallary;
    TextView edcityname;
    TextView edcityname1;
    LinearLayout ll_weather, ll_best_time, ll_best_time_attraction, ll_activities_header, ll_time_attraction, ll_plan_trip_header;
    LinearLayout ll_discover, ll_my_trips, ll_my_favourites, ll_trip_services, ll_emergency, ll_notifications, ll_invite_friends, ll_promos_offers, ll_faq, ll_about_us, ll_logout;
    public TextView tv_login_logout;
    EditText EdSearchpopup;
    ListView citylist;
    ImageView iv_close_EdSearchpopup;
    DBHandler dbHandler;
    public CustomPagerAdapter mCustomPagerAdapter;
    public static String origincity = "";
    ProgressDialog mProgressDialog;
    GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    CallbackManager callbackManager;
    String setgflag = "false";
    LinearLayout ll_top_header;
    String refreshedToken;
    public static Integer destination_exclude_id;
    private SharedPreferences preferences;
    RelativeLayout error_layout;
    TextView tv_distance_title;
    SeekBar sb_distance;
    LinearLayout ll_distance_content, filter_distance_ll;
    private AlarmManager alarmManager;
    private static int TAKE_PICTURE = 1, SELECT_PICTURE = 0, PIC_CROP = 2;
    String path = "", selectedImagePath = "";
    OTP otp;
    public String selectedCat = "", selectedIDs = "";
    ArrayList<OTP> otplist = new ArrayList<>();
    boolean doubleBackToExitPressedOnce = false;
    public static  Boolean whichActivity=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAnalytics mFirebaseAnalytics;
        setContentView(R.layout.activity_explore);
        ButterKnife.bind(this);
        permiExterNalStorage();
        getListDefaultvalues();

        sharedpreferences = getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        preferences = getSharedPreferences(Constants.TripTrackPREFERENCES, 0);
        Constants.executeLogcat(ExploreActivity.this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(ExploreActivity.this);
        callbackManager = CallbackManager.Factory.create();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        dbHandler = new DBHandler(this);

         mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        Bundle bundle = new Bundle();
        //  bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "DashBoard");
        //bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        Log.d("System out", "Refreshed token: " + refreshedToken);

        editor.putString("DEVICE_TOKEN", refreshedToken + "").commit();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("System out", "login success in login manager" + loginResult.toString());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        String packageName = this.getPackageName();
        Log.d("System out","package name "+packageName);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Google signin testlast2020@gmail.com/3178fipl
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        try {
            // 05-27 15:29:20.926: D/System out(25553):
            // keyhash:EA6ONu7X+IrY+1mw6JXkC8VgsFA=
            // //NfgDlIG7XWJzUZRUL+bZySrMX1Q= live

            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.bpcl.happyroads", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("System out",
                        "keyhash:"
                                + Base64.encodeToString(md.digest(),
                                Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            System.out.println("the error is......." + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            System.out.println("the error is......." + e.getMessage());
        }
        sessionManager = new SessionManager(ExploreActivity.this);
        otp = sessionManager.get_Authenticate_User();
        iv_profile_img = (CircularImageView) findViewById(R.id.iv_profile_img);
        LoginCallAPI();


        error_layout = (RelativeLayout) findViewById(R.id.error_layout);
        sb_distance = (SeekBar) findViewById(R.id.sb_distance);
        ll_distance_content = (LinearLayout) findViewById(R.id.ll_distance_content);
        filter_distance_ll = (LinearLayout) findViewById(R.id.filter_distance_ll);
        tv_distance_title = (TextView) findViewById(R.id.tv_distance_title);
        ll_discover = (LinearLayout) findViewById(R.id.ll_discover);
        ll_my_trips = (LinearLayout) findViewById(R.id.ll_my_trips);
        ll_my_favourites = (LinearLayout) findViewById(R.id.ll_my_favourites);
        ll_trip_services = (LinearLayout) findViewById(R.id.ll_trip_services);
        ll_emergency = (LinearLayout) findViewById(R.id.ll_emergency);
        ll_notifications = (LinearLayout) findViewById(R.id.ll_notifications);
        ll_invite_friends = (LinearLayout) findViewById(R.id.ll_invite_friends);
        ll_promos_offers = (LinearLayout) findViewById(R.id.ll_promos_offers);
        ll_faq = (LinearLayout) findViewById(R.id.ll_faq);
        ll_about_us = (LinearLayout) findViewById(R.id.ll_about_us);
        ll_logout = (LinearLayout) findViewById(R.id.ll_logout);
        tv_login_logout = (TextView) findViewById(R.id.tv_login_logout);
        iv_origin_city_header = (ImageView) findViewById(R.id.iv_origin_city_header);
        iv_dest_city_header = (ImageView) findViewById(R.id.iv_dest_city_header);
        iv_close_header = (ImageView) findViewById(R.id.iv_close_header);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        frame = (FrameLayout) findViewById(R.id.frame);
        ll_text_header = (LinearLayout) findViewById(R.id.ll_text_header);
        rl_main_heder = (RelativeLayout) findViewById(R.id.rl_main_heder);
        tv_text_header = (TextView) findViewById(R.id.tv_text_header);
        tv_gallary = (TextView) findViewById(R.id.tv_gallary);
        tv_gallary_total = (TextView) findViewById(R.id.tv_gallary_total);
        tv_img_type = (TextView) findViewById(R.id.tv_img_type);
        iv_arrow_down_img_type = (ImageView) findViewById(R.id.iv_arrow_down_img_type);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        iv_home_icon_header = (ImageView) findViewById(R.id.iv_home_icon_header);
        iv_filter_header = (ImageView) findViewById(R.id.iv_filter_header);
        ll_filter_header = (LinearLayout) findViewById(R.id.ll_filter_header);
        ll_main_filter = (LinearLayout) findViewById(R.id.ll_main_filter);
        ll_apply_filter = (LinearLayout) findViewById(R.id.ll_apply_filter);
        tv_apply_filter = (TextView) findViewById(R.id.tv_apply_filter);
        iv_down_arrow_city = (ImageView) findViewById(R.id.iv_down_arrow_city);
        iv_settings = (ImageView) findViewById(R.id.iv_settings);
        ll_img_type = (LinearLayout) findViewById(R.id.ll_img_type);
        ll_km_time = (LinearLayout) findViewById(R.id.ll_km_time);
        ll_img_type_bottom = (LinearLayout) findViewById(R.id.ll_img_type_bottom);
        navView = (NavigationView) findViewById(R.id.navView);
        ll_plan_trip_header = (LinearLayout) findViewById(R.id.ll_plan_trip_header);
        ll_edit_date_header = (LinearLayout) findViewById(R.id.ll_edit_date_header);

        setSupportActionBar(toolbar);
        toolbar.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        toolbar.setContentInsetsAbsolute(0, 0);
        getSupportActionBar().setElevation(0);
        edcityname = (TextView) findViewById(R.id.edcityname);
        edcityname1 = (TextView) findViewById(R.id.edcityname1);
        ll_weather = (LinearLayout) findViewById(R.id.ll_weather);
        ll_best_time = (LinearLayout) findViewById(R.id.ll_best_time);
        ll_best_time_attraction = (LinearLayout) findViewById(R.id.ll_best_time_attraction);
        ll_activities_header = (LinearLayout) findViewById(R.id.ll_activities_header);
        ll_time_attraction = (LinearLayout) findViewById(R.id.ll_time_attraction);
        ll_gallary_count = (LinearLayout) findViewById(R.id.ll_gallary_count);
        ll_menu_header = (LinearLayout) findViewById(R.id.ll_menu_header);
        ll_close_header = (LinearLayout) findViewById(R.id.ll_close_header);
        tv_nearby_places = (TextView) findViewById(R.id.tv_nearby_places);
        tv_origin_city_header = (TextView) findViewById(R.id.tv_origin_city_header);
        tv_origin_date_header = (TextView) findViewById(R.id.tv_origin_date_header);
        tv_dest_city_header = (TextView) findViewById(R.id.tv_dest_city_header);
        tv_dest_date_header = (TextView) findViewById(R.id.tv_dest_date_header);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        pager = (ViewPager) findViewById(R.id.pager);
        ll_top_header = (LinearLayout) findViewById(R.id.ll_top_header);

        sb_distance.setProgress(200);
        //    tv_max_progress.setVisibility(View.VISIBLE);

        sb_distance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //  set_progress_seekbar(i);
                // setSeekBar(i);
                tvProgressFilter.setText(i+"\nKMs");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

/*
                if (addedlist.size() == 0 && LLsubNearbyDesti.getVisibility() == View.VISIBLE)

                {
                    //   sb.getProgress();

                    SubListingFragment subListingFragment = new SubListingFragment();
                    FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    //((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                    changeTransaction.replace(R.id.frame, subListingFragment, "SubListingFragment");
                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    // Constants.SelectedCat=selectedCat;
                    bundle.putString("name", selectedCat);
                    bundle.putString("nameids", selectedIDs);
                    String progress = "999999";
                    if (sb.getVisibility() == View.VISIBLE) {
                        progress = String.valueOf(sb.getProgress());
                    }
                    bundle.putString("progress", progress);
                    bundle.putString("from", "discover from nearby destination");

                    subListingFragment.setArguments(bundle);
                    changeTransaction.commit();
                }
*/
            }
        });





        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                ExploreActivity.this).threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()

                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO) // Not
                // necessary
                // in
                // common
                .build();
        if (sessionManager.get_Email().equalsIgnoreCase("")) {
            tv_login_logout.setText("Login");
        } else {
            tv_login_logout.setText("Logout");
        }

        ll_main_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        imageLoader.init(ImageLoaderConfiguration.createDefault(ExploreActivity.this));
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
//        img_circle_indicator.setViewPager(pager);
        final float density = getResources().getDisplayMetrics().density;
        // img_circle_indicator.setRadius(2 * density);
       /* final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                pager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);*/

       /* img_circle_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });*/

        if (sessionManager.get_Email().equalsIgnoreCase("")) {

            // tv_login_logout.setText("Login");

        } else {
            Log.d("Systm out", "set image in drawer menu " + Constants.TimbThumb_ImagePath + otp.getUmProfilePhoto() + "&width=" + 100);
            //  iv_profile_img.setImageURI(Constants.TimbThumb_ImagePath + otp.getUmProfilePhoto()+"&width="+100);

            imageLoader.displayImage(Constants.TimbThumb_ImagePath + otp.getUmProfilePhoto() + "&width=" + 100, iv_profile_img, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                }
            });
        }
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        ViewCompat.setTransitionName(findViewById(R.id.collapsing_toolbar), "parallax");
        iv_gallary = (ImageView) findViewById(R.id.iv_gallary);
        iv_ro_detail = (SimpleDraweeView) findViewById(R.id.iv_ro_detail);
        iv_setting_header = (ImageView) findViewById(R.id.iv_setting_header);


        exploreheadersearch.setVisibility(View.VISIBLE);
        drawerLayout.setBackgroundResource(R.drawable.main_bg);

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, navView);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, navigationView);
        navigationView.setVerticalScrollbarPosition(1);



        ll_text_header.setVisibility(View.GONE);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(false);
        bar.setDisplayShowHomeEnabled(false);
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);
        Display mDisplay = ExploreActivity.this.getWindowManager().getDefaultDisplay();
        width = mDisplay.getWidth();
        height = mDisplay.getHeight();
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.hidekeyboard(ExploreActivity.this, toolbar);
            }
        });
        if (IsNetworkConnection.checkNetworkConnection(ExploreActivity.this)) {
            getAllCityList("");

        } else {
            Toast.makeText(ExploreActivity.this, "" + ExploreActivity.this.getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

        }

/*
        if (getIntent().getIntExtra("tripid", 0) != 0) {

            //    b.putString("plantype","STOD");
            if(sessionManager.isLoggedIn()) {

                //updateTripApi
                UpdateTrip_status("OnGoing", getIntent().getIntExtra("tripid", 0));

            }
            else
            {
                LoginModule loginModule=new LoginModule(ExploreActivity.this,"ExploreActivity");
                loginModule.signIn();
            }

        }

        else if (savedInstanceState == null) {
            replace_fragmnet(new DiscoverFragment());
        }*/
        startAlarmManager();

        if (sessionManager.isLoggedIn()) {
            if (IsNetworkConnection.checkNetworkConnection(ExploreActivity.this)) {
                UpdateDeviceToken();

            } else {
                Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), error_layout);

            }
        }

        if (getIntent().getIntExtra("tripid", 0) != 0) {

            Log.i("system", "Enters into get intent" + getIntent().getIntExtra("tripid", 0));
            //    b.putString("plantype","STOD");
            if (sessionManager.isLoggedIn()) {

                //updateTripApi
//                UpdateTrip_status("OnGoing", getIntent().getIntExtra("tripid", 0));
                if (preferences.getBoolean("trip_start_tracking", false)) {
                    Bundle b = new Bundle();
                    b.putInt("tripId", getIntent().getIntExtra("tripid", 0));
                    replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);
                } else {

                    replace_fragmnet(new DiscoverFragment());
                }


            }
            else {
                LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                loginModule.signIn();
            }
        }
        /* else if (getIntent().getStringExtra("fromrsa")!=null) {
            Bundle b = new Bundle();
            b.putSerializable("vehicledetails", (GetVehicleDetails) getIntent().getExtras().get("vehicledetails"));
            b.putSerializable("packagedetails",(SubsciptionPlans)  getIntent().getExtras().get("packagedetails"));
           *//* intent.get("vehicledetails",vehicledetails);
            intent.putExtra("packagedetails",subsciptionPlansArrayList);*//*
            replace_fragmnet_bundle(new ConfirmRSA(), b);

        } else if (getIntent().getStringExtra("fromvehicledetailRSA")!=null) {
            Bundle b = new Bundle();
            b.putSerializable("vehicledetails", (GetVehicleDetails) getIntent().getExtras().get("vehicledetails"));
            b.putSerializable("packagedetails",(SubsciptionPlans)  getIntent().getExtras().get("packagedetails"));
           *//* intent.get("vehicledetails",vehicledetails);
            intent.putExtra("packagedetails",subsciptionPlansArrayList);*//*


            replace_fragmnet_bundle(new SuccessfulRSA(), b);

        }*/ else if (getIntent().getStringExtra("push") != null) {


            Log.d("System out", "set data in push ");
            if (sessionManager.get_Email().equalsIgnoreCase("")) {
                drawerLayout.closeDrawers();
                LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                loginModule.signIn();
            } else {

                drawerLayout.closeDrawers();
                getMyTrip();
            }
        } else if (getIntent().getStringExtra("Poipush") != null) {


            getPOIAPI(Integer.parseInt(getIntent().getStringExtra("PoiId")));
        } else if (preferences.getBoolean("trip_start_tracking", false) == true) {
            Bundle b = new Bundle();
            b.putInt("tripId", preferences.getInt("tripId", -1));
            replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);
        } else {

            replace_fragmnet(new DiscoverFragment());


        }


        //get all trip and set to alarm


    }

    private void LoginCallAPI() {
        if (IsNetworkConnection.checkNetworkConnection(ExploreActivity.this)) {
            String json = "";

            json = "[{\"umMobile\":\"\",\"umEmailId\":\"" + sessionManager.get_Email() + "\",\"umPassword\": \"" + sessionManager.get_Password() + "\",\"umDeviceType\": \"Android\", \"umUDID\": \"" + refreshedToken + "\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\"}]";

            Log.d("System out", "In Sign UP  " + json);

            SignInAPI(json);
        }
    }

    private void SignInAPI(final String json) {
        // get & set progressbar dialog


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<OTP>> call = apiService.signIn(json);
        Log.d("System out", "sign in api call in ___" + json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    otplist.clear();
                    otplist.addAll(response.body());
                    if (otplist.get(0).getResStatus() == true) {//call OTP API here
                        ((ExploreActivity) ExploreActivity.this).LogoutDisplay();
                        // Constants.show_error_popup_success(ExploreActivity.this, ""+otplist.get(0).getResDescription(), error_layout);
                        String json = Constants.convert_object_string(otplist.get(0));
                        sessionManager.create_login_session(json, otplist.get(0).getUmEmailId(), otplist.get(0).getUmPassword());
                        // ((ExploreActivity)ExploreActivity.this).iv_profile_img.setImageURI(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100);
                        imageLoader.displayImage(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto() + "&width=" + 100, iv_profile_img, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                            }
                        });
                        ((ExploreActivity) ExploreActivity.this).tv_login_logout.setText("Logout");

                    } else if (otplist.get(0).getResDescription().contains("Info_ConfirmationCodeIsNotConfirmed")) {
                        sessionManager.clear_login_session();
                        ((ExploreActivity) ExploreActivity.this).tv_login_logout.setText("Login");

                    } else {
                        sessionManager.clear_login_session();
                        ((ExploreActivity) ExploreActivity.this).tv_login_logout.setText("Login");

                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }


    public void open_main_fragment() {
        drawerLayout.closeDrawers();
        exploreheadersearch.setVisibility(View.VISIBLE);
        ll_text_header.setVisibility(View.GONE);
        // ll_img_type.setVisibility(View.GONE);
        ll_img_type_bottom.setVisibility(View.GONE);
        pager.setVisibility(View.GONE);
        rl_main_heder.setVisibility(View.GONE);
        ll_filter_header.setVisibility(View.GONE);
        iv_home_icon_header.setImageResource(R.drawable.logo_navigation);

        replace_fragmnet(new DiscoverFragment());
    }


    public void setpagerdata(boolean tag) {

        Fresco.initialize(ExploreActivity.this, Constants.AddImagesToCache(ExploreActivity.this));


        if (tag == true && destattractionimages.size() > 0) {

            mCustomPagerAdapter = new CustomPagerAdapter(this, tag);
            pager.setAdapter(mCustomPagerAdapter);
            mCustomPagerAdapter.notifyDataSetChanged();
        }


        if (tag == false && mCustomPagerAdapter != null) {
            mCustomPagerAdapter.notifyDataSetChanged();
        }
    }


    @OnClick({R.id.exploreheadersearch, R.id.ll_filter_header, R.id.iv_menu_header, R.id.tv_category_title, R.id.best_visittime_title_ll, R.id.tv_rating_title, R.id.tv_travel_time_title, R.id.tv_clear_filter, R.id.iv_dofilter, R.id.tv_apply_filter, R.id.tv_distance_title, R.id.navView_ll})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.exploreheadersearch:
                if (IsNetworkConnection.checkNetworkConnection(ExploreActivity.this)) {

                    getAllCityList("&&");
                    showPopup();
                } else {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), error_layout);
                }
                break;
            case R.id.tv_category_title:
                if (categoryContentLl.getVisibility() == View.GONE) {
                    categoryContentLl.setVisibility(View.VISIBLE);
                    tvCategoryTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
                } else {
                    categoryContentLl.setVisibility(View.GONE);
                    tvCategoryTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
                }
                break;
            case R.id.best_visittime_title_ll:
                if (bestVisittimeContentLl.getVisibility() == View.GONE) {
                    bestVisittimeContentLl.setVisibility(View.VISIBLE);
                    tvBestVisittimeTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
                } else {
                    bestVisittimeContentLl.setVisibility(View.GONE);
                    tvBestVisittimeTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
                }
                break;
            case R.id.tv_distance_title:
                if (ll_distance_content.getVisibility() == View.GONE) {
                    ll_distance_content.setVisibility(View.VISIBLE);
                    tvBestVisittimeTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
                } else {
                    ll_distance_content.setVisibility(View.GONE);
                    tv_distance_title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
                }
                break;
            case R.id.tv_travel_time_title:
               /* if (llTravelTimeContent.getVisibility() == View.GONE) {
                    llTravelTimeContent.setVisibility(View.VISIBLE);
                    tvTravelTimeTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
                } else {
                    llTravelTimeContent.setVisibility(View.GONE);
                    tvTravelTimeTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
                }*/
                break;

            case R.id.tv_rating_title:
                if (llRatingContent.getVisibility() == View.GONE) {
                    llRatingContent.setVisibility(View.VISIBLE);
                    tvRatingTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up, 0);
                } else {
                    llRatingContent.setVisibility(View.GONE);
                    tvRatingTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down, 0);
                }
                break;

            case R.id.tv_clear_filter:
                // clear filter
                //  ivDofilter.setImageResource(R.drawable.filter_icon);
                //  Stringsss.clear();
                if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {
                    if (isselected != null && isselected.length > 0) {
                        for (int i = 0; i < isselected.length; i++) {
                            isselected[i] = false;
                        }
                    }
                    for (int i = 0; i < months.length; i++) {
                        monthisselected[i] = false;

                    }
                    rbFilterByRating.setRating(0);
                    sb_distance.setProgress(200);
                } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
                    for (int i = 0; i < poiisselected.length; i++) {
                        poiisselected[i] = false;
                    }
                    rbFilterByRating.setRating(0);

                    for (int i = 0; i < months.length; i++) {
                        poimonthisselected[i] = false;

                    }

                }
                setdata();
                break;
            case R.id.iv_dofilter:
                //filter data
                ivDofilter.setImageResource(R.drawable.filter_icon_selected);

                // dofilter();
                drawerLayout.closeDrawer(navigationView);
                if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {

                    FragmentManager fm = getSupportFragmentManager();
                    SubListingFragment fragment = (SubListingFragment) fm.findFragmentByTag("SubListingFragment");
                    fragment.getDestinationAPI();
                } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
                    filter_distance_ll.setVisibility(View.GONE);


                    FragmentManager fm = getSupportFragmentManager();
                    ExplorePlacesFragment fragment = (ExplorePlacesFragment) fm.findFragmentByTag("ExplorePlacesFragment");
                    fragment.getPOIAPI();
                }
                break;
            case R.id.tv_apply_filter:

                drawerLayout.closeDrawer(navigationView);

                //filter data
                dofilter();
                break;
            //  case R.id.ll_apply_filter:
            case R.id.navView_ll:
                break;


            case R.id.ll_filter_header:

                setfiltertonavigationdrawer();


                break;
        }
    }

    void clearALLFilter() {

        if (isselected != null && isselected.length > 0) {
            for (int i = 0; i < isselected.length; i++) {
                isselected[i] = false;
            }
        }
        for (int i = 0; i < months.length; i++) {
            monthisselected[i] = false;

        }
        rbFilterByRating.setRating(0);
        sb_distance.setProgress(200);


        if (Stringsss.size() == 0) {
            if (ll_img_type_bottom.getVisibility() == View.VISIBLE) {
                filter_distance_ll.setVisibility(View.VISIBLE);
                categoryLl.setVisibility(View.GONE);

            } else {
                filter_distance_ll.setVisibility(View.GONE);
                categoryLl.setVisibility(View.VISIBLE);


            }
            rate = "0";
            Gson gson = new Gson();
            String jsonOutput = sharedpreferences.getString("CategoryList", "");
            Type listType = new TypeToken<ArrayList<AllTravelType>>() {
            }.getType();
            ArrayList<AllTravelType> posts = gson.fromJson(jsonOutput, listType);
            Stringsss.clear();
            for (int i = 0; i < posts.size(); i++) {
                Stringsss.add(posts.get(i).getTTypeName());
                Stringsssids.add(posts.get(i).getTTypeId() + "");

            }

            CategoryListAdapter categoryListAdapter = new CategoryListAdapter();
            gvCategoryList.setAdapter(categoryListAdapter);
            isselected = new boolean[Stringsss.size()];


            gvCategoryList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
            gvCategoryList.setVerticalScrollBarEnabled(false);
            gvCategoryList.setSelection(0);


            monthListAdapter = new MonthListAdapter();
            gvBestVisittimeList.setAdapter(monthListAdapter);
            monthisselected = new boolean[months.length];

            gvBestVisittimeList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        }

    }


    public void setmenutonavigationdrawer() {
        Constants.hidekeyboard(ExploreActivity.this, ll_menu_header);

        if (drawerLayout.isDrawerOpen(navigationView) == true) {
            drawerLayout.closeDrawer(navigationView);

        } else {

            navigationView.getMenu().clear();
            // navigationView.inflateMenu(R.menu.drawer_view);
            navViewLl.setVisibility(View.GONE);
           /* if (navViewLl.getChildCount() > 0) {
                navViewLl.removeAllViews();
            }*/
            // FOR NAVIGATION VIEW ITEM TEXT COLOR

            int[][] states = new int[][]{
                    new int[]{-android.R.attr.state_checked},  // unchecked
                    new int[]{android.R.attr.state_checked},   // checked
                    new int[]{}                                // default
            };

            // Fill in color corresponding to state defined in state
            int[] colors = new int[]{
                    Color.parseColor("#FFFFFF"),
                    Color.parseColor("#FCDB00"),
                    Color.parseColor("#FFFFFF"),
            };

            ColorStateList navigationViewColorStateList = new ColorStateList(states, colors);

            // apply to text color
            navigationView.setItemTextColor(navigationViewColorStateList);

            // apply to icon color
            navigationView.setItemIconTintList(navigationViewColorStateList);
            ResetMainMenu();
            drawerLayout.openDrawer(navigationView);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
       /* if (Constants.fromSublistingforSb==false) {
            sb_nearby_attractions.setVisibility(View.GONE);
        }*/

    }

    private void ResetMainMenu() {

        View headerview = navigationView.getHeaderView(0);

        ImageView iv_menu_icon = (ImageView) headerview.findViewById(R.id.iv_menu_icon);
        ImageView iv_settings = (ImageView) headerview.findViewById(R.id.iv_settings);

        ll_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                exploreheadersearch.setVisibility(View.VISIBLE);
                ll_text_header.setVisibility(View.GONE);
                // ll_img_type.setVisibility(View.GONE);
                ll_img_type_bottom.setVisibility(View.GONE);
                pager.setVisibility(View.GONE);
                rl_main_heder.setVisibility(View.GONE);
                ll_filter_header.setVisibility(View.GONE);
                iv_home_icon_header.setImageResource(R.drawable.logo_navigation);
                selectedCat = "";
                selectedIDs = "";
                replace_fragmnet(new DiscoverFragment());

            }
        });
        ll_my_trips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                }
                else
                {
                    drawerLayout.closeDrawers();
                    update_trip();
                }


            }
        });
        ll_my_favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                exploreheadersearch.setVisibility(View.GONE);
                ll_text_header.setVisibility(View.VISIBLE);
                ll_img_type_bottom.setVisibility(View.GONE);
                pager.setVisibility(View.GONE);
                rl_main_heder.setVisibility(View.GONE);
                tv_text_header.setText("My Favorites");
                iv_home_icon_header.setImageResource(R.drawable.back_icon);
                ll_filter_header.setVisibility(View.GONE);

                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);

                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {

                    if (currentFragment.getClass().equals(new MyFavouritesFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {
                        replace_fragmnet(new MyFavouritesFragment());
                    }
                }





            }
        });
        ll_trip_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {
                    drawerLayout.closeDrawers();
                    exploreheadersearch.setVisibility(View.GONE);
                    ll_text_header.setVisibility(View.VISIBLE);
                    ll_img_type_bottom.setVisibility(View.GONE);
                    pager.setVisibility(View.GONE);
                    rl_main_heder.setVisibility(View.GONE);
                    tv_text_header.setText("Road-Side Assistance");
                    ll_filter_header.setVisibility(View.GONE);
                    iv_home_icon_header.setImageResource(R.drawable.back_icon);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                    if (currentFragment.getClass().equals(new TripServicesFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    }
                    else
                    {
                        replace_fragmnet(new TripServicesFragment());
                    }
                }

            }
        });
        ll_emergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {
                    drawerLayout.closeDrawers();
                    ll_text_header.setVisibility(View.VISIBLE);
                    ll_img_type_bottom.setVisibility(View.GONE);
                    tv_text_header.setText("Road-side Assistance");
                    pager.setVisibility(View.GONE);
                    ll_filter_header.setVisibility(View.GONE);

                    rl_main_heder.setVisibility(View.GONE);
                    iv_home_icon_header.setImageResource(R.drawable.back_icon);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                    if (currentFragment.getClass().equals(new RoadsideAssistance().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {


                        replace_fragmnet(new RoadsideAssistance());
                    }
                }

            }
        });
        ll_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                exploreheadersearch.setVisibility(View.GONE);
                ll_text_header.setVisibility(View.VISIBLE);
                ll_img_type_bottom.setVisibility(View.GONE);
                rl_main_heder.setVisibility(View.GONE);
                iv_home_icon_header.setImageResource(R.drawable.back_icon);
                tv_text_header.setText("Notifications");
                ll_filter_header.setVisibility(View.GONE);

                pager.setVisibility(View.GONE);

                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);



                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {

                    if (currentFragment.getClass().equals(new NotificationFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {


                        replace_fragmnet(new NotificationFragment());
                    }
                }


            }
        });
        ll_invite_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                SessionManager sessionManager = new SessionManager(ExploreActivity.this);

                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {
                    exploreheadersearch.setVisibility(View.GONE);
                    ll_text_header.setVisibility(View.VISIBLE);
                    tv_text_header.setText("Invite Friends");
                    ll_img_type_bottom.setVisibility(View.GONE);
                    iv_home_icon_header.setImageResource(R.drawable.back_icon);
                    pager.setVisibility(View.GONE);
                    ll_filter_header.setVisibility(View.GONE);

                    rl_main_heder.setVisibility(View.GONE);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                    if (currentFragment.getClass().equals(new InviteFriendsFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {


                        replace_fragmnet(new InviteFriendsFragment());
                    }
                }

            }
        });
        ll_promos_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                exploreheadersearch.setVisibility(View.GONE);
                ll_text_header.setVisibility(View.VISIBLE);
                tv_text_header.setText("Promos/Offers");
                ll_filter_header.setVisibility(View.GONE);

                ll_img_type_bottom.setVisibility(View.GONE);
                pager.setVisibility(View.GONE);
                rl_main_heder.setVisibility(View.GONE);
                iv_home_icon_header.setImageResource(R.drawable.back_icon);
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                if (currentFragment.getClass().equals(new PromoOffersFragment().getClass())) {
                    drawerLayout.closeDrawer(navigationView);
                } else {


                    replace_fragmnet(new PromoOffersFragment());
                }

            }
        });
        ll_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                exploreheadersearch.setVisibility(View.GONE);
                ll_text_header.setVisibility(View.VISIBLE);
                tv_text_header.setText("FAQ");
                ll_img_type_bottom.setVisibility(View.GONE);
                iv_home_icon_header.setImageResource(R.drawable.back_icon);
                pager.setVisibility(View.GONE);
                ll_filter_header.setVisibility(View.GONE);

                rl_main_heder.setVisibility(View.GONE);


                    Bundle b = new Bundle();

                    b.putString("from", "FAQ");
                    replace_fragmnet_bundle(new AboutUsFragment(),b);

            }
        });
        ll_about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                exploreheadersearch.setVisibility(View.GONE);
                ll_text_header.setVisibility(View.VISIBLE);
                tv_text_header.setText("About Us");
                pager.setVisibility(View.GONE);
                rl_main_heder.setVisibility(View.GONE);
                ll_img_type_bottom.setVisibility(View.GONE);
                ll_filter_header.setVisibility(View.GONE);
                iv_home_icon_header.setImageResource(R.drawable.back_icon);
                FragmentManager fragmentManager = getSupportFragmentManager();
                Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);


                    Bundle b = new Bundle();

                    b.putString("from", "About us");
                    replace_fragmnet_bundle(new AboutUsFragment(),b);


            }
        });
        ll_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {
                    if (IsNetworkConnection.checkNetworkConnection(ExploreActivity.this)) {
                        LogoutApi();
                        ll_discover.performClick();

                    } else {
                        Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), error_layout);

                    }
                }
            }
        });


        iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(navigationView);
                /*setvisibility("",randomeview);
                drawerLayout.setDrawerLockMode (DrawerLayout.LOCK_MODE_LOCKED_CLOSED,navView);
                drawerLayout.setDrawerLockMode (DrawerLayout.LOCK_MODE_LOCKED_CLOSED,navigationView);*/
            }
        });
        iv_profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System out", "clicked_________profile_____!!!!!!!!!!");
                SessionManager sessionManager = new SessionManager(ExploreActivity.this);

                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {
                    drawerLayout.closeDrawers();
                    exploreheadersearch.setVisibility(View.GONE);
                    pager.setVisibility(View.GONE);
                    rl_main_heder.setVisibility(View.GONE);
                    ll_filter_header.setVisibility(View.GONE);

                    ll_text_header.setVisibility(View.VISIBLE);
                    tv_text_header.setText("My Account");
                    ll_img_type_bottom.setVisibility(View.GONE);

                    iv_home_icon_header.setImageResource(R.drawable.back_icon);

                    FragmentManager fragmentManager = getSupportFragmentManager();

                    Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                    if (currentFragment.getClass().equals(new MyAccountFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {


                        replace_fragmnet(new MyAccountFragment());
                    }
                }


            }
        });
        iv_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.get_Email().equalsIgnoreCase("")) {
                    drawerLayout.closeDrawers();
                    LoginModule loginModule = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    loginModule.signIn();
                } else {
                    Log.d("System out", "clicked_________setting_icon_____!!!!!!!!!!");
                    drawerLayout.closeDrawer(GravityCompat.END);
                    tv_text_header.setText("Settings");
                    ll_img_type_bottom.setVisibility(View.GONE);
                    pager.setVisibility(View.GONE);
                    rl_main_heder.setVisibility(View.GONE);
                    ll_filter_header.setVisibility(View.GONE);

                    iv_home_icon_header.setImageResource(R.drawable.back_icon);
                    exploreheadersearch.setVisibility(View.GONE);
                    ll_text_header.setVisibility(View.VISIBLE);
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                    if (currentFragment.getClass().equals(new SettingFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {


                        replace_fragmnet(new SettingFragment());
                    }

                }


            }
        });

    }


    public void LogoutApi() {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Dialog pb_dialog = Constants.get_dialog(ExploreActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;

        json = "[{\"umDeviceType\":\"Android\",\"umUDID\": \"\",\"umId\":\"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
        Log.d("System out", "Logout api  " + json);


        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<LogOut>> call = apiService.Logout(json);
        call.enqueue(new Callback<ArrayList<LogOut>>() {
            @Override
            public void onResponse(Call<ArrayList<LogOut>> call, Response<ArrayList<LogOut>> response) {


//                pb_dialog.dismiss();
                pb_dialog.dismiss();
                if (response.body() != null) {


                    logOutArrayList.clear();
                    logOutArrayList.addAll(response.body());
                    if (logOutArrayList.get(0).getResStatus() == true) {
                        Uri uri = Uri.parse("android.resource://com.bpcl.happyroads/drawable/menu_profile_bg");

                        iv_profile_img.setImageURI(uri);

                        tv_login_logout.setText("Login");

                        final Dialog dialog1 = new Dialog(ExploreActivity.this);
                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog1.setContentView(R.layout.like_remove_layout);
                        final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                        tv_text_like_remove.setText("Logged out Successfully.");
                        SessionManager sessionManager = new SessionManager(ExploreActivity.this);
                        sessionManager.clear_login_session();
                        dialog1.show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(1500);
                                    runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            dialog1.dismiss();
                                            replace_fragmnet(new DiscoverFragment());


                                        }
                                    });
                                } catch (Exception e) {
                                    Log.w("Exception in splash", e);
                                }

                            }
                        }).start();


                    } else {

                        // Constants.show_error_popup(ExploreActivity.this, "Oops! We could not find a destination of your choice. Modify selection.", error_layout);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<LogOut>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(ExploreActivity.this)) {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.server_error), error_layout);
                }


            }
        });
    }

    private void setfiltertonavigationdrawer() {
        if (drawerLayout.isDrawerOpen(navigationView) == true) {
            drawerLayout.closeDrawer(navigationView);


        } else {
            navigationView.getMenu().clear();
            navViewLl.setVisibility(View.VISIBLE);


           /* if (navViewLl.getChildCount() > 0) {
                navViewLl.removeAllViews();
            }

            LayoutInflater layoutInflater = (LayoutInflater) ExploreActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            navViewLl.addView(layoutInflater.inflate(R.layout.filter_layout, null, false));*/
            // FOR NAVIGATION VIEW ITEM TEXT COLOR
            int[][] states = new int[][]{
                    new int[]{-android.R.attr.state_checked},  // unchecked
                    new int[]{android.R.attr.state_checked},   // checked
                    new int[]{}                                // default
            };

            // Fill in color corresponding to state defined in state
            int[] colors = new int[]{
                    Color.parseColor("#FFFFFF"),
                    Color.parseColor("#FCDB00"),
                    Color.parseColor("#FFFFFF"),
            };

            ColorStateList navigationViewColorStateList = new ColorStateList(states, colors);

            // apply to text color
            navigationView.setItemTextColor(navigationViewColorStateList);

            // apply to icon color
            navigationView.setItemIconTintList(navigationViewColorStateList);
            View headerview = navigationView.getHeaderView(0);

            setdata();
            drawerLayout.openDrawer(navigationView);
        }
    }

    private void dofilter() {
        int kmprogress = 0;
        float ratingget = rbFilterByRating.getRating();
        int mainprogress = sbTravelTime.getProgress();
        if (ll_img_type_bottom.getVisibility() == View.VISIBLE) {

            kmprogress = sb_distance.getProgress();

        } else {
            kmprogress = 0000;


        }

        ArrayList<String> selectedcategories = new ArrayList<>();
        ArrayList<String> selectedmont = new ArrayList<>();

        if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {
            String selectedIds = "";
            selectedmomths = "";
            if (monthisselected.length > 0) {
                for (int i = 0; i < months.length; i++) {
                    if (monthisselected[i] == true) {
                        selectedmont.add(months[i]);
                        selectedmomths += (i + 1) + ",";
                    }
                }
            }

            for (int i = 0; i < Stringsss.size(); i++) {
                if (isselected[i] == true) {
                    selectedcategories.add(Stringsss.get(i));
                    selectedIds += Stringsssids.get(i) + ",";
                }
            }
            rate = String.valueOf(Math.round(rbFilterByRating.getRating()));

            if (desStart == -1) {
                desStart = 0;
            } else {
                desStart++;
            }
            if (desEnd == -1) {
                desEnd = 12;
            } else {
                desEnd++;
            }
            FragmentManager fm = getSupportFragmentManager();
            SubListingFragment fragment = (SubListingFragment) fm.findFragmentByTag("SubListingFragment");
            fragment.getDestinationAPIFilter(selectedIds, rate, selectedmomths, desEnd, kmprogress);
        } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
            selectedmomths = "";
            if (poimonthisselected.length > 0) {
                for (int i = 0; i < months.length; i++) {
                    if (poimonthisselected[i] == true) {
                        selectedmont.add(months[i]);
                        selectedmomths += (i + 1) + ",";
                    }
                }
            }

            String selectedIds = "";

            if (poiisselected.length > 0) {
                // selectedmomths="";
                for (int i = 0; i < poiStringsss.size(); i++) {
                    if (poiisselected[i] == true) {
                        selectedcategories.add(poiStringsss.get(i));
                        selectedIds += poiStringsssids.get(i) + ",";
                        // selectedmomths+=(i+1)+",";
                    }
                }
            }
            poirate = String.valueOf(Math.round(rbFilterByRating.getRating()));

            if (poiStart == -1) {
                poiStart = 0;
            } else {
                poiStart++;
            }
            if (poiEnd == -1) {
                poiEnd = 12;
            } else {

                poiEnd++;
            }
            FragmentManager fm = getSupportFragmentManager();
            ExplorePlacesFragment fragment = (ExplorePlacesFragment) fm.findFragmentByTag("ExplorePlacesFragment");
            fragment.getPOIAPIFilter(selectedIds, poirate, selectedmomths, desEnd);
        }

        drawerLayout.closeDrawer(navigationView);

    }

    private void setdata() {

        Log.d("System out", "selected page " + Constants.destSelectionfrom);

        if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {


            if (Stringsss.size() == 0) {
                if (ll_img_type_bottom.getVisibility() == View.VISIBLE) {
                    filter_distance_ll.setVisibility(View.VISIBLE);
                    categoryLl.setVisibility(View.GONE);

                } else {
                    filter_distance_ll.setVisibility(View.GONE);
                    categoryLl.setVisibility(View.VISIBLE);


                }
                rate = "0";
                Gson gson = new Gson();
                String jsonOutput = sharedpreferences.getString("CategoryList", "");
                Type listType = new TypeToken<ArrayList<AllTravelType>>() {
                }.getType();
                ArrayList<AllTravelType> posts = gson.fromJson(jsonOutput, listType);
                Stringsss.clear();
                for (int i = 0; i < posts.size(); i++) {
                    Stringsss.add(posts.get(i).getTTypeName());
                    Stringsssids.add(posts.get(i).getTTypeId() + "");

                }

                CategoryListAdapter categoryListAdapter = new CategoryListAdapter();
                gvCategoryList.setAdapter(categoryListAdapter);
                isselected = new boolean[Stringsss.size()];


                gvCategoryList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
                gvCategoryList.setVerticalScrollBarEnabled(false);
                gvCategoryList.setSelection(0);


                monthListAdapter = new MonthListAdapter();
                gvBestVisittimeList.setAdapter(monthListAdapter);
                monthisselected = new boolean[months.length];

                gvBestVisittimeList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
            } else {
                if (ll_img_type_bottom.getVisibility() == View.VISIBLE) {
                    filter_distance_ll.setVisibility(View.VISIBLE);
                    categoryLl.setVisibility(View.GONE);

                } else {
                    filter_distance_ll.setVisibility(View.GONE);
                    categoryLl.setVisibility(View.VISIBLE);


                }
                CategoryListAdapter categoryListAdapter = new CategoryListAdapter();
                gvCategoryList.setAdapter(categoryListAdapter);
                monthListAdapter = new MonthListAdapter();
                gvBestVisittimeList.setAdapter(monthListAdapter);


            }
            //  rbFilterByRating.setRating(Float.parseFloat(rate));
        } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {

            if (poiStringsss.size() == 0) {


                poirate = "0";
                Gson gson = new Gson();
                categoryLl.setVisibility(View.VISIBLE);

                filter_distance_ll.setVisibility(View.GONE);
                String jsonOutput = sharedpreferences.getString("CategoryList", "");
                Type listType = new TypeToken<ArrayList<AllTravelType>>() {
                }.getType();
                ArrayList<AllTravelType> posts = gson.fromJson(jsonOutput, listType);
                poiStringsss.clear();
                for (int i = 0; i < posts.size(); i++) {
                    poiStringsss.add(posts.get(i).getTTypeName());
                    poiStringsssids.add(posts.get(i).getTTypeId() + "");

                }

                CategoryListAdapter categoryListAdapter = new CategoryListAdapter();
                gvCategoryList.setAdapter(categoryListAdapter);
                poiisselected = new boolean[poiStringsss.size()];
                gvCategoryList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
                gvCategoryList.setVerticalScrollBarEnabled(false);
                gvCategoryList.setSelection(0);
                monthListAdapter = new MonthListAdapter();
                gvBestVisittimeList.setAdapter(monthListAdapter);
                poimonthisselected = new boolean[months.length];
                for (int i = 0; i < months.length; i++) {
                    poimonthisselected[i] = false;

                }
                gvBestVisittimeList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
            } else {
                categoryLl.setVisibility(View.VISIBLE);

                filter_distance_ll.setVisibility(View.GONE);

                CategoryListAdapter categoryListAdapter = new CategoryListAdapter();
                gvCategoryList.setAdapter(categoryListAdapter);
                monthListAdapter = new MonthListAdapter();
                gvBestVisittimeList.setAdapter(monthListAdapter);
            }
            // rbFilterByRating.setRating(Float.parseFloat(poirate));
        }
        int numberOfItems = months.length;

        /*int totalItemsHeight = 0;
        for (int itemPos = 0; itemPos < numberOfItems / 6; itemPos++) {
            View item = monthListAdapter.getView(itemPos, null, gvBestVisittimeList);
            item.measure(0, 0);
            totalItemsHeight += item.getMeasuredHeight();
        }*/


        // Get total height of all item dividers.

        // Set list height.
       /* ViewGroup.LayoutParams params = gvBestVisittimeList.getLayoutParams();
        params.height = totalItemsHeight;
        gvBestVisittimeList.setLayoutParams(params);
        gvBestVisittimeList.requestLayout();*/

        //  rbFilterByRating.setRating(0);
        sbTravelTime.setProgress(0);

        // ivDofilter.setImageResource(R.drawable.filter_icon);
    }


    private class CategoryListAdapter extends BaseAdapter {


        CategoryListAdapter() {
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {
                return Stringsss.size();
            } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
                return poiStringsss.size();
            } else {
                return Stringsss.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) ExploreActivity.this.getSystemService(ExploreActivity.this.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.category_cell, null);

            final MyTextView tv_category_name = (MyTextView) v.findViewById(R.id.tv_category_name);

            if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {

                tv_category_name.setText(Stringsss.get(position));

                boolean checked = isselected[position];
                tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                        R.drawable.category_box_selected) : getResources().getDrawable(
                        R.drawable.category_box_unselected));
                tv_category_name.setTextColor(checked ? getResources().getColor(
                        R.color.textYellow) : getResources().getColor(
                        R.color.colorWhite));
            } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
                tv_category_name.setText(poiStringsss.get(position));

                boolean checked = poiisselected[position];
                tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                        R.drawable.category_box_selected) : getResources().getDrawable(
                        R.drawable.category_box_unselected));
                tv_category_name.setTextColor(checked ? getResources().getColor(
                        R.color.textYellow) : getResources().getColor(
                        R.color.colorWhite));
            }


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {
                        Log.d("System out", "category checked is::" + isselected[position]);

                        if (isselected[position] == true) {
                            isselected[position] = false;
                        } else {
                            isselected[position] = true;
                        }
                        boolean checked = isselected[position];
                        tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                                R.drawable.category_box_selected) : getResources().getDrawable(
                                R.drawable.category_box_unselected));
                        tv_category_name.setTextColor(checked ? getResources().getColor(
                                R.color.textYellow) : getResources().getColor(
                                R.color.colorWhite));
                    } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
                        Log.d("System out", "poi category checked is::" + poiisselected[position]);

                        if (poiisselected[position] == true) {
                            poiisselected[position] = false;
                        } else {
                            poiisselected[position] = true;
                        }
                        boolean checked = poiisselected[position];
                        tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                                R.drawable.category_box_selected) : getResources().getDrawable(
                                R.drawable.category_box_unselected));
                        tv_category_name.setTextColor(checked ? getResources().getColor(
                                R.color.textYellow) : getResources().getColor(
                                R.color.colorWhite));
                    }

                }
            });

            return v;
        }
    }

    private class MonthListAdapter extends BaseAdapter {


        MonthListAdapter() {
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return months.length;

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) ExploreActivity.this.getSystemService(ExploreActivity.this.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.month_cell, null);

            final MyTextView tv_month_name = (MyTextView) v.findViewById(R.id.tv_month_name);
            final ImageView iv_selected = (ImageView) v.findViewById(R.id.iv_selected);
            tv_month_name.setText(months[position]);
            if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {
                boolean checked = monthisselected[position];

                iv_selected.setImageResource(checked ?
                        R.drawable.round_selected_month_filter :
                        R.drawable.round_unselected_month_filter);

                tv_month_name.setTextColor(checked ? getResources().getColor(
                        R.color.textYellow) : getResources().getColor(
                        R.color.colorWhite));
            } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {
                if (poimonthisselected == null) {
                    poimonthisselected = new boolean[months.length];

                }

                boolean checked = poimonthisselected[position];

                iv_selected.setImageResource(checked ?
                        R.drawable.round_selected_month_filter :
                        R.drawable.round_unselected_month_filter);

                tv_month_name.setTextColor(checked ? getResources().getColor(
                        R.color.textYellow) : getResources().getColor(
                        R.color.colorWhite));
            }
            v.setId(position);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {

                        Log.d("System out", "selected poistion list size " + monthisselected.length);
                        Log.d("System out", "start poistion " + desStart + " end poistion " + desEnd);
                        if (monthisselected[view.getId()] == true) {
                            monthisselected[view.getId()] = false;
                        } else {
                            monthisselected[view.getId()] = true;
                        }

                        monthListAdapter = new MonthListAdapter();
                        gvBestVisittimeList.setAdapter(monthListAdapter);
                        Log.d("System out", "month checked is::" + monthisselected[position]);
                    } else if (Constants.destSelectionfrom.equalsIgnoreCase("ExplorePlacesFragment")) {

                        Log.d("System out", "selected poistion list size " + poimonthisselected.length);
                        Log.d("System out", "start poistion " + poiStart + " end poistion " + poiEnd);
                        if (poimonthisselected[view.getId()] == true) {
                            poimonthisselected[view.getId()] = false;
                        } else {
                            poimonthisselected[view.getId()] = true;
                        }
                        monthListAdapter = new MonthListAdapter();
                        gvBestVisittimeList.setAdapter(monthListAdapter);
                        Log.d("System out", "month checked is::" + poimonthisselected[position]);
                    }
                }
            });
            return v;
        }
    }

    private void showPopup() {
        dialog = new Dialog(ExploreActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.choosecitydialog);
        dialog.setCanceledOnTouchOutside(false);
        // dialog.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
        EdSearchpopup = (EditText) dialog.findViewById(R.id.EdSearchpopup);
        iv_close_EdSearchpopup = (ImageView) dialog.findViewById(R.id.iv_close_EdSearchpopup);
        citylist = (ListView) dialog.findViewById(R.id.citylist);
        final ImageView ImgClosepopup = (ImageView) dialog.findViewById(R.id.ImgClosepopup);
        final LinearLayout ll_close_btn_city_popup = (LinearLayout) dialog.findViewById(R.id.ll_close_btn_city_popup);
        ll_close_btn_city_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EdSearchpopup.setText("");
                if (IsNetworkConnection.checkNetworkConnection(ExploreActivity.this)) {
                    getAllCityList("");
                } else {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), error_layout);
                }
            }
        });
        EdSearchpopup.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                if (TextUtils.isEmpty(EdSearchpopup.getText().toString())) {
                    iv_close_EdSearchpopup.setVisibility(View.INVISIBLE);

                    Constants.hidekeyboard(ExploreActivity.this, EdSearchpopup);
                    getAllCityList("");
                } else if (EdSearchpopup.getText().toString().length() >= 1) {
                    getAllCityList(EdSearchpopup.getText().toString());
                    iv_close_EdSearchpopup.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        EdSearchpopup.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String searchtext = EdSearchpopup.getText().toString().trim();
                if (TextUtils.isEmpty(searchtext)) {
                    getAllCityList("");
                } else {
                    getAllCityList(searchtext);
                }

                return false;
            }
        });
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (cityListOfpopup.size() > 0) {
            cityAdapter = new CityAdapter(ExploreActivity.this, cityListOfpopup);
            citylist.setAdapter(cityAdapter);
        }
        dialog.show();
    }

    public void getAllCityList(final String cityname) {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/
        String json;
        if (cityname.equalsIgnoreCase("&&")) {
            json = "[{\"desName\":\"" + "" + "\"}]";
            Log.d("Searchcity", json);
        } else {
            json = "[{\"desName\":\"" + cityname + "\"}]";
            Log.d("Searchcity", json);
        }

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ArrayList<DestinationName>> call = apiService.GetDestinationList(json);
        call.enqueue(new Callback<ArrayList<DestinationName>>() {
            @Override
            public void onResponse(Call<ArrayList<DestinationName>> call, Response<ArrayList<DestinationName>> response) {


                if (response.body() != null && !response.body().isEmpty()) {
                    if (response.body().get(0).getResStatus() == true) {
                        //   Log.e("System out","city name get is::"+response.body().get(0).getCityName());
                        //  Log.e("System out","city id get is::"+response.body().get(0).getCityID());
                        cityListOfpopup.clear();
                        cityListOfpopup.addAll(response.body());


                        if (cityListOfpopup.size() > 0) {
                            if (cityname.toString().trim().equalsIgnoreCase("") && !cityname.toString().trim().equalsIgnoreCase("&&")) {




                                edcityname.setText(cityListOfpopup.get(0).getDesName());
                                if (Constants.destSelectionfrom.equalsIgnoreCase("discover") || Constants.destSelectionfrom.equalsIgnoreCase("sublisting"))
                                    edcityname1.setText(cityListOfpopup.get(0).getDesName());
                                Constants.selectedLat = cityListOfpopup.get(0).getDesLatitude();
                                Constants.selectedLong = cityListOfpopup.get(0).getDesLongitude();
                                origincity = cityListOfpopup.get(0).getDesName();
                                destination_exclude_id = cityListOfpopup.get(0).getDesID();
                            }


                        } else {
                            edcityname.setText("Bangalore");
                            origincity = "Bangalore";
                            if (Constants.destSelectionfrom.equalsIgnoreCase("discover") || Constants.destSelectionfrom.equalsIgnoreCase("sublisting"))
                                edcityname1.setText("Bangalore");

                        }

                        if (cityAdapter != null) {
                            cityAdapter.notifyDataSetChanged();
                        }

                    }

                }


            }

            @Override
            public void onFailure(Call<ArrayList<DestinationName>> call, Throwable t) {
                // Log error here since request failed
                t.printStackTrace();
                if (!Constants.isInternetAvailable(ExploreActivity.this)) {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.server_error), error_layout);
                }


            }
        });
    }


    @OnClick({R.id.iv_menu_header, R.id.ll_menu_header})
    public void onClick() {

        setmenutonavigationdrawer();
    }

    private class CityAdapter extends BaseAdapter {
        Activity activity;
        private ArrayList<DestinationName> arraylist;

        CityAdapter(Activity activity, ArrayList<DestinationName> ExpertsexpertData11) {
            this.activity = activity;
            this.arraylist = ExpertsexpertData11;

        }

        @Override
        public int getCount() {
            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) ExploreActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.citypopupcell, null);
            final MyTextView cityname = (MyTextView) v.findViewById(R.id.tvcitynamepopup);
            // final MyTextView statename = (MyTextView) v.findViewById(R.id.tvstatenamepopup);

            cityname.setText(arraylist.get(position).getDesName());
            //  statename.setText(cityListpopup.get(position).get("State"));


            cityname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EdSearchpopup.setText(arraylist.get(position).getDesName());
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    edcityname.setText(arraylist.get(position).getDesName());
                    if (Constants.destSelectionfrom.equalsIgnoreCase("discover") || Constants.destSelectionfrom.equalsIgnoreCase("sublisting"))
                        edcityname1.setText(arraylist.get(position).getDesName());
                    origincity = arraylist.get(position).getDesName();
                    Constants.selectedLat = arraylist.get(position).getDesLatitude();
                    Constants.selectedLong = arraylist.get(position).getDesLongitude();
                    destination_exclude_id = arraylist.get(position).getDesID();
                    dialog.dismiss();

                    if (Constants.destSelectionfrom.equalsIgnoreCase("sublisting")) {

                        FragmentManager fm = getSupportFragmentManager();
                        SubListingFragment fragment = (SubListingFragment) fm.findFragmentByTag("SubListingFragment");
                        fragment.getDestinationAPI();
                    }

                }
            });


            return v;

        }


    }



    @Override
    public void onBackPressed() {
        // mDrawerToggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        if (drawerLayout.isDrawerVisible(GravityCompat.START))
            //  drawerLayout.closeDrawer(GravityCompat.START);
            drawerLayout.closeDrawer(navigationView);

        else {


            final FragmentManager manager = getSupportFragmentManager();

            if (manager.getBackStackEntryCount() >= 1) {
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame);
                drawerLayout.closeDrawers();
                //the fragment on which you want to handle your back press
                if (f instanceof DiscoverFragment) {
                    Log.i("BACK PRESSED", "BACK PRESSED");

                    showexit();

                } else if (f instanceof TrackRoutePlanningFragment) {
                    showexit();
                } else {

                    manager.popBackStack();
                    f = getSupportFragmentManager().findFragmentById(R.id.frame);
                    if(f instanceof TrackRoutePlanningFragment)
                    {
                        replace_fragmnet(new DiscoverFragment());
                    }
                }
            } else {
                showexit();
            }
            //  mDrawerToggle.syncState();
        }
    }

    public void showexit() {
        //Store Cart Data before exit app

        if (doubleBackToExitPressedOnce) {

            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(ExploreActivity.this, "To exit, press back again.", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);

    }
    private class CustomPagerAdapter extends PagerAdapter {
        Context mContext;
        Boolean dataget;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context, Boolean dataget) {
            mContext = context;
            this.dataget = dataget;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return destattractionimages.size();
        }


        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            //SimpleDraweeView imageView = (SimpleDraweeView) itemView.findViewById(R.id.collapsing_img);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.collapsing_img);

            Log.d("System out", "Image of pager:::" + Constants.TimbThumb_ImagePath + destattractionimages.get(position) + "&height=" + height + "&width=" + width);


            // imageView.setImageURI(Constants.TimbThumb_ImagePath+destattractionimages.get(position)+"&width="+width);
            //  imageLoader.displayImage(Constants.TimbThumb_ImagePath+destattractionimages.get(position)+"&height="+(height*3)+"&width="+(width+200), imageView, options, new SimpleImageLoadingListener()

            imageLoader.displayImage(Constants.TimbThumb_ImagePath + destattractionimages.get(position) + "&width=" + (width + 200) + "&height=" + (width + 500), imageView, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                }
            });
            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    public void replace_fragmnet(Fragment fragment) {

        ll_plan_trip_header.setVisibility(View.GONE);
        iv_setting_header.setVisibility(View.GONE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

        fragmentTransaction.replace(R.id.frame, fragment, "Root Planning");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }


    public void replace_fragmnet_bundle(Fragment fragment, Bundle b) {
        ll_plan_trip_header.setVisibility(View.GONE);
        iv_setting_header.setVisibility(View.GONE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        fragment.setArguments(b);
        fragmentTransaction.replace(R.id.frame, fragment, "Root Planning");

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void replace_fragmnet_bundle_popstack(Fragment fragment, Bundle b) {
        ll_plan_trip_header.setVisibility(View.GONE);
        iv_setting_header.setVisibility(View.GONE);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(fragment);
        trans.commit();
        manager.popBackStack();

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("System out", "onConnectionFailed:" + connectionResult);
    }

    public void signIn(String value) {
        setgflag = "true";
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void handleSignInResult(GoogleSignInResult result) {
        Log.d("System out", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("System out", "Reponse " + acct.getEmail());

            if (setgflag.equalsIgnoreCase("true")) {
                LoginModule m = new LoginModule(ExploreActivity.this, "ExploreActivity");
                m.checkGpluse(acct);
            }

        } else {
            // Signed out, show unauthenticated UI.
            /*updateUI(false);*/
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d("System out", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            //  showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    //  hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* if(sessionManager.isLoggedIn()==true)
        {
            ll_logout.setVisibility(View.GONE);
        }else {
            ll_logout.setVisibility(View.VISIBLE);

        }*/
    }

    public void permiExterNalStorage() {
        int permissionCheck = ContextCompat.checkSelfPermission(ExploreActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Log.d("System out", "permission check " + permissionCheck);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            //  permiCamera();
            permiAccessFineLocation();

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
        }
    }

    public void permiCamera() {

        int permissionCheck1 = ContextCompat.checkSelfPermission(ExploreActivity.this, android.Manifest.permission.CAMERA);
        Log.d("System out", "permission check " + permissionCheck1);
        if (permissionCheck1 == PackageManager.PERMISSION_GRANTED) {
            // permiRedContact();
            permiAccessFineLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 123);
        }
    }

    public void permiAccessFineLocation() {
        int permissionCheck3 = ContextCompat.checkSelfPermission(ExploreActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        Log.d("System out", "permission check " + permissionCheck3);
        if (permissionCheck3 == PackageManager.PERMISSION_GRANTED) {
            permiAccessCall();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 123);

        }
    }

    public void permiAccessCall() {
        int permissionCheck3 = ContextCompat.checkSelfPermission(ExploreActivity.this, android.Manifest.permission.CALL_PHONE);
        Log.d("System out", "permission check " + permissionCheck3);
        if (permissionCheck3 == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE}, 123);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permiExterNalStorage();

                } else {
                    this.finish();
                }
                return;
            }
        }
    }

    public void getListDefaultvalues() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = null;


        json = "[{     \"keyName\": \"default\"    }]";


        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<ListDefaultValues>> call = apiService.GetDefaultValues(json);
        call.enqueue(new Callback<ArrayList<ListDefaultValues>>() {
            @Override
            public void onResponse(Call<ArrayList<ListDefaultValues>> call, Response<ArrayList<ListDefaultValues>> response) {


                if (response.body() != null) {

                    Constants.listDefaultValuesArrayList = new ArrayList<>();


                    if (response.body().size() > 0) {

                        Constants.listDefaultValuesArrayList.addAll(response.body());


                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ListDefaultValues>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();


            }
        });
    }

    public void LogoutDisplay() {
        tv_login_logout.setText("Logout");
    }
    private void UpdateDeviceToken() {
        pb_dialog = Constants.get_dialog(ExploreActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        //  pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String json = "[{\"umDeviceType\": \"" + "Android" + "\",\"umUDID\":\"" + refreshedToken + "\", \"umId\":\"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
        Log.d("System out", " update device token json" + json);
        Call<ArrayList<UpdateDeviceToken>> call = apiService.DeviceTokenUpdate(json);
        call.enqueue(new Callback<ArrayList<UpdateDeviceToken>>() {
            @Override
            public void onResponse(Call<ArrayList<UpdateDeviceToken>> call, Response<ArrayList<UpdateDeviceToken>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();

                    if (response.body().get(0).getResStatus()) {
                        Log.d("System out", "Device token update successfully.");
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<UpdateDeviceToken>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(ExploreActivity.this)) {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), ExploreActivity.this.findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.server_error), ExploreActivity.this.findViewById(R.id.error_layout));
                }

            }
        });

    }

    public void getMyTrip() {
        // get & set progressbar dialog
        final Dialog pb_dialog;
        pb_dialog = Constants.get_dialog(ExploreActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        SessionManager sessionManager = new SessionManager(ExploreActivity.this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{\"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",  \"PageNo\": 1,    \"PageSize\": 10,\"TripStatus\":\"" + "OnGoing" + "\" }]";
        Log.d("json_CreateTrip", json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<MyTrip>> call = apiService.GetMyTrip(json);
        call.enqueue(new Callback<ArrayList<MyTrip>>() {
            @Override
            public void onResponse(Call<ArrayList<MyTrip>> call, Response<ArrayList<MyTrip>> response) {
                if (response.body() != null) {


                    pb_dialog.dismiss();
                    int pos = 2;

                    if (response.body().get(0).getResStatus() == true) {//call OTP API here

                        pos = 1;

                    }


                    exploreheadersearch.setVisibility(View.GONE);
                    ll_text_header.setVisibility(View.VISIBLE);
                    ll_img_type_bottom.setVisibility(View.GONE);
                    tv_text_header.setText("My Trips Summary");
                    pager.setVisibility(View.GONE);
                    rl_main_heder.setVisibility(View.GONE);
                    iv_home_icon_header.setImageResource(R.drawable.back_icon);
                    ll_filter_header.setVisibility(View.GONE);
                    Bundle b = new Bundle();
                    b.putInt("tab_pos", pos);

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    Fragment currentFragment = fragmentManager.findFragmentById(R.id.frame);
                    if (currentFragment!=null && currentFragment.getClass().equals(new MyTripWithSwipableFragment().getClass())) {
                        drawerLayout.closeDrawer(navigationView);
                    } else {


                        replace_fragmnet_bundle(new MyTripWithSwipableFragment(), b);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MyTrip>> call, Throwable t) {
                // Log error here since request failed

                List<Create_Trip> createTripList_Ongoing = new ArrayList<>();
                List<Create_Trip> createTripList_Upcoming = new ArrayList<>();
                createTripList_Ongoing = dbHandler.get_All_TRIP("OnGoing");
                createTripList_Upcoming = dbHandler.get_All_TRIP("UpComing");

                if (createTripList_Ongoing.size() > 0 && preferences.getBoolean("trip_start_tracking", false)) {
                    Bundle b = new Bundle();
                    b.putInt("tab_pos", 1);
                    replace_fragmnet_bundle(new MyTripWithSwipableFragment(), b);

                } else if (createTripList_Upcoming.size() > 0) {
                    Bundle b = new Bundle();
                    b.putInt("tab_pos", 2);
                    replace_fragmnet_bundle(new MyTripWithSwipableFragment(), b);
                } else if (!Constants.isInternetAvailable(ExploreActivity.this)) {
                    pb_dialog.dismiss();
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.internet_error), ExploreActivity.this.findViewById(R.id.error_layout));
                } else {
                    pb_dialog.dismiss();
                    Constants.show_error_popup(ExploreActivity.this, ExploreActivity.this.getString(R.string.server_error), ExploreActivity.this.findViewById(R.id.error_layout));
                }


            }
        });

    }

    private void startAlarmManager() {
        Log.d("LiveTracking", "startAlarmManager");

        Context context = getBaseContext();
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent gpsTrackerIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);


        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                5 * 60000, // 60000 = 1 minute
                pendingIntent);
    }

    public void getPOIAPI(final int pid) {


        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        // explore_places_content_progressbar.setVisibility(View.VISIBLE);
        final Dialog pb_dialog = Constants.get_dialog(this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id = "";
        if (sessionManager.isLoggedIn()) {
            id = sessionManager.get_Authenticate_User().getUmId() + "";
        }
        //"POILatitude":"13", "POILongitude":"132"
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        // String json="[{\"umId\": \"0\",\"POIID\":\""+0+"\"    , \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\"  }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";

        // json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+999999+"\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\"  }]";
        json = "[{\"PoiUmId\": \"" + id + "\",\"POIID\":\"" + pid + "\" ,\"radius\":\"" + 100 + "\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\"" + 123 + "\",\"POILatitude\":\"" + 123 + "\", \"POIRating\":0  }]";

        Log.d("System out", "Explore POI from favorites____" + json);


        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {

                pb_dialog.dismiss();

                if (response.body() != null) {


                    ArrayList<ExplorePOI> explorePoiList1 = new ArrayList<ExplorePOI>();

                    explorePoiList1.addAll(response.body());
                    if (explorePoiList1.size() > 0) {
                        if (explorePoiList1.get(0).getResStatus() == true) {

                            //   explore_places_content_progressbar.setVisibility(View.GONE);

                            //  Collections.reverse(exploreDestList);
                            Gson gson = new Gson();
                            String json = gson.toJson(explorePoiList1); // myObject - instance of MyObject
                            editor.putString("POIList", json);
                            editor.commit();

                                    /*if(destinationPlanList.size()>0)
                                    {
                                        for(int i=0;i<explorePoiList.size();i++) {
                                            for(int j=0;j<destinationPlanList.size();j++)
                                            {
                                                Log.d("desid",String.valueOf(destinationPlanList.get(j).getPoiServerId())+"poiid"+explorePoiList.get(i).getPOIID());
                                                if(destinationPlanList.get(j).getPoiServerId()==explorePoiList.get(i).getPOIID())
                                                    ((ExploreActivity) getActivity()).AddtoMyPlanList.add(String.valueOf(i));
                                            }

                                            }
                                        }*/


                            Log.d("desid", "");

                            Bundle bundle = new Bundle();
                            bundle.putString("position", "0");
                            bundle.putString("placeName", explorePoiList1.get(0).getPOIName());
                            bundle.putInt("tripId", 0);
                            bundle.putString("plan_type", "POI");
                            bundle.putString("destId", "" + explorePoiList1.get(0).getPOINearbyDestination());
                            bundle.putString("DestName", explorePoiList1.get(0).getDesName());
                            json = gson.toJson(explorePoiList1);
                            editor.putString("POIList", json);
                            editor.commit();
                            //  bundle=getArguments();


                            ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                            FragmentTransaction changeTransaction = getSupportFragmentManager().beginTransaction();


                            ExploreActivity.destattractionimages.clear();
                            setpagerdata(false);
                            for (int i = 0; i < explorePoiList1.get(0).getImageList().size(); i++) {
                                if (explorePoiList1.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                                    ExploreActivity.destattractionimages.add(explorePoiList1.get(0).getImageList().get(i).getImgName());
                                }
                            }
                            // bundle.putAll(getArguments());
                            exploreAttractionFragment.setArguments(bundle);
                            changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                            changeTransaction.addToBackStack(null);
                            changeTransaction.commit();


                        } else {
                            //  explore_places_content_progressbar.setVisibility(View.GONE);
                            Constants.show_error_popup(ExploreActivity.this, explorePoiList1.get(0).getResDescription() + "", error_layout);
                        }
                    } else {
                        //   explore_places_content_progressbar.setVisibility(View.GONE);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
                //  explore_places_content_progressbar.setVisibility(View.GONE);
                pb_dialog.dismiss();

                t.printStackTrace();


            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Log.d("System out", "result code" + resultCode + "    "
                    + requestCode);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
         /*   height = displaymetrics.heightPixels;
            width = displaymetrics.widthPixels;*/
            if (requestCode == TAKE_PICTURE) {
                if (requestCode == TAKE_PICTURE) {


                    Log.d("System out", "result code in data not null"
                            + resultCode + "    " + requestCode);


                    try {
                        // Uri pictureUri = data.getData();
                        Uri pictureUri = Uri.fromFile(Constants.file);
                        selectedImagePath = getPath(pictureUri);
                        Log.d("System out", "selected image path   " + selectedImagePath);
                        ContentResolver cr = getContentResolver();


                        LoginModule m = new LoginModule(ExploreActivity.this, "ExploreActivity");
                        m.setprofilepic(pictureUri, selectedImagePath);






                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        Toast.makeText(ExploreActivity.this, "Memory insufficient.", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Log.e("Camera", e.toString());
                    }


                    try {

                    } catch (Exception e) {
                        Log.w("Exception in img", e);
                    }

                }
            } else if (requestCode == SELECT_PICTURE) {
                try {
                    Uri selectedImage = data.getData();

                    selectedImagePath = getPath(selectedImage);
                    LoginModule m = new LoginModule(ExploreActivity.this, "ExploreActivity");
                    m.setprofilepic(selectedImage, selectedImagePath);


                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(ExploreActivity.this, "Memory insufficient. ", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    // TODO: handle exception
                }

            } else if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            } else if (onactivityfb == true) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
                LoginModule m = new LoginModule(ExploreActivity.this, "ExploreActivity");
                onactivityfb = false;
                m.checkfb();
            }


        }


    }

    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    public void setProfilePicture(String text) {
        iv_profile_img.setImageResource(R.drawable.menu_profile_bg);
    }
    public void update_trip( ) {
        try {
            final List<Create_Trip>  createTripList = dbHandler.get_All_TRIP_ByFinalStatus();

            for(int i=0;i<createTripList.size();i++)
            {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                String tripdate = "", tripenddate = "", returndate = "", returndatetimeeta = "";
                tripdate = createTripList.get(i).getStartdate() + " " + createTripList.get(i).getStarttime() + ":00";
                tripenddate = createTripList.get(i).getEnddate() + " " + createTripList.get(i).getEndtime() + ":00";

                returndate = createTripList.get(i).getReturn_start_date() + " " + createTripList.get(i).getReturn_start_time() + ":00";


                returndatetimeeta = createTripList.get(i).getReturn_end_date() + " " + createTripList.get(i).getReturn_end_time() + ":00";

                try {
                    tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

                } catch (ParseException e) {

                    tripdate = "";

                }

                try {
                    tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {

                    tripenddate = "";
                }
                if (returndate.length() > 3) {
                    try {
                        returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
                    } catch (ParseException e) {

                        returndate = "";
                    }
                }

                if (returndatetimeeta.length() > 3) {
                    try {
                        returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

                    } catch (ParseException e) {

                        returndatetimeeta = "";

                    }
                }


                String json = "[{     \"TripName\": \"" + createTripList.get(i).getTripname() + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(i).getSourceId() + ",\"TripDestination_End\":" + createTripList.get(i).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + createTripList.get(i).getTripId() + "\",\"TripStatus\": \"" + createTripList.get(i).getTripstatus() + "\",\"TripGooglePlaceDetails\":\"" + createTripList.get(i).getDes_address() + "\",\"TripKm\": \"" + createTripList.get(i).getKM() + "\" ,\"TripTime\":\"" + createTripList.get(i).getTime() + "\",\"TripReturnKm\":\"" + createTripList.get(i).getReturnKM() + "\",\"TripReturnTime\":\"" + createTripList.get(i).getReturnTime() + "\" }]";

                Log.d("json_CreateTrip1", json);
                //  String json="[{     \"umMobile\": \"9184i1587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
                Call<UpdateTrip> call = apiService.updateTrip(json);
                final int finalI = i;
                call.enqueue(new Callback<UpdateTrip>() {
                    @Override
                    public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                        if (response.body() != null) {

                            if(response.body().getResStatus()==true)
                                dbHandler.update_trip_one_way_completed(createTripList.get(finalI).getTripId(), createTripList.get(finalI).getTripstatus(), 0);



                        }
                    }

                    @Override
                    public void onFailure(Call<UpdateTrip> call, Throwable t) {
                        // Log error here since request failed

                        t.printStackTrace();


                    }
                });
            }

            getMyTrip();




        } catch (Exception e) {
        }
    }

}
