package com.bpcl.happyroads;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

/**
 * Created by ADMIN on 15/12/2016.
 */
public class RSAPlanDetails extends Activity {
    WebView webview_rsa;
    ImageView iv_close_rsa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rsa_plan_details);
        iv_close_rsa=(ImageView)findViewById(R.id.iv_close_rsa);
        webview_rsa=(WebView)findViewById(R.id.webview_rsa);
        iv_close_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
