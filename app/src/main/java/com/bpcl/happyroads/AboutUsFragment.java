package com.bpcl.happyroads;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class AboutUsFragment extends Fragment{
    WebView webview;
    private String URL;
    String from="";


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.about_us_fragment, container, false);
        webview=(WebView)v.findViewById(R.id.webview);
        Bundle bundle = this.getArguments();
        if (bundle != null)
        {
            from = bundle.getString("from");

        }
        if(from.equalsIgnoreCase("About us"))
        {
            URL = Constants.GetCMS +from;
            ((ExploreActivity)getActivity()).tv_text_header.setText("About Us");


        }
        else if(from.equalsIgnoreCase("FAQ"))

        {
            URL = Constants.GetCMS +from;
            ((ExploreActivity)getActivity()).tv_text_header.setText("FAQ");


        }

        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });




        webview.getSettings().setDisplayZoomControls(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        WebSettings settings1 = webview.getSettings();

        String text = "<html><head>"
                + "<style type=\"text/css\">body{color: #FFFFFF;}"
                + "</style></head>"
                + "<body></body></html>";
      /*  String rawHTML = "<HTML>"+
                "<body style='color: #FFFFFF'></body>"+
                "</HTML>";*/

     //   String message ="<font color=\"" + color + "\"></font>";


        settings1.setDefaultTextEncodingName("utf-8");
        webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
         //  webview.loadData(text, "text/html", "utf-8");
            webview.loadUrl(URL);
           webview.setBackgroundColor(Color.TRANSPARENT);
       //   webview.setBackgroundResource(R.drawable.main_bg);
        }
        else
        {

        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ExploreActivity)getActivity()).ll_menu_header.setVisibility(View.VISIBLE);

    }
}
