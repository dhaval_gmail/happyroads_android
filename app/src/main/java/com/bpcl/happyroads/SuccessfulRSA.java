package com.bpcl.happyroads;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.SessionManager;

/**
 * Created by ADMIN on 12/12/2016.
 */
public class SuccessfulRSA extends Fragment {

     TextView tv_msg,tv_car_details,tv_subscription_no,tv_package_name,tv_duration,tv_valid_from,tv_valid_till,tv_confirm_pay;
    SessionManager sessionManager;
    OTP otp;
    Bundle bundle;
    GetVehicleDetails vehicledetails;
    SubsciptionPlans subsciptionPlansList;





    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.successful_rsa, container, false);
        tv_msg=(TextView)v.findViewById(R.id.tv_msg);
        tv_car_details=(TextView)v.findViewById(R.id.tv_car_details);
        tv_subscription_no=(TextView)v.findViewById(R.id.tv_subscription_no);
        tv_car_details=(TextView)v.findViewById(R.id.tv_car_details);
        tv_package_name=(TextView)v.findViewById(R.id.tv_package_name);
        tv_duration=(TextView)v.findViewById(R.id.tv_duration);
        tv_valid_from=(TextView)v.findViewById(R.id.tv_valid_from);
        tv_valid_till=(TextView)v.findViewById(R.id.tv_valid_till);
        tv_confirm_pay=(TextView)v.findViewById(R.id.tv_confirm_pay);
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.VISIBLE);

        sessionManager=new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();

        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Successful Transaction");
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ((ExploreActivity)getActivity()).onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        bundle = this.getArguments();




        if (bundle != null)
        {

            vehicledetails = (GetVehicleDetails) bundle.getSerializable("vehicledetails");
            subsciptionPlansList = (SubsciptionPlans) bundle.getSerializable("packagedetails");
            setdata();

        }



        tv_confirm_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Bundle b=new Bundle();
                b.putSerializable("vehicledetails",vehicledetails);
                b.putSerializable("packagedetails",subsciptionPlansArrayList);


                ((ExploreActivity)getActivity()).replace_fragmnet(new SuccessfulRSA());*/
                ((ExploreActivity)getActivity()).replace_fragmnet(new DiscoverFragment());

            }
        });
        return v;
    }

    private void setdata() {
        tv_msg.setText("Thank you " +otp.getUmFirstName()+"for purchasing "+ subsciptionPlansList.getName() +" package.\nYour package details are as follow and an email \n has been sent to you with all the details:\"");
        tv_car_details.setText(vehicledetails.getVehicleColor()+" "+vehicledetails.getVehicleMake()+" "+vehicledetails.getVehicleModel()+" "+vehicledetails.getVehicleYear()+" "+vehicledetails.getVehicleRegNumber());
        tv_subscription_no.setText(vehicledetails.getSubscriptionID());
        tv_package_name.setText(subsciptionPlansList.getName());
        tv_duration.setText(subsciptionPlansList.getDescription().replace("package","").replace("-"," "));
        try {
           // tv_valid_from.setText(Constants.formatDate(vehicledetails.getSubscriptionPurchase().replace("T", " ").substring(0,vehicledetails.getSubscriptionPurchase().replace("T", " ").lastIndexOf("-")), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy"));
            tv_valid_from.setText(Constants.formatDate(vehicledetails.getSubscriptionPurchase().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy"));
            tv_valid_till.setText(Constants.formatDate(vehicledetails.getSubscriptionExpiry().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy"));


        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }



}
