package com.bpcl.happyroads;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Pojo.VehicleDetails;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 02/01/2017.
 */
public class AddVehicleFragment extends Fragment {
    TextView tv_save_vehicle_rsa;
    EditText etVehicleNumber,etVehicleMake,etVehicleModel,etVehicleYear,etVehicleColor;
    ImageView iv_puc;
    RelativeLayout error_layout;
    SessionManager sessionManager;
    OTP otp;
    int vehicle_umid=0;
    ImageView iv_close_add_vehicle;

    ArrayList<VehicleDetails> vehicleDetails=new ArrayList<>();

    boolean puc=false;
    SubsciptionPlans subsciptionPlansArrayList;
    ArrayList<GetVehicleDetails> getVehicleList=new ArrayList<>();

    Bundle bundle;
    Integer vehicleId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_vehicle, container, false);
        tv_save_vehicle_rsa = (TextView)v.findViewById(R.id.tv_save_vehicle_rsa);
        etVehicleNumber = (EditText) v.findViewById(R.id.et_vehicle_number);
        etVehicleMake = (EditText) v.findViewById(R.id.et_vehicle_make);
        etVehicleModel = (EditText) v.findViewById(R.id.et_vehicle_model);
        etVehicleYear = (EditText) v.findViewById(R.id.et_vehicle_year);
        etVehicleColor = (EditText) v.findViewById(R.id.et_vehicle_color);
        iv_puc = (ImageView) v.findViewById(R.id.iv_puc_rsa);
        iv_close_add_vehicle = (ImageView) v.findViewById(R.id.iv_close_add_vehicle);
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.GONE);
        bundle = this.getArguments();

        if (bundle != null) {

            subsciptionPlansArrayList = (SubsciptionPlans) bundle.getSerializable("packagedetails");


        }
        Log.d("System out","subsciptionPlansArrayList____"+(subsciptionPlansArrayList==null));
        error_layout = (RelativeLayout)v. findViewById(R.id.error_layout);
        sessionManager = new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();

        iv_puc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (puc == false) {
                    iv_puc.setImageResource(R.drawable.checkbox_selected_my_account);
                    puc = true;
                } else {
                    iv_puc.setImageResource(R.drawable.checkbox_unselected_my_account);
                    puc = false;

                }
            }
        });
        iv_close_add_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExploreActivity)getActivity()).onBackPressed();

            }
        });
        tv_save_vehicle_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);

                if (TextUtils.isEmpty(etVehicleColor.getText())) {
                    Log.d("System out", "save btn clicked in___");

                    Constants.show_error_popup(getActivity(), "Provide colour of your vehicle . ", error_layout);
                } else if (TextUtils.isEmpty(etVehicleMake.getText())) {
                    Constants.show_error_popup(getActivity(), "Provide name of vehicle manufacturer.", error_layout);
                } else if (TextUtils.isEmpty(etVehicleYear.getText())) {
                    Constants.show_error_popup(getActivity(), "Provide year of manufacturing of your vehicle.", error_layout);
                } else if (etVehicleYear.getText().toString().length() < 4) {
                    Constants.show_error_popup(getActivity(), "Provide year of manufacturing of your vehicle (YYYY).", error_layout);
                }else if (Integer.parseInt(etVehicleYear.getText().toString())>year) {
                    Constants.show_error_popup(getActivity(), "Provide year of manufacturing of your vehicle (YYYY).", error_layout);
                } else if (TextUtils.isEmpty(etVehicleNumber.getText())) {
                    Constants.show_error_popup(getActivity(), "Provide vehicle registration number (ZZ99ZZ9999).", error_layout);
                } else if (!etVehicleNumber.getText().toString().matches(Constants.VehicleNumber)) {
                    Constants.show_error_popup(getActivity(), "Provide vehicle registration number (ZZ99ZZ9999).", error_layout);
                } else if (TextUtils.isEmpty(etVehicleModel.getText())) {
                    Constants.show_error_popup(getActivity(), "Provide your vehicle model.", error_layout);
                } else {
                    if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                        // String json = "[{\"umMobile\": \"" + etVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";
                        String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\" ,\"vehicleColor\": \"" + etVehicleColor.getText().toString() + "\",\"vehicleHasPUC\": \"" + puc + "\",\"vehicleHasRSA\": \"false\",\"vehicleMake\": \"" + etVehicleMake.getText().toString() + "\",\"vehicleYear\": \"" + etVehicleYear.getText().toString() + "\",\"vehicleRegNumber\": \"" + etVehicleNumber.getText().toString() + "\" ,  \"vehicleId\":\"" + vehicle_umid + "\", \"vehicleModel\": \"" + etVehicleModel.getText().toString() + "\" }]";
                        Log.d("System out", "In Insert and update Vehicle  " + json);
                        VehicleAPI(json, error_layout);
                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                    }


                }

            }
        });
        return v;
    }
    private void VehicleAPI(final String json,final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<VehicleDetails>> call = apiService.AddVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<VehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<VehicleDetails>> call, Response<ArrayList<VehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    vehicleDetails.clear();
                    vehicleDetails= response.body();
                    if (vehicleDetails.get(0).getResStatus() == true)
                    {
                        Constants.show_error_popup_success(getActivity(), ""+vehicleDetails.get(0).getResDescription(), error_layout);
                        vehicleId=vehicleDetails.get(0).getVehicleId();
                        GetVehicleList();

                       /* Intent intent=new Intent(getActivity(),ExploreActivity.class);
                        intent.putExtra("vehicledetails",vehicleDetails.get(0));
                        intent.putExtra("packagedetails",subsciptionPlansArrayList);
                        intent.putExtra("fromrsa","fromrsa");
                        startActivity(intent);*/

                        /* etVehicleColor.setText("");
                        etVehicleYear.setText("");
                        etVehicleMake.setText("");
                        etVehicleModel.setText("");
                        etVehicleNumber.setText("");*/
                        // ((ExploreActivity)getActivity()).replace_fragmnet(new VehicleDetailsRSA());

                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(), ""+vehicleDetails.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }

                }
            }



            @Override
            public void onFailure(Call<ArrayList<VehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }
    private void GetVehicleList() {


        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


            String json = "[{\"vehicleUmId\":\""+otp.getUmId()+"\"}]";
            Log.d("System out", "In Get Vehicle List  " + json);

            GetVehicleListAPI(json,error_layout);

        }
        else
        {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
        }

    }

    private void GetVehicleListAPI(final String json,final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<GetVehicleDetails>> call = apiService.GetVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<GetVehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVehicleDetails>> call, Response<ArrayList<GetVehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    getVehicleList.clear();
                    getVehicleList= response.body();
                    if (getVehicleList.get(0).getResStatus() == true)
                    {
                        //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);
                        // VehicleList();
                        for(int i=0;i<getVehicleList.size();i++)
                        {
                            if(getVehicleList.get(i).getVehicleId()==vehicleId)
                            {
                                Bundle b=new Bundle();
                                b.putSerializable("vehicledetails",getVehicleList.get(i));
                                b.putSerializable("packagedetails",subsciptionPlansArrayList);

                                ((ExploreActivity)getActivity()).replace_fragmnet_bundle_popstack(new ConfirmRSA(), b);


                               /* Intent intent=new Intent(getActivity(),ExploreActivity.class);
                                intent.putExtra("vehicledetails",getVehicleList.get(i));
                                intent.putExtra("packagedetails",subsciptionPlansArrayList);
                                intent.putExtra("fromrsa","fromrsa");
                                startActivity(intent);*/
                                break;
                            }
                        }
                    }
                    else
                    {
                        //  VehicleList();
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<GetVehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.VISIBLE);

    }
}
