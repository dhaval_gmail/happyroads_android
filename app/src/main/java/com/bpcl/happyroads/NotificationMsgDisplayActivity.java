package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideBottomExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.Pojo.push;
import com.bpcl.happyroads.Utils.Constants;

import java.lang.reflect.Type;


public class NotificationMsgDisplayActivity extends Activity {

    Editor editor;

    float scale;
    int orientation;
    float[] dimensions;
    static final float[] DIMENSIONS_DIFF_LANDSCAPE = {20, 60};
    static final float[] DIMENSIONS_DIFF_PORTRAIT = {40, 60};
    private Display display;

    private Dialog progressDialog;
    Thread thread;
    String Message = "", whichActivityOpenNamekey = "", PushOrderID = "";
    TextView pushtext; //txt_jobid;
    TextView pushok;


    private String msgKey = "";
    //private Button pushread;


    private String Messagaetxt = "",msg="",notiId="",msgType="";
    private SharedPreferences prefrence;
    push pushs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pushdialogcontra);
        prefrence = getSharedPreferences(Constants.MyPREFERENCES, 0);
        editor = prefrence.edit();
        pushtext = (TextView) findViewById(R.id.txt_message);
        pushok = (TextView) findViewById(R.id.btn_ok);

        BaseAnimatorSet mBasIn = new BounceTopEnter();
        BaseAnimatorSet mBasOut = new SlideBottomExit();
        whichActivityOpenNamekey = prefrence.getString("whichActivityOpenNamekey", "");
        Log.i("System out", "whichActivityOpenNamekey : " + whichActivityOpenNamekey);
        Message = prefrence.getString("PushMsgkey", "");

        Gson gson = new Gson();
        String jsonOutput = prefrence.getString("notification", "");
        Type listType = new TypeToken<push>() {
        }.getType();
        pushs = gson.fromJson(jsonOutput, listType);
        try {
            final NormalDialog dialog = new NormalDialog(NotificationMsgDisplayActivity.this);
            dialog.content("" + Message)
                    .title("")
                    .isTitleShow(false)
                    .showAnim(mBasIn)
                    .dismissAnim(mBasOut)
                    .btnNum(2)
                    .btnText(pushs.getNtfContent().equalsIgnoreCase("AddPOI")?"OK":"View Itinerary",pushs.getNtfContent().equalsIgnoreCase("AddPOI")?"Cancel":"Later")

                    .show();

            dialog.setOnBtnClickL(
                    new OnBtnClickL() {
                        @Override
                        public void onBtnClick() {
                            dialog.dismiss();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                        runOnUiThread(new Runnable() {



                                            @Override
                                            public void run() {
                                                if(pushs.getNtfContent().equalsIgnoreCase("SendTripReminderPush")|| pushs.getNtfContent().equalsIgnoreCase("SendTripStartReminderPush") || pushs.getNtfContent().equalsIgnoreCase("SendUpcomingTripPush"))
                                                {
                                                    Intent intent=new Intent(NotificationMsgDisplayActivity.this,ExploreActivity.class);
                                                    intent.putExtra("tripid",0);
                                                    intent.putExtra("push","TripeReminder");
                                                    startActivity(intent);

                                                    NotificationMsgDisplayActivity.this.finish();
                                                }
                                                else  if(pushs.getNtfContent().equalsIgnoreCase("AddPOI"))
                                                {
                                                    Intent intent=new Intent(NotificationMsgDisplayActivity.this,ExploreActivity.class);
                                                    intent.putExtra("tripid",0);
                                                    intent.putExtra("Poipush","AddPOI");
                                                    intent.putExtra("PoiId",pushs.getNtfRefKey());
                                                    startActivity(intent);

                                                    NotificationMsgDisplayActivity.this.finish();
                                                }
                                                else {

                                                    NotificationMsgDisplayActivity.this.finish();
                                                }
                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.w("Exception in splash", e);
                                    }

                                }
                            }).start();




                        }
                    },
                    new OnBtnClickL() {
                        @Override
                        public void onBtnClick() {
                            dialog.dismiss();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                        runOnUiThread(new Runnable() {



                                            @Override
                                            public void run() {
                                                NotificationMsgDisplayActivity.this.finish();
                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.w("Exception in splash", e);
                                    }

                                }
                            }).start();

                        }
                    });


            pushtext.setText("" + Message);
            pushok.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();

                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
