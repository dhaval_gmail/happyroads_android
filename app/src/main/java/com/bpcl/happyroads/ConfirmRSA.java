package com.bpcl.happyroads;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 12/12/2016.
 */
public class ConfirmRSA extends Fragment {
    Bundle bundle;
    GetVehicleDetails vehicledetails;
    SubsciptionPlans subsciptionPlansArrayList;
     TextView tv_name,tv_number,tv_email,tv_car_details,tv_package_name,tv_change,tv_confirm_pay,tv_terms;
    SessionManager sessionManager;
    OTP otp;
    RelativeLayout error_layout;
    CheckBox ch_terms;
    private boolean ch_terms_flag=false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.confirmation_rsa, container, false);
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Confirmation");
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);

        tv_name=(TextView)v.findViewById(R.id.tv_name);
        tv_terms=(TextView)v.findViewById(R.id.tv_terms);

        tv_number=(TextView)v.findViewById(R.id.tv_number);
        tv_email=(TextView)v.findViewById(R.id.tv_email);
        tv_car_details=(TextView)v.findViewById(R.id.tv_car_details);
        tv_package_name=(TextView)v.findViewById(R.id.tv_package_name);
        tv_change=(TextView)v.findViewById(R.id.tv_change);
        tv_confirm_pay=(TextView)v.findViewById(R.id.tv_confirm_pay);
        error_layout =(RelativeLayout) v.findViewById(R.id.error_layout);
        ch_terms=(CheckBox)v.findViewById(R.id.ch_terms);
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.VISIBLE);
        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("from", "RSA Service TnC");
                Intent i=new Intent(getActivity(),TermsAndConditionsActivity.class);
                i.putExtras(b);
                startActivity(i);

            }
        });
        ch_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(ch_terms_flag == false)
                {
                    ch_terms_flag=true;
                    ch_terms.setButtonDrawable(R.drawable.checkbox_selected_my_account);
                }
                else
                {
                    ch_terms_flag=false;
                    ch_terms.setButtonDrawable(R.drawable.checkbox_unselected_my_account);
                }

            }
        });

        sessionManager=new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();
        bundle = this.getArguments();



        if (bundle != null)
        {

            vehicledetails = (GetVehicleDetails) bundle.getSerializable("vehicledetails");
            subsciptionPlansArrayList = (SubsciptionPlans) bundle.getSerializable("packagedetails");
            setdata();

        }
        tv_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putSerializable("vehicledetails", vehicledetails);
                b.putString("from","changeFromConfirmRSA");
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new BuyRSA(),b);
            }
        });



        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        tv_confirm_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Bundle b=new Bundle();
                b.putSerializable("vehicledetails",vehicledetails);
                b.putSerializable("packagedetails",subsciptionPlansArrayList);


                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new SuccessfulRSA(),b);*/
                if(ch_terms_flag == false)
                {
                    Constants.show_error_popup(getActivity(), "Accept the Terms & Conditions. ", error_layout);

                }else if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                    String json = "[{\"umMobile\": \"" + otp.getUmMobile() + "\",\"umFirstName\":\"" + otp.getUmFirstName() + "\",\"umEmailId\": \"" + otp.getUmEmailId() + "\",\"UmId\": \"" + otp.getUmId() + "\",\"umDeviceType\": \"Android\"}]";


                    Log.d("System out", "json for RSA customer Registration " + json);
                    RSARegistration(json,subsciptionPlansArrayList.getId(),subsciptionPlansArrayList.getCode());

                } else {
                    Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                }
            }
        });
        return v;
    }

    private void setdata() {
        tv_name.setText(otp.getUmFirstName().trim()+" "+otp.getUmLastName());
        tv_number.setText(otp.getUmMobile());
        tv_email.setText(otp.getUmEmailId());
        tv_car_details.setText(vehicledetails.getVehicleRegNumber()+", "+vehicledetails.getVehicleMake()+", "+vehicledetails.getVehicleModel()+", "+vehicledetails.getVehicleYear()+"");
        tv_package_name.setText(subsciptionPlansArrayList.getDescription().replace("-"," ").replace("package","")+", Rs."+subsciptionPlansArrayList.getCurrentDefaultSalePrice().replace("INR_","")+"/-");
        Log.d("System out"," package rsa name____"+subsciptionPlansArrayList.getDescription().replace("-"," ").replace("package","")+", Rs."+subsciptionPlansArrayList.getCurrentDefaultSalePrice().replace("INR_","")+"/-");
    }


    private void RSARegistration(String json, final String id, final String code) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<OTP>> call = apiService.RSACustomerRegistration(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();

                    if(response.body().get(0).getResStatus() == true) {

                        otp.setUmRSAId(response.body().get(0).getUmRSAId());
                        String json = Constants.convert_object_string(otp);
                        sessionManager.create_login_session(json, otp.getUmEmailId(), otp.getUmPassword());
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json1 = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicledetails.getVehicleId()+ "\"}]";


                            Log.d("System out", "json for RSA Create Vehicle " + json1);
                            VehicleRegister(json1,id,code);

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                        }
                    }
                    else if(response.body().get(0).getResDescription().contains("PHONE_EXISTS"))
                    {
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json1 = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicledetails.getVehicleId()+ "\"}]";


                            Log.d("System out", "json for RSA Create Vehicle phone exists" + json1);
                            VehicleRegister(json1,id,code);

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                        }
                    }
                }


            }



            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }
    private void VehicleRegister(String json, final String id, final String code) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<OTP>> call = apiService.RSACreateVehicle(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    if(response.body().get(0).getResStatus() == true) {

                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            // String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";



                            String json=Constants.BASE_URL+"api/RSA_Ver1/PolicyPurchase?id="+vehicledetails.getVehicleRSAId()+"&code="+code;
                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            GetRSAwebUrl(json);

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                        }
                    }
                    else if(response.body().get(0).getResDescription().contains("VEHICLEREG_EXISTS"))
                    {
                       // Constants.show_error_popup(getActivity(), "" + response.body().get(0).getResDescription(), error_layout);
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            // String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";



                            String json=Constants.BASE_URL+"api/RSA_Ver1/PolicyPurchase?id="+vehicledetails.getVehicleRSAId()+"&code="+code;
                            Log.d("System out", "json for RSA Create Vehicle vehicle exists" + json);
                            GetRSAwebUrl(json);

                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                        }
                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(), "" + response.body().get(0).getResDescription(), error_layout);

                    }
                }


            }



            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }
    private void GetRSAwebUrl(String json) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<String> call = apiService.RSAGetLink(json);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    Log.d("System out","Response of link "+response.body());
                    if(response.body().length()>0) {
                        showPopupForWebview(response.body().replace("\"",""));
                    }
                    else {

                    }
                }


            }



            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void showPopupForWebview(String URL) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.subscriptiondialog);
        dialog.setCanceledOnTouchOutside(false);

        // dialog.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);


        final ListView subcriptionlist = (ListView) dialog.findViewById(R.id.subcriptionlist);
        LinearLayout sub_header_ll=(LinearLayout)dialog.findViewById(R.id.sub_header_ll);
        final ImageView ImgClosepopup = (ImageView) dialog.findViewById(R.id.ImgClosepopup);
        WebView webview = (WebView)dialog.findViewById(R.id.subcription_webview);
        sub_header_ll.setVisibility(View.GONE);
        subcriptionlist.setVisibility(View.GONE);
        webview.setVisibility(View.VISIBLE);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(URL);

        webview.clearView();
        webview.measure(100, 100);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);


        WebClientClass webViewClient = new WebClientClass();
        webview.setWebViewClient(webViewClient);
       /* webview.getSettings().setDisplayZoomControls(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        WebSettings settings1 = webview.getSettings();

        settings1.setDefaultTextEncodingName("utf-8");
       // webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        Log.d("System out","Response of link "+URL);
        String url ="<iframe src=\""+URL+"\"></iframe>";
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width=display.getWidth();

      *//*  String data="<html><head><title>Example</title><meta name=\"viewport\"\"content=\"width="+width+", initial-scale=0.65 \" /></head>";
        data=data+"<body><center><img width=\""+width+"\" src=\""+URL+"\" /></center></body></html>";
        webview.loadData(data, "text/html", null);*//*
       // String URL=Constants.BASE_URL+"api/RSA_Ver1/PolicyPurchase?id="+id+"&code="+code;
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            //webview.loadUrl("https://www.instamojo.com/roadZEN/strandd-subscription-a/?data_readonly=data_amount&embed=form&data_amount=200&data_Field_26118=BPCL_BASIC&data_Field_79418=db59fc03-3489-49e2-90c8-8cbdfb7bf9a0&data_hidden=data_Field_79418");
            //webview.loadUrl(url);
        webview.loadDataWithBaseURL(null,url, "text/html", "UTF-8", null);
        //webView.loadDataWithBaseURL(null, "<iframe src=\"http://files.flipsnack.com/iframe/embed.html?hash=fdk843nc&wmode=window&bg‌​color=EEEEEE&t=1381431045\" width=\"640\" height=\"385\" seamless=\"seamless\" scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\"></iframe>", "text/html", "UTF-8", null);

        } else {
        }*/



        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


                    String json = "[{\"vehicleUmId\":\""+otp.getUmId()+"\"}]";
                    Log.d("System out", "In Get Vehicle List  " + json);

                    GetVehicleListAPI(json,error_layout,true);

                }
                else
                {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                }
            }
        });









        dialog.show();

    }
    private void GetVehicleListAPI(final String json, final RelativeLayout error_layout, final boolean flag) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<GetVehicleDetails>> call = apiService.GetVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<GetVehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVehicleDetails>> call, Response<ArrayList<GetVehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();

                    ArrayList<GetVehicleDetails> getVehicleList= response.body();
                    if (getVehicleList.get(0).getResStatus() == true)
                    {
                        //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);
                        boolean temp=false;

                            for(int i=0 ;i<getVehicleList.size();i++)
                            {
                                Log.d("System out","selected Vehicle id "+(vehicledetails.getVehicleId()));
                                if(vehicledetails.getVehicleId() == getVehicleList.get(i).getVehicleId())
                                {
                                    Log.d("System out","selected Vehicle subscription "+getVehicleList.get(i).getSubscriptionID());
                                    Date datecurrent = null;
                                    Date dateExpiry = null;

                                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                                    Calendar calendar = Calendar.getInstance();

                                    String temp1="";
                                    String eDate="";
                                    temp1=sdf1.format(calendar.getTime());
                                    // eDate=getVehicleList.get(i).getSubscriptionExpiry().replace("T"," ");


                                    try {
                                        datecurrent = sdf1.parse(temp1);
                                        eDate= Constants.formatDate(vehicledetails.getSubscriptionExpiry().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy hh:mm a");

                                        dateExpiry=sdf1.parse(eDate);

                                        Log.d("System out","eDate___"+eDate);
                                        Log.d("System out","current date__"+datecurrent);
                                        Log.d("System out","Expiry date__"+dateExpiry+"  "+datecurrent.after(dateExpiry));
                                    }
                                    catch (ParseException e)
                                    {
                                        e.printStackTrace();
                                    }
                                    if(getVehicleList.get(i).getSubscriptionID().equalsIgnoreCase("") || datecurrent.after(dateExpiry) )
                                    {
                                        //  Constants.show_error_popup(getActivity(), "Payment successful. ", error_layout);

                                    }else
                                    {
                                        temp=true;
                                        vehicledetails=getVehicleList.get(i);
                                        break;
                                    }
                                }
                            }

                            if (temp == true)
                            {
                               // Constants.show_error_popup(getActivity(), "Payment successful. ", error_layout);
                                Bundle b = new Bundle();
                                b.putSerializable("packagedetails", subsciptionPlansArrayList);
                                b.putSerializable("vehicledetails",vehicledetails);
                                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new SuccessfulRSA(),b);


                            }
                            else
                            {

                                Constants.show_error_popup(getActivity(), "Payment request not processed. ", error_layout);

                            }
                        }
                        else {


                        }




                }


            }

            @Override
            public void onFailure(Call<ArrayList<GetVehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }


    public class WebClientClass extends WebViewClient {
        // ProgressDialog pd = null;

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("System out", "Payment Method_first in loading " + url);
            view.loadUrl(url);
            return true;
            //return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);



            if (!url.equals("")) {

                Log.e("System out", "Patment Method_first " + url); // success=2
                if (url.endsWith("sucess") || url.endsWith("Sucess")) {



                } else if (url.endsWith("Fail") || url.endsWith("fail")) {


                    Toast.makeText(getActivity(),"Payment could not processed.", Toast.LENGTH_SHORT);


                }


            } else {

            }


        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler,
                                       SslError error) {

            //super.onReceivedSslError(view, handler, error);
            //handler.proceed();
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("SSL certification invalid");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();


        }
    }

}
