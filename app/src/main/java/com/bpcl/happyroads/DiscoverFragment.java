package com.bpcl.happyroads;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.google.gson.Gson;
import com.bpcl.happyroads.Pojo.AllTravelType;
import com.bpcl.happyroads.Pojo.DestinationName;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.MyTextView;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.transitionseverywhere.ArcMotion;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.TransitionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.Gravity.BOTTOM;
import static android.view.Gravity.CENTER;
import static android.view.Gravity.CENTER_HORIZONTAL;
import static android.view.Gravity.CENTER_VERTICAL;
import static android.view.Gravity.LEFT;
import static android.view.Gravity.RIGHT;
import static android.view.Gravity.TOP;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class DiscoverFragment extends Fragment {
    @Bind(R.id.ImgKnownDesti)
    ImageView ImgKnownDesti;
    @Bind(R.id.TvKnownDesti)
    TextView TvKnownDesti;
    @Bind(R.id.EDKNownDesti)
    AutoCompleteTextView EDKNownDesti;
    @Bind(R.id.linen)
    LinearLayout linen;
    @Bind(R.id.LLKNownDesti)
    LinearLayout LLKNownDesti;
    @Bind(R.id.ImgNearbyDesti)
    ImageView ImgNearbyDesti;
    @Bind(R.id.TvNearbyDesti)
    TextView TvNearbyDesti;
    @Bind(R.id.sb)
    SeekBar sb;
    /*@Bind(R.id.edcityname)
    TextView edcityname;*/

    @Bind(R.id.LLNearbyDesti)
    LinearLayout LLNearbyDesti;
    @Bind(R.id.transitionsContainer)
    FrameLayout transitionsContainer;
    @Bind(R.id.llmain)
    LinearLayout llmain;
    @Bind(R.id.ExploreList)
    ListView ExploreList;
    @Bind(R.id.transimg)
    ImageView transimg;
    @Bind(R.id.discoverbtn)
    MyTextView discoverbtn;
    ExploreAdapter exploreAdapter;
    Boolean EffectDone = false;
    View firstChildInList;
    Boolean scrollednot = false;
    String posi = "", DestinationId;
    @Bind(R.id.fl_discover_points)
    FrameLayout flDiscoverPoints;
    @Bind(R.id.LLsubNearbyDesti)
    LinearLayout LLsubNearbyDesti;
    @Bind(R.id.error_layout)
    RelativeLayout errorLayout;
    private int screenHeight;
    ArrayList<String> addedlist = new ArrayList<>();
    int originasize = 0;
    int scroll=0;
    //  ImageView ExploreCellImage;
    public int currentimageindex = 0;
    Timer timer;
    TimerTask task;
    Gson gson = new Gson();

    boolean mToRightAnimation;
    MyTextView ExploreCellCategoryName;
    ArrayList<String> selectedCatList = new ArrayList<>();
    ArrayList<AllTravelType> listOfCat = new ArrayList<>();

    ContentLoadingProgressBar discover_content_progressbar;
    ArrayAdapter cityadapter;
  //  String selectedCat = "", selectedIDs = "";
    ArrayList<DestinationName> cityListOfpopup = new ArrayList<DestinationName>();
    ArrayList<String> searchcitiesname = new ArrayList<String>();
    ArrayList<String> searchcitiesnameby;
    ArrayList<String> searchcitiesids = new ArrayList<String>();
    ArrayList<ExploreDestination> exploreDestList = new ArrayList<>();

    Dialog dialog;
    /* int[] imgs = new int[]{R.drawable.img_hom1, R.drawable.img_hom2,
            R.drawable.img_hom3, R.drawable.img_hom4};*/
    String[] Stringsss = null;
    String[] ids = null;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private ImagePipelineConfig imagePipelineConfig;
     String progress="200";
    private float px;
   TextView tv_max_progress,tv_min_progress,tv_avg_progress,tv_progress;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.discover_fragment, container, false);
        ButterKnife.bind(this, v);
        tv_max_progress=(TextView)v.findViewById(R.id.tv_max_progress);
        tv_min_progress=(TextView)v.findViewById(R.id.tv_min_progress);
        tv_avg_progress=(TextView)v.findViewById(R.id.tv_avg_progress);
        tv_progress=(TextView)v.findViewById(R.id.tv_progress);
        ((ExploreActivity)getActivity()).whichActivity=false;
        Log.d("system out","OnR in main");

        //  discover_content_progressbar = (ContentLoadingProgressBar) v.findViewById(R.id.discover_content_progressbar);
      //  discover_content_progressbar.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ExploreList.setNestedScrollingEnabled(true);
        }


        Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));
        Constants.destSelectionfrom="discover";
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        ((ExploreActivity) getActivity()).edcityname1.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).edcityname.setVisibility(View.GONE);
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            getTravelType();
            getAllKnownDestination("");
            ((ExploreActivity)getActivity()).getAllCityList("");

            }else
        {
          Toast.makeText(getActivity(),""+getActivity().getString(R.string.internet_error),Toast.LENGTH_SHORT).show();
        }
        ((ExploreActivity) getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity) getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_menu_header.setVisibility(View.VISIBLE);
        exploreAdapter = new ExploreAdapter(listOfCat);
        ExploreList.setAdapter(exploreAdapter);
        //   ((ExploreActivity)getActivity()).ll_img_type.setVisibility(View.GONE);


        ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //((ExploreActivity)getActivity()).onBackPressed();
            }
        });

        EDKNownDesti.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                EDKNownDesti.setThreshold(1);
                if (searchcitiesname.size() > 0) {

                    searchcitiesnameby = new ArrayList<String>();


                    for (int i = 0; i < searchcitiesname.size(); i++) {
                        String str = EDKNownDesti.getText().toString();
                        String origincityselected = ((ExploreActivity) getActivity()).edcityname1.getText().toString().replace("Ex - ","").trim();
                        String cityname = searchcitiesname.get(i);
                        Log.d("System out", " comparition " + searchcitiesname.get(i).toLowerCase() + "      " + str.toLowerCase());
                        Log.d("System out", " comparition  value" + searchcitiesname.get(i).toLowerCase().contains(str.toLowerCase()));
                        if (!searchcitiesname.get(i).equalsIgnoreCase(origincityselected) && searchcitiesname.get(i).toLowerCase().contains(str.toLowerCase())) {
                            searchcitiesnameby.add(searchcitiesname.get(i));
                        }
                    }
                    cityadapter = new ArrayAdapter<String>(getActivity(), R.layout.custontextview, searchcitiesnameby);
                    EDKNownDesti.setAdapter(cityadapter);

                }

                //}

            }

            @Override
            public void afterTextChanged(Editable s) {
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }


        });
        EDKNownDesti.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Log.d("System out", "in DescRoute " + searchcitiesname.size() + "  " + searchcitiesname.size() + "  " + pos);

                Constants.hidekeyboard(getActivity(),EDKNownDesti);
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    getDestinationAPI();

                } else {
                    Toast.makeText(getActivity(), "" + R.string.internet_error, Toast.LENGTH_SHORT).show();

                }
            }
        });

       // originasize = llmain.getHeight();
        originasize = (int)getResources().getDimension(R.dimen._80sdp);
        // ExploreList.setEnabled(false);
        ExploreList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Constants.hidekeyboard(getActivity(),ExploreList);

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                 scroll = getScroll(ExploreList);
                Log.d("System out", "Scroll: " + scroll);
                // EffectDone = false;
                if (scroll > 120 && !scrollednot) {
                    RefreshEffects();
                    changeHeight(llmain, scroll);
                } else if (scroll < 30 && scrollednot) {
                    RefreshEffects();
                    scrollednot = false;
                    Refreshlayoutparams();

                    ImgNearbyDesti.setImageResource(R.drawable.nearby_destination_icon_home);
                    ImgKnownDesti.setImageResource(R.drawable.known_destination_icon_home);
                    TransitionManager.beginDelayedTransition(transitionsContainer,
                            new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(300));
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) LLKNownDesti.getLayoutParams();

                    params.gravity = mToRightAnimation ? (CENTER_HORIZONTAL | TOP) : (LEFT | CENTER_VERTICAL);
                    params.setMargins(0, 0, (int) getResources().getDimension(R.dimen._10sdp), 0);
                    LLKNownDesti.setLayoutParams(params);
                    TvKnownDesti.setVisibility(View.VISIBLE);


                    FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) LLNearbyDesti.getLayoutParams();
                    params1.gravity = mToRightAnimation ? (RIGHT | TOP) : (CENTER | CENTER_VERTICAL);
                    // params1.setMargins(0,0,80,0);
                    LLNearbyDesti.setLayoutParams(params1);
                    TvNearbyDesti.setVisibility(View.VISIBLE);
                    Log.d("System out", "llmain height: " + llmain.getHeight());
                    llmain.getLayoutParams().height = originasize;
                }

            }
        });
        if (addedlist.size() > 0) {
            discoverbtn.setVisibility(View.VISIBLE);
        } else {
            discoverbtn.setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources r = getResources();
        px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,0, r.getDisplayMetrics());
      // tv_progress.setText(progress+" kms");
      // set_progress_seekbar(Integer.valueOf(progress));
        sb.setProgress(200);
    //    tv_max_progress.setVisibility(View.VISIBLE);

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
              //  set_progress_seekbar(i);
             // setSeekBar(i);
                tv_progress.setText(i+"\nKMs");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

/*
                if (addedlist.size() == 0 && LLsubNearbyDesti.getVisibility() == View.VISIBLE)

                {
                    //   sb.getProgress();

                    SubListingFragment subListingFragment = new SubListingFragment();
                    FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    //((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                    changeTransaction.replace(R.id.frame, subListingFragment, "SubListingFragment");
                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    // Constants.SelectedCat=selectedCat;
                    bundle.putString("name", selectedCat);
                    bundle.putString("nameids", selectedIDs);
                    String progress = "999999";
                    if (sb.getVisibility() == View.VISIBLE) {
                        progress = String.valueOf(sb.getProgress());
                    }
                    bundle.putString("progress", progress);
                    bundle.putString("from", "discover from nearby destination");

                    subListingFragment.setArguments(bundle);
                    changeTransaction.commit();
                }
*/
            }
        });

    }

    public void getTravelType() {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
      //  discover_content_progressbar.setVisibility(View.VISIBLE);
        String serialNo = Build.SERIAL;
        String Manufecturer = Build.MANUFACTURER;


        // Device model
        String PhoneModel = Build.MODEL;
        // Android version
        String AndroidVersion = Build.VERSION.SDK;
        String AndroidVersionNAme = Build.VERSION.CODENAME;


        Log.e("System out", "Device Detail" + "\n serial no" + serialNo + "Manufacturer" + Manufecturer + "Phone Model" + PhoneModel + "Android Version" + AndroidVersion + "AndroidVersionNAme" + AndroidVersionNAme);
//ex : json: {"MobileLog": "Xiomi Mi 3, Marshmello"}

        Call<ArrayList<AllTravelType>> call = apiService.allTravellType();
        call.enqueue(new Callback<ArrayList<AllTravelType>>() {
            @Override
            public void onResponse(Call<ArrayList<AllTravelType>> call, Response<ArrayList<AllTravelType>> response) {
                if(pb_dialog!=null)
                pb_dialog.dismiss();
                if (response.body() != null) {


                    listOfCat.clear();
                    listOfCat.addAll(response.body());
                    if (listOfCat.size() > 0) {


                        String json = gson.toJson(listOfCat); // myObject - instance of MyObject
                        editor.putString("CategoryList", json);
                        editor.commit();
                        DisplayMetrics metrics = new DisplayMetrics();
                        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        screenHeight = metrics.heightPixels;
                        Stringsss = new String[listOfCat.size()];
                        ids = new String[listOfCat.size()];
                        for (int i = 0; i < listOfCat.size(); i++) {
                            Stringsss[i] = listOfCat.get(i).getTTypeName();
                            ids[i] = String.valueOf(listOfCat.get(i).getTTypeId());
                        }

                       // discover_content_progressbar.setVisibility(View.GONE);
                        ExploreAdapter     exploreAdapter = new ExploreAdapter(listOfCat);
                        if(ExploreList!=null)
                        ExploreList.setAdapter(exploreAdapter);
                    } else {
                       // discover_content_progressbar.setVisibility(View.GONE);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<AllTravelType>> call, Throwable t) {
                // Log error here since request failed
               // discover_content_progressbar.setVisibility(View.GONE);
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                }
            }
        });
    }


    protected int getScroll(ListView listView) {// as list recycles views , getscrollY wont give us how much it has scrolled, hence we use this hack
        if(listView!=null)
        firstChildInList = listView.getChildAt(0);
        if (firstChildInList == null) return 0;
        return -firstChildInList.getTop() + listView.getFirstVisiblePosition() * firstChildInList.getHeight();
    }

    protected void changeHeight(final View view, final int scroll) { // this is a simple logic , this is a little shaky , but its the way to go , you can smoothen from here
        int priceHeight = view.getHeight();
        Log.d("System out", "Price height: " + priceHeight + " \n screenheight/4: "
                + (screenHeight / 6) + " \n screenHeight / 2: "
                + screenHeight / 3 + " Scroll: " + scroll);

        scrollednot = true;
        Refreshlayoutparams();

        ImgNearbyDesti.setImageResource(R.drawable.nearby_destination_icon_unselected);
        ImgKnownDesti.setImageResource(R.drawable.known_destination_icon_unselected);
        TransitionManager.beginDelayedTransition(transitionsContainer,
                new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(300));
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) LLKNownDesti.getLayoutParams();
        // change 13/9/16 --->     params.setMargins(30, 0, (int) getResources().getDimension(R.dimen._5sdp), 0);
/*params.setMargins(-(int) getResources().getDimension(R.dimen._15sdp), 0, (int) getResources().getDimension(R.dimen._5sdp), 0);
        params.gravity = mToRightAnimation ? (LEFT | BOTTOM) : (CENTER_HORIZONTAL | TOP);
        LLKNownDesti.setLayoutParams(params);
        TvKnownDesti.setVisibility(View.INVISIBLE);


        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) LLNearbyDesti.getLayoutParams();
        params1.gravity = mToRightAnimation ? (RIGHT | BOTTOM) : (CENTER | TOP);

        LLNearbyDesti.setLayoutParams(params1);
        TvNearbyDesti.setVisibility(View.INVISIBLE);
        Log.d("System out", "llmain height: " + llmain.getHeight());
        originasize = llmain.getHeight();
        llmain.getLayoutParams().height = screenHeight * 7 / 100;*/

        params.setMargins(-(int) getResources().getDimension(R.dimen._15sdp), 0, (int) getResources().getDimension(R.dimen._5sdp), 0);
        params.gravity = mToRightAnimation ? (LEFT | BOTTOM) : (CENTER_HORIZONTAL | TOP);
        LLKNownDesti.setLayoutParams(params);
        TvKnownDesti.setVisibility(View.INVISIBLE);


        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) LLNearbyDesti.getLayoutParams();
        params1.setMargins((int) getResources().getDimension(R.dimen._90sdp), 0, (int) getResources().getDimension(R.dimen._5sdp), 0);

        params1.gravity = mToRightAnimation ? (RIGHT | BOTTOM) : (CENTER | TOP);

        LLNearbyDesti.setLayoutParams(params1);
        TvNearbyDesti.setVisibility(View.INVISIBLE);
        Log.d("System out", "llmain height: " + llmain.getHeight());
        originasize = llmain.getHeight();
        llmain.getLayoutParams().height = screenHeight * 7 / 100;
    }

    private void Refreshlayoutparams() {

        FrameLayout.LayoutParams LLKNownDestiParams
                = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, CENTER_VERTICAL);
        // LLKNownDestiParams.setMargins(0,0,80,0);
        LLKNownDesti.setLayoutParams(LLKNownDestiParams);
        FrameLayout.LayoutParams LLNearbyDestiParams
                = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, RIGHT);
        LLNearbyDestiParams.setMargins((int) getResources().getDimension(R.dimen._75sdp), 0, 0, 0);

        LLNearbyDesti.setLayoutParams(LLNearbyDestiParams);
    }

    @OnClick({R.id.LLKNownDesti, R.id.LLNearbyDesti, R.id.discoverbtn, R.id.transimg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.LLKNownDesti:

                if (originasize > 0) {
                    llmain.getLayoutParams().height = originasize;
                }
                // if (!EffectDone) {
                //  llmain.getLayoutParams().height=originasize;
                //  addedlist=new ArrayList<>();

                if (LLsubNearbyDesti.getVisibility() == View.VISIBLE) {

                    LLsubNearbyDesti.setVisibility(View.GONE);
                    ImgNearbyDesti.setVisibility(View.VISIBLE);
                    TvNearbyDesti.setVisibility(View.GONE);
                }

                transimg.setVisibility(View.VISIBLE);
                EffectDone = true;
                ExploreList.setEnabled(false);
                if (exploreAdapter != null) {
                    if (exploreAdapter != null) {
                        exploreAdapter.notifyDataSetChanged();

                    }

                }
                FrameLayout.LayoutParams LLNearbyDestiParams
                        = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, RIGHT);
                // LLNearbyDestiParams.setMargins((int) getResources().getDimension(R.dimen._35sdp), 0, 0, 0);
                LLNearbyDestiParams.setMargins((int) getResources().getDimension(R.dimen._55sdp), 0, (int) getResources().getDimension(R.dimen._5sdp), 0);

                LLNearbyDesti.setLayoutParams(LLNearbyDestiParams);

                LLKNownDesti.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, LEFT | BOTTOM));
//                    LLNearbyDesti.setLayoutParams(new FrameLayout.LayoutParams(
//                            ViewGroup.LayoutParams.WRAP_CONTENT,
//                            ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.BOTTOM));
                TvNearbyDesti.setVisibility(View.GONE);
                TransitionManager.beginDelayedTransition(transitionsContainer,
                        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(1000));
                ImgNearbyDesti.setImageResource(R.drawable.nearby_destination_icon_unselected);
                ImgKnownDesti.setImageResource(R.drawable.destination_icon_home);
                TvKnownDesti.setVisibility(View.GONE);
                EDKNownDesti.setVisibility(View.VISIBLE);
                EDKNownDesti.setText("");

                linen.setVisibility(View.VISIBLE);
                //mToRightAnimation = !mToRightAnimation;
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) LLNearbyDesti.getLayoutParams();
                params.gravity = mToRightAnimation ? (RIGHT | BOTTOM) : (CENTER_HORIZONTAL | TOP);
                LLNearbyDesti.setLayoutParams(params);
                //  EffectDone = true;
                //  discoverbtn.setVisibility(View.VISIBLE);
                if (discoverbtn.getVisibility() == View.GONE) {
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_up);
                    // MyTextView hiddenPane l = (MyTextView) findViewById(R.id.discoverbtn);
                    discoverbtn.startAnimation(bottomUp);
                    discoverbtn.setVisibility(View.VISIBLE);
                }



                break;
            case R.id.LLNearbyDesti:

                sb.setProgress(200);
                tv_progress.setText("200"+"\nKMs");

/*

                tv_avg_progress.setVisibility(View.VISIBLE);
                tv_max_progress.setVisibility(View.VISIBLE);
                tv_min_progress.setVisibility(View.VISIBLE);


                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.radius_icon_home_slider3);
                Bitmap bmp = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                Canvas c = new Canvas(bmp);
                //   String text = Integer.toString(seekBar.getProgress());
                Paint p = new Paint();
                p.setTypeface(Typeface.DEFAULT);
                p.setTextSize(getResources().getDimension(R.dimen._6sdp));
                p.setColor(getResources().getColor(android.R.color.white));
                p.setTextAlign(Paint.Align.LEFT);
                sb.setThumb(new BitmapDrawable(getResources(), bmp));

                int width = (int) p.measureText("");
                int yPos = (int) ((c.getHeight()-10));
               // c.drawText(" "+String.valueOf(progress1)+" kms  ", (bmp.getWidth() - width) / 2, yPos, p);
*/


                Constants.hidekeyboard(getActivity(), LLNearbyDesti);


                if (originasize > 0) {
                    llmain.getLayoutParams().height = originasize;
                }
                // if (!EffectDone) {

                if (EDKNownDesti.getVisibility() == View.VISIBLE) {
                    EDKNownDesti.setVisibility(View.GONE);
                    TvKnownDesti.setVisibility(View.VISIBLE);
                    linen.setVisibility(View.GONE);
                }
                //   addedlist=new ArrayList<>();

                transimg.setVisibility(View.VISIBLE);
                EffectDone = true;
                ExploreList.setEnabled(false);
                if (exploreAdapter != null) {
                    exploreAdapter.notifyDataSetChanged();

                }
                FrameLayout.LayoutParams LLKNownDestiParams
                        = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, LEFT);
                // LLKNownDestiParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen._35sdp), 0);
               // LLKNownDestiParams.setMargins(-(int) getResources().getDimension(R.dimen._50sdp), 0, (int) getResources().getDimension(R.dimen._5sdp), (int) getResources().getDimension(R.dimen._10sdp));
                LLKNownDestiParams.setMargins(-(int) getResources().getDimension(R.dimen._50sdp), -(int) getResources().getDimension(R.dimen._5sdp), (int) getResources().getDimension(R.dimen._5sdp), (int) getResources().getDimension(R.dimen._10sdp));

                LLKNownDesti.setLayoutParams(LLKNownDestiParams);
                LLNearbyDesti.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, RIGHT | BOTTOM));
//                    LLKNownDesti.setLayoutParams(new FrameLayout.LayoutParams(
//                            ViewGroup.LayoutParams.WRAP_CONTENT,
//                            ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.LEFT | Gravity.BOTTOM));
                TvKnownDesti.setVisibility(View.GONE);
                TransitionManager.beginDelayedTransition(transitionsContainer,
                        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(1000));
                ImgKnownDesti.setImageResource(R.drawable.known_destination_icon_unselected);

                //mToRightAnimation = !mToRightAnimation;
                FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) LLKNownDesti.getLayoutParams();
                params1.gravity = mToRightAnimation ? (CENTER_HORIZONTAL | BOTTOM) : (CENTER_HORIZONTAL | TOP);
                LLKNownDesti.setLayoutParams(params1);
               // addedlist.clear();

                LLsubNearbyDesti.setVisibility(View.VISIBLE);
                ImgNearbyDesti.setVisibility(View.GONE);
                TvNearbyDesti.setVisibility(View.GONE);
                // EffectDone = true;
                if (discoverbtn.getVisibility() == View.GONE) {
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_up);
                    discoverbtn.startAnimation(bottomUp);
                    discoverbtn.setVisibility(View.VISIBLE);
                }

                break;

            /*case R.id.exploreheadersearch:
                showPopup();
                break;*/
            case R.id.discoverbtn:

                 if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                     ((ExploreActivity)getActivity()).clearALLFilter();

                     if (addedlist.size() == 0 && LLsubNearbyDesti.getVisibility() == View.GONE) {
                        //((ExploreActivity) getActivity()).replace_fragmnet(new ListingDetailsFragment());
                        boolean falg = false;
                        for (int i = 0; i < searchcitiesname.size(); i++) {
                            if (EDKNownDesti.getText().toString().toString().equalsIgnoreCase(searchcitiesname.get(i).trim())) {
                                falg = true;
                                break;
                            }
                        }

                        if(EDKNownDesti.getText().toString().equalsIgnoreCase(""))
                        {
                            Constants.show_error_popup(getActivity(), "Enter your destination.", errorLayout);
                        }
                        else {

                            if (falg == true)
                                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                    getDestinationAPI();

                                }
                                else
                                {

                                    Toast.makeText(getActivity(), "" + R.string.internet_error, Toast.LENGTH_SHORT).show();

                                }

                            else {
                                Constants.show_error_popup(getActivity(), "We are yet to explore this destination for you. Try another.", errorLayout);
                                EDKNownDesti.setText("");

                            }
                        }
                    } else if (addedlist.size() == 0 || LLsubNearbyDesti.getVisibility() == View.VISIBLE)

                    {
                        //   sb.getProgress();

                        SubListingFragment subListingFragment = new SubListingFragment();
                        FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        //((ExploreActivity)getActivity()).setHeader("Change Password");
                        changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                        changeTransaction.replace(R.id.frame, subListingFragment,"SubListingFragment");
                        changeTransaction.addToBackStack(null);
                        Bundle bundle = new Bundle();
                        // Constants.SelectedCat=selectedCat;
                        bundle.putString("name", ((ExploreActivity)getActivity()).selectedCat);
                        bundle.putString("nameids", ((ExploreActivity)getActivity()).selectedIDs);
                        String progress = "999999";
                        if (sb.getVisibility() == View.VISIBLE) {
                            progress = String.valueOf(sb.getProgress());
                        }
                        bundle.putString("progress", progress);
                        bundle.putString("from", "discover from nearby destination");

                        subListingFragment.setArguments(bundle);
                        changeTransaction.commit();
                    } else {
                        SubListingFragment subListingFragment = new SubListingFragment();
                        FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        //  ((ExploreActivity)getActivity()).setHeader("Change Password");
                        changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                        changeTransaction.replace(R.id.frame, subListingFragment,"SubListingFragment");
                        changeTransaction.addToBackStack(null);
                        Bundle bundle = new Bundle();
                        //Constants.SelectedCat=selectedCat;
                        bundle.putString("name", ((ExploreActivity)getActivity()).selectedCat);
                        bundle.putString("nameids", ((ExploreActivity)getActivity()).selectedIDs);
                        bundle.putString("progress", "999999");
                        bundle.putString("from", "discover");
                        subListingFragment.setArguments(bundle);
                        changeTransaction.commit();
                    }
                } else {
                   //  Toast.makeText(getActivity(),""+getActivity().getString(R.string.internet_error),Toast.LENGTH_SHORT).show();
                     Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));


                 }


                break;
            case R.id.transimg:
                transimg.setVisibility(View.GONE);
                Constants.hidekeyboard(getActivity(),EDKNownDesti);
                EffectDone = false;
                ExploreList.setEnabled(true);
                if (exploreAdapter != null) {
                    exploreAdapter.notifyDataSetChanged();

                }
                LLKNownDesti.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, LEFT | CENTER_VERTICAL));
                LLNearbyDesti.setLayoutParams(new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, RIGHT | CENTER_VERTICAL));
                TransitionManager.beginDelayedTransition(transitionsContainer,
                        new ChangeBounds().setPathMotion(new ArcMotion()).setDuration(1000));
                FrameLayout.LayoutParams params2 = (FrameLayout.LayoutParams) LLKNownDesti.getLayoutParams();
                params2.gravity = mToRightAnimation ? (CENTER_HORIZONTAL | TOP) : (LEFT | CENTER_VERTICAL);
                LLKNownDesti.setLayoutParams(params2);
                ImgNearbyDesti.setImageResource(R.drawable.nearby_destination_icon_home);
                ImgKnownDesti.setImageResource(R.drawable.known_destination_icon_home);
                EffectDone = false;

                LLsubNearbyDesti.setVisibility(View.GONE);
                EDKNownDesti.setVisibility(View.GONE);
                linen.setVisibility(View.GONE);
                ImgNearbyDesti.setVisibility(View.VISIBLE);
                TvNearbyDesti.setVisibility(View.VISIBLE);
                TvKnownDesti.setVisibility(View.VISIBLE);
                // discoverbtn.setVisibility(View.GONE);

                if (addedlist.size() == 0 && discoverbtn.getVisibility() == View.VISIBLE) {
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_down);
                    discoverbtn.startAnimation(bottomUp);
                    discoverbtn.setVisibility(View.GONE);
                }

                break;
        }
    }

    private void getAllKnownDestination(String cityname) {

        String json = "[{\"desName\":\"" + cityname + "\"}]";

        Log.d("System out", "json " + json);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ArrayList<DestinationName>> call = apiService.GetDestinationList(json);
        call.enqueue(new Callback<ArrayList<DestinationName>>() {
            @Override
            public void onResponse(Call<ArrayList<DestinationName>> call, Response<ArrayList<DestinationName>> response) {


                if (response.body() != null) {
                    if (response.body().get(0).getResStatus() == true) {
                        Log.e("System out", "city name get is::" + response.body().get(0).getDesName());
                        Log.e("System out", "city id get is::" + response.body().get(0).getDesID());
                        cityListOfpopup.clear();
                        cityListOfpopup.addAll(response.body());
                        searchcitiesname.clear();
                        for (int i = 0; i < cityListOfpopup.size(); i++) {
                            searchcitiesname.add(cityListOfpopup.get(i).getDesName());
                            searchcitiesids.add(String.valueOf(cityListOfpopup.get(i).getDesID()));
                        }
                    }

                }


            }

            @Override
            public void onFailure(Call<ArrayList<DestinationName>> call, Throwable t) {
                // Log error here since request failed
                t.printStackTrace();


            }
        });

    }


    private void RefreshEffects() {
        LLKNownDesti.setLayoutParams(new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, CENTER_VERTICAL));
        FrameLayout.LayoutParams LLNearbyDestiParams
                = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, CENTER);
        LLNearbyDestiParams.setMargins((int) getResources().getDimension(R.dimen._40sdp), 0, 0, 0);
        LLNearbyDesti.setLayoutParams(LLNearbyDestiParams);
        ImgNearbyDesti.setVisibility(View.VISIBLE);
        ImgKnownDesti.setImageResource(R.drawable.known_destination_icon_home);
        ImgNearbyDesti.setImageResource(R.drawable.nearby_destination_icon_home);
        TvNearbyDesti.setVisibility(View.VISIBLE);
        TvKnownDesti.setVisibility(View.VISIBLE);
        EDKNownDesti.setVisibility(View.GONE);
        linen.setVisibility(View.GONE);
        LLsubNearbyDesti.setVisibility(View.GONE);
        if (addedlist.size() == 0 && discoverbtn.getVisibility() == View.VISIBLE) {
            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                    R.anim.bottom_down);
            discoverbtn.startAnimation(bottomUp);
            discoverbtn.setVisibility(View.GONE);
        } else {
            // discoverbtn.setVisibility(View.VISIBLE);
        }
    }

    private class ExploreAdapter extends BaseAdapter {
        private ArrayList<AllTravelType> arraylist;

        ExploreAdapter(ArrayList<AllTravelType> listCat) {
            this.arraylist = listCat;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.explorecell, null);

            final MyTextView ExploreCellCategoryName = (MyTextView) v.findViewById(R.id.ExploreCellCategoryName);

            SimpleDraweeView ExploreCellImage = (SimpleDraweeView) v.findViewById(R.id.ExploreCellImage);
           /* final Handler mHandler = new Handler();
            int delay = 1000; // delay for 1 sec.

            int period = 8000; // repeat every 4 sec.

            Timer timer = new Timer();

            timer.scheduleAtFixedRate(new TimerTask() {

                public void run() {

                    mHandler.post(mUpdateResults);

                }

            }, delay, period);
            final Runnable mUpdateResults = new Runnable() {
                public void run() {

                    AnimateandSlideShow();

                }

                private void AnimateandSlideShow() {
                  //  ExploreCellImage = (ImageView)findViewById(R.id.ImageView3_Left);
                    ExploreCellImage.setImageResource(imgs[currentimageindex%imgs.length]);

                    currentimageindex++;

                    Animation rotateimage = AnimationUtils.loadAnimation(this, R.anim.custom_anim);

                    ExploreCellImage.startAnimation(rotateimage);
                }
            };*/


            //  final LinearLayout ll_main_list_imgs=(LinearLayout)v.findViewById(R.id.ll_main_list_imgs);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* final String name=ExploreCellCategoryName.getText().toString();

                    SubListingFragment subListingFragment = new SubListingFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    ((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.replace(R.id.frame, subListingFragment);
                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("name",name);
                    subListingFragment.setArguments(bundle);
                    changeTransaction.commit();*/

                }
            });

            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            ExploreCellImage.setHierarchy(hierarchy);
            //  fc_p.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
            final ImageView ExploreCellCategorySelection = (ImageView) v.findViewById(R.id.ExploreCellCategorySelection);
            ExploreCellImage.setImageURI(Constants.TimbThumb_ImagePath + arraylist.get(position).getTTypeImage()+"&width="+(ExploreActivity.width));
            ExploreCellCategoryName.setText(arraylist.get(position).getTTypeName());
           /* if (addedlist.contains(position + "")) {
                // ExploreCellCategorySelection.setLiked(true);
                ExploreCellCategorySelection.setImageResource(R.drawable.round_home_selected);
                ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            } else {
                // ExploreCellCategorySelection.setLiked(false);
                ExploreCellCategorySelection.setImageResource(R.drawable.round_home_unselected);

                ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorWhite));
            }*/
            String[] abc = ((ExploreActivity)getActivity()).selectedCat.split(",");

            for (int i = 0; i < abc.length; i++) {

                if (abc[i].equalsIgnoreCase(arraylist.get(position).getTTypeName())) {
                    ExploreCellCategorySelection.setImageResource(R.drawable.round_home_selected);
                    ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.textYellow));
                }
            }


           /* if (EffectDone) {
                ExploreCellCategorySelection.setVisibility(View.INVISIBLE);
                ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorWhite));
            } else {
                ExploreCellCategorySelection.setVisibility(View.VISIBLE);
                //  ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            }*/

            ExploreCellCategorySelection.setId(position);
            ExploreCellCategorySelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // if (addedlist.contains(position + "")) {
                    if (((ExploreActivity)getActivity()).selectedCat.contains(arraylist.get(position).getTTypeName())) {
                        posi = position + "";
                        ExploreCellCategorySelection.setImageResource(R.drawable.round_home_unselected);

                        ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorWhite));
                        ((ExploreActivity)getActivity()).selectedCat = ((ExploreActivity)getActivity()).selectedCat.replace(Stringsss[ExploreCellCategorySelection.getId()] + ",", "");
                        ((ExploreActivity)getActivity()).selectedIDs = ((ExploreActivity)getActivity()).selectedIDs.replace(ids[ExploreCellCategorySelection.getId()] + ",", "");
                        Log.d("System out", "Name____ deselect " + ((ExploreActivity)getActivity()).selectedCat + "  " + ((ExploreActivity)getActivity()).selectedIDs);

                        if (addedlist.contains(posi)) {
                            addedlist.remove(posi);
                        }
                        if (addedlist.size() == 0) {
                            if (discoverbtn.getVisibility() == View.VISIBLE) {
                                Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                        R.anim.bottom_down);
                                discoverbtn.startAnimation(bottomUp);
                                discoverbtn.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        posi = position + "";
                        // Toast.makeText(ExploreActivity.this,"Liked...",Toast.LENGTH_SHORT).show();
                        ((ExploreActivity)getActivity()).selectedCat += Stringsss[ExploreCellCategorySelection.getId()] + ",";
                        ((ExploreActivity)getActivity()).selectedIDs += ids[ExploreCellCategorySelection.getId()] + ",";

                        Log.d("System out", "Name____" + ((ExploreActivity)getActivity()).selectedCat);
                        if (discoverbtn.getVisibility() == View.GONE) {
                            ExploreCellCategorySelection.setImageResource(R.drawable.round_home_selected);
                            ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.textYellow));

                            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                    R.anim.bottom_up);
                            discoverbtn.startAnimation(bottomUp);
                            discoverbtn.setVisibility(View.VISIBLE);
                        }
                        if (!addedlist.contains(posi)) {
                            addedlist.add(posi);
                        }
                    }
                    notifyDataSetChanged();
                }
            });

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addedlist.contains(position + "")) {
                        posi = position + "";
                        ExploreCellCategorySelection.setImageResource(R.drawable.round_home_unselected);
                        ExploreCellCategorySelection.setImageResource(R.drawable.round_home_unselected);

                        ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorWhite));
                        ((ExploreActivity)getActivity()).selectedCat = ((ExploreActivity)getActivity()).selectedCat.replace(Stringsss[ExploreCellCategorySelection.getId()] + ",", "");
                        ((ExploreActivity)getActivity()).selectedIDs = ((ExploreActivity)getActivity()).selectedIDs.replace(ids[ExploreCellCategorySelection.getId()] + ",", "");

                        Log.d("System out", "Name____ deselect " + ((ExploreActivity)getActivity()).selectedCat);

                        if (addedlist.contains(posi)) {
                            addedlist.remove(posi);
                        }
                        if (addedlist.size() == 0) {
                            if (discoverbtn.getVisibility() == View.VISIBLE) {
                                Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                        R.anim.bottom_down);
                                discoverbtn.startAnimation(bottomUp);
                                discoverbtn.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        posi = position + "";
                        // Toast.makeText(ExploreActivity.this,"Liked...",Toast.LENGTH_SHORT).show();
                        ((ExploreActivity)getActivity()).selectedCat += Stringsss[ExploreCellCategorySelection.getId()] + ",";
                        ((ExploreActivity)getActivity()).selectedIDs += ids[ExploreCellCategorySelection.getId()] + ",";
                        Log.d("System out", "Name____" + ((ExploreActivity)getActivity()).selectedCat);
                        if (discoverbtn.getVisibility() == View.GONE) {
                            ExploreCellCategorySelection.setImageResource(R.drawable.round_home_selected);
                            ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.textYellow));

                            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                    R.anim.bottom_up);
                            discoverbtn.startAnimation(bottomUp);
                            discoverbtn.setVisibility(View.VISIBLE);
                        }
                        if (!addedlist.contains(posi)) {
                            addedlist.add(posi);
                        }
                    }
                    notifyDataSetChanged();
                }
            });
         /*   ExploreCellCategorySelection.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    posi = position + "";
                    // Toast.makeText(ExploreActivity.this,"Liked...",Toast.LENGTH_SHORT).show();
                    selectedCat+=Stringsss[likeButton.getId()]+",";
                    Log.d("System out","Name____"+selectedCat);
                    if (discoverbtn.getVisibility() == View.GONE) {
                        ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.textYellow));

                        Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_up);
                        discoverbtn.startAnimation(bottomUp);
                        discoverbtn.setVisibility(View.VISIBLE);
                    }
                    if (!addedlist.contains(posi)) {
                        addedlist.add(posi);
                    }
                }
                @Override
                public void unLiked(LikeButton likeButton) {

                    posi = position + "";
                    ExploreCellCategoryName.setTextColor(getResources().getColor(R.color.colorWhite));
                    selectedCat=selectedCat.replace(Stringsss[likeButton.getId()]+",","");
                    Log.d("System out","Name____ deselect "+selectedCat);

                    if (addedlist.contains(posi)) {
                        addedlist.remove(posi);
                    }
                    if (addedlist.size() == 0) {
                        if (discoverbtn.getVisibility() == View.VISIBLE) {
                            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                    R.anim.bottom_down);
                            discoverbtn.startAnimation(bottomUp);
                            discoverbtn.setVisibility(View.GONE);
                        }
                    }
                }
            });*/


            return v;
        }
    }

    /*   public void run()
       {
           for(int i=0;i<3;i++)
           {
               ExploreCellImage.setImageResource(imgs[i]);
               //System.out.println("Sanat Pandey");
               try{
                   Thread.sleep(3000);
               }catch(Exception e)
               {
                   System.out.println(e);
               }
           }
       }
   */
    @Override
    public void onResume() {
        super.onResume();
        Log.d("System out","resume from discover");

        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        //  ((ExploreActivity)getActivity()).ll_img_type.setVisibility(View.GONE);
        //  ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.logo_navigation);
        Log.d("system out","addedlist"+addedlist.size());
        originasize = (int)getResources().getDimension(R.dimen._80sdp);


        ((ExploreActivity) getActivity()).edcityname1.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).edcityname.setVisibility(View.GONE);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        //  ((ExploreActivity)getActivity()).replace_fragmnet(new DiscoverFragment());


        // ((ExploreActivity)getActivity()).tv_text_header.setText("Notifications");
    }

    @Override

    public void onDestroyView() {
        ((ExploreActivity) getActivity()).toolbar.setBackgroundResource(0);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ExploreActivity) getActivity()).edcityname1.setVisibility(View.GONE);
    }

    private void getDestinationAPI() {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{     \"umId\": \"" + "1" + "\",     \"radius\": \"999999\",   \"desName\": \"" + EDKNownDesti.getText().toString().trim() + "\",   \"desLongitude\": \""+Constants.selectedLong+"\",   \"desLatitude\": \""+Constants.selectedLat+"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":0 }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Log.d("System out", " json " + json);
        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();

                if (response.body() != null) {

                    pb_dialog.dismiss();
                    exploreDestList.clear();
                    exploreDestList.addAll(response.body());
                    if (exploreDestList.size() > 0) {


                        Collections.reverse(exploreDestList);

                        String json = gson.toJson(exploreDestList); // myObject - instance of MyObject
                        editor.putString("DestList", json);
                        editor.commit();

                        ListingDetailsFragment listingDetailsFragment = new ListingDetailsFragment();
                        android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        // ((ExploreActivity)getActivity()).setHeader("Change Password");
                        changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                        changeTransaction.replace(R.id.frame, listingDetailsFragment);
                        changeTransaction.addToBackStack(null);
                        Bundle bundle = new Bundle();
                        bundle.putString("placeName",exploreDestList.get(0).getDesName());
                        bundle.putString("from","sublisting");
                        bundle.putString("position",0+"");
                        bundle.putSerializable("Explorlist", exploreDestList.get(0));

                        ExploreActivity.destattractionimages.clear();
                        for (int i=0;i<exploreDestList.get(0).getImageList().size();i++)
                        {
                            if(exploreDestList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                                ExploreActivity.destattractionimages.add(exploreDestList.get(0).getImageList().get(i).getImgName());
                            }
                        }


                        listingDetailsFragment.setArguments(bundle);
                        changeTransaction.commit();

                        //  asfd
                        //Constants.show_error_popup(getActivity(), ""+otplist.get(0).getResDescription(), error_layout);


                    } else {
                        Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(), errorLayout);
                        EDKNownDesti.setText("");
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                }

            }
        });
    }

    private void setSeekBar(Integer progress1) {

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.radius_icon_home);
        Bitmap bmp = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas c = new Canvas(bmp);
        //   String text = Integer.toString(seekBar.getProgress());
        Paint p = new Paint();
        p.setTypeface(Typeface.DEFAULT);
        p.setTextSize(getResources().getDimension(R.dimen._10sdp));
        p.setColor(getResources().getColor(android.R.color.white));
        p.setTextAlign(Paint.Align.LEFT);

        sb.setThumb(new BitmapDrawable(getResources(), bmp));
       /* tv_max_progress.setVisibility(View.INVISIBLE);
        tv_min_progress.setVisibility(View.INVISIBLE);
        tv_avg_progress.setVisibility(View.INVISIBLE);*/
        int width = (int) p.measureText(" "+progress1+" ");
        int yPos = (int) ((c.getHeight()));
        String kms=" kms ";

       // c.drawText(" "+String.valueOf(progress1)+" ", (bitmap.getWidth() - width)/2, yPos, p);

        if(progress1==0)
        {
            c.drawText("", (bmp.getWidth() - width) / 2, yPos, p);

        } else if(progress1==400)
        {
            c.drawText("", (bmp.getWidth() - width) / 2, yPos, p);

        }else
        {
            c.drawText(" "+String.valueOf(progress1), (bmp.getWidth() - width) / 2, yPos, p);

        }



       /* if(progress1<40)
        {
            tv_max_progress.setVisibility(View.VISIBLE);
            tv_min_progress.setVisibility(View.INVISIBLE);
            int width = (int) p.measureText("  "+progress1+" kms ");
            int yPos = (int) ((c.getHeight())-5);
            c.drawText("  "+String.valueOf(progress1)+" kms ", (bmp.getWidth() - width) / 2, yPos, p);

        }
        else if(progress1>320)
        {
            tv_max_progress.setVisibility(View.INVISIBLE);
            tv_min_progress.setVisibility(View.VISIBLE);
            int width = (int) p.measureText("  "+progress1+" kms  ");
            int yPos = (int) ((c.getHeight()-5));
            c.drawText("  "+String.valueOf(progress1)+" kms ", (bmp.getWidth() - width) / 2, yPos, p);
        }
        else {
            tv_max_progress.setVisibility(View.INVISIBLE);
            tv_min_progress.setVisibility(View.INVISIBLE);
            int width = (int) p.measureText("  "+progress1+" kms  ");
            int yPos = (int) ((c.getHeight()-5));
            c.drawText("  "+String.valueOf(progress1)+" kms ", (bmp.getWidth() - width) / 2, yPos, p);

        }*/



    }



}
