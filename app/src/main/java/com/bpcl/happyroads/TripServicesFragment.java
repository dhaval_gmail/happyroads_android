package com.bpcl.happyroads;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class TripServicesFragment extends Fragment{
     TextView tv_buy_rsa;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.trip_services_fragment, container, false);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Partner Services");
        tv_buy_rsa=(TextView)v.findViewById(R.id.tv_buy_rsa);
        tv_buy_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ExploreActivity)getActivity()).replace_fragmnet(new BuyRSA());

            }
        });
       // ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Partner Services");
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);

    }
}
