package com.bpcl.happyroads;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;
import com.bpcl.happyroads.Pojo.InviteMyFrined;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.Utils.TwitterLogin;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class InviteFriendsFragment extends Fragment {
    @Bind(R.id.tv_refer_code_invite)
    TextView tvReferCodeInvite;
    @Bind(R.id.iv_fb_invite)
    ImageView ivFbInvite;
    @Bind(R.id.iv_twitter_invite)
    ImageView ivTwitterInvite;
    @Bind(R.id.iv_msg_invite)
    ImageView ivMsgInvite;
    @Bind(R.id.iv_email_invite)
    ImageView ivEmailInvite;
    @Bind(R.id.iv_gmail_invite)
    ImageView ivGmailInvite;
    @Bind(R.id.iv_whatsapp_invite)
    ImageView ivWhatsappInvite;
    @Bind(R.id.error_layout)
    RelativeLayout errorLayout;
    SessionManager sessionManager;
    OTP otp;
    private ShareDialog shareDialog;
    private boolean canPresentShareDialog;
    private Twitter mTwitter;
    private CallbackManager callbackManager;
    SharedPreferences pref;
    private RequestToken mRequestToken;
    SharedPreferences.Editor editor;
    String sharetext="";
    Dialog pb_dialog;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.invite_friend_fragment, container, false);
       // ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Invite Friends");
        ButterKnife.bind(this, v);
        pref = getActivity().getSharedPreferences("LoginSession", 0);
        editor = pref.edit();
        sessionManager = new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();
        tvReferCodeInvite.setText(otp.getumMyUniqueReferralCode());
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
     //   sharetext="Sign Up using my code "+otp.getumMyUniqueReferralCode()+" on Happy Roads App and Enjoy your journey.Download the app using-";
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {


            getInviteText();

        } else {
            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, errorLayout);
        }
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {
            }


            @Override
            public void onError(FacebookException error) {
            }
        });
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);

    }

    @OnClick({R.id.iv_fb_invite, R.id.iv_twitter_invite, R.id.iv_msg_invite, R.id.iv_email_invite, R.id.iv_gmail_invite, R.id.iv_whatsapp_invite})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_fb_invite:
                ((ExploreActivity)getActivity()).onactivityfb=false;
                shareonfb();
                break;
            case R.id.iv_twitter_invite:
                twittershare();
                break;
            case R.id.iv_msg_invite:
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.putExtra("sms_body", sharetext);
                sendIntent.setType("vnd.android-dir/mms-sms");
                startActivity(sendIntent);
                break;
            case R.id.iv_email_invite:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{""});
                i.putExtra(Intent.EXTRA_SUBJECT, "happy Roads");
                i.putExtra(Intent.EXTRA_TEXT   , sharetext);
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Email client not installed. ", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_gmail_invite:
                PlusShare.Builder builder = new PlusShare.Builder(getActivity());

                // Set call-to-action metadata.
                builder.addCallToAction(
                        "CREATE_ITEM", /** call-to-action button label */
                        Uri.parse("http://plus.google.com/pages/create"), /** call-to-action url (for desktop use) */
                        "/pages/create" /** call to action deep-link ID (for mobile use), 512 characters or fewer */);

                // Set the content url (for desktop use).
                builder.setContentUrl(Uri.parse("https://plus.google.com/pages/"));

                // Set the target deep-link ID (for mobile use).
                builder.setContentDeepLinkId("/pages/",
                        null, null, null);

                // Set the share text.
                builder.setText("Create your Google+ Page too!");

                startActivityForResult(builder.getIntent(), 0);
                break;
            case R.id.iv_whatsapp_invite:
                whatsappShare();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Invite Friends");
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }
    public void shareonfb() {

     // String url =Constant.SERVER_URL+"ImageHandlerImage.ashx?q=yes&image=Upload/"+"Unzip/"+preferance1.getString("User_ID", "")+"/Post/"+postid+"/"+preferance.getString("mainImage", "");

        LoginManager.getInstance().logInWithPublishPermissions(getActivity(), Arrays.asList("publish_actions"));
        Uri imageUri = Uri.parse("android.resource://com.bpcl.happyroads/" + R.drawable.appicon);
     // Uri imageUri = Uri.parse("android.resource://com.bpcl.happyroads/drawable/appicon.png");
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Happy Roads")
                    .setContentDescription(sharetext)
                    .setContentUrl(Uri.parse("https://goo.gl/lTciIP"))
                // .setImageUrl(imageUri)

                    .build();

            shareDialog.show(linkContent);
        }

    }

    public void whatsappShare()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, sharetext);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(),"Whatsapp not installed.",Toast.LENGTH_SHORT).show();
        }
    }

    public void twittershare()
    {
        ConfigurationBuilder confbuilder = new ConfigurationBuilder();
        Configuration conf = confbuilder
                .setOAuthConsumerKey(Constants.CONSUMER_KEY)
                .setOAuthConsumerSecret(Constants.CONSUMER_SECRET)
                .build();
        mTwitter = new TwitterFactory(conf).getInstance();

       /* if (pref.getString(Constants.PREF_KEY_ACCESS_TOKEN,"").equalsIgnoreCase("")) {
            mTwitter.setOAuthAccessToken(null);*/
            try {

                mRequestToken = mTwitter.getOAuthRequestToken(Constants.CALLBACK_URL);
                Intent intent = new Intent(getActivity(), TwitterLogin.class);
                intent.putExtra(Constants.IEXTRA_AUTH_URL, mRequestToken.getAuthorizationURL());
                startActivityForResult(intent, 0);
            } catch (TwitterException e) {
                e.printStackTrace();
            }

      /*  }
        else {
            twitter4j.auth.AccessToken accessToken = null;
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(Constants.CONSUMER_KEY);
            builder.setOAuthConsumerSecret(Constants.CONSUMER_SECRET);
            String access_token = pref.getString(Constants.PREF_KEY_ACCESS_TOKEN,"");
            // Access Token Secret
            String access_token_secret = pref.getString(Constants.PREF_KEY_ACCESS_TOKEN_SECRET,"");

            accessToken = new twitter4j.auth.AccessToken(access_token, access_token_secret);
            mTwitter = new TwitterFactory(builder.build()).getInstance(accessToken);

            try {
                StatusUpdate status1 = new StatusUpdate(sharetext);
               // status.setMedia(null);
                mTwitter.updateStatus(sharetext);
                Toast.makeText(getActivity(),"Shared on Twitter.",Toast.LENGTH_SHORT).show();



            } catch (TwitterException e) {
                Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
                e.printStackTrace();
            }
        }*/
    }

    public  void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 0) {
            if (resultCode == getActivity().RESULT_OK) {
                twitter4j.auth.AccessToken accessToken = null;

                try {
                    String oauthVerifier = intent.getExtras().getString(Constants.IEXTRA_OAUTH_VERIFIER);
                    accessToken = mTwitter.getOAuthAccessToken(mRequestToken, oauthVerifier);
                  /*  SharedPreferences pref = getSharedPreferences(Constant.PREF_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();*/
                    editor.putString(Constants.PREF_KEY_ACCESS_TOKEN, accessToken.getToken());
                    editor.putString(Constants.PREF_KEY_ACCESS_TOKEN_SECRET, accessToken.getTokenSecret());
                    editor.commit();


                    StatusUpdate status = new StatusUpdate(sharetext.toString().length()>139?sharetext.toString().substring(0,139):sharetext.toString());

                        mTwitter.updateStatus(status);
                        Toast.makeText(getActivity(), "Shared on Twitter.", Toast.LENGTH_SHORT).show();


                    } catch (TwitterException e) {
                        Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
                        e.printStackTrace();
                    }


                }else if (resultCode == getActivity().RESULT_CANCELED) {
                    Log.w("System out", "Twitter auth canceled.");
                }

        }
    }
    private void getInviteText() {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        //  pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String json = Constants.BASE_URL + "api/userMobileApi/GetDynamicValue?apiKey=pratdroid&stringKey=" + "_MobilePush_Referral_msg_to_the_friend";
        Log.d("System out", " invite friend json" + json);
        Call<ArrayList<InviteMyFrined>> call = apiService.GetDynamicValue(json);
        call.enqueue(new Callback<ArrayList<InviteMyFrined>>() {
            @Override
            public void onResponse(Call<ArrayList<InviteMyFrined>> call, Response<ArrayList<InviteMyFrined>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();


                    if (response.body().size() > 0) {
                        sharetext = response.body().get(0).getDsStringValue().replace("###code###", otp.getumMyUniqueReferralCode());
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<InviteMyFrined>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });

    }
}
