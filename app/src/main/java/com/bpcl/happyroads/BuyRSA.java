package com.bpcl.happyroads;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.MyTextView;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 09/12/2016.
 */
public class BuyRSA extends Fragment {
     TextView tv_view_scope;
     LinearLayout ll_plans,ll_rsa;

    GridView gv_category_list_account;
    ArrayList<String> Stringsss= new ArrayList<>();
    String selectedcat="";
    ArrayList<SubsciptionPlans> subsciptionPlansArrayList=new ArrayList<>();
    RelativeLayout error_layout;
    ImageView iv_left_arrow_hsv_rsa,iv_right_arrow_hsv_rsa;
    HorizontalScrollView hsv_rsa;
    int position=-1;
    private String from="";
    GetVehicleDetails vehicledetails;
    Bundle bundle;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.buy_rsa, container, false);
        tv_view_scope=(TextView)v.findViewById(R.id.tv_view_scope);
        ll_plans=(LinearLayout)v.findViewById(R.id.ll_plans);
        gv_category_list_account=(GridView)v.findViewById(R.id.gv_category_list_account);
        error_layout =(RelativeLayout) v.findViewById(R.id.error_layout);
        iv_left_arrow_hsv_rsa=(ImageView)v.findViewById(R.id.iv_left_arrow_hsv_rsa);
        iv_right_arrow_hsv_rsa=(ImageView)v.findViewById(R.id.iv_right_arrow_hsv_rsa);
        hsv_rsa=(HorizontalScrollView)v.findViewById(R.id.hsv_rsa);
        bundle = this.getArguments();



        if (bundle != null)
        {

            vehicledetails = (GetVehicleDetails) bundle.getSerializable("vehicledetails");
            from=bundle.getString("from");
            Log.d("System out","from "+from);

            //subsciptionPlansArrayList = (SubsciptionPlans) bundle.getSerializable("packagedetails");

        }
        iv_left_arrow_hsv_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hsv_rsa.scrollTo((int)hsv_rsa.getScrollX() - 20, (int)hsv_rsa.getScrollY());

            }
        });
        iv_right_arrow_hsv_rsa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hsv_rsa.scrollTo((int)hsv_rsa.getScrollX() + 20, (int)hsv_rsa.getScrollY());
            }
        });


        ll_rsa=(LinearLayout)v.findViewById(R.id.ll_rsa);
        for(int i=0;i<4;i++) {
            RsaServices(i);
        }
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";

            SubscribePolicy();

        } else {
            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
        }



        tv_view_scope.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("from", "RSA Service");
                Intent i=new Intent(getActivity(),TermsAndConditionsActivity.class);
                i.putExtras(b);
                startActivity(i);



            }
        });
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        return v;
    }

    private void SubscribePolicy() {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<SubsciptionPlans>> call = apiService.getSubscribepolicy();
        call.enqueue(new Callback<ArrayList<SubsciptionPlans>>() {
            @Override
            public void onResponse(Call<ArrayList<SubsciptionPlans>> call, Response<ArrayList<SubsciptionPlans>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    subsciptionPlansArrayList.clear();
                    Stringsss.clear();
                    //   vehicleDetails.clear();
                    subsciptionPlansArrayList= response.body();
                    if (subsciptionPlansArrayList.size()>0)
                    {
                        // Constants.show_error_popup(getActivity(), ""+vehicleDetails.get(0).getResDescription(), error_layout);
                       /* int days, years, weeks;
                        days=subsciptionPlansArrayList.get(0).getDefaultDurationDays();
                        years = days/365;   //Ignoring leap year
                        weeks = (days%365)/7;
                        days = days- ((years*365) + (weeks*7));*/
                        for(int i=0;i<subsciptionPlansArrayList.size();i++)
                        Stringsss.add(subsciptionPlansArrayList.get(i).getDescription().replace("package","").replace("-"," ")+"/ Rs."+subsciptionPlansArrayList.get(i).getCurrentDefaultSalePrice().replace("INR_","")+"/-");

                      //  selectedcat=subsciptionPlansArrayList.get(0).getDescription()+subsciptionPlansArrayList.get(0).getCurrentDefaultSalePrice();
                        setDataInGrid();




                    }
                    else
                    {
                      //  Constants.show_error_popup(getActivity(), ""+vehicleDetails.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }



            @Override
            public void onFailure(Call<ArrayList<SubsciptionPlans>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void setDataInGrid() {
        GridListAdapter gridListAdapter = new GridListAdapter();
        gv_category_list_account.setAdapter(gridListAdapter);
        //isselected = new boolean[Stringsss.length];
        gv_category_list_account.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        gv_category_list_account.setVerticalScrollBarEnabled(false);
        gv_category_list_account.setSelection(0);


    }

    private void RsaServices(int i) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.facility_content_bottom, null);
        final SimpleDraweeView fc_cat=(SimpleDraweeView) v.findViewById(R.id.fc_cat);
        final TextView tv_facility_name=(TextView) v.findViewById(R.id.tv_facility_name);

        if(i==0)
        {
            fc_cat.setImageResource(R.drawable.ras_icon1_unselected);
            tv_facility_name.setText(" Locked out ");
            if(position==0)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon1_selected);

            }

        }
        else if(i==1)
        {
            fc_cat.setImageResource(R.drawable.ras_icon2_unselected);
            tv_facility_name.setText(" Need a Tow ");
            if(position==1)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon2_selected);


            }
        }
        else if(i==2)
        {
            fc_cat.setImageResource(R.drawable.ras_icon3_unselected);
            tv_facility_name.setText("  Accident  ");
            if(position==2)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon3_selected);
            }

        }
        else
        {
            fc_cat.setImageResource(R.drawable.ras_icon4_unselected);
            tv_facility_name.setText("Dead Battery");
            if(position==3)
            {
                tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));
                fc_cat.setImageResource(R.drawable.ras_icon4_selected);
            }


        }
        v.setId(i);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  if (tv_facility_name.getText().toString().equalsIgnoreCase("Petrol Pump")) {
                position=v.getId();
                Log.d("System out","position rsa service clicked..."+position);


                //  fl_pop_up_zoo.setVisibility(View.GONE);
                // tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));


                ll_rsa.removeAllViews();
                for(int i=0;i<4;i++) {
                    RsaServices(i);
                    //  }


                }

            }
        });
        ll_rsa.addView(v);
    }

    private class GridListAdapter extends BaseAdapter {


        GridListAdapter() {
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return Stringsss.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup parent) {


            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.category_cell, null);

            final MyTextView tv_category_name = (MyTextView) v.findViewById(R.id.tv_category_name);
            tv_category_name.setText(" "+Stringsss.get(pos)+" ");
            Log.d("System out","cat___"+selectedcat);



           /* if(selectedcat.contains(Stringsss.get(position))) {

                tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                        R.drawable.category_box_selected));
                tv_category_name.setTextColor(getResources().getColor(
                        R.color.textYellow) );
            }
            else {
             */   tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                        R.drawable.category_box_unselected));
                tv_category_name.setTextColor(getResources().getColor(
                        R.color.colorWhite));
            //}
            v.setId(pos);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                        if (!selectedcat.contains(Stringsss.get(pos))) {

                            selectedcat += Stringsss.get(pos) + ",";
                            tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                                    R.drawable.category_box_selected));
                            tv_category_name.setTextColor(getResources().getColor(
                                    R.color.textYellow));
                            Log.d("System out","from click"+from);
                            if(from.equalsIgnoreCase("changeFromConfirmRSA")) {
                                Bundle b = new Bundle();
                                b.putSerializable("packagedetails", subsciptionPlansArrayList.get(view.getId()));
                                b.putSerializable("vehicledetails",vehicledetails);
                                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new ConfirmRSA(), b);
                            }else
                            {
                                try {
                                    Bundle b = new Bundle();
                                    b.putSerializable("packagedetails", subsciptionPlansArrayList.get(view.getId()));
                                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new VehicleList(), b);
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }

                            }

                        } else {
                            selectedcat = selectedcat.replace(Stringsss.get(pos) + ",", "");
                            tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                                    R.drawable.category_box_unselected));
                            tv_category_name.setTextColor(getResources().getColor(
                                    R.color.colorWhite));
                        }



                  /*  tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                            R.drawable.category_box_selected) : getResources().getDrawable(
                            R.drawable.category_box_unselected));
                    tv_category_name.setTextColor(checked ? getResources().getColor(
                            R.color.textYellow) : getResources().getColor(
                            R.color.colorWhite));*/


                }
            });

            return v;
        }
    }

}
