package com.bpcl.happyroads;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;

/**
 * Created by ADMIN on 03/02/2017.
 */
public class TermsAndConditionsActivity extends Activity{
    WebView wb_terms;
    private String URL;
    ImageView iv_close;
    String from="";
    TextView tv_text_header_tc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_conditions_layout);
        wb_terms=(WebView)findViewById(R.id.wb_terms);
        iv_close=(ImageView)findViewById(R.id.iv_close);
        tv_text_header_tc=(TextView) findViewById(R.id.tv_text_header_tc);


        Bundle bundle = new Bundle();
        bundle=getIntent().getExtras();
        if (bundle != null) {
            from = bundle.getString("from");

        }



        wb_terms.getSettings().setDisplayZoomControls(true);
        wb_terms.getSettings().setBuiltInZoomControls(true);
        wb_terms.getSettings().setUseWideViewPort(true);
        wb_terms.getSettings().setLoadWithOverviewMode(true);
        WebSettings settings1 = wb_terms.getSettings();
        settings1.setDefaultTextEncodingName("utf-8");
        wb_terms.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        wb_terms.setVerticalScrollBarEnabled(false);
        wb_terms.setHorizontalScrollBarEnabled(false);
        wb_terms.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        wb_terms.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        if(from.equalsIgnoreCase("RSA Service TnC"))
        {
            URL = Constants.GetCMS +from;
            tv_text_header_tc.setText("Terms and Conditions");
        }else if(from.equalsIgnoreCase("Signup TnC"))
        {
            URL = Constants.GetCMS +from;
            tv_text_header_tc.setText("Terms and Conditions");
        }else if(from.equalsIgnoreCase("RSA Service"))
        {
            URL = Constants.GetCMS +from;
            tv_text_header_tc.setText("RSA Service");

        }else if(from.equalsIgnoreCase("Privacy Policy"))
        {
            URL = Constants.GetCMS +from;
            tv_text_header_tc.setText("Privacy Policy");

        }

        if (IsNetworkConnection.checkNetworkConnection(TermsAndConditionsActivity.this)) {
            //  webview.loadData(text, "text/html", "utf-8");

            wb_terms.loadUrl(URL);
            wb_terms.setBackgroundColor(0);
            wb_terms.setBackgroundResource(R.drawable.main_bg);
        }
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}
