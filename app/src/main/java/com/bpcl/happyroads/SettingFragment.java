package com.bpcl.happyroads;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ADMIN on 8/18/2016.
 */
public class SettingFragment extends Fragment {
    @Bind(R.id.iv_arrow_up_profile)
    ImageView ivArrowUpProfile;
    @Bind(R.id.iv_my_account)
    ImageView ivMyAccount;
    @Bind(R.id.iv_change_pwd)
    ImageView ivChangePwd;
    @Bind(R.id.ll_profile_content)
    LinearLayout llProfileContent;
    @Bind(R.id.iv_arrow_up_notification)
    ImageView ivArrowUpNotification;
    @Bind(R.id.iv_swt_on_news)
    ImageView ivSwtOnNews;
    @Bind(R.id.iv_swt_off_promos)
    ImageView ivSwtOffPromos;
    @Bind(R.id.iv_swt_on_emergency_alerts)
    ImageView ivSwtOnEmergencyAlerts;
    @Bind(R.id.ll_notification_content)
    LinearLayout llNotificationContent;
    boolean arrowProfile=false;
    boolean arrowNotification=false;
    boolean swtNews=false;
    boolean swtPromo=false;
    boolean swtEmergency=false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_fragment, container, false);
        ButterKnife.bind(this, v);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Settings");
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });

        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.iv_arrow_up_profile, R.id.iv_my_account, R.id.iv_change_pwd, R.id.iv_arrow_up_notification, R.id.iv_swt_on_news, R.id.iv_swt_off_promos, R.id.iv_swt_on_emergency_alerts})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_arrow_up_profile:
                if(arrowProfile==false)
                {
                    ivArrowUpProfile.setImageResource(R.drawable.arrow_down);
                    llProfileContent.setVisibility(View.GONE);
                    arrowProfile=true;
                }
                else
                {
                    ivArrowUpProfile.setImageResource(R.drawable.arrow_up);
                    llProfileContent.setVisibility(View.VISIBLE);
                    arrowProfile=false;
                }
                break;
            case R.id.iv_my_account:
                ((ExploreActivity)getActivity()).replace_fragmnet(new MyAccountFragment());
                break;
            case R.id.iv_change_pwd:
                ((ExploreActivity)getActivity()).replace_fragmnet(new ChangePasswordFragment());

               /* ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
                android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                ((ExploreActivity)getActivity()).setHeader("Change Password");
                changeTransaction.replace(R.id.frame, changePasswordFragment);
                changeTransaction.commit();
*/
                break;
            case R.id.iv_arrow_up_notification:
                if(arrowNotification==false)
                {
                    ivArrowUpNotification.setImageResource(R.drawable.arrow_down);
                    llNotificationContent.setVisibility(View.GONE);
                    arrowNotification=true;
                }
                else
                {
                    ivArrowUpNotification.setImageResource(R.drawable.arrow_up);
                    llNotificationContent.setVisibility(View.VISIBLE);
                    arrowNotification=false;
                }
                break;
            case R.id.iv_swt_on_news:
                if(swtNews==false)
                {
                    ivSwtOnNews.setImageResource(R.drawable.swt_off);
                    swtNews=true;
                }else
                {
                    ivSwtOnNews.setImageResource(R.drawable.swt_on);
                    swtNews=false;
                }
                break;
            case R.id.iv_swt_off_promos:
                if(swtPromo==false)
                {
                    ivSwtOffPromos.setImageResource(R.drawable.swt_on);
                    swtPromo=true;
                }else
                {
                    ivSwtOffPromos.setImageResource(R.drawable.swt_off);
                    swtPromo=false;
                }
                break;
            case R.id.iv_swt_on_emergency_alerts:
                if(swtEmergency==false)
                {
                    ivSwtOnEmergencyAlerts.setImageResource(R.drawable.swt_off);
                    swtEmergency=true;
                }else
                {
                    ivSwtOnEmergencyAlerts.setImageResource(R.drawable.swt_on);
                    swtEmergency=false;
                }
                break;
        }
    }
}
