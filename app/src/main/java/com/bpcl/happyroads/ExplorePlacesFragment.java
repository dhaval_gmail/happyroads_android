package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Pojo.AddFavourite;
import com.bpcl.happyroads.Pojo.CreateTripApi;
import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Poi_Night;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.Insert_Poi;
import com.bpcl.happyroads.Pojo.MyTrip;
import com.bpcl.happyroads.Pojo.RemoveTripPoi;
import com.bpcl.happyroads.Pojo.TripListforPOIPlanning;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DownloadCode;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.Utils.ImageSaver;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.LoginModule;
import com.bpcl.happyroads.Utils.MyTextView;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ADMIN on 8/29/2016.
 */
public class ExplorePlacesFragment extends Fragment  implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener{
    ListView ExploreAttractionList;
    //LinearLayout ll_km_favourite_list,ll_best_time_to_visit;
    TextView tv_time_km_fav_list,tv_from_to_month;
    ImageView iv_time_km_fav_list;
    MyTextView tv_view_my_plan_btn;
    ArrayList<String> addedlist = new ArrayList<>();
    boolean selectedList[];
    String posi = "";
    String poiposi = "";
    int position1=0;
    String destId = "";
    String destLat = "";
    String destLong = "";
    SessionManager sessionManager;
    Dialog   pb_dialog;



    boolean like=false;
    String from="";
    ContentLoadingProgressBar explore_places_content_progressbar;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    ArrayList<ExplorePOI> explorePoiList=new ArrayList<>();
    Gson gson=new Gson();
    String name="";
    RelativeLayout error_layout;
    ArrayList<AddFavourite> addFavList=new ArrayList<AddFavourite>();
    Integer tripid=0;
    String temptime="19:00";
    List<DestinationPlan> destinationPlanList=new ArrayList<>();

    DBHandler dbHandler;
    ExploreAttractionAdapter exploreAttractionAdapter;
    ArrayList<TripListforPOIPlanning> myTripArrayList = new ArrayList<>();
    ArrayList<String> searchtripesname = new ArrayList<String>();
    ArrayList<String> searchtripesnameby;
    ArrayList<String> searchtripesids = new ArrayList<String>();
    Dialog  myTripDialog;
    TextView tv_start_date_my_trip, tv_return_date_my_trip;
    EditText et_trip_name;
    TextView tv_save_my_trip;
    AutoCompleteTextView av_tripname;
    String start_date="",start_time="",end_date="",end_time="";
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    String date;
    String select="";
    Double dayhours = 8.0;
    Double starttime=7.0;
    boolean setclick=false;
    String date1=null;
    String tripdate_eta="",trip_return_date_eta="";
    String trip_return_end_date = "", trip_return_end_time = "", trip_end_date_eta = "", trip_end_time_eta = "";

    Bitmap originbitmap = null, desbitamp = null;



    List<Create_Trip> create_tripArrayList=new ArrayList<>();
    String excepted_date="", excepted_time = "",return_excepted_date="",return_excepted_time="";
    List<DestinationPlan> destinationplanList = new ArrayList<>();
    public static ArrayList<ExploreDestination> exploreDestList=new ArrayList<>();
    ArrayList<MyTrip> mySpecificTripArrayList = new ArrayList<>();
    public static Bitmap destinationbmp=null;
    int DestinationPos=0;

    //List<DestinationPlan> destinationplanList = new ArrayList<>();
    ArrayList<Integer> noday;
    ArrayList<DestinationPlan> tempdestiDestinationPlanlist;




    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.explore_places_fragment, container, false);
        tv_view_my_plan_btn=(MyTextView)v.findViewById(R.id.tv_view_my_plan_btn);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        error_layout =(RelativeLayout) v.findViewById(R.id.error_layout);
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);


        // explore_places_content_progressbar=(ContentLoadingProgressBar)v.findViewById(R.id.explore_places_content_progressbar);
        Bundle bundle = this.getArguments();
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        if (bundle != null)
        {
            if (bundle.get("position") !=null) {
                posi = bundle.getString("position");
            }
            from = bundle.getString("from");
            destId = bundle.getString("destID");
            destLat = bundle.getString("destLat");
            destLong = bundle.getString("destLong");
            name = bundle.getString("placeName");
            if(tripid==0)
            tripid= bundle.getInt("tripId",0);

        }
        Log.d("System out","tripid on create___"+tripid);
        ((ExploreActivity) getActivity()).AddtoMyPlanList.clear();
        dbHandler=new DBHandler(getActivity());
        destinationPlanList=new ArrayList<>();


        destinationPlanList=dbHandler.get_DestinationList(tripid);
        if(sharedpreferences.getString("DestList","").equalsIgnoreCase(""))
        {
            destList();
        }
        else {

            String jsonOutput1 = sharedpreferences.getString("DestList", "");
            Type listType1 = new TypeToken<ArrayList<ExploreDestination>>() {
            }.getType();
            exploreDestList = gson.fromJson(jsonOutput1, listType1);
        }

        DestinationPos=0;

        for(int i=0;i<exploreDestList.size();i++)
        {
            if(String.valueOf(exploreDestList.get(i).getDesID()).equalsIgnoreCase(destId))
            {
                DestinationPos=i;
                break;
            }
        }
        create_tripArrayList = dbHandler.get_TRIP_By_TripId(tripid);
        destinationplanList = dbHandler.get_DestinationList(tripid);
        if (create_tripArrayList.size() > 0) {


            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++) {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

            }
            String tripdate = create_tripArrayList.get(0).getReturn_start_date() + " " + starttime;


            excepted_date = create_tripArrayList.get(0).getReturn_start_date();
            try {



                excepted_time =Constants.formatDate(create_tripArrayList.get(0).getReturn_start_time(), "HH:mm", "hh:mm a");
                start_date=create_tripArrayList.get(0).getStartdate();
                Log.d("System out","tripdate___"+start_date +" "+start_time);
                start_time=create_tripArrayList.get(0).getStarttime();
                Log.d("System out","tripdate___"+start_date +" "+start_time);
                end_date  = create_tripArrayList.get(0).getReturn_start_date();
                Log.d("System out","returndate___"+end_date+"  "+end_time);
                end_time = create_tripArrayList.get(0).getReturn_end_time();

                Log.d("System out","returndate___"+end_date+"  "+end_time);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


            // set_destination_layout();
        }
        ((ExploreActivity)getActivity()).tv_text_header.setText(name);
        Constants.destSelectionfrom="ExplorePlacesFragment";
        sessionManager=new SessionManager(getActivity());

        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
       // ((ExploreActivity) getActivity()).edcityname.setText("Ex-Banguluru");
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.VISIBLE);
       // ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.main_bg_top);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent i=new Intent(getContext(),ExploreActivity.class);
                //    startActivity(i);
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            getPOIAPI();

        }
        else
        {
            Toast.makeText(getActivity(),getActivity().getString(R.string.internet_error),Toast.LENGTH_SHORT).show();


        }

       // ExploreAttractionAdapter exploreAttractionAdapter=new ExploreAttractionAdapter(getActivity(),explorePoiList);
       // ExploreAttractionList.setAdapter(exploreAttractionAdapter);
        selectedList=new boolean[8];
        ExploreAttractionList=(ListView)v.findViewById(R.id.ExploreAttractionList);

        if (((ExploreActivity) getActivity()).AddtoMyPlanList.size()>0)
        {
            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                    R.anim.bottom_up);
            tv_view_my_plan_btn.startAnimation(bottomUp);
            tv_view_my_plan_btn.setVisibility(View.VISIBLE);

        }
        tv_view_my_plan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle b=new Bundle();
                b.putInt("tripId",tripid);
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new MyPlanFragment(),b);
            }
        });

        return v;
    }

    public void getPOIAPI() {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
       // explore_places_content_progressbar.setVisibility(View.VISIBLE);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id="1";
        if(sessionManager.isLoggedIn())
        {
            id=sessionManager.get_Authenticate_User().getUmId()+"";
        }
        //"POILatitude":"13", "POILongitude":"132"
      //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
       // String json="[{\"umId\": \"0\",\"POIID\":\""+0+"\"    , \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\"  }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        if(from.equalsIgnoreCase("nearbyactivities"))
        {
            json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+100+"\",\"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":50, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\",\"PoiNotIncludeThisId\":\""+destId+"\"}]";

            Log.d("System out","Explore POI from nearbyactivities____"+json);

        }else
        {
           // json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+999999+"\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\"  }]";
            //json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+100+"\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\"  }]";
            json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+100+"\" ,\"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":50, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\" , \"POIRating\":0  }]";

            Log.d("System out","Explore POI____"+json);


        }

        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {

              pb_dialog.dismiss();

                if (response.body() != null) {


                    explorePoiList.clear();

                    explorePoiList.addAll(response.body());
                    if (explorePoiList.size()>0)
                    {
                        if (explorePoiList.get(0).getResStatus() == true) {

                         //   explore_places_content_progressbar.setVisibility(View.GONE);

                            //  Collections.reverse(exploreDestList);

                            String json = gson.toJson(explorePoiList); // myObject - instance of MyObject
                            editor.putString("POIList", json);
                            editor.commit();

                            if(destinationPlanList.size()>0)
                            {
                                for(int i=0;i<explorePoiList.size();i++) {
                                    for(int j=0;j<destinationPlanList.size();j++)
                                    {
                                        Log.d("desid",String.valueOf(destinationPlanList.get(j).getPoiServerId())+"poiid"+explorePoiList.get(i).getPOIID());
                                        if(destinationPlanList.get(j).getPoiServerId()==explorePoiList.get(i).getPOIID())
                                            ((ExploreActivity) getActivity()).AddtoMyPlanList.add(String.valueOf(i));
                                    }
                                }
                            }

                            if (((ExploreActivity) getActivity()).AddtoMyPlanList.size()>0)
                            {
                                Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                        R.anim.bottom_up);
                                tv_view_my_plan_btn.startAnimation(bottomUp);
                                tv_view_my_plan_btn.setVisibility(View.VISIBLE);

                            }
                            exploreAttractionAdapter = new ExploreAttractionAdapter(getActivity(), explorePoiList);
                            ExploreAttractionList.setAdapter(exploreAttractionAdapter);
                            ExploreAttractionList.setSelection(position1);

                        }
                        else
                        {
                          //  explore_places_content_progressbar.setVisibility(View.GONE);
                            Constants.show_error_popup(getActivity(),explorePoiList.get(0).getResDescription()+"", error_layout);
                        }
                    }
                    else
                    {
                     //   explore_places_content_progressbar.setVisibility(View.GONE);
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
              //  explore_places_content_progressbar.setVisibility(View.GONE);
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }


            }
        });
    }

    public void getPOIAPIFilter(String Nameids,String rate,String startmonth,int endmonth) {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        // explore_places_content_progressbar.setVisibility(View.VISIBLE);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id="1";
        if(sessionManager.isLoggedIn())
        {
            id=sessionManager.get_Authenticate_User().getUmId()+"";
        }
        //"POILatitude":"13", "POILongitude":"132"
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        // String json="[{\"umId\": \"0\",\"POIID\":\""+0+"\"    , \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\"  }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        String[] split= startmonth.split(",");

        if(from.equalsIgnoreCase("nearbyactivities"))
        {


            //json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + 999999 + "\",   \"desName\": \"\",   \"desLongitude\": \"" + Constants.selectedLong + "\",   \"desLatitude\": \"" + Constants.selectedLat + "\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + ExploreActivity.destination_exclude_id + ",\"desRating\":" + rate + ",\"destBestTimeFrom\":" + startmonth + ",\"desBestTimeTo\":" + endmonth + " }]";
                json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+100+"\", \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":50, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\",\"POIRating\":" + rate + ",\"POIBestTimeFrom\":" + (!startmonth.equalsIgnoreCase("")?split[0]:0)  + ",\"POIBestTimeTo\":" + (!startmonth.equalsIgnoreCase("")?(split.length>1?split[split.length-1]:split[0]):0) + "\",\"monthsCommaSapList\":\"" +(!startmonth.equalsIgnoreCase("")? startmonth.substring(0,startmonth.length()-1):"") + "\" }]";
            Log.d("System out", "Explore Destination from nearby____" + json);



            Log.d("System out","Explore POI____"+json);

        }else
        {
            //json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+999999+"\" , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\",\"POIRating\":" + rate + ",\"POIBestTimeFrom\":" + startmonth + ",\"POIBestTimeTo\":" + endmonth + " , \"POIRating\":0  }]";
            json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+100+"\" , \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":50, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\",\"POIRating\":" + rate + ",\"POIBestTimeFrom\":" + (!startmonth.equalsIgnoreCase("")?split[0]:0)  + ",\"POIBestTimeTo\":" + (!startmonth.equalsIgnoreCase("")?(split.length>1?split[split.length-1]:split[0]):0) + ", \"POITypeId\":\""+(!Nameids.equalsIgnoreCase("")?Nameids.substring(0, Nameids.length() - 1):"") +"\",\"monthsCommaSapList\":\"" + (!startmonth.equalsIgnoreCase("")?startmonth.substring(0,startmonth.length()-1):"") + "\" }]";

            Log.d("System out","Explore POI____"+json);


        }

        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {

                pb_dialog.dismiss();

                if (response.body() != null) {


                    explorePoiList.clear();
                    explorePoiList.addAll(response.body());
                    if (explorePoiList.size()>0)
                    {
                        if (explorePoiList.get(0).getResStatus() == true) {

                            //   explore_places_content_progressbar.setVisibility(View.GONE);

                            //  Collections.reverse(exploreDestList);

                            String json = gson.toJson(explorePoiList); // myObject - instance of MyObject
                            editor.putString("POIList", json);
                            editor.commit();
                            exploreAttractionAdapter = new ExploreAttractionAdapter(getActivity(), explorePoiList);
                            ExploreAttractionList.setAdapter(exploreAttractionAdapter);
                        }
                        else
                        {
                            Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", error_layout);
                            explorePoiList.clear();
                            exploreAttractionAdapter = new ExploreAttractionAdapter(getActivity(), explorePoiList);
                            ExploreAttractionList.setAdapter(exploreAttractionAdapter);
                            //  explore_places_content_progressbar.setVisibility(View.GONE);

                        }
                    }
                    else
                    {
                        //   explore_places_content_progressbar.setVisibility(View.GONE);
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
                //  explore_places_content_progressbar.setVisibility(View.GONE);
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }


            }
        });
    }

    private class ExploreAttractionAdapter extends BaseAdapter {
        Context context;
        ArrayList<ExplorePOI> explorePoiList=new ArrayList<ExplorePOI>();

        public ExploreAttractionAdapter(Context context,ArrayList<ExplorePOI> explorePoiList) {
            this.context=context;
            this.explorePoiList=explorePoiList;
        }

        @Override
        public int getCount() {
            return explorePoiList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = convertView;
            v = inflater.inflate(R.layout.favourites_list, null);
           final LinearLayout ll_km_favourite_list=(LinearLayout)v.findViewById(R.id.ll_km_favourite_list);
            final LinearLayout  ll_best_time_to_visit=(LinearLayout)v.findViewById(R.id.ll_best_time_to_visit);
            iv_time_km_fav_list=(ImageView) v.findViewById(R.id.iv_time_km_fav_list);
            tv_time_km_fav_list=(TextView) v.findViewById(R.id.tv_time_km_fav_list);
            tv_from_to_month=(TextView) v.findViewById(R.id.tv_from_to_month);
          final TextView  tv_rating_dest=(TextView) v.findViewById(R.id.tv_rating_dest);
          final TextView  tv_time_to_spend=(MyTextView) v.findViewById(R.id.tv_time_to_spend);
            tv_time_to_spend.setVisibility(View.VISIBLE);
            final ImageView like_btn = (ImageView) v.findViewById(R.id.like_btn);
            final LinearLayout ll_like_btn = (LinearLayout) v.findViewById(R.id.ll_like_btn);
            ll_like_btn.setVisibility(View.VISIBLE);
          //  tv_time_km_fav_list.setText("2 hrs");
            iv_time_km_fav_list.setVisibility(View.GONE);
            iv_time_km_fav_list.setImageResource(R.drawable.required_time_icon);
            SimpleDraweeView iv_img_favourites=(SimpleDraweeView)v.findViewById(R.id.iv_img_favourites);
            final TextView tv_img_name=(TextView)v.findViewById(R.id.tv_img_name);
            final String placeName=tv_img_name.getText().toString();
            final ImageView iv_like_icon=(ImageView)v.findViewById(R.id.iv_like_icon);
            final Dialog dialog1 = new Dialog(getActivity());
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog1.setContentView(R.layout.like_remove_layout);
            final TextView tv_text_like_remove=(TextView)dialog1.findViewById(R.id.tv_text_like_remove);
            dialog1.setCanceledOnTouchOutside(true);
            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            iv_img_favourites.setHierarchy(hierarchy);

            //  fc_p.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
         //   if(explorePoiList.get(position).getImageList().get(position).getImgDescription().equalsIgnoreCase("Banner")) {


                iv_img_favourites.setImageURI(Constants.TimbThumb_ImagePath + explorePoiList.get(position).getPOIImage()+"&width="+(ExploreActivity.width));
          // }
            tv_img_name.setText(explorePoiList.get(position).getPOIName());
            tv_rating_dest.setText(explorePoiList.get(position).getPOIRating());
            if (explorePoiList.get(position).getPOIBestTimeFrom() != null && explorePoiList.get(position).getPOIBestTimeTo()!=null) {
                tv_from_to_month.setText(Constants.getMonthName(Integer.parseInt(explorePoiList.get(position).getPOIBestTimeFrom())) + " to " + Constants.getMonthName(Integer.parseInt(explorePoiList.get(position).getPOIBestTimeTo())));

            }
          //  tv_time_km_fav_list.setText(Constants.convert_minute_hrs(explorePoiList.get(position).getDuration().doubleValue()));
            if(explorePoiList.get(position).getPOISpendTime()!=null) {
                tv_time_km_fav_list.setText(explorePoiList.get(position).getPOISpendTime()+" hrs");
            }
            ImageView  iv_share_img=(ImageView)v.findViewById(R.id.iv_share_img);
            iv_share_img.setId(position);
            iv_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url= "https://goo.gl/lTciIP";

                    try {
                      //  get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + explorePoiList.get(position).getPOIImage() + "&width=" + (500) + "&height=" + (500),explorePoiList.get(position).getPOIImage(),"Destination: "+explorePoiList.get(position).getPOIName()+"\n"+"Best Time To Visit: "+Constants.getMonthName(Integer.parseInt(explorePoiList.get(position).getPOIBestTimeFrom()))+" - "+Constants.getMonthName(Integer.parseInt(explorePoiList.get(position).getPOIBestTimeTo())));
                        get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + explorePoiList.get(position).getPOIImage() + "&width=" + (500) + "&height=" + (500),explorePoiList.get(position).getPOIImage(), "Take a peek at " + explorePoiList.get(position).getPOIName()+ ".\n" + "Plan your adventure filled roadtrip with Happy Roads."+ Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));


                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }


            });


           /* boolean checked = selectedList[position];

            like_btn.setImageResource(checked ?
                    R.drawable.round_home_selected :
                    R.drawable.round_home_unselected);

            tv_img_name.setTextColor(checked ? getResources().getColor(
                    R.color.textYellow) : getResources().getColor(
                    R.color.colorWhite));*/


            if (((ExploreActivity) getActivity()).AddtoMyPlanList.contains(position + "")) {
                // ExploreCellCategorySelection.setLiked(true);
                ll_km_favourite_list.setVisibility(View.INVISIBLE);
                ll_best_time_to_visit.setVisibility(View.INVISIBLE);
                ll_like_btn.setVisibility(View.VISIBLE);
                like_btn.setImageResource(R.drawable.round_home_selected);
                tv_img_name.setTextColor(getResources().getColor(R.color.textYellow));
            }
            else
            {

                ll_km_favourite_list.setVisibility(View.VISIBLE);
                ll_best_time_to_visit.setVisibility(View.VISIBLE);
                ll_like_btn.setVisibility(View.VISIBLE);
                // ExploreCellCategorySelection.setLiked(false);
                like_btn.setImageResource(R.drawable.add_icon);
                tv_img_name.setTextColor(getResources().getColor(R.color.colorWhite));
            }
            iv_like_icon.setId(position);

            if(explorePoiList.get(position).getFavId()==0)
            {
                iv_like_icon.setImageResource(R.drawable.like_icon_unselected);
            }else
            {
                iv_like_icon.setImageResource(R.drawable.like_icon_selected);

            }


            iv_like_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                        if(sessionManager.isLoggedIn())
                        {

                            String json = "[{\"FavUmId\": \""+sessionManager.get_Authenticate_User().getUmId()+"\", \"FavType\": \"POI\",   \"FavRefId\": \""+explorePoiList.get(v.getId()).getPOIID()+"\"}]";
                            Log.d("System out", "In add favourite  " + json);
                            // AddFavouritesAPI(json,(ImageView) v);
                            AddFavouritesAPI(json,(ImageView) v,explorePoiList,v.getId());

                        }else
                        {
                            LoginModule loginModule = new LoginModule(getActivity(),"favourite Destination");
                            loginModule.signIn();
                        }


                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                    }


                }

                private void AddFavouritesAPI(final String json, final ImageView imageView, final ArrayList<ExplorePOI> explorePOI, final int pos) {
                    final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                    pb_dialog.setCancelable(false);
                    pb_dialog.show();

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
                    //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
                    Call<ArrayList<AddFavourite>> call = apiService.addFavourites(json);
                    call.enqueue(new Callback<ArrayList<AddFavourite>>() {
                        @Override
                        public void onResponse(Call<ArrayList<AddFavourite>> call, Response<ArrayList<AddFavourite>> response) {
                            if (response.body() != null) {

                                pb_dialog.dismiss();
                                addFavList= response.body();
                                if (addFavList.get(0).getResStatus() == true)
                                {
                                    //  Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                    if(addFavList.get(0).getResDescription().toLowerCase().contains("add")) {

                                        imageView.setImageResource(R.drawable.like_icon_selected);

                                        explorePOI.get(pos).setFavId(addFavList.get(0).getFavId());

                                        final Dialog dialog1 = new Dialog(getActivity());
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        dialog1.setContentView(R.layout.like_remove_layout);
                                        final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                        tv_text_like_remove.setText("Added to your favourites");
                                        dialog1.setCanceledOnTouchOutside(true);
                                        dialog1.show();
                                        like = true;
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    Thread.sleep(1500);
                                                    ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            dialog1.dismiss();

                                                            //Code for the UiThread
                                                        }
                                                    });
                                                    /*runOnUiThread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            dialog1.dismiss();
                                                        }
                                                    });*/
                                                } catch (Exception e) {
                                                    Log.w("Exception in splash", e);
                                                }

                                            }
                                        }).start();


                                    }
                                    else
                                    {
                                        imageView.setImageResource(R.drawable.like_icon_unselected);

                                        explorePOI.get(pos).setFavId(0);

                                        final Dialog dialog1 = new Dialog(getActivity());
                                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        dialog1.setContentView(R.layout.like_remove_layout);
                                        final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                        tv_text_like_remove.setText("Removed from your favourites");
                                        dialog1.setCanceledOnTouchOutside(true);
                                        dialog1.show();
                                        like = true;
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    Thread.sleep(1500);
                                                    ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            dialog1.dismiss();

                                                            //Code for the UiThread
                                                        }
                                                    });
                                                   /* runOnUiThread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            dialog1.dismiss();
                                                        }
                                                    });*/
                                                } catch (Exception e) {
                                                    Log.w("Exception in splash", e);
                                                }

                                            }
                                        }).start();


                                    }
                                }
                                else
                                {
                                    // Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                    //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                                }



                            }


                        }

                        @Override
                        public void onFailure(Call<ArrayList<AddFavourite>> call, Throwable t) {
                            // Log error here since request failed
                            pb_dialog.dismiss();
                            t.printStackTrace();
                            if (!Constants.isInternetAvailable(getActivity())) {
                                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                            } else {
                                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                            }

                        }
                    });
                }

            });
          /*  if(like==false) {
                iv_like_icon.setImageResource(R.drawable.like_icon_selected);
                tv_text_like_remove.setText("Added to your favourites");
                dialog1.show();
                like=true;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1500);
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog1.dismiss();


                                }
                            });
                        } catch (Exception e) {
                            Log.w("Exception in splash", e);
                        }

                    }
                }).start();
            }
            else
            {
                iv_like_icon.setImageResource(R.drawable.like_icon_unselected);
                tv_text_like_remove.setText("Removed from your favourites");
                dialog1.show();
                like=false;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1500);
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog1.dismiss();


                                }
                            });
                        } catch (Exception e) {
                            Log.w("Exception in splash", e);
                        }

                    }
                }).start();
            }*/
            ll_like_btn.setId(position);
            ll_like_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    poiposi= String.valueOf(view.getId());
                    position1= view.getId();
                    if(tripid == 0)
                    {
                        if(sessionManager.isLoggedIn())
                        {

                            GetTripList();
                        }else
                        {
                            LoginModule loginModule = new LoginModule(getActivity(),"favourite Destination");
                            loginModule.signIn();
                        }


                    }
                    else {


                        Integer pos1 = Integer.valueOf(view.getId());
                        if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(view.getId()).getPOIID()) == 0)

                            if(!excepted_date.trim().equalsIgnoreCase("")) {
                                addData();
                            }
                            else {
                                Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(view.getId()).getPOIID().toString(), explorePoiList.get(view.getId()).getDuration().toString(), explorePoiList.get(view.getId()).getDistance(), explorePoiList.get(view.getId()).getPOIImage(), explorePoiList.get(view.getId()).getPOINearbyDestination());
                            }
                        else {

                            Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

                        }



                    }


                }
            });

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* if(tripid != 0)
                    {*/
                        if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(position).getPOIID()) == 0) {
                            // Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                            Bundle bundle = new Bundle();
                            bundle.putString("position",String.valueOf(position));
                            bundle.putString("placeName",tv_img_name.getText().toString());
                            bundle.putInt("tripId",tripid);
                            bundle.putString("destId",destId);
                            bundle.putString("DestName",((ExploreActivity)getActivity()).tv_text_header.getText().toString());
                            String json = gson.toJson(explorePoiList);
                            editor.putString("POIList", json);
                            editor.commit();
                            //  bundle=getArguments();


                            ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();




                            ExploreActivity.destattractionimages.clear();
                            ((ExploreActivity)getActivity()).setpagerdata(false);
                            for (int i=0;i<explorePoiList.get(position).getImageList().size();i++)
                            {
                                if(explorePoiList.get(position).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                                    ExploreActivity.destattractionimages.add(explorePoiList.get(position).getImageList().get(i).getImgName());
                                }
                            }
                            // bundle.putAll(getArguments());
                            exploreAttractionFragment.setArguments(bundle);
                            changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                            changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                            changeTransaction.addToBackStack(null);
                            changeTransaction.commit();
                       /* }
                        else {
                            // Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);

                            Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(position).getPOIID()));
                        }*/
                    }
                    else {

                        Bundle bundle = new Bundle();
                        bundle.putString("position", String.valueOf(position));
                        bundle.putString("placeName", tv_img_name.getText().toString());
                        bundle.putInt("tripId", tripid);
                        bundle.putString("destId", destId);
                        bundle.putString("DestName", ((ExploreActivity) getActivity()).tv_text_header.getText().toString());
                        String json = gson.toJson(explorePoiList);
                        editor.putString("POIList", json);
                        editor.commit();
                        //  bundle=getArguments();


                        ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                        android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();


                        ExploreActivity.destattractionimages.clear();
                        ((ExploreActivity) getActivity()).setpagerdata(false);
                        for (int i = 0; i < explorePoiList.get(position).getImageList().size(); i++) {
                            if (explorePoiList.get(position).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                                ExploreActivity.destattractionimages.add(explorePoiList.get(position).getImageList().get(i).getImgName());
                            }
                        }
                        // bundle.putAll(getArguments());
                        exploreAttractionFragment.setArguments(bundle);
                        changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                        changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                        changeTransaction.addToBackStack(null);
                        changeTransaction.commit();
                    }
                   /* Bundle bundle = new Bundle();
                    bundle.putString("position",String.valueOf(position));
                    bundle.putString("placeName",tv_img_name.getText().toString());
                    bundle.putInt("tripId",tripid);
                    bundle.putString("destId",destId);
                    bundle.putString("DestName",((ExploreActivity)getActivity()).tv_text_header.getText().toString());
                    String json = gson.toJson(explorePoiList);
                    editor.putString("POIList", json);
                    editor.commit();
                    //  bundle=getArguments();


                    ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();




                    ExploreActivity.destattractionimages.clear();
                    ((ExploreActivity)getActivity()).setpagerdata(false);
                    for (int i=0;i<explorePoiList.get(position).getImageList().size();i++)
                    {
                        if(explorePoiList.get(position).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                            ExploreActivity.destattractionimages.add(explorePoiList.get(position).getImageList().get(i).getImgName());
                        }
                    }
                    // bundle.putAll(getArguments());
                    exploreAttractionFragment.setArguments(bundle);
                    changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                    changeTransaction.addToBackStack(null);
                    changeTransaction.commit();*/


                }
            });           /* like_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tv_view_my_plan_btn.getVisibility() == View.GONE) {
                       // like_btn.setImageResource(R.drawable.round_home_selected);
                        tv_img_name.setTextColor(getResources().getColor(R.color.textYellow));
                        Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_up);
                        tv_view_my_plan_btn.startAnimation(bottomUp);
                        tv_view_my_plan_btn.setVisibility(View.VISIBLE);
                        tv_view_my_plan_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((ExploreActivity)getActivity()).replace_fragmnet(new MyPlanFragment());
                            }
                        });

                    }else
                    {
                      //  like_btn.setImageResource(R.drawable.round_home_unselected);
                        tv_img_name.setTextColor(getResources().getColor(R.color.colorWhite));
                        tv_view_my_plan_btn.setVisibility(View.GONE);
                        like_btn.setVisibility(View.GONE);

                    }


                }

            });*/
 /*           like_btn.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    if (tv_view_my_plan_btn.getVisibility() == View.GONE) {
                        Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_up);
                        tv_view_my_plan_btn.startAnimation(bottomUp);
                        tv_view_my_plan_btn.setVisibility(View.VISIBLE);
                        tv_view_my_plan_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((ExploreActivity)getActivity()).replace_fragmnet(new MyPlanFragment());
                            }
                        });

                    }
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    tv_view_my_plan_btn.setVisibility(View.GONE);
                    like_btn.setVisibility(View.GONE);

                }
            });*/




            return v;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).tv_text_header.setText(name);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);


    }
    public void  get_bitmap_from_freco_download(final String url, final String name,final  String besttime)
    {
        Log.d("System out","sharing image path "+url);


//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    dataSource.close();
                    DownloadCode code = new DownloadCode(getActivity(),Constants.TimbThumb_ImagePath,name,bitmap,besttime);
                    code.share_image_text_GPLUS();


                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }
    private void Delete_Poi_Api(final Integer waypointid) {
        // get & set progressbar dialog

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{     \"TripWayPointId\": \"" + waypointid + "\"}]";

        Log.d("json_CreateTrip", json);

        Call<ArrayList<RemoveTripPoi>> call = apiService.RemovePoi(json);
        call.enqueue(new Callback<ArrayList<RemoveTripPoi>>() {
            @Override
            public void onResponse(Call<ArrayList<RemoveTripPoi>> call, final Response<ArrayList<RemoveTripPoi>> response) {
                if (response.body() != null) {


                    if (response.body().get(0).getResStatus() == true) {

                        pb_dialog.dismiss();
                        dbHandler.delete_Destination_ById(waypointid);

                        Constants.show_error_popup_success(getActivity(),"You have removed attraction.", error_layout);

                        //     AddDayWisePlan();
                        //   ll_day_wise_plan.removeAllViews();

                        //set_destination_layout();
                        destinationPlanList = new ArrayList<>();

                        destinationPlanList = dbHandler.get_DestinationList(tripid);
                        ((ExploreActivity) getActivity()).AddtoMyPlanList.clear();
                        if (destinationPlanList.size() > 0) {
                            for (int i = 0; i < explorePoiList.size(); i++) {
                                for (int j = 0; j < destinationPlanList.size(); j++) {
                                    Log.d("desid", String.valueOf(destinationPlanList.get(j).getPoiServerId()) + "poiid" + explorePoiList.get(i).getPOIID());
                                    if (destinationPlanList.get(j).getPoiServerId() == explorePoiList.get(i).getPOIID())
                                        ((ExploreActivity) getActivity()).AddtoMyPlanList.add(String.valueOf(i));

                                }
                            }
                        }
                        if (((ExploreActivity) getActivity()).AddtoMyPlanList.size() > 0) {
                            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                    R.anim.bottom_up);
                            tv_view_my_plan_btn.startAnimation(bottomUp);
                            tv_view_my_plan_btn.setVisibility(View.VISIBLE);

                        } else {
                            Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                                    R.anim.bottom_down);
                            tv_view_my_plan_btn.startAnimation(bottomUp);
                            tv_view_my_plan_btn.setVisibility(View.GONE);
                        }
                        exploreAttractionAdapter.notifyDataSetChanged();
                        exploreAttractionAdapter = new ExploreAttractionAdapter(getActivity(), explorePoiList);
                        ExploreAttractionList.setAdapter(exploreAttractionAdapter);

                    } else {
                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().get(0).getResDescription(), error_layout);
                    }
                } else {
                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RemoveTripPoi>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }
    public void GetTripList()
    {

        // get & set progressbar dialog
        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(true);
        pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{\"TripDestination_End\":" + destId + ",  \"TripUmID\": "+sessionManager.get_Authenticate_User().getUmId() +" }]";
        Log.d("json_CreateTrip", json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<TripListforPOIPlanning>> call = apiService.GetMyTripByDestination(json);
        call.enqueue(new Callback<ArrayList<TripListforPOIPlanning>>() {
            @Override
            public void onResponse(Call<ArrayList<TripListforPOIPlanning>> call, Response<ArrayList<TripListforPOIPlanning>> response) {
                if (response.body() != null) {


                    pb_dialog.dismiss();

                    if (response.body().get(0).getResStatus() == true) {
                        myTripArrayList = new ArrayList<>();
                        myTripArrayList.addAll(response.body());
                        // set_adapter(trip_status);
                        searchtripesname.clear();
                        for (int i = 0; i < myTripArrayList.size(); i++) {
                            searchtripesname.add(myTripArrayList.get(i).getTripName());
                            searchtripesids.add(String.valueOf(myTripArrayList.get(i).getTripId()));
                        }

                        Mytrip_Dialog(getActivity());

                    } else {
                        //  Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                        myTripArrayList = new ArrayList<>();
                        Mytrip_Dialog(getActivity());

                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<TripListforPOIPlanning>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();

                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });


    }
    public void Mytrip_Dialog(final Activity activity)
    {
        final SessionManager sessionManager1 = new SessionManager(activity);
        myTripDialog=new Dialog(activity);
        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_my_trips);
        tv_start_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_start_date_my_trip);
        tv_return_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_return_date_my_trip);
        final TextView tv_for_better_dates=(TextView)myTripDialog.findViewById(R.id.tv_for_better_dates);
        final ImageView iv_swt_off_my_trip=(ImageView)myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
        final LinearLayout ll_start_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        et_trip_name=(EditText)myTripDialog.findViewById(R.id.et_trip_name);
        av_tripname= (AutoCompleteTextView) myTripDialog.findViewById(R.id.av_tripname);
        tv_save_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_save_my_trip);

        et_trip_name.setVisibility(View.GONE);
        av_tripname.setVisibility(View.VISIBLE);

        av_tripname.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                  /*  if (ShipperTvSelctCtyOriginPlaceOrder.getText().toString().trim().length() == 0 && ShipperTvLocalityOriginPlaceOrder.getText().toString().trim().length() > 0) {
                        Constants.showtoast(getActivity(), "Please select city for origin.", llplaceoredefrg);
                    } else {*/

                av_tripname.setThreshold(1);
                if (searchtripesname.size() > 0) {

                    searchtripesnameby = new ArrayList<String>();


                    for (int i = 0; i < searchtripesname.size(); i++) {
                        String str = av_tripname.getText().toString();
                        String origincityselected = ((ExploreActivity) getActivity()).edcityname.getText().toString().replace("Ex - ","").trim();
                        String cityname = searchtripesname.get(i);
                        Log.d("System out", " comparition " + searchtripesname.get(i).toLowerCase() + "      " + str.toLowerCase());
                        Log.d("System out", " comparition  value" + searchtripesname.get(i).toLowerCase().contains(str.toLowerCase()));
                        if (!searchtripesname.get(i).equalsIgnoreCase(origincityselected) && searchtripesname.get(i).toLowerCase().contains(str.toLowerCase())) {
                            searchtripesnameby.add(searchtripesname.get(i));
                        }
                    }
                    ArrayAdapter cityadapter = new ArrayAdapter<String>(getActivity(), R.layout.custontextview, searchtripesnameby);


                    av_tripname.setAdapter(cityadapter);

                }

                //}

            }

            @Override
            public void afterTextChanged(Editable s) {
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }
        });
        av_tripname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int j, long l) {
                for(int i=0;i<myTripArrayList.size();i++)
                {
                    if(av_tripname.getText().toString().equalsIgnoreCase(myTripArrayList.get(i).getTripName()) && myTripArrayList.get(i).getTripDate()!=null)
                    {

                        try {

                            tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                            tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripReturnDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                            start_date=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");
                            Log.d("System out","tripdate___"+start_date +" "+start_time);
                            start_time=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","HH:mm");
                            Log.d("System out","tripdate___"+start_date +" "+start_time);
                            end_date  = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM ,yyyy");
                            Log.d("System out","returndate___"+end_date+"  "+end_time);
                            end_time = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm");

                            Log.d("System out","returndate___"+end_date+"  "+end_time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
          /*  av_tripname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<myTripArrayList.size();i++)
                    {
                       if(av_tripname.getText().toString().equalsIgnoreCase(myTripArrayList.get(i).getTripName()))
                       {
                           try {
                               tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                               tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));

                           } catch (ParseException e) {
                               e.printStackTrace();
                           }
                       }
                    }

                }
            });*/

           /* av_tripname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                        tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));

                        start_date=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");
                        Log.d("System out","tripdate___"+start_date +" "+start_time);
                        start_time=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","HH:mm");
                        Log.d("System out","tripdate___"+start_date +" "+start_time);
                        end_date  = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM ,yyyy");
                        Log.d("System out","returndate___"+end_date+"  "+end_time);
                        end_time = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm");

                        Log.d("System out","returndate___"+end_date+"  "+end_time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });*/
        Calendar now = Calendar.getInstance();


        dpd = DatePickerDialog.newInstance(
                ExplorePlacesFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf.format(new Date());

        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 0);
        dpd.setMinDate(calendar);
        tpd = TimePickerDialog.newInstance(
                ExplorePlacesFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),false
        );

        //  tpd.setMinTime(calendar.getTime().getHours(),calendar.getTime().getMinutes(),calendar.getTime().getSeconds());
        tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  if(tripFlag==true) {
                select = "start";
                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                //  }


            }

        });

        tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  if(tripFlag==true) {
                select = "stop";


                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                // }
                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            }
        });

/*
            iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(tripFlag==false)
                    {
                        iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                        tv_for_better_dates.setVisibility(View.GONE);
                        ll_start_date_my_trip.setAlpha(1);
                        ll_return_date_my_trip.setAlpha(1);
                        tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(tripFlag==true) {
                                    select = "start";
                                    dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                                }


                            }

                        });

                        tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(tripFlag==true) {
                                    select = "stop";


                                    dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                                }
                                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                            }
                        });
                        tripFlag=true;



                    }
                    else
                    {
                        iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                        tv_for_better_dates.setVisibility(View.VISIBLE);
                        ll_start_date_my_trip.setAlpha((float) 0.6);
                        ll_return_date_my_trip.setAlpha((float) 0.6);
                        tripFlag=false;
                    }

                }
            });
*/

        if(exploreDestList.size()>0)
            get_bitmap_from_freco( Constants.TimbThumb_ImagePath+exploreDestList.get(DestinationPos).getDesImage()+"&width="+50+"&height="+50);
        tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                       /* if (tv_start_date_my_trip.getText().length() == 0) {
                            setclick = false;
                            Constants.show_error_popup(activity, "Select your trip dates.", activity.findViewById(R.id.error_layout));

                        } else*/ if (av_tripname.getText().toString().length() > 0) {

                    Log.d("Sizemytrip", "trip name size" + searchtripesname.size());
                    String flag = "";
                    for (int i = 0; i < searchtripesname.size(); i++) {
                        Log.d("Sizemytrip", "trip name size in for " + searchtripesname.get(i) + "   " + av_tripname.getText().toString().trim());
                        if (searchtripesname.get(i).equalsIgnoreCase(av_tripname.getText().toString().trim())) {
                            String tripdate = start_date + " " + start_time;
                            String returndate = end_date + " " + end_time;
                            if (returndate.length() > 2) {
                                if (CheckDates(tripdate, returndate)) {
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                    Date date = null;
                                    int pos = Integer.parseInt(poiposi);
                                    try {
                                        date = sdf.parse(tripdate);
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(date);
                                        int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
                                        calendar.add(Calendar.HOUR, convertime[0]);
                                        calendar.add(Calendar.MINUTE, convertime[1]);
                                        Log.d("trip_Date_eta", date.toString());
                                        tripdate = sdf.format(calendar.getTime());
                                        //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                    } catch (ParseException e) {
                                        e.printStackTrace();

                                    }
                                    if (CheckDates(tripdate, returndate)) {
                                        //  CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                        // searchtripesnameby.add(searchtripesname.get(i));
                                        Log.d("Sizemytrip", "trip name id " + searchtripesids.get(i));
                                        int check_trip_in_db = dbHandler.check_trip_in_local_db(Integer.parseInt(searchtripesids.get(i)));

                                        Log.d("Sizemytrip", check_trip_in_db + "");
                                        tripid = Integer.parseInt(searchtripesids.get(i));
                                        myTripDialog.dismiss();
                                        if (check_trip_in_db >= 1) {

                                            Integer pos1 = Integer.valueOf(poiposi);

                                            if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(pos1).getPOIID()) == 0)
                                                if (!excepted_date.trim().equalsIgnoreCase("")) {
                                                    addData();
                                                } else {
                                                    Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(pos1).getPOIID().toString(), explorePoiList.get(pos1).getDuration().toString(), explorePoiList.get(pos1).getDistance(), explorePoiList.get(pos1).getPOIImage(), explorePoiList.get(pos1).getPOINearbyDestination());
                                                }

                                            else {
                                                setclick = false;
                                                destinationPlanList=new ArrayList<>();


                                                destinationPlanList=dbHandler.get_DestinationList(tripid);

                                                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                                    getPOIAPI();

                                                }
                                                else
                                                {
                                                    Toast.makeText(getActivity(),getActivity().getString(R.string.internet_error),Toast.LENGTH_SHORT).show();


                                                }
                                               // Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

                                            }
                                        } else {
                                            getMyTrip(tripid);
                                            // add_trip_in_to_database(position);
                                        }
                                        flag += "true";
                                    } else {
                                        try {
                                            setclick = false;
                                            //Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                            Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                                        } catch (Exception e) {
                                            setclick = false;
                                            e.printStackTrace();
                                            Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                        }
                                    }
                                } else {
                                    setclick = false;
                                    int pos = Integer.parseInt(poiposi);
                                    //  Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                    Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule. ", activity.findViewById(R.id.error_layout));

                                }
                            } else {

                                // Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                                int pos = Integer.parseInt(posi);
                                //CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                // searchtripesnameby.add(searchtripesname.get(i));
                                Log.d("Sizemytrip", "trip name id " + searchtripesids.get(i));
                                int check_trip_in_db = dbHandler.check_trip_in_local_db(Integer.parseInt(searchtripesids.get(i)));

                                Log.d("Sizemytrip", check_trip_in_db + "");
                                tripid = Integer.parseInt(searchtripesids.get(i));
                                myTripDialog.dismiss();
                                if (check_trip_in_db >= 1) {

                                    Integer pos1 = Integer.valueOf(poiposi);

                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(pos1).getPOIID()) == 0)
                                        if (!excepted_date.trim().equalsIgnoreCase("")) {
                                            addData();
                                        } else {
                                            Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(pos1).getPOIID().toString(), explorePoiList.get(pos1).getDuration().toString(), explorePoiList.get(pos1).getDistance(), explorePoiList.get(pos1).getPOIImage(), explorePoiList.get(pos1).getPOINearbyDestination());
                                        }

                                    else {
                                        setclick = false;
                                        destinationPlanList=new ArrayList<>();


                                        destinationPlanList=dbHandler.get_DestinationList(tripid);
                                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                            getPOIAPI();
                                        }
                                        else
                                        {
                                            Toast.makeText(getActivity(),getActivity().getString(R.string.internet_error),Toast.LENGTH_SHORT).show();
                                        }
                                     //   Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

                                    }
                                } else {
                                    getMyTrip(tripid);
                                    // add_trip_in_to_database(position);
                                }
                                flag += "true";
                            }
                        } else {
                            flag += "false";

                        }
                    }

                    if (!flag.contains("true")) {

                        String tripdate = start_date + " " + start_time;
                        String returndate = end_date + " " + end_time;

                        if (returndate.length() > 2) {
                            if (CheckDates(tripdate, returndate)) {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;

                                int pos = Integer.parseInt(poiposi);


                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta", date.toString());
                                    tripdate = sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if (CheckDates(tripdate, returndate)) {
                                    setclick = false;
                                    CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                } else {
                                    try {
                                        setclick = false;
                                      //  Constants.show_error_popup(activity, "Your destination requires" + Constants.convert_minute_hrs(exploreDestList.get(DestinationPos).getDuration().doubleValue()) + " hours of travel. Plan accordingly.", activity.findViewById(R.id.error_layout));
                                        Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                                    } catch (Exception e) {
                                        setclick = false;
                                        e.printStackTrace();
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                    }
                                }
                            } else {
                                setclick = false;

                                //Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                            }
                        } else {
                            // Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());

                            CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                        }

                    }
                } else {
                    setclick = false;
                    Constants.show_error_popup(activity, "Provide trip name.", activity.findViewById(R.id.error_layout));
                }

            }

        });
        ImageView ImgClosepopup=(ImageView)myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //  SimpleDateFormat formatter = new SimpleDateFormat("dd MMM ,yyyy");
        date = dayOfMonth + " " + (++monthOfYear) + "," + year;

        //  dateTextView.setText(date);


        // formatter.format(date);
        tpd.show(getActivity().getFragmentManager(), "TimePickerDialog");


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {


        String time = hourOfDay+":"+minute+"";

        try {
            date1= Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","dd MMM, yyyy hh:mm a");
            if(select.equalsIgnoreCase("start"))
            {
                start_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                start_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");

                Log.d("System out","date1___"+date1);
                String tripdate=start_date+" "+start_time;
                String returndate=end_date+" "+end_time;
                Log.d("System out","tripdate___"+tripdate);
                Log.d("System out","returndate___"+returndate);
                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date1);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf1.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf1.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);
                // calendar1.add(Calendar.HOUR, 2);
                calendar1.add(Calendar.HOUR, 0);
                Log.d("System out","datenew___"+datenew);
                Log.d("System out","current "+datecurrent);
                Log.d("System out","ctime___"+calendar1.getTime());
                Log.d("System out","ctime___"+calendar1.getTime().after(datenew));
                Log.d("System out","ctime___"+datecurrent.before(datenew));

                //  currentDateandTime.set
                if (calendar1.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                    // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", tv_save_my_trip);


                }else  if (datenew.before(datecurrent))
                {
                    // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", tv_save_my_trip);

                }
              /*  if (calendar.getTime() > datenew.getTime()) {
                    tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                }*/
                else {
                    tv_start_date_my_trip.setText(date1);
                    if (returndate.length() > 2) {
                        if (CheckDates(tripdate, returndate)) {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                            Date date = null;
                            int pos = Integer.parseInt(poiposi);
                            try {
                                date = sdf.parse(tripdate);
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                int[] convertime = splitToComponentTimes(exploreDestList.get(pos).getDuration().doubleValue());
                                calendar.add(Calendar.HOUR, convertime[0]);
                                calendar.add(Calendar.MINUTE, convertime[1]);
                                Log.d("trip_Date_eta", date.toString());
                                tripdate = sdf.format(calendar.getTime());
                                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                            } catch (ParseException e) {
                                e.printStackTrace();

                            }
                            if (CheckDates(tripdate, returndate)) {
                                // CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.get(pos).getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                            } else {
                                try {
                                    // Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need ", error_layout);
                                    Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule..", error_layout);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. ", error_layout);
                                }
                            }
                        } else {
                            // Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", error_layout);
                           // Constants.show_error_popup(getActivity(), "Your destination requires " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + "hours of travel. Plan accordingly.", error_layout);
                            try {
                                Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule.", error_layout);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            else if(select.equalsIgnoreCase("stop"))
            {
                end_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                end_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");
                tv_return_date_my_trip.setText(date1);

                if (av_tripname.getText().toString().length() > 0) {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {

                        Constants.show_error_popup(getActivity(),"Select your trip dates.", error_layout);
                    }

                    else
                    {
                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;

                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;
                                int pos=Integer.parseInt(poiposi);
                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDestList.get(pos).getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta",date.toString());
                                    tripdate=sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate)) {
                                    // CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.get(pos).getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                                }
                                else {
                                    try {

                                        Constants.show_error_popup(getActivity(),"Change start date & time of your return trip. you need " , error_layout);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(getActivity(),"Change start date & time of your return trip. ", error_layout);
                                    }
                                }
                            }
                            else
                            {
                                int pos=Integer.parseInt(poiposi);
                                Constants.show_error_popup(getActivity(),"You require more time for travel/stay. Reschedule. ", error_layout);
                            }
                        }
                        else {
                            Log.d("UserId", sessionManager.get_Authenticate_User().getUmUdid());
                           /* int pos=Integer.parseInt(posi);
                            CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.get(pos).getDesID().toString(), sessionManager.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);*/
                        }
                    }
                } else

                {
                    Constants.show_error_popup(getActivity(),"Please Enter Trip Name", error_layout);
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public static boolean CheckDates(String startDate, String endDate) {



        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare",String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare",e.toString());
        }
        return b;
    }
    public void addData()
    {
        DestinationPlan create_poi = new DestinationPlan();
        create_poi.setTripId(tripid);


        create_poi.setKM(Math.ceil(explorePoiList.get(Integer.parseInt(poiposi)).getDistance() * 1000));
        create_poi.setTime(Double.valueOf(explorePoiList.get(Integer.parseInt(poiposi)).getDuration()));


        create_poi.setArrivalTime(0.0);

        //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));


        create_poi.setPoiName(explorePoiList.get(Integer.parseInt(poiposi)).getPOIName());
        create_poi.setPoiLatitude(Double.valueOf(explorePoiList.get(Integer.parseInt(poiposi)).getPOILatitude()));
        create_poi.setPoiLongitude(Double.valueOf(explorePoiList.get(Integer.parseInt(poiposi)).getPOILongitude()));
        create_poi.setStayTime(Double.valueOf(explorePoiList.get(Integer.parseInt(poiposi)).getPOISpendTime().replace(":",".")));

        create_poi.setPoidescription(explorePoiList.get(Integer.parseInt(poiposi)).getPOIShortDescription());


        create_poi.setPoiServerId(explorePoiList.get(Integer.parseInt(poiposi)).getPOIID());

        create_poi.setPoitype("DesPlan");
        create_poi.setWayPointId(00);

        create_poi.setPoiDesID(explorePoiList.get(Integer.parseInt(poiposi)).getPOINearbyDestination());


        create_poi.setImagename("");


        if(explorePoiList.get(Integer.parseInt(poiposi)).getPOIIsFullTime())
        {
            create_poi.setStarttime(24.00);
            create_poi.setEndtime(00.00);
        }
        else
        {
            String starttime="0:0" ,endtime="0:0";

            try {
                starttime=Constants.formatDate(explorePoiList.get(Integer.parseInt(poiposi)).getPOIStartTime(),"HH:mm:ss","HH:mm");
                endtime=Constants.formatDate(explorePoiList.get(Integer.parseInt(poiposi)).getPOIEndTime(),"HH:mm:ss","HH:mm");

            } catch (ParseException e) {
                e.printStackTrace();
                starttime="0:0" ;endtime="0:0";
            }


            create_poi.setStarttime(Double.valueOf(starttime.replace(":",".")));
            create_poi.setEndtime(Double.valueOf(endtime.replace(":",".")));
        }


        dbHandler.add_Destination_into_Table(create_poi);
        destinationplanList.clear();
        destinationplanList = dbHandler.get_DestinationList(tripid);
        set_destination_layout();
    }
    private void Insert_Poi_Api( final Integer pos, final String waytype, final String wayId, final String duration, Double origindistance, final String imageurl, final Integer desid) {
        // get & set progressbar dialog


        Long distance;


        Double d = origindistance * 1000;

        distance = d.longValue();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


       /* pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(true);
        pb_dialog.show();*/


        String json = "[{     \"TripWayType\": \"" + waytype + "\",     \"TripWayRefId\": \"" + wayId + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"\" ,\"TripEstOutTime\":\"\",\"TripDesId\":" + desid + ", \"TripIsPause\":\"false\" ,\"TripResumeDateTime\":\"\",\"TripDistanceInKm\":" + distance + ",\"TripDistanceInMinute\":" + duration + ",\"TripWayIsReturnPlanned\":" + false + "}]";

        Log.d("json_CreateTrip", json);

        Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
        call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
            @Override
            public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {
                if (response.body() != null) {


                    if (response.body().get(0).getResStatus() == true )
                    {

                        Constants.show_error_popup_success(getActivity(),"Attraction added successfully.", error_layout);

                        String url = Constants.TimbThumb_ImagePath + imageurl + "&width=" + 50 + "&height=" + 50;

                        //stuff that updates ui
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(url))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


                        dataSource.subscribe(new BaseBitmapDataSubscriber() {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                                if (dataSource.isFinished() && bitmap != null) {

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            //stuff that updates ui




                                            DestinationPlan create_poi = new DestinationPlan();
                                            create_poi.setTripId(tripid);


                                            create_poi.setKM(Math.ceil(explorePoiList.get(pos).getDistance() * 1000));
                                            create_poi.setTime(Double.valueOf(explorePoiList.get(pos).getDuration()));


                                            create_poi.setArrivalTime(0.0);
                                            create_poi.setDepartureTime(0.0);

                                            new ImageSaver(getActivity()).
                                                    setFileName(imageurl).

                                                    setExternal(true).
                                                    save(bitmap);
                                            //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));


                                            create_poi.setPoiName(explorePoiList.get(pos).getPOIName());
                                            create_poi.setPoiLatitude(Double.valueOf(explorePoiList.get(pos).getPOILatitude()));
                                            create_poi.setPoiLongitude(Double.valueOf(explorePoiList.get(pos).getPOILongitude()));
                                            create_poi.setStayTime(Double.valueOf(explorePoiList.get(pos).getPOISpendTime().replace(":",".")));

                                            create_poi.setPoidescription(explorePoiList.get(pos).getPOIShortDescription());


                                            create_poi.setPoiServerId(explorePoiList.get(pos).getPOIID());

                                            create_poi.setPoitype("DesPlan");
                                            create_poi.setWayPointId(response.body().get(0).getTripWayPointId());

                                            create_poi.setPoiDesID(explorePoiList.get(pos).getPOINearbyDestination());


                                            create_poi.setImagename(imageurl);


                                            if(explorePoiList.get(pos).getPOIIsFullTime())
                                            {
                                                create_poi.setStarttime(24.00);
                                                create_poi.setEndtime(00.00);
                                            }
                                            else
                                            {
                                                String starttime="0:0" ,endtime="0:0";

                                                try {
                                                    starttime=Constants.formatDate(explorePoiList.get(pos).getPOIStartTime(),"HH:mm:ss","HH:mm");
                                                    endtime=Constants.formatDate(explorePoiList.get(pos).getPOIEndTime(),"HH:mm:ss","HH:mm");

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                    starttime="0:0" ;endtime="0:0";
                                                }


                                                create_poi.setStarttime(Double.valueOf(starttime.replace(":",".")));
                                                create_poi.setEndtime(Double.valueOf(endtime.replace(":",".")));
                                            }


                                            dbHandler.add_Destination_into_Table(create_poi);

                                            if(pb_dialog != null)
                                            pb_dialog.dismiss();


                                            dataSource.close();

                                            if (!TextUtils.isEmpty(poiposi)) {
                                                Log.d("System out","IF In Explore");
                                                if (((ExploreActivity) getActivity()).AddtoMyPlanList.contains(poiposi + "")) {
                                                    Log.d("System out","sub IF In Explore");
                                                }
                                                else
                                                {
                                                    Log.d("System out"," sub Else In Explore");
                                                    if (!((ExploreActivity) getActivity()).AddtoMyPlanList.contains(poiposi)) {
                                                        ((ExploreActivity) getActivity()).AddtoMyPlanList.add(poiposi);
                                                    }


                                                }
                                            }

/*
                                            ExplorePlacesFragment explorePlacesFragment = new ExplorePlacesFragment();
                                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                            changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                                            changeTransaction.replace(R.id.frame, explorePlacesFragment);
                                            changeTransaction.addToBackStack(null);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("from","add to my plan");
                                            //  bundle.putString("destLat",""+create_tripArrayList.get(0).getDestination_latitude());
                                            // bundle.putString("destLong",""+create_tripArrayList.get(0).getDestination_longitude());
                                            bundle.putString("destLat",exploreDestList.get(DestinationPos).getDesLatitude());
                                            bundle.putString("destLong",exploreDestList.get(DestinationPos).getDesLongitude());
                                            bundle.putString("destID",explorePoiList.get(Integer.parseInt(poiposi)).getPOINearbyDestination()+"");
                                            bundle.putString("placeName",name);
                                            bundle.putInt("tripId",tripid);
                                            Log.d("System out","Response of trip id on create paas to explore "+tripid);
                                            explorePlacesFragment.setArguments(bundle);
                                            changeTransaction.commit();*/

                                            if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                                getPOIAPI();

                                            }
                                            else
                                            {
                                                Toast.makeText(getActivity(),getActivity().getString(R.string.internet_error),Toast.LENGTH_SHORT).show();


                                            }

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }
                                pb_dialog.dismiss();
                            }
                        }, CallerThreadExecutor.getInstance());


                    } else {
                        setclick = false;
                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().get(0).getResDescription(), error_layout);
                    }
                } else {
                    setclick = false;
                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                // Log error here since request failed
                setclick = false;
                t.printStackTrace();
                pb_dialog.dismiss();



                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });


    }
    public void destList()
    {

        final SessionManager sessionManager1 = new SessionManager(getActivity());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{     \"umId\": \"" + sessionManager1.get_Authenticate_User().getUmId() + "\",     \"radius\": \"999999\",   \"desName\": \"" + name+ "\",   \"desLongitude\": \"999\",   \"desLatitude\": \"999\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":100,\"desNotIncludeThisID\":0 }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Log.d("System out", " json destlist___" + json);
        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {

                        exploreDestList.addAll(response.body());
                    } else {
                        Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(),  error_layout);
                    }
                    if(pb_dialog!=null)
                    pb_dialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });
    }
    private void getMyTrip(int TripId) {
        // get & set progressbar dialog

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(true);
        pb_dialog.show();
        String json = "[{\"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",  \"PageNo\": 1, \"TripId\": "+TripId+",    \"PageSize\": 50 }]";
        Log.d("json_CreateTrip", json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<MyTrip>> call = apiService.GetMyTrip(json);
        call.enqueue(new Callback<ArrayList<MyTrip>>() {
            @Override
            public void onResponse(Call<ArrayList<MyTrip>> call, Response<ArrayList<MyTrip>> response) {
                if (response.body() != null) {


                    pb_dialog.dismiss();

                    if (response.body().get(0).getResStatus() == true) {//call OTP API here
                        mySpecificTripArrayList = new ArrayList<>();
                        mySpecificTripArrayList.addAll(response.body());
                        add_trip_in_to_database(0);
                        // set_adapter(trip_status);


                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<MyTrip>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();

                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });

    }
    public void CreateTripApi(final String tripname, String sourceId, String destinationId, String userId, final String source, final String destination, final Activity activity, final Dialog myTripDialog) {
        // get & set progressbar dialog
        final SessionManager sessionManager1 = new SessionManager(activity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String tripdate=start_date+" "+start_time+":00";
        String tripreturndate=end_date+" "+end_time+":00";

        tripdate_eta="";trip_return_date_eta="";

        Log.d("start_date",tripdate);
        try {
            tripdate=  Constants.formatDate(tripdate,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");

        } catch (ParseException e) {
            e.printStackTrace();
            tripdate="";

        }

        try{
            tripreturndate=  Constants.formatDate(tripreturndate,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        }
        catch (ParseException e){

            tripreturndate="";

        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        Date date = null;

        try {
            date = sdf.parse(tripdate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
            calendar.add(Calendar.HOUR, convertime[0]);
            calendar.add(Calendar.MINUTE, convertime[1]);
            Log.d("trip_Date_eta",date.toString());
            tripdate_eta=sdf.format(calendar.getTime());
            //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
            tripdate_eta="";

        }
        try {
            date = sdf.parse(tripreturndate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
            calendar.add(Calendar.HOUR, convertime[0]);
            calendar.add(Calendar.MINUTE, convertime[1]);
            trip_return_date_eta=sdf.format(calendar.getTime());
            //  trip_returndate_eta=  Constants.formatDate(dateeta,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_date_eta="";

        }


        pb_dialog = Constants.get_dialog(activity, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        String json = "[{ \"TripName\": \"" + tripname + "\",\"TripDate\":\"" + tripdate  + "\",\"TripEndDateTime\":\"" + tripdate_eta+"\",\"TripReturnDate\":\"" + tripreturndate +"\",\"TripReturnDateETA\":\"" + trip_return_date_eta   +"\", \"TripGoingTo_Start\": "+sourceId+",\"TripDestination_End\":"+destinationId+", \"TripUmID\":"+sessionManager1.get_Authenticate_User().getUmId()+" }]";
        Log.d("json_CreateTrip",json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<CreateTripApi> call = apiService.createTrip(json);
        call.enqueue(new Callback<CreateTripApi>() {
            @Override
            public void onResponse(Call<CreateTripApi> call, Response<CreateTripApi> response) {
                if (response.body() != null) {

                    CreateTripApi createTripApi = new CreateTripApi();
                    createTripApi = response.body();
                    if (createTripApi.getResStatus() == true) {//call OTP API here

                        // getDistanceOnRoad(23.034325,72.501297,23.725664,72.533523,createTripApi,tripname,source);
                        tripid=createTripApi.getTripId();
                        add_trip_in_to_database(source,tripname,createTripApi,activity,myTripDialog);

                    } else {
                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().getResDescription(), error_layout);
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateTripApi> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });


    }
    public void  get_bitmap_from_freco(final String url)
    {




//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");
                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    destinationbmp=bitmap;
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                destinationbmp=null;
            }
        }, CallerThreadExecutor.getInstance());


    }
    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }
    public void add_trip_in_to_database(final String sourcename, final String tripname, final CreateTripApi createTripApi, final Activity activity, final Dialog myTripDialog)
    {
        final   ArrayList<ExploreDestination> exploreDestList1=new ArrayList<>();

        final SessionManager sessionManager1 = new SessionManager(activity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{     \"umId\": \"" + sessionManager1.get_Authenticate_User().getUmId() + "\",     \"radius\": \"999999\",   \"desName\": \"" + ExploreActivity.origincity + "\",   \"desLongitude\": \"999\",   \"desLatitude\": \"999\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":100,\"desNotIncludeThisID\":0 }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Log.d("System out", " json " + json);

        trip_return_end_date="";trip_return_end_time="";trip_end_date_eta="";trip_end_time_eta="";

        Log.d("tripdateeta",tripdate_eta+"ret"+trip_return_date_eta);
        try{
            trip_end_date_eta=Constants.formatDate(tripdate_eta,"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");

            trip_end_time_eta=Constants.formatDate(tripdate_eta,"yyyy-MM-dd HH:mm:ss","HH:mm");


        } catch (ParseException e) {
            e.printStackTrace();
            trip_end_date_eta="";trip_end_time_eta="";
        }


        try{

            trip_return_end_date=Constants.formatDate(trip_return_date_eta,"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");

            trip_return_end_time=Constants.formatDate(trip_return_date_eta,"yyyy-MM-dd HH:mm:ss","HH:mm");

        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_end_date="";trip_return_end_time="";
        }

        Log.d("tripdateeta1",tripdate_eta+"ret"+trip_return_date_eta+"trip_end_date_eta:"+trip_end_date_eta);


        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();

                if (response.body() != null) {


                    exploreDestList1.clear();
                    exploreDestList1.addAll(response.body());
                    if (exploreDestList1.size() > 0) {


                        String url=Constants.TimbThumb_ImagePath+exploreDestList1.get(0).getDesImage()+"&width="+50+"&height="+50;

                        //stuff that updates ui
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(url))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




                        dataSource.subscribe(new BaseBitmapDataSubscriber()
                        {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                                if (dataSource.isFinished() && bitmap != null) {
                                    Log.d("Bitmap", "has come");
                                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                    final int pos=Integer.parseInt(posi);




                                    //  dbHandler.deleteAllTrip();
                                    //  String jsonOutput = sharedpreferences.getString("DestList","");
                                    //  Type listType = new TypeToken<ArrayList<ExploreDestination>>(){}.getType();
                                    //     ArrayList<ExploreDestination>  exploreDestList = gson.fromJson(jsonOutput, listType);

                                    Create_Trip create_trip = new Create_Trip();
                                    create_trip.setSource(ExploreActivity.origincity);
                                    create_trip.setDestination(exploreDestList.get(DestinationPos).getDesName());

                                    create_trip.setStartdate(start_date);
                                    create_trip.setStarttime(start_time);

                                    create_trip.setReturn_start_date(end_date);
                                    create_trip.setReturn_start_time(end_time);

                                    create_trip.setEnddate(trip_end_date_eta);
                                    create_trip.setEndtime(trip_end_time_eta);
                                    create_trip.setReturn_end_date(trip_return_end_date);
                                    create_trip.setReturn_end_time(trip_return_end_time);

                                    if(end_date.length()>1)
                                    {
                                        create_trip.setReturnstatus(true);
                                    }


                                    create_trip.setTripname(tripname);
                                    create_trip.setSource_latitude(Double.valueOf(exploreDestList1.get(0).getDesLatitude()));
                                    create_trip.setSource_longitude(Double.valueOf(exploreDestList1.get(0).getDesLongitude()));
                                    create_trip.setDestination_latitude(Double.valueOf(exploreDestList.get(DestinationPos).getDesLatitude()));
                                    create_trip.setDestination_longitude(Double.valueOf(exploreDestList.get(DestinationPos).getDesLongitude()));


                                    create_trip.setKM(exploreDestList.get(DestinationPos).getDistance()*1000);
                                    create_trip.setTime(exploreDestList.get(DestinationPos).getDuration().doubleValue());
                                    create_trip.setTripstatus(createTripApi.getTripStatus());
                                    create_trip.setTriprating(createTripApi.getTripRating());
                                    create_trip.setTripId(createTripApi.getTripId());
                                    create_trip.setSourceId(createTripApi.getTripGoingToStart());
                                    create_trip.setDestinationId(createTripApi.getTripDestinationEnd());
                                    create_trip.setSource_image(Constants.getImageBytes(bitmap));
                                    create_trip.setDesNoOfNearByAttraction(exploreDestList.get(DestinationPos).getDesNoOfNearByAttraction());
                                    create_trip.setDestination_image(Constants.getImageBytes(destinationbmp));
                                    //   DBHandler dbHandler=new DBHandler(activity);

                                    dbHandler.add_Trip_into_Table(create_trip);

                                    //   pb_dialog.dismiss();

                                    myTripDialog.dismiss();
                                    dataSource.close();
                                    Integer pos1 = Integer.valueOf(posi);
                                    create_tripArrayList.clear();
                                    create_tripArrayList = dbHandler.get_TRIP_By_TripId(tripid);
                                    excepted_date = create_tripArrayList.get(0).getReturn_start_date();
                                    try {

                                        excepted_time=Constants.formatDate(create_tripArrayList.get(0).getReturn_start_time(), "HH:mm", "hh:mm a");
                                    }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(pos1).getPOIID()) == 0)

                                        if(!excepted_date.trim().equalsIgnoreCase("")) {
                                            addData();
                                        }
                                        else {
                                            Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(pos1).getPOIID().toString(), explorePoiList.get(pos1).getDuration().toString(), explorePoiList.get(pos1).getDistance(), explorePoiList.get(pos1).getPOIImage(), explorePoiList.get(pos1).getPOINearbyDestination());
                                        }

                                    else {
                                        Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

                                    }
                                 /*   Bundle b=new Bundle();
                                    b.putInt("tripId",createTripApi.getTripId());
                                    b.putString("from","createtrip");
                                    ((ExploreActivity)activity).replace_fragmnet_bundle(new RoutePlanningFragment(),b);*/


                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }

                            }
                        }, CallerThreadExecutor.getInstance());






                    } else {
                        Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(),  error_layout);
                    }

                    pb_dialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });
    }
    public void add_trip_in_to_database(final int pos) {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        //   final SessionManager sessionManager1 = new SessionManager(getActivity());




        // String   trip_return_end_date="",trip_return_end_time="",trip_end_date_eta="",trip_end_time_eta="";



        try {
            if(mySpecificTripArrayList.get(pos).getTripEndDateTime()!=null) {
                trip_end_date_eta = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripEndDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                trip_end_time_eta = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripEndDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            }
            else
            {
                trip_end_date_eta = "";
                trip_end_time_eta = "";
            }
            if(mySpecificTripArrayList.get(pos).getTripReturnDateETA()!=null) {
                trip_return_end_date = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDateETA(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");

                trip_return_end_time = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDateETA(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            }
            else
            {
                trip_return_end_date = "";
                trip_return_end_time = "";
            }


        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_end_date = "";
            trip_return_end_time = "";
            trip_end_date_eta = "";
            trip_end_time_eta = "";
        }
        try {
            if(mySpecificTripArrayList.get(pos).getTripDate()!=null) {
                start_date = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                start_time = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripDate(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            }
            else
            {
                start_date = "";
                start_time = "";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            start_date = "";
            start_time = "";
        }
        try {
            if(mySpecificTripArrayList.get(pos).getTripReturnDate()!=null) {
                end_date = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                end_time = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDate(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            }
            else
            {
                end_date = "";
                end_time = "";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            end_date = "";
            end_time = "";
        }


        final String desurl = Constants.TimbThumb_ImagePath + mySpecificTripArrayList.get(pos).getEndDesImage() + "&width=" + 50 + "&height=" + 50;
        String originurl = Constants.TimbThumb_ImagePath + mySpecificTripArrayList.get(pos).getOrigindesImage() + "&width=" + 50 + "&height=" + 50;

        //stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(originurl))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


        dataSource.subscribe(new BaseBitmapDataSubscriber()
        {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    originbitmap = bitmap;

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    ImageRequest imageRequest1 = ImageRequestBuilder
                            .newBuilderWithSource(Uri.parse(desurl))
                            .setAutoRotateEnabled(true)
                            .build();

                    ImagePipeline imagePipeline1 = Fresco.getImagePipeline();
                    final DataSource<CloseableReference<CloseableImage>>
                            dataSource1 = imagePipeline1.fetchDecodedImage(imageRequest1, getActivity());


                    dataSource1.subscribe(new BaseBitmapDataSubscriber() {

                        @Override
                        public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                            if (dataSource1.isFinished() && bitmap != null) {
                                Log.d("Bitmap", "has come destination");
                                //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                Create_Trip create_trip = new Create_Trip();
                                create_trip.setSource(mySpecificTripArrayList.get(pos).getOriginDesName());
                                create_trip.setDestination(mySpecificTripArrayList.get(pos).getEndDesName());

                                create_trip.setStartdate(start_date);
                                create_trip.setStarttime(start_time);

                                create_trip.setReturn_start_date(end_date);
                                create_trip.setReturn_start_time(end_time);

                                create_trip.setEnddate(trip_end_date_eta);
                                create_trip.setEndtime(trip_end_time_eta);
                                create_trip.setReturn_end_date(trip_return_end_date);
                                create_trip.setReturn_end_time(trip_return_end_time);

                                if (end_date.length() > 1) {
                                    create_trip.setReturnstatus(true);
                                }


                                create_trip.setTripname(mySpecificTripArrayList.get(pos).getTripName());
                                create_trip.setSource_latitude(Double.valueOf(mySpecificTripArrayList.get(pos).getOriginLat()));
                                create_trip.setSource_longitude(Double.valueOf(mySpecificTripArrayList.get(pos).getOriginLong()));
                                create_trip.setDestination_latitude(Double.valueOf(mySpecificTripArrayList.get(pos).getEndDeslat()));
                                create_trip.setDestination_longitude(Double.valueOf(mySpecificTripArrayList.get(pos).getEndDesLong()));


                                create_trip.setKM(mySpecificTripArrayList.get(pos).getDistance() * 1000);
                                create_trip.setTime(mySpecificTripArrayList.get(pos).getDuration().doubleValue());
                                create_trip.setTripstatus(mySpecificTripArrayList.get(pos).getTripStatus());
                                create_trip.setTriprating("");
                                create_trip.setTripId(mySpecificTripArrayList.get(pos).getTripId());
                                create_trip.setSourceId(mySpecificTripArrayList.get(pos).getTripGoingToStart());
                                create_trip.setDestinationId(mySpecificTripArrayList.get(pos).getTripDestinationEnd());
                                create_trip.setSource_image(Constants.getImageBytes(originbitmap));
                                create_trip.setDesNoOfNearByAttraction(mySpecificTripArrayList.get(pos).getEnddesNoOfNearByAttraction());
                                create_trip.setDestination_image(Constants.getImageBytes(bitmap));
                                if(mySpecificTripArrayList.get(pos).getTripReturnKm()!=null)
                                    create_trip.setReturnKM(Double.valueOf(mySpecificTripArrayList.get(pos).getTripReturnKm()));
                                if(mySpecificTripArrayList.get(pos).getTripReturnTime()!=null)
                                    create_trip.setReturnTime(Double.valueOf(mySpecificTripArrayList.get(pos).getTripReturnTime()));

                                //   DBHandler dbHandler = new DBHandler(getActivity());

                                dbHandler.add_Trip_into_Table(create_trip);

                                Log.d("gettripplan",String.valueOf(mySpecificTripArrayList.get(pos).getGetTripPlans().get(0).getResStatus()));
                                dataSource1.close();
                                if(mySpecificTripArrayList.get(pos).getGetTripPlans().get(0).getResStatus())
                                {
                                    int i;
                                    for (i=0; i < mySpecificTripArrayList.get(pos).getGetTripPlans().size(); i++)
                                    {
                                        //  if(!myTripArrayList.get(pos).getGetTripPlans().get(i).getTripWayType().contains("PoiNight"))

                                        add_poi_plan1(mySpecificTripArrayList.get(pos).getGetTripPlans().get(i),pos);
                                    }
                                       /* Bundle b = new Bundle();
                                        b.putInt("tripId", mySpecificTripArrayList.get(pos).getTripId());
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                                        pb_dialog.dismiss();*/
                                    pb_dialog.dismiss();

                                    Integer pos1 = Integer.valueOf(poiposi);
                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(pos1).getPOIID()) == 0)
                                        if(!excepted_date.trim().equalsIgnoreCase("")) {
                                            addData();
                                        }
                                        else {
                                            Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(pos1).getPOIID().toString(), explorePoiList.get(pos1).getDuration().toString(), explorePoiList.get(pos1).getDistance(), explorePoiList.get(pos1).getPOIImage(), explorePoiList.get(pos1).getPOINearbyDestination());
                                        }

                                    else {
                                        Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

                                    }



                                }
                                else {
                                    pb_dialog.dismiss();

                                    Integer pos1 = Integer.valueOf(poiposi);
                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(pos1).getPOIID()) == 0)
                                        if(!excepted_date.trim().equalsIgnoreCase("")) {
                                            addData();
                                        }
                                        else {
                                            Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(pos1).getPOIID().toString(), explorePoiList.get(pos1).getDuration().toString(), explorePoiList.get(pos1).getDistance(), explorePoiList.get(pos1).getPOIImage(), explorePoiList.get(pos1).getPOINearbyDestination());
                                        }

                                    else {
                                        Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

                                    }
                                       /* Bundle b = new Bundle();
                                        b.putInt("tripId", mySpecificTripArrayList.get(pos).getTripId());
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);*/
                                }
                            }
                        }

                        @Override
                        public void onFailureImpl(DataSource dataSource) {
                            if (dataSource != null) {
                                dataSource.close();
                            }

                            pb_dialog.dismiss();
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                        }
                    }, CallerThreadExecutor.getInstance());

                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                pb_dialog.dismiss();
                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));

            }
        }, CallerThreadExecutor.getInstance());




    }
    public  void add_poi_plan1(final MyTrip.GetTripPlan getTripPlan, final int pos)
    {
        if(getTripPlan.getGeneralFields().getImage().length()<=1)
            getTripPlan.getGeneralFields().setImage("20161013_0512atm-selected.png");
        if(!getTripPlan.getTripWayType().contains("PoiNight"))
        {

            Log.d("Bitmap", "has come poi");
            Create_Poi create_poi = new Create_Poi();
            create_poi.setTripId(tripid);
            create_poi.setPoiName(getTripPlan.getGeneralFields().getName());
            create_poi.setStayTime(getTripPlan.getTripSpendTime());
            create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
            create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
            create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() / 1000));
            create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));
            create_poi.setPoiStay(false);
            create_poi.setArrivalTime(0.0);
            create_poi.setDepartureTime(0.0);

      /*      if (bitmap != null) {
                if (getTripPlan.getTripWayType().contains("RO")) {
                    Bitmap icon = BitmapFactory.decodeResource(getResources(),
                            R.drawable.map_slider_icon4_selected);
                   // create_poi.setPoiImage(Constants.getImageBytes(icon));

                } else
                    //create_poi.setPoiImage(Constants.getImageBytes(bitmap));
            } else {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.drawable.map_slider_icon4_selected);
                create_poi.setPoiImage(Constants.getImageBytes(icon));
            }*/
            create_poi.setPoitype(getTripPlan.getTripWayType());
            create_poi.setWayPointId(getTripPlan.getTripWayPointId());

            create_poi.setPoiDesID(getTripPlan.getTripWayRefId());
            create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

            create_poi.setReturnstatus(getTripPlan.getTripWayIsReturnPlanned());
            create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());
            create_poi.setImagename( getTripPlan.getGeneralFields().getImage());
            if (getTripPlan.getTripLegNo() != null)
                create_poi.setLegNo(Integer.valueOf(getTripPlan.getTripLegNo()));
            String resume_date = "", resume_time = "";



            try {
                if (getTripPlan.getTripResumeDateTime() != null) {
                    resume_date = Constants.formatDate(getTripPlan.getTripResumeDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                    resume_time = Constants.formatDate(getTripPlan.getTripResumeDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                } else {
                    resume_date = "";
                    resume_time = "";
                }

            } catch (ParseException e) {
                e.printStackTrace();
                resume_date = "";
                resume_time = "";
            }


            if (resume_date.length() > 1)
                create_poi.setPoiStay(true);
            create_poi.setResumeDate(resume_date);
            create_poi.setResumeTime(resume_time);

            dbHandler.add_POI_into_Table(create_poi);
        }
        else
        {
            Create_Poi_Night create_poi = new Create_Poi_Night();
            create_poi.setPoiId(getTripPlan.getTripDesId());
            create_poi.setTripId(tripid);
            create_poi.setPoiName(getTripPlan.getGeneralFields().getName());

            create_poi.setStayTime(getTripPlan.getTripSpendTime());
            create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
            create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
            create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() / 1000));
            create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));
            create_poi.setPoiStay(false);
            create_poi.setArrivalTime(0.0);
            create_poi.setDepartureTime(0.0);

            //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));
            create_poi.setPoitype("PoiNight");
            create_poi.setWayPointId(getTripPlan.getTripWayPointId());

            create_poi.setPoiDesID(getTripPlan.getTripWayRefId());
            create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

            create_poi.setReturnstatus(getTripPlan.getTripWayIsReturnPlanned());
            create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());
            create_poi.setImagename( getTripPlan.getGeneralFields().getImage());
            Log.d("add_nightdes",String.valueOf(getTripPlan.getTripWayRefId()));
            dbHandler.add_Night_POI_into_Table(create_poi);


        }


        store_image(getTripPlan.getGeneralFields().getImage());




    }
    public void store_image(String url)
    {
        if(url.length()<=1)
            url="20161013_0512atm-selected.png";

        String originurl = Constants.TimbThumb_ImagePath + url + "&width=" + 50 + "&height=" + 50;

        //stuff that updates ui
        ImageRequest imageRequest2 = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(originurl))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline2 = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource2 = imagePipeline2.fetchDecodedImage(imageRequest2, this);

        Log.d("Bitmapurl", originurl+"has come poi");
        final String finalUrl = url;
        dataSource2.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {


                new ImageSaver(getActivity()).
                        setFileName(finalUrl).

                        setExternal(true).
                        save(bitmap);

                Log.d("Bitmapurl", finalUrl+"has come poi");
                dataSource2.close();


            }
            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }


                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
            }
        }, CallerThreadExecutor.getInstance());
    }
    public void set_destination_layout() {

        for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

        {
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

        }
        try {

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
            Calendar c = Calendar.getInstance();
            Date date = format.parse(create_tripArrayList.get(0).getStartdate()+" "+create_tripArrayList.get(0).getEndtime());
            Date date1 = format.parse(create_tripArrayList.get(0).getStartdate()+" "+temptime);
            Date firstPoiDate=format.parse(create_tripArrayList.get(0).getStartdate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
            c.setTime(date);
            c.add(Calendar.HOUR, 2);
            Log.d("System out","End time of trip "+c.getTime());
            Log.d("System out","End time of day "+date1.getTime());
            if(c.getTimeInMillis()<=date1.getTime())
            {
                if(firstPoiDate.getTime()<=c.getTimeInMillis())
                {
                    destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                }
                else {

                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }





                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(0);
            }
            else
            {
                if (destinationplanList.get(0).getStarttime() == 24.00) {
                    destinationplanList.get(0).setArrivalTime(07.00);
                } else {
                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }


                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(1);
            }


        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            destinationplanList.get(0).setDay(1);
            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

        }
        if (destinationplanList.size() > 0) {

            Log.d("System out","Destination poi list size "+destinationplanList.size());
           /* if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(0);*/



            Double tempDepart=destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++)
            {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out","required time  "+reqtime);
                Log.d("System out","depature time  "+destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours)
                {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart=destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out","required time  "+ destinationplanList.get(j).getDay());


            }

        }

        noday = new ArrayList<>();
        noday.clear();

        //  noday.add(1);

        Integer day = 0;
       /* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*/
        if(destinationplanList.get(0).getDay() == 0)
        {
            day = destinationplanList.get(0).getDay();
            noday.add(destinationplanList.get(0).getDay());
        }

        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);
            try {
                if (create_tripArrayList.get(0).getEnddate().length() > 0) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                    try {
                        Date date = format.parse(create_tripArrayList.get(0).getEnddate());
                        c.setTime(date);
                        c.add(Calendar.DATE, day);
                        destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }

                }
            } catch (Exception e) {
            }

            Log.d("days","pos"+day+"v2"+destinationplanList.get(i).getDay());

            if (day != destinationplanList.get(i).getDay() ) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());

            }


        }

        Log.d("System out","Reponse of set time of days count "+noday.size());
        for (int j = 0; j < noday.size(); j++) {
            AddDayWisePlan(j);
        }

        if(pb_dialog != null)
        pb_dialog.dismiss();
        Validate_destination();
    }
    public void Validate_destination()
    {
        Date expecteddate = null;
        Date returndatenew = null;
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
            DecimalFormat f = new DecimalFormat("##.00");
            Date date = format1.parse(create_tripArrayList.get(0).getEnddate()+" "+f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".",":"));
            c.setTime(date);
            Log.d("check Destination","last_date "+date.toString());
            Log.d("check Destination","no days "+noday.get(noday.size()-1) +"   "+ create_tripArrayList.get(0).getEnddate() + "   "+f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".",":"));
            c.add(Calendar.DATE,noday.get(noday.size()-1));

            Log.d("check Destination", "last date " + create_tripArrayList.get(0).getEnddate() + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
            Log.d("check Destination", "return start time " + excepted_date + "Time  " + excepted_time);


            String date_excepted_date=excepted_date +" "+excepted_time;
            String date_return_excepted_date=return_excepted_date +" "+Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");
            Log.d("check Destination","new date "+c.getTime());
            expecteddate = format.parse(date_excepted_date);
            String temp=format.format(c.getTime());
            returndatenew= format.parse(temp);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Log.d("check Destination", "return start time " + expecteddate.toString());
        Log.d("check Destination", "new saved time " + returndatenew.toString());
        Log.d("check Destination", "new saved time " + returndatenew.after(expecteddate));
        if (returndatenew.after(expecteddate)) {
            // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            dbHandler.delete_Destination_ById(00);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Constants.show_error_popup(getActivity(),"No time for additional attractions. ", error_layout);
                              //  ((ExploreActivity)getActivity()).iv_home_icon_header.performClick();

                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();










        }
        else
        {
            dbHandler.delete_Destination_ById(00);
            Integer pos1 = Integer.valueOf(poiposi);
            if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(pos1).getPOIID()) == 0)

                Insert_Poi_Api(pos1, "DesPlan", explorePoiList.get(pos1).getPOIID().toString(), explorePoiList.get(pos1).getDuration().toString(), explorePoiList.get(pos1).getDistance(), explorePoiList.get(pos1).getPOIImage(), explorePoiList.get(pos1).getPOINearbyDestination());
            else {
                Delete_Poi_Api(dbHandler.check_des_trip_in_local_db_get_wayid(tripid, explorePoiList.get(pos1).getPOIID()));

            }

        }
    }
    public void AddDayWisePlan(int pos) {





        try {

            if (create_tripArrayList.get(0).getEnddate().length() > 0) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                try {
                    Date date = format.parse(create_tripArrayList.get(0).getEnddate());
                    c.setTime(date);
                    c.add(Calendar.DATE, noday.get(pos));

                    return_excepted_date=format.format(c.getTime());

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();


        }



        //  ll_day_wise_plan.removeAllViews();
        //  ll_add_places_to_plan.removeAllViews();

        //set time


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == noday.get(pos)) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        if (tempdestiDestinationPlanlist.size() > 0) {
            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");

            try {


                Date date = format.parse(create_tripArrayList.get(0).getStartdate()+" "+create_tripArrayList.get(0).getEndtime());
                Date date1 = format.parse(create_tripArrayList.get(0).getStartdate()+" "+temptime);
                Date firstPoiDate=format.parse(create_tripArrayList.get(0).getStartdate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime() && pos == 0)
                {

                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY) + "." + ((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                    }
                    else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }



                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                }
                else
                {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }



            Double tempDepart=tempdestiDestinationPlanlist.get(0).getArrivalTime();


            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempDepart) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    //      dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                    if (tempdestiDestinationPlanlist.get(j).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(07.00);
                    } else {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }
                    tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());

                    tempDepart=tempdestiDestinationPlanlist.get(j).getArrivalTime();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }


        }

       /* Date expecteddate = null;
        Date returndatenew = null;
        try {

            DecimalFormat f = new DecimalFormat("##.00");
            Log.d("System out", "last date " + return_excepted_date + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
            Log.d("System out", "return start time " + excepted_date + "Time  " + excepted_time);


            String date_excepted_date=excepted_date +" "+excepted_time;
            String date_return_excepted_date=return_excepted_date +" "+Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");

            expecteddate = format.parse(date_excepted_date);
            returndatenew= format.parse(date_return_excepted_date);
//            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        if (returndatenew.after(expecteddate)) {
            // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            dbHandler.delete_Destination_ById(00);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Constants.show_error_popup(context,"No time for additional attractions. ", error_layout);
                                ((ExploreActivity)getActivity()).iv_home_icon_header.performClick();

                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();










        }
        else
        {
            dbHandler.delete_Destination_ById(00);
            Integer pos1 = Integer.valueOf(posi);
            if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)

                Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
            else {
                Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
            }

        }*/


    }
}
