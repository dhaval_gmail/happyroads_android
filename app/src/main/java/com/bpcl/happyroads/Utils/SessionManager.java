package com.bpcl.happyroads.Utils;

/**
 * Created by ADMIN on 7/5/2016.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.gson.Gson;
import com.bpcl.happyroads.Pojo.OTP;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "LoginSession";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_USER_OBJ = "AuthenticateUser";
    private static final String KEY_USER_EMAIL = "UserEmail";
    private static final String KEY_USER_PASSWORD = "UserPassword";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void create_login_session(String AuthenticateUser,String email,String password) {

        editor.putBoolean(KEY_IS_LOGGEDIN, true);
        editor.putString(KEY_USER_OBJ,AuthenticateUser);
        editor.putString(KEY_USER_EMAIL,email);
        editor.putString(KEY_USER_PASSWORD,password);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void set_password(String password) {
        editor.putString(KEY_USER_PASSWORD,password);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    //check login status
    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    //check login user name
    public OTP get_Authenticate_User(){

        Gson gson = new Gson();
        String json = pref.getString(KEY_USER_OBJ, "");
        OTP obj = gson.fromJson(json, OTP.class);
        return obj;
    }
    public  String get_Email()
    {
        return pref.getString(KEY_USER_EMAIL, "");
    }
    public  String get_Password()
    {
        return pref.getString(KEY_USER_PASSWORD, "");
    }
    public void clear_login_session()
    {
        editor.clear();
        editor.commit();
        Log.d(TAG, "User login session clear!");
    }
}
