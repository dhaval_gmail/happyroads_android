

package com.bpcl.happyroads.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.common.util.ByteConstants;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.flyco.animation.BounceEnter.BounceRightEnter;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideBottomExit;
import com.flyco.animation.SlideExit.SlideLeftExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.flyco.dialog.widget.popup.BubblePopup;
import com.google.gson.Gson;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.Polygon;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.bpcl.happyroads.Pojo.ListDefaultValues;
import com.bpcl.happyroads.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by ADMIN on 8/29/2016.
 */
public class Constants {
    public static String frommapplan="";
    public static String SelectedCat="";

  /*  public static final String BASE_URL = "http://67.220.184.226/PTOinternal/";
    public static final String BASE_URL_image = "http://67.220.184.226/PTO/";
    public static String ImagePath=BASE_URL_image+"UploadImages/";
    public static String TimbThumb_ImagePath=BASE_URL_image+"ImageHandler.ashx?q=yes&image=UploadImages/";
*/
   public static final String BASE_URL = "http://52.66.6.176/pto/";


  // public static final String BASE_URL = "http://www.bpclhappyroads.in/HappyRoads/";
    public static String ImagePath=BASE_URL+"UploadImages/";
    public static String TimbThumb_ImagePath=BASE_URL+"ImageHandler.ashx?q=yes&image=UploadImages/";

    public static final String ANDROID_KEY = "AIzaSyAWVSrvxDZqeEFMK4hmeQc3EqzN-vN-ISk";
    public  static String WeatherIconPath="http://openweathermap.org/img/w/";
    public static boolean fromSublistingforSb=false;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private static ImagePipelineConfig imagePipelineConfig;
    public static String selectedLat="";
    public static String selectedLong="";
    public static String destSelectionfrom="";
  //  public static String selectedPosition="0";
    public static double MyLatitute=0.0;
    public static double MyLongitute=0.0;
    public static File file;
    public static final String version="2.0";
    public static String GetCMS=BASE_URL+"CMSTemplate/GetCMS?TemplateName=";

    Bitmap bmp=null;
    public static String VehicleNumber = "^[a-zA-Z]{2}[0-9]{1,2}[a-zA-Z]{1,2}[0-9]{4}$";
    public static String MobilePattern = "[0-9]{10}";
    public static String emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

    //twitter
    public static final String CONSUMER_KEY = "dySuYJeQAK9mMWKcCHq48i9zU";
    public static final String CONSUMER_SECRET = "JQh1VxYZtuVYIsPmxcl6Y0aFtlmhmRyZEyruEcLnmOvMt9o98O";
    public static final String CALLBACK_URL_TWITTER = "myapp://oauth";
    public static final String PREF_NAME = "com.example.android-twitter-oauth-demo";
    public static final String PREF_KEY_ACCESS_TOKEN = "access_token";
    public static final String PREF_KEY_ACCESS_TOKEN_SECRET = "access_token_secret";

    public static final String IEXTRA_AUTH_URL = "auth_url";
    public static final String IEXTRA_OAUTH_VERIFIER = "oauth_verifier";
    public static final String IEXTRA_OAUTH_TOKEN = "oauth_token";
    public static final String CALLBACK_URL = "instagram://connect";
    public static String DEVICE_TOKEN="";
    public static String SENDER_ID="451129519557";

    public static final String TripTrackPREFERENCES = "MyPrefs" ;

    static File cacheDir;
    public static  ArrayList<ListDefaultValues> listDefaultValuesArrayList=new ArrayList<>();

    public static Polygon polygon = null;
    public static Polygon polygon1 = null;
    public static Marker withinMarker = null;

    public static void executeLogcat(Context context){
        Log.d("System out", "Create Log file..");

        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"HappyRoads");
        else
            cacheDir=context.getCacheDir();

        if(!cacheDir.exists())
            cacheDir.mkdirs();

        File logFile = new File(cacheDir, "logoutput.log"); // log file name
        int sizePerFile = 60; // size in kilobytes
        int rotationCount = 10; // file rotation count
        String filter = "D"; // Debug priority

        String[] args = new String[] { "logcat",
                "-v", "time",
                "-f",logFile.getAbsolutePath(),
                "-r", Integer.toString(sizePerFile),
                "-n", Integer.toString(rotationCount),
                "*:" + filter };

        try {
            Runtime.getRuntime().exec(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String formatDate(String date, String initDateFormat, String endDateFormat) throws ParseException {
        Date initDate = new SimpleDateFormat(initDateFormat).parse(date.trim());
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);
        return parsedDate;
    }
    public static String getMonthName(int month)
    {
        Calendar cal= Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        int monthnum=month;
        cal.set(Calendar.MONTH,monthnum-1);
        String month_name = month_date.format(cal.getTime());

        Log.e("Month get is",""+month_name);
        return month_name;
    }
    public static String convert_object_string(Object obj){
        Gson gson = new Gson();
        String json = gson.toJson(obj); // myObject - instance of MyObject
        return json;
    }
    public static String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})";

    public static ImagePipelineConfig AddImagesToCache(Context co) {
        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(co).setBaseDirectoryPath(co.getCacheDir())
                .setBaseDirectoryName("v1")
                .setMaxCacheSize(100 * ByteConstants.MB)
                .setMaxCacheSizeOnLowDiskSpace(10 * ByteConstants.MB)
                .setMaxCacheSizeOnVeryLowDiskSpace(5 * ByteConstants.MB)
                .setVersion(1)
                .build();
        imagePipelineConfig = ImagePipelineConfig.newBuilder(co)
                .setMainDiskCacheConfig(diskCacheConfig)
                .build();
        return imagePipelineConfig;
    }
    public static byte[] getImageBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
    /**
     * hide soft keyboard
     *
     */
    public static void hideSoftKeyBoardOnTabClicked(Context context,View v) {
        if (v != null && context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targtetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(targtetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
    /**
     * Checks if the Internet connection is available.
     *
     * @return Returns true if the Internet connection is available. False otherwise.
     * *
     */
    public static boolean isInternetAvailable(Context ctx) {
        ConnectivityManager check = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (check != null) {
            NetworkInfo[] info = check.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                        //Toast.makeText(context, "Internet is connected", Toast.LENGTH_SHORT).show();
                    }
        }
        return false;
    }
    public static void show_error_popup(Context context,String msg,View v)
    {
        View inflate = View.inflate(context, R.layout.popup_bubble_text, null);
        TextView tv = (TextView) inflate.findViewById(R.id.tv_bubble);
        tv.setText(msg);
        BubblePopup bubblePopup = new BubblePopup(context, inflate);
        bubblePopup.anchorView(v)
                .showAnim(new BounceRightEnter())
                .dismissAnim(new SlideLeftExit())
                .autoDismiss(true)
                .gravity(Gravity.TOP)
                .bubbleColor(Color.parseColor("#FF0000"))

                .show();
        //   v.setFocusable(true);
        // v.requestFocus();
    }
    public static void show_error_popup_success(Context context,String msg,View v)
    {
        View inflate = View.inflate(context, R.layout.popup_bubble_text, null);
        TextView tv = (TextView) inflate.findViewById( R.id.tv_bubble);
        tv.setText(msg);
        BubblePopup bubblePopup = new BubblePopup(context, inflate);
        bubblePopup.anchorView(v)
                .showAnim(new BounceRightEnter())
                .dismissAnim(new SlideLeftExit())
                .autoDismiss(true)
                .gravity(Gravity.TOP)
                .bubbleColor(Color.parseColor("#19A412"))

                .show();
        //   v.setFocusable(true);
        // v.requestFocus();
    }

    public  static Dialog get_dialog(Context context, int layoutid)
    {
        Dialog dialog = new  Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layoutid);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.transparent)));
        return dialog;
    }
    public  static String getDistanceOnRoad(double latitude, double longitude,
                                            double prelatitute, double prelongitude) {

        String url = "json?origins="
                + latitude + "," + longitude + "&destinations=" + prelatitute
                + "," + prelongitude + "&mode=driving&language=en-EN&sensor=false";

        return url;
    }

    public Bitmap  get_bitmap_from_freco(final String url)
    {




//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");
                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    bmp=bitmap;
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                bmp=null;
            }
        }, CallerThreadExecutor.getInstance());

        return bmp;
    }

    public static  String convert_minute_hrs(Double t)
    {
        String timeformat="";

        long longVal = t.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours , mins , secs};



        if(hours==0)
        {
            if(mins>0)
                timeformat=hours+"."+String.format("%02d",mins)+" hrs";
            else
                timeformat= String.valueOf(hours)+" hrs";
        }
        else if(mins<=0)
        {
            timeformat= String.valueOf(hours)+" hrs";
        }
        else
        {
            timeformat= String.valueOf(hours)+"."+String.format("%02d",mins)+" hrs";
        }

        return timeformat;
    }

    public static  String convert_minute_hrs_both(Double t)
    {
        String timeformat="";

        long longVal = t.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours , mins , secs};



        if(hours==0)
        {
            if(mins>0)
                timeformat=String.format("%02d",mins)+" mins";
            else
                timeformat= String.valueOf(hours)+" hrs";
        }
        else if(mins<=0)
        {
            timeformat= String.valueOf(hours)+" hrs";
        }
        else
        {
            timeformat= String.valueOf(hours)+" hrs "+String.format("%02d",mins)+" mins";
        }

        return timeformat;
    }

    public static  String convert_minute_hrs1(Double t)
    {
        String timeformat="";

        long longVal = t.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
        if(mins<0)
            mins=Math.abs(mins);
        int[] ints = {hours , mins , secs};
        timeformat= String.valueOf(hours)+"."+String.format("%02d",mins);


        return timeformat;
    }

    public static boolean checkEmail(String email) {
        return Constants.EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }
    public static Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );



    // To animate view slide out from left to right
    public static void slideToRight(View view){
        TranslateAnimation animate = new TranslateAnimation(0,view.getWidth(),0,0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }
    // To animate view slide out from right to left
    public static void slideToLeft(View view){
        TranslateAnimation animate = new TranslateAnimation(0,-view.getWidth(),0,0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    // To animate view slide out from top to bottom
    public static void slideToBottom(View view,boolean flag){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);

        view.setVisibility(View.VISIBLE);

    }

    // To animate view slide out from bottom to top
    public static void slideToTop(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,-view.getHeight());
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    public static void display_popup(Context c,String msg)
    {
        BounceTopEnter mBasIn = new BounceTopEnter();
        SlideBottomExit mBasOut = new SlideBottomExit();

        final NormalDialog dialog = new NormalDialog(c);


        dialog.isTitleShow(false)//
                .bgColor(c.getResources().getColor(R.color.white))//
                .cornerRadius(5)//
                //.title(getString(R.string.forgot_password_success_title))//
                .content(msg)//
                .contentGravity(Gravity.CENTER)//
                .contentTextColor(c.getResources().getColor(R.color.colorAccent))//
                .dividerColor(c.getResources().getColor(R.color.white))//
                .btnNum(1)
                .btnText("Ok")
                .btnTextSize(15.5f)//
                .btnTextColor(c.getResources().getColor(R.color.colorAccent))//
                .btnPressColor(c.getResources().getColor(R.color.colorAccent))//
                .widthScale(0.85f)//
                .showAnim(mBasIn)//
                .dismissAnim(mBasOut)//
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();

                    }
                });
    }
    public static void Calling(String number,Context context) {


        Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
        callIntent.setData(Uri.parse("tel:" + number));    //this is the phone number calling
        //check permission
        //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
        //the system asks the user to grant approval.
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            //request permission from user if the app hasn't got the required permission
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                    10);
            return;
        } else {     //have got permission
            try {
                context.startActivity(callIntent);  //call activity and make phone call
            } catch (android.content.ActivityNotFoundException ex) {
                //  Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
            }
        }
    }
    public static void hidekeyboard(Activity activity, View v) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }




    public static ArrayList<LatLng> points = new ArrayList<>();

    public static String getDirectionsUrl(LatLng origin, LatLng dest, String waypoint) {

        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + "," + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();

        // Sensor enabled
        String sensor = "sensor=false";

        String waypoints = "waypoints=";
        waypoints += waypoint;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints + "&mode=driving";

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    public static String getDirectionsUrl1(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + "," + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&mode=driving";

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        Log.i("ok","google direction--->"+url);
        return url;
    }
  public static String getDirectionsUrl_offline(LatLng origin, LatLng dest, String waypoint) {
        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + "," + origin.getLongitude();
        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();// Sensor enabled
        String sensor = "sensor=false";
        String waypoints = "waypoints=";
        waypoints += waypoint;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints + "&mode=driving";
        // Output format
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    public static String getDirectionsUrl1_without_baseurl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + "," + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&mode=driving";

        // Output format
        String output = "json";

        String url = "directions/" + output + "?" + parameters;

        Log.i("ok","google direction--->"+url);
        return url;
    }
    public static String getDirectionsUrl_offline1__without_baseurl(LatLng origin, LatLng dest, String waypoint) {
        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + "," + origin.getLongitude();
        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();// Sensor enabled
        String sensor = "sensor=false";
        String waypoints = "waypoints=optimize:true|";
        waypoints += waypoint;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints + "&mode=driving";
        // Output format
        String output = "json";
        String url = "directions/" + output + "?" + parameters;
        return url;
    }
    public static void SpacialCharacterNotAllow(EditText editText) {
        if (editText.getText().toString().length() > 0) {

            char x;
            int[] t = new int[editText.getText().toString()
                    .length()];

            for (int i = 0; i < editText.getText().toString()
                    .length(); i++) {
                x = editText.getText().toString().charAt(i);
                int z = (int) x;
                t[i] = z;

                if ((z > 47 && z < 58) || (z >= 64 && z < 91)
                        || (z > 96 && z < 123) || z==33 || z==35) {

                } else {

                    editText.setText(editText
                            .getText()
                            .toString()
                            .substring(
                                    0,
                                    (editText.getText()
                                            .toString().length()) - 1));
                    editText.setSelection(editText
                            .getText().length());
                }
                Log.d("System out", "decimal value of character" + z);

            }
        }
    }

    public static void showSnackMsg(Context c,String msg,View v)
    {
        Snackbar snackbar = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        View sbView = snackbar.getView();
        TextView tv = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextSize(14f);
        tv.setTextColor(Color.WHITE);
        sbView.setBackgroundColor(ContextCompat.getColor(c, R.color.pink));
        snackbar.show();
    }

}
