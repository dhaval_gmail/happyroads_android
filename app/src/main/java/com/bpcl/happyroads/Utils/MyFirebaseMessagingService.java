package com.bpcl.happyroads.Utils;

/**
 * Created by ADMIN on 9/6/2016.
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.bpcl.happyroads.ExploreActivity;
import com.bpcl.happyroads.Services.StartTrip;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.bpcl.happyroads.IncomingMessageView;
import com.bpcl.happyroads.NotificationMsgDisplayActivity;
import com.bpcl.happyroads.R;

import java.util.List;


/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String ActivityName = "";
    SharedPreferences preferences;

    SharedPreferences.Editor editor;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "From: " + remoteMessage.getNotification());
        Log.d(TAG, "From: " + remoteMessage.getData().get("body").toString());

       // Log.d(TAG, "From: " + remoteMessage.getData().get("ntfRefKey").toString());

        preferences = getSharedPreferences(Constants.MyPREFERENCES, this.MODE_PRIVATE);
        editor=preferences.edit();
        // Check if message contains a data payload.
       /* if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if(remoteMessage.getData().containsKey("image") )
            {

                String message = remoteMessage.getData().get("msg").toString();
                String image = remoteMessage.getData().get("image").toString();
                Log.d("NotificationImage1", "From: " + message+"/"+image);

                Intent resultIntent;

                resultIntent = new Intent(getApplicationContext(), SplashActivity.class);



                resultIntent.putExtra("message", message);
                resultIntent.putExtra("image",image);



                // eg. Server Send Structure data:{"post_id":"12345","post_title":"A Blog Post"}
            }
        }*/
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
            {
                editor.putString("notification",  remoteMessage.getData().get("body").toString());
                editor.commit();
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

                    String message = remoteMessage.getNotification().getBody();
                  //  Log.d("System out", "Got a message!---" + remoteMessage.getString("payload"));
                    Log.d("System out", "Got a Activity!---" + ActivityName);

                    editor = preferences.edit();
                    Log.d("System out", "Got a Activity!--- Spit size in not back"+message);
                    Log.d("System out", "Got a Activity!--- Spit size in not back"+message.contains("~"));
                    if(message.contains("~"))
                    {
                        String[] abc =message.split("~");
                        Log.d("System out", "Got a Activity!--- Spit size in not back" + abc.length + abc[0]+" "+abc[1]);
                        if(abc.length<=5)
                        {

                            //editor.putString("PushMsgkey", abc[0]+" "+abc[1]);
                            editor.putString("PushMsgkey", abc[1]);
                        }
                        else
                        {
                            editor.putString("PushMsgkey", message);
                        }
                    }
                    else
                    {
                        editor.putString("PushMsgkey", message);
                    }



                    editor.commit();
                if (!isApplicationBroughtToBackground(this)) {
                    Intent intent2 = new Intent(this, NotificationMsgDisplayActivity.class);
                  //  intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent2);

                }
                else {

                    sendNotification(remoteMessage.getNotification().getBody().toString());
                }

        }
       // Log.d("NotificationImage", "From: " + remoteMessage.getData());


       /* if(remoteMessage.getNotification()!=null) {
            Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

            //Calling method to generate notification
            sendNotification(remoteMessage.getNotification().getBody());
        }*/

    }

    //This method is only generating push notification
    //It is same as we did in earlier posts

    private void sendNotification(String messageBody)
    {
       /* Intent intent = new Intent(this, IncomingMessageView.class);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                0);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.appicon))
                .setContentTitle("Firebase Push Notification")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
      *//*  NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(this);
        Resources res = this.getResources();
        builder.setSmallIcon(R.drawable.appicon)
                .setContentTitle("Firebase Push Notification")
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.appicon))
                .setContentText(messageBody)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        Notification notification = builder.getNotification();
        notification.vibrate = new long[]{100, 250, 100, 500};
        Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notification.sound = defaultRingtoneUri;
        nm.notify(0, notification);*//*
       NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());*/
        Intent intent = new Intent(this, IncomingMessageView.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


        // Create remote view and set bigContentView.
        RemoteViews expandedView = new RemoteViews(this.getPackageName(),
                R.layout.custom_notification);
        expandedView.setTextViewText(R.id.tv_trip_name, "Happy Roads");
        expandedView.setTextViewText(R.id.tv_trip_date, messageBody);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Notification notification = new NotificationCompat.Builder(MyFirebaseMessagingService.this)
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSound(defaultSoundUri)
                .setCustomBigContentView(expandedView)
                .setContentText(messageBody)
                .setContentTitle("Happy Roads").build();

        NotificationManager notificationManager =
                (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);
    }
    /**
     * Showing notification with text only
     */
    private boolean isApplicationBroughtToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);

        if (!tasks.isEmpty()) {

            Log.d("System out", " ticket 5!---" );
            ComponentName topActivity = tasks.get(0).topActivity;
            Log.d("System out", " ticket 5!---" );
            ActivityName = topActivity.getClassName().toString();
            if (!topActivity.getPackageName().equals(context.getPackageName())) {

                Log.d("System out", " in if " + topActivity.getPackageName() + " sdhfksfh " + context.getPackageName());

                return true;
            }

        }
        return false;
    }
}
