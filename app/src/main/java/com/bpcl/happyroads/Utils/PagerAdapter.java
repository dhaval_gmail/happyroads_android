package com.bpcl.happyroads.Utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bpcl.happyroads.MyTripsFragment;

/**
 * Created by ADMIN on 17/11/2016.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    int tripid;

    public PagerAdapter(FragmentManager fm, int NumOfTabs,int tripid) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.tripid=tripid;
    }

    @Override
    public Fragment getItem(int position) {
        MyTripsFragment myTripsFragment=new MyTripsFragment();
        Bundle bundle=new Bundle();
        if(position==0)
        {
            bundle.putString("trip_status","Completed");
            bundle.putInt("tripid",-1);

        }else if(position==1)
        {
            bundle.putString("trip_status","OnGoing");
            bundle.putInt("tripid",-1);


        }else if(position==2)
        {
            bundle.putString("trip_status","UpComing");
            bundle.putInt("tripid",tripid);


        }else if(position==3)
        {
            bundle.putString("trip_status","Saved");
            bundle.putInt("tripid",tripid);


        }
        myTripsFragment.setArguments(bundle);
        return myTripsFragment;



    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
