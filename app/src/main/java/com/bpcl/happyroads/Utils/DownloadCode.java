package com.bpcl.happyroads.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bpcl.happyroads.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;


public class DownloadCode {

	ProgressBar progressBar;
	Dialog pb_dialog;
	String path, filename;
	Activity activity;
	String imgURL;
	int flag=0;
	File[] listFile;
	 String PATH ;
	 private ProgressDialog mProgressDialog;
	Bitmap bitmap;
	String sharetext="";
	public DownloadCode(Activity ac, String path, String filename,Bitmap bm,String text) {
		this.path = path;
		Log.d("System out","selected imaage name "+filename);
		if(filename.contains("_"))
		{
			this.filename = filename;
		}
		else
		{
			this.filename="";
		this.filename = filename;
		}
		this.bitmap=bm;
		this.activity = ac;
		this.sharetext=text;
		 //imgURL = "http://mobile.sneh-desai.com" + this.path;
		 imgURL = this.filename;
		 Log.d("System out", "image path: "+imgURL);
	}

	File pictureFile;
	private Object contentResolver;

	@SuppressLint("NewApi")
	public void share_image_text_GPLUS() {
		try {
			
			Log.d("System out", "In Download Code 1");


			String root = Environment.getExternalStorageDirectory().toString();
            File rootSdDirectory = new File(root + "/PTO");
            Log.d("System out", "file path"+rootSdDirectory.toString());
            rootSdDirectory.mkdirs();
            
       
			/*final File rootSdDirectory = new File(Environment.getExternalStoragePublicDirectory(
			        Environment.DIRECTORY_PICTURES), "SnehWorld");
			*/
		//	File rootSdDirectory = new File(rootSdDirectoryPath);

			if (!rootSdDirectory.exists())
				rootSdDirectory.mkdir();
			
			System.out.println("++++++++++++++++++++"+filename);
			if(this.filename.contains("/"))
			{
				
				Log.d("System out", "In Download Code"+filename);
				
				System.out.println("++++++++++++++++++++//////////////////////////////////in if condition"+filename);
				String[] spit=this.filename.split("/");
				this.filename=spit[1];
			}
			System.out.println("++++++++++++++++++++//////////////////////////////////"+filename);
			pictureFile = new File(rootSdDirectory, this.filename);

			if (pictureFile.exists()) {
				Log.d("System out", "yes already downloaded "+filename);
				//handler.sendEmptyMessage(1);
				
				if(pictureFile.toString().contains(".ppt"))
				handler.sendEmptyMessage(1);
				else if(pictureFile.toString().contains(".mp4") || pictureFile.toString().contains(".mov"))
					handler.sendEmptyMessage(2);
				else if(pictureFile.toString().contains(".mp3"))
				handler.sendEmptyMessage(3);
				else if(pictureFile.toString().contains(".jpg") || pictureFile.toString().contains(".png"))
					handler.sendEmptyMessage(4);

			}
			else{
				Log.d("System out", "need to downloaded");
			pictureFile.createNewFile();
			/*if(pictureFile.toString().contains(".mp3"))
			{
				 new DownloadFileAsync().execute(pictureFile);
			}
			else
			{*/
			
			//callDialog(1);
			
			if(filename.contains(".ppt")){
				try
				{
					 new DownloadPPTAsync().execute();
				//getDataSource(pictureFile);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			else if(pictureFile.toString().contains(".mp4") || pictureFile.toString().contains(".mov") )
			{try
			{
				 new Downloadmp4Async().execute();
				//getDataSource1(pictureFile);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			}
			else if(pictureFile.toString().contains(".mp3"))
			{
				try
				{
					 new DownloadMp3Async().execute();
				//getDataSource2(pictureFile);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			else if(pictureFile.toString().contains(".jpg") || pictureFile.toString().contains(".png"))
			{
				try
				{

					SaveImage(bitmap);
					//getDataSource2(pictureFile);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			//}
			}

		} catch (Exception e) {
			System.out.print(e);
			e.printStackTrace();
			return;
		}
	}

	Dialog dialog;

	public void callDialog(final int from) {

		System.gc();
		Runtime.getRuntime().gc();
		pb_dialog = Constants.get_dialog(activity, R.layout.custom_progressbar);
		pb_dialog.setCancelable(false);
		pb_dialog.show();

		Window window = dialog.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		dialog.show();

		new Thread(new Runnable() {

			@Override
			public void run() {
				
				if(filename.contains(".pdf")){
					downloadPdfContent(imgURL);
					//downloadPdfContent("http://mobile.sneh-desai.com/joomlatools-files/docman-files/Brochures/"+filename);
					
				}else if(filename.contains(".ppt")){
					try
					{
					getDataSource(pictureFile);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
				}
				else if(pictureFile.toString().contains(".mp4") || pictureFile.toString().contains(".mov") )
				{try
				{
					getDataSource1(pictureFile);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				}
				else if(pictureFile.toString().contains(".mp3"))
				{
					try
					{
					getDataSource2(pictureFile);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				
			}
		}).start();
	}

	public void downloadPictureFile(File pictureFile, int from) {
		try {
			if (pictureFile.exists())
				pictureFile.delete();

			FileOutputStream fos = new FileOutputStream(pictureFile);
			 
			 imgURL = this.path+filename;
			 //imgURL = "http://mobile.sneh-desai.com" + this.path;
			
			Log.d("System out", "In Download Code Path "+imgURL);
			
			URL url = new URL(imgURL);
			Log.d("System out", "above http ");
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			Log.d("System out", "after http");
			connection.setRequestMethod("GET");
			Log.d("System out", "after get");
			connection.setDoOutput(true);
			connection.connect();
			Log.d("System out", "after connect");
			/*if(connection.)
			{
				 flag = 1;
			
            	dialog.cancel();
			}*/
			InputStream in = connection.getInputStream();
			Log.d("System out", "afeter in");

			byte[] buffer = new byte[1024];
			int size = 0;
			
			while ((size = in.read(buffer)) > 0) {
				fos.write(buffer, 0, size);
			}
			fos.close();
			//dialog.cancel();
			handler.sendEmptyMessage(0);
		} catch (Exception e) {
			System.out.print(e);
			e.printStackTrace();
			return;
		}

	}
	
	 public void downloadPdfContent(String urlToDownload){

         try {

             String fileName1=filename;
         String fileExtension=".pdf";

//       download pdf file.

         Log.d("System out", "pdf url "+urlToDownload);
         
            URL url = new URL(urlToDownload);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            String PATH = Environment.getExternalStorageDirectory() + "/Saflize/";
            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(file, fileName1);
            FileOutputStream fos = new FileOutputStream(outputFile);
            InputStream is = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
                
                Log.d("System out", "pdf url "+len1);
            }
           
            fos.close();
            is.close();
            handler.sendEmptyMessage(1);
           System.out.println("--pdf downloaded--ok--"+urlToDownload);
        } catch (Exception e) {
            e.printStackTrace();

        }
}
	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			
			if(dialog!=null){
				dialog.cancel();
			}
			
			switch (msg.what) {
			case 0:
				
			/*	if(flag == 1)
				{
					Toast.makeText(activity, "Download Error", 3000).show();
				}else
				{*/
					if(dialog!=null){
						dialog.cancel();
					}
					System.out.println("============================================="+imgURL);
			/*	Intent i=new Intent(activity,ImageViewDisplay.class);
				i.putExtra("imgURL", imgURL);
				System.out.println("============================================="+imgURL);
				activity.startActivity(i);*/
				//}
				break;
			case 1:
				if(dialog!=null){
					dialog.cancel();
				}
				try
				{
				 Intent intent = new Intent(Intent.ACTION_VIEW);
				 String myfile = Environment.getExternalStorageDirectory() + "/Saflize/"+filename;
		         intent.setDataAndType(Uri.fromFile(new File(myfile)), "application/vnd.ms-powerpoint");
		         activity.startActivity(intent);
				}
				catch (Exception e) {
					// TODO: handle exception
					//Toast.makeText(activity, "Please install Document reader", Toast.LENGTH_LONG).show();
				}
				break;
				
			case 2:
				if(dialog!=null){
					dialog.cancel();
				}
				try
				{
				 Intent intent1 = new Intent(Intent.ACTION_VIEW);
				 String myfile1 = Environment.getExternalStorageDirectory() + "/Saflize/"+filename;
		         intent1.setDataAndType(Uri.fromFile(new File(myfile1)), "video/*");
		         activity.startActivity(intent1);
				}
				catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(activity, "Please install Document reader", Toast.LENGTH_LONG).show();
				}
				break;
			case 3:
				if(dialog!=null){
					dialog.cancel();
				}
				try
				{
				 Intent intent1 = new Intent(Intent.ACTION_VIEW);
				 String myfile1 = Environment.getExternalStorageDirectory() + "/Saflize/"+filename;
		         intent1.setDataAndType(Uri.fromFile(new File(myfile1)), "audio/mp3");
		         activity.startActivity(intent1);
				}
				catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(activity, "Please install Document reader", Toast.LENGTH_LONG).show();
				}
				break;
				case 4:

					try
					{
						String myfile1 = Environment.getExternalStorageDirectory() + "/PTO/"+filename;

						Intent shareIntent = new Intent();

						shareIntent.setAction(Intent.ACTION_SEND);
						shareIntent.setType("text/plain");
						shareIntent.putExtra(Intent.EXTRA_STREAM,(Uri.fromFile(new File(myfile1))));
						shareIntent.putExtra(Intent.EXTRA_TEXT, sharetext);
						shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Happy Roads");
						shareIntent.setType("image/jpeg");
						activity.startActivity(Intent.createChooser(shareIntent,"Share images to"));

					}
					catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(activity, "Please install Document reader", Toast.LENGTH_LONG).show();
					}
					break;
				
			default:
				break;
			}
		}
	};

	
	public boolean getFromSdcard(String filename)
	{
	    File file= new File(Environment.getExternalStorageDirectory(),"Sefliz");

	        if (file.isDirectory())
	        {
	            listFile = file.listFiles();
	            for (int i = 0; i < listFile.length; i++)
	            {
	            	Log.d("System out", "path in function "+listFile[i].toString());
	            	String str=listFile[i].toString().substring(listFile[i].toString().lastIndexOf("/")+1, listFile[i].toString().length());
	            	Log.d("System out", "str "+str);
	            	
	            	if(str.equalsIgnoreCase(filename))
	            	{
	            		Log.d("System out", "path in condition "+listFile[i].toString()+" "+filename);
	            		return true;
	            		
	            	}
	            }
	        }
	       return false;
	}
	  private void getDataSource(File path) throws IOException {
	       /* if (!URLUtil.isNetworkUrl(path)) {
	            return path;
	        } else {*/
		 
		  	String abc = Constants.TimbThumb_ImagePath +filename;
		  	 Log.d("System out", "in download method"+abc);
	            URL url = new URL(abc);
	            URLConnection cn = url.openConnection();
	            cn.connect();
	            InputStream stream = cn.getInputStream();
	            if (stream == null)
	                throw new RuntimeException("stream is null");
	            File temp = File.createTempFile("mediaplayertmp", filename);
	            temp.deleteOnExit();
	            String tempPath = temp.getAbsolutePath();
	            FileOutputStream fos = new FileOutputStream(path);
	            byte buf[] = new byte[128];
	            do {
	                int numread = stream.read(buf);
	                if (numread <= 0)
	                    break;
	                fos.write(buf, 0, numread);
	            } while (true);
	            try {
	                stream.close();
	            } catch (IOException ex) {
	                //Log.e(TAG, "error: " + ex.getMessage(), ex);
	           // }
	        }
	            handler.sendEmptyMessage(1);
	    }
	  private void getDataSource1(File path) throws IOException {
	       /* if (!URLUtil.isNetworkUrl(path)) {
	            return path;
	        } else {*/
		 
		  	String abc = Constants.TimbThumb_ImagePath +filename;
		  	 Log.d("System out", "in download method"+abc);
	            URL url = new URL(abc);
	            URLConnection cn = url.openConnection();
	            cn.connect();
	            InputStream stream = cn.getInputStream();
	            if (stream == null)
	                throw new RuntimeException("stream is null");
	            File temp = File.createTempFile("mediaplayertmp", filename);
	            temp.deleteOnExit();
	            String tempPath = temp.getAbsolutePath();
	            FileOutputStream fos = new FileOutputStream(path);
	            byte buf[] = new byte[128];
	            do {
	                int numread = stream.read(buf);
	                if (numread <= 0)
	                    break;
	                fos.write(buf, 0, numread);
	            } while (true);
	            try {
	                stream.close();
	            } catch (IOException ex) {
	                //Log.e(TAG, "error: " + ex.getMessage(), ex);
	           // }
	        }
	            handler.sendEmptyMessage(2);
	    }
	  private void getDataSource3(File path) throws IOException {
		  int count;
		  try {
		  String abc = Constants.TimbThumb_ImagePath +filename;
		    URL url = new URL(abc);
		   Log.d("System out", "dowload .m3 path "+abc);
		    URLConnection conexion = url.openConnection();
		    conexion.connect();
		    // this will be useful so that you can show a tipical 0-100% progress bar
		    int lenghtOfFile = conexion.getContentLength();

		    // downlod the file
		    InputStream input = new BufferedInputStream(url.openStream());
		    OutputStream output = new FileOutputStream(path);

		    byte data[] = new byte[1024*10];

		    long total = 0;

		    while ((count = input.read(data)) != -1) {
		        total += count;
		        // publishing the progress....
		     
		        output.write(data, 0, count);
		    }

		    output.flush();
		    output.close();
		    input.close();
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		  handler.sendEmptyMessage(3);  
	  }
	  
	  
	  private void getDataSource2(File path) throws IOException {
	        InputStream stream = null;
	        String urlString =Constants.TimbThumb_ImagePath +filename;
	        URL url = new URL(urlString);
	        URLConnection connection = url.openConnection();
	        Log.d("System out", "dowload .m3 path "+urlString);
	        try {
	            HttpURLConnection httpConnection = (HttpURLConnection) connection;
	            httpConnection.setRequestMethod("GET");
	            httpConnection.connect();

	            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
	                stream = httpConnection.getInputStream();
	            }
	            
	            try {
	            	Log.d("System out", "dowload in output stream "+(stream==null));	   
	                // OutputStream output = new FileOutputStream("/sdcard/PHOTOBOOTH/downloadedfile.jpg");
	      // File file = new File(sdIconStorageDir, filename);
	       FileOutputStream outStream = new FileOutputStream(path);
	    
	       BufferedInputStream inStream = new BufferedInputStream(stream, 1024 * 1);
	                byte[] buff = new byte[1 * 1024];
	                //Read bytes (and store them) until there is nothing more to read(-1)
	                int len = 0;
	                while ((len = inStream.read(buff)) != -1){
	                    outStream.write(buff,0,len);
	                    Log.d("System out", "dowload in output stream  in while");
	                }
	                //clean up
	                outStream.flush();
	                outStream.close();
	                inStream.close();
	                
	       /*OutputStream output = new FileOutputStream(file);
	                pathp = file.getAbsolutePath();
	                byte data[] = new byte[1024];
	                //long total = 0;
	                while ((count = input.read(data)) != -1) {
	                    output.write(data, 0, count);
	                }
	                // flushing output
	                output.flush();
	                // closing streams
	                output.close();
	                input.close();*/
	       
	      }catch (FileNotFoundException e) {
	       Log.w("TAG", "Error saving image file: " + e.getMessage());
	    
	      } catch (IOException e) {
	       Log.w("TAG", "Error saving image file: " + e.getMessage());
	      
	      }catch (Exception e) {
	       // TODO: handle exception
	       e.printStackTrace();
	     
	      }
	            
	            
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        handler.sendEmptyMessage(3);  
	    }
	  
	  
	  class DownloadMp3Async extends AsyncTask<String, String, String> {
		   
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				mProgressDialog = new ProgressDialog(activity);
				mProgressDialog.setMessage("Downloading file..");
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
			}

			@Override
			protected String doInBackground(String... aurl) {
				int count;

			try {
				 String abc = Constants.TimbThumb_ImagePath +filename;
			URL url = new URL(abc);
			URLConnection conexion = url.openConnection();
			conexion.connect();

			int lenghtOfFile = conexion.getContentLength();
			Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = new FileOutputStream(pictureFile);

			byte data[] = new byte[1024];

			long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					publishProgress(""+(int)((total*100)/lenghtOfFile));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();
			} catch (Exception e) {}
			return null;

			}
			protected void onProgressUpdate(String... progress) {
				 Log.d("ANDRO_ASYNC",progress[0]);
				 mProgressDialog.setProgress(Integer.parseInt(progress[0]));
			}

			@Override
			protected void onPostExecute(String unused) {
				mProgressDialog.dismiss();
				handler.sendEmptyMessage(3);
			}
		}
	  
	  
	  
	  class DownloadPPTAsync extends AsyncTask<String, String, String> {
		   
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				mProgressDialog = new ProgressDialog(activity);
				mProgressDialog.setMessage("Downloading file..");
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
			}

			@Override
			protected String doInBackground(String... aurl) {
				int count;
				 try {
				String abc = Constants.TimbThumb_ImagePath +filename;
			  	 Log.d("System out", "in download method"+abc);
		            URL url = new URL(abc);
		            URLConnection cn = url.openConnection();
		            cn.connect();
		            InputStream stream = cn.getInputStream();
		            if (stream == null)
		                throw new RuntimeException("stream is null");
		            File temp = File.createTempFile("mediaplayertmp", filename);
		            temp.deleteOnExit();
		            String tempPath = temp.getAbsolutePath();
		            FileOutputStream fos = new FileOutputStream(pictureFile);
		            byte buf[] = new byte[128];
		            int lenghtOfFile = cn.getContentLength();
		            long total = 0;
		            do {
		                int numread = stream.read(buf);
		                if (numread <= 0)
		                    break;
		                total += numread;
						publishProgress(""+(int)((total*100)/lenghtOfFile));
		                fos.write(buf, 0, numread);
		            } while (true);
		            
		            
		            
		            
		         
					
		           
		                stream.close();
		            } catch (IOException ex) {
		                //Log.e(TAG, "error: " + ex.getMessage(), ex);
		           // }
		        }
			return null;

			}
			protected void onProgressUpdate(String... progress) {
				 Log.d("ANDRO_ASYNC",progress[0]);
				 mProgressDialog.setProgress(Integer.parseInt(progress[0]));
			}

			@Override
			protected void onPostExecute(String unused) {
				mProgressDialog.dismiss();
				handler.sendEmptyMessage(1);
			}
		}
	  class Downloadmp4Async extends AsyncTask<String, String, String> {
		   
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				mProgressDialog = new ProgressDialog(activity);
				mProgressDialog.setMessage("Downloading file..");
				mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
			}

			@Override
			protected String doInBackground(String... aurl) {
				int count;
				  try {
			  	String abc = Constants.TimbThumb_ImagePath +filename;
			  	 Log.d("System out", "in download method"+abc);
		            URL url = new URL(abc);
		            URLConnection cn = url.openConnection();
		            cn.connect();
		            InputStream stream = cn.getInputStream();
		            if (stream == null)
		                throw new RuntimeException("stream is null");
		            File temp = File.createTempFile("mediaplayertmp", filename);
		            temp.deleteOnExit();
		            String tempPath = temp.getAbsolutePath();
		            FileOutputStream fos = new FileOutputStream(pictureFile);
		            byte buf[] = new byte[128];
		            int lenghtOfFile = cn.getContentLength();
		            long total = 0;
		            do {
		                int numread = stream.read(buf);
		                if (numread <= 0)
		                    break;
		                total += numread;
						publishProgress(""+(int)((total*100)/lenghtOfFile));
		                fos.write(buf, 0, numread);
		            } while (true);
		          
		                stream.close();
		            } catch (IOException ex) {
		                //Log.e(TAG, "error: " + ex.getMessage(), ex);
		           // }
		        }
			return null;

			}
			protected void onProgressUpdate(String... progress) {
				 Log.d("ANDRO_ASYNC",progress[0]);
				 mProgressDialog.setProgress(Integer.parseInt(progress[0]));
			}

			@Override
			protected void onPostExecute(String unused) {
				mProgressDialog.dismiss();
				handler.sendEmptyMessage(2);
			}
		}
	private void SaveImage(Bitmap finalBitmap) {

		String root = Environment.getExternalStorageDirectory().toString();
		File myDir = new File(root + "/PTO");
		myDir.mkdirs();
		Random generator = new Random();
		int n = 10000;
		n = generator.nextInt(n);
		String fname = "Image-"+ n +".jpg";
		File file = new File (myDir, filename);
		if (file.exists ()) file.delete ();
		try {
			FileOutputStream out = new FileOutputStream(file);
			finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
			out.flush();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}Log.d("System out","in end bitmap dialog");
		handler.sendEmptyMessage(4);
	}
}
