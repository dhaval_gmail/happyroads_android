package com.bpcl.happyroads.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.ExploreActivity;
import com.bpcl.happyroads.Interface.CallMyTrip;
import com.bpcl.happyroads.ListingDetailsFragment;
import com.bpcl.happyroads.MyTripsFragment;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.ForgotPwd;
import com.bpcl.happyroads.Pojo.ImageUpload;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.Signup;
import com.bpcl.happyroads.R;
import com.bpcl.happyroads.RoutePlanningFragment;
import com.bpcl.happyroads.TermsAndConditionsActivity;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.nostra13.universalimageloader.utils.L;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
//updated after git

/**
 * Created by ADMIN on 9/30/2016.
 */
public class LoginModule implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    public Activity context;
    public Context mcontext;
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    public String select="";
    final Calendar now= Calendar.getInstance();
    String date;
    String date1=null;
    TextView tv_start_date_my_trip,tv_return_date_my_trip;
    boolean tripFlag=false;
    Signup signupList=new Signup();
    ArrayList<OTP> otplist=new ArrayList<>();
    ArrayList<ForgotPwd> forgotPwdList=new ArrayList<>();
    ContentLoadingProgressBar listing_content_progressbar;
    DBHandler dbHandler;
    String end_date="",start_date="";
    String start_time="",end_time="";
    Dialog signupDialog;
    Dialog signinDialig;
    RelativeLayout otperror_layout;
    Dialog OTPDialog;
    Dialog resetPwdDialog;
    Boolean backpressed=false;
    GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;
    boolean isemail=false;
    Dialog forgotDialog=null;
    Dialog getNumger=null;
    CallbackManager callbackManager;
    String fname="",lname="",email="",DOB="",fbId="";
    GoogleSignInOptions gso;
     EditText et_email_mobile_forgot_psd;
    EditText et_confirm_pwd_reset_pwd;
    private static int TAKE_PICTURE = 1, SELECT_PICTURE = 0, PIC_CROP = 2;
    String path = "", imgName = "", imgStore = "",
            ResponseString, nameOFimageName = "";
    static String selectedImagePath="";
    File f;
    Bitmap bm;
    static ImageView iv_profile_img;

    CallMyTrip myTrip;
    String from="";
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    String refreshedToken;
     long timeForImgname;
    static EditText et_name_sign_up,et_mobile_sign_up,et_email_sign_up,et_password_sign_up,et_referral_sign_up;
    LinearLayout ll_terms,ll_privacy_policy;
    CheckBox ch_terms;
    boolean ch_terms_flag=false;
    RelativeLayout error_layout;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options;
    public LoginModule(Activity context,String name) {
        this.context=context;
        this.mcontext=context;
      from=name;
        sharedpreferences = context.getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        dbHandler=new DBHandler(context);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
               context).threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()

                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO) // Not
                // necessary
                // in
                // common
                .build();

        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        options = new DisplayImageOptions.Builder()
                /*.showImageForEmptyUri(R.drawable.no_image_available)
                .showImageOnFail(R.drawable.no_image_available)*/
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();

    }





    public void signIn()
    {

        signinDialig=new Dialog(context);
        signinDialig.requestWindowFeature(Window.FEATURE_NO_TITLE);
        signinDialig.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        signinDialig.setContentView(R.layout.signin_popup);
        ImageView iv_fb_signin=(ImageView)signinDialig.findViewById(R.id.iv_fb_signin);
        final  ImageView iv_google_signin=(ImageView)signinDialig.findViewById(R.id.iv_google_signin);
        final EditText et_name_sign_up=(EditText)signinDialig.findViewById(R.id.et_name_sign_up);
        et_name_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.email_mobile)));
        if(et_mobile_sign_up==null)
        {

        }else {
            et_name_sign_up.setText(et_mobile_sign_up.getText().toString().trim());
        }

        final EditText et_mobile_sign_up=(EditText)signinDialig.findViewById(R.id.et_mobile_sign_up);
        et_mobile_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.password)));

        final TextView tv_login_signin=(TextView)signinDialig.findViewById(R.id.tv_login_signin);
        LinearLayout ll_signup_signin=(LinearLayout)signinDialig.findViewById(R.id.ll_signup_signin);
        TextView   tv_forgot_pswd_siginin=(TextView)signinDialig.findViewById(R.id.tv_forgot_pswd_siginin);
        final RelativeLayout error_layout =(RelativeLayout) signinDialig.findViewById(R.id.error_layout);
        final LoginButton loginButton = (LoginButton) signinDialig.findViewById(R.id.login_button);
        final SignInButton signInButton = (SignInButton) signinDialig.findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());

        et_name_sign_up.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("System out","ontextchanged ___________");

                if(charSequence.toString().length()==9) {
                    Log.d("System out","if 9"+ (i1==9));
                    if(TextUtils.isDigitsOnly(charSequence.toString()))
                    {
                        et_name_sign_up.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(10)});
                    }
                    else
                    {

                        et_name_sign_up.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(50)});

                    }


                }
                else
                {
                    Log.d("System out","if not 9"+(i1==9));

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });




        callbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("System out","login success in login button"+loginResult.toString());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        tv_login_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_name_sign_up.getText().toString().trim().equalsIgnoreCase(""))
                {
                    Constants.show_error_popup(context, "Provide valid details.", tv_login_signin);
                }
                else if(et_mobile_sign_up.getText().toString().trim().equalsIgnoreCase(""))
                {
                    Constants.show_error_popup(context, "Provide valid details.", tv_login_signin);
                }
                else {

                    if (IsNetworkConnection.checkNetworkConnection(context)) {
                        boolean test = et_name_sign_up.getText().toString().matches("^[0-9]+$");
                        String json = "";
                        if (test) {
                            json = "[{\"umMobile\":\"" + et_name_sign_up.getText().toString().trim() + "\",\"umEmailId\":\"\",\"umPassword\": \"" + et_mobile_sign_up.getText().toString().trim() + "\",\"umDeviceType\": \"Android\", \"umUDID\": \""+refreshedToken+"\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\"}]";
                        } else {
                            json = "[{\"umMobile\":\"\",\"umEmailId\":\"" + et_name_sign_up.getText().toString().trim() + "\",\"umPassword\": \"" + et_mobile_sign_up.getText().toString().trim() + "\",\"umDeviceType\": \"Android\", \"umUDID\": \""+refreshedToken+"\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\"}]";
                        }
                        Log.d("System out", "In Sign UP  " + json);

                        SignInAPI(json, tv_login_signin);
                    } else {
                        Constants.show_error_popup(context, "" + R.string.internet_error, error_layout);
                    }
                }
                // Mytrip();
            }
        });

        ll_signup_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                signinDialig.dismiss();
                signUp();
            }
        });
        tv_forgot_pswd_siginin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signinDialig.dismiss();


                forgotPswd(et_name_sign_up.getText().toString());
            }
        });
        iv_fb_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)context).fromsignUp=true;
                ((ExploreActivity)context).onactivityfb=true;

                Log.d("System out"," set data in profile tracker "+AccessToken.getCurrentAccessToken());

//testlast1313@gmail.com/

                if(AccessToken.getCurrentAccessToken() == null) {
                    loginButton.performClick();
                }
                else
                {
                    signinDialig.dismiss();
                    // getNumber();
                    checkfb();
                }
                //    signinDialig.dismiss();


            }
        });



        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //((ExploreActivity)context).signIn();
            }
        });

        iv_google_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //signInButton.performClick();
                signinDialig.dismiss();
                ((ExploreActivity)context).fromsignUp=true;

                ((ExploreActivity)context).signIn("true");
                /*int permissionCheck3 = ContextCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS);



                if (permissionCheck3 == android.content.pm.PackageManager.PERMISSION_GRANTED) {
                    backpressed=false;
                    if (IsNetworkConnection.checkNetworkConnection(context)) {
                        try {
                            signinDialig.dismiss();

                            signintogoogle();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                } else {

                    ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.GET_ACCOUNTS}, 123);

                }*/

            }
        });
        ImageView ImgClosepopup=(ImageView)signinDialig.findViewById(R.id.ImgClosepopup);
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signinDialig.dismiss();
            }
        });
        signinDialig.show();
    }
    private void signintogoogle() {
        backpressed=false;
        mIntentInProgress = false;


        try
        {

            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) context)
                    .addOnConnectionFailedListener(this)
                    .addApi(Plus.API)
                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                    .addScope(Plus.SCOPE_PLUS_PROFILE)
                    .build();

            mGoogleApiClient.connect();

            //iv_google_signin.setFocusable(false);
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
    }




    public void signUp()
    {
   signupDialog=new Dialog(context);
        signupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        signupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        signupDialog.setContentView(R.layout.signup_popup);

        ll_terms=(LinearLayout)signupDialog.findViewById(R.id.ll_terms);
        ll_privacy_policy=(LinearLayout)signupDialog.findViewById(R.id.ll_privacy_policy);
        ch_terms=(CheckBox)signupDialog.findViewById(R.id.ch_terms);

        iv_profile_img=(ImageView)signupDialog.findViewById(R.id.iv_profile_img);
        ImageView iv_fb_sign_up=(ImageView)signupDialog.findViewById(R.id.iv_fb_sign_up);
        ImageView iv_google_sign_up=(ImageView)signupDialog.findViewById(R.id.iv_google_sign_up);
         et_name_sign_up=(EditText)signupDialog.findViewById(R.id.et_name_sign_up);
        et_name_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.Name)));
        et_mobile_sign_up=(EditText)signupDialog.findViewById(R.id.et_mobile_sign_up);
        et_mobile_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.Mobile)));
        et_email_sign_up=(EditText)signupDialog.findViewById(R.id.et_email_sign_up);
        et_email_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.Email)));

        et_password_sign_up=(EditText)signupDialog.findViewById(R.id.et_password_sign_up);
        et_password_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.password)));

        et_referral_sign_up=(EditText)signupDialog.findViewById(R.id.et_referral_sign_up);
      //  et_referral_sign_up.setHint(Html.fromHtml(context.getResources().getString(R.string.referral_code)));
        final TextView tv_sign_up=(TextView) signupDialog.findViewById(R.id.tv_sign_up);
        error_layout =(RelativeLayout) signupDialog.findViewById(R.id.error_layout);
        final LoginButton loginButton = (LoginButton) signupDialog.findViewById(R.id.login_signup_button);
        final SignInButton signInButton = (SignInButton) signupDialog.findViewById(R.id.sign_up_button);
        callbackManager = CallbackManager.Factory.create();

        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());

        ll_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  signupDialog.dismiss();
                Bundle b = new Bundle();
                b.putString("from", "Signup TnC");
                Intent i=new Intent(context,TermsAndConditionsActivity.class);
                i.putExtras(b);
                context.startActivity(i);

            }
        });
        ll_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  signupDialog.dismiss();
                Bundle b = new Bundle();
                b.putString("from", "Privacy Policy");
                Intent i=new Intent(context,TermsAndConditionsActivity.class);
                i.putExtras(b);
                context.startActivity(i);

            }
        });
        ch_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(ch_terms_flag == false)
                {
                    ch_terms_flag=true;
                    ch_terms.setButtonDrawable(R.drawable.checkbox_selected_my_account);
                }
                else
                {
                    ch_terms_flag=false;
                    ch_terms.setButtonDrawable(R.drawable.checkbox_unselected_my_account);
                }

            }
        });

        et_password_sign_up.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                Constants.SpacialCharacterNotAllow(et_password_sign_up);
            }
        });
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        callbackManager = CallbackManager.Factory.create();

        iv_profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog();
            }
        });
        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";

                Log.d("System out","In Sign UP++++++++++");
                if(et_name_sign_up.getText().toString().equalsIgnoreCase("") &&et_mobile_sign_up.getText().toString().equalsIgnoreCase("") &&et_email_sign_up.getText().toString().equalsIgnoreCase("")&&et_password_sign_up.getText().toString().equalsIgnoreCase(""))
                {
                    Constants.show_error_popup(context, "Provide valid details.", tv_sign_up);

                }
              else if(et_name_sign_up.getText().toString().equalsIgnoreCase(""))
                {
                    // Toast.makeText(context, "Please provide name", Toast.LENGTH_SHORT).show();
                    Constants.show_error_popup(context, "Provide your name.", tv_sign_up);

                }
                else if(et_mobile_sign_up.getText().toString().equalsIgnoreCase(""))
                {
                    Constants.show_error_popup(context, "Provide 10 digit mobile number.", tv_sign_up);

                } else if(et_mobile_sign_up.getText().toString().length()<10 || et_mobile_sign_up.getText().toString().length()>10)
                {
                    Constants.show_error_popup(context, "Provide 10 digit mobile number.", tv_sign_up);

                }else if(et_email_sign_up.getText().toString().equalsIgnoreCase(""))
                {
                    Constants.show_error_popup(context, "Provide valid email address.", tv_sign_up);


                }else if(!et_email_sign_up.getText().toString().trim().matches(Constants.emailPattern)) {
                    Constants.show_error_popup(context, "Provide valid email address.", tv_sign_up);

                }
                else if(et_password_sign_up.getText().toString().equalsIgnoreCase(""))
                {
                    Constants.show_error_popup(context, "Set your password.", tv_sign_up);


                }else if(ch_terms_flag==false)
                {
                    Constants.show_error_popup(context, "Accept the Terms & Conditions.", tv_sign_up);

                }
              /*  else if (!et_password_sign_up.getText().toString().matches(Constants.PASSWORD_PATTERN)) {
                    Constants.show_error_popup(context, "Please provide valid password.", error_layout);
                }*/
                else {



                        if (IsNetworkConnection.checkNetworkConnection(context)) {
                            Log.d("System out","selected image path in click "+selectedImagePath);
                            if(!selectedImagePath.equalsIgnoreCase(""))
                            {
                                ImageUpload(tv_sign_up);
                            }
                            else {
                                String json = "[{\"umMobile\": \"" + et_mobile_sign_up.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString().trim() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"" + refreshedToken + "\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";
                                Log.d("System out", "In Sign UP  " + json);

                                SignUpAPI(json, tv_sign_up);
                            }

                        } else {
                            Constants.show_error_popup(context, "" + R.string.internet_error, error_layout);
                        }


                }



            }
        });
        iv_fb_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)context).fromsignUp=true;
                ((ExploreActivity)context).onactivityfb=true;

                signupDialog.dismiss();
                if(AccessToken.getCurrentAccessToken() == null) {
                    loginButton.performClick();
                }
                else
                {

                    // getNumber();
                    checkfb();
                }

            }
        });
        iv_google_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupDialog.dismiss();
                ((ExploreActivity)context).fromsignUp=true;
                ((ExploreActivity)context).signIn("true");


            }
        });


        ImageView ImgClosepopup=(ImageView)signupDialog.findViewById(R.id.ImgClosepopup);
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupDialog.dismiss();


            }
        });
        signupDialog.show();


    }


    public void forgotPswd(String text)
    {
        forgotDialog=new Dialog(context);

        forgotDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgotDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        forgotDialog.setContentView(R.layout.forgot_password_popup);
        final EditText et_email_mobile_forgot_psd=(EditText)forgotDialog.findViewById(R.id.et_email_mobile_forgot_psd);
        final TextView tv_submit_forgot_pswd=(TextView)forgotDialog.findViewById(R.id.tv_submit_forgot_pswd);
        final RelativeLayout error_layout =(RelativeLayout) forgotDialog.findViewById(R.id.error_layout);
        final ImageView ImgClosepopup=(ImageView)forgotDialog.findViewById(R.id.ImgClosepopup);

        et_email_mobile_forgot_psd.setText(text);

        et_email_mobile_forgot_psd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("System out","ontextchanged ___________");

                if(charSequence.toString().length()==9) {
                    Log.d("System out","if 9"+ (i1==9));
                    if(TextUtils.isDigitsOnly(charSequence.toString()))
                    {
                        et_email_mobile_forgot_psd.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(10)});
                    }
                    else
                    {

                        et_email_mobile_forgot_psd.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(50)});

                    }


                }
                else
                {
                    Log.d("System out","if not 9"+(i1==9));

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        tv_submit_forgot_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System out","digit check,"+TextUtils.isDigitsOnly(et_email_mobile_forgot_psd.getText()));
                Log.d("System out","email check"+et_email_mobile_forgot_psd.getText().toString().trim().contains("@"));
                if (TextUtils.isDigitsOnly(et_email_mobile_forgot_psd.getText().toString()) ) {
                    Log.d("System out","TextUtils.isDigitsOnly(et_email_mobile_forgot_psd.getText()");
                    isemail = false;
                } else if (et_email_mobile_forgot_psd.getText().toString().trim().contains("@")) {
                    Log.d("System out","et_email_mobile_forgot_psd.getText().toString().trim().contains(\"@\")");

                    isemail = true;
                }else
                {
                    Log.d("System out","else");

                    Constants.show_error_popup(context, "Provide valid details.", tv_submit_forgot_pswd);

                }

                if (TextUtils.isEmpty(et_email_mobile_forgot_psd.getText().toString()))
                {
                    Log.d("System out","else et_email_mobile_forgot_psd.getText().toString()");

                    Constants.show_error_popup(context, "Provide valid details. ", tv_submit_forgot_pswd);
                }

                else if (isemail==true && !Constants.checkEmail(et_email_mobile_forgot_psd.getText().toString().trim())) {
                    Constants.show_error_popup(context,"Provide valid details.",tv_submit_forgot_pswd);
                } else if (isemail==false && !et_email_mobile_forgot_psd.getText().toString().trim().matches(Constants.MobilePattern)) {
                    Constants.show_error_popup(context,"Provide valid details.",tv_submit_forgot_pswd);

                } else if (isemail==false && (et_email_mobile_forgot_psd.getText().toString().trim().length()<1 && et_email_mobile_forgot_psd.getText().toString().trim().length()>12)) {
                    Constants.show_error_popup(context,"Provide valid details. ",tv_submit_forgot_pswd);

                }
                else
                {
                    if (IsNetworkConnection.checkNetworkConnection(context)) {
                        String json = "[{\"umMobile\": \"" + et_email_mobile_forgot_psd.getText().toString().trim() + "\"}]";
                        Log.d("System out", "In Sign UP  " + json);

                        ForgotPwdAPI(json, (ImageView)ImgClosepopup,isemail);

                    }
                    else
                    {
                        Constants.show_error_popup(context, ""+R.string.internet_error, error_layout);
                    }

                }
            }
        });

        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotDialog.dismiss();
            }
        });
        forgotDialog.show();

    }


    public void Mytrip()
    {
        final Dialog myTripDialog=new Dialog(context);
        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_my_trips);
        tv_start_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_start_date_my_trip);
        tv_return_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_return_date_my_trip);
        final TextView tv_for_better_dates=(TextView)myTripDialog.findViewById(R.id.tv_for_better_dates);
        final ImageView iv_swt_off_my_trip=(ImageView)myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
        final LinearLayout ll_start_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        final EditText et_trip_name=(EditText)myTripDialog.findViewById(R.id.et_trip_name);
        TextView tv_save_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_save_my_trip);
        dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),true
        );



        iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tripFlag==false)
                {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                    tv_for_better_dates.setVisibility(View.GONE);
                    ll_start_date_my_trip.setAlpha(1);
                    ll_return_date_my_trip.setAlpha(1);
                    tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tripFlag==true){
                                select = "start";
                                start_date = "";
                                start_time = "";
                                dpd.show(context.getFragmentManager(), "Datepickerdialog");
                            }


                        }

                    });

                    tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tripFlag==true) {
                                select = "stop";
                                end_date = "";
                                end_time = "";
                                dpd.show(context.getFragmentManager(), "Datepickerdialog");
                                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                            }



                        }
                    });


                    tripFlag=true;
                }
                else
                {

                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                    tv_start_date_my_trip.setText("Tap to set date &amp; time");
                    tv_return_date_my_trip.setText("Tap to set date &amp; time");
                    tv_for_better_dates.setVisibility(View.VISIBLE);
                    ll_start_date_my_trip.setAlpha((float) 0.6);
                    ll_return_date_my_trip.setAlpha((float) 0.6);
                    tripFlag=false;
                }

            }
        });

        tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_trip_name.getText().toString().length()>0) {

                    dbHandler.deleteAllTrip();
                    Create_Trip create_trip = new Create_Trip();
                    create_trip.setSource("Ahemdabad");
                    create_trip.setDestination("Ghandhinagar");
                    create_trip.setStartdate(start_date);
                    create_trip.setStarttime(start_time);
                    create_trip.setEnddate(end_date);
                    create_trip.setEndtime(end_time);
                    create_trip.setTripname(et_trip_name.getText().toString());
                    create_trip.setSource_latitude(23.022505);
                    create_trip.setSource_longitude(72.571362);
                    create_trip.setDestination_latitude(23.215635);
                    create_trip.setDestination_longitude(72.636941);
                    create_trip.setKM(35.00);
                    create_trip.setTime(0.60);

                    dbHandler.add_Trip_into_Table(create_trip);
                }
                myTripDialog.dismiss();


                ((ExploreActivity)context).replace_fragmnet(new RoutePlanningFragment());
            }
        });
        ImageView ImgClosepopup=(ImageView)myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //  SimpleDateFormat formatter = new SimpleDateFormat("dd MMM ,yyyy");
        date =dayOfMonth+" "+(++monthOfYear)+","+year;

        //  dateTextView.setText(date);


        // formatter.format(date);
        tpd.show(context.getFragmentManager(),"TimePickerDialog");




    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        // tv_start_date_my_trip.setText();
        //  String date = "You picked the following date: "+dayOfMonth+"/"+(++monthOfYear)+"/"+year;

        String time = hourOfDay+":"+minute+"";

        try {
            date1= Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","dd MMM, yyyy hh:mm a");
            if(select.equalsIgnoreCase("start")) {
                start_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                start_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");
                tv_start_date_my_trip.setText(date1);
            }
            else if(select.equalsIgnoreCase("stop")) {
                end_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                end_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");
                tv_return_date_my_trip.setText(date1);
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        // tv_return_date_my_trip.setText(date1);



        // tv_return_date_my_trip.setText(date1);

    }
    private void SignUpAPI(final String json,final TextView tv_sign_up) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
       /* if(listing_content_progressbar!=null) {
            listing_content_progressbar.setVisibility(View.VISIBLE);
        } */       //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<Signup> call = apiService.signup(json);
        call.enqueue(new Callback<Signup>() {
            @Override
            public void onResponse(Call<Signup> call, Response<Signup> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    signupList= response.body();
                    if (signupList.getResStatus() == true)
                    {//call OTP API here
                        signupDialog.dismiss();

                        Constants.show_error_popup(context, ""+signupList.getResDescription(), tv_sign_up);

                        et_mobile_sign_up.setText("");
                        OTP("signup");

                    }
                    else
                    {
                        if(signupList.getResDescription().contains("Hi! You are a registered user of Happy Roads. Kindly login."))
                        {

                            Constants.show_error_popup(context, ""+signupList.getResDescription(), tv_sign_up);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1500);
                                        ((Activity) context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                signupDialog.dismiss();
                                                signIn();


                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.w("Exception in splash", e);
                                    }

                                }
                            }).start();


                        }
                        else {
                            et_mobile_sign_up.setText("");
                            Constants.show_error_popup(context, "" + signupList.getResDescription(), tv_sign_up);
                        }
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }else
                {
                    pb_dialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<Signup> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }

    private void ForgotPwdAPI(final String json, final ImageView error_layout, final Boolean isemail) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if(listing_content_progressbar!=null) {
            listing_content_progressbar.setVisibility(View.VISIBLE);
        }        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<ForgotPwd>> call = apiService.forgotPwd(json);
        call.enqueue(new Callback<ArrayList<ForgotPwd>>() {
            @Override
            public void onResponse(Call<ArrayList<ForgotPwd>> call, Response<ArrayList<ForgotPwd>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                   /* forgotPwdList.clear();
                    forgotPwdList.add(response.body());*/
                   // Constants.show_error_popup(context, ""+response.body().get(0).getResDescription(), error_layout);

                    if (response.body().get(0).getResStatus() == true)
                    {//call OTP API here

                        forgotDialog.dismiss();
                        forgotPwdList=response.body();
                        Log.d("System out","number in forgot___"+forgotPwdList.get(0).getUmMobile());
                        final Dialog resetpwdDialog=new Dialog(context);
                        resetpwdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        resetpwdDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        resetpwdDialog.setContentView(R.layout.reset_pswd_link_layout);
                        final TextView tv_alert_msg = (TextView) resetpwdDialog.findViewById(R.id.tv_alert_msg);
                                               /*    if(response.body().get(0).getUmMobile().equalsIgnoreCase("")) {
                                tv_alert_msg.setText("Link to reset your password has \n been sent to your email");
                            }
                            else
                            {
                                tv_alert_msg.setText("Link to reset your password has \n been sent to your mobile number");

                        }*/
                       /* if(no_or_email) {
                            tv_alert_msg.setText("Link to reset your password has \n been sent to your mobile number");

                        }
                        else
                        {
                            tv_alert_msg.setText("Link to reset your password has \n been sent to your email");

                        }*/
                        resetpwdDialog.show();
                        if (isemail==true) {
                            tv_alert_msg.setText("Link to reset your password has \n been sent to your email");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1500);
                                        ((Activity) context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                resetpwdDialog.dismiss();
                                                //Code for the UiThread
                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.w("Exception in splash", e);
                                    }

                                }
                            }).start();


                        }
                        else
                        {
                            tv_alert_msg.setText("Link to reset your password has \n been sent to your mobile number");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1500);
                                        ((Activity) context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                resetpwdDialog.dismiss();
                                                OTP("forgotpwsd");
                                                //Code for the UiThread
                                            }
                                        });
                                    /*runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            resetpwdDialog.dismiss();
                                            OTP("forgotpwsd");
                                        }
                                    });*/
                                    } catch (Exception e) {
                                        Log.w("Exception in splash", e);
                                    }

                                }
                            }).start();
                        }
                        // tv_alert_msg.setText(response.body().getResDescription());




                    }
                    else
                    {
                       Constants.show_error_popup(context, ""+response.body().get(0).getResDescription(), error_layout);
                       // Toast.makeText(context,""+response.body().get(0).getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<ForgotPwd>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }
    private void SignInAPI(final String json,final TextView error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if(listing_content_progressbar!=null) {
            listing_content_progressbar.setVisibility(View.VISIBLE);
        }
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<OTP>> call = apiService.signIn(json);
        Log.d("System out","sign in api call___"+json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    otplist.clear();
                    otplist.addAll(response.body());
                    if (otplist.get(0).getResStatus() == true)
                    {//call OTP API here
                        signinDialig.dismiss();
                        ((ExploreActivity)context).LogoutDisplay();
                        Constants.show_error_popup_success(context, ""+otplist.get(0).getResDescription(), error_layout);
                        SessionManager sessionManager = new SessionManager(context);
                        String json =Constants.convert_object_string(otplist.get(0));
                        sessionManager.create_login_session(json,otplist.get(0).getUmEmailId(),otplist.get(0).getUmPassword());
                        Log.d("Systm out","set image in login module "+Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100);
                        ((ExploreActivity)context).setProfilePicture(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100);
                        // ListingDetailsFragment.Mytrip(context);
                        ((ExploreActivity)context).tv_login_logout.setText("Logout");
                        if(from.equalsIgnoreCase("ListingDetailsFragment")) {
                            myTrip = new ListingDetailsFragment();
                            myTrip.CallTrip(context);
                        }
                        else if(from.equalsIgnoreCase("MyTripsFragment"))

                        {
                           /* myTrip = new MyTripsFragment();
                           myTrip.CallTrip(context);*/
                            ((ExploreActivity)context).replace_fragmnet(new MyTripsFragment());
                        }



                    }
                    else if(otplist.get(0).getResDescription().contains("Pl confirm confirmation code."))
                    {
                        signinDialig.dismiss();
                        OTP("signin");
                    }
                    else
                    {
                        Constants.show_error_popup(context, ""+otplist.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }

    private void OTP(final String from) {


        OTPDialog=new Dialog(context);
        OTPDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        OTPDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        OTPDialog.setContentView(R.layout.otp);
        final  EditText et_mobile_otp=(EditText)OTPDialog.findViewById(R.id.et_mobile_otp);
        TextView tv_submit_otp=(TextView)OTPDialog.findViewById(R.id.tv_submit_otp);
        final RelativeLayout error_layout =(RelativeLayout) OTPDialog.findViewById(R.id.error_layout);
        TextView tv_resend=(TextView)OTPDialog.findViewById(R.id.tv_resend);
        OTPDialog.show();
        //  String json = "[{\"umMobile\": \"" + et_mobile_otp.getText().toString() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (IsNetworkConnection.checkNetworkConnection(context)) {
                    String json;
                    if(from.equalsIgnoreCase("signup")) {
                         json = "[{\"umMobile\": \"" + signupList.getUmMobile() + "\" }]";
                    }else if(from.equalsIgnoreCase("forgotpwsd"))
                    {
                        json = "[{\"umMobile\": \"" + forgotPwdList.get(0).getUmMobile() + "\" }]";
                        Log.d("forgot pswd",json);

                    }
                    else
                    {
                        json = "[{\"umMobile\": \"" + otplist.get(0).getUmMobile() + "\" }]";
                    }
                    Log.d("System out", "In Resend OTP  " + json);

                    ResendOtpAPI(json,error_layout);
                }
                else
                {
                    Constants.show_error_popup(context, ""+R.string.internet_error, error_layout);
                }
            }
        });

        tv_submit_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String abc="";
                if(et_mobile_otp.getText().toString().equalsIgnoreCase(""))
                {

                    Constants.show_error_popup(context, "Provide OTP.", error_layout);

                }else {

                  /*  try {
                        byte[] data = signupList.getUmPassword().getBytes("UTF-8");
                        byte[] data1 = Base64.decode(data, Base64.DEFAULT);
                        abc = new String(data1, "UTF-8");
                        Log.d("System out", "Decode password " + (abc));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    if (IsNetworkConnection.checkNetworkConnection(context)) {
                        String json;
                        if(from.equalsIgnoreCase("signup")) {

                             json = "[{\"umId\": \"" + signupList.getUmId() + "\",\"umOTP\": \"" + et_mobile_otp.getText().toString() + "\",\"umPassword\":\"" + signupList.getUmPassword() +
                                    "\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\"  }]";
                        }else if(from.equalsIgnoreCase("forgotpwsd"))
                        {
                            json = "[{\"umId\": \"" + forgotPwdList.get(0).getUmId() + "\",\"umOTP\": \"" + et_mobile_otp.getText().toString() + "\",\"umPassword\":\"" + forgotPwdList.get(0).getUmPassword() +
                                    "\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\"  }]";
                        }
                        else
                        {
                             json = "[{\"umId\": \"" + otplist.get(0).getUmId() + "\",\"umOTP\": \"" + et_mobile_otp.getText().toString() + "\",\"umPassword\":\"" + otplist.get(0).getUmPassword() +
                                    "\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\"  }]";
                        }
                        Log.d("System out", "In OTP  " + json);
                        OtpAPI(json,error_layout,from);
                    }
                    else
                    {
                        Constants.show_error_popup(context, ""+R.string.internet_error, error_layout);
                    }
                }

            }
        });
        ImageView ImgClosepopup=(ImageView)OTPDialog.findViewById(R.id.ImgClosepopup);
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTPDialog.dismiss();
            }
        });





    }
    private void OtpAPI(String json, final RelativeLayout error_layout, final String fromwhich) {
        // get & set progressbar dialog

        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if(listing_content_progressbar!=null) {
            listing_content_progressbar.setVisibility(View.VISIBLE);
        }        Call<ArrayList<OTP>> call = apiService.otp(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {
                    pb_dialog.dismiss();

                            otplist.clear();
                    otplist.addAll(response.body());
                    if (otplist.get(0).getResStatus() == true)
                    {//call OTP API here
                        OTPDialog.dismiss();
                        ((ExploreActivity)context).LogoutDisplay();
                        Constants.show_error_popup(context, ""+otplist.get(0).getResDescription(), error_layout);
                        SessionManager sessionManager = new SessionManager(context);
                        String json =Constants.convert_object_string(otplist.get(0));
                        sessionManager.create_login_session(json,otplist.get(0).getUmEmailId(),otplist.get(0).getUmPassword());

                        imageLoader.displayImage(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100, ((ExploreActivity)context).iv_profile_img, options, new SimpleImageLoadingListener()            {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                            }
                        });
                      //  ((ExploreActivity)context).iv_profile_img.setImageURI(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100);
                        if(from.equalsIgnoreCase("ListingDetailsFragment")) {
                            myTrip = new ListingDetailsFragment();
                            myTrip.CallTrip(context);
                        }

                        else if(from.equalsIgnoreCase("MyTripsFragment"))

                        {
                            ((ExploreActivity)context).replace_fragmnet(new MyTripsFragment());
                        }else if(fromwhich.equalsIgnoreCase("signup"))
                        {
                            OTPDialog.dismiss();
                            Toast.makeText(context,""+otplist.get(0).getResDescription(),Toast.LENGTH_SHORT).show();
                        }else if(fromwhich.equalsIgnoreCase("signin"))
                        {
                            OTPDialog.dismiss();
                        }
                        else
                        {

                           // Constants.show_error_popup(context, "success", error_layout);
                            resetPwdDialog=new Dialog(context);
                            resetPwdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            resetPwdDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            resetPwdDialog.setContentView(R.layout.reset_pwd);



                            final RelativeLayout error_layout =(RelativeLayout) resetPwdDialog.findViewById(R.id.error_layout);
                            TextView tv_save_reset_pwd=(TextView)resetPwdDialog.findViewById(R.id.tv_save_reset_pwd);
                            EditText et_new_pwd_reset_pwd=(EditText) resetPwdDialog.findViewById(R.id.et_new_pwd_reset_pwd);
                             et_confirm_pwd_reset_pwd=(EditText) resetPwdDialog.findViewById(R.id.et_confirm_pwd_reset_pwd);
                            tv_save_reset_pwd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    resetpwdAPI(error_layout);
                                    resetPwdDialog.dismiss();

                                }
                            });
                            ImageView ImgClosepopup=(ImageView)resetPwdDialog.findViewById(R.id.ImgClosepopup);
                            ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    resetPwdDialog.dismiss();
                                }
                            });
                            resetPwdDialog.show();


                        }

                    }
                    else
                    {
                        Constants.show_error_popup(context, ""+otplist.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            private void resetpwdAPI(final RelativeLayout error_layout) {
                // get & set progressbar dialog
                final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
                pb_dialog.setCancelable(false);
                pb_dialog.show();

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


                String json = "[{\"umId\":\"" + forgotPwdList.get(0).getUmId() + "\",\"umPassword\":\"" + et_confirm_pwd_reset_pwd.getText().toString() + "\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\",\"umOTP\":\"Success\"}]";
                Call<ArrayList<Signup>> call = apiService.updatePassword(json);
                call.enqueue(new Callback<ArrayList<Signup>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Signup>> call, Response<ArrayList<Signup>> response) {
                        if (response.body() != null) {

                            pb_dialog.dismiss();

                            if (response.body().get(0).getResStatus() == true) {//call OTP API here

                                SessionManager sessionManager=new SessionManager(context);
                                sessionManager.set_password(et_confirm_pwd_reset_pwd.getText().toString());
                                Constants.show_error_popup(context, "" + response.body().get(0).getResDescription(), error_layout);
                              //  ((ExploreActivity) getActivity()).onBackPressed();
                            } else {
                                Constants.show_error_popup(context, "" + response.body().get(0).getResDescription(), error_layout);

                            }


                        }


                    }

                    @Override
                    public void onFailure(Call<ArrayList<Signup>> call, Throwable t) {
                        // Log error here since request failed

                        t.printStackTrace();


                    }
                });
            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }




    public void checkfb()
    {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {

                        try {

                            Log.d("System out"," ---- "+response.getJSONObject().getString("name"));
                            Log.d("System out"," ---- "+response.getJSONObject().getString("id"));

                            Log.d("System out"," ---- "+response.getJSONObject().getString("email"));

                            fname=response.getJSONObject().getString("name");
                            fbId=response.getJSONObject().getString("id");
                            email=response.getJSONObject().getString("email");
                            if (IsNetworkConnection.checkNetworkConnection(context)) {
                                String json = "[{\"umMobile\":\"\",\"umEmailId\":\""+email+"\",\"umPassword\":\"\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\",\"umFacebookToken\":\""+fbId+"\",\"umGoogleToken\":\""+fbId+"\" }]";
                                Log.d("System out", "In Sign UP facebook " + json);

                                CheckFacebbok(json);

                            } else {

                                Toast.makeText(context,""+ R.string.internet_error,Toast.LENGTH_SHORT).show();
                            }



                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday");
        request.setParameters(parameters);
        request.executeAsync();








    }

    public void checkGpluse(GoogleSignInAccount acct)
    {


        if (IsNetworkConnection.checkNetworkConnection(context)) {
            String json = "[{\"umMobile\":\"\",\"umEmailId\":\""+acct.getEmail()+"\",\"umPassword\":\"\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\",\"umFacebookToken\":\""+acct.getId()+"\",\"umGoogleToken\":\""+acct.getId()+"\" }]";
            Log.d("System out", "In Sign UP facebook " + json);
            email=acct.getEmail();
            fname=acct.getDisplayName();
            CheckFacebbok(json);

        } else {

            Toast.makeText(context,""+ R.string.internet_error,Toast.LENGTH_SHORT).show();
        }





    }



    public void getNumber()
    {

        getNumger=new Dialog(context);

        getNumger.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getNumger.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getNumger.setContentView(R.layout.forgot_password_popup);
        TextView tv_upper=(TextView)getNumger.findViewById(R.id.tv_upper);
      //  final EditText et_email_mobile_forgot_psd=(EditText)getNumger.findViewById(R.id.et_email_mobile_forgot_psd);
        et_email_mobile_forgot_psd=(EditText)getNumger.findViewById(R.id.et_email_mobile_forgot_psd);
        TextView tv_submit_forgot_pswd=(TextView)getNumger.findViewById(R.id.tv_submit_forgot_pswd);
        final RelativeLayout error_layout =(RelativeLayout) getNumger.findViewById(R.id.error_layout);
        tv_upper.setText("Add Number");
        et_email_mobile_forgot_psd.setHint("Add Number");
        et_email_mobile_forgot_psd.setInputType(InputType.TYPE_CLASS_NUMBER);





        tv_submit_forgot_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_email_mobile_forgot_psd.getText().toString()))
                {
                    Constants.show_error_popup(context, "Provide 10 digit mobile number.", error_layout);
                }
                else if (!et_email_mobile_forgot_psd.getText().toString().trim().matches(Constants.MobilePattern)) {
                    Constants.show_error_popup(context,"Provide 10 digit mobile number.",error_layout);

                } else if ((et_email_mobile_forgot_psd.getText().toString().trim().length()<1 && et_email_mobile_forgot_psd.getText().toString().trim().length()>12)) {
                    Constants.show_error_popup(context,"Provide 10 digit mobile number.",error_layout);

                }
                else {

                    if (IsNetworkConnection.checkNetworkConnection(context)) {
                        String json = "[{\"umMobile\":\"" + et_email_mobile_forgot_psd.getText().toString() + "\",\"umFirstName\":\"" + fname.trim() + "\",\"umLastName\":\"\",\"umEmailId\":\"" + email + "\",\"umPassword\":\""+"Test@123"+"\",\"umDeviceType\":\"Android\",\"umUDID\":\"\",\"umProfilePhoto\":\"\",\"umDOB\":\"\",\"umDescription\":\"\",\"umLocation\": \"\",\"umVehicleNumber\":\"\",\"umLat\":\"123\",\"umLong\":\"123\",\"umIsPushOn\":true,\"umTitle\":\"\",\"umPetroCard\":\"\",\"umReferralCode\":\"\",\"umGoogleToken\":\"\",\"umFacebookToken\":\"" + fbId + "\",\"umEmrgncyName\":\"\",\"umEmrgncyNumber\":\"\"}]";
                        Log.d("System out", "In Sign UP facebook " + json);

                        SignUpAPIFacebbok(json, error_layout);

                    } else {
                        Constants.show_error_popup(context, "" + R.string.internet_error, error_layout);
                    }
                }


            }
        });
        ImageView ImgClosepopup=(ImageView)getNumger.findViewById(R.id.ImgClosepopup);
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNumger.dismiss();
            }
        });
        getNumger.show();

    }

    private void SignUpAPIFacebbok(final String json,final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if(listing_content_progressbar!=null) {
            listing_content_progressbar.setVisibility(View.VISIBLE);
        }        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<Signup> call = apiService.AccessViaGoogleOrFB(json);
        call.enqueue(new Callback<Signup>() {
            @Override
            public void onResponse(Call<Signup> call, Response<Signup> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    signupList= response.body();
                    if (signupList.getResStatus() == true)
                    {//call OTP API here
                        getNumger.dismiss();


                        Constants.show_error_popup(context, ""+signupList.getResDescription(), error_layout);


                        OTP("signup");

                    }
                    else if(signupList.getResDescription().contains("Pl confirm confirmation code."))
                    {
                        getNumger.dismiss();
                        OTP("signup");
                    }
                    else
                    {
                        Constants.show_error_popup(context, ""+signupList.getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<Signup> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }
    private void CheckFacebbok(final String json) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        if(listing_content_progressbar!=null) {
            listing_content_progressbar.setVisibility(View.VISIBLE);
        }        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<OTP>> call = apiService.GetUser(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    otplist.clear();
                    otplist= response.body();
                    if (otplist.get(0).getResStatus() == true)
                    {//call ArrayList<OTP> API here


                        // Constants.show_error_popup(context, ""+signupList.getResDescription(), error_layout);

                        SessionManager sessionManager = new SessionManager(context);
                        String json =Constants.convert_object_string(otplist.get(0));
                        sessionManager.create_login_session(json,otplist.get(0).getUmEmailId(),otplist.get(0).getUmPassword());
                        //((ExploreActivity)context).iv_profile_img.setImageURI(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100);
                        imageLoader.displayImage(Constants.TimbThumb_ImagePath + otplist.get(0).getUmProfilePhoto()+"&width="+100, ((ExploreActivity)context).iv_profile_img, options, new SimpleImageLoadingListener()            {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                            }
                        });
                        ((ExploreActivity)context).tv_login_logout.setText("Logout");
                        if(((ExploreActivity)context).fromsignUp==false) {
                            Toast.makeText(context,""+otplist.get(0).getResDescription(),Toast.LENGTH_SHORT).show();
                            if (from.equalsIgnoreCase("ListingDetailsFragment")) {
                                myTrip = new ListingDetailsFragment();
                                myTrip.CallTrip(context);
                            } else if (from.equalsIgnoreCase("MyTripsFragment"))

                            {
                           /* myTrip = new MyTripsFragment();
                            myTrip.CallTrip(context);*/
                                ((ExploreActivity) context).replace_fragmnet(new MyTripsFragment());
                            }
                        }
                        else
                        {
                            signinDialig.dismiss();

                            LayoutInflater li = context.getLayoutInflater();
                            //Getting the View object as defined in the customtoast.xml file
                            View layout = li.inflate(R.layout.customtoast,
                                    (ViewGroup)context.findViewById(R.id.custom_toast_layout));
                            TextView textView=(TextView)layout.findViewById(R.id.custom_toast_message) ;
                            //Creating the Toast object
                            Toast toast = new Toast(context);
                            toast.setDuration(Toast.LENGTH_SHORT);
                           //  toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.setView(layout);//setting the view of custom toast layout
                            textView.setText(otplist.get(0).getResDescription().toString());
                            toast.show();
                            //Toast.makeText(context,""+otplist.get(0).getResDescription(),Toast.LENGTH_SHORT).show();
                         //   Toast.makeText(context,"User is already registered..",Toast.LENGTH_SHORT).show();
                        }

                        // getNumber();;

                        // OTP();

                    }


                    else {

                        if (((ExploreActivity)context).fromsignUp == true) {
                            getNumber();
                        } else
                        {
                            if (otplist.get(0).getResDescription().contains("User Not Found")) {
                            Toast.makeText(context, "" + otplist.get(0).getResDescription(), Toast.LENGTH_SHORT).show();
                        }else  if (otplist.get(0).getResDescription().contains("Pl confirm confirmation code.") && !otplist.get(0).getUmMobile().equalsIgnoreCase("")) {
                             //   Toast.makeText(context, "" + otplist.get(0).getResDescription(), Toast.LENGTH_SHORT).show();
                                OTP("signin");
                            }else  if (otplist.get(0).getResDescription().contains("Pl confirm confirmation code.")) {
                             //   Toast.makeText(context, "" + otplist.get(0).getResDescription(), Toast.LENGTH_SHORT).show();
                                getNumber();
                            }
                    }

                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }

    private void ResendOtpAPI(String json, final RelativeLayout error_layout) {
        // get & set progressbar dialog

        final Dialog pb_dialog = Constants.get_dialog(context, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<OTP>> call = apiService.ResendConfirmationCode(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {
                    pb_dialog.dismiss();
                    otplist.addAll(response.body());
                    if (otplist.get(0).getResStatus() == true)
                    {//call OTP API here

                        Constants.show_error_popup(context, ""+otplist.get(0).getResDescription(), error_layout);
                    }
                    else
                    {
                        Constants.show_error_popup(context, ""+otplist.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }
    public void dialog() {

        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_gallery);

        ImageView gal = (ImageView) dialog.findViewById(R.id.gal);
        ImageView camera = (ImageView) dialog.findViewById(R.id.camera);
        ImageView closebtn = (ImageView) dialog.findViewById(R.id.closebtn);



        closebtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // SignUpActivity.this.finish();
            }
        });

        gal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");

                //intent.setAction(Intent.ACTION_GET_CONTENT);
                //Toast.makeText(AddPost.this, "Select gallery option", 3000).show();
                Log.d("System out", "Select gallery option");
                context.startActivityForResult(
                        Intent.createChooser(intent, "Select Picture"),
                        SELECT_PICTURE);

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //Toast.makeText(AddPost.this, "Select camera option", 3000).show();
                Log.d("System out", "Select camera option");
                timeForImgname = System.currentTimeMillis();
                imgName ="img_" + timeForImgname
                        + ".jpg";

                imgStore = imgName;
                //Toast.makeText(AddPost.this, "Image name which capture from camera"+imgName, 3000).show();
                Log.d("System out", "Image name which capture from camera" + imgName);
                f = new File(android.os.Environment.getExternalStorageDirectory(),
                        imgName);

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                Constants.file=f;
                context.startActivityForResult(cameraIntent, TAKE_PICTURE);
            }
        });

        dialog.show();
    }

    public void setprofilepic(Uri uri,String imagepath)
    {
        selectedImagePath=imagepath;
        try {
            ContentResolver cr = context.getContentResolver();
            bm = MediaStore.Images.Media.getBitmap(cr,
                    uri);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.d("System out","selected image path "+selectedImagePath);
        iv_profile_img.setImageBitmap(bm);

        //new RetrieveFeedTasknew().execute();

    }


    public void ImageUpload(final TextView tv_sign_up)
    {
        timeForImgname = System.currentTimeMillis();
        imgName ="img_" + timeForImgname
                + ".jpg";
        File file = new File(selectedImagePath);
        okhttp3.RequestBody reqFile = okhttp3.RequestBody.create(okhttp3.MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("upload", imgName, reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<ImageUpload>> req = apiService.postImage(body);
        req.enqueue(new Callback<ArrayList<ImageUpload>>() {
            @Override
            public void onResponse(Call<ArrayList<ImageUpload>> call, Response<ArrayList<ImageUpload>> response) {
                // Do Something
                Log.d("System out","Response of image uplod "+response.body().toString());
                Log.d("System out","Response of image uplod "+response.body().get(0).getMessage());
                if(response.body().get(0).getStatus() == true)
                {
                    if (IsNetworkConnection.checkNetworkConnection(context)) {

                        String json = "[{\"umMobile\": \"" + et_mobile_sign_up.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString().trim() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"" + refreshedToken + "\",\"umProfilePhoto\": \""+response.body().get(0).getMessage()+"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";
                        Log.d("System out", "In Sign UP  " + json);

                        SignUpAPI(json, tv_sign_up);


                    } else {
                        Constants.show_error_popup(context, "" + R.string.internet_error, tv_sign_up);
                    }
                }
                else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<ImageUpload>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}


