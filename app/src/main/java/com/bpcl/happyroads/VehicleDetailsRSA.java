package com.bpcl.happyroads;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ADMIN on 09/12/2016.
 */
public class VehicleDetailsRSA extends Fragment {
    TextView tv_vehicle_reg_no, tv_year, tv_make, tv_color, tv_submit_vehicle;
    GetVehicleDetails vehicledetails;
    SubsciptionPlans subsciptionPlansArrayList;
    ImageView iv_close_vehicle_details;

    Bundle bundle;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.vehicle_details_rsa, container, false);

        bundle = this.getArguments();
        tv_submit_vehicle = (TextView)v. findViewById(R.id.tv_submit_vehicle);
        tv_color = (TextView) v.findViewById(R.id.tv_color);
        tv_make = (TextView) v.findViewById(R.id.tv_make);
        tv_year = (TextView) v.findViewById(R.id.tv_year);
        tv_vehicle_reg_no = (TextView) v.findViewById(R.id.tv_vehicle_reg_no);
        iv_close_vehicle_details=(ImageView)v.findViewById(R.id.iv_close_vehicle_details);
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.GONE);
        if (bundle != null) {

            vehicledetails = (GetVehicleDetails)bundle.getSerializable("vehicledetails");
            subsciptionPlansArrayList = (SubsciptionPlans) bundle.getSerializable("packagedetails");
            setdata();

        }

        Date datecurrent = null;
        Date dateExpiry = null;

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
        Calendar calendar = Calendar.getInstance();

        String temp1="";
        String eDate="";
        temp1=sdf1.format(calendar.getTime());
        // eDate=getVehicleList.get(i).getSubscriptionExpiry().replace("T"," ");


        try {
            datecurrent = sdf1.parse(temp1);
            eDate= Constants.formatDate(vehicledetails.getSubscriptionExpiry().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy hh:mm a");

            dateExpiry=sdf1.parse(eDate);

            Log.d("System out","eDate___"+eDate);
            Log.d("System out","current date__"+datecurrent);
            Log.d("System out","Expiry date__"+dateExpiry+"  "+datecurrent.after(dateExpiry));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        if(vehicledetails.getSubscriptionID().equalsIgnoreCase("") || datecurrent.after(dateExpiry))
        {
           // tv_submit_vehicle.setText("View Details");
        }else
        {
            tv_submit_vehicle.setText("View Details");
        }






        iv_close_vehicle_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ExploreActivity)getActivity()).onBackPressed();

            }
        });
        tv_submit_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Bundle b = new Bundle();
                b.putSerializable("vehicledetails", vehicledetails);
                b.putSerializable("packagedetails", subsciptionPlansArrayList);


                ExploreActivity.replace_fragmnet_bundle(new ConfirmRSA(), b);*/
                if(tv_submit_vehicle.getText().toString().equalsIgnoreCase("View Details"))
                {
                    Bundle b=new Bundle();
                   /*

                    Intent intent = new Intent(VehicleDetailsRSA.this, ExploreActivity.class);*/
                    b.putSerializable("vehicledetails", vehicledetails);
                    b.putSerializable("packagedetails", subsciptionPlansArrayList);
                    ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new SuccessfulRSA(), b);

                   /* intent.putExtra("fromvehicledetailRSA", "fromvehicledetailRSA");
                    startActivity(intent);*/

                }else {

                   /* Intent intent = new Intent(VehicleDetailsRSA.this, ExploreActivity.class);
                    intent.putExtra("vehicledetails", vehicledetails);
                    intent.putExtra("packagedetails", subsciptionPlansArrayList);
                    intent.putExtra("fromrsa", "fromrsa");
                    startActivity(intent);*/
                    Bundle b = new Bundle();
                    b.putSerializable("vehicledetails", vehicledetails);
                    b.putSerializable("packagedetails", subsciptionPlansArrayList);
                    ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new ConfirmRSA(), b);

                }
            }
        });

       /* ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity) getActivity()).onBackPressed();
            }
        });*/
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(getActivity()).addApi(AppIndex.API).build();
        return v;
    }

    private void setdata() {
        tv_vehicle_reg_no.setText(vehicledetails.getVehicleRegNumber());
        Log.d("system out", "vehicle reg no_____" + vehicledetails.getVehicleRegNumber());
        tv_make.setText(vehicledetails.getVehicleMake());
        tv_year.setText(vehicledetails.getVehicleYear());
        tv_color.setText(vehicledetails.getVehicleColor());
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("VehicleDetailsRSA Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.VISIBLE);

    }
}
