package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideBottomExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Interface.CallMyTrip;
import com.bpcl.happyroads.Pojo.CopyTrip;
import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Poi_Night;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DeleteTrip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.MyTrip;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.Utils.BlurBuilder;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DownloadCode;
import com.bpcl.happyroads.Utils.GPSTracker;
import com.bpcl.happyroads.Utils.ImageSaver;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class MyTripsFragment extends Fragment implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, CallMyTrip {
    LinearLayout ll_main_hsv;
    TextView tv_tip_to_name;

    RelativeLayout error_layout;
    View load_pb;
    ArrayList<MyTrip> myTripArrayList = new ArrayList<>();
    ArrayList<MyTrip> myTripfilterArrayList = new ArrayList<>();
    String trip_status = "Completed";
    ListView lv_mytrip_list;
    MyTripAdapter myTripAdapter;
    View no_trip_found_layout;
    TextView tv_discover_trip;
    Dialog myTripDialog, startMyTripDialog;
    TextView tv_start_date_my_trip, tv_return_date_my_trip;
    String start_date = "", start_time = "", end_date = "", end_time = "";
    String start_date1 = "", start_time1 = "", end_date1 = "", end_time1 = "";
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    private Calendar now;
    boolean tripFlag = false;
    String select = "";
    String select1 = "";
    Integer pos_date=-1;
    String name="";
    TextView  tv_start_date, tv_start_time;
    String date;
    String date1 = null;
    ArrayList<MyTrip> myTripFileterlist;
    ArrayList<UpdateTrip> updateTripList = new ArrayList<>();
    DeleteTrip deleteTripList = new DeleteTrip();
    int count = 0;
    SessionManager sessionManager;
    int width;
    int height;
    Dialog pb_dialog;
    DBHandler dbHandler;
    Bitmap originbitmap = null, desbitamp = null;
    String trip_return_end_date = "", trip_return_end_time = "", trip_end_date_eta = "", trip_end_time_eta = "";
    ImageSaver imageSaver;
    LinearLayout ll_return_date,ll_start_date;
    private boolean fragmentResume=false;
    private boolean fragmentVisible=false;
    private boolean fragmentOnCreated=false;
    TextView tv_trip_name;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    private String select_dpd_type;
    private TextView tv_start_date_my_return_trip, tv_start_time_my_return_trip;
    private List<DestinationPlan> destinationplanList;
    Double dayhours = 8.0;
    Double starttime = 7.0;
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM ,yyyy");
    private int visibleThreshold = 5;
    // The current offset index of data you have loaded
    private int currentPage = 0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean loading = true;
    // Sets the starting page index
    private int startingPageIndex = 0;
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    Integer tripiditem=-1;
    Boolean flag=false;
    private GPSTracker gpsTracker;
    private SharedPreferences preferences;
    Integer returnPlan=-1;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mytrips_fragment, container, false);
        imageSaver=new ImageSaver(getActivity());
        sessionManager = new SessionManager(getActivity());
        dbHandler = new DBHandler(getActivity());
        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();
        width = mDisplay.getWidth();
        height = mDisplay.getHeight();
        tv_tip_to_name = (TextView) v.findViewById(R.id.tv_tip_to_name);
        ll_main_hsv = (LinearLayout) v.findViewById(R.id.ll_main_hsv);
        load_pb = (View) v.findViewById(R.id.load_progressBar);
        error_layout = (RelativeLayout) v.findViewById(R.id.error_layout);
        lv_mytrip_list = (ListView) v.findViewById(R.id.lv_mytrip_list);
        no_trip_found_layout = (View) v.findViewById(R.id.no_trip_found_layout);
        tv_discover_trip = (TextView) v.findViewById(R.id.tv_discoverbtn);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);

        trip_status = "UpComing";

        now = Calendar.getInstance();
        Date date = null;
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf1.format(new Date());
        try {
            date = sdf1.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);


        dpd = DatePickerDialog.newInstance(
                MyTripsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(calendar);
        tpd = TimePickerDialog.newInstance(
                MyTripsFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );
        tpd.setMinTime(calendar.getTime().getHours(),calendar.getTime().getMinutes(),calendar.getTime().getSeconds());

        if (!fragmentResume && fragmentVisible){   //only when first time fragment is created
            myUIUpdate();
        }
        preferences = getActivity().getSharedPreferences(Constants.TripTrackPREFERENCES, 0);
        return v;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);




        lv_mytrip_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {


                try {

                    //    lv_mytrip_list.setSelection(lv_mytrip_list.getLastVisiblePosition());


                    if (myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getEndDesImage().contains("No_Image.jpg")) {
                        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                    } else {

                        if (myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().size() > 0)

                        {
                            String url = "";
                            for(int i=0;i<myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().size();i++)
                            {
                                if (myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                                    url = myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().get(i).getImgName();
                                    break;
                                }

                            }

                            if(url.length()<=0) {
                                if (myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().get(0).getImgName() != null) {
                                    url = myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().get(0).getImgName();
                                } else if (myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().size() > 1) {
                                    url = myTripFileterlist.get(lv_mytrip_list.getFirstVisiblePosition()).getImageListByDestinationId().get(1).getImgName();
                                }
                            }

                            Log.d("mytrip_scroll_pos", String.valueOf(lv_mytrip_list.getFirstVisiblePosition()) + "url:" + url);


                        }
                    }


                    // get_bitmap_from_freco(Constants.ImagePath+"20161001_0925Nature.png");
                } catch (Exception e) {
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.i("SCROLLING UP", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;


                lv_mytrip_list.setSelection(lv_mytrip_list.getSelectedItemPosition());

                if (totalItemCount < previousTotalItemCount) {
                    currentPage = startingPageIndex;
                    previousTotalItemCount = totalItemCount;
                    if (totalItemCount == 0) { loading = true; }
                }

                // If it’s still loading, we check to see if the dataset count has
                // changed, if so we conclude it has finished loading and update the current page
                // number and total item count.
                if (loading && (totalItemCount > previousTotalItemCount)) {
                    loading = false;
                    previousTotalItemCount = totalItemCount;
                    currentPage++;
                }

                // If it isn’t currently loading, we check to see if we have breached
                // the visibleThreshold and need to reload more data.
                // If we do need to reload some more data, we execute onLoadMore to fetch the data.
                if (!loading && (totalItemCount - visibleItemCount)<=(firstVisibleItem + visibleThreshold)) {
                    onLoadMore(currentPage + 1, totalItemCount);
                    loading = true;
                }


            }

            public void onLoadMore(int page, int totalItemsCount) {
                getMyTrip(page);

            }

        });



        tv_discover_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity) getActivity()).ll_discover.performClick();
            }
        });

    }
    private void myUIUpdate() {
        myTripArrayList = new ArrayList<>();
        myTripFileterlist = new ArrayList<>();
        no_trip_found_layout.setVisibility(View.GONE);
        ll_main_hsv.setVisibility(View.VISIBLE);
        load_pb.setVisibility(View.VISIBLE);
        trip_status = getArguments().getString("trip_status");
        tripiditem = getArguments().getInt("tripid");
        Log.d("System out","tripid from Good to go in my trip"+tripiditem);
        myTripAdapter = new MyTripAdapter(getActivity(), myTripArrayList);
        lv_mytrip_list.setAdapter(myTripAdapter);





        getMyTrip(1);

    }
    @Override
    public void onResume() {
        super.onResume();

        ((ExploreActivity) getActivity()).tv_text_header.setText("My Trips Summary");
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }
   /*  @Override
   public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
        myTripArrayList = new ArrayList<>();
            myTripFileterlist = new ArrayList<>();
            no_trip_found_layout.setVisibility(View.GONE);
            ll_main_hsv.setVisibility(View.VISIBLE);
            load_pb.setVisibility(View.VISIBLE);
            myTripAdapter = new MyTripAdapter(getActivity(), myTripArrayList);
            lv_mytrip_list.setAdapter(myTripAdapter);
            trip_status = getArguments().getString("trip_status");
            load_pb.setVisibility(View.GONE);
        getMyTrip();
        lv_mytrip_list.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {


              *//*  try {

                    //    lv_mytrip_list.setSelection(lv_mytrip_list.getLastVisiblePosition());


                    if (myTripFileterlist.get(lv_mytrip_list.getLastVisiblePosition()).getEndDesImage().contains("No_Image.jpg")) {
                        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                    } else {

                        if (myTripFileterlist.get(lv_mytrip_list.getLastVisiblePosition()).getImageListByDestinationId().size() > 0)

                        {
                            String url = "";
                            if (myTripFileterlist.get(lv_mytrip_list.getLastVisiblePosition()).getImageListByDestinationId().get(0).getImgName() != null) {
                                url = myTripFileterlist.get(lv_mytrip_list.getLastVisiblePosition()).getImageListByDestinationId().get(0).getImgName();
                            } else if (myTripFileterlist.get(lv_mytrip_list.getLastVisiblePosition()).getImageListByDestinationId().size() > 1) {
                                url = myTripFileterlist.get(lv_mytrip_list.getLastVisiblePosition()).getImageListByDestinationId().get(1).getImgName();
                            }
                            if (url.length() > 0)
                                get_bitmap_from_freco(Constants.TimbThumb_ImagePath + url + "&width=" + (width + 200) + "&height=" + (width + 500));
                            else {
                                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                            }
                        } else {
                            ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                        }
                    }


                    // get_bitmap_from_freco(Constants.ImagePath+"20161001_0925Nature.png");
                } catch (Exception e) {
                }*//*
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.i("SCROLLING UP", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;


                //  lv_mytrip_list.setSelection(lv_mytrip_list.getSelectedItemPosition());

            }
        });




        *//*  ll_completed_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_completed_tab.setTextColor(getResources().getColor(R.color.textYellow));
                iv_completed_tab.setVisibility(View.VISIBLE);
                iv_upcoming_tab.setVisibility(View.INVISIBLE);
                iv_saves_tab.setVisibility(View.INVISIBLE);
                iv_ongoing_tab.setVisibility(View.INVISIBLE);


                tv_upcoming_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_ongoing_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_saves_tab.setTextColor(getResources().getColor(R.color.colorWhite));

                trip_status = "Completed";


                set_adapter(trip_status);


            }
        });*//*
       *//* ll_ongoing_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_ongoing_tab.setTextColor(getResources().getColor(R.color.textYellow));
                iv_ongoing_tab.setVisibility(View.VISIBLE);
                iv_upcoming_tab.setVisibility(View.INVISIBLE);
                iv_saves_tab.setVisibility(View.INVISIBLE);
                iv_completed_tab.setVisibility(View.INVISIBLE);
             *//**//*   LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v1;
                v1 = inflater.inflate(R.layout.ongoing_trips,null);*//**//*


                // ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.my_trip_bg2);


                tv_completed_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_upcoming_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_saves_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                trip_status = "OnGoing";
                set_adapter(trip_status);


            }
        });*//*
      *//*  ll_upcoming_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tv_upcoming_tab.setTextColor(getResources().getColor(R.color.textYellow));
                iv_upcoming_tab.setVisibility(View.VISIBLE);
                iv_completed_tab.setVisibility(View.INVISIBLE);
                iv_saves_tab.setVisibility(View.INVISIBLE);
                //    ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.my_trip_bg3);

                iv_ongoing_tab.setVisibility(View.INVISIBLE);
                tv_completed_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_ongoing_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_saves_tab.setTextColor(getResources().getColor(R.color.colorWhite));

                trip_status = "UpComing";
                set_adapter(trip_status);


            }
        });*//*

      *//*  ll_saves_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_saves_tab.setTextColor(getResources().getColor(R.color.textYellow));
                iv_saves_tab.setVisibility(View.VISIBLE);
                iv_upcoming_tab.setVisibility(View.INVISIBLE);
                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.my_trip_bg4);
                iv_completed_tab.setVisibility(View.INVISIBLE);
                iv_ongoing_tab.setVisibility(View.INVISIBLE);
                tv_completed_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_ongoing_tab.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_upcoming_tab.setTextColor(getResources().getColor(R.color.colorWhite));

                trip_status = "Saved";
                set_adapter(trip_status);


            }
        });*//*

        tv_discover_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity) getActivity()).open_main_fragment();
            }
        });

    }*/

    public void setDate(String fromDate,Integer pos) {
        if(fromDate.equalsIgnoreCase("start date"))
        {
            select="";
            select1 = "start";
            start_date1 = "";
            start_time1 = "";
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");



        }

    }





    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
    }


    private void getMyTrip(final int page) {
        // get & set progressbar dialog
        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        // pb_dialog.show();
        pb_dialog.dismiss();
        if (Constants.isInternetAvailable(getActivity())) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            String json = "[{\"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",  \"PageNo\":" + page + ",    \"PageSize\": 15,\"TripStatus\":\"" + trip_status + "\" }]";
            Log.d("json_CreateTrip", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<ArrayList<MyTrip>> call = apiService.GetMyTrip(json);
            call.enqueue(new Callback<ArrayList<MyTrip>>() {
                @Override
                public void onResponse(Call<ArrayList<MyTrip>> call, Response<ArrayList<MyTrip>> response) {
                    if (response.body() != null) {

                        if (page == 1)
                        {
                            myTripArrayList = new ArrayList<>();
                        }
                        pb_dialog.dismiss();
                        //myTripArrayList.addAll(response.body());
                        if (response.body().get(0).getResStatus() == true)
                        {

                            // else {
                            myTripArrayList.addAll(response.body());
                            try {


                              /*  myTripFileterlist.clear();
                                myTripFileterlist = myTripArrayList;*/
                                Log.d("System out", "myTripArrayList_____" + myTripArrayList.size());
                                Log.d("System out", "myTripFileterlist_____" + myTripFileterlist.size());
                                myTripAdapter = new MyTripAdapter(getActivity(), myTripArrayList);

                                lv_mytrip_list.setAdapter(myTripAdapter);
                                myTripAdapter.notifyDataSetChanged();
                                if (page > 1) {
                                    lv_mytrip_list.setSelection((page - 1) * 15);
                                }

                                if (myTripFileterlist.size() == 0)
                                {
                                    no_trip_found_layout.setVisibility(View.VISIBLE);
                                    ll_main_hsv.setVisibility(View.GONE);
                                    load_pb.setVisibility(View.GONE);
                                }
                                else
                                {
                                    no_trip_found_layout.setVisibility(View.GONE);
                                    ll_main_hsv.setVisibility(View.VISIBLE);
                                    load_pb.setVisibility(View.GONE);

                                    String url = "";
                                    for (int i = 0; i < myTripFileterlist.get(0).getImageListByDestinationId().size(); i++) {
                                        if (myTripFileterlist.get(0).getImageListByDestinationId().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                                            url = myTripFileterlist.get(0).getImageListByDestinationId().get(i).getImgName();
                                            break;
                                        }

                                    }

                                    if (url.length() <= 0) {
                                        if (myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName() != null) {
                                            url = myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName();
                                        } else if (myTripFileterlist.get(0).getImageListByDestinationId().size() > 1) {
                                            url = myTripFileterlist.get(0).getImageListByDestinationId().get(1).getImgName();
                                        }
                                    }
                                    Log.d("status_trip_url", url);
                                    //((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                                }
                            } catch (Exception e) {
                                e.toString();
                            }
                            //   set_adapter(trip_status);
//                        }


                        } else {
                            if (myTripArrayList.size() <= 0) {
                                pb_dialog.dismiss();
                                myTripArrayList = new ArrayList<>();
                                no_trip_found_layout.setVisibility(View.VISIBLE);
                                ll_main_hsv.setVisibility(View.GONE);
                                load_pb.setVisibility(View.GONE);
                            }

                        }

                    }
                    load_pb.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<ArrayList<MyTrip>> call, Throwable t) {
                    // Log error here since request failed
                    t.printStackTrace();
                    load_pb.setVisibility(View.GONE);
                    pb_dialog.dismiss();
                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                    }
                }
            });
        }
        else {
            pb_dialog.dismiss();
            load_pb.setVisibility(View.GONE);
            myTripArrayList = new ArrayList<>();
            if (trip_status.contains("OnGoing") || trip_status.contains("UpComing"))
            {
                List<Create_Trip> createTripList = new ArrayList<>();
                createTripList = dbHandler.get_All_TRIP(trip_status);
                for (int i = 0; i < createTripList.size(); i++) {
                    get_trip_in_to_database(createTripList.get(i));
                }
                createTripList.clear();
                createTripList = dbHandler.get_All_TRIP(trip_status);
                for (int i = 0; i < createTripList.size(); i++) {
                    if(createTripList.get(i).getTripIsOneSideCompleted())
                        get_trip_in_to_database(createTripList.get(i));
                }
                myTripAdapter = new MyTripAdapter(getActivity(), myTripArrayList);
                lv_mytrip_list.setAdapter(myTripAdapter);
                myTripAdapter.notifyDataSetChanged();
            }
            if (myTripArrayList.size() <= 0) {
                myTripArrayList = new ArrayList<>();
                no_trip_found_layout.setVisibility(View.VISIBLE);
                ll_main_hsv.setVisibility(View.GONE);
                load_pb.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }

        }
    }

    public ArrayList<MyTrip> get_filter_list(String trip_status) {
        ArrayList<MyTrip> myTripsList = new ArrayList<>();
        for (int i = 0; i < myTripArrayList.size(); i++) {
            if (myTripArrayList.get(i).getTripStatus().toLowerCase().contains(trip_status.toLowerCase())) {
                myTripsList.add(myTripArrayList.get(i));
            }
            if (myTripArrayList.get(i).getTripStatus().trim().equalsIgnoreCase("OnGoing")) {
                count++;
            }
        }
        return myTripsList;
    }


    public Integer get_postion_main_list(Integer trip_Id) {
        Integer pos = -1;
        ArrayList<MyTrip> myTripsList = new ArrayList<>();
        for (int i = 0; i < myTripArrayList.size(); i++) {
            if (myTripArrayList.get(i).getTripId() == trip_Id) {
                pos = i;
                break;
            }
        }
        return pos;
    }
    public Integer get_postion_main_list1(Integer trip_Id, ArrayList<MyTrip> myTripsList) {
        Integer pos = -1;
        for (int i = 0; i < myTripsList.size(); i++) {
            Log.d("System out","myTripsList.get(i).getTripId()"+myTripsList.get(i).getTripId());
            Log.d("System out","trip_Id"+trip_Id);
            Log.d("System out","trip status "+(myTripsList.get(i).getTripId().intValue() == trip_Id.intValue()));
            if (myTripsList.get(i).getTripId().intValue() == trip_Id.intValue()) {
                pos = i;
                break;
            }
        }
        return pos;
    }
    @Override
    public void CallTrip(Activity con) {
        myTripArrayList = new ArrayList<>();
        myTripFileterlist = new ArrayList<>();
        no_trip_found_layout.setVisibility(View.GONE);
        ll_main_hsv.setVisibility(View.VISIBLE);
        load_pb.setVisibility(View.VISIBLE);
        myTripAdapter = new MyTripAdapter(getActivity(), myTripArrayList);
        lv_mytrip_list.setAdapter(myTripAdapter);
        trip_status = "Completed";
        getMyTrip(1);
    }
    private class MyTripAdapter extends BaseAdapter {
        Context context;
        ArrayList<MyTrip> myTripsList = new ArrayList<>();
        MyTripAdapter(Context context, ArrayList<MyTrip> tripList) {
            this.context = context;
            this.myTripsList = tripList;
            myTripFileterlist=new ArrayList<>();
            myTripFileterlist.clear();
            myTripFileterlist=tripList;
        }
        @Override
        public boolean isEnabled(int position) {
            return false;
        }
        @Override
        public int getCount() {
            return myTripsList.size();
        }
        @Override
        public Object getItem(int position) {
            return position;
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.completed_trips, null);
            TextView tv_source, tv_destination, tv_end_date, tv_end_time, tv_return_date, tv_return_time, tv_trip_return_date_eta, tv_trip_return_time_eta;
            LinearLayout ll_trip_return_layout;
            ImageView iv_delete, iv_copy, iv_star_rating, iv_share, iv_start_trip, iv_saved_calender;
            LinearLayout iv_car_down,iv_destination,iv_car_up;
            TextView tv_car_down_text,tv_destination_text,tv_car_up_text;

            tv_source = (TextView) v.findViewById(R.id.tv_source);
            tv_destination = (TextView) v.findViewById(R.id.tv_destination);
            tv_car_down_text = (TextView) v.findViewById(R.id.tv_car_down_text);
            tv_destination_text = (TextView) v.findViewById(R.id.tv_destination_text);
            tv_car_up_text = (TextView) v.findViewById(R.id.tv_car_up_text);

            tv_start_date = (TextView) v.findViewById(R.id.tv_start_date);
            tv_start_time = (TextView) v.findViewById(R.id.tv_start_time);
            tv_end_date = (TextView) v.findViewById(R.id.tv_end_date);
            tv_end_time = (TextView) v.findViewById(R.id.tv_end_time);
            tv_trip_name = (TextView) v.findViewById(R.id.tv_tip_to_name);
            ll_start_date=(LinearLayout)v.findViewById(R.id.ll_start_date);
            ll_return_date=(LinearLayout)v.findViewById(R.id.ll_return_date);

            iv_delete = (ImageView) v.findViewById(R.id.iv_delete_trip);
            iv_copy = (ImageView) v.findViewById(R.id.iv_copy_trip);
            iv_share = (ImageView) v.findViewById(R.id.iv_share_trip);
            iv_star_rating = (ImageView) v.findViewById(R.id.iv_rate_trip);
            iv_start_trip = (ImageView) v.findViewById(R.id.iv_start_trip);
            iv_saved_calender = (ImageView) v.findViewById(R.id.iv_saved_calender);
            iv_destination = (LinearLayout) v.findViewById(R.id.iv_destination);

            iv_car_up = (LinearLayout) v.findViewById(R.id.iv_car_up);
            iv_car_down = (LinearLayout) v.findViewById(R.id.iv_car_down);

            ll_trip_return_layout = (LinearLayout) v.findViewById(R.id.ll_return_date_trip);
            tv_return_date = (TextView) v.findViewById(R.id.tv_trip_return_date);
            tv_return_time = (TextView) v.findViewById(R.id.tv_trip_return_time);

            tv_trip_return_date_eta = (TextView) v.findViewById(R.id.tv_trip_return_date_eta);
            tv_trip_return_time_eta = (TextView) v.findViewById(R.id.tv_trip_return_time_eta);


            final MyTrip myTrip = myTripsList.get(position);

            if (myTrip.getGetTripPlans().get(0).getResStatus() == false) {
                //  iv_destination.setBackgroundResource(R.drawable.my_trip_box);
                tv_destination_text.setTextColor(getResources().getColor(R.color.graycolor));

                //  iv_car_down.setBackgroundResource(R.drawable.my_trip_box);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.graycolor));
                // iv_car_down.setRotation(180);
                // iv_car_up.setBackgroundResource(R.drawable.my_trip_box);
                tv_car_up_text.setTextColor(getResources().getColor(R.color.graycolor));
            } else {
                //  iv_destination.setBackgroundResource(R.drawable.my_trip_box);
                tv_destination_text.setTextColor(getResources().getColor(R.color.graycolor));
                //  iv_car_down.setBackgroundResource(R.drawable.my_trip_box_unselected);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.colorWhite));
                //  iv_car_up.setBackgroundResource(R.drawable.my_trip_box);
                tv_car_up_text.setTextColor(getResources().getColor(R.color.graycolor));


            }

            v.setId(myTripsList.get(position).getTripId());

            ll_start_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    start_date="";
                    start_time="";
                    end_date="";
                    end_time="";
                    pos_date = get_postion_main_list(myTripsList.get(position).getTripId());
                    name=myTripsList.get(position).getTripName();
                    if(trip_status.equalsIgnoreCase("Saved")) {
                        setDate("start date",pos_date);
                    }
                }

            });
            if (trip_status.contains("Completed")) {
                iv_delete.setVisibility(View.VISIBLE);
                iv_copy.setVisibility(View.VISIBLE);
                iv_share.setVisibility(View.VISIBLE);
                iv_star_rating.setVisibility(View.VISIBLE);
                iv_start_trip.setVisibility(View.GONE);
                iv_saved_calender.setVisibility(View.GONE);


            } else if (trip_status.contains("OnGoing")) {
                iv_delete.setVisibility(View.GONE);
                iv_copy.setVisibility(View.GONE);
                iv_start_trip.setVisibility(View.VISIBLE);
                iv_share.setVisibility(View.VISIBLE);
                iv_star_rating.setVisibility(View.GONE);
                iv_saved_calender.setVisibility(View.GONE);
                //  iv_car_down.setBackgroundResource(R.drawable.my_trip_box_unselected);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.colorWhite));



            } else if (trip_status.contains("UpComing")) {
                iv_delete.setVisibility(View.VISIBLE);
                iv_copy.setVisibility(View.GONE);
                iv_share.setVisibility(View.VISIBLE);
                iv_star_rating.setVisibility(View.GONE);
                iv_start_trip.setVisibility(View.VISIBLE);
                iv_saved_calender.setVisibility(View.GONE);
                if(tripiditem!=-1 && flag==false) {
                    int pos = get_postion_main_list1(tripiditem,myTripsList);
                    lv_mytrip_list.smoothScrollToPosition(pos);
                    tripiditem=-1;
                    flag=true;

                }


                //   iv_destination.setImageResource(R.drawable.destination_unselected_my_trip);
                //  iv_car_down.setBackgroundResource(R.drawable.my_trip_box_unselected);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.colorWhite));
                //   iv_car_up.setImageResource(R.drawable.car_up_unselected_my_trip);

            } else if (trip_status.contains("Saved")) {
                iv_delete.setVisibility(View.VISIBLE);
                iv_copy.setVisibility(View.GONE);
                iv_share.setVisibility(View.VISIBLE);
                iv_star_rating.setVisibility(View.GONE);
                iv_start_trip.setVisibility(View.GONE);
                iv_saved_calender.setVisibility(View.VISIBLE);
                //  iv_car_down.setBackgroundResource(R.drawable.my_trip_box);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.graycolor));
                //  iv_car_down.setRotation(180);

                if(tripiditem!=-1 && flag==false) {

                    int pos = get_postion_main_list1(tripiditem,myTripsList);
                    lv_mytrip_list.smoothScrollToPosition(pos);
                    tripiditem=-1;
                    flag=true;
                    Log.d("System out","tripid is in my trip"+pos);
                }

            }

            if (myTrip.getGetTripPlans().size() > 0 && myTrip.getResStatus() == true) {

                for (int i = 0; i < myTrip.getGetTripPlans().size(); i++) {
                    if (myTrip.getGetTripPlans().get(i).getTripWayType().contains("DesPlan")) {
                        myTrip.setTripIsDestinationPlanned(true);
                        break;
                    }
                }
            }
            else
            {
                myTrip.setTripIsDestinationPlanned(false);
            }


            if (myTrip.getTripIsDestinationPlanned() == true) {
                // iv_destination.setBackgroundResource(R.drawable.my_trip_box_unselected);
                tv_destination_text.setTextColor(getResources().getColor(R.color.colorWhite));


            } else {
                // iv_destination.setBackgroundResource(R.drawable.my_trip_box);
                tv_destination_text.setTextColor(getResources().getColor(R.color.graycolor));

            }


            Boolean isReturnplan = false, isOnewayplan = false;

            if (myTrip.getGetTripPlans().size() > 0 && myTrip.getResStatus() == true) {

                for (int i = 0; i < myTrip.getGetTripPlans().size(); i++) {
                    if (myTrip.getGetTripPlans().get(i).getTripWayIsReturnPlanned() != null) {
                        if (myTrip.getGetTripPlans().get(i).getTripWayIsReturnPlanned()) {
                            isReturnplan = true;
                            break;
                        }
                    }
                }
            }


            if (isReturnplan) {
                //  iv_car_up.setBackgroundResource(R.drawable.my_trip_box_unselected);
                tv_car_up_text.setTextColor(getResources().getColor(R.color.colorWhite));
                // iv_car_up.setRotation(180);

            } else {
                // iv_car_up.setBackgroundResource(R.drawable.my_trip_box);
                tv_car_up_text.setTextColor(getResources().getColor(R.color.graycolor));

            }

            if (myTrip.getGetTripPlans().size() > 0 && myTrip.getResStatus() == true) {

                for (int i = 0; i < myTrip.getGetTripPlans().size(); i++) {
                    if (myTrip.getGetTripPlans().get(i).getTripWayIsReturnPlanned() != null) {
                        if (!myTrip.getGetTripPlans().get(i).getTripWayIsReturnPlanned() && !myTrip.getGetTripPlans().get(i).getTripWayType().contains("DesPlan")) {
                            isOnewayplan = true;
                            break;

                        }
                    }
                }
            }


            if (isOnewayplan) {

                //  iv_car_down.setBackgroundResource(R.drawable.my_trip_box_unselected);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.colorWhite));
                // iv_car_down.setRotation(0);

            } else {
                //iv_car_down.setBackgroundResource(R.drawable.my_trip_box);
                tv_car_down_text.setTextColor(getResources().getColor(R.color.graycolor));
                // iv_car_down.setRotation(180);
            }


            tv_source.setText("\n" + myTrip.getOriginDesName());
            tv_destination.setText(myTrip.getEndDesName());
            tv_trip_name.setText(myTrip.getTripName());
            if (myTrip.getTripDate() == null) {
                tv_start_date.setText("DD MM,YYYY");
                tv_start_time.setText("00:00");

            } else {
                try {

                    tv_start_date.setText(Constants.formatDate(myTrip.getTripDate().toString(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_start_date.setText(myTrip.getTripDate().toString());
                }

                try {
                    tv_start_time.setText(Constants.formatDate(myTrip.getTripDate().toString(), "yyyy-MM-dd'T'HH:mm:ss", "hh:mm a"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_start_time.setText(myTrip.getTripDate().toString());
                }
            }
            if (myTrip.getTripEndDateTime() == null) {
                tv_end_date.setText("DD MM,YYYY");
                tv_end_time.setText("00:00");
                tv_end_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("System out", "in upcoming trips____");
                    }
                });
            } else {
                try {
                    tv_end_date.setText(Constants.formatDate(myTrip.getTripEndDateTime().toString(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_end_date.setText(myTrip.getTripEndDateTime().toString());
                }

                try {
                    tv_end_time.setText(Constants.formatDate(myTrip.getTripEndDateTime().toString(), "yyyy-MM-dd'T'HH:mm:ss", "hh:mm a"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_end_time.setText(myTrip.getTripEndDateTime().toString());
                }

            }

            if (myTrip.getTripReturnDate() == null) {
                tv_return_date.setText("DD MM,YYYY");
                tv_return_time.setText("00:00 AM");
                ll_trip_return_layout.setVisibility(View.INVISIBLE);

            } else {
                ll_trip_return_layout.setVisibility(View.VISIBLE);
                try {

                    tv_return_date.setText(Constants.formatDate(myTrip.getTripReturnDate().toString(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_return_date.setText("DD MM,YYYY");
                    tv_return_time.setText("00:00 AM");
                    ll_trip_return_layout.setVisibility(View.INVISIBLE);
                }

                try {
                    tv_return_time.setText(Constants.formatDate(myTrip.getTripReturnDate().toString(), "yyyy-MM-dd'T'HH:mm:ss", "hh:mm a"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_return_date.setText("DD MM,YYYY");
                    tv_return_time.setText("00:00 AM");
                }
            }

            if (myTrip.getTripReturnDateETA() == null) {
                tv_trip_return_date_eta.setText("DD MM,YYYY");
                tv_trip_return_time_eta.setText("00:00 AM");
                ll_trip_return_layout.setVisibility(View.INVISIBLE);

            } else {
                ll_trip_return_layout.setVisibility(View.VISIBLE);
                try {

                    tv_trip_return_date_eta.setText(Constants.formatDate(myTrip.getTripReturnDateETA().toString(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_trip_return_date_eta.setText("DD MM,YYYY");
                    tv_trip_return_time_eta.setText("00:00 AM");
                    ll_trip_return_layout.setVisibility(View.INVISIBLE);
                }

                try {
                    tv_trip_return_time_eta.setText(Constants.formatDate(myTrip.getTripReturnDateETA().toString(), "yyyy-MM-dd'T'HH:mm:ss", "hh:mm a"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    tv_trip_return_date_eta.setText("DD MM,YYYY");
                    tv_trip_return_time_eta.setText("00:00 AM");
                    ll_trip_return_layout.setVisibility(View.INVISIBLE);
                }
            }


            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BounceTopEnter mBasIn = new BounceTopEnter();
                    SlideBottomExit mBasOut = new SlideBottomExit();

                    final NormalDialog dialog = new NormalDialog(getActivity());


                    dialog.isTitleShow(false)//

                            .cornerRadius(5)//

                            .content("Are you sure to delete this trip?")//
                            .contentGravity(Gravity.CENTER)//
                            .contentTextColor(getResources().getColor(R.color.colorWhite))//
                            .bgColor(getResources().getColor(R.color.colorAccent))

                            .btnNum(2)
                            .btnText(getString(R.string.yes), getString(R.string.no))
                            .btnTextSize(16f, 16f)//
                            .btnTextColor(getResources().getColor(R.color.colorWhite), getResources().getColor(R.color.colorWhite))//

                            .widthScale(0.85f)//
                            .showAnim(mBasIn)//
                            .dismissAnim(mBasOut)//
                            .show();

                    dialog.setOnBtnClickL(
                            new OnBtnClickL() {
                                @Override
                                public void onBtnClick() {

                                    int pos = get_postion_main_list(myTripsList.get(position).getTripId());
                                    if (pos > -1) {

                                        DeleteTrip(pos,position);
                                        //myTripAdapter.notifyDataSetChanged();
                                    }
                                    dialog.dismiss();
                                }

                                private void DeleteTrip(final int pos, final int mainposition) {
                                    // get & set progressbar dialog

                                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);



                                    String json = "[{     \"TripId\":"+myTripsList.get(position).getTripId()+"}]";

                                    Call<DeleteTrip> call = apiService.DeleteTrip(json);
                                    call.enqueue(new Callback<DeleteTrip>() {
                                        @Override
                                        public void onResponse(Call<DeleteTrip> call, Response<DeleteTrip> response) {
                                            if (response.body() != null) {
                                                if (response.body().getResStatus() == true)
                                                {
                                                    deleteTripList = response.body();
                                                    Constants.show_error_popup_success(getActivity(), response.body().getResDescription(), error_layout);




                                                    myTripsList.remove(position);
                                                    myTripAdapter.notifyDataSetChanged();
                                                    if (myTripsList.size() == 0) {

                                                        no_trip_found_layout.setVisibility(View.VISIBLE);
                                                        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);

                                                    }
                                                    //set_adapter(trip_status);

                                                } else {
                                                    Constants.show_error_popup(getActivity(), response.body().getResDescription(), error_layout);
                                                }


                                            }
                                            load_pb.setVisibility(View.GONE);

                                        }

                                        @Override
                                        public void onFailure(Call<DeleteTrip> call, Throwable t) {
                                            // Log error here since request failed

                                            t.printStackTrace();
                                            load_pb.setVisibility(View.GONE);

                                            if (!Constants.isInternetAvailable(getActivity())) {
                                                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                                            } else {
                                                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                                            }

                                        }
                                    });

                                }

                            },
                            new OnBtnClickL() {
                                @Override
                                public void onBtnClick() {

                                    dialog.dismiss();
                                }
                            });
                }
            });

            v.setId(myTripsList.get(position).getTripId());
            iv_copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    copyTrip(myTripsList.get(position).getTripId());
                }
            });


            iv_share.setId(position);
            iv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.d("System out", "image name " + myTrip.getEndDesImage());
                    try {
                       String url= "https://goo.gl/lTciIP";
                        //get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + myTrip.getEndDesImage() + "&width=" + (500) + "&height=" + (500), myTrip.getEndDesImage(), "Destination :" + myTrip.getOriginDesName() + "\n" + "Trip Name: " + myTrip.getTripName() + "\n" + "Trip Date: " + (myTrip.getTripDate()==null?"":Constants.formatDate(myTrip.getTripDate().toString(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy")));
                        get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + myTrip.getEndDesImage() + "&width=" + (500) + "&height=" + (500), myTrip.getEndDesImage(), "I'm headed on a fun road trip to " + myTrip.getOriginDesName() +  "Join me with "+Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            });


            iv_saved_calender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = get_postion_main_list(myTripsList.get(position).getTripId());
                    Mytrip_Dialog(pos);
                }
            });
            v.setId(myTripsList.get(position).getTripId());

            iv_start_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("System out", "start trip click____" + myTripsList.size());

                    gpsTracker = new GPSTracker(getActivity());
                    if (gpsTracker.isGPSEnabled) {
                        int check_trip_in_db = dbHandler.check_trip_in_local_db(myTripsList.get(position).getTripId());


                        if (check_trip_in_db >= 1)
                        {
                            getMyTrip1(myTripsList.get(position).getTripId());
                        }
                        else {
                            add_trip_in_to_database(position, "tracking", false);
                        }
                    } else {
                        gpsTracker.showSettingsAlertDialog();
                        //  Constants.show_error_popup(getActivity(), "Please GPS is enabled", error_layout);
                    }

                }
            });

            iv_destination.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                   /* if(myTrip.getTripIsDestinationPlanned() == true)
                    {*/
                    if ( myTripsList.get(position).getTripStatus().contains("Completed") || myTripsList.get(position).getEnddesNoOfNearByAttraction()==0) {

                    }
                    else {
                        int check_trip_in_db = dbHandler.check_trip_in_local_db(myTripsList.get(position).getTripId());

                        Log.d("Sizemytrip", String.valueOf(myTripsList.size()));


                        if (check_trip_in_db >= 1) {

                            if (myTripsList.get(position).getTripStatus().contains("Saved")) {
                                dbHandler.update_all_trip_date_time(myTripsList.get(position).getTripId());
                            }

                            Bundle b = new Bundle();
                            b.putInt("tripId", myTripsList.get(position).getTripId());
                           // b.putString("from", "mytrips");
                            ((ExploreActivity)getActivity()).whichActivity=true;
                            ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyPlanFragment(), b);
                        } else {

                            add_trip_in_to_database(position, "DES", false);


                        }

                   /* }*/
                    }
                }
            });

/*
            iv_car_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(myTrip.getTripReturnDate() != null) {
                        int check_trip_in_db = dbHandler.check_trip_in_local_db(myTripsList.get(position).getTripId());

                        Log.d("Sizemytrip", String.valueOf(myTripsList.size()));

                        if (check_trip_in_db >= 1) {


                            if( myTripsList.get(position).getTripStatus().contains("Saved"))
                            {
                                dbHandler.update_all_trip_date_time(myTripsList.get(position).getTripId());
                            }

                            Bundle b = new Bundle();
                            b.putInt("tripId", myTripsList.get(position).getTripId());
                            b.putString("plantype","DTOS");
                            ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                        } else {

                            add_trip_in_to_database(position,"DTOS");


                        }
                    }
                }
            });
*/

            iv_car_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(myTripsList.get(position).getTripStatus().contains("OnGoing") || myTripsList.get(position).getTripStatus().contains("Completed"))
                    {

                    }
                    else
                    {
                        int check_trip_in_db = dbHandler.check_trip_in_local_db(myTripsList.get(position).getTripId());

                        Log.d("Sizemytrip ", String.valueOf(myTripsList.size())+" check_trip_in_db ___ "+check_trip_in_db);

                        if (check_trip_in_db >= 1)
                        {


                            if (myTripsList.get(position).getTripStatus().contains("Saved")) {
                                dbHandler.update_all_trip_date_time(myTripsList.get(position).getTripId());
                            }

                            if(myTripsList.get(position).getTripStatus().contains("UpComing")&& myTripsList.get(position).getTripReturnDate()==null)
                            {
                                insert_trip_return_date(position,false);
                            }
                            else
                            {

                                Bundle b = new Bundle();
                                b.putInt("tripId", myTripsList.get(position).getTripId());
                                b.putString("plantype", "DTOS");
                                b.putString("from", "mytrips");
                                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                            }
                        }
                        else {
                            if (myTripsList.get(position).getTripStatus().contains("Saved")) {
                                dbHandler.update_all_trip_date_time(myTripsList.get(position).getTripId());
                            }

                            if(myTripsList.get(position).getTripStatus().contains("UpComing")&& myTripsList.get(position).getTripReturnDate()==null)
                            {
                                Log.d("System out","myTripsList.get(position).getTripReturnDate() in if   "+myTripsList.get(position).getTripReturnDate());
                                insert_trip_return_date(position,false);
                            }

                            else
                            {
                                Log.d("System out","myTripsList.get(position).getTripReturnDate() in else   "+myTripsList.get(position).getTripReturnDate());

                                add_trip_in_to_database(position, "DTOS",true);
                            }



                        }
                    }
                }

            });

            iv_car_down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (myTripsList.get(position).getTripStatus().contains("OnGoing") || myTripsList.get(position).getTripStatus().contains("Completed")) {

                    } else {

                        int check_trip_in_db = dbHandler.check_trip_in_local_db(myTripsList.get(position).getTripId());

                        Log.d("Sizemytrip", String.valueOf(myTripsList.size()));

                        if (check_trip_in_db >= 1) {


                            if (myTripsList.get(position).getTripStatus().contains("Saved")) {
                                dbHandler.update_all_trip_date_time(myTripsList.get(position).getTripId());
                            }
                            if (myTripsList.get(position).getTripStatus().contains("OnGoing")) {
                                Bundle b = new Bundle();
                                b.putInt("tripId", myTripsList.get(position).getTripId());
                                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);

                            } else {
                                Bundle b = new Bundle();
                                b.putString("plantype", "STOD");
                                b.putString("from", "mytrips");
                                b.putInt("tripId", myTripsList.get(position).getTripId());
                                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                            }
                        } else {

                            add_trip_in_to_database(position, "STOD", false);


                        }

                    }
                }
            });

            return v;
        }
        @Override
        public int getViewTypeCount() {

            if (getCount() != 0)
                return getCount();

            return 1;
        }
    }




    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //  SimpleDateFormat formatter = new SimpleDateFormat("dd MMM ,yyyy");
        date = dayOfMonth + " " + (++monthOfYear) + "," + year;

        //  dateTextView.setText(date);


        // formatter.format(date);
        tpd.show(getActivity().getFragmentManager(), "TimePickerDialog");
        // tpd1.show(getActivity().getFragmentManager(), "TimePickerDialog");


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        // tv_start_date_my_trip.setText();
        //  String date = "You picked the following date: "+dayOfMonth+"/"+(++monthOfYear)+"/"+year;

        String time = hourOfDay + ":" + minute + "";

        try {
            date1 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM, yyyy hh:mm a");
            if (select.equalsIgnoreCase("start")) {
                start_date = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
                start_time = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "HH:mm");
                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date1);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(datecurrent);
                //  calendar.add(Calendar.HOUR, 2);
                calendar.add(Calendar.HOUR, 0);

                //  currentDateandTime.set
                if (calendar.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                    // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", error_layout);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", error_layout);

                }else {
                    tv_start_date_my_trip.setText(date1);
                }
            } else if (select.equalsIgnoreCase("stop")) {
                end_date = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
                end_time = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "HH:mm");
                tv_return_date_my_trip.setText(date1);
            }

            else  if(select1.equalsIgnoreCase("start"))
            {


                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date1);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(datecurrent);
                //calendar.add(Calendar.HOUR, 2);
                calendar.add(Calendar.HOUR, 0);

                //  currentDateandTime.set
                if (calendar.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                    //  Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", error_layout);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", error_layout);

                }
              /*  if (calendar.getTime() > datenew.getTime()) {
                    tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                }*/
                else {
                    Log.d("System out", "selected1===" + select1);
                    start_date = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
                    start_time = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "HH:mm");
                    //tv_start_date.setText(start_date);
                    // tv_start_time.setText(start_time);
                    //  SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                    // Date date = null;
                    update_trip(pos_date, name);
                }
            }
            else  if(select_dpd_type.contains("return"))
            {
                tv_start_date_my_return_trip.setText( Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy"));
                tv_start_time_my_return_trip.setText(Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "hh:mm a"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void get_bitmap_from_freco(final String url) {


        Log.d("imageurl", url);
//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");
                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Bitmap bmp = BlurBuilder.blur(getActivity(), bitmap);

                            BitmapDrawable background = new BitmapDrawable(bmp);
//                            Drawable d = new BitmapDrawable(getResources(), bitmap);
                            //   ((ExploreActivity) getActivity()).drawerLayout.setBackground(background);
                            //  ((ExploreActivity) getActivity()).drawerLayout.setAlpha((float) 0.7);



                        }
                    });
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }
/*    public void set_adapter(String status) {
        try {

            myTripFileterlist.clear();
            myTripFileterlist = get_filter_list(status);
            myTripAdapter = new MyTripAdapter(getActivity(), myTripFileterlist);
            lv_mytrip_list.setAdapter(myTripAdapter);
            myTripAdapter.notifyDataSetChanged();

            if (myTripFileterlist.size() == 0) {

                no_trip_found_layout.setVisibility(View.VISIBLE);
                ll_main_hsv.setVisibility(View.GONE);
                load_pb.setVisibility(View.GONE);

                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);

            } else {

                no_trip_found_layout.setVisibility(View.GONE);
                ll_main_hsv.setVisibility(View.VISIBLE);
                load_pb.setVisibility(View.GONE);
                String url = "";
                if (myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName() != null) {
                    url = myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName();
                } else if (myTripFileterlist.get(0).getImageListByDestinationId().size() > 1) {
                    url = myTripFileterlist.get(0).getImageListByDestinationId().get(1).getImgName();
                }
                if (url.length() > 0)
                    //get_bitmap_from_freco(Constants.TimbThumb_ImagePath + url + "&width=" + (width + 200) + "&height=" + (width + 500));
                get_bitmap_from_freco(Constants.TimbThumb_ImagePath + url + "&height=" +height);
                else {
                    ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                }


            }
        } catch (Exception e) {
        }
    }*/

    public void set_adapter(String status)
    {
        try {


            myTripFileterlist.clear();
            myTripFileterlist = myTripArrayList;
            Log.d("System out","myTripArrayList_____"+myTripArrayList.size());
            Log.d("System out","myTripFileterlist_____"+myTripFileterlist.size());
            myTripAdapter = new MyTripAdapter(getActivity(), myTripFileterlist);

            lv_mytrip_list.setAdapter(myTripAdapter);

            myTripAdapter.notifyDataSetChanged();

            if (myTripFileterlist.size() == 0) {

                no_trip_found_layout.setVisibility(View.VISIBLE);
                ll_main_hsv.setVisibility(View.GONE);
                load_pb.setVisibility(View.GONE);


                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);


            } else {

                no_trip_found_layout.setVisibility(View.GONE);
                ll_main_hsv.setVisibility(View.VISIBLE);
                load_pb.setVisibility(View.GONE);

                String url = "";
                for(int i=0;i<myTripFileterlist.get(0).getImageListByDestinationId().size();i++)
                {
                    if (myTripFileterlist.get(0).getImageListByDestinationId().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                        url = myTripFileterlist.get(0).getImageListByDestinationId().get(i).getImgName();
                        break;
                    }

                }

                if(url.length()<=0) {
                    if (myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName() != null) {
                        url = myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName();
                    } else if (myTripFileterlist.get(0).getImageListByDestinationId().size() > 1) {
                        url = myTripFileterlist.get(0).getImageListByDestinationId().get(1).getImgName();
                    }
                }
               /* if (myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName() != null) {
                    url = myTripFileterlist.get(0).getImageListByDestinationId().get(0).getImgName();
                } else if (myTripFileterlist.get(0).getImageListByDestinationId().size() > 1) {
                    url = myTripFileterlist.get(0).getImageListByDestinationId().get(1).getImgName();
                }*/

                Log.d("status_trip_url", url);

                if (url.length() > 0)
                    //get_bitmap_from_freco(Constants.TimbThumb_ImagePath + url + "&width=" + (width + 200) + "&height=" + (width + 500));
                    //  get_bitmap_from_freco(Constants.TimbThumb_ImagePath + url + "&height=" + height);
                    ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);

                else {
                    ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
                }
                //((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);


            }
        } catch (Exception e) {
            e.toString();
        }
    }






    public void get_bitmap_from_freco_download(final String url, final String name, final String text) {
        Log.d("System out", "sharing image path " + url);


//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");
                    Log.d("System out", "bitmap " + (bitmap == null));
                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    dataSource.close();
                    DownloadCode code = new DownloadCode(getActivity(), Constants.TimbThumb_ImagePath, name, bitmap, text);
                    code.share_image_text_GPLUS();


                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }

    public static boolean CheckDates(String startDate, String endDate) {


        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = false;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare", String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }







    public void store_image(String waytype, String url) {


        if ( waytype.contains("RO"))
        {

            Bitmap icon = BitmapFactory.decodeResource(getResources(),
                    R.drawable.map_slider_icon4_selected);
            //  create_poi.setPoiImage(Constants.getImageBytes(icon));
            new ImageSaver(getActivity()).
                    setFileName("ro").
                    setExternal(true).
                    save(icon);


        }

        if (url.length() > 0)
        {

            if (!new ImageSaver(getActivity()).check_file_exists(url))
            {
                if (waytype.contains("Services"))
                {
                    url = Constants.ImagePath + url;
                } else {
                    url = Constants.TimbThumb_ImagePath + url + "&width=" + 50 + "&height=" + 50;

                }


                //stuff that updates ui
                ImageRequest imageRequest2 = ImageRequestBuilder
                        .newBuilderWithSource(Uri.parse(url))
                        .setAutoRotateEnabled(true)
                        .build();

                ImagePipeline imagePipeline2 = Fresco.getImagePipeline();
                final DataSource<CloseableReference<CloseableImage>>
                        dataSource2 = imagePipeline2.fetchDecodedImage(imageRequest2, this);


                final String finalUrl = url;
                dataSource2.subscribe(new BaseBitmapDataSubscriber() {

                    @Override
                    public void onNewResultImpl(@Nullable final Bitmap bitmap) {


                        new ImageSaver(getActivity()).
                                setFileName(finalUrl).

                                setExternal(true).
                                save(bitmap);


                        dataSource2.close();


                    }

                    @Override
                    public void onFailureImpl(DataSource dataSource) {
                        if (dataSource != null) {
                            dataSource.close();
                        }


                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                    }
                }, CallerThreadExecutor.getInstance());
            }
        }
    }

    public void Mytrip_Dialog(final int pos) {

        final MyTrip myTrip = myTripArrayList.get(pos);


        myTripDialog = new Dialog(getActivity());
        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_my_trips);
        tv_start_date_my_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_trip);
        tv_return_date_my_trip = (TextView) myTripDialog.findViewById(R.id.tv_return_date_my_trip);
        final TextView tv_for_better_dates = (TextView) myTripDialog.findViewById(R.id.tv_for_better_dates);
        final ImageView iv_swt_off_my_trip = (ImageView) myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
        final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        final EditText et_trip_name = (EditText) myTripDialog.findViewById(R.id.et_trip_name);
        TextView tv_save_my_trip = (TextView) myTripDialog.findViewById(R.id.tv_save_my_trip);


        et_trip_name.setText(myTrip.getTripName());



        Date date = null;
        start_date = "";
        start_time = "";
        end_date="";
        end_time="";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf.format(new Date());

        try {
            date = sdf.parse(currentDateandTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 0);
            dpd.setMinDate(calendar);

        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
        iv_swt_off_my_trip.setEnabled(false);
        tripFlag=true;

        ll_start_date_my_trip.setAlpha(1);
        ll_return_date_my_trip.setAlpha(1);
        tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select1="";
                select = "start";
                start_date = "";
                start_time = "";
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


            }

        });

        tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select1="";
                select = "stop";
                end_date = "";
                end_time = "";
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");


            }
        });




        iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tripFlag == false) {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                    tv_for_better_dates.setVisibility(View.GONE);
                    ll_start_date_my_trip.setAlpha(1);
                    ll_return_date_my_trip.setAlpha(1);
                    tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            select = "start";
                            start_date = "";
                            start_time = "";
                            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


                        }

                    });

                    tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            select = "stop";
                            end_date = "";
                            end_time = "";
                            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                            //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");


                        }
                    });


                    tripFlag = true;
                } else {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                    tv_for_better_dates.setVisibility(View.VISIBLE);
                    ll_start_date_my_trip.setAlpha((float) 0.6);
                    ll_return_date_my_trip.setAlpha((float) 0.6);
                    tripFlag = false;
                }

            }
        });

        tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_trip_name.getText().toString().length() > 0)
                {
                    if(tv_start_date_my_trip.getText().length() == 0)
                    {
                        Constants.show_error_popup(getActivity(), "Select your trip dates.", getActivity().findViewById(R.id.error_layout));
                    }
                    else   if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {

                        Constants.show_error_popup(getActivity(), "Select your trip dates.", getActivity().findViewById(R.id.error_layout));
                    }

                    else
                    {

                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;




                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;

                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(myTripArrayList.get(pos).getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);

                                    tripdate=sdf.format(calendar.getTime());
                                    Log.d("trip_Date_eta_c",tripdate.toString());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate))
                                {

                                    update_trip(pos,et_trip_name.getText().toString());
                                }
                                else {
                                    try {
                                        //Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(myTripArrayList.get(pos).getDuration().doubleValue()) + " reach at destination", getActivity().findViewById(R.id.error_layout));
                                       // Constants.show_error_popup(getActivity(), "Your destination requires" + Constants.convert_minute_hrs(myTripArrayList.get(pos).getDuration().doubleValue()) + " hours of travel. Plan accordingly. ", getActivity().findViewById(R.id.error_layout));
                                        Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule. ", getActivity().findViewById(R.id.error_layout));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. ", getActivity().findViewById(R.id.error_layout));
                                    }
                                }
                            }
                            else
                            {
                              //  Constants.show_error_popup(getActivity(), "Your destination requires " + Constants.convert_minute_hrs(myTripArrayList.get(pos).getDuration().doubleValue()) + " hours of travel. Plan accordingly. ", getActivity().findViewById(R.id.error_layout));
                                Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule. ", getActivity().findViewById(R.id.error_layout));
                            }
                        }
                        else {

                            update_trip(pos,et_trip_name.getText().toString());
                        }
                    }
                } else

                {
                    Constants.show_error_popup(getActivity(), "Please Enter Trip Name", getActivity().findViewById(R.id.error_layout));
                }
            }

        });
        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

    }


    public  void update_trip(final Integer pos,String tripname)
    {
        try {
            pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(false);
            pb_dialog.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String tripdate = start_date + " " + start_time + ":00";
            String tripenddate = "";
            String returndate = end_date + " " + end_time + ":00";
            String returndatetimeeta="";

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm:ss");
            Date date = null;

            try {
                date = sdf.parse(tripdate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(myTripArrayList.get(pos).getDuration());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);
                Log.d("trip_Date_eta", date.toString());
                tripenddate = sdf.format(calendar.getTime());
                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {
                e.printStackTrace();
                tripenddate="";

            }


            try {
                date = sdf.parse(returndate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(myTripArrayList.get(pos).getDuration());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);
                Log.d("trip_Date_eta", date.toString());
                returndatetimeeta = sdf.format(calendar.getTime());
                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {
                e.printStackTrace();
                returndatetimeeta="";

            }



            Log.d("start_date", returndate);
            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            try {
                returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                returndate = "";
            }

            try {
                returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();
                returndatetimeeta = "";

            }

            String json = "[{ \"TripName\": \"" + tripname + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + myTripArrayList.get(pos).getTripGoingToStart() + ",\"TripDestination_End\":" +myTripArrayList.get(pos).getTripDestinationEnd() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + myTripArrayList.get(pos).getTripId() +  "\",\"TripStatus\": \"" + "UpComing" +"\",\"TripGooglePlaceDetails\":\"" + "" + "\"}]";
            Log.d("json_CreateTrip1", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<UpdateTrip> call = apiService.updateTrip(json);
            final String finalReturndate = returndate;
            final String finalReturndatetimeeta = returndatetimeeta;
            call.enqueue(new Callback<UpdateTrip>() {
                @Override
                public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                    if (response.body() != null) {

                        pb_dialog.dismiss();
                        if (response.body().getResStatus() == true) {
                            dbHandler.update_Start_trip_date_time(myTripArrayList.get(pos).getTripId(), start_date, start_time);
                            dbHandler.update_Return_trip_date_time(myTripArrayList.get(pos).getTripId(), finalReturndate, finalReturndatetimeeta, true);
                            if (myTripDialog != null)
                                myTripDialog.dismiss();
                            // UpdateTrip_status("UpComing", pos);
                            //    getMyTrip(1);
                            Bundle b=new Bundle();
                            // b.putInt("TripId",tripid);
                            b.putInt("tab_pos", 2);

                            ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyTripWithSwipableFragment(), b);
                        }
                        else
                        {
                            tv_start_date.setText("DD MM,YYYY");
                            tv_start_time.setText("00:00");
                            Constants.show_error_popup(getActivity(), ""+response.body().getResDescription(), getActivity().findViewById(R.id.error_layout));
                        }
                    }
                }

                @Override
                public void onFailure(Call<UpdateTrip> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    pb_dialog.dismiss();

                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                    }

                }
            });
        }
        catch (Exception e){}
    }
    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }
    private void getMyTrip1(final Integer tripid) {
        // get & set progressbar dialog
        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{\"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",  \"PageNo\": 1,    \"PageSize\": 20,\"TripStatus\":\"OnGoing\" }]";
        Log.d("json_CreateTrip", json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<MyTrip>> call = apiService.GetMyTrip(json);
        call.enqueue(new Callback<ArrayList<MyTrip>>() {
            @Override
            public void onResponse(Call<ArrayList<MyTrip>> call, Response<ArrayList<MyTrip>> response) {
                if (response.body() != null) {


                    pb_dialog.dismiss();

                    if (response.body().get(0).getResStatus() == true && !trip_status.contains("OnGoing")) {//call OTP API here
                        Constants.show_error_popup(getActivity(), "You are already on a trip.", error_layout);
                    }
                    else {
                        start_myTrip_Dailog(tripid,false);
                        //Toast.makeText(getActivity(), "You are already on a trip. ", Toast.LENGTH_SHORT).show();
                    }

                }
                load_pb.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ArrayList<MyTrip>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                load_pb.setVisibility(View.GONE);
                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });

    }
    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);
        if (visible && isResumed()){   // only at fragment screen is resumed
            fragmentResume=true;
            fragmentVisible=false;
            fragmentOnCreated=true;
            myUIUpdate();

        }else  if (visible){        // only at fragment onCreated
            fragmentResume=false;
            fragmentVisible=true;
            fragmentOnCreated=true;
        }
        else if(!visible && fragmentOnCreated){// only when you go out of fragment screen
            fragmentVisible=false;
            fragmentResume=false;
        }
    }

    public void insert_trip_return_date(final Integer pos, final Boolean isdbsaved)
    {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {


                List<Create_Trip> createTripList=new ArrayList<>();
                createTripList   = dbHandler.get_TRIP_By_TripId(myTripArrayList.get(pos).getTripId());
                if(createTripList.size()>0) {

                    final Dialog myTripDialog = new Dialog(getActivity());

                    myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    myTripDialog.setContentView(R.layout.plan_trip_from_to_popup);

                    tv_start_date_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_return_trip);
                    tv_start_time_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_time_my_return_trip);

                    TextView tv_source = (TextView) myTripDialog.findViewById(R.id.tv_source);
                    TextView tv_des = (TextView) myTripDialog.findViewById(R.id.tv_des);
                    tv_source.setText(myTripArrayList.get(pos).getEndDesName());
                    tv_des.setText(myTripArrayList.get(pos).getOriginDesName());

                    String date = "", time = "";
                    dpd = DatePickerDialog.newInstance(
                            MyTripsFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );


                    tpd = TimePickerDialog.newInstance(
                            MyTripsFragment.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE), false
                    );


                    select_dpd_type = "return";
                    String arrival_date = "";
                    arrival_date = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime();


                    destinationplanList = new ArrayList<>();
                    destinationplanList = dbHandler.get_DestinationList(myTripArrayList.get(pos).getTripId());
                    if (destinationplanList.size() > 0) {
                        Calendar calendar = Calendar.getInstance();
                        try {
                            calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Double[] des_plan_date_time_eta = get_no_days_destination_plan();

                        calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

                        calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
                        calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

                        arrival_date = sdf.format(calendar.getTime());


                        Log.d("resumetimedparrival_hrs", des_plan_date_time_eta[1].intValue() + " min " + des_plan_date_time_eta[2].intValue());


                    }


                    Log.d("resumetimedparrival", arrival_date);
                    final String final_arrival_date = arrival_date;


                    Date datecurrent = null;
                    try {
                        datecurrent = sdf.parse(final_arrival_date);
                        Calendar calendar1 = Calendar.getInstance();
                        calendar1.setTime(datecurrent);

                        dpd.setMinDate(calendar1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");


                    tv_start_date_my_return_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //select="start";
                            select_dpd_type = "return";
                            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


                        }

                    });

                    tv_start_time_my_return_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //  select="stop";
                            select_dpd_type = "return";
                            tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");


                        }
                    });

                    final TextView tv_plan_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_plan_my_return_trip);
                    tv_plan_my_return_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            if (tv_start_date_my_return_trip.getText().toString().equalsIgnoreCase(""))

                            {
                                Constants.show_error_popup(getActivity(), "Select your trip dates.", tv_plan_my_return_trip);
                            } else if (tv_start_time_my_return_trip.getText().toString().equalsIgnoreCase("")) {
                                Constants.show_error_popup(getActivity(), "Please select trip start  time.", tv_plan_my_return_trip);
                            } else {

                                String start_date = "", end_time = "";

                                try {
                                    start_date = tv_start_date_my_return_trip.getText().toString();
                                    end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                                if (CheckDates(final_arrival_date, start_date + " " + end_time)) {

                                    if (!isdbsaved) {
                                        dbHandler.update_Return_trip_status(myTripArrayList.get(pos).getTripId());

                                        dbHandler.update_Return_trip_date_time(myTripArrayList.get(pos).getTripId(), start_date, end_time, false);
                                        Bundle b = new Bundle();
                                        b.putInt("tripId", myTripArrayList.get(pos).getTripId());
                                        b.putString("plantype", "DTOS");
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                                        myTripDialog.dismiss();
                                    }


                                } else {
                                    Constants.show_error_popup(getActivity(), "Please change trip return date time.", tv_plan_my_return_trip);
                                }
                            }


                        }
                    });
                    ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);

                    ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myTripDialog.dismiss();
                        }
                    });


                    myTripDialog.show();
                }
                else
                {
                    add_trip_in_to_database(pos, "DTOS",true);
                }
            }
        });

    }

    public Double[] get_no_days_destination_plan()
    {
        if (Constants.listDefaultValuesArrayList.size() > 0) {
            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

            {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

            }
        }
        Double[] destination_plan={0.0,0.0,0.0};
        Double total_time_stay_last=0.0;
        if (destinationplanList.size() > 0) {
            if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(1);

            for (int j = 1; j < destinationplanList.size(); j++) {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());


                if ((destinationplanList.get(j).getDepartureTime() - destinationplanList.get(0).getArrivalTime()) > dayhours) {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);
                    dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }


            }

        }

        ArrayList<Integer>    noday = new ArrayList<>();
        noday.clear();

        //  noday.add(1);

        Integer day = 0;


        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);

            // Log.d("days","pos"+String.valueOf(j)+"value"+noday.get(j).toString()+"v2"+String.valueOf(destinationplanList.get(i).getDay()));
            if (day != destinationplanList.get(i).getDay()) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());
            }

        }

        Integer last_day=0;

        if(noday.size()>0)
            last_day=noday.get(noday.size()-1);

        ArrayList<DestinationPlan> tempdestiDestinationPlanlist= new ArrayList<>();;

        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == last_day) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }

        if (tempdestiDestinationPlanlist.size() > 0) {
            if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
            } else {
                tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
            }


            tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
            tempdestiDestinationPlanlist.get(0).setDay(1);

            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempdestiDestinationPlanlist.get(0).getArrivalTime()) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }
            total_time_stay_last=tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size()-1).getDepartureTime();

            Log.d("total_time_stay_last",String.valueOf(total_time_stay_last));
        }

        destination_plan[0]=Double.valueOf(noday.size()); //day
        //int[] convertime = splitToComponentTimes(total_time_stay_last);
        destination_plan[1]=Double.valueOf(total_time_stay_last.intValue()); //hours
        BigDecimal bd = new BigDecimal((total_time_stay_last - Math.floor(total_time_stay_last)) * 100);
        bd = bd.setScale(4, RoundingMode.HALF_DOWN);

        destination_plan[2]=Double.valueOf(bd.doubleValue()); //minute



        return destination_plan;


    }

    public void copyTrip(final Integer tripid)
    {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{    \"TripId\": "+tripid+" }]";
        Log.d("json_CopyTrip", json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<CopyTrip> call = apiService.copytrip(json);
        call.enqueue(new Callback<CopyTrip>() {
            @Override
            public void onResponse(Call<CopyTrip> call, Response<CopyTrip> response) {
                if (response.body() != null) {


                    pb_dialog.dismiss();

                    if (response.body().getResStatus() == true) {//call OTP API here


                        Constants.show_error_popup_success(getActivity(), "Trip copied successfully under Saved Trips", error_layout);

                    }
                    else {

                        //Toast.makeText(getActivity(), "You are already on a trip. ", Toast.LENGTH_SHORT).show();
                    }

                }
                load_pb.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<CopyTrip> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                load_pb.setVisibility(View.GONE);
                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });

    }
    public void get_trip_in_to_database(Create_Trip create_trip) {


        // String   trip_return_end_date="",trip_return_end_time="",trip_end_date_eta="",trip_end_time_eta="";


        try {
            String tripdate = "", tripenddate = "", returndate = "", returndatetimeeta = "";
            tripdate = create_trip.getStartdate() + " " + create_trip.getStarttime() + ":00";
            tripenddate = create_trip.getEnddate() + " " + create_trip.getEndtime() + ":00";

            returndate = create_trip.getReturn_start_date() + " " + create_trip.getReturn_start_time() + ":00";


            returndatetimeeta = create_trip.getReturn_end_date() + " " + create_trip.getReturn_end_time() + ":00";
            Log.d("start_date", returndate);
            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss");

            } catch (ParseException e) {

                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            if (returndate.length() > 3) {
                try {
                    returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss");
                } catch (ParseException e) {

                    returndate = "";
                }
            }

            if (returndatetimeeta.length() > 3) {
                try {
                    returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss");

                } catch (ParseException e) {

                    returndatetimeeta = "";

                }
            }




            MyTrip myTrip=new MyTrip();
            myTrip.setOriginDesName(create_trip.getSource());
            myTrip.setEndDesName(create_trip.getDestination());




            myTrip.setTripDate(tripdate);
            myTrip.setTripEndDateTime(tripenddate);
            myTrip.setTripReturnDate(returndate);
            myTrip.setTripReturnDateETA(returndatetimeeta);





            myTrip.setTripName(create_trip.getTripname());



            myTrip.setTripStatus(create_trip.getTripstatus());

            myTrip.setTripId(create_trip.getTripId());
            myTrip.setTripGoingToStart(create_trip.getSourceId());
            myTrip.setTripDestinationEnd(create_trip.getDestinationId());
            myTrip.setTripIsOneSideCompleted(create_trip.getTripIsOneSideCompleted());




            List<Create_Poi> create_poiList=new ArrayList<>();

            create_poiList=dbHandler.get_POI_TRIP(create_trip.getTripId(),0);
            List<MyTrip.GetTripPlan > getTripPlanList=new ArrayList<>();

            MyTrip.GetTripPlan getTripPlan=new MyTrip().new GetTripPlan();
            getTripPlan.setResStatus(false);
            getTripPlan.setTripWayIsReturnPlanned(false);
            getTripPlan.setTripWayType("");
            getTripPlanList.add(getTripPlan);

            for(int i=0;i<create_poiList.size();i++)
            {
                getTripPlan=new MyTrip().new GetTripPlan();
                getTripPlan.setResStatus(true);
                getTripPlan.setTripWayIsReturnPlanned(false);
                getTripPlan.setTripWayType("POI");
                getTripPlanList.add(getTripPlan);
            }
            create_poiList.clear();
            create_poiList=dbHandler.get_POI_TRIP(create_trip.getTripId(),1);
            for(int i=0;i<create_poiList.size();i++)
            {
                getTripPlan=new MyTrip().new GetTripPlan();
                getTripPlan.setResStatus(true);
                getTripPlan.setTripWayIsReturnPlanned(true);
                getTripPlan.setTripWayType("POI");
                getTripPlanList.add(getTripPlan);
            }
            List<DestinationPlan> destinationPlanList=new ArrayList<>();
            destinationPlanList=dbHandler.get_DestinationList(create_trip.getTripId());
            for(int i=0;i<destinationPlanList.size();i++)
            {
                getTripPlan=new MyTrip().new GetTripPlan();
                getTripPlan.setResStatus(true);
                getTripPlan.setTripWayIsReturnPlanned(false);
                getTripPlan.setTripWayType("DesPlan");
                getTripPlanList.add(getTripPlan);
            }


            myTrip.setGetTripPlans(getTripPlanList);
            myTrip.setResStatus(true);
            myTripArrayList.add(myTrip);


        }
        catch (Exception e){}


    }
    public void add_trip_in_to_database(final int pos, final String plantype, final boolean returntype) {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        final SessionManager sessionManager1 = new SessionManager(getActivity());


        // String   trip_return_end_date="",trip_return_end_time="",trip_end_date_eta="",trip_end_time_eta="";


        try {
            if (myTripFileterlist.get(pos).getTripEndDateTime() != null) {
                trip_end_date_eta = Constants.formatDate(myTripFileterlist.get(pos).getTripEndDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                trip_end_time_eta = Constants.formatDate(myTripFileterlist.get(pos).getTripEndDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            } else {
                trip_end_date_eta = "";
                trip_end_time_eta = "";
            }
            if (myTripFileterlist.get(pos).getTripReturnDateETA() != null) {
                trip_return_end_date = Constants.formatDate(myTripFileterlist.get(pos).getTripReturnDateETA(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");

                trip_return_end_time = Constants.formatDate(myTripFileterlist.get(pos).getTripReturnDateETA(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            } else {
                trip_return_end_date = "";
                trip_return_end_time = "";
            }


        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_end_date = "";
            trip_return_end_time = "";
            trip_end_date_eta = "";
            trip_end_time_eta = "";
        }
        try {
            if (myTripFileterlist.get(pos).getTripDate() != null) {
                start_date = Constants.formatDate(myTripFileterlist.get(pos).getTripDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                start_time = Constants.formatDate(myTripFileterlist.get(pos).getTripDate(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            } else {
                start_date = "";
                start_time = "";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            start_date = "";
            start_time = "";
        }
        try {
            if (myTripFileterlist.get(pos).getTripReturnDate() != null) {
                end_date = Constants.formatDate(myTripFileterlist.get(pos).getTripReturnDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                end_time = Constants.formatDate(myTripFileterlist.get(pos).getTripReturnDate(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
            } else {
                end_date = "";
                end_time = "";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            end_date = "";
            end_time = "";
        }


        final String desurl = Constants.TimbThumb_ImagePath + myTripFileterlist.get(pos).getEndDesImage() + "&width=" + 50 + "&height=" + 50;
        String originurl = Constants.TimbThumb_ImagePath + myTripFileterlist.get(pos).getOrigindesImage() + "&width=" + 50 + "&height=" + 50;

        //stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(originurl))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    originbitmap = bitmap;

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    ImageRequest imageRequest1 = ImageRequestBuilder
                            .newBuilderWithSource(Uri.parse(desurl))
                            .setAutoRotateEnabled(true)
                            .build();

                    ImagePipeline imagePipeline1 = Fresco.getImagePipeline();
                    final DataSource<CloseableReference<CloseableImage>>
                            dataSource1 = imagePipeline1.fetchDecodedImage(imageRequest1, getActivity());


                    dataSource1.subscribe(new BaseBitmapDataSubscriber() {

                        @Override
                        public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                            if (dataSource1.isFinished() && bitmap != null) {
                                Log.d("Bitmap", "has come destination");
                                //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                Create_Trip create_trip = new Create_Trip();
                                create_trip.setSource(myTripFileterlist.get(pos).getOriginDesName());
                                create_trip.setDestination(myTripFileterlist.get(pos).getEndDesName());

                                create_trip.setStartdate(start_date);
                                create_trip.setStarttime(start_time);

                                create_trip.setReturn_start_date(end_date);
                                create_trip.setReturn_start_time(end_time);

                                create_trip.setEnddate(trip_end_date_eta);
                                create_trip.setEndtime(trip_end_time_eta);
                                create_trip.setReturn_end_date(trip_return_end_date);
                                create_trip.setReturn_end_time(trip_return_end_time);

                                if (end_date.length() > 1) {
                                    create_trip.setReturnstatus(true);
                                }


                                create_trip.setTripname(myTripFileterlist.get(pos).getTripName());
                                create_trip.setSource_latitude(Double.valueOf(myTripFileterlist.get(pos).getOriginLat()));
                                create_trip.setSource_longitude(Double.valueOf(myTripFileterlist.get(pos).getOriginLong()));
                                create_trip.setDestination_latitude(Double.valueOf(myTripFileterlist.get(pos).getEndDeslat()));
                                create_trip.setDestination_longitude(Double.valueOf(myTripFileterlist.get(pos).getEndDesLong()));


                                create_trip.setKM(myTripFileterlist.get(pos).getDistance() * 1000);
                                create_trip.setTime(myTripFileterlist.get(pos).getDuration().doubleValue());
                                create_trip.setTripstatus(myTripFileterlist.get(pos).getTripStatus());
                                create_trip.setTriprating("");
                                create_trip.setTripId(myTripFileterlist.get(pos).getTripId());
                                create_trip.setSourceId(myTripFileterlist.get(pos).getTripGoingToStart());
                                create_trip.setDestinationId(myTripFileterlist.get(pos).getTripDestinationEnd());
                                create_trip.setSource_image(Constants.getImageBytes(originbitmap));
                                create_trip.setDestination_image(Constants.getImageBytes(bitmap));
                                create_trip.setTripIsOneSideCompleted(myTripFileterlist.get(pos).getTripIsOneSideCompleted());
                                create_trip.setDesNoOfNearByAttraction(myTripFileterlist.get(pos).getEnddesNoOfNearByAttraction());

                                if(myTripFileterlist.get(pos).getTripReturnKm()!=null)
                                    create_trip.setReturnKM(Double.valueOf(myTripFileterlist.get(pos).getTripReturnKm()));
                                if(myTripFileterlist.get(pos).getTripReturnTime()!=null)
                                    create_trip.setReturnTime(Double.valueOf(myTripFileterlist.get(pos).getTripReturnTime()));



                                DBHandler dbHandler = new DBHandler(getActivity());

                                dbHandler.add_Trip_into_Table(create_trip);

                                Log.d("gettripplan", String.valueOf(myTripFileterlist.get(pos).getGetTripPlans().get(0).getResStatus()));
                                dataSource1.close();
                                if (myTripFileterlist.get(pos).getGetTripPlans().get(0).getResStatus()) {
                                    pb_dialog.dismiss();
                                    int i;
                                    for (i = 0; i < myTripFileterlist.get(pos).getGetTripPlans().size(); i++) {
                                        //  if(!myTripArrayList.get(pos).getGetTripPlans().get(i).getTripWayType().contains("PoiNight"))

                                        add_poi_plan1(myTripFileterlist.get(pos).getGetTripPlans().get(i), pos);
                                    }
                                    Bundle b = new Bundle();
                                    b.putInt("tripId", myTripFileterlist.get(pos).getTripId());
                                    if (plantype.equalsIgnoreCase("DES")) {
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyPlanFragment(), b);
                                    } else if (plantype.equalsIgnoreCase("tracking")) {
                                        getMyTrip1(myTripFileterlist.get(pos).getTripId());
                                    } else if (returntype) {
                                        insert_trip_return_date(pos, false);
                                    } else {
                                        b.putString("plantype", plantype);

                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                                    }



                                } else {
                                    pb_dialog.dismiss();
                                    if (plantype.equalsIgnoreCase("tracking"))
                                    {
                                        getMyTrip1(myTripFileterlist.get(pos).getTripId());
                                    } else if (returntype) {
                                        insert_trip_return_date(pos, false);
                                    } else {
                                        Bundle b = new Bundle();
                                        b.putString("plantype", plantype);
                                        b.putInt("tripId", myTripFileterlist.get(pos).getTripId());
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailureImpl(DataSource dataSource) {
                            if (dataSource != null) {
                                dataSource.close();
                            }

                            pb_dialog.dismiss();
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                        }
                    }, CallerThreadExecutor.getInstance());

                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                pb_dialog.dismiss();
                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));

            }
        }, CallerThreadExecutor.getInstance());


    }

    public void add_poi_plan1(final MyTrip.GetTripPlan getTripPlan, final int pos) {
        if (getTripPlan.getGeneralFields().getImage().length() <= 1)
            getTripPlan.getGeneralFields().setImage("");
        if (getTripPlan.getTripWayType().contains("PoiNight"))

        {
            Create_Poi_Night create_poi = new Create_Poi_Night();
            create_poi.setPoiId(getTripPlan.getTripDesId());
            create_poi.setTripId(myTripFileterlist.get(pos).getTripId());
            create_poi.setPoiName(getTripPlan.getGeneralFields().getName());

            create_poi.setStayTime(getTripPlan.getTripSpendTime());
            create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
            create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
            create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() * 1000));
            create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));
            create_poi.setPoiStay(false);
            create_poi.setArrivalTime(0.0);
            create_poi.setDepartureTime(0.0);

            //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));
            create_poi.setPoitype("PoiNight");
            create_poi.setWayPointId(getTripPlan.getTripWayPointId());

            create_poi.setPoiDesID(getTripPlan.getTripWayRefId());
            create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

            create_poi.setReturnstatus(getTripPlan.getTripWayIsReturnPlanned());
            create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());
            create_poi.setImagename(getTripPlan.getGeneralFields().getImage());


            dbHandler.add_Night_POI_into_Table(create_poi);


        } else if (getTripPlan.getTripWayType().contains("DesPlan")) {
            DestinationPlan create_poi = new DestinationPlan();
            create_poi.setTripId(myTripFileterlist.get(pos).getTripId());


            create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() * 1000));
            create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));


            create_poi.setArrivalTime(0.0);
            create_poi.setDepartureTime(0.0);


            create_poi.setPoiName(getTripPlan.getGeneralFields().getName());
            create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
            create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
            create_poi.setStayTime(getTripPlan.getTripSpendTime());
            create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());

            create_poi.setPoiDesID(getTripPlan.getTripWayRefId());
            create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

            create_poi.setPoitype("DesPlan");
            create_poi.setWayPointId(getTripPlan.getTripWayPointId());


            create_poi.setImagename(getTripPlan.getGeneralFields().getImage());


            if (getTripPlan.getGeneralFields().getPOIIsFullTime()) {
                create_poi.setStarttime(24.00);
                create_poi.setEndtime(24.00);
            } else {
                String starttime = "0:0", endtime = "0:0";

                try {
                    starttime = Constants.formatDate(getTripPlan.getGeneralFields().getPOIStartTime(), "HH:mm:ss", "HH:mm");
                    endtime = Constants.formatDate(getTripPlan.getGeneralFields().getPOIEndTime(), "HH:mm:ss", "HH:mm");

                } catch (ParseException e) {
                    e.printStackTrace();
                    starttime = "0:0";
                    endtime = "0:0";
                }


                create_poi.setStarttime(Double.valueOf(starttime.replace(":", ".")));
                create_poi.setEndtime(Double.valueOf(endtime.replace(":", ".")));
            }


            dbHandler.add_Destination_into_Table(create_poi);
        } else {

            Log.d("Bitmap", "has come poi");
            Create_Poi create_poi = new Create_Poi();
            create_poi.setTripId(myTripFileterlist.get(pos).getTripId());
            create_poi.setPoiName(getTripPlan.getGeneralFields().getName());
            create_poi.setStayTime(getTripPlan.getTripSpendTime());
            create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
            create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
            create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() * 1000));
            create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));
            create_poi.setPoiStay(false);
            create_poi.setArrivalTime(0.0);
            create_poi.setDepartureTime(0.0);


            create_poi.setPoitype(getTripPlan.getTripWayType());
            create_poi.setWayPointId(getTripPlan.getTripWayPointId());

            create_poi.setPoiDesID(getTripPlan.getTripWayRefId());

            if (getTripPlan.getTripWayType().contains("Services"))
                create_poi.setPoiServerId(getTripPlan.getSerCategoryId());
            else
                create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

            // create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

            create_poi.setReturnstatus(getTripPlan.getTripWayIsReturnPlanned());
            create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());
            if (getTripPlan.getTripWayType().contains("RO")) {
                create_poi.setImagename("ro");
                create_poi.setPoiside(getTripPlan.getGeneralFields().getSide());
            }
            else if (getTripPlan.getTripWayType().contains("Services")) {
                create_poi.setImagename(getTripPlan.getGeneralFields().getSerCatFooterImageSelected());
                create_poi.setPoiside(getTripPlan.getGeneralFields().getSide());
            }
            else
                create_poi.setImagename(getTripPlan.getGeneralFields().getImage());

            if (getTripPlan.getTripWayType().contains("DES"))
                create_poi.setAttractionCount(getTripPlan.getDesNoOfNearByAttraction());

            if (getTripPlan.getTripLegNo() != null)
                create_poi.setLegNo(Integer.valueOf(getTripPlan.getTripLegNo()));

            String resume_date = "", resume_time = "";


            try {
                if (getTripPlan.getTripResumeDateTime() != null) {
                    resume_date = Constants.formatDate(getTripPlan.getTripResumeDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                    resume_time = Constants.formatDate(getTripPlan.getTripResumeDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                } else {
                    resume_date = "";
                    resume_time = "";
                }

            } catch (ParseException e) {
                e.printStackTrace();
                resume_date = "";
                resume_time = "";
            }


            if (resume_date.length() > 1)
                create_poi.setPoiStay(true);
            create_poi.setResumeDate(resume_date);
            create_poi.setResumeTime(resume_time);

            dbHandler.add_POI_into_Table(create_poi);
        }

        String url = "";
        if (getTripPlan.getTripWayType().contains("RO"))
            url = "ro";
        else if (getTripPlan.getTripWayType().contains("Services"))
            url = getTripPlan.getGeneralFields().getSerCatFooterImageSelected();
        else
            url = getTripPlan.getGeneralFields().getImage();

        store_image(getTripPlan.getTripWayType().toString(), url);


    }



   /* public void start_myTrip_Dailog(final int pos, Boolean returntrip) {
        startMyTripDialog = new Dialog(getActivity());
        startMyTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        startMyTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        startMyTripDialog.setContentView(R.layout.pop_up_start_tracking);
        TextView tv_start_tracking = (TextView) startMyTripDialog.findViewById(R.id.tv_start_tracking);
        TextView tv_trip_start_text = (TextView) startMyTripDialog.findViewById(R.id.tv_trip_start_text);
        tv_trip_start_text.setText("It Seems you have not started \n your trip,\n  Would you like to Start?");
        ImageView ImgClosepopup = (ImageView) startMyTripDialog.findViewById(R.id.ImgClosepopup);
        startMyTripDialog.show();
        tv_start_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gpsTracker = new GPSTracker(getActivity());
                if (gpsTracker.isGPSEnabled) {
                    startMyTripDialog.dismiss();

                    UpdateTrip_status("OnGoing", pos);

                } else {
                    gpsTracker.showSettingsAlertDialog();
                }

               *//* Bundle b=new Bundle();
             //  b.putInt("tripId",myTripsList.get(position).getTripId());
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(),b);*//*
            }


        });
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyTripDialog.dismiss();
            }
        });


    }


    public static boolean CheckDates1(String startDate, String endDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
        boolean b = false;

        try {
            Log.d("checkdate", dfDate.parse(startDate.toString()) + "enddate " + dfDate.parse(endDate.toString()) +"  "+dfDate.parse(startDate).before(dfDate.parse(endDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = false;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = true; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("System out", "datecompare in checkdate 1:"+String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }
    public void UpdateTrip_status(final String status, final Integer pos) {
        // get & set progressbar dialog


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = df.format(c.getTime());

        String json = "[{     \"TripId\": \"" + pos + "\", \"TripUmID\":\"" + sessionManager.get_Authenticate_User().getUmId() + "\",    \"TripStatus\": \"" + status + "\"}]";


        // String json = "[{\"TripName\": \""+myTripArrayList.get(pos).getTripName()+"\",\"TripDate\":\""+formattedDate+"\", \"TripGoingTo_Start\": \""+myTripArrayList.get(pos).getTripGoingToStart()+"\",\"TripDestination_End\":\""+myTripArrayList.get(pos).getTripDestinationEnd()+"\", \"TripEndDateTime\":\"\", \"TripUmID\":\""+sessionManager.get_Authenticate_User().getUmId()+"\", \"TripCurrentLatitude\":\""+myTripArrayList.get(pos).getOriginLat()+"\", \"TripCurrentLongitude\":\""+myTripArrayList.get(pos).getOriginLong()+"\", \"TripCurrentLocation\":\""+myTripArrayList.get(pos).getUmLocation()+"\", \"TripRoute\":2, \"TripId\": "+myTripArrayList.get(pos).getTripId()+",\"TripStatus\": \"OnGoing\" }]";
        Log.d("json_UpdateTrip", json);
        Call<ArrayList<UpdateTrip>> call = apiService.updateTripstatus(json);
        call.enqueue(new Callback<ArrayList<UpdateTrip>>() {
            @Override
            public void onResponse(Call<ArrayList<UpdateTrip>> call, Response<ArrayList<UpdateTrip>> response) {
                pb_dialog.dismiss();
                if (response.body() != null)
                {


                    updateTripList.addAll(response.body());

                    if (status.contains("OnGoing"))
                    {
                        Calendar calendar = Calendar.getInstance();
                        List<Create_Trip> createTripList = new ArrayList<Create_Trip>();
                        createTripList = dbHandler.get_TRIP_By_TripId(pos);
                        Calendar calendar1=Calendar.getInstance();
                        String currentdate = sdf.format(calendar1.getTime());

                        if(!CheckDates1(currentdate,createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime()))
                        {


                            dbHandler.update_trip_one_way_completed1(createTripList.get(0).getTripId(), "OnGoing", 0);
                            dbHandler.update_Start_trip_date_time_current(pos, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()), 0);
                            preferences.edit().putString("DirectionJson", "").commit();
                            Bundle b = new Bundle();
                            b.putInt("tripId", pos);
                            ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);

                        }
                        else
                        {
                            if(createTripList.get(0).getReturnstatus() )
                            {
                                dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), "OnGoing",0);
                                dbHandler.update_Start_trip_date_time_current(pos, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()), 1);
                                preferences.edit().putString("DirectionJson", "").commit();
                                Bundle b = new Bundle();
                                b.putInt("tripId", pos);
                                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);
                            }
                            else
                            {
                                Constants.show_error_popup(getActivity(), "No return plan made.", error_layout);
                            }
                        }

                    }
                    else {
                        // trip_status="UpComing";
                        getMyTrip(1);
                    }


                }
                load_pb.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ArrayList<UpdateTrip>> call, Throwable t) {
                // Log error here since request failed


                pb_dialog.dismiss();
                t.printStackTrace();
                load_pb.setVisibility(View.GONE);

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);

                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });

    }*/
   public void start_myTrip_Dailog(final int pos, Boolean returntrip)
   {


       Calendar calendar = Calendar.getInstance();
       List<Create_Trip> createTripList = new ArrayList<Create_Trip>();

       createTripList = dbHandler.get_TRIP_By_TripId(pos);
       if(trip_status.contains("UpComing"))
       {
           dbHandler.update_trip_one_way_completed1(createTripList.get(0).getTripId(), "UpComing", 0);
           createTripList = dbHandler.get_TRIP_By_TripId(pos);
       }

       Calendar calendar1=Calendar.getInstance();
       String currentdate = sdf.format(calendar1.getTime());

       if(createTripList.get(0).getTripIsOneSideCompleted())
       {
           if(createTripList.get(0).getReturnstatus() )
           {
               returnPlan=1;
               startTripDialog(pos,returnPlan);
           }
           else
           {
               Constants.show_error_popup(getActivity(), "No return plan made.", error_layout);
           }
       }
       else
       {
           if(!CheckDates1(currentdate,createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime()))
           {

               //let go
               returnPlan=0;
               startTripDialog(pos,returnPlan);

           }

           else
           {
               if(createTripList.get(0).getReturnstatus()==false )
               {
                   returnPlan=0;
                   startTripDialog(pos,returnPlan);

               }
               else
               {
                   startMyTripDialog = new Dialog(getActivity());
                   startMyTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                   startMyTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                   startMyTripDialog.setContentView(R.layout.pop_up_start_tracking);
                   TextView tv_start_tracking = (TextView) startMyTripDialog.findViewById(R.id.tv_start_tracking);
                   TextView tv_trip_start_text = (TextView) startMyTripDialog.findViewById(R.id.tv_trip_start_text);
                   if(returnPlan==0) {
                       tv_trip_start_text.setText("Would you like to start your trip?");
                   }else {
                       tv_trip_start_text.setText("Would you like to start your return trip?");
                   }
                   ImageView ImgClosepopup = (ImageView) startMyTripDialog.findViewById(R.id.ImgClosepopup);
                   startMyTripDialog.show();
                   tv_start_tracking.setVisibility(View.GONE);

                   LinearLayout ll_selectTrip=(LinearLayout)startMyTripDialog.findViewById(R.id.ll_selectTrip);
                   TextView tv_oneway_tracking=(TextView)startMyTripDialog.findViewById(R.id.tv_oneway_tracking);
                   TextView tv_returnway_tracking=(TextView)startMyTripDialog.findViewById(R.id.tv_return_tracking);


                   ll_selectTrip.setVisibility(View.VISIBLE);


                   tv_oneway_tracking.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {


                           gpsTracker = new GPSTracker(getActivity());
                           if (gpsTracker.isGPSEnabled)
                           {
                               startMyTripDialog.dismiss();
                               returnPlan=0;
                               UpdateTrip_status("OnGoing", pos);

                           }
                           else {
                               gpsTracker.showSettingsAlertDialog();
                           }

               /* Bundle b=new Bundle();
             //  b.putInt("tripId",myTripsList.get(position).getTripId());
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(),b);*/
                       }


                   });
                   tv_returnway_tracking.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {


                           gpsTracker = new GPSTracker(getActivity());
                           if (gpsTracker.isGPSEnabled)
                           {
                               startMyTripDialog.dismiss();
                               returnPlan=1;
                               UpdateTrip_status("OnGoing", pos);

                           }
                           else {
                               gpsTracker.showSettingsAlertDialog();
                           }

               /* Bundle b=new Bundle();
             //  b.putInt("tripId",myTripsList.get(position).getTripId());
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(),b);*/
                       }


                   });
                   ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           startMyTripDialog.dismiss();
                       }
                   });
               }

           }

       }







   }

    public  void startTripDialog(final Integer tripId, Integer plan)
    {
        startMyTripDialog = new Dialog(getActivity());
        startMyTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        startMyTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        startMyTripDialog.setContentView(R.layout.pop_up_start_tracking);
        TextView tv_start_tracking = (TextView) startMyTripDialog.findViewById(R.id.tv_start_tracking);
        TextView tv_trip_start_text = (TextView) startMyTripDialog.findViewById(R.id.tv_trip_start_text);
        if(plan==0) {
            tv_trip_start_text.setText("Would you like to start your trip?");
        }else {
            tv_trip_start_text.setText("Would you like to start your return trip?");
        }
        ImageView ImgClosepopup = (ImageView) startMyTripDialog.findViewById(R.id.ImgClosepopup);
        startMyTripDialog.show();
        tv_start_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gpsTracker = new GPSTracker(getActivity());
                if (gpsTracker.isGPSEnabled)
                {
                    startMyTripDialog.dismiss();

                    UpdateTrip_status("OnGoing", tripId);

                }
                else {
                    gpsTracker.showSettingsAlertDialog();
                }

               /* Bundle b=new Bundle();
             //  b.putInt("tripId",myTripsList.get(position).getTripId());
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(),b);*/
            }


        });
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyTripDialog.dismiss();
            }
        });
    }


    public void UpdateTrip_status(final String status, final Integer pos)
    {
        // get & set progressbar dialog

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String formattedDate = df.format(c.getTime());

        String json = "[{     \"TripId\": \"" + pos + "\", \"TripUmID\":\"" + sessionManager.get_Authenticate_User().getUmId() + "\",    \"TripStatus\": \"" + status + "\"}]";


        // String json = "[{\"TripName\": \""+myTripArrayList.get(pos).getTripName()+"\",\"TripDate\":\""+formattedDate+"\", \"TripGoingTo_Start\": \""+myTripArrayList.get(pos).getTripGoingToStart()+"\",\"TripDestination_End\":\""+myTripArrayList.get(pos).getTripDestinationEnd()+"\", \"TripEndDateTime\":\"\", \"TripUmID\":\""+sessionManager.get_Authenticate_User().getUmId()+"\", \"TripCurrentLatitude\":\""+myTripArrayList.get(pos).getOriginLat()+"\", \"TripCurrentLongitude\":\""+myTripArrayList.get(pos).getOriginLong()+"\", \"TripCurrentLocation\":\""+myTripArrayList.get(pos).getUmLocation()+"\", \"TripRoute\":2, \"TripId\": "+myTripArrayList.get(pos).getTripId()+",\"TripStatus\": \"OnGoing\" }]";
        Log.d("json_UpdateTrip", json);
        Call<ArrayList<UpdateTrip>> call = apiService.updateTripstatus(json);
        call.enqueue(new Callback<ArrayList<UpdateTrip>>() {
            @Override
            public void onResponse(Call<ArrayList<UpdateTrip>> call, Response<ArrayList<UpdateTrip>> response) {
                pb_dialog.dismiss();
                if (response.body() != null)
                {


                    updateTripList.addAll(response.body());

                    if (status.contains("OnGoing"))
                    {



                        Calendar calendar = Calendar.getInstance();
                        List<Create_Trip> createTripList = new ArrayList<Create_Trip>();
                        createTripList = dbHandler.get_TRIP_By_TripId(pos);
                        Calendar calendar1=Calendar.getInstance();
                        String currentdate = sdf.format(calendar1.getTime());


                        if(returnPlan==0)
                        {

                            //upTrip
                            dbHandler.update_trip_one_way_completed1(createTripList.get(0).getTripId(), "OnGoing", 0);
                            dbHandler.update_Start_trip_date_time_current(pos, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()), 0);
                            preferences.edit().putString("DirectionJson", "").commit();
                            preferences.edit().putBoolean("trip_start_tracking", true).commit();
                            preferences.edit().putInt("tripId", pos).commit();
                            Bundle b = new Bundle();
                            b.putInt("tripId", pos);
                            ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);

                        }


                        else
                        {
                            //return Trip
                            if(returnPlan==1 )
                            {
                                dbHandler.update_trip_one_way_completed(createTripList.get(0).getTripId(), "OnGoing",0);
                                dbHandler.update_Start_trip_date_time_current(pos, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()), 1);
                                preferences.edit().putString("DirectionJson", "").commit();
                                preferences.edit().putBoolean("trip_start_tracking", true).commit();
                                preferences.edit().putInt("tripId", pos).commit();
                                Bundle b = new Bundle();
                                b.putInt("tripId", pos);
                                ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new TrackRoutePlanningFragment(), b);
                            }
                            else
                            {
                                Constants.show_error_popup(getActivity(), "No return plan made.", error_layout);
                            }
                        }





                    }
                    else {
                        // trip_status="UpComing";
                        getMyTrip(1);
                    }


                }
                load_pb.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ArrayList<UpdateTrip>> call, Throwable t) {
                // Log error here since request failed


                pb_dialog.dismiss();
                t.printStackTrace();
                load_pb.setVisibility(View.GONE);

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);

                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });

    }
    public static boolean CheckDates1(String startDate, String endDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
        boolean b = false;

        try {
            Log.d("checkdate", dfDate.parse(startDate.toString()) + "enddate " + dfDate.parse(endDate.toString()) +"  "+dfDate.parse(startDate).before(dfDate.parse(endDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = false;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = true; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("System out", "datecompare in checkdate 1:"+String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }


    public  void tripCompltedDialog(final int tripid)
    {
        myTripDialog = new Dialog(getActivity());

        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_start_tracking);
        myTripDialog.setCancelable(false);

        TextView tv_complete_msg = (TextView) myTripDialog.findViewById(R.id.tv_trip_start_text);

        tv_complete_msg.setText("Are you sure you want to complete up trip?");

        TextView tv_yes = (TextView) myTripDialog.findViewById(R.id.tv_start_tracking);
        tv_yes.setText("Yes");
        //tv_yes.setText("Yes");
        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myTripDialog.dismiss();
                tripCompltedDialog(tripid);



                //android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);

        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                myTripDialog.dismiss();
            }
        });

        myTripDialog.show();



    }





    public void tripCompleted( final Integer tripid)
    {
        // get & set progressbar dialog
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{     \"TripId\": \"" + tripid + "\", \"TripUmID\":\"" + sessionManager.get_Authenticate_User().getUmId() + "\",    \"TripStatus\": \"" + "Completed" + "\"}]";



        Call<ArrayList<UpdateTrip>> call = apiService.updateTripstatus(json);
        call.enqueue(new Callback<ArrayList<UpdateTrip>>() {
            @Override
            public void onResponse(Call<ArrayList<UpdateTrip>> call, Response<ArrayList<UpdateTrip>> response) {
                pb_dialog.dismiss();
                if (response.body() != null)
                {
                    if(response.body().get(0).getResStatus())
                    {

                        dbHandler.update_trip_status(tripid,"Completed");
                        ((ExploreActivity)getActivity()).replace_fragmnet(new DiscoverFragment());

                    }

                }
                else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }


            }

            @Override
            public void onFailure(Call<ArrayList<UpdateTrip>> call, Throwable t) {
                // Log error here since request failed


                pb_dialog.dismiss();
                t.printStackTrace();


                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);

                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });

    }





}


