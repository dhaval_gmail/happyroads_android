package com.bpcl.happyroads;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpcl.happyroads.Utils.Constants;

public class StartTripActivity extends Activity {

    Integer tripid;
    SharedPreferences preferences;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pop_up_start_tracking);
      /*  final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_up_start_tracking);
        final Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/
        preferences = getSharedPreferences(Constants.TripTrackPREFERENCES, 0);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        Log.i("system","starttripactivity tripid bit is--"+getIntent().getIntExtra("tripid",0));
        if(getIntent().getIntExtra("tripid",0)!=0)
        {
            tripid=getIntent().getIntExtra("tripid",0);
        Log.i("system","get intent starttripactivity tripid bit is--"+getIntent().getIntExtra("tripid",0));
        }
        Log.d("tripid",tripid.toString());
        TextView tv_trip_start_text = (TextView) findViewById(R.id.tv_trip_start_text);
        ImageView ivclose=(ImageView)findViewById(R.id.ImgClosepopup);
        TextView tv_start_tracking = (TextView) findViewById(R.id.tv_start_tracking);

        tv_trip_start_text.setText("It Seems you are \n On the way, \n Would you like to start Tracking?");

        tv_start_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent intent=new Intent(StartTripActivity.this,ExploreActivity.class);
                intent.putExtra("tripid",tripid);
                preferences.edit().putInt("tripid",tripid).commit();
                preferences.edit().putBoolean("trip_start_tracking", true).commit();
                startActivity(intent);
                finish();
            }
        });

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
