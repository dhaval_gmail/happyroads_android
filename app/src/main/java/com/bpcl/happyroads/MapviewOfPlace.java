package com.bpcl.happyroads;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.bpcl.happyroads.Pojo.Direction;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DirectionsJSONParser;
import com.bpcl.happyroads.Utils.GPSTracker;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 20/01/2017.
 */
public class MapviewOfPlace extends FragmentActivity implements OnMapReadyCallback {
    GoogleMap map = null;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 125;
    static final LatLng myLocation = new LatLng(23.034469, 72.501988);
    SupportMapFragment supportMapFragment;
    GPSTracker gpsTracker;
    Location location;
   // View v;
    String poiLat = "";
    String poiLong = "";
    String name = "";
    private String url = "";
    List<List<HashMap<String, String>>> routelineresult;
    LatLngBounds.Builder b1;
    Polyline polylineFinal;
    TextView tv_name_place_map;
    ImageView iv_close_add_vehicle;
    static final com.mapbox.mapboxsdk.geometry.LatLng source = new com.mapbox.mapboxsdk.geometry.LatLng(23.034617, 72.501813);
    static final com.mapbox.mapboxsdk.geometry.LatLng destination = new com.mapbox.mapboxsdk.geometry.LatLng(23.1138, 72.5412);

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.map_view_of_place_fragment);

      /*  if(v==null) {

            v = inflater.inflate(R.layout.map_view_of_place_fragment, container, false);
        }*/
        if (Build.VERSION.SDK_INT >= 23) {
            check_permission_marshmallow();
        } else {
            ViewMap();
        }
        gpsTracker = new GPSTracker(MapviewOfPlace.this);
        location = gpsTracker.getLocation();
        Bundle bundle = new Bundle();
        bundle=getIntent().getExtras();

        b1 = new LatLngBounds.Builder();

        tv_name_place_map=(TextView)findViewById(R.id.tv_name_place_map);
        iv_close_add_vehicle = (ImageView)findViewById(R.id.iv_close_add_vehicle);

       /* ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);*/
        if (bundle != null) {
            poiLat = bundle.getString("poiLat");
            poiLong = bundle.getString("poiLong");
            name = bundle.getString("name");
        }
        //((ExploreActivity)getActivity()).tv_text_header.setText(name);
        tv_name_place_map.setText(name);
        if(location!=null) {
            source.setLatitude(location.getLatitude());
            source.setLongitude(location.getLongitude());
            destination.setLatitude(Double.parseDouble(poiLat));
            destination.setLongitude(Double.parseDouble(poiLong));
            url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, "");


        }else
        {
            Toast.makeText(MapviewOfPlace.this,"Location not found.",Toast.LENGTH_LONG).show();
        }
        iv_close_add_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // ((ExploreActivity)getActivity()).onBackPressed();
                finish();



            }
        });

       // return v;
    }
    private void check_permission_marshmallow() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Access Content");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                });
                return;

            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);

            return;
        }
      ViewMap();
        //else open ur
    }
    public void ViewMap() {
      /*  FragmentManager fm = getChildFragmentManager();
       ((SupportMapFragment) fm.findFragmentById(R.id.map_home1)).getMapAsync(MapviewOfPlace.this);*/

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_home1);
        fm.getMapAsync(MapviewOfPlace.this);


       // ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map_home1)).getMapAsync(MapviewOfPlace.this);
    }
    private void getRoute(String url) {

        ApiInterface apiService = ApiClient.get_retrofit_client_google().create(ApiInterface.class);
        Call<Direction> call = apiService.GetRoute(url);
        call.enqueue(new Callback<Direction>() {
            @Override
            public void onResponse(Call<Direction> call, Response<Direction> response)
            {
                if (response.body() != null)
                {


                    try {

                        if (response.body().getRoutes().size() > 0)
                        {

                            DirectionsJSONParser parser = new DirectionsJSONParser();

                            // Starts parsing data
                            routelineresult=new ArrayList<List<HashMap<String, String>>>();
                            Gson gson = new GsonBuilder().create();

                            String routeresponse = gson.toJson(response.body());
                            routelineresult = parser.parse(new JSONObject(routeresponse));

                            draw_route();


                        }
                        else {

                        //   Constants.show_error_popup(getActivity(), "Sorry! Route not found try again", iv_bottom_up_arrow);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<Direction> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();

                if (!Constants.isInternetAvailable(MapviewOfPlace.this)) {
                  //  Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    //Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });

    }
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (MapviewOfPlace.this.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MapviewOfPlace.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        Log.d("Sytem out","url mapviewofplace"+url);

            map.addMarker(new MarkerOptions().snippet("point").position(new LatLng(source.getLatitude(),source.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.round_blue_map)));
            map.addMarker(new MarkerOptions().snippet("point").position(new LatLng(destination.getLatitude(),destination.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.round_red_map)));



        getRoute(url);
//        draw_route();
    }

    public void draw_route()
    {
        if (routelineresult != null && map != null)

        {
            ArrayList<LatLng> points = new ArrayList<>();
            PolylineOptions lineOptions = new PolylineOptions();
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < routelineresult.size(); i++)
            {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = routelineresult.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    b1.include(position);
                    points.add(position);
                    //  Constants.points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(MapviewOfPlace.this.getResources().getColor(R.color.colorAccent));
            }
            LatLngBounds bounds = b1.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);

            polylineFinal=map.addPolyline(lineOptions);
            map.animateCamera(cu);
        }
    }

  /*  @Override
    public void onDestroy() {
        super.onDestroy();
        ((ExploreActivity)getActivity()).collapsing_toolbar.setVisibility(View.VISIBLE);

    }*/
}

