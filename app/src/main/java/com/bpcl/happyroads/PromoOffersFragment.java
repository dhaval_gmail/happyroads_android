package com.bpcl.happyroads;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class PromoOffersFragment extends Fragment
{
    TextView tv_text;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.no_notification, container, false);
       // ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Promos/Offers");
        tv_text=(TextView)v.findViewById(R.id.tv_text);
        tv_text.setText("We are working hard to bring awesome offers just for you!!! We will be in touch.");


        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Promos/Offers");

        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);

    }
}
