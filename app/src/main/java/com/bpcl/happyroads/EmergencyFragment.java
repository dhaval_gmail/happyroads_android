package com.bpcl.happyroads;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class EmergencyFragment extends Fragment{
    TextView tv_number_emergency;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.emergency_fragment, container, false);
        tv_number_emergency=(TextView)v.findViewById(R.id.tv_number_emergency);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).  exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Road-Side Assistance");
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }
}
