package com.bpcl.happyroads;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.ImageList;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by ADMIN on 8/23/2016.
 */
public class GallaryFragment extends Fragment {
    ArrayList<String> photoGallary = new ArrayList<>();

    Gson gson=new Gson();
    String selectedPosition="";
    String posi="";
    String from="";
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    Gallery imageView,gallery;
    static int width ;
    int height;
    int pos=0;
    TextView tv_photo_cc,tv_photo_by;
    ArrayList<ExploreDestination> exploreDestList=new ArrayList<>();
    ArrayList<ExplorePOI> explorePoiList=new ArrayList<>();
    ArrayList<ImageList> selectedimagelist=new ArrayList<>();
    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gallary_fragment, container, false);
  gallery = (Gallery)v.findViewById(R.id.gallery1);
        imageView = (Gallery) v.findViewById(R.id.image1);

        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        //  ((ExploreActivity)getActivity()).ivMenuHeader.setImageResource(R.drawable.close_btn_popup);
        ((ExploreActivity)getActivity()).ll_gallary_count.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_close_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_menu_header.setVisibility(View.GONE);
        Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));
        Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();
        tv_photo_by=(TextView)v.findViewById(R.id.tv_photo_by);
        tv_photo_cc=(TextView)v.findViewById(R.id.tv_photo_cc);

        width  = mDisplay.getWidth();
        height = mDisplay.getHeight();
        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            selectedPosition = bundle.getString("position");
            from = bundle.getString("from");
        }
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        if(from.equalsIgnoreCase("DestListing")) {

            String jsonOutput = sharedpreferences.getString("DestList", "");
            Type listType = new TypeToken<ArrayList<ExploreDestination>>() {
            }.getType();
            exploreDestList = gson.fromJson(jsonOutput, listType);
            photoGallary.clear();

            pos=Integer.parseInt(selectedPosition);
            Log.d("System out","gallary size_________"+exploreDestList.get(pos).getImageList().size());

                for (int i=0;i<exploreDestList.get(pos).getImageList().size();i++)
            {
                if(exploreDestList.get(pos).getImageList().get(i).getImgDescription().equalsIgnoreCase("Photo")) {
                    photoGallary.add(exploreDestList.get(pos).getImageList().get(i).getImgName());
                    selectedimagelist.add(exploreDestList.get(pos).getImageList().get(i));
                    /*if(exploreDestList.get(pos).getImageList().get(i).getImgCredits().equalsIgnoreCase("")) {
                        tv_photo_by.setText("Photo by " + exploreDestList.get(pos).getImageList().get(i).getImgCredits());
                    }*/


                }
            }
            ((ExploreActivity)getActivity()).tv_gallary_total.setText(photoGallary.size()+"");


        }
        else
        {

            String jsonOutput1 = sharedpreferences.getString("POIList", "");
            Type listType1 = new TypeToken<ArrayList<ExplorePOI>>() {
            }.getType();
            explorePoiList = gson.fromJson(jsonOutput1, listType1);
            photoGallary.clear();
            pos=Integer.parseInt(selectedPosition);

            for (int i=0;i<explorePoiList.get(pos).getImageList().size();i++)
            {
                if(explorePoiList.get(pos).getImageList().get(i).getImgDescription().equalsIgnoreCase("Photo")) {
                    photoGallary.add(explorePoiList.get(pos).getImageList().get(i).getImgName());
                    selectedimagelist.add(explorePoiList.get(pos).getImageList().get(i));

                }
            }
            ((ExploreActivity)getActivity()).tv_gallary_total.setText(photoGallary.size()+"");


        }
        if(photoGallary.size()>0)
        {
            if(!selectedimagelist.get(0).getImgCredits().equalsIgnoreCase("")) {
                tv_photo_by.setText("Photo by " + selectedimagelist.get(0).getImgCredits());
            }  else
            {
                tv_photo_by.setText("");

            }

            if(!selectedimagelist.get(0).getImgLicenseCCSA().equalsIgnoreCase(""))
            {
               // tv_photo_cc.setText(" CC BY – SA");
                tv_photo_cc.setText(selectedimagelist.get(0).getImgLicenseCCBY());
            }else if(selectedimagelist.get(0).getImgCredits().equalsIgnoreCase("") && !selectedimagelist.get(0).getImgLicenseCCSA().equalsIgnoreCase(""))
            {
                tv_photo_cc.setText(selectedimagelist.get(0).getImgLicenseCCBY());

            }else
            {
                tv_photo_cc.setText(" ");
            }
        }
        tv_photo_by.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(selectedimagelist.get(imageView.getSelectedItemPosition()).getImgPhotoLink()); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

            }
        });
        tv_photo_cc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().trim()); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


        ((ExploreActivity)getActivity()).ll_close_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();


            }
        });
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });




        Log.d("size of photogallary",""+photoGallary.size());
        // imageView.setImageResource( R.drawable.img_hom1);
        gallery.setAdapter(new ImageAdapter(this));
        imageView.setAdapter(new ImageAdapter1(this));
        gallery.setSelection(0);
        imageView.setSelection(0);
        GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
        GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
        hierarchy.setProgressBarImage(new ImageLoadProgressBar());
      //  imageView.setHierarchy(hierarchy);

       // imageView.setImageURI(Constants.TimbThumb_ImagePath+photoGallary.get(0)+"&width="+width);
        Log.d("System out","gallary image path___"+Constants.TimbThumb_ImagePath+photoGallary.get(0)+"&width="+width);
       // ((ExploreActivity)getActivity()).tv_gallary.setText(photoGallary.get(0)+"");


        imageView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((ExploreActivity)getActivity()).tv_gallary.setText((imageView.getSelectedItemPosition()+1)+"");
                gallery.setSelection(imageView.getSelectedItemPosition());

                if(!selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits().equalsIgnoreCase("")) {
                    tv_photo_by.setText("Photo by " + selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits());
                }  else
                {
                    tv_photo_by.setText("");

                }

                if(!selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().equalsIgnoreCase(""))
                {
                    tv_photo_cc.setText("/ "+selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCBY());
                }else if(selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits().equalsIgnoreCase("") && !selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().equalsIgnoreCase(""))
                {
                    tv_photo_cc.setText(selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCBY());

                }else
                {
                    tv_photo_cc.setText(" ");
                }
                Log.d("System out","photo by & CC _________"+selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits()+"  "+selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().equalsIgnoreCase(""));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

  /*      imageView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                ((ExploreActivity)getActivity()).tv_gallary.setText((imageView.getSelectedItemPosition()+1)+"");
                gallery.setSelection(imageView.getSelectedItemPosition());

                if(!selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits().equalsIgnoreCase("")) {
                    tv_photo_by.setText("Photo by " + selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits());
                }  else
                {
                    tv_photo_by.setText("");

                }

                if(!selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().equalsIgnoreCase(""))
                {
                    tv_photo_cc.setText("/ CC BY");
                }else if(selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits().equalsIgnoreCase("") && !selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().equalsIgnoreCase(""))
                {
                    tv_photo_cc.setText(" CC BY");

                }else
                {
                    tv_photo_cc.setText(" ");
                }
                Log.d("System out","photo by & CC _________"+selectedimagelist.get(imageView.getSelectedItemPosition()).getImgCredits()+"  "+selectedimagelist.get(imageView.getSelectedItemPosition()).getImgLicenseCCSA().equalsIgnoreCase(""));

            }

        });*/



        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position,long id)
            {
                GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
                GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
                hierarchy.setProgressBarImage(new ImageLoadProgressBar());
             imageView.setSelection(position);
               //imageView.setBackgroundDrawable(Constants.TimbThumb_ImagePath+photoGallary.get(position)+"&width="+width);
                ((ExploreActivity)getActivity()).tv_gallary.setText((position+1)+"");
                if(!selectedimagelist.get(position).getImgCredits().equalsIgnoreCase("")) {
                    tv_photo_by.setText("Photo by " + selectedimagelist.get(position).getImgCredits());
                }  else
                {
                    tv_photo_by.setText("");

                }

                if(!selectedimagelist.get(position).getImgLicenseCCSA().equalsIgnoreCase(""))
                {
                    tv_photo_cc.setText("/ "+selectedimagelist.get(position).getImgLicenseCCBY());
                }else if(selectedimagelist.get(position).getImgCredits().equalsIgnoreCase("") && !selectedimagelist.get(position).getImgLicenseCCSA().equalsIgnoreCase(""))
                {
                    tv_photo_cc.setText(selectedimagelist.get(position).getImgLicenseCCBY());

                }else
                {
                    tv_photo_cc.setText(" ");
                }
            }
        });

        return v;
    }
    public class ImageAdapter extends BaseAdapter {
        private GallaryFragment context;
        private int itemBackground;
        public ImageAdapter(GallaryFragment c)
        {
            context = c;
            // sets a grey background; wraps around the images
            //  TypedArray a =obtainStyledAttributes(R.styleable.MyGallery);
            //  itemBackground = a.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 0);
            //  a.recycle();
        }
        // returns the number of images
        public int getCount() {
            return photoGallary.size();
        }
        // returns the ID of an item
        public Object getItem(int position) {
            return position;
        }
        // returns the ID of an item
        public long getItemId(int position) {
            return position;
        }
        // returns an ImageView view
        public View getView(int position, View convertView, ViewGroup parent) {
            SimpleDraweeView imageView = new SimpleDraweeView(getContext());
            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            imageView.setHierarchy(hierarchy);
            //  fc_p.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());

            imageView.setImageURI(Constants.TimbThumb_ImagePath+photoGallary.get(position));
            //  ((ExploreActivity)getActivity()).tv_gallary_total.setText(photoGallary.size());



            //    imageView.setImageResource(photoGallary);
            imageView.setLayoutParams(new Gallery.LayoutParams(180, 180));
            imageView.setBackgroundResource(itemBackground);
            return imageView;
        }
    }
    public class ImageAdapter1 extends BaseAdapter {
        private GallaryFragment context;
        private int itemBackground;
        public ImageAdapter1(GallaryFragment c)
        {
            context = c;
            // sets a grey background; wraps around the images
            //  TypedArray a =obtainStyledAttributes(R.styleable.MyGallery);
            //  itemBackground = a.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 0);
            //  a.recycle();
        }
        // returns the number of images
        public int getCount() {
            return photoGallary.size();
        }
        // returns the ID of an item
        public Object getItem(int position) {
            return position;
        }
        // returns the ID of an item
        public long getItemId(int position) {
            return position;
        }
        // returns an ImageView view
        public View getView(int position, View convertView, ViewGroup parent) {
            SimpleDraweeView imageView = new SimpleDraweeView(getContext());
            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            imageView.setHierarchy(hierarchy);
            //  fc_p.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());

            imageView.setImageURI(Constants.TimbThumb_ImagePath+photoGallary.get(position));

            //  ((ExploreActivity)getActivity()).tv_gallary_total.setText(photoGallary.size());



            //    imageView.setImageResource(photoGallary);
            imageView.setLayoutParams(new Gallery.LayoutParams(width, 700));
            imageView.setBackgroundResource(itemBackground);
            return imageView;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        ((ExploreActivity)getActivity()).ll_gallary_count.setVisibility(View.VISIBLE);
        //((ExploreActivity)getActivity()).ivMenuHeader.setImageResource(R.drawable.close_btn_popup);
        ((ExploreActivity)getActivity()).ll_close_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_menu_header.setVisibility(View.GONE);


        //  ((ExploreActivity)getActivity()).ivMenuHeader.setImageResource(R.drawable.close_btn_popup);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d("System out","OnDestroy__");
        // ((ExploreActivity)getActivity()).ivMenuHeader.setImageResource(R.drawable.menu_icon);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
        ((ExploreActivity)getActivity()).ll_gallary_count.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ivMenuHeader.setImageResource(R.drawable.menu_icon);
        ((ExploreActivity)getActivity()).ll_close_header.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_menu_header.setVisibility(View.VISIBLE);
       /* ((ExploreActivity)getActivity()).ivMenuHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).setmenutonavigationdrawer();
            }
        });*/





    }

    @Override
    public void onDetach() {
        Log.d("System out","OnDetach__");
        super.onDetach();

    }
}
