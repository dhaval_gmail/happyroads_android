package com.bpcl.happyroads;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class FAQFragment extends Fragment {
    @Bind(R.id.lv_faq)
    ListView lvFaq;
    boolean arrowContent=false;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.faq_fragment, container, false);
        ButterKnife.bind(this, v);
        FaqAdapter faqAdapter = new FaqAdapter(this);
        lvFaq.setAdapter(faqAdapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lvFaq.setNestedScrollingEnabled(true);
        }
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private class FaqAdapter extends BaseAdapter {
        FAQFragment context;

        public FaqAdapter(FAQFragment faqFragment) {
            this.context = faqFragment;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = convertView;
            v = inflater.inflate(R.layout.faq_list, null);
            final TextView tv_text_header_faq=(TextView)v.findViewById(R.id.tv_text_header_faq);
            TextView tv_text_content_faq=(TextView)v.findViewById(R.id.tv_text_content_faq);
            final ImageView iv_arrow_down_faq=(ImageView) v.findViewById(R.id.iv_arrow_down_faq);
            final LinearLayout ll_content_faq=(LinearLayout) v.findViewById(R.id.ll_content_faq);
            iv_arrow_down_faq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(arrowContent==false)
                    {
                        iv_arrow_down_faq.setImageResource(R.drawable.arrow_up);
                        ll_content_faq.setVisibility(View.VISIBLE);
                        tv_text_header_faq.setTextColor(getResources().getColor(R.color.textYellow));
                        arrowContent=true;
                    }
                    else
                    {
                        iv_arrow_down_faq.setImageResource(R.drawable.arrow_down);
                        tv_text_header_faq.setTextColor(getResources().getColor(R.color.colorWhite));
                        ll_content_faq.setVisibility(View.GONE);

                        arrowContent=false;
                    }
                }
            });
            return v;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).tv_text_header.setText("FAQ");
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }
}
