package com.bpcl.happyroads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.CustomeClass.CirclePageIndicator;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Interface.CallMyTrip;
import com.bpcl.happyroads.Pojo.AddFavourite;
import com.bpcl.happyroads.Pojo.AllTravelType;
import com.bpcl.happyroads.Pojo.CreateTripApi;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExploreWeather;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DownloadCode;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.LoginModule;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ADMIN on 8/20/2016.
 */
public class ListingDetailsFragment extends Fragment implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener,CallMyTrip
{
    SlidingImage_Adapter slidingImage_adapter;
    ViewPager mPager;
    CirclePageIndicator indicator;
    ImageView ImgClosepopup;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    TextView tv_plan_my_trip,tv_explore_place;
    LinearLayout ll_plan_my_trip;
    //LinearLayout ll_activities_header,ll_nearby_places_header;
    boolean tripFlag=false;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView tv_start_date_my_trip,tv_return_date_my_trip,tv_weather_text,tv_activities,tv_nearby_activities_num_dest,tv_time_km_detail,tv_save_my_trip;
    String date;
    String date1=null;
    String name="",from= "";
    TextView tv_about_city;
    Calendar now;
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    String select="";
    FrameLayout main_fl;
    private ListView ExploreList;
    private int screenHeight;
    int originasize = 0;
    ExploreAdapter exploreAdapter;
    Boolean isfrombottom = false;
    int lastscrollamount=0;
    View firstChildInList;
    LinearLayout main_ll_second,main_ll_first,ll_weather,ll_weather_small;
    private boolean isscroll=true;
    String finalFrom="";
    TextView tv_nearby_places,tv_from_to_month_dest,tv_nearby_places_num,tv_rating_dest_detail;
    int scroll=0;
    boolean like=false;
    ContentLoadingProgressBar listing_content_progressbar;
    public static ArrayList<ExploreDestination> exploreDestList=new ArrayList<>();
    public static ExploreDestination exploreDest=new ExploreDestination();
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    Boolean readmoreclicked=false;
    ImageView iv_gallary,iv_best_time_icon,iv_nearby_icon,iv_distance_icon,iv_activities_icon,iv_like_icon;
    SimpleDraweeView iv_weather,iv_weather_small;
    CirclePageIndicator img_circle_indicator;
    LinearLayout ll_km_time,ll_best_time,ll_best_time_attraction,ll_activities_header,ll_time_attraction,llNearbyPlacesHeader;
    ImageView iv_share_img;
    private Gson gson=new Gson();
    String start_date="",start_time="",end_date="",end_time="";
    Dialog myTripDialog;
    String tripdate_eta="",trip_return_date_eta="";

    String trip_return_end_date="",trip_return_end_time="",trip_end_date_eta="",trip_end_time_eta="";

    DBHandler dbHandler;
    RelativeLayout error_layout;

    public static Bitmap destinationbmp=null;
    SessionManager sessionManager;
    Activity activity;
    ArrayList<AddFavourite> addFavList=new ArrayList<AddFavourite>();

    Dialog pb_dialog ;
    TextView tv_dest_name_weather,tv_weather_text_small;
    LinearLayout ll_all_temp_weather;
    ArrayList<ExploreWeather> exploreWeatherlist=new ArrayList<>();
    public  static Integer destination_exclude_id1;
    public static String subselectedLat="";
    public static String subselectedLong="";
    EditText et_trip_name;
SessionManager sessionManager1;
    String plan_type = "";

    String selectedposition="0";
    @TargetApi(Build.VERSION_CODES.M)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.listing_details_fragment, container, false);

        tv_plan_my_trip=(TextView)v.findViewById(R.id.tv_plan_my_trip);
        ll_plan_my_trip=(LinearLayout)v.findViewById(R.id.ll_plan_my_trip);
        tv_explore_place=(TextView)v.findViewById(R.id.tv_explore_place);


        sessionManager=new SessionManager(getActivity());

        tripFlag=false;
        now = Calendar.getInstance();
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        activity=getActivity();
        //DestList
        dbHandler=new DBHandler(getActivity());
        pb_dialog = Constants.get_dialog(activity, R.layout.custom_progressbar);
        String jsonOutput = sharedpreferences.getString("DestList","");
        Type listType = new TypeToken<ArrayList<ExploreDestination>>(){}.getType();
        exploreDestList = gson.fromJson(jsonOutput, listType);
        dpd = DatePickerDialog.newInstance(
                ListingDetailsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        ExploreActivity.destattractionimages.clear();




        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf.format(new Date());

        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, 3);
        dpd.setMinDate(calendar);


        tpd = TimePickerDialog.newInstance(
                ListingDetailsFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),true
        );
        tpd.setMinTime(calendar.getTime().getHours(),calendar.getTime().getMinutes(),calendar.getTime().getSeconds());

        final Bundle bundle = this.getArguments();
        if (bundle != null) {
            plan_type = bundle.getString("plan_type", "");
            name = bundle.getString("placeName");
            from = bundle.getString("from");
            selectedposition=bundle.getString("position");
            exploreDest=(ExploreDestination)bundle.getSerializable("Explorlist");
            if(exploreDest.getDesNoOfNearByAttraction()==0)
            {
                //ll_activities_header.setVisibility(View.GONE);
                tv_explore_place.setVisibility(View.GONE);
                ll_plan_my_trip.setVisibility(View.VISIBLE);
                tv_plan_my_trip.setText("("+(String.valueOf(ExploreActivity.origincity).contains(" ")?String.valueOf(ExploreActivity.origincity).substring(0,String.valueOf(ExploreActivity.origincity).indexOf(" ")):String.valueOf(ExploreActivity.origincity))+" to "+(exploreDest.getDesName().contains(" ")?exploreDest.getDesName().substring(0,exploreDest.getDesName().indexOf(" ")):exploreDest.getDesName())+")");

            }else {
                //ll_activities_header.setVisibility(View.VISIBLE);
                ll_plan_my_trip.setVisibility(View.GONE);
                tv_explore_place.setVisibility(View.VISIBLE);

            }
            Log.d("source",name+"/"+"from"+from);
            try {
               // selectedposition = bundle.getString("position",selectedposition);
                Log.d("source",name+"/"+"from"+from+"pos "+selectedposition+"size"+exploreDestList.size());
                if(Integer.valueOf(selectedposition)>=exploreDestList.size())
                {
                    selectedposition="0";
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                selectedposition="0";

            }
            destination_exclude_id1 = exploreDest.getDesID();
            subselectedLat = exploreDest.getDesLatitude();
            subselectedLong = exploreDest.getDesLongitude();
        }
        else

        {

        }

        for (int i=0;i<exploreDest.getImageList().size();i++)
        {
            if(exploreDest.getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                ExploreActivity.destattractionimages.add(exploreDest.getImageList().get(i).getImgName());
            }
        }
       // if(exploreDestList.size()>0)
            get_bitmap_from_freco( Constants.TimbThumb_ImagePath+exploreDest.getDesImage()+"&width="+50+"&height="+50);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.VISIBLE);

        initVar(v);
        Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));


        if (!TextUtils.isEmpty(selectedposition))
        {

            setdestdetail();
        }
        ((ExploreActivity)getActivity()).tv_text_header.setText(name);
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

            ExploreWeatherAPI(exploreDest.getDesID());
        }






        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        //  ll_activities_header=(LinearLayout)v.findViewById(R.id.ll_activities_header);
        // ll_nearby_places_header=(LinearLayout)v.findViewById(R.id.ll_nearby_places_header);

        finalFrom = from;

        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);

        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });




        iv_gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*String jsonOutput = sharedpreferences.getString("DestList", "");
                Type listType = new TypeToken<ArrayList<ExploreDestination>>() {
                }.getType();
                exploreDestList = gson.fromJson(jsonOutput, listType);*/
                Log.d("System out","Response of selected image size "+exploreDest.getImageList().size());
                ArrayList<String> photoGallary = new ArrayList<>();
                for (int i = 0; i<exploreDest.getImageList().size(); i++)
                {
                    if(exploreDest.getImageList().get(i).getImgDescription().equalsIgnoreCase("Photo")) {
                        photoGallary.add(exploreDest.getImageList().get(i).getImgName());

                    }
                }
                if(photoGallary.size() >0) {


                    GallaryFragment gallaryfragment = new GallaryFragment();
                   FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // ((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
                    changeTransaction.replace(R.id.frame, gallaryfragment);
                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("position", selectedposition);
                    bundle.putString("from", "DestListing");
                    gallaryfragment.setArguments(bundle);
                    changeTransaction.commit();
                    ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
                }
                else
                {
                    Constants.show_error_popup(getActivity(),"No image Available",error_layout);
                }
            }
        });
        iv_like_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";

                    if(sessionManager.isLoggedIn())
                    {

                        String json = "[{     \"FavUmId\": \""+sessionManager.get_Authenticate_User().getUmId()+"\", \"FavType\": \"Destination\",   \"FavRefId\": \""+exploreDest.getDesID()+"\"}]";
                        Log.d("System out", "In add favourite  " + json);

                        AddFavouritesAPI(json,(ImageView) v);
                    }else
                    {
                        LoginModule loginModule = new LoginModule(getActivity(),"favourite Destination");
                        loginModule.signIn();
                    }


                }
                else
                {
                    Constants.show_error_popup(getActivity(),getActivity().getString(R.string.internet_error), error_layout);
                }


            }

            private void AddFavouritesAPI(final String json, final ImageView imageView) {
                final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                pb_dialog.setCancelable(false);
                pb_dialog.show();

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ArrayList<AddFavourite>> call = apiService.addFavourites(json);
                call.enqueue(new Callback<ArrayList<AddFavourite>>() {
                    @Override
                    public void onResponse(Call<ArrayList<AddFavourite>> call, Response<ArrayList<AddFavourite>> response) {
                        if (response.body() != null) {

                            pb_dialog.dismiss();
                            addFavList= response.body();
                            if (addFavList.get(0).getResStatus() == true)
                            {
                                //  Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                if(addFavList.get(0).getResDescription().toLowerCase().contains("add")) {

                                    imageView.setImageResource(R.drawable.like_icon_selected_detail);


                                    final Dialog dialog1 = new Dialog(getActivity());
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog1.setContentView(R.layout.like_remove_layout);
                                    final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                    tv_text_like_remove.setText("Added to your favourites");
                                    dialog1.setCanceledOnTouchOutside(true);
                                    dialog1.show();
                                    like = true;
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(1500);
                                                ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();

                                                        //Code for the UiThread
                                                    }
                                                });
                                               /* runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();
                                                    }
                                                });*/
                                            } catch (Exception e) {
                                                Log.w("Exception in splash", e);
                                            }

                                        }
                                    }).start();


                                }
                                else
                                {
                                    imageView.setImageResource(R.drawable.like_icon_unselected_detail);
                                    final Dialog dialog1 = new Dialog(getActivity());
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog1.setContentView(R.layout.like_remove_layout);
                                    final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                    tv_text_like_remove.setText("Removed from your favourites");
                                    dialog1.setCanceledOnTouchOutside(true);
                                    dialog1.show();
                                    like = true;
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(1500);
                                                ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();

                                                        //Code for the UiThread
                                                    }
                                                });
                                               /* runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();
                                                    }
                                                });*/
                                            } catch (Exception e) {
                                                Log.w("Exception in splash", e);
                                            }

                                        }
                                    }).start();


                                }
                            }
                            else
                            {
                                // Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                            }



                        }


                    }

                    @Override
                    public void onFailure(Call<ArrayList<AddFavourite>> call, Throwable t) {
                        // Log error here since request failed
                        pb_dialog.dismiss();
                        t.printStackTrace();
                        if (!Constants.isInternetAvailable(getActivity())) {
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                        } else {
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                        }



                    }
                });
            }

        });



        llNearbyPlacesHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putString("name",name);
                b.putString("nameids", "");
                b.putString("progress", "999999");
                b.putString("from","nearbyplaces");


                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new SubListingFragment(),b);

            }
        });
        ll_activities_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   ((ExploreActivity)getActivity()).replace_fragmnet(new ExplorePlacesFragment());
                tv_explore_place.performClick();


            }
        });



        main_fl=(FrameLayout) v.findViewById(R.id.main_fl);
        main_ll_first=(LinearLayout) v.findViewById(R.id.main_ll_first);
        main_ll_second=(LinearLayout) v.findViewById(R.id.main_ll_second);
        ll_weather=(LinearLayout) v.findViewById(R.id.ll_weather);
        ll_weather_small=(LinearLayout) v.findViewById(R.id.ll_weather_small);
        tv_weather_text_small=(TextView) v.findViewById(R.id.tv_weather_text_small);
        ExploreList=(ListView) v.findViewById(R.id.ExploreList_listing);
        img_circle_indicator=(CirclePageIndicator)v.findViewById(R.id.img_circle_indicator);
        if( ((ExploreActivity)getActivity()).pager!=null) {
            ((ExploreActivity)getActivity()).setpagerdata(true);
            final float density = getResources().getDisplayMetrics().density;
            img_circle_indicator.setRadius(2 * density);
            img_circle_indicator.setViewPager(((ExploreActivity) getActivity()).pager);
        }

      /*  final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                if( ((ExploreActivity)getActivity()).pager!=null) {
                    ((ExploreActivity) getActivity()).pager.setCurrentItem(currentPage++, true);
                }

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        img_circle_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ExploreList.setNestedScrollingEnabled(true);
        }
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenHeight = metrics.heightPixels;
        ExploreAdapter exploreAdapter=new ExploreAdapter(getActivity(),exploreDest);
        ExploreList.setAdapter(exploreAdapter);

        ExploreList.setScrollingCacheEnabled(true);
        ExploreList.setSaveEnabled(true);
        originasize = main_ll_first.getHeight();
        ll_weather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(getActivity());
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog1.setContentView(R.layout.weather_pop_up);
                tv_dest_name_weather=(TextView)dialog1.findViewById(R.id.tv_dest_name_weather);
                 ll_all_temp_weather=(LinearLayout)dialog1.findViewById(R.id.ll_all_temp_weather);
                int pos=Integer.parseInt(selectedposition);
                tv_dest_name_weather.setText(exploreDest.getDesName());
                mPager=(ViewPager)dialog1.findViewById(R.id.pager_weather);
                ImgClosepopup=(ImageView)dialog1.findViewById(R.id.ImgClosepopup);
                indicator = (CirclePageIndicator)dialog1.findViewById(R.id.indicator);
                init();


                if(exploreWeatherlist.size()>0) {
                    setWeatherAllDays(exploreWeatherlist);
                    slidingImage_adapter = new SlidingImage_Adapter(getActivity(), exploreWeatherlist);
                    mPager.setAdapter(slidingImage_adapter);
                    indicator.setViewPager(mPager);
                }
                ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog1.dismiss();
                    }
                });

               /* WeatherPagerAdapter weatherPagerAdapter=new WeatherPagerAdapter(this);
                pager_weather.setAdapter(weatherPagerAdapter);*/
                dialog1.show();
            }


        });
        ll_weather_small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_weather.performClick();

            }
        });




        ExploreList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.d("System out", "scrollState: " + scrollState+" sharedpreferences readmoreclicked "+sharedpreferences.getString("isinreadlesslisting","false"));
                if (scrollState == 0 || scrollState == SCROLL_STATE_IDLE) {
                    isscroll = false;
                } else {
                    isscroll = true;
                }



            }


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {


                Log.d("System out", "isscroll In Scroll: " + isscroll+" readmoreclicked "+readmoreclicked+"pref value"+sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("true"));
                if (sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("false") &&  isscroll==true ) {


                    if (ExploreList != null) {


                        scroll = getScroll(ExploreList);


                        Log.d("System out", "Scroll: " + scroll+"isscroll :::"+isscroll);
                        Log.d("System out", "view pos: " + view.getVerticalScrollbarPosition());
                        Log.d("System out", "view pos: " + view.getScrollY());

                        if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1) {
                            Log.d("System out", "main list height: " + ExploreList.getHeight() / 2 + " child height :::" + (ExploreList.getChildAt(ExploreList.getChildCount() - 1).getHeight()) / 2);
                            Log.d("System out", "main bottom  height: " + ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() + " main height :::" + ExploreList.getHeight());
                            Log.d("System out", "main bottom  height: " + ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop());
                        }

                    }
                   /* View v = ExploreList.getChildAt(0);
                    topOffset = (v == null) ? 0 : v.getTop();
                    if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1) {
                        ExploreList.setSelectionFromTop(ExploreList.getLastVisiblePosition(), topOffset);
                    }
*/
                    if (scroll == 0 && ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1 && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop()!=0) {

                     //   settooriginal();


                    }
                    else if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1
                            && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() <= ExploreList.getHeight()) {

                        //else if ((scroll > lastscrollamount + 5 || scroll > lastscrollamount - 5) && scroll >= ExploreList.getMaxScrollAmount() / 2) {

                        settopimage();

                    }
                    else if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1
                            && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() >= ExploreList.getHeight() && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop()!=0)
                    {
                        if (sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("false"))
                        {
                         //   settooriginal();
                        }
                        isfrombottom=false;


                    }

                    else if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1
                            && (ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() == ExploreList.getHeight() || ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop() == ExploreList.getHeight())) {
                     //   setmiddleimage();

                    }

                }
                else
                {
                    if (ExploreList != null) {


                        scroll = getScroll(ExploreList);


                        Log.d("System out", "Scroll: " + scroll+"isscroll :::"+isscroll);
                        Log.d("System out", "view pos: " + view.getVerticalScrollbarPosition());
                        Log.d("System out", "view pos: " + view.getScrollY());

                        if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1) {
                            Log.d("System out", "main list height: " + ExploreList.getHeight() / 2 + " child height :::" + (ExploreList.getChildAt(ExploreList.getChildCount() - 1).getHeight()) / 2);
                            Log.d("System out", "main bottom  height: " + ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() + " main height :::" + ExploreList.getHeight());
                            Log.d("System out", "main bottom  height: " + ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop());
                        }

                    }
                   /* View v = ExploreList.getChildAt(0);
                    topOffset = (v == null) ? 0 : v.getTop();
                    if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1) {
                        ExploreList.setSelectionFromTop(ExploreList.getLastVisiblePosition(), topOffset);
                    }
*/

                    settopimage();

                    if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1
                            && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() <= ExploreList.getHeight()) {

                        //else if ((scroll > lastscrollamount + 5 || scroll > lastscrollamount - 5) && scroll >= ExploreList.getMaxScrollAmount() / 2) {

                        settopimage();

                    }
                    else if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1
                            && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() >= ExploreList.getHeight() && ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop()!=0)
                    {


                        //  settooriginal();

                        //   isfrombottom=false;

                    }

                    else if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1
                            && (ExploreList.getChildAt(ExploreList.getChildCount() - 1).getBottom() == ExploreList.getHeight() || ExploreList.getChildAt(ExploreList.getChildCount() - 1).getTop() == ExploreList.getHeight())) {
                        setmiddleimage();

                    }

                }



                /*if (ExploreList.getLastVisiblePosition() == ExploreList.getAdapter().getCount() - 1) {
                    ExploreList.setSelectionFromTop(ExploreList.getLastVisiblePosition(), topOffset);
                }*/

            }



        });






        ButterKnife.bind(this, v);



        return v;
    }



    public void ExploreWeatherAPI(int destid) {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        // explore_places_content_progressbar.setVisibility(View.VISIBLE);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id="1";
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());

            json="[{\"Weather_PTO_RefId\": \""+destid+"\", \"Weather_Date\": \""+formattedDate+"\"  }]";

            Log.d("System out","Explore weather api____"+json);



        Call<ArrayList<ExploreWeather>> call = apiService.exploreweather(json);
        call.enqueue(new Callback<ArrayList<ExploreWeather>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreWeather>> call, Response<ArrayList<ExploreWeather>> response) {

                pb_dialog.dismiss();

                if (response.body() != null) {


                   /* exploreWeatherlist.clear();

                    exploreWeatherlist.addAll(response.body());
                    if (exploreWeatherlist.size()>0)
                    {
                        if (exploreWeatherlist.get(0).getResStatus() == true) {
                            Log.d("System out","in api call_____________"+exploreWeatherlist.size());
                            //double ferenhite=(1.8 * (exploreWeatherlist.get(0).getWeatherTempMax() - 273) + 32);
                            double ferenhite=(exploreWeatherlist.get(0).getWeatherTempMax()- 273.15);

                            tv_weather_text.setText(""+ferenhite);
                            iv_weather.setImageURI(Constants.WeatherIconPath+exploreWeatherlist.get(0).getWeatherIcon()+".png");
                            iv_weather_small.setImageURI(Constants.WeatherIconPath+exploreWeatherlist.get(0).getWeatherIcon()+".png");
                            tv_weather_text_small.setText(""+ferenhite);



                        }
                        else
                        {
                            //  explore_places_content_progressbar.setVisibility(View.GONE);
                            Constants.show_error_popup(getActivity(),exploreWeatherlist.get(0).getResDescription()+"", error_layout);
                        }
                    }
                    else
                    {
                        //   explore_places_content_progressbar.setVisibility(View.GONE);
                    }

*/
                    exploreWeatherlist.clear();
                    ArrayList<ExploreWeather> exploreWeatherlist1=new ArrayList<>();
                    exploreWeatherlist1.addAll(response.body());
                    if (exploreWeatherlist1.size()>1)
                    {
                        if (exploreWeatherlist1.get(0).getResStatus() == true) {
                            Log.d("System out","in api call_____________"+exploreWeatherlist1.size());
                            //double ferenhite=(1.8 * (exploreWeatherlist.get(0).getWeatherTempMax() - 273) + 32);


                            for(int i=0;i<exploreWeatherlist1.size();i++)
                            {
                                try {
                                    String date=Constants.formatDate(exploreWeatherlist1.get(i).getWeatherDate().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "EEE");

                                    if(exploreWeatherlist.size()<5)
                                    {  int temp=0;
                                        for(int ii=0;ii<exploreWeatherlist.size();ii++)
                                        {

                                            if(date.equalsIgnoreCase(exploreWeatherlist.get(ii).getWeatherDay()))
                                            {
                                                temp++;
                                            }

                                        }
                                        if(temp >0)
                                        {

                                        }
                                        else
                                        {
                                            exploreWeatherlist1.get(i).setWeatherDay(date);
                                            exploreWeatherlist.add(exploreWeatherlist1.get(i));
                                            //  exploreWeatherlist.get(exploreWeatherlist.size()-1).setWeatherDay(date);
                                        }
                                    }
                                   /* else
                                    {

                                        int temp=0;
                                        for(int ii=0;ii<exploreWeatherlist.size();ii++)
                                        {
                                            Log.d("System out","compare condition else "+date + " "+exploreWeatherlist.get(ii).getWeatherDay() + "   "+date.equalsIgnoreCase(exploreWeatherlist.get(ii).getWeatherDay()));
                                            if(date.equalsIgnoreCase(exploreWeatherlist.get(ii).getWeatherDay()))
                                            {
                                                temp++;
                                            }

                                        }
                                        if(temp >0)
                                        {

                                        }
                                        else {
                                            exploreWeatherlist1.get(i).setWeatherDay(date);
                                            exploreWeatherlist.add(exploreWeatherlist1.get(i));
                                            //exploreWeatherlist.get(i).setWeatherDay(date);
                                            break;
                                        }

                                    }*/
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                            for(int i=0;i<exploreWeatherlist.size();i++)
                            {
                                try {
                                    String date=Constants.formatDate(exploreWeatherlist.get(i).getWeatherDate().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "EEE");

                                    for(int ii=0;ii<exploreWeatherlist1.size();ii++) {

                                        String date1=Constants.formatDate(exploreWeatherlist1.get(ii).getWeatherDate().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "EEE");

                                        if (exploreWeatherlist.get(i).getWeatherDay().equalsIgnoreCase(date1)) {
                                            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                                            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM, yyyy");
                                            String currentDateandTime = sdf1.format(new Date());
                                            String newDateandTime1 = Constants.formatDate(exploreWeatherlist1.get(ii).getWeatherDate().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy hh:mm a");
                                            String newDateandDate1 = Constants.formatDate(exploreWeatherlist1.get(ii).getWeatherDate().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy");

                                            Date datecurrent = null;
                                            Date newDateandTime = null;
                                            Date datecurrentdate = null;
                                            Date newDateanddate = null;

                                            Date temp = null;
                                            String temp1="";
                                            temp = sdf1.parse(currentDateandTime);
                                            Calendar calendar = Calendar.getInstance();
                                            calendar.setTime(temp);

                                            calendar.add(Calendar.DATE, i);

                                            temp1=sdf1.format(calendar.getTime());

                                            try {
                                                datecurrent = sdf1.parse(temp1);
                                                newDateandTime = sdf1.parse(newDateandTime1);
                                                datecurrentdate = sdf2.parse(temp1);
                                                newDateanddate = sdf2.parse(newDateandDate1);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            Log.d("System out","current date and time "+datecurrent.toString()+" new date "+newDateandTime.toString());
                                            Log.d("System out","current date "+datecurrentdate.toString()+" new date "+newDateanddate.toString());
                                            Log.d("System out","date comparison "+(datecurrent.getTime()<newDateandTime.getTime()) +" date "+datecurrentdate.equals(newDateanddate));
                                            if(newDateandTime.getTime()<datecurrent.getTime() && datecurrentdate.equals(newDateanddate))
                                            {
                                                Log.d("System out","old max tamp"+ exploreWeatherlist.get(i).getWeatherTempMax());
                                                exploreWeatherlist.get(i).setWeatherTempMax(exploreWeatherlist1.get(ii).getWeatherTempMax());
                                                exploreWeatherlist.get(i).setWeatherIcon(exploreWeatherlist1.get(ii).getWeatherIcon());
                                                Log.d("Syatem out","weather icon path in adapter____"+exploreWeatherlist1.get(ii).getWeatherIcon());

                                                Log.d("System out","max tamp change "+ exploreWeatherlist.get(i).getWeatherTempMax());
                                            }

                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                            if(exploreWeatherlist.size()>0) {
                                double ferenhite = (exploreWeatherlist.get(0).getWeatherTempMax() - 273.15);
                                //                                tv_weather_text.setText(""+new DecimalFormat("##.##").format(ferenhite));


                                tv_weather_text.setText("" + ferenhite);
                                iv_weather.setImageURI(Constants.WeatherIconPath + exploreWeatherlist.get(0).getWeatherIcon() + ".png");
                                Log.d("Syatem out","weather icon path____"+Constants.WeatherIconPath + exploreWeatherlist.get(0).getWeatherIcon() + ".png");
                                iv_weather_small.setImageURI(Constants.WeatherIconPath + exploreWeatherlist.get(0).getWeatherIcon() + ".png");
                                tv_weather_text_small.setText("" + ferenhite);
                            }


                        }
                        else
                        {
                            //  explore_places_content_progressbar.setVisibility(View.GONE);
                            Constants.show_error_popup(getActivity(),exploreWeatherlist1.get(0).getResDescription()+"", error_layout);
                        }
                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(),exploreWeatherlist1.get(0).getResDescription()+"", error_layout);
                        //   explore_places_content_progressbar.setVisibility(View.GONE);
                    }

                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreWeather>> call, Throwable t) {
                // Log error here since request failed
                //  explore_places_content_progressbar.setVisibility(View.GONE);
                pb_dialog.dismiss();

                t.printStackTrace();


            }
        });
    }

    private void setWeatherAllDays(ArrayList<ExploreWeather> exploreWeatherlist) {
        for(int i=0;i<exploreWeatherlist.size();i++) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View v1;
            v1 = inflater.inflate(R.layout.weather_all_day_temp_layout, null);
            TextView tv_day_weather=(TextView)v1.findViewById(R.id.tv_day_weather);
            TextView tv_temp_weather=(TextView)v1.findViewById(R.id.tv_temp_weather);
            SimpleDraweeView iv_weather_icon_all_day=(SimpleDraweeView) v1.findViewById(R.id.iv_weather_icon_all_day);
            tv_day_weather.setText(""+exploreWeatherlist.get(i).getWeatherTempDay());

           // double ferenhite=(1.8 * (exploreWeatherlist.get(i).getWeatherTempMax() - 273) + 32);
            double ferenhite=(exploreWeatherlist.get(i).getWeatherTempMax()- 273.15);
            iv_weather_icon_all_day.setImageURI(Constants.WeatherIconPath+exploreWeatherlist.get(i).getWeatherIcon()+".png");



            try {
                tv_day_weather.setText(Constants.formatDate(exploreWeatherlist.get(i).getWeatherDate().replace("T"," "), "yyyy-MM-dd hh:mm:ss", "EEE"));
                tv_temp_weather.setText(""+ferenhite);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            ll_all_temp_weather.addView(v1);
        }
    }


    private void initVar(View v) {
        iv_gallary=(ImageView)v.findViewById(R.id.iv_gallary);
        iv_weather=(SimpleDraweeView) v.findViewById(R.id.iv_weather);
        iv_weather_small=(SimpleDraweeView) v.findViewById(R.id.iv_weather_small);
        iv_best_time_icon=(ImageView)v.findViewById(R.id.iv_best_time_icon);
        iv_nearby_icon=(ImageView)v.findViewById(R.id.iv_nearby_icon);
        iv_distance_icon=(ImageView)v.findViewById(R.id.iv_distance_icon);
        iv_activities_icon=(ImageView)v.findViewById(R.id.iv_activities_icon);
        iv_like_icon=(ImageView)v.findViewById(R.id.iv_like_icon);
        iv_share_img=(ImageView)v.findViewById(R.id.iv_share_img);
        ll_km_time=(LinearLayout)v.findViewById(R.id.ll_km_time);
        ll_best_time=(LinearLayout)v.findViewById(R.id.ll_best_time);
        ll_best_time_attraction=(LinearLayout)v.findViewById(R.id.ll_best_time_attraction);
        ll_activities_header=(LinearLayout)v.findViewById(R.id.ll_activities_header);
        ll_time_attraction=(LinearLayout)v.findViewById(R.id.ll_time_attraction);
        llNearbyPlacesHeader=(LinearLayout)v.findViewById(R.id.ll_nearby_places_header);
        tv_weather_text=(TextView) v.findViewById(R.id.tv_weather_text);
        tv_activities=(TextView) v.findViewById(R.id.tv_activities);
        tv_nearby_activities_num_dest=(TextView) v.findViewById(R.id.tv_nearby_activities_num_dest);
        tv_time_km_detail=(TextView) v.findViewById(R.id.tv_time_km_detail);
        listing_content_progressbar=(ContentLoadingProgressBar)v.findViewById(R.id.listing_content_progressbar);

        tv_nearby_places=(TextView)v.findViewById(R.id.tv_nearby_places);
        tv_from_to_month_dest=(TextView)v.findViewById(R.id.tv_from_to_month_dest);
        tv_nearby_places_num=(TextView)v.findViewById(R.id.tv_nearby_places_num);
        tv_rating_dest_detail=(TextView)v.findViewById(R.id.tv_rating_dest_detail);

        error_layout=(RelativeLayout)v.findViewById(R.id.error_layout);
    }

    private void setdestdetail() {
        int pos=Integer.parseInt(selectedposition);
        if (exploreDestList.size()>0 && pos<exploreDestList.size())
        {

            if (selectedposition.equalsIgnoreCase(String.valueOf(selectedposition)))
            {
                ((ExploreActivity)getActivity()).tv_text_header.setText(exploreDest.getDesName());
                if (exploreDest.getDestBestTimeFrom().replace("null","") != null && exploreDest.getDesBestTimeTo().replace("null","")!=null) {

                    tv_from_to_month_dest.setText(Constants.getMonthName(Integer.parseInt(exploreDest.getDestBestTimeFrom())) + " to " + Constants.getMonthName(Integer.parseInt(exploreDest.getDesBestTimeTo())));

                }
               // tv_time_km_detail.setText((int)Math.ceil(exploreDest.getDistance())+" kms ["+Constants.convert_minute_hrs(exploreDest.getDuration().doubleValue())+"]");
                tv_time_km_detail.setText((int)Math.ceil(exploreDest.getDistance())+" kms");

                tv_nearby_places_num.setText(exploreDest.getDesNoOfNearByDestination()+"");
                tv_rating_dest_detail.setText(exploreDest.getDesRating());
                tv_nearby_activities_num_dest.setText(exploreDest.getDesNoOfNearByAttraction()+"");

                if(exploreDest.getFavId()==0)
                {
                    iv_like_icon.setImageResource(R.drawable.like_icon_unselected_detail);
                }else
                {
                    iv_like_icon.setImageResource(R.drawable.like_icon_selected_detail);

                }




            }

        }
    }


    private void setmiddleimage() {


        lastscrollamount = scroll;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);

        // scroll = ExploreList.getMaxScrollAmount() / 2;


        lp.weight = 55 ;
        lp1.weight = 45 ;




        main_ll_second.setLayoutParams(lp);
        main_ll_first.setLayoutParams(lp1);

        Log.e("System out","in Middle  main first ll weight ::::"+lp1.weight+" second  ll weight ::::"+lp.weight);

        if ((lp1.weight<70 || lp1.weight==70) &&  lp1.weight>50) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._350sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else if ((lp1.weight<50 ||lp1.weight==50) && lp1.weight<40) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._285sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        } else if ((lp1.weight<40 ||lp1.weight==40) && lp1.weight<30) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._210sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else
        {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._250sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();
        }
        setContentSize("small");

    }

    private void settopimage() {
        isfrombottom=true;
        lastscrollamount = scroll;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        // scroll = ExploreList.getMaxScrollAmount();





        lp.weight = 40 ;
        lp1.weight = 60 ;




        Log.e("System out","in pager   height ::::"+ ((ExploreActivity) getActivity()).pager.getHeight()+" second  ll weight ::::"+ ((ExploreActivity) getActivity()).pager.getWidth());
        Log.e("System out","in BOttom if::::"+lp.weight+" second  ll weight ::::"+lp1.weight);
        main_ll_second.setLayoutParams(lp1);
        main_ll_first.setLayoutParams(lp);


        if ((lp.weight<70 || lp.weight==70) &&  lp.weight>50) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._350sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else if ((lp.weight<50 ||lp.weight==50) && lp.weight<40) {
            // ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._285sdp);
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._205sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        } else if ((lp.weight<40 ||lp.weight==40) && lp.weight<30) {
            // ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._210sdp);
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._170sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else
        {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._250sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();
        }
        setContentSize("small");
    }

    private void settooriginal() {
        lastscrollamount = scroll;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);

        lp.weight = 60 ;
        lp1.weight = 40 ;

        isfrombottom=false;


        Log.e("System out","in Top if::::"+lp.weight+" second  ll weight ::::"+lp1.weight);
        main_ll_second.setLayoutParams(lp1);
        main_ll_first.setLayoutParams(lp);


        if ((lp.weight<70 || lp.weight==70) &&  lp.weight>50) {

            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._350sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else if ((lp.weight<50 ||lp.weight==50) && lp.weight<40) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._285sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        } else if ((lp.weight<40 ||lp.weight==40) && lp.weight<30) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._210sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else
        {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._250sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();
        }
        setContentSize("big");

    }


    private void setContentSize(String size) {
        Log.d("System out","SIZE IS_-----"+size);
        if(size.equalsIgnoreCase("big"))
        {
            iv_gallary.setImageResource(R.drawable.photo_gallery_big);
          //  iv_weather.setImageResource(R.drawable.weather_icon);
        //    iv_weather.setImageURI(Constants.WeatherIconPath+exploreWeatherlist.get(0).getWeatherIcon()+".png");
            iv_best_time_icon.setImageResource(R.drawable.best_time_visit_icon);
            iv_nearby_icon.setImageResource(R.drawable.nearby_places_icon);
            iv_distance_icon.setImageResource(R.drawable.distance_icon_detail);
            iv_activities_icon.setImageResource(R.drawable.activities_icon);
            tv_nearby_places.setVisibility(View.VISIBLE);
            tv_activities.setVisibility(View.VISIBLE);
            ll_weather_small.setVisibility(View.GONE);
            ll_weather.setVisibility(View.VISIBLE);

            // tv_nearby_places.setTextSize((int)getResources().getDimension(R.dimen._6sdp));
            //  tv_activities.setTextSize((int)getResources().getDimension(R.dimen._6sdp));
        }
        else
        {

            iv_gallary.setImageResource(R.drawable.photo_gallery_small);
         //  iv_weather.setImageResource(R.drawable.weather_icon_small);
            iv_best_time_icon.setImageResource(R.drawable.best_time_icon_small);
            iv_nearby_icon.setImageResource(R.drawable.nearby_icon_small);
            iv_distance_icon.setImageResource(R.drawable.distance_icon);
            iv_activities_icon.setImageResource(R.drawable.poi_icon_small);
            tv_nearby_places.setVisibility(View.GONE);
            tv_activities.setVisibility(View.GONE);
            ll_weather_small.setVisibility(View.VISIBLE);
            ll_weather.setVisibility(View.GONE);







        }
    }



    @Override
    public void onResume() {
        super.onResume();
        Log.d("System out","resume from listing");

        editor.putString("isinreadlesslisting","false").commit();
        ((ExploreActivity)getActivity()).pager.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); // transperent color = #00000000
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);


        settooriginal();


    }

    private void init() {
        final float density = getResources().getDisplayMetrics().density;

        indicator.setRadius(3 * density);

//
//         NUM_PAGES = 5;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        },3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
                currentPage=pos;


            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }



    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //  SimpleDateFormat formatter = new SimpleDateFormat("dd MMM ,yyyy");
        date =dayOfMonth+" "+(++monthOfYear)+","+year;

        tpd.show(activity.getFragmentManager(),"TimePickerDialog");




    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {


        String time = hourOfDay+":"+minute+"";

        try {
            date1= Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","dd MMM, yyyy hh:mm a");
            if(select.equalsIgnoreCase("start"))
            {
                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date1);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(datecurrent);
                //calendar.add(Calendar.HOUR, 2);
                calendar.add(Calendar.HOUR, 0);

                //  currentDateandTime.set
                if (calendar.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                   // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours from current time.", tv_save_my_trip);
                    Constants.show_error_popup(activity,"Select valid date & time.", tv_save_my_trip);

                }
              /*  if (calendar.getTime() > datenew.getTime()) {
                    tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                }*/
                else {
                    start_date = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
                    start_time = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "HH:mm");
                    tv_start_date_my_trip.setText(date1);
                }
            }
            else if(select.equalsIgnoreCase("stop"))
            {
                end_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                end_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");
                tv_return_date_my_trip.setText(date1);

                if (et_trip_name.getText().toString().length() > 0) {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {
                        Constants.show_error_popup(activity, "Select your trip dates.", activity.findViewById(R.id.error_layout));
                    }

                    else
                    {
                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;

                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;
                                int pos=Integer.parseInt(selectedposition);
                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDest.getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta",date.toString());
                                    tripdate=sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate)) {
                                   // CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                                }
                                else {
                                    try {
                                       // Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDest.getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                        Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                    }
                                }
                            }
                            else
                            {
                                int pos=Integer.parseInt(selectedposition);
                                Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDest.getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                            }
                        }
                        else {
                            Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                            int pos=Integer.parseInt(selectedposition);
                            CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDest.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                        }
                    }
                } else

                {
                    Constants.show_error_popup(activity, "Please Enter Trip Name", activity.findViewById(R.id.error_layout));
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void CallTrip(Activity acon) {
        activity=acon;
        Mytrip_Dialog(acon);
    }


    public class SlidingImage_Adapter extends PagerAdapter {

        Context context;
        LayoutInflater inflater;
        ArrayList<ExploreWeather> exploreWeatherArrayList;

        public SlidingImage_Adapter(Context context,ArrayList<ExploreWeather> exploreWeatherlist1) {
            this.context=context;
            inflater = LayoutInflater.from(context);
            exploreWeatherArrayList=exploreWeatherlist1;
            Log.d("System out","in adapter_____________"+exploreWeatherArrayList.size());
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
        @Override
        public int getCount() {
            return exploreWeatherArrayList.size();
        }
        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
            View imageLayout = inflater.inflate(R.layout.weather_pager_item, view, false);
            TextView tv_date_day_weather=(TextView)imageLayout.findViewById(R.id.tv_date_day_weather);
            TextView tv_temprature_day_weather=(TextView)imageLayout.findViewById(R.id.tv_temprature_day_weather);
            SimpleDraweeView iv_weather_icon=(SimpleDraweeView)imageLayout.findViewById(R.id.iv_weather_icon);
            iv_weather_icon.setImageURI(Constants.WeatherIconPath+exploreWeatherlist.get(position).getWeatherIcon()+".png");
          //  double ferenhite = (((exploreWeatherArrayList.get(position).getWeatherTempMax() - 273) * 9/5) + 32);
          //  double ferenhite=(1.8 * (exploreWeatherArrayList.get(position).getWeatherTempMax() - 273) + 32);
            double ferenhite=(exploreWeatherlist.get(position).getWeatherTempMax()- 273.15);
            tv_temprature_day_weather.setText(""+ferenhite);
          //  iv_weather_icon.setImageURI(exploreWeatherlist.get(0).getWeatherIcon());
            try {
                tv_date_day_weather.setText(Constants.formatDate(exploreWeatherArrayList.get(position).getWeatherDate().replace("T"," "), "yyyy-MM-dd hh:mm:ss", "EEEE, dd MMM yyyy"));
                Log.d("System out","weather date_____________"+exploreWeatherArrayList.get(position).getWeatherDate());


            }catch (Exception e)
            {
                e.printStackTrace();
            }



            view.addView(imageLayout);

            return imageLayout;



        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }


    }
    public void onDestroyView() {
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        editor.putString("isinreadlesslisting","false").commit();

        //  lastscrollamount=0;
     /*   LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        lp.weight = 40;
        main_ll_second.setLayoutParams(lp);

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        lp1.weight = 60;
        main_ll_first.setLayoutParams(lp1);

        setContentSize("big");*/
        ((ExploreActivity)getActivity()).pager.getLayoutParams().height= (int)getResources().getDimension(R.dimen._350sdp);
        ((ExploreActivity)getActivity()).pager.requestLayout();
        super.onDestroyView();
    }


    protected int getScroll(ListView listView) {// as list recycles views , getscrollY wont give us how much it has scrolled, hence we use this hack
        firstChildInList = listView.getChildAt(0);
        if (firstChildInList == null) return 0;
        return -firstChildInList.getTop() + listView.getFirstVisiblePosition() * firstChildInList.getHeight();
    }

    private class ExploreAdapter extends BaseAdapter {
        Context context;
        ExploreDestination exploreDestList;

        ExploreAdapter(Context context,ExploreDestination exploreDestList) {
            this.context=context;
            this.exploreDestList=exploreDestList;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.destination_detail_cell, null);

            final TextView tv_read_more_dest=(TextView)v.findViewById(R.id.tv_read_more_dest);
            final TextView tv_addintional_details_dest=(TextView)v.findViewById(R.id.tv_addintional_details_dest);
            tv_about_city=(TextView)v.findViewById(R.id.tv_about_city);
            final  TextView tv_text_dest=(TextView)v.findViewById(R.id.tv_text_dest);
            final  TextView tv_festival_desc=(TextView)v.findViewById(R.id.tv_festival_desc);
            final  TextView tv_category_title_dest=(TextView)v.findViewById(R.id.tv_category_title_dest);
            final  LinearLayout ll_main_festival_desc=(LinearLayout)v.findViewById(R.id.ll_main_festival_desc);

            final int pos=Integer.parseInt(selectedposition);
         //   if (exploreDestList.size()>0 && pos<exploreDestList.size()) {

            if(exploreDest.getDesNoOfNearByAttraction()==0)
            {
                //ll_activities_header.setVisibility(View.GONE);
                tv_explore_place.setVisibility(View.GONE);
                ll_plan_my_trip.setVisibility(View.VISIBLE);
            }else {
                //ll_activities_header.setVisibility(View.VISIBLE);
                ll_plan_my_trip.setVisibility(View.GONE);
                tv_explore_place.setVisibility(View.VISIBLE);

            }

                if (selectedposition.equalsIgnoreCase(String.valueOf(selectedposition))) {
                    tv_explore_place.setText("Discover " + exploreDestList.getDesName());
                    tv_plan_my_trip.setText("("+(String.valueOf(ExploreActivity.origincity).contains(" ")?String.valueOf(ExploreActivity.origincity).substring(0,String.valueOf(ExploreActivity.origincity).indexOf(" ")):String.valueOf(ExploreActivity.origincity))+" to "+(exploreDestList.getDesName().contains(" ")?exploreDestList.getDesName().substring(0,exploreDestList.getDesName().indexOf(" ")):exploreDestList.getDesName())+")");

                    tv_about_city.setText("About "+exploreDestList.getDesName());
                    if(!TextUtils.isEmpty(Html.fromHtml(exploreDestList.getDesFestival().trim())))
                    {
                        ll_main_festival_desc.setVisibility(View.VISIBLE);

                        tv_festival_desc.setText(Html.fromHtml(exploreDestList.getDesFestival()));

                    }
                    else
                    {


                        ll_main_festival_desc.setVisibility(View.GONE);
                    }

                    if (tv_festival_desc.getText().toString().matches("\\s*")) {

                        ll_main_festival_desc.setVisibility(View.GONE);

                    }else
                    {

                        ll_main_festival_desc.setVisibility(View.VISIBLE);

                        tv_festival_desc.setText(Html.fromHtml(exploreDestList.getDesFestival()));
                    }
                    tv_text_dest.setText(Html.fromHtml(exploreDestList.getDesShortDescription()));
                    //    tv_addintional_details_dest.setText(Html.fromHtml(exploreDestList.getDesLongDescription()));

                    String CatName="";

                    ArrayList<AllTravelType> allTravelTypes = new ArrayList<>();
                    for (int i=0;i<exploreDestList.getTravelTypelList().size();i++)
                    {
                        AllTravelType travelType = new AllTravelType();
                        if (TextUtils.isEmpty(CatName))
                        {

                            CatName=exploreDestList.getTravelTypelList().get(i).getTTypeName();
                            travelType.setTTypeName(exploreDestList.getTravelTypelList().get(i).getTTypeName());
                            travelType.setTTypeId(exploreDestList.getTravelTypelList().get(i).getTTypeId());
                            tv_category_title_dest.setText(CatName);

                        }
                        else
                        {
                            CatName=CatName+", "+exploreDestList.getTravelTypelList().get(i).getTTypeName();
                            travelType.setTTypeName(exploreDestList.getTravelTypelList().get(i).getTTypeName());
                            travelType.setTTypeId(exploreDestList.getTravelTypelList().get(i).getTTypeId());
                            tv_category_title_dest.setText(CatName);
                        }
                        allTravelTypes.add(travelType);

                    }
                    Log.d("System out","Response of category list of poi "+allTravelTypes.size());
                    String json = gson.toJson(allTravelTypes); // myObject - instance of MyObject
                    editor.putString("CategoryListPOi", json);
                    editor.commit();
                }
      //      }
            //hide plan my route and explore btn if coming from map
            if(plan_type.length()>0)
            {
                tv_explore_place.setVisibility(View.GONE);
                tv_plan_my_trip.setVisibility(View.GONE);
                ll_plan_my_trip.setVisibility(View.GONE);
            }
            tv_explore_place.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //((ExploreActivity)getActivity()).replace_fragmnet(new ExplorePlacesFragment());
                    ExplorePlacesFragment explorePlacesFragment = new ExplorePlacesFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // ((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                    changeTransaction.replace(R.id.frame, explorePlacesFragment,"ExplorePlacesFragment");

                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("placeName",exploreDestList.getDesName());
                    bundle.putString("position",position+"");
                    bundle.putString("destID",exploreDestList.getDesID()+"");
                    bundle.putString("destLat",exploreDestList.getDesLatitude());
                    bundle.putString("destLong",exploreDestList.getDesLongitude());
                    //   ExploreActivity.destattractionimages.clear();
                    bundle.putString("from","explore from dest");
                    explorePlacesFragment.setArguments(bundle);
                    changeTransaction.commit();


                }
            });
            ll_plan_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tv_plan_my_trip.performClick();
                }
            });
            tv_plan_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    start_date="";start_time="";end_date="";end_time="";
                    if(sessionManager.isLoggedIn())
                    {
                        Mytrip_Dialog(getActivity());
                    }else
                    {
                        LoginModule loginModule = new LoginModule(getActivity(),"ListingDetailsFragment");
                        loginModule.signIn();
                    }
                }
            });

            if (!TextUtils.isEmpty(Html.fromHtml(exploreDestList.getDesLongDescription())))
            {
                tv_read_more_dest.setVisibility(View.VISIBLE);
            }
            else
            {
                tv_read_more_dest.setVisibility(View.GONE);
            }
             /*change     */
            iv_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url= "https://goo.gl/lTciIP";

                    try
                    {
                       // get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + exploreDestList.getDesImage() + "&width=" + (500) + "&height=" + (500),exploreDestList.getDesImage(),"Destination: "+exploreDestList.getDesName()+"\n"+"Best Time To Visit: "+Constants.getMonthName(Integer.parseInt(exploreDestList.getDestBestTimeFrom()))+" - "+Constants.getMonthName(Integer.parseInt(exploreDestList.getDesBestTimeTo())));
                       // get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + exploreDestList.getDesImage() + "&width=" + (500) + "&height=" + (500),exploreDestList.getDesImage(),"Destination: "+exploreDestList.getDesName()+"\n"+"Best Time To Visit: "+Constants.getMonthName(Integer.parseInt(exploreDestList.getDestBestTimeFrom()))+" - "+Constants.getMonthName(Integer.parseInt(exploreDestList.getDesBestTimeTo())));
                        get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + exploreDestList.getDesImage() + "&width=" + (500) + "&height=" + (500), exploreDestList.getDesImage(), "Take a peek at " + exploreDestList.getDesName() + ".\n" + "Plan your adventure filled roadtrip with Happy Roads."+Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));


                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }


            });

            tv_read_more_dest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (tv_read_more_dest.getText().toString().equalsIgnoreCase("[Read Less]")) {
                        Log.d("System out","IF in read_____________");
                        editor.putString("isinreadlesslisting","false").commit();
                        ExploreList.setSelectionAfterHeaderView();
                        settooriginal();
                        //   tv_text_dest.setMaxLines(3);
                        tv_text_dest.setText(Html.fromHtml(exploreDestList.getDesShortDescription()));
                        tv_read_more_dest.setText("[Read More]");
                        readmoreclicked=false;
                        ExploreList.smoothScrollToPosition(0);

                    } else
                    {
                        Log.d("System out","Else in read_____________");
                        readmoreclicked=true;
                        settopimage();
                        editor.putString("isinreadlesslisting","true").commit();
                        tv_text_dest.append(Html.fromHtml(exploreDestList.getDesLongDescription()));
                        tv_read_more_dest.setText("[Read Less]");

                    }
                }
            });


            if (sharedpreferences.getString("isinreadlesslisting","false").equalsIgnoreCase("true"))
            {
                settopimage();
                tv_text_dest.append(Html.fromHtml(exploreDestList.getDesLongDescription()));
                tv_read_more_dest.setText("[Read Less]");

            }
            else
            {
                settooriginal();
                ExploreList.setSelectionAfterHeaderView();
                // tv_text_dest.setMaxLines(3);
                tv_text_dest.setText(Html.fromHtml(exploreDestList.getDesShortDescription()));
                tv_read_more_dest.setText("[Read More]");

            }


            return v;
        }
    }
    public void Mytrip_Dialog(final Activity activity) {
        myTripDialog=new Dialog(activity);
        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.pop_up_my_trips);
        tv_start_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_start_date_my_trip);
        tv_return_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_return_date_my_trip);
        final TextView tv_for_better_dates=(TextView)myTripDialog.findViewById(R.id.tv_for_better_dates);
        final ImageView iv_swt_off_my_trip=(ImageView)myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
        final LinearLayout ll_start_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_return_date_my_trip);
      et_trip_name=(EditText)myTripDialog.findViewById(R.id.et_trip_name);
         tv_save_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_save_my_trip);
        sessionManager1=new SessionManager(activity);

        Calendar now = Calendar.getInstance();


        dpd = DatePickerDialog.newInstance(
                ListingDetailsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf.format(new Date());

        Date date = null;
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 0);


        dpd.setMinDate(calendar);
        tpd = TimePickerDialog.newInstance(
                ListingDetailsFragment.this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE),false
        );

        //  tpd.setMinTime(calendar.getTime().getHours(),calendar.getTime().getMinutes(),calendar.getTime().getSeconds());

        tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  if(tripFlag==true) {
                    select = "start";
                    start_date = "";
                    start_time = "";


                    dpd.show(activity.getFragmentManager(), "Datepickerdialog");
               // }


            }

        });

        tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(tripFlag==true) {
                    select = "stop";
                    end_date = "";
                    end_time = "";

                    dpd.show(activity.getFragmentManager(), "Datepickerdialog");
             //   }
                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            }
        });
       /* iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tripFlag==false)
                {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                    tv_for_better_dates.setVisibility(View.GONE);
                    ll_start_date_my_trip.setAlpha(1);
                    ll_return_date_my_trip.setAlpha(1);
                    tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tripFlag==true) {
                                select = "start";
                                start_date = "";
                                start_time = "";


                                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                            }


                        }

                    });

                    tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tripFlag==true) {
                                select = "stop";
                                end_date = "";
                                end_time = "";

                                dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                            }
                            //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                        }
                    });
                    tripFlag=true;



                }
                else
                {
                    iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                    tv_for_better_dates.setVisibility(View.VISIBLE);
                    ll_start_date_my_trip.setAlpha((float) 0.6);
                    ll_return_date_my_trip.setAlpha((float) 0.6);
                    tripFlag=false;
                }

            }
        });*/

/*
        tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_trip_name.getText().toString().length() > 0) {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {
                        Constants.show_error_popup(activity, "Please select Trip start date", activity.findViewById(R.id.error_layout));
                    }

                    else
                    {
                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;

                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                final int pos = Integer.parseInt(selectedposition);
                                CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                            }
                            else
                            {
                                Constants.show_error_popup(activity, "Please change Trip return date", activity.findViewById(R.id.error_layout));
                            }
                        }
                        else {
                            Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                            final int pos = Integer.parseInt(selectedposition);
                            CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                        }
                    }
                } else

                {
                    Constants.show_error_popup(activity, "Please Enter Trip Name", activity.findViewById(R.id.error_layout));
                }
            }

        });
*/

        tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(tv_start_date_my_trip.getText().length() == 0 ) {
                    Constants.show_error_popup(activity, "Select your trip dates.", activity.findViewById(R.id.error_layout));

                }

               else*/ if (et_trip_name.getText().toString().length() > 0) {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {
                        Constants.show_error_popup(activity, "Select your trip dates.", activity.findViewById(R.id.error_layout));
                    }

                    else
                    {
                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;

                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;
                                int pos=Integer.parseInt(selectedposition);
                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDest.getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta",date.toString());
                                    tripdate=sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate)) {
                                    CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDest.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                                }
                                else {
                                    try {
                                        Constants.show_error_popup(activity, "Your destination requires " + Constants.convert_minute_hrs(exploreDest.getDuration().doubleValue()) + " of travel. Plan accordingly.", activity.findViewById(R.id.error_layout));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                    }
                                }
                            }
                            else
                            {
                                int pos=Integer.parseInt(selectedposition);
                                Constants.show_error_popup(activity, "Your destination requires " + Constants.convert_minute_hrs(exploreDest.getDuration().doubleValue()) + " of travel. Plan accordingly.", activity.findViewById(R.id.error_layout));
                            }
                        }
                        else {
                            int pos=Integer.parseInt(selectedposition);
                            CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDest.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                        }
                    }
                } else

                {
                    Constants.show_error_popup(activity, "Please Enter Trip Name", activity.findViewById(R.id.error_layout));
                }



            }

        });

 /*       tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_trip_name.getText().toString().length() > 0) {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {
                        Constants.show_error_popup(activity, "Please select Trip start date", activity.findViewById(R.id.error_layout));
                    }

                    else
                    {

                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;




                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;
                                int pos=Integer.parseInt(selectedposition);
                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDestList.getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta",date.toString());
                                    tripdate=sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate)) {


                                    CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                                }
                                else {
                                    try {
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                    }
                                }
                            }
                            else
                            {
                                Constants.show_error_popup(activity, "Please change Trip return date", activity.findViewById(R.id.error_layout));
                            }
                        }
                        else {
                            Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                            int pos=Integer.parseInt(selectedposition);
                            CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                        }
                    }
                } else

                {
                    Constants.show_error_popup(activity, "Please Enter Trip Name", activity.findViewById(R.id.error_layout));
                }
            }

        });*/
        ImageView ImgClosepopup=(ImageView)myTripDialog.findViewById(R.id.ImgClosepopup);
        myTripDialog.show();
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

    }
    public void CreateTripApi(final String tripname, String sourceId, String destinationId, String userId, final String source, final String destination, final Activity activity, final Dialog myTripDialog) {
        // get & set progressbar dialog
        final SessionManager sessionManager1 = new SessionManager(activity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String tripdate=start_date+" "+start_time+":00";
        String tripreturndate=end_date+" "+end_time+":00";

        tripdate_eta="";trip_return_date_eta="";

        Log.d("start_date",tripdate);
        try {
            tripdate=  Constants.formatDate(tripdate,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");

        } catch (ParseException e) {
            e.printStackTrace();
            tripdate="";

        }

        try{
            tripreturndate=  Constants.formatDate(tripreturndate,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        }
        catch (ParseException e){

            tripreturndate="";

        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        Date date = null;
        final int pos=Integer.parseInt(selectedposition);
        try {
            date = sdf.parse(tripdate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] convertime = splitToComponentTimes(exploreDest.getDuration().doubleValue());
            calendar.add(Calendar.HOUR, convertime[0]);
            calendar.add(Calendar.MINUTE, convertime[1]);
            Log.d("trip_Date_eta",date.toString());
            tripdate_eta=sdf.format(calendar.getTime());
            //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
            tripdate_eta="";

        }
        try
        {
            date = sdf.parse(tripreturndate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] convertime = splitToComponentTimes(exploreDest.getDuration().doubleValue());
            calendar.add(Calendar.HOUR, convertime[0]);
            calendar.add(Calendar.MINUTE, convertime[1]);
            trip_return_date_eta=sdf.format(calendar.getTime());
            //  trip_returndate_eta=  Constants.formatDate(dateeta,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_date_eta="";

        }


        pb_dialog = Constants.get_dialog(activity, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        String json = "[{\"TripName\": \"" + tripname + "\",\"TripDate\":\"" + tripdate  + "\",\"TripEndDateTime\":\"" + tripdate_eta+"\",\"TripReturnDate\":\"" + tripreturndate +"\",\"TripReturnDateETA\":\"" + trip_return_date_eta   +"\", \"TripGoingTo_Start\": "+sourceId+",\"TripDestination_End\":"+destinationId+", \"TripUmID\":"+sessionManager1.get_Authenticate_User().getUmId()+" }]";
        Log.d("json_CreateTrip",json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<CreateTripApi> call = apiService.createTrip(json);
        call.enqueue(new Callback<CreateTripApi>() {
            @Override
            public void onResponse(Call<CreateTripApi> call, Response<CreateTripApi> response) {
                if (response.body() != null) {

                    CreateTripApi createTripApi = new CreateTripApi();
                    createTripApi = response.body();
                    if (createTripApi.getResStatus() == true) {//call OTP API here

                        // getDistanceOnRoad(23.034325,72.501297,23.725664,72.533523,createTripApi,tripname,source);

                        add_trip_in_to_database(source,tripname,createTripApi,activity,myTripDialog);

                    } else {
                        pb_dialog.dismiss();
                        Constants.show_error_popup(activity, response.body().getResDescription(), activity.findViewById(R.id.error_layout));
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateTripApi> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(activity)) {
                    Constants.show_error_popup(activity, activity.getString(R.string.internet_error), activity.findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(activity, getActivity().getString(R.string.server_error), activity.findViewById(R.id.error_layout));
                }

            }
        });


    }
    public void add_trip_in_to_database(final String sourcename, final String tripname, final CreateTripApi createTripApi, final Activity activity, final Dialog myTripDialog)
    {
        final   ArrayList<ExploreDestination> exploreDestList1=new ArrayList<>();

        final SessionManager sessionManager1 = new SessionManager(activity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{     \"umId\": \"" + sessionManager1.get_Authenticate_User().getUmId() + "\",     \"radius\": \"999999\",   \"desName\": \"" + ExploreActivity.origincity + "\",   \"desLongitude\": \"999\",   \"desLatitude\": \"999\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":100,\"desNotIncludeThisID\":0 }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Log.d("System out", " json " + json);

        trip_return_end_date="";trip_return_end_time="";trip_end_date_eta="";trip_end_time_eta="";

        Log.d("tripdateeta",tripdate_eta+"ret"+trip_return_date_eta);
        try{
            trip_end_date_eta=Constants.formatDate(tripdate_eta,"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");

            trip_end_time_eta=Constants.formatDate(tripdate_eta,"yyyy-MM-dd HH:mm:ss","HH:mm");
            trip_return_end_date=Constants.formatDate(trip_return_date_eta,"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");

            trip_return_end_time=Constants.formatDate(trip_return_date_eta,"yyyy-MM-dd HH:mm:ss","HH:mm");

        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_end_date="";trip_return_end_time="";trip_end_date_eta="";trip_end_time_eta="";
        }
        Log.d("tripdateeta1",tripdate_eta+"ret"+trip_return_date_eta+"trip_end_date_eta:"+trip_end_date_eta);


        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();

                if (response.body() != null) {


                    exploreDestList1.clear();
                    exploreDestList1.addAll(response.body());
                    if (exploreDestList1.size() > 0)
                    {


                        String url=Constants.TimbThumb_ImagePath+exploreDestList1.get(0).getDesImage()+"&width="+50+"&height="+50;

                        //stuff that updates ui
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(url))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




                        dataSource.subscribe(new BaseBitmapDataSubscriber()
                        {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap)
                            {
                                if (dataSource.isFinished() && bitmap != null)
                                {
                                    Log.d("Bitmap", "has come");
                                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                    final int pos=Integer.parseInt(selectedposition);


                                    Create_Trip create_trip = new Create_Trip();
                                    create_trip.setSource(ExploreActivity.origincity);
                                    create_trip.setDestination(exploreDest.getDesName());

                                    create_trip.setStartdate(start_date);
                                    create_trip.setStarttime(start_time);

                                    create_trip.setReturn_start_date(end_date);
                                    create_trip.setReturn_start_time(end_time);

                                    create_trip.setEnddate(trip_end_date_eta);
                                    create_trip.setEndtime(trip_end_time_eta);
                                    create_trip.setReturn_end_date(trip_return_end_date);
                                    create_trip.setReturn_end_time(trip_return_end_time);

                                    if(end_date.length()>1)
                                    {
                                        create_trip.setReturnstatus(true);
                                    }


                                    create_trip.setTripname(tripname);
                                    create_trip.setSource_latitude(Double.valueOf(exploreDestList1.get(0).getDesLatitude()));
                                    create_trip.setSource_longitude(Double.valueOf(exploreDestList1.get(0).getDesLongitude()));
                                    create_trip.setDestination_latitude(Double.valueOf(exploreDest.getDesLatitude()));
                                    create_trip.setDestination_longitude(Double.valueOf(exploreDest.getDesLongitude()));


                                    create_trip.setKM(exploreDest.getDistance()*1000);
                                    create_trip.setTime(exploreDest.getDuration().doubleValue());
                                    create_trip.setTripstatus(createTripApi.getTripStatus());
                                    create_trip.setTriprating(createTripApi.getTripRating());
                                    create_trip.setTripId(createTripApi.getTripId());
                                    create_trip.setSourceId(createTripApi.getTripGoingToStart());
                                    create_trip.setDestinationId(createTripApi.getTripDestinationEnd());
                                    create_trip.setSource_image(Constants.getImageBytes(bitmap));
                                    create_trip.setDesNoOfNearByAttraction(exploreDest.getDesNoOfNearByAttraction());

                                    create_trip.setDestination_image(Constants.getImageBytes(destinationbmp));
                                    create_trip.setReturnKM(exploreDest.getDistance()*1000);
                                    create_trip.setReturnTime(exploreDest.getDuration().doubleValue());
                                    DBHandler dbHandler=new DBHandler(activity);

                                    dbHandler.add_Trip_into_Table(create_trip);

                                    pb_dialog.dismiss();

                                    myTripDialog.dismiss();

                                    Bundle b=new Bundle();
                                    b.putInt("tripId",createTripApi.getTripId());
                                    b.putString("plantype", "STOD");
                                    ((ExploreActivity)activity).replace_fragmnet_bundle(new RoutePlanningFragment(),b);

                                    dataSource.close();
                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }

                            }
                        }, CallerThreadExecutor.getInstance());

                    } else {
                        Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(),  error_layout);
                    }

                    pb_dialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });
    }

    public void  get_bitmap_from_freco(final String url)
    {




//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");
                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    destinationbmp=bitmap;
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                destinationbmp=null;
            }
        }, CallerThreadExecutor.getInstance());


    }
    public void  get_bitmap_from_freco_download(final String url, final String name,final  String besttime)
    {
        Log.d("System out","sharing image path "+url);


//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    dataSource.close();
                    DownloadCode code = new DownloadCode(getActivity(),Constants.TimbThumb_ImagePath,name,bitmap,besttime);
                    code.share_image_text_GPLUS();


                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }

    public static boolean CheckDates(String startDate, String endDate) {



        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = false;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare",String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare",e.toString());
        }
        return b;
    }
    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }
}
