package com.bpcl.happyroads;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideBottomExit;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.bpcl.happyroads.Pojo.DeleteNotification;
import com.bpcl.happyroads.Pojo.NotificationList;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/12/2016.
 */
public class NotificationFragment extends Fragment {
    RecyclerView lv_notification;
    View no_notification;
    Dialog pb_dialog;
    SessionManager sessionManager;
    int counter = 0;
    RelativeLayout error_layout;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationList> notificationLists =new ArrayList<>();
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notification_fragment, container, false);
        lv_notification = (RecyclerView) v.findViewById(R.id.lv_notification);
        error_layout=(RelativeLayout)v.findViewById(R.id.error_layout);
        no_notification = (View) v.findViewById(R.id.no_trip_found_layout);

        ((ExploreActivity)getActivity()).iv_setting_header.setVisibility(View.VISIBLE);
        sessionManager = new SessionManager(getActivity());
        notificationAdapter = new NotificationAdapter();
        lv_notification.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
     //  ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });

        lv_notification.setLayoutManager(layoutManager);

        ((ExploreActivity)getActivity()).iv_setting_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).replace_fragmnet(new SettingFragment());
            }
        });
        counter++;
        getNotification();
        return v;
    }
    private void getNotification() {
        // get & set progressbar dialog
        if (counter == 1) {
            pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(false);
            // pb_dialog.show();
            pb_dialog.dismiss();
        }

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            String json = "[{\"ntfUsrId\":" + sessionManager.get_Authenticate_User().getUmId() + ",  \"pageindex\":" + counter + " }]";
            Log.d("json_CreateTrip", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<ArrayList<NotificationList>> call = apiService.NotificationList(json);
            call.enqueue(new Callback<ArrayList<NotificationList>>() {
                @Override
                public void onResponse(Call<ArrayList<NotificationList>> call, Response<ArrayList<NotificationList>> response) {
                    if (response.body() != null) {
                        if(response.body().get(0).getResStatus() == true) {
                            if (counter == 1) {
                                notificationLists.clear();
                                notificationLists.addAll(response.body());
                                counter++;
                                lv_notification.setAdapter(notificationAdapter);
                                getNotification();


                            } else {
                                notificationLists.addAll(response.body());
                                lv_notification.setAdapter(notificationAdapter);
                                counter++;
                                getNotification();

                            }
                        }
                        else
                        {
                            if (notificationLists.size() == 0) {

                                no_notification.setVisibility(View.VISIBLE);
                                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);

                            }
                        }
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<NotificationList>> call, Throwable t) {
                    // Log error here since request failed
                    t.printStackTrace();
                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                    }
                }
            });

    }
    public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
        ViewHolder viewHolder;
        @Bind(R.id.iv_notification_icon_adapter)
        ImageView ivNotificationIconAdapter;
        @Bind(R.id.ll_notification_icon_adapter)
        LinearLayout llNotificationIconAdapter;
        @Bind(R.id.tv_text_notification_adapter)
        TextView tvTextNotificationAdapter;
        @Bind(R.id.tv_date_notification_adapter)
        TextView tvDateNotificationAdapter;
        @Bind(R.id.iv_close_notification_adapter)
        ImageView ivCloseNotificationAdapter;
        @Bind(R.id.ll_close_notification_adapter)
        LinearLayout llCloseNotificationAdapter;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.notification_adapter_fragment, parent, false);
            viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.tvTextNotificationAdapter.setText(notificationLists.get(position).getNtfMsgText());
            try {
                holder.tvDateNotificationAdapter.setText( Constants.formatDate(notificationLists.get(position).getDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy"));
            } catch (ParseException e) {
                e.printStackTrace();
                holder.tvDateNotificationAdapter.setText("");
            }

            holder.llCloseNotificationAdapter.setId(position);

            holder.llCloseNotificationAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {


                    BounceTopEnter mBasIn = new BounceTopEnter();
                    SlideBottomExit mBasOut = new SlideBottomExit();
                    final NormalDialog dialog = new NormalDialog(getActivity());
                    dialog.isTitleShow(false)//
                            .cornerRadius(5)//
                            .content("Are you sure to delete this notification?")//
                            .contentGravity(Gravity.CENTER)//
                            .contentTextColor(getResources().getColor(R.color.colorWhite))//
                            .bgColor(getResources().getColor(R.color.colorAccent))
                            .btnNum(2)
                            .btnText(getString(R.string.yes), getString(R.string.no))
                            .btnTextSize(16f, 16f)//
                            .btnTextColor(getResources().getColor(R.color.colorWhite), getResources().getColor(R.color.colorWhite))//

                            .widthScale(0.85f)//
                            .showAnim(mBasIn)//
                            .dismissAnim(mBasOut)//
                            .show();

                    dialog.setOnBtnClickL(
                            new OnBtnClickL() {
                                @Override
                                public void onBtnClick() {

                                    DeleteNotification(view.getId());

                                    dialog.dismiss();
                                }


                            },
                            new OnBtnClickL() {
                                @Override
                                public void onBtnClick() {

                                    dialog.dismiss();
                                }
                            });

                }


            });

        }

        @Override
        public int getItemCount() {
            return notificationLists.size();
        }

        @OnClick({R.id.iv_close_notification_adapter})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_close_notification_adapter:
                    break;

            }
        }


        class ViewHolder extends RecyclerView.ViewHolder {
            @Bind(R.id.iv_notification_icon_adapter)
            ImageView ivNotificationIconAdapter;
            @Bind(R.id.ll_notification_icon_adapter)
            LinearLayout llNotificationIconAdapter;
            @Bind(R.id.tv_text_notification_adapter)
            TextView tvTextNotificationAdapter;
            @Bind(R.id.tv_date_notification_adapter)
            TextView tvDateNotificationAdapter;
            @Bind(R.id.iv_close_notification_adapter)
            ImageView ivCloseNotificationAdapter;
            @Bind(R.id.ll_close_notification_adapter)
            LinearLayout llCloseNotificationAdapter;

            ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }
    }

    private void DeleteNotification(final int id) {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        // pb_dialog.show();
        pb_dialog.dismiss();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{\"ntfId\":" + notificationLists.get(id).getNtfId() + " }]";
        Log.d("json_CreateTrip", json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<DeleteNotification>> call = apiService.DeleteNotification(json);
        call.enqueue(new Callback<ArrayList<DeleteNotification>>() {
            @Override
            public void onResponse(Call<ArrayList<DeleteNotification>> call, Response<ArrayList<DeleteNotification>> response) {
                if (response.body() != null) {
                    if(response.body().get(0).getResStatus() == true) {

                        notificationLists.remove(id);
                        notificationAdapter.notifyDataSetChanged();
                        lv_notification.smoothScrollToPosition(id);

                        if (notificationLists.size() == 0) {

                            no_notification.setVisibility(View.VISIBLE);
                            ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);

                        }


                    }
                    else
                    {

                    }




                }

            }

            @Override
            public void onFailure(Call<ArrayList<DeleteNotification>> call, Throwable t) {
                // Log error here since request failed
                t.printStackTrace();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).iv_setting_header.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);


    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()). exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).iv_setting_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Notifications");    }
}
