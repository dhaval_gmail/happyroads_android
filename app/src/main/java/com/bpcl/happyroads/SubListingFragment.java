package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.Pojo.AddFavourite;
import com.bpcl.happyroads.Pojo.AllTravelType;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DownloadCode;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.LoginModule;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ADMIN on 8/18/2016.
 */
public class SubListingFragment extends Fragment {
    //@Bind(R.id.lv_sub_list)
    String From = "", Name = "", Nameids = "", isfrom = "", progress = "999999";

    Dialog dialog;
    String[] sub = new String[0];
    PrefferedAdapter prefferedAdapter;
    ArrayList<String> imgTypeArray = new ArrayList<>();
    ArrayList<String> imgTypeArrayIDs = new ArrayList<>();
    boolean like = false;
    LinearLayout ll_main_sb_nearby_attractions;
    SeekBar sb_nearby_attraction;
    Boolean pref_select = false;
    ContentLoadingProgressBar sublist_content_progressbar;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    ListView lvSubList;
    RelativeLayout error_layout;
    ArrayList<AddFavourite> addFavList = new ArrayList<AddFavourite>();
    ListView lv_prefferedList;
    //TextView tv_progress;
  TextView tv_max_progress,tv_min_progress,tv_progress;


    ArrayList<ExploreDestination> exploreDestList = new ArrayList<>();
    Gson gson = new Gson();
    String from = "";
    ImageView iv_like_icon;
    SessionManager sessionManager;
    SubListingAdapter subListingAdapter;
    private float px;
    String prgrs="999999";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sublist_fragment, container, false);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.GONE);
        lvSubList = (ListView) v.findViewById(R.id.lv_sub_list);
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);

        // tv_progress = (TextView) v.findViewById(R.id.tv_progress);
        tv_max_progress=(TextView)v.findViewById(R.id.tv_max_progress);
        tv_min_progress =(TextView) v.findViewById(R.id.tv_min_progress);
        tv_progress =(TextView) v.findViewById(R.id.tv_progress);


        //((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        error_layout = (RelativeLayout) v.findViewById(R.id.error_layout);
        sb_nearby_attraction = (SeekBar) v.findViewById(R.id.sb_nearby_attraction);
        // BitmapDrawable bd = writeOnDrawable(R.drawable.textbox_edit, Double.toString(50));

        //  ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.VISIBLE);
        // ((ExploreActivity) getActivity()).edcityname.setText("Ex-Bengaluru");
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.VISIBLE);
        ll_main_sb_nearby_attractions = (LinearLayout) v.findViewById(R.id.ll_main_sb_nearby_attractions);
        sublist_content_progressbar = (ContentLoadingProgressBar) v.findViewById(R.id.sublist_content_progressbar);
        sublist_content_progressbar.setVisibility(View.GONE);
        //imgTypeArray.add("Art and Culture");
        //   Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));
        Constants.destSelectionfrom = "sublisting";

        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        String jsonOutput = sharedpreferences.getString("CategoryList", "");
        Type listType = new TypeToken<ArrayList<AllTravelType>>() {
        }.getType();
        ArrayList<AllTravelType> posts = gson.fromJson(jsonOutput, listType);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lvSubList.setNestedScrollingEnabled(true);
        }
        sessionManager = new SessionManager(getActivity());
        Resources r = getResources();
        px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,0, r.getDisplayMetrics());
        //Instruct GSON to parse as a Post array (which we convert into a list)

        imgTypeArray.clear();

        for (AllTravelType task : posts) {
            imgTypeArray.add(task.getTTypeName());
            imgTypeArrayIDs.add(String.valueOf(task.getTTypeId()));
        }


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (Name.equalsIgnoreCase("")) {
                Name = bundle.getString("name", "");
              //  Nameids = bundle.getString("nameids", "");
                progress = bundle.getString("progress", "");
                from = bundle.getString("from", "");

                Log.d("System out", "Name___" + Name);
                Log.d("System out", "from" + from);
                Log.d("System out", "namenameids" + Nameids);
            }
        } else {
            Name = "";
            progress = "999999";
        }


        ButterKnife.bind(this, v);


        if (progress.equalsIgnoreCase("999999")) {


            if (from.equalsIgnoreCase("nearbyplaces")) {
                Log.d("System out", "if______" + from);

                ll_main_sb_nearby_attractions.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).edcityname.setText(Name);

                ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
            } else {
                Log.d("System out", "else______" + from);
                ll_main_sb_nearby_attractions.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).ll_top_header.setPadding(0, (int) getResources().getDimension(R.dimen._8sdp), 0, 0);

            }
            setheader();


        } else {
            Log.d("System out", "progress_______" + progress);
            Log.d("System out", "else______");
            Constants.fromSublistingforSb = true;
        //   sb_nearby_attraction.setProgress(Integer.parseInt(progress));

           // sb_nearby_attraction.setThumb(writeOnDrawable(R.drawable.radius_icon_home, progress));

           // set_progress_seekbar(Integer.parseInt(progress));
           // setSeekBar(Integer.parseInt(progress));
            tv_progress.setText(Integer.parseInt(progress)+"\nKMs");


            // Log.d("System out","tempvalue in sublisting"+((ExploreActivity)getActivity()).tempvalue);

          //  tv_progress.setX(((ExploreActivity)getActivity()).tempvalue);
           // set_progress_seekbar(Integer.parseInt(progress));


            ll_main_sb_nearby_attractions.setVisibility(View.VISIBLE);
            ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
            ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        }
        System.out.println("imgTypeArray size" + imgTypeArray.size() + "listofcat" + posts.size());

        ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ExploreActivity) getActivity()).onBackPressed();
            }
        });


        ((ExploreActivity) getActivity()).ll_img_type_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowPopup();
            }
        });


        sb_nearby_attraction.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress1, boolean fromUser) {

              //  set_progress_seekbar(progress1);
              //  setSeekBar(progress1);
                tv_progress.setText(progress1+"\nKMs");

                progress= String.valueOf(progress1);
                // tv_max_progress.setVisibility(View.GONE);



              /*  progress=String.valueOf(progress1);
              //  tv_progress.setVisibility(View.VISIBLE);
                float val = (progress1* (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                tv_progress.setX((float)seekBar.getX()-px+ val + seekBar.getThumbOffset() /2-seekBar.getThumbOffset());
                tv_progress.setText(progress+" kms");*/
                // tv_progress.setX(seekBar.getX()+ 100 + progress1 + seekBar.getThumbOffset());

               /* int pos = seekBar.getProgress();
                Toast.makeText(getActivity(), "discrete = "+String.valueOf(progress1), Toast.LENGTH_SHORT).show();

                TextView tv=new TextView(getActivity());
                tv.setText(progress);
                BitmapDrawable bd = writeOnDrawable(R.drawable.textbox_edit,progress);
                bd.setBounds(new Rect(0,0,
                        bd.getIntrinsicWidth(),
                        bd.getIntrinsicHeight()
                ));
                seekBar.setThumb(bd);*/


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
              //  tv_max_progress.setVisibility(View.GONE);


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //  tv_progress.setVisibility(View.GONE);
              //  set_progress_seekbar(seekBar.getProgress());
                getDestinationAPI();


            }
        });
        exploreDestList = new ArrayList<>();
        subListingAdapter = new SubListingAdapter(getActivity(), exploreDestList);
        lvSubList.setAdapter(subListingAdapter);

        Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            getDestinationAPI();

        } else {

            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
        }


        //    SubListingAdapter subListingAdapter = new SubListingAdapter(getActivity(),exploreDestList);
        //   lvSubList.setAdapter(subListingAdapter);


        return v;
    }

    public void getDestinationAPI() {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id = "1";
            String tempProgress="";
        if (sessionManager.isLoggedIn()) {
            id = sessionManager.get_Authenticate_User().getUmId() + "";
        }

        if (from.equalsIgnoreCase("nearbyplaces")) {

            json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + 100 + "\",   \"desName\": \"\",   \"desLongitude\": \"" + ListingDetailsFragment.subselectedLong + "\",   \"desLatitude\": \"" + ListingDetailsFragment.subselectedLat + "\", \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + ListingDetailsFragment.destination_exclude_id1 + " }]";
            Log.d("System out", "Explore Destination from nearby____" + json);

        } else if (((ExploreActivity)getActivity()).selectedIDs.equalsIgnoreCase("")) {
            if (progress.equalsIgnoreCase("0")) {
                tempProgress = "1";
            }else
            {
                tempProgress=progress;
            }
            json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + tempProgress + "\",   \"desName\": \"\",   \"desLongitude\": \"" + Constants.selectedLong + "\",   \"desLatitude\": \"" + Constants.selectedLat + "\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + ExploreActivity.destination_exclude_id + "}]";
            Log.d("System out", "Explore Destination json " + json);



        } else {
            if (progress.equalsIgnoreCase("0")) {
                tempProgress = "1";
            }else
            {
                tempProgress=progress;
            }
            json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + tempProgress + "\",   \"desName\": \"\",   \"desLongitude\": \"" + Constants.selectedLong + "\",   \"desLatitude\": \"" + Constants.selectedLat + "\",      \"TravelTypeTag\":\"" + ((ExploreActivity)getActivity()).selectedIDs.substring(0, ((ExploreActivity)getActivity()).selectedIDs.length() - 1) + "\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + ExploreActivity.destination_exclude_id + "}]";
            Log.d("System out", "Explore Destination json " + json);

        }

        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();
                pb_dialog.dismiss();
                if (response.body() != null) {


                    exploreDestList.clear();
                    exploreDestList.addAll(response.body());
                    if (exploreDestList.get(0).getResStatus() == true) {
                        // Collections.reverse(exploreDestList);

                        String json = gson.toJson(exploreDestList); // myObject - instance of MyObject
                        editor.putString("DestList", json);
                        editor.commit();

                        subListingAdapter.notifyDataSetChanged();

                    } else {
                        exploreDestList.clear();
                        subListingAdapter.notifyDataSetChanged();
                        Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", error_layout);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }


            }
        });
    }else
        {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
        }
    }

    public void getDestinationAPIFilter(String Nameids, String rate, String startmonth, int endmonth, int km) {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id = "1";
        if (sessionManager.isLoggedIn()) {
            id = sessionManager.get_Authenticate_User().getUmId() + "";
        }
if(km==0000)
{
    km=sb_nearby_attraction.getProgress();
}
        if(km==0)
        {
            km=1;
        }

        String[] split= startmonth.split(",");
        if (Nameids.equalsIgnoreCase("")) {

            json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + km + "\",   \"desName\": \"\",   \"desLongitude\": \"" + Constants.selectedLong + "\",   \"desLatitude\": \"" + Constants.selectedLat + "\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + ExploreActivity.destination_exclude_id + ",\"desRating\":" + rate + ",\"destBestTimeFrom\":" + (!startmonth.equalsIgnoreCase("")?split[0]:0) + ",\"desBestTimeTo\":" +  (!startmonth.equalsIgnoreCase("")?(split.length>1?split[split.length-1]:split[0]):0) + ",\"monthsCommaSapList\":\"" + (!startmonth.equalsIgnoreCase("")?startmonth.substring(0,startmonth.length()-1):"") + "\"}]";
            Log.d("System out", "Explore Destination from nearby____" + json);

        } else {
            json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + km + "\",   \"desName\": \"\",   \"desLongitude\": \"" + Constants.selectedLong + "\",   \"desLatitude\": \"" + Constants.selectedLat + "\",      \"TravelTypeTag\":\"" + Nameids.substring(0, Nameids.length() - 1) + "\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + ExploreActivity.destination_exclude_id + ",\"desRating\":" + rate + ",\"destBestTimeFrom\":" +  (!startmonth.equalsIgnoreCase("")?split[0]:0) + ",\"desBestTimeTo\":" + (!startmonth.equalsIgnoreCase("")?(split.length>1?split[split.length-1]:split[0]):0) + ",\"monthsCommaSapList\":\"" + (!startmonth.equalsIgnoreCase("")?startmonth.substring(0,startmonth.length()-1):"") + "\" }]";

            Log.d("System out", "Explore Destination from nearby____" + json);


        }

        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();
                pb_dialog.dismiss();
                if (response.body() != null) {


                    exploreDestList.clear();
                    exploreDestList.addAll(response.body());
                    if (exploreDestList.get(0).getResStatus() == true) {


                        // Collections.reverse(exploreDestList);

                        String json = gson.toJson(exploreDestList); // myObject - instance of MyObject
                        editor.putString("DestList", json);
                        editor.commit();

                        subListingAdapter.notifyDataSetChanged();

                    } else {
                        exploreDestList.clear();
                        subListingAdapter.notifyDataSetChanged();
                        Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", error_layout);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }


            }
        });
    }

    private void ShowPopup() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.preffered_category);
        lv_prefferedList = (ListView) dialog.findViewById(R.id.lv_prefferedList);
        TextView tv_apply_preferred_cat = (TextView) dialog.findViewById(R.id.tv_apply_preferred_cat);
        tv_apply_preferred_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    getDestinationAPI();

                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);

                }


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        final ImageView ImgClosepopup = (ImageView) dialog.findViewById(R.id.ImgClosepopup);
        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        prefferedAdapter = new PrefferedAdapter(this, imgTypeArray, imgTypeArrayIDs);
        lv_prefferedList.setAdapter(prefferedAdapter);

        dialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        Log.d("System out", "onDestroyView called_____");
        //((ExploreActivity)getActivity()).toolbar.getLayoutParams().height =(int)getResources().getDimension(R.dimen._50sdp);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.logo_navigation);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        // ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ll_main_sb_nearby_attractions.setVisibility(View.GONE);
    }

    private class SubListingAdapter extends BaseAdapter {
        Context context;
        ArrayList<ExploreDestination> exploreDest = new ArrayList<>();

        public SubListingAdapter(Context subListingFragment, ArrayList<ExploreDestination> exploreDest) {
            this.context = subListingFragment;
            this.exploreDest = exploreDest;
        }

        @Override
        public int getCount() {
            return exploreDest.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = convertView;
            v = inflater.inflate(R.layout.favourites_list, null);
            SimpleDraweeView iv_img_favourites = (SimpleDraweeView) v.findViewById(R.id.iv_img_favourites);
            final TextView tv_img_name = (TextView) v.findViewById(R.id.tv_img_name);
            final TextView tv_time_km_fav_list = (TextView) v.findViewById(R.id.tv_time_km_fav_list);
            final TextView tv_from_to_month = (TextView) v.findViewById(R.id.tv_from_to_month);
            final TextView tv_rating_dest = (TextView) v.findViewById(R.id.tv_rating_dest);
            // iv_img_favourites.getHierarchy().setProgressBarImage(R.drawable.spinningwheel);

            // final String placeName=tv_img_name.getText().toString();
            iv_like_icon = (ImageView) v.findViewById(R.id.iv_like_icon);
            ImageView iv_share_img = (ImageView) v.findViewById(R.id.iv_share_img);
            iv_share_img.setId(position);
            iv_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url= "https://goo.gl/lTciIP";

                    try {
                        //get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + exploreDest.get(position).getDesImage() + "&width=" + (500) + "&height=" + (500), exploreDest.get(position).getDesImage(), "Destination: " + exploreDest.get(position).getDesName() + "\n" + "Best Time To Visit: " + Constants.getMonthName(Integer.parseInt(exploreDest.get(position).getDestBestTimeFrom())) + " - " + Constants.getMonthName(Integer.parseInt(exploreDest.get(position).getDesBestTimeTo())));
                        get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + exploreDest.get(position).getDesImage() + "&width=" + (500) + "&height=" + (500), exploreDest.get(position).getDesImage(), "Take a peek at " + exploreDest.get(position).getDesName() + ".\n" + "Plan your adventure filled roadtrip with Happy Roads."+Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            });


            // iv_img_favourites.setImageURI(Constants.ImagePath+exploreDest.get(position).getDesImage());
            //  SimpleDraweeView fc_p=(SimpleDraweeView)findViewById(R.id.fc_profile);
            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            iv_img_favourites.setHierarchy(hierarchy);
            //  fc_p.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
            tv_rating_dest.setText(exploreDest.get(position).getDesRating());
            //  iv_img_favourites.setImageURI(Constants.TimbThumb_ImagePath+exploreDest.get(position).getDesImage());
            iv_img_favourites.setImageURI(Constants.TimbThumb_ImagePath + exploreDest.get(position).getDesImage() + "&width=" + (ExploreActivity.width));

           // tv_time_km_fav_list.setText((int) Math.ceil(exploreDest.get(position).getDistance()) + " kms [" + Constants.convert_minute_hrs(exploreDest.get(position).getDuration().doubleValue()) + "]");
            tv_time_km_fav_list.setText((int) Math.ceil(exploreDest.get(position).getDistance())+ " kms" );
            if (exploreDest.get(position).getFavId() == 0) {
                iv_like_icon.setImageResource(R.drawable.like_icon_unselected);
            } else {
                iv_like_icon.setImageResource(R.drawable.like_icon_selected);

            }

          /*  if(exploreDest.get(position).getImageList().size()>0)
            {
                for (int i=0;i<exploreDest.get(position).getImageList().size();i++)
                {
                    if(exploreDest.get(position).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner"))
                    {
                        iv_img_favourites.setImageURI(Constants.ImagePath+exploreDest.get(position).getImageList().get(i).getImgName());
                        break;
                    }
                }
            }*/

            Log.e("System out", "Image Path :::::::::" + Constants.TimbThumb_ImagePath + exploreDest.get(position).getDesImage());
            tv_img_name.setText(exploreDest.get(position).getDesName());
            if (exploreDest.get(position).getDestBestTimeFrom() != null && exploreDest.get(position).getDesBestTimeTo() != null) {
                tv_from_to_month.setText(Constants.getMonthName(Integer.parseInt(exploreDest.get(position).getDestBestTimeFrom())) + " to " + Constants.getMonthName(Integer.parseInt(exploreDest.get(position).getDesBestTimeTo())));

            } // ((ExploreActivity)getActivity()).edcityname.setText("Coorg");
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // ((ExploreActivity)getActivity()).clearALLFilter();

                    ListingDetailsFragment listingDetailsFragment = new ListingDetailsFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // ((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
                    changeTransaction.replace(R.id.frame, listingDetailsFragment);
                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("placeName", tv_img_name.getText().toString());
                    bundle.putString("from", "sublisting");
                   // Constants.selectedPosition = position + "";
                    bundle.putSerializable("Explorlist", exploreDest.get(position));
                    bundle.putString("position", position + "");
                    String json = gson.toJson(exploreDest);
                    editor.putString("DestList", json);
                    editor.commit();
                    ExploreActivity.destattractionimages.clear();
                    for (int i = 0; i < exploreDest.get(position).getImageList().size(); i++) {
                        if (exploreDest.get(position).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                            ExploreActivity.destattractionimages.add(exploreDest.get(position).getImageList().get(i).getImgName());
                        }
                    }
                    listingDetailsFragment.setArguments(bundle);
                    changeTransaction.commit();

                }
            });
            iv_like_icon.setId(position);
            iv_like_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                        // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";

                        if (sessionManager.isLoggedIn()) {

                            String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\", \"FavType\": \"Destination\",   \"FavRefId\": \"" + exploreDest.get(v.getId()).getDesID() + "\"}]";
                            Log.d("System out", "In add favourite  " + json);

                            AddFavouritesAPI(json, (ImageView) v, exploreDest, v.getId());
                        } else {
                            LoginModule loginModule = new LoginModule(getActivity(), "favourite Destination");
                            loginModule.signIn();
                        }


                    } else {
                        Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                    }


                }
            });


            return v;
        }

        private void AddFavouritesAPI(final String json, final ImageView imageView, final ArrayList<ExploreDestination> exploreDest, final int pos) {
            final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(false);
            pb_dialog.show();

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<ArrayList<AddFavourite>> call = apiService.addFavourites(json);
            call.enqueue(new Callback<ArrayList<AddFavourite>>() {
                @Override
                public void onResponse(Call<ArrayList<AddFavourite>> call, Response<ArrayList<AddFavourite>> response) {
                    if (response.body() != null) {

                        pb_dialog.dismiss();
                        addFavList = response.body();
                        if (addFavList.get(0).getResStatus() == true) {
                            //  Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                            if (addFavList.get(0).getResDescription().toLowerCase().contains("add")) {

                                imageView.setImageResource(R.drawable.like_icon_selected);

                                exploreDest.get(pos).setFavId(addFavList.get(0).getFavId());
                                final Dialog dialog1 = new Dialog(getActivity());
                                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog1.setContentView(R.layout.like_remove_layout);
                                final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                tv_text_like_remove.setText("Added to your favourites");
                                dialog1.setCanceledOnTouchOutside(true);
                                dialog1.show();
                                like = true;
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(1500);
                                            ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog1.dismiss();

                                                    //Code for the UiThread
                                                }
                                            });
                                           /* runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                    dialog1.dismiss();
                                                }
                                            });*/
                                        } catch (Exception e) {
                                            Log.w("Exception in splash", e);
                                        }

                                    }
                                }).start();


                            } else {
                                imageView.setImageResource(R.drawable.like_icon_unselected);

                                exploreDest.get(pos).setFavId(0);
                                final Dialog dialog1 = new Dialog(getActivity());
                                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                dialog1.setContentView(R.layout.like_remove_layout);
                                final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                tv_text_like_remove.setText("Removed from your favourites");
                                dialog1.setCanceledOnTouchOutside(true);
                                dialog1.show();
                                like = true;
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(1500);
                                            ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog1.dismiss();

                                                    //Code for the UiThread
                                                }
                                            });
                                            /*runOnUiThread(new Runnable() {

                                                @Override
                                                public void run() {
                                                    dialog1.dismiss();
                                                }
                                            });*/
                                        } catch (Exception e) {
                                            Log.w("Exception in splash", e);
                                        }

                                    }
                                }).start();


                            }
                        } else {
                            // Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                            //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                        }


                    }


                }

                @Override
                public void onFailure(Call<ArrayList<AddFavourite>> call, Throwable t) {
                    // Log error here since request failed
                    pb_dialog.dismiss();
                    t.printStackTrace();
                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                    }


                }
            });
        }

    }


    private class PrefferedAdapter extends BaseAdapter {
        private final ArrayList<String> arraylist;
        private final ArrayList<String> arraylistids;
        SubListingFragment context;

        public PrefferedAdapter(SubListingFragment subListingFragment, ArrayList<String> imgTypeArray, ArrayList<String> imgTypeArrayIDs) {
            this.context = subListingFragment;
            this.arraylist = imgTypeArray;
            this.arraylistids = imgTypeArrayIDs;
        }

        @Override
        public int getCount() {
            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.preffered_cell_layout, parent, false);
            final ImageView iv_selected_preffered = (ImageView) v.findViewById(R.id.iv_selected_preffered);
            final TextView tv_name_type = (TextView) v.findViewById(R.id.tv_name_type);

            String[] abc = ((ExploreActivity)getActivity()).selectedCat.split(",");

            for (int i = 0; i < abc.length; i++) {

                if (abc[i].equalsIgnoreCase(arraylist.get(position))) {
                    iv_selected_preffered.setImageResource(R.drawable.round_home_selected);
                    tv_name_type.setTextColor(getResources().getColor(R.color.textYellow));
                }/* else {
                    iv_selected_preffered.setImageResource(R.drawable.round_home_unselected);
                }*/
            }
            v.setId(position);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //    Name=Constants.SelectedCat;

                    String ReplaceNAme = "";


                    if (((ExploreActivity)getActivity()).selectedCat.contains(arraylist.get(v.getId()))) {
                        if (((ExploreActivity)getActivity()).selectedCat.split(",").length >= 2) {
                            ReplaceNAme = ((ExploreActivity)getActivity()).selectedCat.replace(arraylist.get(v.getId()) + ",", "");
                            ((ExploreActivity)getActivity()).selectedCat = ReplaceNAme;
                            ((ExploreActivity)getActivity()).selectedIDs = ((ExploreActivity)getActivity()).selectedIDs.replace(arraylistids.get(v.getId()) + ",", "");
                            iv_selected_preffered.setImageResource(R.drawable.round_home_unselected);
                            tv_name_type.setTextColor(getResources().getColor(R.color.colorWhite));
                        }

                    } else {
                        ((ExploreActivity)getActivity()).selectedCat += arraylist.get(v.getId()) + ",";
                        ((ExploreActivity)getActivity()).selectedIDs += arraylistids.get(v.getId()) + ",";

                        iv_selected_preffered.setImageResource(R.drawable.round_home_selected);
                        tv_name_type.setTextColor(getResources().getColor(R.color.textYellow));


                    }
                    if (((ExploreActivity)getActivity()).selectedCat.split(",").length >= 1) {

                        setheader();
                    } else {

                    }
                }
            });
            tv_name_type.setText(arraylist.get(position));
            return v;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("System out", "resume from sublisting");
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.VISIBLE);
        //((ExploreActivity) getActivity()).edcityname.setText("Ex-Bengaluru");
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
        setheader();
        if (progress.equalsIgnoreCase("999999")) {
            if (from.equalsIgnoreCase("nearbyplaces")) {
                Log.d("System out", "if______" + from);
                ll_main_sb_nearby_attractions.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).edcityname.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).edcityname1.setVisibility(View.GONE);
            } else {
                Log.d("System out", "else______" + from);
                ll_main_sb_nearby_attractions.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).edcityname.setVisibility(View.GONE);
                ((ExploreActivity) getActivity()).edcityname1.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).ll_top_header.setPadding(0, (int) getResources().getDimension(R.dimen._8sdp), 0, 0);
                ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.VISIBLE);
            }
            setheader();


        } else {
            Log.d("System out", "progress_______" + progress);
            Log.d("System out", "else______");
            Constants.fromSublistingforSb = true;
            sb_nearby_attraction.setProgress(Integer.parseInt(progress));



            ll_main_sb_nearby_attractions.setVisibility(View.VISIBLE);
            ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
            ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        }


    }

    public void setheader() {

        if (!TextUtils.isEmpty(((ExploreActivity)getActivity()).selectedCat) && ((ExploreActivity)getActivity()).selectedCat.contains(",")) {
            sub = ((ExploreActivity)getActivity()).selectedCat.split(",");
            Log.d("System out", "Split____" + sub.toString());
            if (sub.length == imgTypeArray.size()) {
                ((ExploreActivity) getActivity()).tv_img_type.setText("All");
            } else {
                if (sub.length == 1) {
                    ((ExploreActivity) getActivity()).tv_img_type.setText(sub[0]);
                } else {
                    Log.d("System out", "Name___OTHER IF_______" + ((ExploreActivity)getActivity()).selectedCat);

                    ((ExploreActivity) getActivity()).tv_img_type.setText(sub[0] + " +" + (sub.length - 1));
                }
            }
        } else {
            if (((ExploreActivity)getActivity()).selectedCat.length() == 0) {
                ((ExploreActivity) getActivity()).tv_img_type.setText("");

            } else {
                ((ExploreActivity) getActivity()).tv_img_type.setText(((ExploreActivity)getActivity()).selectedCat.replace(",", ""));
            }
        }
    }

    public void get_bitmap_from_freco_download(final String url, final String name, final String besttime) {
        Log.d("System out", "sharing image path " + url);


//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    dataSource.close();
                    DownloadCode code = new DownloadCode(getActivity(), Constants.TimbThumb_ImagePath, name, bitmap, besttime);
                    code.share_image_text_GPLUS();


                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }




    //set text to seekbar


    private void setSeekBar(Integer progress1) {
        prgrs= String.valueOf(progress1);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),

                R.drawable.radius_icon_home_slider3);


        Bitmap bmp = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas c = new Canvas(bmp);



        //   String text = Integer.toString(seekBar.getProgress());

        Paint p = new Paint();

        p.setTypeface(Typeface.DEFAULT);

        p.setTextSize(getResources().getDimension(R.dimen._6sdp));

        p.setColor(getResources().getColor(android.R.color.white));
        p.setTextAlign(Paint.Align.LEFT);
        tv_max_progress.setVisibility(View.INVISIBLE);
        tv_min_progress.setVisibility(View.INVISIBLE);

        sb_nearby_attraction.setThumb(new BitmapDrawable(getResources(), bmp));

        int width = (int) p.measureText(" "+progress1+" kms  ");
        int yPos = (int) ((c.getHeight()));
        c.drawText(" "+String.valueOf(progress1)+" kms  ", (bmp.getWidth() - width) / 2, yPos, p);
/*
        if(progress1<40)
        {
            tv_max_progress.setVisibility(View.VISIBLE);
            tv_min_progress.setVisibility(View.INVISIBLE);
            int width = (int) p.measureText(progress1+" kms ");
            int yPos = (int) ((c.getHeight())-5);
            c.drawText(String.valueOf(progress1)+" kms ", (bmp.getWidth() - width) / 2, yPos, p);
        }
        else if(progress1>320)
        {
            tv_max_progress.setVisibility(View.INVISIBLE);
            tv_min_progress.setVisibility(View.VISIBLE);
            int width = (int) p.measureText(progress1+" kms ");
            int yPos = (int) ((c.getHeight()-5));
            c.drawText(String.valueOf(progress1)+" kms ", (bmp.getWidth() - width) / 2, yPos, p);
        }
        else {
            *//*tv_progress.setVisibility(View.VISIBLE);
            float val = (progress1 * (sb_nearby_attraction.getWidth() - 2 + px * sb_nearby_attraction.getThumbOffset())) / sb_nearby_attraction.getMax();
            tv_progress.setX(val);
            Log.d("System out","setx__"+val);
            tv_progress.setText(progress);*//*
            tv_max_progress.setVisibility(View.INVISIBLE);
            tv_min_progress.setVisibility(View.INVISIBLE);
            int width = (int) p.measureText(progress1+" kms ");
            int yPos = (int) ((c.getHeight()-5));
            c.drawText(String.valueOf(progress1)+" kms ", (bmp.getWidth() - width) / 2, yPos, p);

        }*/

    }

}
