package com.bpcl.happyroads.Database;

/**
 * Created by ADMIN on 7/28/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Poi_Night;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.PushNotification;

import java.util.ArrayList;
import java.util.List;


public class DBHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "PTO";

    // table name
    private static final String TABLE_TRIP = "Trip";
    private static final String TABLE_POI = "Poi";
    private static final String TABLE_POI_Night = "PoiNight";
    private static final String TABLE_Push_Notification = "PushNotification";
    private static final String TABLE_DESTINATION = "Destination"; //destination planning


    // TRIP Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TRIP_ID = "tripid";
    private static final String KEY_SOURCE = "source";
    private static final String KEY_DESTINATION = "destination";
    private static final String KEY_START_DATE = "startdate";
    private static final String KEY_START_TIME = "starttime";
    private static final String KEY_END_DATE = "enddate";
    private static final String KEY_END_TIME = "endtime";
    private static final String KEY_TRIP_NAME = "tripname";
    private static final String KEY_SOURCE_LATITUDE = "sourcelatitude";
    private static final String KEY_SOURCE_LONGITUDE = "sourcelongitude";
    private static final String KEY_DESTINATION_LATITUDE = "deslatitude";
    private static final String KEY_DESTINATION_LONGITUDE = "deslongitude";
    private static final String KEY_SOURCE_IMAGE = "sourceimage";
    private static final String KEY_DESTINATION_IMAGE = "destinationimage";
    private static final String KEY_TRIP_TIME = "tripTime";
    private static final String KEY_TRIP_KM = "tripskm";
    private static final String KEY_TRIP_STATUS = "tripstatus";
    private static final String KEY_TRIP_RATING = "triprating";
    private static final String KEY_TRIP_SOURCE_ID = "tripsourceid";
    private static final String KEY_TRIP_DESTINATION_ID = "tripdestinationid";
    private static final String KEY_TRIP_Des_Address = "desaddress";
    private static final String KEY_TRIP_Return_status = "returnstatus";
    private static final String KEY_Return_END_DATE = "returnenddate";
    private static final String KEY_Return_END_TIME = "returnendtime";
    private static final String KEY_Return_START_DATE = "returnstartdate";
    private static final String KEY_Return_START_TIME = "returnstarttime";
    private static final String KEY_TripIsOneSideCompleted="tripIsOneSideCompleted";
    private static final String KEY_TRIP_ETA_DIFF="tripetadiff";// trip eta reach time diff in seconds
    private static final String KEY_TRIP_Return_ETA_DIFF="tripetareturndiff";
    private static final String KEY_TRIP_ReturnKM="returnKm";
    private static  final String KEY_TRIP_ReturnTime="returnTime";
    private static final String KEY_DES_NO_OF_NEARBYATTRACTION = "DesNoOfNearByAttraction";
    private static final String KEY_TripGooglePlaceLat = "TripGooglePlaceLat";
    private static final String KEY_TripGooglePlaceLong= "TripGooglePlaceLong";
    private static final String KEY_TripFinalStatus= "TripFinalStatus"; //0 for not completed 1 completed





    // POI Table Columns names
    private static final String KEY_POI_ID = "poiid";
    private static final String KEY_POI_TRIP_ID = "tripid";
    private static final String KEY_POI_NAME = "name";
    private static final String KEY_STAY_TIME_HRS = "staytime";
    private static final String KEY_POI_LATITUDE = "latitude";
    private static final String KEY_POI_LONGITUDE = "longitude";
    private static final String KEY_POI_IMAGE = "image";
    private static final String KEY_POI_STAY = "stay";
    private static final String KEY_POI_RESUME_DATE = "resumedate";
    private static final String KEY_POI_RESUME_TIME = "resumetime";
    private static final String KEY_POI_TIME = "poiTime";
    private static final String KEY_POI_KM = "poiKM";
    private static final String KEY_POI_DATE = "poidate";
    private static final String KEY_POI_ARRIVAL_TIME = "arrivaltime";
    private static final String KEY_POI_DepartureTime = "departuretime";
    private static final String KEY_POI_WayPointId = "wayPointId";
    private static final String KEY_POI_Type = "poitype";
    private static final String KEY_POI_ID_S = "poidesids";//server poi id
    private static final String KEY_POI_DesID = "poidesid";
    private static final String KEY_POI_Return_status = "returnstatus";
    private static final String KEY_POI_ImageName = "imagename";
    private static final String KEY_POI_ETA_DIFF="poietadiff";// trip eta reach time diff in seconds
    private static final String KEY_POI_Actual_Km = "actualkm"; //Actual KM form google
    private static final String KEY_POI_Actual_Time = "actualtime"; //Actual Time form google
    private static final String KEY_POI_Leg_no = "legno"; //google
    private static final String KEY_POI_Step_no = "stepno"; //google
    private static final String KEY_POI_Track_flag = "poitrackflag"; //tracking
    private static final String KEY_POI_Side = "poiside"; // poi road side




    // POI Night Table Columns names
    private static final String KEY_POI_ID_N = "poiid";
    private static final String KEY_POI_TRIP_ID_N = "tripid";
    private static final String KEY_POI_NAME_N = "name";
    private static final String KEY_STAY_TIME_HRS_N = "staytime";
    private static final String KEY_POI_LATITUDE_N = "latitude";
    private static final String KEY_POI_LONGITUDE_N = "longitude";
    private static final String KEY_POI_IMAGE_N = "image";
    private static final String KEY_POI_STAY_N = "stay";
    private static final String KEY_POI_TIME_N = "poiTime";
    private static final String KEY_POI_KM_N = "poiKM";
    private static final String KEY_POI_DATE_N = "poidate";
    private static final String KEY_POI_ImageName_N = "imagename";
    private static final String KEY_POI_WayPointId_N = "wayPointId";
    private static final String KEY_POI_Type_N = "poitype";
    private static final String KEY_POI_ID_S_N = "poidesids";//server poi id
    private static final String KEY_POI_DesID_N = "poidesid";
    private static final String KEY_POI_Des_AttractionCount = "attractionCount";
    private static final String KEY_POI_Return_status_N = "returnstatus";
    private static final String KEY_POI_Description = "poidescription";

    // Destination Plan Table Columns names
    private static final String KEY_ID_DES = "id";
    private static final String KEY_DES_TRIP_ID = "tripid";
    private static final String KEY_DES_NAME = "name";
    private static final String KEY_DES_STAY_TIME_HRS = "staytime";
    private static final String KEY_DES_LATITUDE = "latitude";
    private static final String KEY_DES_LONGITUDE = "longitude";
    // private static final String KEY_DES_IMAGE = "image";
    private static final String KEY_DES_TIME = "desTime";
    private static final String KEY_DES_KM = "poiKM";
    private static final String KEY_DES_DATE = "desdate";
    private static final String KEY_DES_ImageName = "imagename";
    private static final String KEY_DES_WayPointId = "wayPointId";
    private static final String KEY_DES_Type = "poitype";
    private static final String KEY_DES_ID_S = "desids";//server des id

    private static final String KEY_DES_START_TIME = "starttime";
    private static final String KEY_DES_END_TIME = "endtime";
    private static final String KEY_DES_Description = "desdescription";
    private static final String KEY_DES_DAY = "desday";
    private static final String KEY_DES_DAY_START_TIME = "desdaystarttime";
















    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_TRIP_TABLE = "CREATE TABLE " + TABLE_TRIP + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TRIP_ID + " INTEGER ," + KEY_SOURCE + " TEXT , " + KEY_DESTINATION + " TEXT ," + KEY_START_DATE + " TEXT ," + KEY_START_TIME + " TEXT ," + KEY_END_DATE + " TEXT ," + KEY_END_TIME + " TEXT ," + KEY_TRIP_NAME + " TEXT ," + KEY_SOURCE_LATITUDE + " DOUBLE ," + KEY_SOURCE_LONGITUDE + " DOUBLE ," + KEY_DESTINATION_LATITUDE + " DOUBLE ," + KEY_DESTINATION_LONGITUDE + " DOUBLE ," + KEY_SOURCE_IMAGE + " BLOB ," + KEY_DESTINATION_IMAGE + " BLOB ," + KEY_TRIP_KM + " DOUBLE ," + KEY_TRIP_TIME + " DOUBLE ," + KEY_TRIP_STATUS + " TEXT ," + KEY_TRIP_RATING + " TEXT ," + KEY_TRIP_SOURCE_ID + " INTEGER ," + KEY_TRIP_DESTINATION_ID + " INTEGER ,"+ KEY_TRIP_Des_Address + " TEXT ,"+ KEY_TRIP_Return_status + " BOOLEAN ,"+ KEY_Return_START_DATE + " TEXT ," + KEY_Return_START_TIME + " TEXT ," + KEY_Return_END_DATE + " TEXT ," + KEY_Return_END_TIME + " TEXT ," +KEY_TripIsOneSideCompleted +" BOOLEAN ,"+KEY_TRIP_ETA_DIFF+" DOUBLE ,"+KEY_TRIP_Return_ETA_DIFF+" DOUBLE ," +KEY_TRIP_ReturnKM+" DOUBLE ," +KEY_TRIP_ReturnTime+" DOUBLE ," +KEY_DES_NO_OF_NEARBYATTRACTION+" INTEGER ,"+KEY_TripGooglePlaceLat+" DOUBLE ,"+KEY_TripGooglePlaceLong+" DOUBLE ," +KEY_TripFinalStatus+" INTEGER"+ ")";

        String CREATE_POI_TABLE = "CREATE TABLE " + TABLE_POI + "("
                + KEY_POI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_POI_TRIP_ID + " INTEGER , " + KEY_POI_NAME + " TEXT ," + KEY_STAY_TIME_HRS + " DOUBLE ," + KEY_POI_LATITUDE + " DOUBLE ," + KEY_POI_LONGITUDE + " DOUBLE ," + KEY_POI_IMAGE + " BLOB ," + KEY_POI_STAY + " BOOLEAN ," + KEY_POI_RESUME_DATE + " TEXT ," + KEY_POI_RESUME_TIME + " TEXT ," + KEY_POI_KM + " DOUBLE ," + KEY_POI_TIME + " DOUBLE ," + KEY_POI_DATE + " TEXT ," +KEY_POI_ARRIVAL_TIME+" DOUBLE ," +KEY_POI_DepartureTime+" DOUBLE ,"+KEY_POI_Type+" TEXT ,"+KEY_POI_WayPointId+" INTEGER ,"+KEY_POI_DesID+" INTEGER ,"+KEY_POI_ID_S+" INTEGER ,"+KEY_POI_Des_AttractionCount+" INTEGER ,"+ KEY_POI_Return_status + " BOOLEAN ,"+KEY_POI_Description + " TEXT ,"+KEY_POI_ImageName + " TEXT ," +KEY_POI_ETA_DIFF+" DOUBLE ," +KEY_POI_Actual_Km+" DOUBLE ,"+KEY_POI_Actual_Time+" DOUBLE ,"+KEY_POI_Leg_no+" INTEGER ,"+KEY_POI_Step_no+" INTEGER ,"+KEY_POI_Track_flag + " BOOLEAN ,"+KEY_POI_Side+" TEXT"  +")";


        String CREATE_POI_Night_TABLE = "CREATE TABLE " + TABLE_POI_Night + "("
                + KEY_POI_ID_N + " INTEGER ," + KEY_POI_TRIP_ID_N + " INTEGER , " + KEY_POI_NAME_N + " TEXT ," + KEY_STAY_TIME_HRS_N + " DOUBLE ," + KEY_POI_LATITUDE_N + " DOUBLE ," + KEY_POI_LONGITUDE_N + " DOUBLE ," + KEY_POI_IMAGE_N + " BLOB ," + KEY_POI_STAY_N + " BOOLEAN ,"  + KEY_POI_KM_N + " DOUBLE ," + KEY_POI_TIME_N + " DOUBLE ," + KEY_POI_DATE_N + " TEXT ,"+KEY_POI_Type_N+" TEXT ,"+KEY_POI_WayPointId_N+" INTEGER ,"+KEY_POI_DesID_N+" INTEGER ,"+KEY_POI_ID_S_N+" INTEGER ,"+ KEY_POI_Return_status_N + " BOOLEAN ," +KEY_POI_Description + " TEXT ," +KEY_POI_ImageName_N + " TEXT " +")";


        String CREATE_Push_Notification_TABLE = "CREATE TABLE " + TABLE_Push_Notification + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_POI_NAME + " TEXT ," +  KEY_POI_LATITUDE + " DOUBLE ," + KEY_POI_LONGITUDE + " DOUBLE ," +KEY_POI_Type+" TEXT ,"+KEY_POI_STAY + " BOOLEAN "+")";


        String CREATE_DESTINATION = "CREATE TABLE " + TABLE_DESTINATION + "("
                + KEY_ID_DES + " INTEGER PRIMARY KEY AUTOINCREMENT ," + KEY_DES_TRIP_ID + " INTEGER , " + KEY_DES_NAME + " TEXT ," + KEY_DES_STAY_TIME_HRS + " DOUBLE ," + KEY_DES_LATITUDE + " DOUBLE ," + KEY_DES_LONGITUDE + " DOUBLE ," +   KEY_DES_KM + " DOUBLE ," + KEY_DES_TIME + " DOUBLE ," + KEY_DES_DATE + " TEXT ,"+KEY_DES_Type+" TEXT ,"+KEY_DES_WayPointId+" INTEGER ,"+KEY_DES_ID_S+" INTEGER ,"+ KEY_DES_Description + " TEXT ," +KEY_DES_ImageName + " TEXT ,"+KEY_DES_START_TIME+ " DOUBLE ,"+KEY_DES_END_TIME+ " DOUBLE ,"+KEY_DES_DAY+" INTEGER , "+KEY_DES_DAY_START_TIME+" DOUBLE ,"+KEY_POI_ID_S+" INTEGER " +")";


        db.execSQL(CREATE_TRIP_TABLE);

        db.execSQL(CREATE_POI_TABLE);

        db.execSQL(CREATE_POI_Night_TABLE);

        db.execSQL(CREATE_Push_Notification_TABLE);

        db.execSQL(CREATE_DESTINATION);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRIP);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POI);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POI_Night);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Push_Notification);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DESTINATION);
        // Creating tables again
        onCreate(db);
    }


    // Adding new TRIP
    public void add_Trip_into_Table(Create_Trip item) {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TRIP_ID, item.getTripId());
        values.put(KEY_SOURCE, item.getSource());
        values.put(KEY_DESTINATION, item.getDestination());
        values.put(KEY_START_DATE, item.getStartdate());
        values.put(KEY_START_TIME, item.getStarttime());
        values.put(KEY_END_DATE, item.getEnddate());
        values.put(KEY_END_TIME, item.getEndtime());
        values.put(KEY_TRIP_NAME, item.getTripname());
        values.put(KEY_SOURCE_LATITUDE, item.getSource_latitude());
        values.put(KEY_SOURCE_LONGITUDE, item.getSource_longitude());
        values.put(KEY_DESTINATION_LATITUDE, item.getDestination_latitude());
        values.put(KEY_DESTINATION_LONGITUDE, item.getDestination_longitude());
        values.put(KEY_SOURCE_IMAGE, item.getSource_image());
        values.put(KEY_DESTINATION_IMAGE, item.getDestination_image());

        values.put(KEY_TRIP_KM, item.getKM());
        values.put(KEY_TRIP_TIME, item.getTime());
        values.put(KEY_TRIP_STATUS, item.getTripstatus());
        values.put(KEY_TRIP_RATING, item.getTriprating());
        values.put(KEY_TRIP_SOURCE_ID, item.getSourceId());
        values.put(KEY_TRIP_DESTINATION_ID, item.getDestinationId());
        if(item.getReturn_start_date().length()>1)
        {
            values.put(KEY_TRIP_Return_status,true);
        }
        else
            values.put(KEY_TRIP_Return_status,false);

        values.put(KEY_TRIP_Des_Address,"");
        values.put(KEY_Return_START_DATE,item.getReturn_start_date());
        values.put(KEY_Return_START_TIME,item.getReturn_start_time());
        values.put(KEY_Return_END_DATE,item.getReturn_end_date());
        values.put(KEY_Return_END_TIME,item.getReturn_end_time());
        values.put(KEY_TripIsOneSideCompleted,item.getTripIsOneSideCompleted());
        values.put(KEY_TRIP_ETA_DIFF,0);
        values.put(KEY_TRIP_ReturnKM, item.getReturnKM());
        values.put(KEY_TRIP_ReturnTime, item.getReturnTime());
        values.put(KEY_DES_NO_OF_NEARBYATTRACTION,item.getDesNoOfNearByAttraction());
        values.put(KEY_TripFinalStatus,0);


        // Inserting Row
        db.insert(TABLE_TRIP, null, values);
        db.close(); // Closing database connection

    }


    // Adding new POI
    public void add_POI_into_Table(Create_Poi item)
    {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_POI_TRIP_ID, item.getTripId());
        values.put(KEY_POI_NAME, item.getPoiName());
        values.put(KEY_STAY_TIME_HRS, item.getStayTime());
        values.put(KEY_POI_LATITUDE, item.getPoiLatitude());
        values.put(KEY_POI_LONGITUDE, item.getPoiLongitude());
        values.put(KEY_POI_IMAGE, item.getPoiImage());
        values.put(KEY_POI_STAY, item.getPoiStay());
        values.put(KEY_POI_RESUME_DATE, item.getResumeDate());
        values.put(KEY_POI_RESUME_TIME, item.getResumeTime());
        values.put(KEY_POI_KM, item.getKM());
        values.put(KEY_POI_TIME, item.getTime());
        values.put(KEY_POI_DATE, item.getPoiDate());
        values.put(KEY_POI_ARRIVAL_TIME,0.0);
        values.put(KEY_POI_DepartureTime,0.0);

        values.put(KEY_POI_Type,item.getPoitype());
        values.put(KEY_POI_WayPointId,item.getWayPointId());
        values.put(KEY_POI_ID_S,item.getPoiServerId());
        values.put(KEY_POI_DesID,item.getPoiDesID());
        values.put(KEY_POI_Des_AttractionCount,item.getAttractionCount());
        values.put(KEY_POI_Return_status,item.getReturnstatus());
        values.put(KEY_POI_Description,item.getPoidescription());
        Log.d("poiimagename",item.getImagename());
        values.put(KEY_POI_ImageName,item.getImagename());
        values.put(KEY_POI_ETA_DIFF,0);
        values.put(KEY_POI_Actual_Km, item.getKM());
        values.put(KEY_POI_Actual_Time, item.getTime());
        values.put(KEY_POI_Leg_no,0);
        values.put(KEY_POI_Step_no,0);
        values.put(KEY_POI_Track_flag,false);
        values.put(KEY_POI_Side,item.getPoiside());




        // Inserting Row
        db.insert(TABLE_POI, null, values);
        db.close(); // Closing database connection

    }


    // Adding new Night Stay POI
    public void add_Night_POI_into_Table(Create_Poi_Night item)
    {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        Log.d("selectpoi",item.getStayTime()+"tripid:"+item.getTripId());


        values.put(KEY_POI_ID_N, item.getPoiId());
        values.put(KEY_POI_TRIP_ID_N, item.getTripId());
        values.put(KEY_POI_NAME_N, item.getPoiName());
        values.put(KEY_STAY_TIME_HRS_N, item.getStayTime());
        values.put(KEY_POI_LATITUDE_N, item.getPoiLatitude());
        values.put(KEY_POI_LONGITUDE_N, item.getPoiLongitude());
        values.put(KEY_POI_IMAGE_N, item.getPoiImage());
        values.put(KEY_POI_STAY_N, item.getPoiStay());
        values.put(KEY_POI_KM_N, item.getKM());
        values.put(KEY_POI_TIME_N, item.getTime());
        values.put(KEY_POI_DATE_N, item.getPoiDate());
        values.put(KEY_POI_Type_N,item.getPoitype());
        values.put(KEY_POI_WayPointId_N,item.getWayPointId());
        values.put(KEY_POI_ID_S_N,item.getPoiServerId());
        values.put(KEY_POI_DesID_N,item.getPoiDesID());
        values.put(KEY_POI_Return_status_N,item.getReturnstatus());
        values.put(KEY_POI_Description,item.getPoidescription());
        values.put(KEY_POI_ImageName_N,item.getImagename());

        // Inserting Row
        db.insert(TABLE_POI_Night, null, values);
        db.close(); // Closing database connection

    }



    // Adding new Night Stay POI
    public void add_Destination_into_Table(DestinationPlan item)
    {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_DES_TRIP_ID, item.getTripId());
        values.put(KEY_DES_NAME, item.getPoiName());
        values.put(KEY_DES_STAY_TIME_HRS, item.getStayTime());
        values.put(KEY_DES_LATITUDE, item.getPoiLatitude());
        values.put(KEY_DES_LONGITUDE, item.getPoiLongitude());


        values.put(KEY_DES_KM, item.getKM());
        values.put(KEY_DES_TIME, item.getTime());
        values.put(KEY_DES_DATE, item.getPoiDate());
        values.put(KEY_DES_Type,item.getPoitype());
        values.put(KEY_DES_WayPointId,item.getWayPointId());
        values.put(KEY_DES_ID_S,item.getPoiDesID());

        values.put(KEY_DES_Description,item.getPoidescription());
        values.put(KEY_DES_ImageName,item.getImagename());

        values.put(KEY_DES_START_TIME,item.getStarttime());
        values.put(KEY_DES_END_TIME,item.getEndtime());
        values.put(KEY_POI_ID_S,item.getPoiServerId());

        Log.d("poiserverid","serverid:"+item.getPoiServerId()+"desid"+item.getPoiDesID());
        // Inserting Row
        db.insert(TABLE_DESTINATION, null, values);
        db.close(); // Closing database connection

    }

    // add notification
    // Adding new Night Stay POI
    public void add_Notification_into_Table(PushNotification item)
    {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_POI_NAME, item.getName());
        values.put(KEY_POI_LATITUDE, item.getLatitude());
        values.put(KEY_POI_LONGITUDE, item.getLongitude());
        values.put(KEY_POI_Type, item.getType());
        values.put(KEY_POI_STAY, item.getFlag());



        // Inserting Row
        db.insert(TABLE_Push_Notification, null, values);
        db.close(); // Closing database connection

    }



    //get all trip in list

    public List<Create_Trip> get_All_TRIP(String status)
    {
        List<Create_Trip> TripList = new ArrayList<Create_Trip>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TRIP+" where "+KEY_TRIP_STATUS+"='"+status+"'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Create_Trip item = new Create_Trip();
                item.setId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setSource(cursor.getString(2));
                item.setDestination(cursor.getString(3));
                item.setStartdate(cursor.getString(4));
                item.setStarttime(cursor.getString(5));
                item.setEnddate(cursor.getString(6));
                item.setEndtime(cursor.getString(7));
                item.setTripname(cursor.getString(8));
                item.setSource_latitude(cursor.getDouble(9));
                item.setSource_longitude(cursor.getDouble(10));
                item.setDestination_latitude(cursor.getDouble(11));
                item.setDestination_longitude(cursor.getDouble(12));
                item.setSource_image(cursor.getBlob(13));
                item.setDestination_image(cursor.getBlob(14));
                item.setKM(cursor.getDouble(15));
                item.setTime(cursor.getDouble(16));
                item.setTripstatus(cursor.getString(17));
                item.setTriprating(cursor.getString(18));
                item.setSourceId(cursor.getInt(19));
                item.setDestinationId(cursor.getInt(20));
                item.setDes_address(cursor.getString(21));
                if (cursor.getString(22).contains("1"))
                    item.setReturnstatus(true);
                else
                    item.setReturnstatus(false);

                item.setReturn_start_date(cursor.getString(23));
                item.setReturn_start_time(cursor.getString(24));
                item.setReturn_end_date(cursor.getString(25));
                item.setReturn_end_time(cursor.getString(26));
                if(cursor.getString(27).contains("1"))
                    item.setTripIsOneSideCompleted(true);
                else
                    item.setTripIsOneSideCompleted(false);
                // Adding Trip to list
                TripList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return TripList;
    }


    public List<Create_Trip> get_TRIP_By_TripId(Integer tripId) {
        List<Create_Trip> TripList = new ArrayList<Create_Trip>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TRIP+" Where "+KEY_TRIP_ID+" ="+tripId ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Create_Trip item = new Create_Trip();
                item.setId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setSource(cursor.getString(2));
                item.setDestination(cursor.getString(3));
                item.setStartdate(cursor.getString(4));
                item.setStarttime(cursor.getString(5));
                item.setEnddate(cursor.getString(6));
                item.setEndtime(cursor.getString(7));
                item.setTripname(cursor.getString(8));
                item.setSource_latitude(cursor.getDouble(9));
                item.setSource_longitude(cursor.getDouble(10));
                item.setDestination_latitude(cursor.getDouble(11));
                item.setDestination_longitude(cursor.getDouble(12));
                item.setSource_image(cursor.getBlob(13));
                item.setDestination_image(cursor.getBlob(14));
                item.setKM(cursor.getDouble(15));
                item.setTime(cursor.getDouble(16));
                item.setTripstatus(cursor.getString(17));
                item.setTriprating(cursor.getString(18));
                item.setSourceId(cursor.getInt(19));
                item.setDestinationId(cursor.getInt(20));
                item.setDes_address(cursor.getString(21));
                if (cursor.getString(22).contains("1"))
                    item.setReturnstatus(true);
                else
                    item.setReturnstatus(false);

                item.setReturn_start_date(cursor.getString(23));
                item.setReturn_start_time(cursor.getString(24));
                item.setReturn_end_date(cursor.getString(25));
                item.setReturn_end_time(cursor.getString(26));
                if(cursor.getString(27).contains("1"))
                    item.setTripIsOneSideCompleted(true);
                else
                    item.setTripIsOneSideCompleted(false);

                item.setEta_diff_second(cursor.getDouble(28));
                item.setEta_return_diff_second(cursor.getDouble(29));
                item.setReturnKM(cursor.getDouble(30));
                item.setReturnTime(cursor.getDouble(31));
                item.setDesNoOfNearByAttraction(cursor.getInt(cursor.getColumnIndex(KEY_DES_NO_OF_NEARBYATTRACTION)));
                item.setTripGooglePlaceLat(cursor.getDouble(cursor.getColumnIndex(KEY_TripGooglePlaceLat)));
                item.setTripGooglePlaceLong(cursor.getDouble(cursor.getColumnIndex(KEY_TripGooglePlaceLong)));

                // Adding Trip to list
                TripList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return TripList;
    }

    //get POI  in list by TripId

    public List<Create_Poi> get_POI_Into_Cart_By_TripId(Integer tripId,Integer returnstatus) {



        List<Create_Poi> PoiList = new ArrayList<Create_Poi>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_POI + " where " + KEY_POI_TRIP_ID + '=' + tripId +" AND "+KEY_POI_Return_status+ '='+returnstatus+ " ORDER BY " + KEY_POI_KM;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Create_Poi item = new Create_Poi();
                item.setPoiId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setPoiName(cursor.getString(2));
                item.setStayTime(cursor.getDouble(3));
                item.setPoiLatitude(cursor.getDouble(4));
                item.setPoiLongitude(cursor.getDouble(5));
                item.setPoiImage(cursor.getBlob(6));
                if (cursor.getString(7).contains("1"))
                    item.setPoiStay(true);
                else
                    item.setPoiStay(false);

                item.setResumeDate(cursor.getString(8));
                item.setResumeTime(cursor.getString(9));
                item.setKM(cursor.getDouble(10));
                item.setTime(cursor.getDouble(11));
                item.setPoiDate(cursor.getString(12));
                item.setArrivalTime(cursor.getDouble(13));
                item.setDepartureTime(cursor.getDouble(14));
                item.setPoitype(cursor.getString(15));
                item.setWayPointId(cursor.getInt(16));
                item.setPoiDesID(cursor.getInt(17));
                item.setPoiServerId(cursor.getInt(18));
                item.setAttractionCount(cursor.getInt(19));

                if (cursor.getString(20).contains("1"))
                    item.setReturnstatus(true);
                else
                    item.setReturnstatus(false);

                item.setPoidescription(cursor.getString(21));

                // Log.d("poiimagename",cursor.getString(22).toString());
                //   item.setImagename("20161006_06303.png");

                item.setImagename(cursor.getString(22));
                item.setPoi_diff_seconds(cursor.getDouble(23));
                item.setActual_Km(cursor.getDouble(24));
                item.setActual_Time(cursor.getDouble(25));
                item.setFlag(false);

                // Adding poi to list
                PoiList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return PoiList;
    }



    //get Night POI  in list by TripId & POIId

    public List<Create_Poi_Night> get_Night_POI_Into_Cart_By_TripId(Integer tripId,Integer poiId,Integer returnstatus) {

        List<Create_Poi_Night> PoiList = new ArrayList<Create_Poi_Night>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_POI_Night + " where " + KEY_POI_TRIP_ID_N + '=' + tripId + " And "+KEY_POI_ID_N + '=' + poiId +" AND "+KEY_POI_Return_status+ '='+returnstatus+" ORDER BY " + KEY_POI_KM_N;

        Log.d("selectquery",selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Create_Poi_Night item = new Create_Poi_Night();
                item.setPoiId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setPoiName(cursor.getString(2));
                item.setStayTime(cursor.getDouble(3));
                item.setPoiLatitude(cursor.getDouble(4));
                item.setPoiLongitude(cursor.getDouble(5));
                item.setPoiImage(cursor.getBlob(6));
                if (cursor.getString(7).contains("1"))
                    item.setPoiStay(true);
                else
                    item.setPoiStay(false);


                item.setKM(cursor.getDouble(8));
                item.setTime(cursor.getDouble(9));
                item.setPoiDate(cursor.getString(10));

                item.setPoitype(cursor.getString(11));
                item.setWayPointId(cursor.getInt(12));
                item.setPoiDesID(cursor.getInt(13));
                item.setPoiServerId(cursor.getInt(14));

                if (cursor.getString(15).contains("1"))
                    item.setReturnstatus(true);
                else
                    item.setReturnstatus(false);

                item.setPoidescription(cursor.getString(16));

                item.setImagename(cursor.getString(17));


                // Adding poi to list
                PoiList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return PoiList;
    }




    //get Night POI  in list by TripId & POIId

    public List<DestinationPlan> get_DestinationList(Integer tripId)
    {

        List<DestinationPlan> PoiList = new ArrayList<DestinationPlan>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_DESTINATION + " where " + KEY_DES_TRIP_ID + '=' + tripId +" ORDER BY " + KEY_DES_START_TIME+" ,"+KEY_DES_STAY_TIME_HRS;

        Log.d("selectquerydes",selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst())
        {
            do {
                DestinationPlan item = new DestinationPlan();
                item.setId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setPoiName(cursor.getString(2));
                item.setStayTime(cursor.getDouble(3));
                item.setPoiLatitude(cursor.getDouble(4));
                item.setPoiLongitude(cursor.getDouble(5));


                item.setKM(cursor.getDouble(6));
                item.setTime(cursor.getDouble(7));
                item.setPoiDate(cursor.getString(8));

                item.setPoitype(cursor.getString(9));
                item.setWayPointId(cursor.getInt(10));

                item.setPoiDesID(cursor.getInt(11));


                item.setPoidescription(cursor.getString(12));

                item.setImagename(cursor.getString(13));
                item.setStarttime(cursor.getDouble(14));
                item.setEndtime(cursor.getDouble(15));

                item.setPoiServerId(cursor.getInt(cursor.getColumnIndex(KEY_POI_ID_S)));
                Log.d("poiserverid1","serverid:"+item.getPoiServerId()+"desid"+item.getPoiDesID());

                // Adding poi to list
                PoiList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return PoiList;
    }


    //get All push

    public List<PushNotification> get_push_notification_list() {

        List<PushNotification> NotificationList = new ArrayList<PushNotification>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_Push_Notification +" where "+KEY_POI_STAY+"=0";

        Log.d("selectquery",selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PushNotification item = new PushNotification();
                item.setId(cursor.getInt(0));
                item.setName(cursor.getString(1));
                item.setLatitude(cursor.getDouble(2));
                item.setLongitude(cursor.getDouble(3));
                item.setType(cursor.getString(4));


                if (cursor.getString(5).contains("1"))
                    item.setFlag(true);
                else
                    item.setFlag(false);






                // Adding poi to list
                NotificationList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return NotificationList;
    }




//Delete trip by id

    public int delete_Trip_ById(Integer rowId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_TRIP_ID + "='" + rowId + "'";

        return db.delete(TABLE_TRIP, where, null);
    }

    public int delete_POI_ById(Integer rowId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_POI_WayPointId + "='" + rowId + "'";

        return db.delete(TABLE_POI, where, null);

    }
    public int delete_Push_by_Poi_type(String type) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_POI_Type + "='" + type + "'";

        return  db.delete(TABLE_TRIP, where, null);



    }

    public int delete_Night_POI_ById(Integer tripId,Integer poiId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_POI_WayPointId_N + " = " + poiId + " And "+KEY_POI_TRIP_ID_N +" = "+tripId+ "";


        return db.delete(TABLE_POI_Night, where, null);
    }
    public int delete_All_Night_POI_ById(Integer tripId,Integer poiId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_POI_ID_N + " = " + poiId + " And "+KEY_POI_TRIP_ID_N +" = "+tripId+ "";


        return db.delete(TABLE_POI_Night, where, null);
    }


    public int delete_Destination_ById(Integer waypointid) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_DES_WayPointId + " = " + waypointid ;


        return db.delete(TABLE_DESTINATION, where, null);
    }

    public int delete_All_Destination_ById(Integer tripid) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where = KEY_DES_TRIP_ID + " = " + tripid ;


        return db.delete(TABLE_DESTINATION, where, null);
    }



//Delete all trip

    public int deleteAllTrip() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_POI, null, null);
        db.delete(TABLE_POI_Night,null,null);
        return db.delete(TABLE_TRIP, null, null);
    }


//Delete all trip

    public int deleteAllTPOI() {

        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_POI, null, null);
    }


//get count total km POI

    public Integer get_POI_TotaL_km(Integer tripId) {
        Integer totalKm = 0;
// Select All Query
        String selectQuery = "SELECT Sum(poiKM) FROM " + TABLE_POI + " where " + KEY_POI_TRIP_ID + '=' + tripId;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst())
            totalKm = c.getInt(0);
        else
            totalKm = -1;
        c.close();

// return Km
        return totalKm;
    }

    public Double get_POI_TotaL_Minute(Integer tripId) {
        Double totalminute = 0.0;
// Select All Query
        String selectQuery = "SELECT Sum("+KEY_POI_TIME+") FROM " + TABLE_POI + " where " + KEY_POI_TRIP_ID + '=' + tripId;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst())
            totalminute = c.getDouble(0);
        else
            totalminute = -1.0;
        c.close();

// return Km
        return totalminute;
    }

    //Update Stay Time
    public int update_stay_km_by_PoiId(Integer waypointid, Double KM) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_STAY_TIME_HRS, KM);
        return db.update(TABLE_POI, args, KEY_POI_WayPointId + "=" + waypointid, null);
    }

    public Integer get_first_row_tripId() {
        Integer totalKm = 0;
// Select All Query
        String selectQuery = "SELECT "+KEY_TRIP_ID +" FROM " + TABLE_POI;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst())
            totalKm = c.getInt(0);
        else
            totalKm = -1;
        c.close();

// return Km
        return totalKm;
    }


    //Update Stay Time
    public int update_resumedate_time_by_PoiId(Integer poiId, String resumedate,String resumetime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_POI_ID, poiId);
        args.put(KEY_POI_RESUME_DATE, resumedate);
        args.put(KEY_POI_RESUME_TIME, resumetime);
        if(resumedate!=null)
            args.put(KEY_POI_STAY,true);
        else

            args.put(KEY_POI_STAY,false);

        return db.update(TABLE_POI, args, KEY_POI_ID + "=" + poiId, null);
    }

    //Update Stay Time
    public int update_stay_km_by_NightPoiId(Integer waypointid, Double KM) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_POI_WayPointId_N, waypointid);
        args.put(KEY_STAY_TIME_HRS_N, KM);
        return db.update(TABLE_POI_Night, args, KEY_POI_WayPointId_N + "=" + waypointid, null);
    }

    //Update Stay  Address
    public int update_stay_at_destination(Integer tripid,String address)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_TRIP_Des_Address, address);

        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    //Update Stay  Address
    public int update_Return_trip_date_time(Integer tripid,String returndate,String returntime,Boolean startdateupdate)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_Return_START_DATE, returndate);
        args.put(KEY_Return_START_TIME, returntime);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);


    }

    //Update Stay  Address
    public int update_Start_trip_date_time(Integer tripid,String returndate,String returntime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_START_DATE, returndate);
        args.put(KEY_START_TIME, returntime);
        if (returndate.length() > 1)
            args.put(KEY_TRIP_STATUS, "UpComing");


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }


    public int update_end_trip_date_time(Integer tripid,String returndate,String returntime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        Log.d("updateendtime","tripid"+String.valueOf(tripid)+"date"+returndate+"time"+returntime);
        args.put(KEY_END_DATE, returndate);
        args.put(KEY_END_TIME, returntime);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    public int update_return_end_trip_date_time(Integer tripid,String returndate,String returntime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_Return_END_DATE, returndate);
        args.put(KEY_Return_END_TIME, returntime);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }


    //Update Stay  Address
    public int update_Return_trip_status(Integer tripid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_TRIP_Return_status, true);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    //Update  trip status onging
    public int update_trip_status(Integer tripid,String tripstatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        //    args.put(KEY_TRIP_STATUS, "");
        //  db.update(TABLE_TRIP, args,null, null);
        args.put(KEY_TRIP_STATUS, tripstatus);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }





    public int check_trip_in_local_db(Integer tripId) {
        String countQuery = "SELECT  * FROM " + TABLE_TRIP + " where " + KEY_TRIP_ID + '=' + tripId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
    public int check_des_trip_in_local_db(Integer tripId,Integer poiid) {
        String countQuery = "SELECT  * FROM " + TABLE_DESTINATION + " where " + KEY_DES_TRIP_ID + '=' + tripId+" AND "+KEY_POI_ID_S +'='+poiid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    //update staytime
    //Update  trip status onging
    public int update_stay_time_destination_plan(Integer waypointid,Double staytime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        //    args.put(KEY_TRIP_STATUS, "");
        //  db.update(TABLE_TRIP, args,null, null);
        args.put(KEY_DES_STAY_TIME_HRS, staytime);


        return db.update(TABLE_DESTINATION, args, KEY_DES_WayPointId + "=" + waypointid, null);
    }


    public int update_all_trip_date_time(Integer tripid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_START_DATE, "");
        args.put(KEY_START_TIME, "");
        args.put(KEY_Return_END_DATE, "");
        args.put(KEY_Return_END_TIME, "");
        args.put(KEY_Return_START_DATE, "");
        args.put(KEY_Return_START_TIME, "");
        args.put(KEY_END_DATE, "");
        args.put(KEY_END_TIME, "");
        args.put(KEY_TRIP_Return_status,false);
        args.put(KEY_TRIP_STATUS,"Saved");

        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }




    public int delete_All_Push() {
        SQLiteDatabase db = this.getWritableDatabase();
        return  db.delete(TABLE_Push_Notification, null, null);



    }

    //Update push status
    public int update_push_status(Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_POI_STAY, false);

        return db.update(TABLE_Push_Notification, args, KEY_ID + "=" + id, null);
    }

    public int check_poi_trip_in_local_db(Integer tripId,Integer poiid,String poitype,Integer returnstatus) {
        String countQuery="";
        if(poitype.contains("Service"))
            countQuery = "SELECT  * FROM " + TABLE_POI + " where " + KEY_POI_TRIP_ID + '=' + tripId+" AND "+KEY_POI_DesID +'='+poiid +" AND "+KEY_POI_Return_status+ '='+returnstatus;
        else
            countQuery = "SELECT  * FROM " + TABLE_POI + " where " + KEY_POI_TRIP_ID + '=' + tripId+" AND "+KEY_POI_DesID +'='+poiid+" AND "+KEY_POI_Return_status+ '='+returnstatus;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
    public int check_poi_night_trip_in_local_db(Integer tripId,Integer poiid) {
        String countQuery="";

        countQuery = "SELECT  * FROM " + TABLE_POI_Night + " where " + KEY_POI_TRIP_ID_N + '=' + tripId+" AND "+KEY_POI_ID_S_N +'='+poiid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
    //Update Stay  Address
    public int update_place_stay_at_destination(Integer tripid,String desplace,Double latitude,Double longitude)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_TRIP_Des_Address, desplace);
        args.put(KEY_TripGooglePlaceLat,latitude);
        args.put(KEY_TripGooglePlaceLong,longitude);



        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    //Update one way completed
    public int update_trip_one_way_completed(Integer tripid,String tripstatus,Integer finalstatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_TripIsOneSideCompleted, true);
        args.put(KEY_TRIP_STATUS,tripstatus);
        args.put(KEY_TripFinalStatus,finalstatus);

        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    public int update_trip_one_way_completed1(Integer tripid,String tripstatus,Integer finalstatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_TripIsOneSideCompleted, false);
        args.put(KEY_TRIP_STATUS,tripstatus);
        args.put(KEY_TripFinalStatus,finalstatus);

        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    public int update_Return_trip_date_time_ETA(Integer tripid,String returndate,String returntime,String returndateETA,String returntimeETA)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_Return_START_DATE, returndate);
        args.put(KEY_Return_START_TIME, returntime);
        args.put(KEY_Return_END_DATE, returndateETA);
        args.put(KEY_Return_END_TIME, returntimeETA);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);


    }

    public int check_des_trip_in_local_db_get_wayid(Integer tripId,Integer poiid) {
        String countQuery = "SELECT  * FROM " + TABLE_DESTINATION + " where " + KEY_DES_TRIP_ID + '=' + tripId + " AND " + KEY_POI_ID_S + '=' + poiid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        int cnt = cursor.getInt(cursor.getColumnIndex(KEY_DES_WayPointId));
        cursor.close();
        return cnt;
    }

    //Update trip eta diff
    public int update_trip_eta_diff_seconds(Integer tripid,Double seconds,Integer return_plan)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        if(return_plan==0) {
            args.put(KEY_TRIP_ETA_DIFF, seconds);
        }
        else {
            args.put(KEY_TRIP_Return_ETA_DIFF, seconds);
        }

        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }



    public int update_destination_plan_km_time(Integer tripid,Double km,Double time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        //    args.put(KEY_TRIP_STATUS, "");
        //  db.update(TABLE_TRIP, args,null, null);
        args.put(KEY_DES_KM, km);
        args.put(KEY_DES_TIME,time);


        return db.update(TABLE_DESTINATION, args, KEY_DES_TRIP_ID + "=" + tripid, null);
    }


    public int update_trip_poi_eta_diff_seconds(Integer waypointid,Double second)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();


        args.put( KEY_POI_ETA_DIFF,second);


        return db.update(TABLE_POI, args, KEY_POI_WayPointId + "=" + waypointid, null);
    }



    public int update_trip_poi_eta_diff_seconds_zero(Double second,Integer tripid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();


        args.put(KEY_POI_ETA_DIFF,0);
        args.put(KEY_STAY_TIME_HRS,2.0);

        ContentValues args1 = new ContentValues();
        args1.put(KEY_TRIP_ETA_DIFF,0);
        db.update(TABLE_TRIP, args1,KEY_TRIP_ID + "="+tripid, null);

        return db.update(TABLE_POI, args,KEY_POI_TRIP_ID + "="+tripid, null);
    }



    public int update_trip_poi_track_time_km(Integer waypointid,Double km,Double time,Integer legno,Integer step_no)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();


        args.put(KEY_POI_TIME,time);
        args.put(KEY_POI_KM,km);
        args.put(KEY_POI_Leg_no,legno);
        args.put(KEY_POI_Step_no,step_no);


        return db.update(TABLE_POI, args, KEY_POI_WayPointId + "=" + waypointid, null);
    }

    public int update_trip_track_time_km(Integer tripid,Double km,Double time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_TRIP_KM,km);
        args.put(KEY_TRIP_TIME,time);


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }


    public int update_trip_poi_track_time_km_google(Integer waypointid,Double km,Double time,Integer legno,Integer stepno)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();


        args.put(KEY_POI_TIME,time);
        args.put(KEY_POI_KM, km);
        args.put(KEY_POI_Leg_no,legno);
        args.put(KEY_POI_Step_no,stepno);
        return db.update(TABLE_POI, args, KEY_POI_WayPointId + "=" + waypointid, null);
    }


    public List<Create_Poi> get_POI_TRIP(Integer tripId, Integer returnstatus) {

        List<Create_Poi> PoiList = new ArrayList<Create_Poi>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_POI + " where " + KEY_POI_TRIP_ID + '=' + tripId +" AND "+KEY_POI_Return_status+ '='+returnstatus+ " ORDER BY " + KEY_POI_Leg_no +","+KEY_POI_Actual_Time;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Create_Poi item = new Create_Poi();
                item.setPoiId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setPoiName(cursor.getString(2));
                item.setStayTime(cursor.getDouble(3));
                item.setPoiLatitude(cursor.getDouble(4));
                item.setPoiLongitude(cursor.getDouble(5));
                item.setPoiImage(cursor.getBlob(6));
                if (cursor.getString(7).contains("1"))
                    item.setPoiStay(true);
                else
                    item.setPoiStay(false);

                item.setResumeDate(cursor.getString(8));
                item.setResumeTime(cursor.getString(9));
                item.setKM(cursor.getDouble(10));
                item.setTime(cursor.getDouble(11));
                item.setPoiDate(cursor.getString(12));
                item.setArrivalTime(cursor.getDouble(13));
                item.setDepartureTime(cursor.getDouble(14));
                item.setPoitype(cursor.getString(15));
                item.setWayPointId(cursor.getInt(16));
                item.setPoiDesID(cursor.getInt(17));
                item.setPoiServerId(cursor.getInt(18));
                item.setAttractionCount(cursor.getInt(19));

                if (cursor.getString(20).contains("1"))
                    item.setReturnstatus(true);
                else
                    item.setReturnstatus(false);

                item.setPoidescription(cursor.getString(21));

                // Log.d("poiimagename",cursor.getString(22).toString());
                //   item.setImagename("20161006_06303.png");

                item.setImagename(cursor.getString(22));
                item.setPoi_diff_seconds(cursor.getDouble(23));
                item.setActual_Km(cursor.getDouble(24));
                item.setActual_Time(cursor.getDouble(25));
                item.setLegNo(cursor.getInt(26));
                item.setStepNo(cursor.getInt(27));
                if (cursor.getString(28).contains("1"))
                    item.setPoi_track_flag(true);
                else
                    item.setPoi_track_flag(false);

                item.setPoiside(cursor.getString(cursor.getColumnIndex(KEY_POI_Side)));



                item.setFlag(false);
                // Adding poi to list
                PoiList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return PoiList;
    }


    public int update_Start_trip_date_time_current(Integer tripid,String returndate,String returntime, Integer return_plan)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        if(return_plan==0) {
            args.put(KEY_START_DATE, returndate);
            args.put(KEY_START_TIME, returntime);
        }
        else
        {
            args.put(KEY_Return_START_DATE, returndate);
            args.put(KEY_Return_START_TIME, returntime);
        }

        args.put(KEY_TRIP_STATUS, "OnGoing");


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    public int update_poi_track_flag(Integer waypointid,Boolean flag)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_POI_Track_flag,flag);

        return db.update(TABLE_POI, args, KEY_POI_WayPointId + "=" + waypointid, null);
    }


    public int update_KM_Time_Trip(Integer tripid,Double km,Double time, Integer return_plan)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        if(return_plan==0) {
            args.put(KEY_TRIP_KM, km);
            args.put(KEY_TRIP_TIME, time);
        }
        else
        {
            args.put(KEY_TRIP_ReturnKM, km);
            args.put(KEY_TRIP_ReturnTime, time);
        }


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }

    //Update  trip status onging
    public int update_source_Latlong(Integer tripid,Double latitude,Double longitude ,Integer return_plan)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        if(return_plan==0) {
            args.put(KEY_SOURCE_LATITUDE, latitude);
            args.put(KEY_SOURCE_LONGITUDE, longitude);
        }
        else
        {
            args.put(KEY_DESTINATION_LATITUDE, latitude);
            args.put(KEY_DESTINATION_LONGITUDE, longitude);
        }


        return db.update(TABLE_TRIP, args, KEY_TRIP_ID + "=" + tripid, null);
    }



    public List<Create_Trip> get_All_TRIP_ByFinalStatus()
    {
        List<Create_Trip> TripList = new ArrayList<Create_Trip>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TRIP+" where "+KEY_TripFinalStatus+"='"+1+"'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Create_Trip item = new Create_Trip();
                item.setId(cursor.getInt(0));
                item.setTripId(cursor.getInt(1));
                item.setSource(cursor.getString(2));
                item.setDestination(cursor.getString(3));
                item.setStartdate(cursor.getString(4));
                item.setStarttime(cursor.getString(5));
                item.setEnddate(cursor.getString(6));
                item.setEndtime(cursor.getString(7));
                item.setTripname(cursor.getString(8));
                item.setSource_latitude(cursor.getDouble(9));
                item.setSource_longitude(cursor.getDouble(10));
                item.setDestination_latitude(cursor.getDouble(11));
                item.setDestination_longitude(cursor.getDouble(12));
                item.setSource_image(cursor.getBlob(13));
                item.setDestination_image(cursor.getBlob(14));
                item.setKM(cursor.getDouble(15));
                item.setTime(cursor.getDouble(16));
                item.setTripstatus(cursor.getString(17));
                item.setTriprating(cursor.getString(18));
                item.setSourceId(cursor.getInt(19));
                item.setDestinationId(cursor.getInt(20));
                item.setDes_address(cursor.getString(21));
                if (cursor.getString(22).contains("1"))
                    item.setReturnstatus(true);
                else
                    item.setReturnstatus(false);

                item.setReturn_start_date(cursor.getString(23));
                item.setReturn_start_time(cursor.getString(24));
                item.setReturn_end_date(cursor.getString(25));
                item.setReturn_end_time(cursor.getString(26));
                if(cursor.getString(27).contains("1"))
                    item.setTripIsOneSideCompleted(true);
                else
                    item.setTripIsOneSideCompleted(false);
                // Adding Trip to list
                TripList.add(item);
            } while (cursor.moveToNext());
        }

// return contact list
        return TripList;
    }
}