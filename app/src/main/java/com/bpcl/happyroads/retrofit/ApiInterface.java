package com.bpcl.happyroads.retrofit;


import com.bpcl.happyroads.Pojo.AddFavourite;
import com.bpcl.happyroads.Pojo.AllTravelType;
import com.bpcl.happyroads.Pojo.CalculateDistance;
import com.bpcl.happyroads.Pojo.CopyTrip;
import com.bpcl.happyroads.Pojo.CreateTripApi;
import com.bpcl.happyroads.Pojo.DeleteNotification;
import com.bpcl.happyroads.Pojo.DeleteTrip;
import com.bpcl.happyroads.Pojo.DeleteVehicle;
import com.bpcl.happyroads.Pojo.DestinationName;
import com.bpcl.happyroads.Pojo.Direction;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.ExploreWeather;
import com.bpcl.happyroads.Pojo.ForgotPwd;
import com.bpcl.happyroads.Pojo.GetAdvancePOIDetails;
import com.bpcl.happyroads.Pojo.GetFavourites;
import com.bpcl.happyroads.Pojo.GetRO;
import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.Google_Distance_Calculate;
import com.bpcl.happyroads.Pojo.ImageUpload;
import com.bpcl.happyroads.Pojo.Insert_Poi;
import com.bpcl.happyroads.Pojo.InviteMyFrined;
import com.bpcl.happyroads.Pojo.ListDefaultValues;
import com.bpcl.happyroads.Pojo.ListServiceCategory;
import com.bpcl.happyroads.Pojo.LogOut;
import com.bpcl.happyroads.Pojo.MyTrip;
import com.bpcl.happyroads.Pojo.NotificationList;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.Place;
import com.bpcl.happyroads.Pojo.RemoveTripPoi;
import com.bpcl.happyroads.Pojo.RoutePojo;
import com.bpcl.happyroads.Pojo.SearchCountry;
import com.bpcl.happyroads.Pojo.ServiceImage;
import com.bpcl.happyroads.Pojo.ServicesPojo;
import com.bpcl.happyroads.Pojo.Signup;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Pojo.TollBooth;
import com.bpcl.happyroads.Pojo.TripCompleted;
import com.bpcl.happyroads.Pojo.TripListforPOIPlanning;
import com.bpcl.happyroads.Pojo.UpdateDeviceToken;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.Pojo.VehicleDetails;
import com.bpcl.happyroads.Utils.Constants;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by ADMIN on 7/6/2016.
 */
public interface ApiInterface {

   /* @Headers(
            "Content-type: application/json"
    )*/

    @POST("api/TravelCityAPI/GetAllTravelType?apiKey=shradAndroid&apiVer="+ Constants.version)
    Call<ArrayList<AllTravelType>> allTravellType();

    @FormUrlEncoded
    @POST("api/TravelCityAPI/SearchCity?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<SearchCountry>> countrylist(@Field("json") String IsActive);


    @FormUrlEncoded
    @POST("api/DestinationApi/GetDestinationList?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<DestinationName>> GetDestinationList(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/DestinationApi/ExploreDestination?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<ExploreDestination>> exploredestination(@Field("json") String IsActive);


    @FormUrlEncoded
    @POST("api/poiapi/ExplorePoi?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<ExplorePOI>> explorepoi(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/SignUp?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<Signup> signup(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/UpdatePassword?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<Signup>> updatePassword(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/Signin?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> signIn(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/ConfirmUser?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> otp(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/tripapi/CreateTrip?apiKey=chandDroid&apiVer="+Constants.version)
    Call<CreateTripApi> createTrip(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/tripapi/GetTrip?apiKey=chandDroid&apiVer="+Constants.version)
    Call<ArrayList<MyTrip>> GetMyTrip(@Field("json") String IsActive);


    @FormUrlEncoded
    @POST("api/notifications/NotificationList?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<NotificationList>> NotificationList(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/notifications/DeleteNotifications?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<DeleteNotification>> DeleteNotification(@Field("json") String IsActive);

    @GET()
    Call<Google_Distance_Calculate> GetDistanceTime(@Url String url);
    @FormUrlEncoded
    @POST("api/userMobileApi/InsertUpdateVehicle?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<VehicleDetails>> AddVehicleDetails(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/GetMyVehicleByUmId?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<GetVehicleDetails>> GetVehicleDetails(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/ForgetPassword?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<ForgotPwd>> forgotPwd(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/RouteApi/ExploreRoute?apiKey=maheshdroid&apiVer="+Constants.version)
    Call<ArrayList<RoutePojo>> RouteExplore(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/ServiceApi/ExploreServices?apiKey=maheshdroid&apiVer="+Constants.version)
    Call<ArrayList<ServicesPojo>> servicesReponse(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/DestinationApi/ExploreWeather?apiKey=pratdroid&apiVer="+Constants.version)
    Call<ArrayList<ExploreWeather>> exploreweather(@Field("json") String IsActive);



      @FormUrlEncoded
    @POST("api/userMobileApi/EditUser?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> editProfile(@Field("json") String IsActive);


    @FormUrlEncoded
    @POST("api/userMobileApi/AccessViaGoogleOrFB?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<Signup> AccessViaGoogleOrFB(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/GetUser?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> GetUser(@Field("json") String IsActive);


    @FormUrlEncoded
    @POST("api/TripAPI/InsertUserTripPlans?apiKey=chanDroid&apiVer="+Constants.version)
    Call<ArrayList<Insert_Poi>> InsertPoi(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/ResendConfirmationCode?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> ResendConfirmationCode(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/DeleteVehicle?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<DeleteVehicle>> deleteVehicle(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/tripapi/RemoveTripPlans?apiKey=chanDroid&apiVer="+Constants.version)
    Call<ArrayList<RemoveTripPoi>> RemovePoi(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/ServiceApi/GetServiceCategories?apiKey=chanDroid&apiVer="+Constants.version)
    Call<ArrayList<ListServiceCategory>> ListServiceCategory(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/roapi/Explorero?apiKey=chanDroid&apiVer="+Constants.version)
    Call<ArrayList<GetRO>> ListRo(@Field("json") String IsActive);

    @FormUrlEncoded

    @POST("api/FavApi/AddRemoveFavourite?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<AddFavourite>> addFavourites(@Field("json") String IsActive);

    @FormUrlEncoded

    @POST("api/FavApi/GetFavourite?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<GetFavourites>> getFavourites(@Field("json") String IsActive);

    @FormUrlEncoded

    @POST("api/poiapi/GetAdvancePoi?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<GetAdvancePOIDetails>> getAdvancePoiDetail(@Field("json") String IsActive);



    @FormUrlEncoded
    @POST("api/tripapi/DeleteTrip?apiKey=chandroid&apiVer="+Constants.version)
    Call<DeleteTrip> DeleteTrip(@Field("json") String IsActive);


    @FormUrlEncoded
    @POST("api/userMobileApi/exploreTollbooth?apiKey=chanddroid&apiVer="+Constants.version)
    Call<ArrayList<TollBooth>> GetToolBooth(@Field("json") String IsActive);

    @GET()
    Call<Place> GetPlaces(@Url String url);

    @FormUrlEncoded
    @POST("api/tripapi/UpdateTrip?apiKey=chandDroid&apiVer="+Constants.version)
    Call<UpdateTrip> updateTrip(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/TripAPI/UpdateStatus?apiKey=chandDroid&apiVer="+Constants.version)
    Call<ArrayList<UpdateTrip>> updateTripstatus(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/GetValue?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ServiceImage> GetValue(@Field("json") String IsActive);

    //RSA


    @POST("api/RSA_Ver1/Policies?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<SubsciptionPlans>> getSubscribepolicy();


    @FormUrlEncoded
    @POST("api/RSA_Ver1/RegisterCustomer?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> RSACustomerRegistration(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/RSA_Ver1/CreateVehicle?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> RSACreateVehicle(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/RSA_Ver1/AddNewIncidents?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<OTP>> AddNewIncidents(@Field("json") String IsActive);

    @POST()
    Call<String> RSAGetLink(@Url String url);
    /*@FormUrlEncoded
    @POST("http://67.220.184.226/PTO/api/RSA_Ver1/PolicyPurchase?")
    Call<String> RSAGetLink(@Field("json") String IsActive);*/

    @POST()
    Call<ArrayList<CalculateDistance>> GetDistanceTimebylatlong(@Url String url);

    @FormUrlEncoded
    @POST("api/tripapi/GetMyTripByDestination?apiKey=chandDroid&apiVer="+Constants.version)
    Call<ArrayList<TripListforPOIPlanning>> GetMyTripByDestination(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/GetAllValue?apiKey=chandDroid&apiVer="+Constants.version)
    Call<ArrayList<ListDefaultValues>> GetDefaultValues(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/userMobileApi/DeviceTokenUpdate?apiKey=shradAndroid&apiVer="+Constants.version)
    Call<ArrayList<LogOut>> Logout(@Field("json") String IsActive);

    @POST()
    Call<ArrayList<InviteMyFrined>> GetDynamicValue(@Url String url);

    @FormUrlEncoded
    @POST("api/userMobileApi/DeviceTokenUpdate?apiKey=chandDroid&apiVer="+Constants.version)
    Call<ArrayList<UpdateDeviceToken>> DeviceTokenUpdate(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/TripAPI/UpdateTripToOneSideCompleted?apiKey=chandDroid&apiVer="+Constants.version)
    Call<ArrayList<TripCompleted>> UpdateTripToOneSideCompleted(@Field("json") String IsActive);

    @FormUrlEncoded
    @POST("api/tripapi/CopyTrip?apiKey=chandDroid&apiVer="+Constants.version)
    Call<CopyTrip> copytrip(@Field("json") String IsActive);

    @GET()
    Call<Direction> GetRoute(@Url String url);

    @Multipart
    @POST("api/UploadFile/Post")
    Call<ArrayList<ImageUpload>> postImage(@Part MultipartBody.Part image);

}
