package com.bpcl.happyroads;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 09/12/2016.
 */
public class VehicleList extends Fragment {
     LinearLayout ll_vehicle_list;
    OTP otp;
    SessionManager sessionManager;
     RelativeLayout error_layout;
    ArrayList<GetVehicleDetails> getVehicleList=new ArrayList<>();
     TextView tv_add_vehicle;
     Bundle bundle;
    SubsciptionPlans subsciptionPlansArrayList;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.vehicle_list, container, false);
        ll_vehicle_list=(LinearLayout)v.findViewById(R.id.ll_vehicle_list);
        tv_add_vehicle=(TextView)v.findViewById(R.id.tv_add_vehicle);
        ((ExploreActivity)getActivity()).tv_text_header.setText("Trip Service");

        sessionManager = new SessionManager(getActivity());
        error_layout =(RelativeLayout) v.findViewById(R.id.error_layout);

        bundle = this.getArguments();
        if (bundle != null)
        {

            subsciptionPlansArrayList = (SubsciptionPlans) bundle.getSerializable("packagedetails");

        }
        otp = sessionManager.get_Authenticate_User();
        GetVehicleList();


        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });
        tv_add_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b=new Bundle();
                b.putSerializable("packagedetails",subsciptionPlansArrayList);
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new AddVehicleFragment(),b);




            }
        });
        return v;
    }
    private void GetVehicleList() {


        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


            String json = "[{\"vehicleUmId\":\""+otp.getUmId()+"\"}]";
            Log.d("System out", "In Get Vehicle List  " + json);

            GetVehicleListAPI(json,error_layout);

        }
        else
        {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
        }

    }

    private void GetVehicleListAPI(final String json,final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<GetVehicleDetails>> call = apiService.GetVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<GetVehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVehicleDetails>> call, Response<ArrayList<GetVehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    getVehicleList.clear();
                    getVehicleList= response.body();
                    if (getVehicleList.get(0).getResStatus() == true)
                    {
                        //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);
                        VehicleList();
                    }
                    else
                    {
                        VehicleList();
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }



                }


            }

            @Override
            public void onFailure(Call<ArrayList<GetVehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }
    public void VehicleList() {

        if (getVehicleList.size()>0) {
            if(getVehicleList.get(0).getResStatus()==true) {
                ll_vehicle_list.setVisibility(View.VISIBLE);
                if (ll_vehicle_list.getChildCount() > 0) {
                    ll_vehicle_list.removeAllViews();
                }
                for (int i = 0; i < getVehicleList.size(); i++) {


                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View v;
                    v = inflater.inflate(R.layout.vehicle_list_account, null);
                    final TextView tv_while_line = (TextView) v.findViewById(R.id.tv_while_line);
                    final TextView tv_vehicle_make = (TextView) v.findViewById(R.id.tv_vehicle_make);
                    final TextView tv_vehicle_year_color = (TextView) v.findViewById(R.id.tv_vehicle_year_color);
                    final ImageView iv_vehicle_explore = (ImageView) v.findViewById(R.id.iv_vehicle_explore);
                    final LinearLayout ll_vehicle_explore = (LinearLayout) v.findViewById(R.id.ll_vehicle_explore);
                    final LinearLayout ll_vehicle_edit = (LinearLayout) v.findViewById(R.id.ll_vehicle_edit);
                    final LinearLayout ll_vehicle_delete = (LinearLayout) v.findViewById(R.id.ll_vehicle_delete);
                    final LinearLayout ll_subscribe = (LinearLayout) v.findViewById(R.id.ll_subscribe);
                    Date datecurrent = null;
                    Date dateExpiry = null;

                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                    Calendar calendar = Calendar.getInstance();

                    String temp1="";
                    String eDate="";
                    temp1=sdf1.format(calendar.getTime());
                   // eDate=getVehicleList.get(i).getSubscriptionExpiry().replace("T"," ");


                    try {
                        datecurrent = sdf1.parse(temp1);
                        eDate= Constants.formatDate(getVehicleList.get(i).getSubscriptionExpiry().replace("T", " "), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy hh:mm a");

                        dateExpiry=sdf1.parse(eDate);

                        Log.d("System out","eDate___"+eDate);
                        Log.d("System out","current date__"+datecurrent);
                        Log.d("System out","Expiry date__"+dateExpiry+"  "+datecurrent.after(dateExpiry));
                    }
                    catch (ParseException e)
                    {
                        e.printStackTrace();
                    }


                    try {
                        if(getVehicleList.get(i).getSubscriptionID().equalsIgnoreCase("") || datecurrent.after(dateExpiry))
                            ll_subscribe.setVisibility(View.VISIBLE);
                        else
                            ll_subscribe.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ll_subscribe.setVisibility(View.VISIBLE);

                    }

                    ll_vehicle_delete.setVisibility(View.GONE);
                    ll_vehicle_edit.setVisibility(View.GONE);
                    ll_vehicle_explore.setVisibility(View.VISIBLE);
                    if(i==getVehicleList.size()-1)
                    {
                        tv_while_line.setVisibility(View.GONE);
                    }

                    iv_vehicle_explore.setRotation(-90);

                    try {
                        tv_vehicle_make.setText(getVehicleList.get(i).getVehicleMake());
                        tv_vehicle_year_color.setText(getVehicleList.get(i).getVehicleRegNumber()+", "+getVehicleList.get(i).getVehicleYear() + ", " + getVehicleList.get(i).getVehicleColor());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ll_vehicle_list.addView(v);
                    v.setId(i);

                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                           /* Bundle b=new Bundle();
                            b.putSerializable("vehicledetails",getVehicleList.get(view.getId()));
                            b.putSerializable("packagedetails",subsciptionPlansArrayList);*/

                            Bundle bundle=new Bundle();
                            bundle.putSerializable("vehicledetails",getVehicleList.get(view.getId()));
                            bundle.putSerializable("packagedetails",subsciptionPlansArrayList);
                            ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new VehicleDetailsRSA(),bundle);

                        }
                    });

                }
            }

        }else
        {
            ll_vehicle_list.removeAllViews();
            ll_vehicle_list.setVisibility(View.GONE);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity)getActivity()).tv_text_header.setText("Trip Service");

    }
}
