package com.bpcl.happyroads;

import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bpcl.happyroads.Utils.PagerAdapter;


/**
 * Created by ADMIN on 17/11/2016.
 */

public class MyTripWithSwipableFragment extends Fragment {
    private int tripiditem;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mytrip_fragment_swipable, container, false);
        final TabLayout tabLayout = (TabLayout)v.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Completed));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Ongoing));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Upcoming));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Saved));
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        ((ExploreActivity) getActivity()).tv_text_header.setText("My Trips Summary");
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity) getActivity()).ll_menu_header.setVisibility(View.VISIBLE);
        final ViewPager viewPager = (ViewPager)v.findViewById(R.id.pager);
        if (getArguments() != null) {
            tripiditem = getArguments().getInt("TripId");
            Log.d("System out","tripid from Good to go"+tripiditem);
        }
        final PagerAdapter adapter = new PagerAdapter
                (getChildFragmentManager(),tabLayout.getTabCount(),tripiditem);

        // viewPager.setOffscreenPageLimit(1);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.setAdapter(adapter);
        Integer pos;
        Integer tripid;


        tabLayout.getTabAt(getArguments().getInt("tab_pos",2)).select();

        viewPager.setCurrentItem(getArguments().getInt("tab_pos",2));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                tabLayout.setTabTextColors(getResources().getColor(R.color.colorWhite),getResources().getColor(R.color.textYellow));

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
    }
}
