package com.bpcl.happyroads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;




/**
 * Created by fipl11 on 11/07/2016.
 */
public class TutorialScreen extends Activity implements ViewPager.OnPageChangeListener{
    ViewPager gallery;
  // int[] myImageList = new int[]{R.drawable.screen_1, R.drawable.screen_2, R.drawable.screen_3, R.drawable.screen_4, R.drawable.screen_5};
 int[] myImageList = new int[]{R.drawable.screen_1 , R.drawable.screen_2, R.drawable.screen_3, R.drawable.screen_4,R.drawable.screen_5};
    private final int delay = 4000;  //for quick test put 2500
    Handler handler;
    //Runnable finishRunnable;
    SharedPreferences preferences2;
    SharedPreferences.Editor editor2;
    boolean lastPageChange = false;
    CustomPagerAdapter adapter;
    private float mStartDragX;
    boolean callHappened=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial);
        preferences2 = getSharedPreferences("tutorial", 0);
        editor2 = preferences2.edit();

        editor2.putString("tutorial", "true").commit();
        gallery = (ViewPager) findViewById(R.id.tutorial_gellery);

        adapter = new CustomPagerAdapter(TutorialScreen.this);
        gallery.setAdapter(adapter);

        // gallery.setPageTransformer(false, new IntroPageTransformer());
       gallery.setOnPageChangeListener(this);


       /* gallery.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float x = event.getX();
                switch (event.getAction())
                {



                        case MotionEvent.ACTION_DOWN:
                            mStartDragX = x;
                            break;
                        case MotionEvent.ACTION_MOVE:

                            if (event.getX() > mStartDragX) {

                                mStartDragX = event.getX();
                            } else if (event.getX() < mStartDragX)
                            {
                                int lastIdx = adapter.getCount() - 1;
                                if(lastIdx==gallery.getCurrentItem())
                                {
                                    Intent intent = new Intent(TutorialScreen.this,ExploreActivity.class);
                                    startActivity(intent);

                                    finish();
                                    overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
                                }
                                mStartDragX = event.getX();
                            }


                            break;
                    }
                    return false;






            }
        });*/

        }



       /* handler = new Handler();
        callhandler();*/


    @Override
    protected void onDestroy() {
        super.onDestroy();
       // removehandler();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int lastIdx = adapter.getCount() - 1;

        Log.d("viewpagerpos", "pos:" + position);
        if(lastPageChange && position == lastIdx && !callHappened)
        {
            callHappened=true;
            Intent intent = new Intent(TutorialScreen.this,ExploreActivity.class);
            startActivity(intent);

            finish();
            overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        int lastIdx = adapter.getCount() - 1;

        int curItem = gallery.getCurrentItem();
        if(curItem==lastIdx /*&& lastPos==lastIdx*/  && state==1)
            lastPageChange = true;
        else
            lastPageChange = false;
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return myImageList.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {

            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.collapsing_img);


                imageView.setImageResource(myImageList[position]);



            container.addView(itemView);

            itemView.setTag(position);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    }
