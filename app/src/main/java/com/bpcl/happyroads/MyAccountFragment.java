package com.bpcl.happyroads;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.bpcl.happyroads.Pojo.AllTravelType;
import com.bpcl.happyroads.Pojo.DeleteVehicle;
import com.bpcl.happyroads.Pojo.GetVehicleDetails;
import com.bpcl.happyroads.Pojo.ImageUpload;
import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.SubsciptionPlans;
import com.bpcl.happyroads.Pojo.VehicleDetails;
import com.bpcl.happyroads.Utils.CircularImageView;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.MyTextView;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/13/2016.
 */
public class MyAccountFragment extends Fragment implements
        DatePickerDialog.OnDateSetListener {
    @Bind(R.id.ll_basic_profile)
    LinearLayout llBasicProfile;
    @Bind(R.id.iv_arrow_up_personal)
    ImageView ivArrowUpPersonal;
    @Bind(R.id.ll_personal_content)
    LinearLayout llPersonalContent;
    @Bind(R.id.iv_arrow_up_vehicle)
    ImageView ivArrowUpVehicle;
    @Bind(R.id.ll_personal_header)
    LinearLayout llPersonalHeader;
    @Bind(R.id.ll_vehical_header)
    LinearLayout llVehicalHeader;
    @Bind(R.id.tv_save_my_account)
    TextView tvSaveMyAccount;
    @Bind(R.id.tv_subscribe_vehicle)
    TextView tvSubscribeVehicle;
    @Bind(R.id.tv_basic_profile)
    TextView tvBasicProfile;
    @Bind(R.id.iv_basic_profile)
    ImageView ivBasicProfile;
    @Bind(R.id.tv_advance_profile)
    TextView tvAdvanceProfile;
    @Bind(R.id.iv_advance_profile)
    ImageView ivAdvanceProfile;
    @Bind(R.id.ll_advance_profile)
    LinearLayout llAdvanceProfile;
    boolean personalInfo = true;
    boolean vehicleInfo = true;
    @Bind(R.id.ll_vehicle_content)
    LinearLayout llVehicleContent;
    @Bind(R.id.ll_vehicle_list)
    LinearLayout llVehicleList;
    @Bind(R.id.ll_co_traveller_list)
    LinearLayout llCoTravellerList;
    @Bind(R.id.tv_change_pwd_account)
    TextView tvChangePwdAccount;
    ScrollView scroll_account;
    TextView tv_D_O_B_my_account;
    String date1 = null;
    DatePickerDialog dpd;
    Calendar now;
    Boolean editFlag = false;

    SessionManager sessionManager;
    OTP otp;
    @Bind(R.id.ed_name_account)
    EditText edNameAccount;
    @Bind(R.id.ed_number_account)
    EditText edNumberAccount;
    @Bind(R.id.ed_email_account)
    EditText edEmailAccount;
    @Bind(R.id.ed_password_account)
    EditText edPasswordAccount;

    @Bind(R.id.tv_petro_card_no_account)
    EditText tvPetroCardNoAccount;
    @Bind(R.id.et_emergency_name_account)
    EditText etEmergencyNameAccount;
    @Bind(R.id.et_emergency_no_account)
    EditText etEmergencyNoAccount;
    @Bind(R.id.scroll_account)
    ScrollView scrollAccount;
    @Bind(R.id.tv_vehicle_number)
    EditText tvVehicleNumber;
    @Bind(R.id.tv_vehicle_make)
    EditText tvVehicleMake;
    @Bind(R.id.tv_vehicle_model)
    EditText tvVehicleModel;
    @Bind(R.id.tv_vehicle_year)
    EditText tvVehicleYear;
    @Bind(R.id.tv_vehicle_color)
    EditText tvVehicleColor;


    RelativeLayout error_layout;
    ArrayList<VehicleDetails> vehicleDetails = new ArrayList<>();
    ArrayList<SubsciptionPlans> subsciptionPlansArrayList = new ArrayList<>();
    ArrayList<GetVehicleDetails> getVehicleList = new ArrayList<>();
    ArrayList<DeleteVehicle> deleteVehicleArrayList = new ArrayList<>();
    ImageView iv_puc;
    boolean puc = false;
    int vehicle_umid = 0;
    int templen = 0;
    ArrayList<OTP> signupList = new ArrayList<OTP>();
    GridView gv_category_list_account, gv_co_traveller_list_account;
   /* String[] Stringsss = new String[]{"Nature", "Historical",
            "Adventure", "Wildlife"};*/

    ArrayList<String> Stringsss = new ArrayList<>();
    boolean isselected[];
    boolean isselectedCoTraveller[];
    ArrayList<String> selectedcategories = new ArrayList<>();

    String[] StringCoTravellers = new String[]{"Solo", "Friends",
            "Spouse/Partener", "Family with Kids", "Family with Parents"};

    TextView tvNameText;

    TextView tvEmailText;

    TextView tvNameEmergencyText;

    TextView tvMobileEmergencyText,tv_vehicle_reg_no_text,tv_make_text,tv_model_text,tv_year_text,tv_color_text;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    String selectedcat = "";
    Dialog subdialog;
    int vehiclepos = -1;
    String space = " ";
    String tempstring = " ";

    CircularImageView user_profile_my_account;
    private static int TAKE_PICTURE = 1, SELECT_PICTURE = 0, PIC_CROP = 2;
    String path = "", imgName = "", imgStore = "",
            ResponseString, nameOFimageName = "";
    static String selectedImagePath = "";
    File f;
    Bitmap bm;
    long timeForImgname;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options;
    int VehicleIdForRSASubscription = -1;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_account_fragment, container, false);
        scroll_account = (ScrollView) v.findViewById(R.id.scroll_account);
        ((ExploreActivity) getActivity()).tv_text_header.setText("My Account");
        error_layout = (RelativeLayout) v.findViewById(R.id.error_layout);
        tv_D_O_B_my_account = (TextView) v.findViewById(R.id.tv_D_O_B_my_account);
        gv_co_traveller_list_account = (GridView) v.findViewById(R.id.gv_co_traveller_list_account);
        gv_category_list_account = (GridView) v.findViewById(R.id.gv_category_list_account);
        iv_puc = (ImageView) v.findViewById(R.id.iv_puc);
        user_profile_my_account = (CircularImageView) v.findViewById(R.id.user_profile_my_account);
        tvNameText = (TextView) v.findViewById(R.id.tv_name_text);
        tvEmailText = (TextView) v.findViewById(R.id.tv_email_text);
        tvNameEmergencyText = (TextView) v.findViewById(R.id.tv_name_emergency_text);
        tvMobileEmergencyText = (TextView) v.findViewById(R.id.tv_mobile_emergency_text);
        tv_vehicle_reg_no_text = (TextView) v.findViewById(R.id.tv_vehicle_reg_no_text);
        tv_make_text = (TextView) v.findViewById(R.id.tv_make_text);
        tv_model_text = (TextView) v.findViewById(R.id.tv_model_text);
        tv_year_text = (TextView) v.findViewById(R.id.tv_year_text);
        tv_color_text = (TextView) v.findViewById(R.id.tv_color_text);

        tvNameText.setText(Html.fromHtml(getResources().getString(R.string.Name)));
        tvEmailText.setText(Html.fromHtml(getResources().getString(R.string.Email)));
       // tvNameEmergencyText.setText(Html.fromHtml(getResources().getString(R.string.Name)));
       // tvMobileEmergencyText.setText(Html.fromHtml(getResources().getString(R.string.Mobile)));
        tv_vehicle_reg_no_text.setText(Html.fromHtml(getResources().getString(R.string.veh_reg_no)));
        tv_make_text.setText(Html.fromHtml(getResources().getString(R.string.make)));
        tv_model_text.setText(Html.fromHtml(getResources().getString(R.string.model)));
        tv_year_text.setText(Html.fromHtml(getResources().getString(R.string.year)));
        tv_color_text.setText(Html.fromHtml(getResources().getString(R.string.color)));

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity()).threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()

                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO) // Not
                // necessary
                // in
                // common
                .build();

        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        options = new DisplayImageOptions.Builder()
                /*.showImageForEmptyUri(R.drawable.no_image_available)
                .showImageOnFail(R.drawable.no_image_available)*/
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        user_profile_my_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });


        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        ButterKnife.bind(this, v);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scroll_account.setNestedScrollingEnabled(true);
        }
        ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity) getActivity()).onBackPressed();
            }
        });

        sessionManager = new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();
        selectedcat = otp.getUmPreferredType();

        isselectedCoTraveller = new boolean[StringCoTravellers.length];
        for (int i = 0; i < StringCoTravellers.length; i++) {
            if (otp.getUmCoTraveller().contains(StringCoTravellers[i])) {
                isselectedCoTraveller[i] = true;
            }
        }
        if (otp != null) {
            setdata();
        }
        Gson gson = new Gson();
        String jsonOutput = sharedpreferences.getString("CategoryList", "");
        Type listType = new TypeToken<ArrayList<AllTravelType>>() {
        }.getType();
        ArrayList<AllTravelType> posts = gson.fromJson(jsonOutput, listType);
        Stringsss.clear();
        for (int i = 0; i < posts.size(); i++) {
            Stringsss.add(posts.get(i).getTTypeName());

        }

        tvPetroCardNoAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (tvPetroCardNoAccount.getText().toString().length() == 0) {
                    tempstring = "";
                }
                if (tvPetroCardNoAccount.getText().length() <= 3) {
                    if (tempstring.length() == tvPetroCardNoAccount.getText().toString().length()) {

                    } else if (tempstring.length() > tvPetroCardNoAccount.getText().toString().length()) {
                        tempstring = tempstring.substring(0, tvPetroCardNoAccount.getText().toString().length());
                    } else {
                        tempstring += tvPetroCardNoAccount.getText().toString().substring(tempstring.length(), tvPetroCardNoAccount.getText().toString().length());
                    }
                    Log.d("System out", "stored string " + tempstring);

                } else if (tvPetroCardNoAccount.getText().length() > 3 && tvPetroCardNoAccount.getText().length() <= 13) {
                    if (tempstring.length() == tvPetroCardNoAccount.getText().toString().length()) {

                    } else if (tempstring.length() > tvPetroCardNoAccount.getText().toString().length()) {
                        tempstring = tempstring.substring(0, tvPetroCardNoAccount.getText().toString().length());
                    } else {
                        tempstring += tvPetroCardNoAccount.getText().toString().substring(tempstring.length(), tvPetroCardNoAccount.getText().toString().length());
                    }
                    Log.d("System out", "stored string " + tempstring);
                    if (templen != tvPetroCardNoAccount.getText().toString().length()) {
                        int len = tvPetroCardNoAccount.getText().toString().length();
                        String abc = tvPetroCardNoAccount.getText().toString().substring(0, 3);
                        Log.d("System out", "string " + abc);
                        String tot = abc;
                        Log.d("System out", "string after add " + tot);
                        for (int j = 0; j < len - 3; j++) {
                            tot += "*";
                            Log.d("System out", "string after add in for " + tot);

                        }
                        templen = len;
                        tvPetroCardNoAccount.setText(tot);
                        tvPetroCardNoAccount.setSelection(tot.length());
                    }
                } else {
                    if (tempstring.length() == tvPetroCardNoAccount.getText().toString().length()) {

                    } else if (tempstring.length() > tvPetroCardNoAccount.getText().toString().length()) {
                        tempstring = tempstring.substring(0, tvPetroCardNoAccount.getText().toString().length());
                    } else {
                        tempstring += tvPetroCardNoAccount.getText().toString().substring(tempstring.length(), tvPetroCardNoAccount.getText().toString().length());
                    }
                    Log.d("System out", "stored string " + tempstring);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        now = Calendar.getInstance();
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        String currentDateandTime = sdf.format(new Date());
        try {
            date = sdf.parse(currentDateandTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);

        dpd = DatePickerDialog.newInstance(
                (DatePickerDialog.OnDateSetListener) MyAccountFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        //  dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.setMaxDate(calendar);


        tv_D_O_B_my_account.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

            }

        });
        iv_puc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (puc == false) {
                    iv_puc.setImageResource(R.drawable.checkbox_selected_my_account);
                    puc = true;
                } else {
                    iv_puc.setImageResource(R.drawable.checkbox_unselected_my_account);
                    puc = false;

                }
            }
        });
        tvSubscribeVehicle.setVisibility(View.GONE);

        tvSubscribeVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // asdf
               /* if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";




                    SubscribePolicy();

                } else {
                    Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                }*/
            }
        });


        return v;
    }

    private void SubscribePolicy(final int vehiclepos) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<SubsciptionPlans>> call = apiService.getSubscribepolicy();
        call.enqueue(new Callback<ArrayList<SubsciptionPlans>>() {
            @Override
            public void onResponse(Call<ArrayList<SubsciptionPlans>> call, Response<ArrayList<SubsciptionPlans>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    //   vehicleDetails.clear();
                    subsciptionPlansArrayList = response.body();
                    if (subsciptionPlansArrayList.size() > 0) {
                        // Constants.show_error_popup(getActivity(), ""+vehicleDetails.get(0).getResDescription(), error_layout);

                        showPopup(subsciptionPlansArrayList, vehiclepos);

                    } else {
                        Constants.show_error_popup(getActivity(), "" + vehicleDetails.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }


                }


            }


            @Override
            public void onFailure(Call<ArrayList<SubsciptionPlans>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void VehicleAPI(final String json, final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<VehicleDetails>> call = apiService.AddVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<VehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<VehicleDetails>> call, Response<ArrayList<VehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    vehicleDetails.clear();
                    vehicleDetails = response.body();
                    if (vehicleDetails.get(0).getResStatus() == true) {
                        // Constants.show_error_popup_success(getActivity(), ""+vehicleDetails.get(0).getResDescription(), error_layout);
                        Constants.show_error_popup_success(getActivity(), "Profile updated successfully.", error_layout);
                        clearData();
                        GetVehicleList();

                    } else {
                        Constants.show_error_popup(getActivity(), "" + vehicleDetails.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }


                }


            }


            @Override
            public void onFailure(Call<ArrayList<VehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }


    public void setdata() {
        imgName = otp.getUmProfilePhoto();

        //  user_profile_my_account.setImageURI(Constants.TimbThumb_ImagePath + otp.getUmProfilePhoto()+"&width="+200);
        imageLoader.displayImage(Constants.TimbThumb_ImagePath + otp.getUmProfilePhoto() + "&width=" + 200, user_profile_my_account, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


            }
        });
        edNameAccount.setText(otp.getUmFirstName() + " " + otp.getUmLastName());
        edNumberAccount.setText(otp.getUmMobile());
        edEmailAccount.setText(otp.getUmEmailId());
        edPasswordAccount.setText(sessionManager.get_Password());
        try {
            tv_D_O_B_my_account.setText(Constants.formatDate(otp.getUmDOB(), "yyyy-MM-dd hh:mm:ss", "dd MMM, yyyy"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tempstring = otp.getUmPetroCard().toString();
        int len = otp.getUmPetroCard().length();
        Log.d("System out", "length____ " + len);

        //  String abc = tvPetroCardNoAccount.getText().toString().substring(0, 3);
        //   Log.d("System out", "string " + abc);
        String tot = "";
        // String tempstr="";
        Log.d("System out", "string after add " + tot);
        for (int j = 0; j < len; j++) {

            if (j > 3 && j <= 13) {
                tot += "*";
                Log.d("System out", "string after add in for " + tot);
            } else {
                tot += otp.getUmPetroCard().substring(tot.length(), j);
                Log.d("System out", "string in else " + tot + " j size " + j);
            }

        }
        //  templen = len;
        tvPetroCardNoAccount.setText(tot);
        //tvPetroCardNoAccount.setSelection(tot.length());
        //  tvPetroCardNoAccount.setText(otp.getUmPetroCard());
        etEmergencyNameAccount.setText(otp.getUmEmrgncyName());
        etEmergencyNoAccount.setText(otp.getUmEmrgncyNumber());
        tvVehicleNumber.setText("");
        tvVehicleMake.setText("");
        tvVehicleModel.setText("");
        tvVehicleYear.setText("");
        tvVehicleColor.setText("");


    }

    public void VehicleList() {

        if (getVehicleList.size() > 0) {
            if (getVehicleList.get(0).getResStatus() == true) {
                llVehicleList.setVisibility(View.VISIBLE);
                if (llVehicleList.getChildCount() > 0) {
                    llVehicleList.removeAllViews();
                }
                for (int i = 0; i < getVehicleList.size(); i++) {


                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View v;
                    v = inflater.inflate(R.layout.vehicle_list_account, null);
                    final TextView tv_while_line = (TextView) v.findViewById(R.id.tv_while_line);

                    final TextView tv_vehicle_make = (TextView) v.findViewById(R.id.tv_vehicle_make);
                    final TextView tv_vehicle_year_color = (TextView) v.findViewById(R.id.tv_vehicle_year_color);
                    final LinearLayout ll_vehicle_edit = (LinearLayout) v.findViewById(R.id.ll_vehicle_edit);
                    final LinearLayout ll_subscribe = (LinearLayout) v.findViewById(R.id.ll_subscribe);
                    if (getVehicleList.get(i).getSubscriptionID().equalsIgnoreCase(""))
                        ll_subscribe.setVisibility(View.VISIBLE);
                    else
                        ll_subscribe.setVisibility(View.GONE);
                    final ImageView iv_edit_icon = (ImageView) v.findViewById(R.id.iv_edit_icon);
                    if (i == getVehicleList.size() - 1) {
                        tv_while_line.setVisibility(View.GONE);
                    }
                    final int finalI = i;
                    if (vehiclepos == i) {
                        vehicle_umid = getVehicleList.get(vehiclepos).getVehicleId();
                        tvVehicleColor.setText(getVehicleList.get(vehiclepos).getVehicleColor());
                        tvVehicleYear.setText(getVehicleList.get(vehiclepos).getVehicleYear());
                        tvVehicleMake.setText(getVehicleList.get(vehiclepos).getVehicleMake());
                        tvVehicleModel.setText(getVehicleList.get(vehiclepos).getVehicleModel());
                        tvVehicleNumber.setText(getVehicleList.get(vehiclepos).getVehicleRegNumber());
                        iv_edit_icon.setImageResource(R.drawable.edit_icon_selected_my_plan);

                    }
                    ll_subscribe.setId(i);
                    ll_subscribe.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";

                                SubscribePolicy(view.getId());

                            } else {
                                Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    ll_vehicle_edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (editFlag == false) {
                                iv_edit_icon.setImageResource(R.drawable.edit_icon_selected_my_plan);
                                vehiclepos = finalI;
                                vehicle_umid = getVehicleList.get(finalI).getVehicleId();
                                tvVehicleColor.setText(getVehicleList.get(finalI).getVehicleColor());
                                tvVehicleYear.setText(getVehicleList.get(finalI).getVehicleYear());
                                tvVehicleMake.setText(getVehicleList.get(finalI).getVehicleMake());
                                tvVehicleModel.setText(getVehicleList.get(finalI).getVehicleModel());
                                tvVehicleNumber.setText(getVehicleList.get(finalI).getVehicleRegNumber());
                                if (getVehicleList.get(finalI).getVehicleHasPUC() == true) {
                                    iv_puc.setImageResource(R.drawable.checkbox_selected_my_account);
                                    puc = true;
                                } else {
                                    iv_puc.setImageResource(R.drawable.checkbox_unselected_my_account);
                                    puc = false;

                                }
                                editFlag = true;
                                VehicleList();
                            } else {
                                iv_edit_icon.setImageResource(R.drawable.edit_icon_vehicle);
                                //ll_subscribe.setVisibility(View.GONE);
                                clearData();
                                editFlag = false;
                            }
                        }
                    });
                    final LinearLayout ll_vehicle_delete = (LinearLayout) v.findViewById(R.id.ll_vehicle_delete);
                    final int finalI1 = i;
                    ll_vehicle_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


                                String json = "[{\"vehicleId\": \"" + getVehicleList.get(finalI1).getVehicleId() + "\" }]";
                                Log.d("System out", "In delete vehicle  " + json);

                                DeleteVehicle(json, error_layout);

                            } else {
                                Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                    try {
                        tv_vehicle_make.setText(getVehicleList.get(i).getVehicleMake());
                        tv_vehicle_year_color.setText(getVehicleList.get(i).getVehicleYear() + " " + getVehicleList.get(i).getVehicleColor());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    llVehicleList.addView(v);

                }
            } else {
                tvSubscribeVehicle.setVisibility(View.GONE);
                llVehicleList.removeAllViews();
                llVehicleList.setVisibility(View.GONE);
                vehicle_umid = 0;
            }
        } else {
            tvSubscribeVehicle.setVisibility(View.GONE);
            llVehicleList.removeAllViews();
            llVehicleList.setVisibility(View.GONE);
            vehicle_umid = 0;
        }


    }

    /*  private void GetVehicleListAPI(final String json,final RelativeLayout error_layout) {
          // get & set progressbar dialog
          final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
          pb_dialog.setCancelable(false);
          pb_dialog.show();

          ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
          //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
          //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
          Call<ArrayList<GetVehicleDetails>> call = apiService.GetVehicleDetails(json);
          call.enqueue(new Callback<ArrayList<GetVehicleDetails>>() {
              @Override
              public void onResponse(Call<ArrayList<GetVehicleDetails>> call, Response<ArrayList<GetVehicleDetails>> response) {
                  if (response.body() != null) {

                      pb_dialog.dismiss();
                      getVehicleList.clear();
                      getVehicleList= response.body();
                      if (getVehicleList.get(0).getResStatus() == true)
                      {
                          //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);
                          VehicleList();
                      }
                      else
                      {
                          VehicleList();
                          //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                      }



                  }


              }

              @Override
              public void onFailure(Call<ArrayList<GetVehicleDetails>> call, Throwable t) {
                  // Log error here since request failed
                  pb_dialog.dismiss();
                  t.printStackTrace();
                  if (!Constants.isInternetAvailable(getActivity())) {
                      Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                  } else {
                      Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                  }

              }
          });
      }*/
    private void GetVehicleListAPI(final String json, final RelativeLayout error_layout, final boolean flag) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<GetVehicleDetails>> call = apiService.GetVehicleDetails(json);
        call.enqueue(new Callback<ArrayList<GetVehicleDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<GetVehicleDetails>> call, Response<ArrayList<GetVehicleDetails>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    getVehicleList.clear();
                    getVehicleList = response.body();
                    if (getVehicleList.get(0).getResStatus() == true) {
                        //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);
                        boolean temp = false;
                        if (flag == true) {
                            for (int i = 0; i < getVehicleList.size(); i++) {
                                Log.d("System out", "selected Vehicle id " + VehicleIdForRSASubscription);
                                if (VehicleIdForRSASubscription == getVehicleList.get(i).getVehicleId()) {
                                    Log.d("System out", "selected Vehicle subscription " + getVehicleList.get(i).getSubscriptionID());
                                    if (!getVehicleList.get(i).getSubscriptionID().equalsIgnoreCase("")) {
                                        //  Constants.show_error_popup(getActivity(), "Payment successful. ", error_layout);
                                        temp = true;
                                        break;
                                    }
                                }
                            }

                            if (temp == true) {
                                Constants.show_error_popup(getActivity(), "Payment successful. ", error_layout);
                                VehicleIdForRSASubscription = -1;
                                VehicleList();
                            } else {

                                Constants.show_error_popup(getActivity(), "Payment request not processed. ", error_layout);
                                VehicleIdForRSASubscription = -1;
                            }
                        } else {

                            VehicleList();
                        }
                    } else {
                        VehicleList();
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<GetVehicleDetails>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void DeleteVehicle(final String json, final RelativeLayout error_layout) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<DeleteVehicle>> call = apiService.deleteVehicle(json);
        call.enqueue(new Callback<ArrayList<DeleteVehicle>>() {
            @Override
            public void onResponse(Call<ArrayList<DeleteVehicle>> call, Response<ArrayList<DeleteVehicle>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    deleteVehicleArrayList.clear();
                    deleteVehicleArrayList = response.body();
                    if (deleteVehicleArrayList.get(0).getResStatus() == true) {
                        //Constants.show_error_popup(getActivity(), ""+getVehicleList.get(0).getr(), error_layout);

                        Constants.show_error_popup(getActivity(), "" + deleteVehicleArrayList.get(0).getResDescription(), error_layout);
                        tvSubscribeVehicle.setVisibility(View.GONE);

                        clearData();
                        GetVehicleList();

                    } else {
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                        Constants.show_error_popup(getActivity(), "" + deleteVehicleArrayList.get(0).getResDescription(), error_layout);

                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<DeleteVehicle>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void GetVehicleList() {


        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


            String json = "[{\"vehicleUmId\":\"" + otp.getUmId() + "\"}]";
            Log.d("System out", "In Get Vehicle List  " + json);

            GetVehicleListAPI(json, error_layout, false);

        } else {
            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.ll_personal_header, R.id.ll_vehical_header, R.id.tv_basic_profile, R.id.tv_advance_profile, R.id.tv_change_pwd_account})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_personal_header:
                if (personalInfo == true) {
                    llPersonalContent.setVisibility(View.GONE);
                    ivArrowUpPersonal.setImageResource(R.drawable.arrow_down);
                    personalInfo = false;
                } else {
                    llPersonalContent.setVisibility(View.VISIBLE);
                    ivArrowUpPersonal.setImageResource(R.drawable.arrow_up);

                    personalInfo = true;

                }
                break;
            case R.id.ll_vehical_header:
                if (vehicleInfo == true) {
                    llVehicleContent.setVisibility(View.VISIBLE);
                    ivArrowUpVehicle.setImageResource(R.drawable.arrow_up);
                    llVehicleList.setVisibility(View.VISIBLE);
                   /* for (int i = 0; i < 4; i++) {
                        VehicleList();
                    }*/
                    vehicleInfo = false;
                } else {
                    llVehicleContent.setVisibility(View.GONE);
                    ivArrowUpVehicle.setImageResource(R.drawable.arrow_down);
                    // llVehicleList.setVisibility(View.GONE);


                    vehicleInfo = true;

                }

                break;
            case R.id.tv_basic_profile:
                llBasicProfile.setVisibility(View.VISIBLE);
                ivBasicProfile.setVisibility(View.VISIBLE);
                llAdvanceProfile.setVisibility(View.GONE);
                tvBasicProfile.setTextColor(getResources().getColor(R.color.textYellow));
                tvAdvanceProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                ivAdvanceProfile.setVisibility(View.INVISIBLE);
                llVehicleList.setVisibility(View.GONE);
                break;
            case R.id.tv_advance_profile:
                llBasicProfile.setVisibility(View.GONE);
                llAdvanceProfile.setVisibility(View.VISIBLE);
                llBasicProfile.setVisibility(View.GONE);
                ivAdvanceProfile.setVisibility(View.VISIBLE);
                tvAdvanceProfile.setTextColor(getResources().getColor(R.color.textYellow));
                tvBasicProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                ivBasicProfile.setVisibility(View.INVISIBLE);
                llBasicProfile.setVisibility(View.GONE);
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    GetVehicleList();
                    setDataInGrid();
                } else {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();

                }


                break;
            case R.id.tv_change_pwd_account:
                ((ExploreActivity) getActivity()).replace_fragmnet(new ChangePasswordFragment());
             /*   ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
                ((ExploreActivity)getActivity()).setHeader("Change Password");


                //   llmain.setVisibility(View.GONE);
                android.support.v4.app.FragmentTransaction changFragmentTransaction = getFragmentManager().beginTransaction();
                changFragmentTransaction.replace(R.id.frame,changePasswordFragment);
                changFragmentTransaction.commit();*/
                //((ExploreActivity.this)this.tv_text_header.setText("Change Password");

                break;


        }
    }


    private void setDataInGrid() {

        GridListAdapter gridListAdapter = new GridListAdapter();
        gv_category_list_account.setAdapter(gridListAdapter);
        //isselected = new boolean[Stringsss.length];
        gv_category_list_account.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        gv_category_list_account.setVerticalScrollBarEnabled(false);
        gv_category_list_account.setSelection(0);


        GridCoTravellerList gridCoTravellerList = new GridCoTravellerList();
        gv_co_traveller_list_account.setAdapter(gridCoTravellerList);

        gv_category_list_account.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        gv_category_list_account.setVerticalScrollBarEnabled(false);
        gv_category_list_account.setSelection(0);





      /*  MonthListAdapter monthListAdapter = new MonthListAdapter();
        gvBestVisittimeList.setAdapter(monthListAdapter);
        monthisselected = new boolean[months.length];

        gvBestVisittimeList.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);


        int numberOfItems = months.length;

        int totalItemsHeight = 0;
        for (int itemPos = 0; itemPos < numberOfItems / 6; itemPos++) {
            View item = monthListAdapter.getView(itemPos, null, gvBestVisittimeList);
            item.measure(0, 0);
            totalItemsHeight += item.getMeasuredHeight();
        }*/


        // Get total height of all item dividers.

        // Set list height.
       /* ViewGroup.LayoutParams params = gvBestVisittimeList.getLayoutParams();
        params.height = totalItemsHeight;
        gvBestVisittimeList.setLayoutParams(params);
        gvBestVisittimeList.requestLayout();*/


    }


    @OnClick(R.id.tv_save_my_account)
    public void onClick() {
       /* Intent i = new Intent(getContext(), ExploreActivity.class);
        getContext().startActivity(i);*/
        // ((ExploreActivity)getActivity()).BackHandle(MyAccountFragment.class);
        String selectedcotra = "";
        for (int i = 0; i < isselectedCoTraveller.length; i++) {
            if (isselectedCoTraveller[i] == true) {
                selectedcotra += StringCoTravellers[i] + ",";
            }
        }

        if (!selectedcotra.equalsIgnoreCase("")) {
            selectedcotra = selectedcotra.substring(0, selectedcotra.length() - 1);
        }
        if (llBasicProfile.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(edNameAccount.getText())) {
                Constants.show_error_popup(getActivity(), "Update all fields. ", error_layout);
            } else if (TextUtils.isEmpty(edNumberAccount.getText())) {
                Constants.show_error_popup(getActivity(), "Provide your mobile number ", error_layout);
            } else if (TextUtils.isEmpty(edEmailAccount.getText())) {
                Constants.show_error_popup(getActivity(), "Update all fields. ", error_layout);
            } else if (!edEmailAccount.getText().toString().trim().matches(Constants.emailPattern)) {
                Constants.show_error_popup(getActivity(), "Provide valid email address.", error_layout);
            } else if (TextUtils.isEmpty(edPasswordAccount.getText())) {
                Constants.show_error_popup(getActivity(), "Provide password.", error_layout);
            } else {
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";
                    Log.d("System out", "selectedImagePath__" + selectedImagePath);
                    if (!selectedImagePath.equalsIgnoreCase("")) {
                        Log.d("System out", "in if basic");
                        ImageUpload("Basic");

                    } else {
                        Log.d("System out", "in else basic");

                        try {
                            String json = "[{\"umEmrgncyName\":\"" + etEmergencyNameAccount.getText().toString() + "\",\"umEmrgncyNumber\" : \"" + etEmergencyNoAccount.getText().toString() + "\",\"umCoTraveller\" : \"" + selectedcotra + "\",\"umId\" : \"" + otp.getUmId() + "\",\"umMobile\": \"" + edNumberAccount.getText().toString().trim() + "\",\"umFirstName\":\"" + edNameAccount.getText().toString().trim() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + edEmailAccount.getText().toString() + "\",   \"umPassword\": \"" + edPasswordAccount.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"" + otp.getUmUdid() + "\",\"umProfilePhoto\": \"" + imgName + "\",   \"umDOB\": \"" + (tv_D_O_B_my_account.getText().toString().equalsIgnoreCase("") ? "" : (Constants.formatDate(tv_D_O_B_my_account.getText().toString(), "dd MMM, yyyy", "yyyy-MM-dd hh:mm:ss"))) + "\", \"umDescription\": \"" + otp.getUmDescription() + "\",\"umLocation\": \"" + otp.getUmLocation() + "\",\"umVehicleNumber\": \"\",\"umLat\":\"" + otp.getUmLat() + "\",\"umLong\":\"" + otp.getUmLong() + "\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"" + tvPetroCardNoAccount.getText().toString() + "\", \"umReferralCode\":\"" + otp.getUmReferralCode() + "\",\"umPreferredType\":\"" + selectedcat + "\"}]";
                            Log.d("System out", "In Edit Profile  " + json);

                            EditProfile(json, error_layout, "BasicProfile");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                } else {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }
            }


        } else {
           /* if (tv_D_O_B_my_account.getText().toString().equalsIgnoreCase("")) {
                Constants.show_error_popup(getActivity(), "Please select Date of Birth.", error_layout);

            }else if(tvPetroCardNoAccount.getText().toString().equalsIgnoreCase(""))
            {
                Constants.show_error_popup(getActivity(), "Please enter Petro card number.", error_layout);

            }else */
            if (!etEmergencyNameAccount.getText().toString().equalsIgnoreCase("") || !etEmergencyNoAccount.getText().toString().equalsIgnoreCase(""))
            {
                if (etEmergencyNameAccount.getText().toString().equalsIgnoreCase("")) {
                    Constants.show_error_popup(getActivity(), "Provide the name of your emergency contact.", error_layout);

                } else if (etEmergencyNoAccount.getText().toString().equalsIgnoreCase("")) {
                    Constants.show_error_popup(getActivity(), "Provide valid mobile number of your emergency contact. ", error_layout);

                } else if (etEmergencyNoAccount.getText().toString().length() < 10) {
                    Constants.show_error_popup(getActivity(), "Provide valid mobile number of your emergency contact.", error_layout);

                } else if (etEmergencyNoAccount.getText().toString().equalsIgnoreCase(otp.getUmMobile())) {
                    Constants.show_error_popup(getActivity(), "Emergency contact and your mobile numbers can't be same.", error_layout);
                }

            }else {



                    if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                        // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";

                        if (!selectedImagePath.equalsIgnoreCase("")) {
                            ImageUpload("Advance");

                        } else {
                            try {


                                String json = "[{\"umEmrgncyName\":\"" + etEmergencyNameAccount.getText().toString() + "\",\"umEmrgncyNumber\" : \"" + etEmergencyNoAccount.getText().toString() + "\",\"umCoTraveller\" : \"" + selectedcotra + "\",\"umId\" : \"" + otp.getUmId() + "\",\"umMobile\": \"" + otp.getUmMobile() + "\",\"umFirstName\":\"" + edNameAccount.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + edEmailAccount.getText().toString() + "\",   \"umPassword\": \"" + edPasswordAccount.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"" + otp.getUmUdid() + "\",\"umProfilePhoto\": \"" + imgName + "\",   \"umDOB\": \"" + (tv_D_O_B_my_account.getText().toString().equalsIgnoreCase("") ? "" : (Constants.formatDate(tv_D_O_B_my_account.getText().toString(), "dd MMM, yyyy", "yyyy-MM-dd hh:mm:ss"))) + "\",   \"umDescription\": \"" + otp.getUmDescription() + "\",\"umLocation\": \"" + otp.getUmLocation() + "\",\"umVehicleNumber\": \"\",\"umLat\":\"" + otp.getUmLat() + "\",\"umLong\":\"" + otp.getUmLong() + "\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"" + tempstring + "\", \"umReferralCode\":\"" + otp.getUmReferralCode() + "\",\"umPreferredType\":\"" + selectedcat + "\"}]";
                                Log.d("System out", "In Edit Profile  " + json);

                                EditProfile(json, error_layout, "Advance Profile");
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                    }



            }
        }


    }

    private void EditProfile(final String json, final RelativeLayout error_layout, final String isfrom) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<OTP>> call = apiService.editProfile(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    signupList = response.body();
                    if (signupList.get(0).getResStatus() == true) {
                        Fresco.getImagePipeline().evictFromCache(Uri.parse(Constants.TimbThumb_ImagePath + response.body().get(0).getUmProfilePhoto() + "&width=" + 100));
                        //   ((ExploreActivity)getActivity()).iv_profile_img.setImageURI(Uri.parse(Constants.TimbThumb_ImagePath + response.body().get(0).getUmProfilePhoto()+"&width="+100));
                        //user_profile_my_account.setImageURI(Constants.TimbThumb_ImagePath + response.body().get(0).getUmProfilePhoto()+"&width="+200);
                        imageLoader.displayImage(Constants.TimbThumb_ImagePath + response.body().get(0).getUmProfilePhoto() + "&width=" + 200, ((ExploreActivity) getActivity()).iv_profile_img, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                            }
                        });
                        imageLoader.displayImage(Constants.TimbThumb_ImagePath + response.body().get(0).getUmProfilePhoto() + "&width=" + 200, user_profile_my_account, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                            }
                        });
                        if (isfrom.equalsIgnoreCase("Advance Profile")) {
                            Calendar calendar = Calendar.getInstance();
                            int year = calendar.get(Calendar.YEAR);
                            if (!TextUtils.isEmpty(tvVehicleColor.getText()) || !TextUtils.isEmpty(tvVehicleMake.getText()) || !TextUtils.isEmpty(tvVehicleYear.getText()) || !TextUtils.isEmpty(tvVehicleNumber.getText()) || !TextUtils.isEmpty(tvVehicleModel.getText())) {

                                if (TextUtils.isEmpty(tvVehicleNumber.getText())) {
                                    Constants.show_error_popup(getActivity(), "Provide vehicle registration number (ZZ99ZZ9999).", error_layout);
                                } else if (!tvVehicleNumber.getText().toString().matches(Constants.VehicleNumber)) {
                                    Constants.show_error_popup(getActivity(), "Provide vehicle registration number (ZZ99ZZ9999).", error_layout);
                                } else if (TextUtils.isEmpty(tvVehicleMake.getText())) {
                                    Constants.show_error_popup(getActivity(), "Provide name of vehicle manufacturer.", error_layout);
                                } else if (TextUtils.isEmpty(tvVehicleModel.getText())) {
                                    Constants.show_error_popup(getActivity(), "Please provide model of your vehicle.", error_layout);
                                } else if (TextUtils.isEmpty(tvVehicleYear.getText())) {
                                    Constants.show_error_popup(getActivity(), "Please provide year of manufacture of your vehicle as YYYY.", error_layout);
                                } else if (tvVehicleYear.getText().toString().length() < 4) {
                                    Constants.show_error_popup(getActivity(), "Provide year of manufacturing of your vehicle (YYYY).", error_layout);
                                } else if (Integer.parseInt(tvVehicleYear.getText().toString()) > year) {
                                    Constants.show_error_popup(getActivity(), "Provide year of manufacturing of your vehicle (YYYY).", error_layout);
                                } else if (TextUtils.isEmpty(tvVehicleColor.getText())) {
                                    Constants.show_error_popup(getActivity(), "Provide colour of your vehicle. ", error_layout);
                                } else {
                                    if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                                        // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


                                        String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\" ,\"vehicleColor\": \"" + tvVehicleColor.getText().toString() + "\",\"vehicleHasPUC\": \"" + puc + "\",\"vehicleHasRSA\": \"false\",\"vehicleMake\": \"" + tvVehicleMake.getText().toString() + "\",\"vehicleYear\": \"" + tvVehicleYear.getText().toString() + "\",\"vehicleRegNumber\": \"" + tvVehicleNumber.getText().toString() + "\" ,  \"vehicleId\":\"" + vehicle_umid + "\", \"vehicleModel\": \"" + tvVehicleModel.getText().toString() + "\" }]";
                                        Log.d("System out", "In Insert and update Vehicle  " + json);

                                        VehicleAPI(json, error_layout);

                                    } else {
                                        Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                                    }


                                }
                            } else {
                                Constants.show_error_popup_success(getActivity(), "" + signupList.get(0).getResDescription(), error_layout);
                                //Constants.show_error_popup_success(getActivity(), "Profile is updated successfully.", error_layout);

                                SessionManager sessionManager = new SessionManager(getActivity());

                                String json = Constants.convert_object_string(signupList.get(0));
                                sessionManager.create_login_session(json, signupList.get(0).getUmEmailId(), signupList.get(0).getUmPassword());
                            }
                        } else {
                            Constants.show_error_popup_success(getActivity(), "" + signupList.get(0).getResDescription(), error_layout);

                            SessionManager sessionManager = new SessionManager(getActivity());

                            String json = Constants.convert_object_string(signupList.get(0));
                            sessionManager.create_login_session(json, signupList.get(0).getUmEmailId(), signupList.get(0).getUmPassword());
                        }


                    } else {
                        Constants.show_error_popup(getActivity(), "" + signupList.get(0).getResDescription(), error_layout);
                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + " " + (++monthOfYear) + "," + year;


        //  dateTextView.setText(date);

       /* Calendar now1 = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                (TimePickerDialog.OnTimeSetListener) MyAccountFragment.this,
                now1.get(Calendar.HOUR_OF_DAY),
                now1.get(Calendar.MINUTE),true
        );*/
        // formatter.format(date);
        // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");

        try {
            date1 = Constants.formatDate(date, "dd MM,yyyy", "dd MMM, yyyy");
            tv_D_O_B_my_account.setText(date1);


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    private class GridListAdapter extends BaseAdapter {


        GridListAdapter() {
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return Stringsss.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.category_cell, null);

            final MyTextView tv_category_name = (MyTextView) v.findViewById(R.id.tv_category_name);

            tv_category_name.setText(Stringsss.get(position));


            if (selectedcat.contains(Stringsss.get(position))) {

                tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                        R.drawable.category_box_selected));
                tv_category_name.setTextColor(getResources().getColor(
                        R.color.textYellow));
            } else {
                tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                        R.drawable.category_box_unselected));
                tv_category_name.setTextColor(getResources().getColor(
                        R.color.colorWhite));
            }

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (!selectedcat.contains(Stringsss.get(position))) {

                        selectedcat += Stringsss.get(position) + ",";
                        tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                                R.drawable.category_box_selected));
                        tv_category_name.setTextColor(getResources().getColor(
                                R.color.textYellow));
                    } else {
                        selectedcat = selectedcat.replace(Stringsss.get(position) + ",", "");
                        tv_category_name.setBackgroundDrawable(getResources().getDrawable(
                                R.drawable.category_box_unselected));
                        tv_category_name.setTextColor(getResources().getColor(
                                R.color.colorWhite));
                    }


                  /*  tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                            R.drawable.category_box_selected) : getResources().getDrawable(
                            R.drawable.category_box_unselected));
                    tv_category_name.setTextColor(checked ? getResources().getColor(
                            R.color.textYellow) : getResources().getColor(
                            R.color.colorWhite));*/


                }
            });

            return v;
        }
    }

    private class GridCoTravellerList extends BaseAdapter {


        GridCoTravellerList() {
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return StringCoTravellers.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.category_cell, null);

            final MyTextView tv_category_name = (MyTextView) v.findViewById(R.id.tv_category_name);

            tv_category_name.setText(StringCoTravellers[position]);

            boolean checked = isselectedCoTraveller[position];

            tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                    R.drawable.category_box_selected) : getResources().getDrawable(
                    R.drawable.category_box_unselected));
            tv_category_name.setTextColor(checked ? getResources().getColor(
                    R.color.textYellow) : getResources().getColor(
                    R.color.colorWhite));

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Log.d("System out", "category checked is::" + isselectedCoTraveller[position]);

                    if (isselectedCoTraveller[position] == true) {
                        isselectedCoTraveller[position] = false;
                    } else {
                        isselectedCoTraveller[position] = true;
                    }
                    boolean checked = isselectedCoTraveller[position];
                    tv_category_name.setBackgroundDrawable(checked ? getResources().getDrawable(
                            R.drawable.category_box_selected) : getResources().getDrawable(
                            R.drawable.category_box_unselected));
                    tv_category_name.setTextColor(checked ? getResources().getColor(
                            R.color.textYellow) : getResources().getColor(
                            R.color.colorWhite));


                }
            });

            return v;
        }
    }

    private void showPopup(ArrayList<SubsciptionPlans> subsciptionPlansArrayList, int vehiclepos) {

        subdialog = new Dialog(getActivity());
        subdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        subdialog.setContentView(R.layout.subscriptiondialog);
        subdialog.setCanceledOnTouchOutside(false);

        // dialog.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);


        final ListView subcriptionlist = (ListView) subdialog.findViewById(R.id.subcriptionlist);
        final ImageView ImgClosepopup = (ImageView) subdialog.findViewById(R.id.ImgClosepopup);


        SubscriptionAdapter adapter = new SubscriptionAdapter(getActivity(), subsciptionPlansArrayList, vehiclepos);
        subcriptionlist.setAdapter(adapter);


        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subdialog.dismiss();
            }
        });


        subdialog.show();

    }

    public class SubscriptionAdapter extends BaseAdapter {
        Activity activity;
        ArrayList<SubsciptionPlans> subsciptionPlansArrayList;
        LayoutInflater inflater;
        int pos;

        public SubscriptionAdapter(Activity ac, ArrayList<SubsciptionPlans> subsciptionPlansArrayList1, int pos1) {
            this.subsciptionPlansArrayList = subsciptionPlansArrayList1;
            this.activity = ac;
            this.pos = pos1;
        }


        @Override
        public int getCount() {
            return subsciptionPlansArrayList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View v;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.subscriptionplan_adapter, null);
            TextView tv_sub_name = (TextView) v.findViewById(R.id.tv_sub_name);
            TextView tv_sub_des = (TextView) v.findViewById(R.id.tv_sub_des);
            TextView buy_plan = (TextView) v.findViewById(R.id.buy_plan);

            tv_sub_name.setText(subsciptionPlansArrayList.get(i).getName());
            tv_sub_des.setText(subsciptionPlansArrayList.get(i).getDescription());

            buy_plan.setId(i);
            buy_plan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (otp.getUmRSAId() == null) {
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json = "[{\"umMobile\": \"" + otp.getUmMobile() + "\",\"umFirstName\":\"" + otp.getUmFirstName() + "\",\"umEmailId\": \"" + otp.getUmEmailId() + "\",\"UmId\": \"" + otp.getUmId() + "\",\"umDeviceType\": \"Android\"}]";


                            Log.d("System out", "json for RSA customer Registration " + json);
                            RSARegistration(json, subsciptionPlansArrayList.get(view.getId()).getId(), subsciptionPlansArrayList.get(view.getId()).getCode(), pos);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    } else if (getVehicleList.get(pos).getVehicleRSAId() != null) {

                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            // String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";


                            String json = Constants.BASE_URL + "api/RSA_Ver1/PolicyPurchase?id=" + getVehicleList.get(pos).getVehicleRSAId() + "&code=" + subsciptionPlansArrayList.get(view.getId()).getCode();
                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            VehicleIdForRSASubscription = getVehicleList.get(pos).getVehicleId();
                            GetRSAwebUrl(json);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }

                        //  showPopupForWebview(subsciptionPlansArrayList.get(view.getId()).getId(),subsciptionPlansArrayList.get(view.getId()).getCode());
                        //VehicleRegister(json,subsciptionPlansArrayList.get(view.getId()).getId(),subsciptionPlansArrayList.get(view.getId()).getCode());
                    } else {

                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + getVehicleList.get(pos).getVehicleId() + "\"}]";


                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            VehicleRegister(json, subsciptionPlansArrayList.get(view.getId()).getId(), subsciptionPlansArrayList.get(view.getId()).getCode(), pos);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            return v;
        }
    }

    private void VehicleRegister(String json, final String id, final String code, final int pos) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<OTP>> call = apiService.RSACreateVehicle(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    vehicleDetails.clear();
                    if (response.body().get(0).getResStatus() == true) {
                        subdialog.dismiss();

                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            // String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";


                            String json = Constants.BASE_URL + "api/RSA_Ver1/PolicyPurchase?id=" + getVehicleList.get(pos).getVehicleRSAId() + "&code=" + code;
                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            VehicleIdForRSASubscription = getVehicleList.get(pos).getVehicleId();
                            GetRSAwebUrl(json);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.body().get(0).getResDescription().contains("VEHICLEREG_EXISTS")) {
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            // String json = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + vehicle_umid+ "\"}]";


                            String json = Constants.BASE_URL + "api/RSA_Ver1/PolicyPurchase?id=" + getVehicleList.get(pos).getVehicleRSAId() + "&code=" + code;
                            Log.d("System out", "json for RSA Create Vehicle " + json);
                            VehicleIdForRSASubscription = getVehicleList.get(pos).getVehicleId();
                            GetRSAwebUrl(json);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }
                }


            }


            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void GetRSAwebUrl(String json) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<String> call = apiService.RSAGetLink(json);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    Log.d("System out", "Response of link " + response.body());
                    if (response.body().length() > 0) {
                        showPopupForWebview(response.body().replace("\"", ""));
                    } else {

                    }
                }


            }


            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void RSARegistration(String json, final String id, final String code, final int pos) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<OTP>> call = apiService.RSACustomerRegistration(json);
        call.enqueue(new Callback<ArrayList<OTP>>() {
            @Override
            public void onResponse(Call<ArrayList<OTP>> call, Response<ArrayList<OTP>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();

                    if (response.body().get(0).getResStatus() == true) {

                        otp.setUmRSAId(response.body().get(0).getUmRSAId());
                        String json = Constants.convert_object_string(otp);
                        sessionManager.create_login_session(json, otp.getUmEmailId(), otp.getUmPassword());
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json1 = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + getVehicleList.get(pos).getVehicleId() + "\"}]";


                            Log.d("System out", "json for RSA Create Vehicle " + json1);
                            VehicleRegister(json1, id, code, pos);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.body().get(0).getResDescription().contains("PHONE_EXISTS")) {
                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {

                            String json1 = "[{\"vehicleUmId\": \"" + otp.getUmId() + "\",\"vehicleId\":\"" + getVehicleList.get(pos).getVehicleId() + "\"}]";


                            Log.d("System out", "json for RSA Create Vehicle " + json1);
                            VehicleRegister(json1, id, code, pos);

                        } else {
                            Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }


            }


            @Override
            public void onFailure(Call<ArrayList<OTP>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });
    }

    private void showPopupForWebview(String URL) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.subscriptiondialog);
        dialog.setCanceledOnTouchOutside(false);

        // dialog.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);


        final ListView subcriptionlist = (ListView) dialog.findViewById(R.id.subcriptionlist);
        LinearLayout sub_header_ll = (LinearLayout) dialog.findViewById(R.id.sub_header_ll);
        final ImageView ImgClosepopup = (ImageView) dialog.findViewById(R.id.ImgClosepopup);
        WebView webview = (WebView) dialog.findViewById(R.id.subcription_webview);
        sub_header_ll.setVisibility(View.GONE);
        subcriptionlist.setVisibility(View.GONE);
        webview.setVisibility(View.VISIBLE);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(URL);

        webview.clearView();
        webview.measure(100, 100);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);


        WebClientClass webViewClient = new WebClientClass();
        webview.setWebViewClient(webViewClient);
       /* webview.getSettings().setDisplayZoomControls(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        WebSettings settings1 = webview.getSettings();

        settings1.setDefaultTextEncodingName("utf-8");
       // webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        Log.d("System out","Response of link "+URL);
        String url ="<iframe src=\""+URL+"\"></iframe>";
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int width=display.getWidth();

      *//*  String data="<html><head><title>Example</title><meta name=\"viewport\"\"content=\"width="+width+", initial-scale=0.65 \" /></head>";
        data=data+"<body><center><img width=\""+width+"\" src=\""+URL+"\" /></center></body></html>";
        webview.loadData(data, "text/html", null);*//*
       // String URL=Constants.BASE_URL+"api/RSA_Ver1/PolicyPurchase?id="+id+"&code="+code;
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            //webview.loadUrl("https://www.instamojo.com/roadZEN/strandd-subscription-a/?data_readonly=data_amount&embed=form&data_amount=200&data_Field_26118=BPCL_BASIC&data_Field_79418=db59fc03-3489-49e2-90c8-8cbdfb7bf9a0&data_hidden=data_Field_79418");
            //webview.loadUrl(url);
        webview.loadDataWithBaseURL(null,url, "text/html", "UTF-8", null);
        //webView.loadDataWithBaseURL(null, "<iframe src=\"http://files.flipsnack.com/iframe/embed.html?hash=fdk843nc&wmode=window&bg‌​color=EEEEEE&t=1381431045\" width=\"640\" height=\"385\" seamless=\"seamless\" scrolling=\"no\" frameborder=\"0\" allowtransparency=\"true\"></iframe>", "text/html", "UTF-8", null);

        } else {
        }*/


        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


                    String json = "[{\"vehicleUmId\":\"" + otp.getUmId() + "\"}]";
                    Log.d("System out", "In Get Vehicle List  " + json);

                    GetVehicleListAPI(json, error_layout, true);

                } else {
                    Toast.makeText(getActivity(), "" + getActivity().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }


            }
        });


        dialog.show();

    }

    public class WebClientClass extends WebViewClient {
        // ProgressDialog pd = null;

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("System out", "Payment Method_first in loading " + url);
            view.loadUrl(url);
            return true;
            //return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);


            if (!url.equals("")) {

                Log.e("System out", "Patment Method_first " + url); // success=2
                if (url.endsWith("sucess") || url.endsWith("Sucess")) {


                } else if (url.endsWith("Fail") || url.endsWith("fail")) {


                    Toast.makeText(getActivity(), "Payment could not processed. ", Toast.LENGTH_SHORT);


                }


            } else {

            }


        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler,
                                       SslError error) {

            //super.onReceivedSslError(view, handler, error);
            //handler.proceed();
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("SSL certification invalid");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();


        }

    }

    public void dialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_gallery);

        ImageView gal = (ImageView) dialog.findViewById(R.id.gal);
        ImageView camera = (ImageView) dialog.findViewById(R.id.camera);
        ImageView closebtn = (ImageView) dialog.findViewById(R.id.closebtn);

        // gal.setTypeface(null, Typeface.BOLD);
        // camera.setTypeface(null, Typeface.BOLD);

        closebtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // SignUpActivity.this.finish();
            }
        });

        gal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");

                //intent.setAction(Intent.ACTION_GET_CONTENT);
                //Toast.makeText(AddPost.this, "Select gallery option", 3000).show();
                Log.d("System out", "Select gallery option");
                startActivityForResult(
                        Intent.createChooser(intent, "Select Picture"),
                        SELECT_PICTURE);

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //Toast.makeText(AddPost.this, "Select camera option", 3000).show();
                Log.d("System out", "Select camera option");
                timeForImgname = System.currentTimeMillis();
                imgName = "img_" + timeForImgname
                        + ".jpg";

                imgStore = imgName;
                //Toast.makeText(AddPost.this, "Image name which capture from camera"+imgName, 3000).show();
                Log.d("System out", "Image name which capture from camera" + imgName);
                f = new File(Environment.getExternalStorageDirectory(),
                        imgName);

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(cameraIntent, TAKE_PICTURE);
            }
        });

        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {
            Log.d("System out", "result code" + resultCode + "    "
                    + requestCode);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
         /*   height = displaymetrics.heightPixels;
            width = displaymetrics.widthPixels;*/
            if (requestCode == TAKE_PICTURE) {
                if (requestCode == TAKE_PICTURE) {


                    Log.d("System out", "result code in data not null"
                            + resultCode + "    " + requestCode);

     /*Uri pictureUri = Uri.fromFile(f);
     selectedImagePath = getPath(pictureUri);
     Log.d("System out", "Path of image which capture from camera"+selectedImagePath);*/

                    try {
                        //  Uri pictureUri = data.getData();
                        Uri pictureUri = Uri.fromFile(f);
                        selectedImagePath = getPath(pictureUri);
                        Log.d("System out", "selected image path   " + selectedImagePath);
                        ContentResolver cr = getActivity().getContentResolver();


                        user_profile_my_account.setImageURI(pictureUri);


                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Memory insufficient. ", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Log.e("Camera", e.toString());
                    }


                    try {

                    } catch (Exception e) {
                        Log.w("Exception in img", e);
                    }

                }
            } else if (requestCode == SELECT_PICTURE) {
                try {
                    Uri selectedImage = data.getData();

                    selectedImagePath = getPath(selectedImage);
                    user_profile_my_account.setImageURI(selectedImage);
                  /*  LoginModule m = new LoginModule(ExploreActivity.this,"ExploreActivity");
                    m.setprofilepic(selectedImage,selectedImagePath);*/
              /*    bm = MediaStore.Images.Media.getBitmap(cr,
                          selectedImage);
                  img_profile.setImageBitmap(bm);*/


                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Memory insufficient.", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    // TODO: handle exception
                }

            }


        }


    }

    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    public void ImageUpload(final String from) {
        timeForImgname = System.currentTimeMillis();
        imgName = "img_" + timeForImgname
                + ".jpg";
        File file = new File(selectedImagePath);
        okhttp3.RequestBody reqFile = okhttp3.RequestBody.create(okhttp3.MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("upload", imgName, reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<ImageUpload>> req = apiService.postImage(body);
        req.enqueue(new Callback<ArrayList<ImageUpload>>() {
            @Override
            public void onResponse(Call<ArrayList<ImageUpload>> call, Response<ArrayList<ImageUpload>> response) {
                // Do Something
                Log.d("System out", "Response of image uplod " + response.body().toString());


                if (response.body().get(0).getStatus() == true) {
                    Log.d("System out", "Response of image uplod " + response.body().get(0).getMessage());
                    imgName = response.body().get(0).getMessage();
                    if (from.equalsIgnoreCase("Basic")) {

                        try {
                            String json = "[{\"umEmrgncyName\":\"" + etEmergencyNameAccount.getText().toString() + "\",\"umEmrgncyNumber\" : \"" + etEmergencyNoAccount.getText().toString() + "\",\"umCoTraveller\" : \"" + selectedcat + "\",\"umId\" : \"" + otp.getUmId() + "\",\"umMobile\": \"" + edNumberAccount.getText().toString().trim() + "\",\"umFirstName\":\"" + edNameAccount.getText().toString().trim() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + edEmailAccount.getText().toString() + "\",   \"umPassword\": \"" + edPasswordAccount.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"" + otp.getUmUdid() + "\",\"umProfilePhoto\": \"" + imgName + "\",   \"umDOB\": \"" + (tv_D_O_B_my_account.getText().toString().equalsIgnoreCase("") ? "" : (Constants.formatDate(tv_D_O_B_my_account.getText().toString(), "dd MMM, yyyy", "yyyy-MM-dd hh:mm:ss"))) + "\", \"umDescription\": \"" + otp.getUmDescription() + "\",\"umLocation\": \"" + otp.getUmLocation() + "\",\"umVehicleNumber\": \"\",\"umLat\":\"" + otp.getUmLat() + "\",\"umLong\":\"" + otp.getUmLong() + "\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"" + tvPetroCardNoAccount.getText().toString() + "\", \"umReferralCode\":\"" + otp.getUmReferralCode() + "\",\"umPreferredType\":\"" + selectedcat + "\"}]";
                            Log.d("System out", "In Edit Profile from Basic " + json);

                            EditProfile(json, error_layout, "BasicProfile");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            String json = "[{\"umEmrgncyName\":\"" + etEmergencyNameAccount.getText().toString() + "\",\"umEmrgncyNumber\" : \"" + etEmergencyNoAccount.getText().toString() + "\",\"umCoTraveller\" : \"" + selectedcat + "\",\"umId\" : \"" + otp.getUmId() + "\",\"umMobile\": \"" + otp.getUmMobile() + "\",\"umFirstName\":\"" + edNameAccount.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + edEmailAccount.getText().toString() + "\",   \"umPassword\": \"" + edPasswordAccount.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"" + otp.getUmUdid() + "\",\"umProfilePhoto\": \"" + imgName + "\",   \"umDOB\": \"" + (tv_D_O_B_my_account.getText().toString().equalsIgnoreCase("") ? "" : (Constants.formatDate(tv_D_O_B_my_account.getText().toString(), "dd MMM, yyyy", "yyyy-MM-dd hh:mm:ss"))) + "\",   \"umDescription\": \"" + otp.getUmDescription() + "\",\"umLocation\": \"" + otp.getUmLocation() + "\",\"umVehicleNumber\": \"\",\"umLat\":\"" + otp.getUmLat() + "\",\"umLong\":\"" + otp.getUmLong() + "\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"" + tempstring + "\", \"umReferralCode\":\"" + otp.getUmReferralCode() + "\",\"umPreferredType\":\"" + selectedcat + "\"}]";
                            Log.d("System out", "In Edit Profile  " + json);

                            EditProfile(json, error_layout, "Advance Profile");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<ImageUpload>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity) getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
        sessionManager = new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();
        edNameAccount.setText(otp.getUmFirstName() + " " + otp.getUmLastName());
        edNumberAccount.setText(otp.getUmMobile());
        edEmailAccount.setText(otp.getUmEmailId());
        edPasswordAccount.setText(sessionManager.get_Password());

    }

    public void clearData() {
        tvVehicleColor.setText("");
        tvVehicleYear.setText("");
        tvVehicleMake.setText("");
        tvVehicleModel.setText("");
        tvVehicleNumber.setText("");
        iv_puc.setImageResource(R.drawable.checkbox_unselected_my_account);
        vehicle_umid = 0;
        vehiclepos = -1;
    }
}
