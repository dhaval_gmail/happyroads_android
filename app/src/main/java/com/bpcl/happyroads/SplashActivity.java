package com.bpcl.happyroads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class SplashActivity extends Activity {

    RelativeLayout main_rl_seekbar;
    int w;
    private SharedPreferences preferences;
    Handler mHandler;
    Runnable mRunnable;
    SharedPreferences preferences2,preferences3;
    SharedPreferences.Editor editor2,editor3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
      /*  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
     //   Fresco.initialize(this);





        setContentView(R.layout.activity_splash);

        preferences2=getSharedPreferences("tutorial",0);
        editor2=preferences2.edit();
        ImageView simpleDraweeView = (ImageView) findViewById(R.id.imf_gif);
      /*  ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.raw.splash).build();
        InputStream is = getResources().openRawResource(R.raw.splash);
        Movie movie = Movie.decodeStream(is);
        int duration = movie.duration();
        Log.d("duration","time "+duration);
        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(
                    String id,
                    @Nullable ImageInfo imageInfo,
                    @Nullable Animatable anim) {
                if (anim != null) {
                    // app-specific logic to enable animation starting
                    anim.start();
                }
            }
        };
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(imageRequest.getSourceUri())
                .setAutoPlayAnimations(false)
                .setControllerListener(controllerListener)
                .build();
        simpleDraweeView.setController(controller);

        SharedPreferences wmbPreference = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun)
        {
            // Code to run once
            new ImageSaver(SplashActivity.this).setExternal(true).deleteFolder();
            SharedPreferences.Editor editor = wmbPreference.edit();
            editor.putBoolean("FIRSTRUN", false);
            editor.commit();
        }

        preferences = getSharedPreferences(Constants.TripTrackPREFERENCES, 0);*/
        mHandler=new Handler();
        mRunnable=new Runnable() {
            @Override
            public void run() {

if(preferences2.getString("tutorial","false").equalsIgnoreCase("true")) {


    Intent intent = new Intent(SplashActivity.this, ExploreActivity.class);
    SplashActivity.this.startActivity(intent);
    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    SplashActivity.this.finish();
}else
{
    Intent intent = new Intent(SplashActivity.this, TutorialScreen.class);
    SplashActivity.this.startActivity(intent);
    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    SplashActivity.this.finish();
}
            }
        };
        mHandler.postDelayed(mRunnable,2000);



       /* Intent intent = new Intent(SplashActivity.this,
                ExploreActivity.class);
        SplashActivity.this.startActivity(intent);
        SplashActivity.this.finish();
*/
        Display mDisplay = SplashActivity.this.getWindowManager().getDefaultDisplay();
        final int width  = mDisplay.getWidth();
        final int height = mDisplay.getHeight();

        // main_rl_seekbar.getLayoutParams().width = w;

        Log.e("System out","width get is::::"+width);
   /*     if (width==720 ) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(6400);
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (preferences.getInt("tripid",0)==0) {
                                    Intent intent = new Intent(SplashActivity.this,ExploreActivity.class);
                                    SplashActivity.this.startActivity(intent);
                                    overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
                                    SplashActivity.this.finish();
                                }else
                                {
                                    Intent intent=new Intent(SplashActivity.this,ExploreActivity.class);
                                    intent.putExtra("tripid",preferences.getInt("tripid",0));
                                    startActivity(intent);
                                }

                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();

        }

       else if (width==1080 ) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5600);//5500
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (preferences.getInt("tripid",0)==0) {
                                    Intent intent = new Intent(SplashActivity.this,ExploreActivity.class);
                                    SplashActivity.this.startActivity(intent);
                                    SplashActivity.this.finish();
                                }else
                                {
                                    Intent intent=new Intent(SplashActivity.this,ExploreActivity.class);
                                    intent.putExtra("tripid",preferences.getInt("tripid",0));
                                    startActivity(intent);
                                }


                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();

        }
        else
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(6300);
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (preferences.getInt("tripid",0)==0) {
                                    Intent intent = new Intent(SplashActivity.this,ExploreActivity.class);
                                    SplashActivity.this.startActivity(intent);
                                    SplashActivity.this.finish();
                                }else
                                {
                                    Intent intent=new Intent(SplashActivity.this,ExploreActivity.class);
                                    intent.putExtra("tripid",preferences.getInt("tripid",0));
                                    startActivity(intent);
                                }


                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();

        }*/


    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if(new DeviceUtils().isDeviceRooted(getApplicationContext())){
            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
        }
    }


    public class DeviceUtils {

        public Boolean isDeviceRooted(Context context){
            boolean isRooted = isrooted1() || isrooted2() || checkRootMethod3();
            return isRooted;
        }

        private boolean isrooted1() {
            String buildTags = android.os.Build.TAGS;         return buildTags != null && buildTags.contains("test- keys");


        }

        // try executing commands
        private boolean isrooted2() {
            String[] paths = {
                    "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",                 "/system/bin/failsafe/su", "/data/local/su" };
            for (String path : paths) {
                if (new File(path).exists()) return true;         }
            return false;
        }
    }
    private static boolean checkRootMethod3() {         Process process = null;         try {             process = Runtime.getRuntime().exec(new String[] { "/system/xbin/which", "su" });             BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));             if (in.readLine() != null) return true;             return false;         } catch (Throwable t) {             return false;         } finally {             if (process != null) process.destroy();         }     }
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }
    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }
}
