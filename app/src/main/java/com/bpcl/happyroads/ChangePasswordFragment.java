package com.bpcl.happyroads;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bpcl.happyroads.Pojo.OTP;
import com.bpcl.happyroads.Pojo.Signup;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/17/2016.
 */
public class ChangePasswordFragment extends Fragment {
    @Bind(R.id.tv_current_password)
    EditText tvCurrentPassword;
    @Bind(R.id.tv_new_password)
    EditText tvNewPassword;
    @Bind(R.id.tv_retype_password)
    EditText tvRetypePassword;
    @Bind(R.id.tv_save_password)
    TextView tvSavePassword;
    OTP otp;
    ArrayList<Signup> signupList;
    @Bind(R.id.error_layout)
    RelativeLayout errorLayout;
    SessionManager sessionManager;
    TextView tv_retype_pwd_text,tv_new_pwd_text,tv_current_pwd_text;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.change_password_fragment, container, false);
        tv_current_pwd_text=(TextView)v.findViewById(R.id.tv_current_pwd_text);
        tv_new_pwd_text=(TextView)v.findViewById(R.id.tv_new_pwd_text);
        tv_retype_pwd_text=(TextView)v.findViewById(R.id.tv_retype_pwd_text);
        ((ExploreActivity) getActivity()).tv_text_header.setText("Change Password");
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);

        tv_current_pwd_text.setText(Html.fromHtml(getResources().getString(R.string.crnt_pwd)));
        tv_new_pwd_text.setText(Html.fromHtml(getResources().getString(R.string.new_pwd)));
        tv_retype_pwd_text.setText(Html.fromHtml(getResources().getString(R.string.retype_pwd)));


        ButterKnife.bind(this, v);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity) getActivity()).onBackPressed();
            }
        });

        tvNewPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                Constants.SpacialCharacterNotAllow(tvNewPassword);
            }
        });
        sessionManager = new SessionManager(getActivity());
        otp = sessionManager.get_Authenticate_User();


        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);

        ButterKnife.unbind(this);
    }


    @OnClick(R.id.tv_save_password)
    public void onClick() {
       /* Intent i=new Intent(getContext(),MyAccountFragment.class);
        getContext().startActivity(i);*/
        // ((ExploreActivity)getActivity()).onBackPressed();
        if(tvCurrentPassword.getText().toString().equalsIgnoreCase(""))
        {
            Constants.show_error_popup(getActivity(), "Incorrect password.", errorLayout);
        }
        else if(!tvCurrentPassword.getText().toString().equalsIgnoreCase(sessionManager.get_Password()))
        {
            Constants.show_error_popup(getActivity(), "Incorrect password.", errorLayout);
        }
        else if(tvNewPassword.getText().toString().equalsIgnoreCase(""))
        {
            Constants.show_error_popup(getActivity(), "Password cannot be blank.", errorLayout);
        }
        else if(tvRetypePassword.getText().toString().equalsIgnoreCase(""))
        {
            Constants.show_error_popup(getActivity(), "Passwords do not match.", errorLayout);
        }
        else if(!tvNewPassword.getText().toString().equalsIgnoreCase(tvRetypePassword.getText().toString()))
        {
            Constants.show_error_popup(getActivity(), "Passwords do not match.", errorLayout);
        }
        else {
            forgot();
        }

    }

    private void forgot() {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{\"umId\":\"" + otp.getUmId() + "\",\"umPassword\":\"" + tvNewPassword.getText().toString() + "\",\"umDeviceType\":\"Android\",\"umUDID\":\"max200Min0\",\"umLat\":\"23.2323\",\"umLong\":\"72.01234\",\"umOTP\":\"Success\"}]";
        Call<ArrayList<Signup>> call = apiService.updatePassword(json);
        call.enqueue(new Callback<ArrayList<Signup>>() {
            @Override
            public void onResponse(Call<ArrayList<Signup>> call, Response<ArrayList<Signup>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    signupList = response.body();
                    if (signupList.get(0).getResStatus() == true) {//call OTP API here

                        sessionManager.set_password(tvNewPassword.getText().toString());
                        Constants.show_error_popup_success(getActivity(), "" + signupList.get(0).getResDescription(), errorLayout);
                        ((ExploreActivity) getActivity()).onBackPressed();




                    } else {
                        Constants.show_error_popup(getActivity(), "" + signupList.get(0).getResDescription(), errorLayout);

                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<Signup>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();


            }
        });
    }
}
