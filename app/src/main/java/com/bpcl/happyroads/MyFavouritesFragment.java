package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.Gson;
import com.bpcl.happyroads.Pojo.AddFavourite;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.GetFavourites;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DownloadCode;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.LoginModule;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ADMIN on 8/12/2016.
 */
public class MyFavouritesFragment extends Fragment {
   RecyclerView lvFavourites;
    ImageView iv_like_icon,iv_share_img;
    SimpleDraweeView iv_img_favourites;
    RelativeLayout error_layout;
    ArrayList<GetFavourites> getFavouritelist=new ArrayList<GetFavourites>();
    SessionManager sessionManager;
    TextView tv_img_name,tv_time_km_fav_list,tv_from_to_month,tv_rating_dest;
    ArrayList<AddFavourite> addFavList=new ArrayList<AddFavourite>();
    boolean like=false;
    Gson gson=new Gson();
    Integer tripid=0;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    ArrayList<ExploreDestination> exploreDestList = new ArrayList<>();
    RelativeLayout errorLayout;
    ArrayList<ExplorePOI> explorePoiList=new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notification_fragment, container, false);

        ButterKnife.bind(this, v);

        // lvFavourites.setAdapter(favouritesAdapter);
        lvFavourites = (RecyclerView) v.findViewById(R.id.lv_notification);
        lvFavourites.setHasFixedSize(true);
        error_layout =(RelativeLayout) v.findViewById(R.id.error_layout);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        lvFavourites.setLayoutManager(layoutManager);
        sessionManager=new SessionManager(getActivity());
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    ((ExploreActivity)getActivity()).onBackPressed();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        Constants.destSelectionfrom="my favorites";

        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";
            if(sessionManager.isLoggedIn())
            {

                String json = "[{     \"FavUmId\": \""+sessionManager.get_Authenticate_User().getUmId()+"\"}]";
                Log.d("System out", "In Get Favourites List  " + json);

                GetFavouritesAPI(json);
            }else
            {
                LoginModule loginModule = new LoginModule(getActivity(),"favourite Destination");
                loginModule.signIn();
            }



        }
        else
        {
            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
        }




        return v;
    }

    private void GetFavouritesAPI(String json) {
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<ArrayList<GetFavourites>> call = apiService.getFavourites(json);
        call.enqueue(new Callback<ArrayList<GetFavourites>>() {
            @Override
            public void onResponse(Call<ArrayList<GetFavourites>> call, Response<ArrayList<GetFavourites>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();
                    getFavouritelist.clear();

                    ArrayList<GetFavourites> getFavouritelist1=new ArrayList<GetFavourites>();
                    getFavouritelist1.addAll(response.body());
                    if (getFavouritelist1.get(0).getResStatus() == true)
                    {
                        for(int i=0;i<getFavouritelist1.size();i++)
                        {
                            if(getFavouritelist1.get(i).getFavDestination().size()>0 )
                            {
                            if (getFavouritelist1.get(i).getFavDestination().get(0).getIsActive() == true)
                            {
                                getFavouritelist.add(getFavouritelist1.get(i));
                            }
                        }
                            if(getFavouritelist1.get(i).getFavPOILists().size()>0 )
                            {
                               if (getFavouritelist1.get(i).getFavPOILists().get(0).getResStatus() == true)
                                {
                                    getFavouritelist.add(getFavouritelist1.get(i));
                                }
                            }
                        }
                        MyFavouritesAdapter favouritesAdapter = new MyFavouritesAdapter();
                        lvFavourites.setAdapter(favouritesAdapter);
                    }
                    else
                    {
                        getFavouritelist.clear();
                        MyFavouritesAdapter favouritesAdapter = new MyFavouritesAdapter();
                        lvFavourites.setAdapter(favouritesAdapter);
                        Constants.show_error_popup(getActivity(), "Favourites not added.", error_layout);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetFavourites>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                }


            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public class MyFavouritesAdapter extends RecyclerView.Adapter<MyFavouritesAdapter.ViewHolder> {
        LinearLayout ll_km_favourite_list,ll_favourite_list;
        View view=null;
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
             view = inflater.inflate(R.layout.favourites_list, parent, false);
            iv_like_icon=(ImageView)view.findViewById(R.id.iv_like_icon);
            iv_share_img=(ImageView)view.findViewById(R.id.iv_share_img);
            iv_like_icon.setImageResource(R.drawable.like_icon_selected);
             ll_km_favourite_list=(LinearLayout)view.findViewById(R.id.ll_km_favourite_list);
             ll_favourite_list=(LinearLayout)view.findViewById(R.id.ll_favourite_list);
            ll_km_favourite_list.setVisibility(View.GONE);
            MyFavouritesAdapter.ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
          iv_img_favourites.setHierarchy(hierarchy);

            if(getFavouritelist.get(position).getFavType().equalsIgnoreCase("Destination")) {
                iv_img_favourites.setImageURI(Constants.TimbThumb_ImagePath + getFavouritelist.get(position).getFavDestination().get(0).getDesImage()+"&width="+(ExploreActivity.width));

                tv_rating_dest.setText(getFavouritelist.get(position).getFavDestination().get(0).getDesRating());

                //     tv_time_km_fav_list.setText((int)Math.ceil(getFavouritelist.get(position).getFavDestination().get(0).getDistance())+"Kms ["+Constants.convert_minute_hrs(getFavouritelist.get(position).getFavDestination().get(0).getDuration().doubleValue())+"]");
                if (getFavouritelist.get(position).getFavId() == 0) {
                    iv_like_icon.setImageResource(R.drawable.like_icon_unselected);
                } else {
                    iv_like_icon.setImageResource(R.drawable.like_icon_selected);

                }
                iv_like_icon.setId(position);

                tv_img_name.setText(getFavouritelist.get(position).getFavDestination().get(0).getDesName());
                Log.d("System out", "Fav Dest Name_______" + getFavouritelist.get(position).getFavDestination().get(0).getDesName());
                if (getFavouritelist.get(position).getFavDestination().get(0).getDestBestTimeFrom() != null && getFavouritelist.get(position).getFavDestination().get(0).getDesBestTimeTo() != null) {
                    tv_from_to_month.setText(Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavDestination().get(0).getDestBestTimeFrom())) + " - " + Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavDestination().get(0).getDesBestTimeTo())));

                }
                iv_like_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


                            String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\", \"FavType\": \"Destination\",   \"FavRefId\": \"" + getFavouritelist.get(v.getId()).getFavRefId() + "\"}]";
                            Log.d("System out", "In add favourite from my favourote menu " + json);

                            RemoveFromFavouritesAPI(json, (ImageView) v);


                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                        }


                    }





                    private void RemoveFromFavouritesAPI(final String json, final ImageView imageView) {
                        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                        pb_dialog.setCancelable(false);
                        pb_dialog.show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
                        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
                        Call<ArrayList<AddFavourite>> call = apiService.addFavourites(json);
                        call.enqueue(new Callback<ArrayList<AddFavourite>>() {
                            @Override
                            public void onResponse(Call<ArrayList<AddFavourite>> call, Response<ArrayList<AddFavourite>> response) {
                                if (response.body() != null) {

                                    pb_dialog.dismiss();
                                    addFavList = response.body();
                                    if (addFavList.get(0).getResStatus() == true) {
                                        //  Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                        if (addFavList.get(0).getResDescription().toLowerCase().contains("remove"))

                                        {
                                            //imageView.setImageResource(R.drawable.like_icon_unselected);


                                            final Dialog dialog1 = new Dialog(getActivity());
                                            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            dialog1.setContentView(R.layout.like_remove_layout);
                                            final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                            tv_text_like_remove.setText("Removed from your favourites");
                                            dialog1.setCanceledOnTouchOutside(true);
                                            dialog1.show();
                                            like = true;
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        Thread.sleep(1500);
                                                        ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                dialog1.dismiss();
                                                                String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
                                                                Log.d("System out", "In Remove icon click___" + json);
                                                                GetFavouritesAPI(json);
                                                                //Code for the UiThread
                                                            }
                                                        });
                                                       /* runOnUiThread(new Runnable() {

                                                            @Override
                                                            public void run() {
                                                                dialog1.dismiss();
                                                                String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
                                                                Log.d("System out", "In Remove icon click___" + json);
                                                                GetFavouritesAPI(json);
                                                            }
                                                        });*/
                                                    } catch (Exception e) {
                                                        Log.w("Exception in splash", e);
                                                    }

                                                }
                                            }).start();


                                        }
                                    } else {
                                        // Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                                    }


                                }


                            }

                            @Override
                            public void onFailure(Call<ArrayList<AddFavourite>> call, Throwable t) {
                                // Log error here since request failed
                                pb_dialog.dismiss();
                                t.printStackTrace();
                                if (!Constants.isInternetAvailable(getActivity())) {
                                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                                } else {
                                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                                }


                            }
                        });
                    }

                });
            }else
            {
                iv_img_favourites.setImageURI(Constants.TimbThumb_ImagePath + getFavouritelist.get(position).getFavPOILists().get(0).getPOIImage()+"&width="+(ExploreActivity.width));

                tv_rating_dest.setText(getFavouritelist.get(position).getFavPOILists().get(0).getPOIRating());

                //     tv_time_km_fav_list.setText((int)Math.ceil(getFavouritelist.get(position).getFavDestination().get(0).getDistance())+"Kms ["+Constants.convert_minute_hrs(getFavouritelist.get(position).getFavDestination().get(0).getDuration().doubleValue())+"]");
                if (getFavouritelist.get(position).getFavId() == 0) {
                    iv_like_icon.setImageResource(R.drawable.like_icon_unselected);
                } else {
                    iv_like_icon.setImageResource(R.drawable.like_icon_selected);

                }
                iv_like_icon.setId(position);

                tv_img_name.setText(getFavouritelist.get(position).getFavPOILists().get(0).getPOIName());
                Log.d("System out", "Fav Dest Name_______" + getFavouritelist.get(position).getFavPOILists().get(0).getPOIName());
                if (getFavouritelist.get(position).getFavPOILists().get(0).getPOIBestTimeFrom() != null && getFavouritelist.get(position).getFavPOILists().get(0).getPOIBestTimeTo() != null) {
                    tv_from_to_month.setText(Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavPOILists().get(0).getPOIBestTimeFrom())) + " - " + Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavPOILists().get(0).getPOIBestTimeTo())));

                }
                iv_like_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                            // String json = "[{\"umMobile\": \"" + tvVehicleColor.getText().toString().trim() + "\",\"umFirstName\":\"" + et_name_sign_up.getText().toString() + "\",   \"umLastName\": \"\",\"umEmailId\": \"" + et_email_sign_up.getText().toString() + "\",   \"umPassword\": \"" + et_password_sign_up.getText().toString() + "\",\"umDeviceType\": \"Android\",   \"umUDID\": \"\",\"umProfilePhoto\": \"\",   \"umDOB\": \"\",   \"umDescription\": \"\",\"umLocation\": \"\",\"umVehicleNumber\": \"\",\"umLat\":\"\",\"umLong\":\"\" ,\"umIsPushOn\":\"" + "true" + "\",\"umTitle\":\"\" ,\"umPetroCard\":\"\", \"umReferralCode\":\"" + et_referral_sign_up.getText().toString() + "\"}]";


                            String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\", \"FavType\": \"POI\",   \"FavRefId\": \"" + getFavouritelist.get(v.getId()).getFavRefId() + "\"}]";
                            Log.d("System out", "In add favourite from my favourite menu " + json);

                            RemoveFromFavouritesAPI(json, (ImageView) v);


                        } else {
                            Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                        }


                    }

                    private void RemoveFromFavouritesAPI(final String json, final ImageView imageView) {
                        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                        pb_dialog.setCancelable(false);
                        pb_dialog.show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
                        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
                        Call<ArrayList<AddFavourite>> call = apiService.addFavourites(json);
                        call.enqueue(new Callback<ArrayList<AddFavourite>>() {
                            @Override
                            public void onResponse(Call<ArrayList<AddFavourite>> call, Response<ArrayList<AddFavourite>> response) {
                                if (response.body() != null) {

                                    pb_dialog.dismiss();
                                    addFavList = response.body();
                                    if (addFavList.get(0).getResStatus() == true) {
                                        //  Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                        if (addFavList.get(0).getResDescription().toLowerCase().contains("remove"))

                                        {
                                            //imageView.setImageResource(R.drawable.like_icon_unselected);


                                            final Dialog dialog1 = new Dialog(getActivity());
                                            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            dialog1.setContentView(R.layout.like_remove_layout);
                                            final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                            tv_text_like_remove.setText("Removed from your favourites");
                                            dialog1.setCanceledOnTouchOutside(true);
                                            dialog1.show();
                                            like = true;
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        Thread.sleep(1500);
                                                        ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                dialog1.dismiss();
                                                                String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
                                                                Log.d("System out", "In Remove icon click___" + json);
                                                                GetFavouritesAPI(json);

                                                                //Code for the UiThread
                                                            }
                                                        });
                                                        /*runOnUiThread(new Runnable() {

                                                            @Override
                                                            public void run() {
                                                                dialog1.dismiss();
                                                                String json = "[{     \"FavUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\"}]";
                                                                Log.d("System out", "In Remove icon click___" + json);
                                                                GetFavouritesAPI(json);
                                                            }
                                                        });*/
                                                    } catch (Exception e) {
                                                        Log.w("Exception in splash", e);
                                                    }

                                                }
                                            }).start();


                                        }
                                    } else {
                                        // Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                        //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                                    }


                                }


                            }

                            @Override
                            public void onFailure(Call<ArrayList<AddFavourite>> call, Throwable t) {
                                // Log error here since request failed
                                pb_dialog.dismiss();
                                t.printStackTrace();
                                if (!Constants.isInternetAvailable(getActivity())) {
                                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                                } else {
                                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                                }

                            }
                        });
                    }

                });
            }
            iv_share_img.setId(position);
            iv_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url= "https://goo.gl/lTciIP";

                    try {
                        if(getFavouritelist.get(position).getFavType().equalsIgnoreCase("Destination")) {

                          /*  get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath +
                                    getFavouritelist.get(position).getFavDestination().get(0).getDesImage()
                                    + "&width=" + (500) + "&height=" + (500),
                                    getFavouritelist.get(position).getFavDestination().get(0).getDesImage(),
                                    "Destination: " + getFavouritelist.get(position).getFavDestination().get(0).getDesName()
                                            + "\n" + "Best Time To Visit: "
                                            + Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavDestination().get(0).getDestBestTimeFrom()))
                                            + " - " + Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavDestination().get(0).getDesBestTimeTo())));*/
                            get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath +  getFavouritelist.get(position).getFavDestination().get(0).getDesImage()+ "&width=" + (500) + "&height=" + (500),  getFavouritelist.get(position).getFavDestination().get(0).getDesImage(), "Take a peek at " +getFavouritelist.get(position).getFavDestination().get(0).getDesName()+ ".\n" + "Plan your adventure filled roadtrip with Happy Roads."+ Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));


                        }else
                        {
                            //get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + getFavouritelist.get(position).getFavPOILists().get(0).getPOIImage() + "&width=" + (500) + "&height=" + (500), getFavouritelist.get(position).getFavPOILists().get(0).getPOIImage(), "POI: " + getFavouritelist.get(position).getFavPOILists().get(0).getPOIName() + "\n" + "Best Time To Visit: " + Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavPOILists().get(0).getPOIBestTimeFrom())) + " - " + Constants.getMonthName(Integer.parseInt(getFavouritelist.get(position).getFavPOILists().get(0).getPOIBestTimeTo())));
                            get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + getFavouritelist.get(position).getFavPOILists().get(0).getPOIImage()+ "&width=" + (500) + "&height=" + (500), getFavouritelist.get(position).getFavPOILists().get(0).getPOIImage(), "Take a peek at " +getFavouritelist.get(position).getFavPOILists().get(0).getPOIName()+ ".\n" + "Plan your adventure filled roadtrip with Happy Roads."+ Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));


                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }


            });

            view.setId(position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getFavouritelist.get(v.getId()).getFavType().equalsIgnoreCase("Destination")) {
                        getDestinationAPI(getFavouritelist.get(v.getId()).getFavDestination().get(0).getDesName());
                    }else
                    {
                        getPOIAPI(getFavouritelist.get(v.getId()).getFavPOILists().get(0).getPOINearbyDestination(),v.getId());
                    }
                    /*{


                            Bundle bundle = new Bundle();
                            bundle.putString("position", String.valueOf(v.getId()));
                            bundle.putString("placeName",getFavouritelist.get(v.getId()).getFavPOILists().get(0).getPOIName());
                            bundle.putInt("tripId",tripid);
                            bundle.putString("destId","");
                            bundle.putString("DestName",((ExploreActivity)getActivity()).tv_text_header.getText().toString());
                            String json = gson.toJson(explorePoiList);
                            editor.putString("POIList", json);
                            editor.commit();
                            //  bundle=getArguments();
                            ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                            ExploreActivity.destattractionimages.clear();
                            ((ExploreActivity)getActivity()).setpagerdata(false);
                            for (int i=0;i<explorePoiList.get(v.getId()).getImageList().size();i++)
                            {
                                if(explorePoiList.get(v.getId()).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                                    ExploreActivity.destattractionimages.add(explorePoiList.get(v.getId()).getImageList().get(i).getImgName());
                                }
                            }
                            // bundle.putAll(getArguments());
                            exploreAttractionFragment.setArguments(bundle);
                            changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                            changeTransaction.addToBackStack(null);
                            changeTransaction.commit();




                    }*/
                }
            });


        }

        public void getPOIAPI(final int destId, final int position) {


                // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                // explore_places_content_progressbar.setVisibility(View.VISIBLE);
                final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                pb_dialog.setCancelable(false);
                pb_dialog.show();
                String json = null;
                String id="";
                if(sessionManager.isLoggedIn())
                {
                    id=sessionManager.get_Authenticate_User().getUmId()+"";
                }
                //"POILatitude":"13", "POILongitude":"132"
                //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
                // String json="[{\"umId\": \"0\",\"POIID\":\""+0+"\"    , \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\"  }]";
                // String json = "[{\"umId\":\"" + "1" + "\"}]";

                    // json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+999999+"\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\"  }]";
                    json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+getFavouritelist.get(position).getFavPOILists().get(0).getPOIID()+"\" ,\"radius\":\""+999999+"\"   , \"POINearbyDestination\": \"0\", \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+123+"\",\"POILatitude\":\""+123+"\", \"POIRating\":0  }]";

                    Log.d("System out","Explore POI from favorites____"+json);




                Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
                call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
                    @Override
                    public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {

                        pb_dialog.dismiss();

                        if (response.body() != null) {


                            explorePoiList.clear();

                            explorePoiList.addAll(response.body());
                            if (explorePoiList.size()>0)
                            {
                                if (explorePoiList.get(0).getResStatus() == true) {

                                    //   explore_places_content_progressbar.setVisibility(View.GONE);

                                    //  Collections.reverse(exploreDestList);

                                    String json = gson.toJson(explorePoiList); // myObject - instance of MyObject
                                    editor.putString("POIList", json);
                                    editor.commit();

                                    /*if(destinationPlanList.size()>0)
                                    {
                                        for(int i=0;i<explorePoiList.size();i++) {
                                            for(int j=0;j<destinationPlanList.size();j++)
                                            {
                                                Log.d("desid",String.valueOf(destinationPlanList.get(j).getPoiServerId())+"poiid"+explorePoiList.get(i).getPOIID());
                                                if(destinationPlanList.get(j).getPoiServerId()==explorePoiList.get(i).getPOIID())
                                                    ((ExploreActivity) getActivity()).AddtoMyPlanList.add(String.valueOf(i));
                                            }

                                            }
                                        }*/


                                    Log.d("desid","");

                                            Bundle bundle = new Bundle();
                                            bundle.putString("position","0");
                                            bundle.putString("placeName",explorePoiList.get(0).getPOIName());
                                            bundle.putInt("tripId",0);
                                            bundle.putString("destId",""+destId);
                                            bundle.putString("DestName",((ExploreActivity)getActivity()).tv_text_header.getText().toString());
                                        json = gson.toJson(explorePoiList);
                                            editor.putString("POIList", json);
                                            editor.commit();
                                            //  bundle=getArguments();


                                            ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();




                                            ExploreActivity.destattractionimages.clear();
                                            ((ExploreActivity)getActivity()).setpagerdata(false);
                                            for (int i=0;i<explorePoiList.get(0).getImageList().size();i++)
                                            {
                                                if(explorePoiList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                                                    ExploreActivity.destattractionimages.add(explorePoiList.get(0).getImageList().get(i).getImgName());
                                                }
                                            }
                                            // bundle.putAll(getArguments());
                                            exploreAttractionFragment.setArguments(bundle);
                                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                                    changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                                            changeTransaction.addToBackStack(null);
                                            changeTransaction.commit();







                                }
                                else
                                {
                                    //  explore_places_content_progressbar.setVisibility(View.GONE);
                                    Constants.show_error_popup(getActivity(),explorePoiList.get(0).getResDescription()+"", error_layout);
                                }
                            }
                            else
                            {
                                //   explore_places_content_progressbar.setVisibility(View.GONE);
                            }



                        }


                    }

                    @Override
                    public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                        // Log error here since request failed
                        //  explore_places_content_progressbar.setVisibility(View.GONE);
                        pb_dialog.dismiss();
                        t.printStackTrace();
                        if (!Constants.isInternetAvailable(getActivity())) {
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                        } else {
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                        }

                    }
                });


        }


        private void getDestinationAPI(String des) {
            // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/
            final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(false);
            pb_dialog.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String id="";
            if(sessionManager.isLoggedIn())
            {
                id=sessionManager.get_Authenticate_User().getUmId()+"";
            }

            String json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"999999\",   \"desName\": \"" + des + "\",   \"desLongitude\": \""+Constants.selectedLong+"\",   \"desLatitude\": \""+Constants.selectedLat+"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":0 }]";
            Log.d("System out", " json " + json);
            Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
            call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
                @Override
                public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();

                    if (response.body() != null) {

                        pb_dialog.dismiss();
                        exploreDestList.clear();
                        exploreDestList.addAll(response.body());
                        if (exploreDestList.size() > 0) {

                            Collections.reverse(exploreDestList);

                            String json = gson.toJson(exploreDestList); // myObject - instance of MyObject
                            editor.putString("DestList", json);
                            editor.commit();

                            ListingDetailsFragment listingDetailsFragment = new ListingDetailsFragment();
                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            // ((ExploreActivity)getActivity()).setHeader("Change Password");
                            changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                            changeTransaction.replace(R.id.frame, listingDetailsFragment);
                            changeTransaction.addToBackStack(null);
                            Bundle bundle = new Bundle();
                            bundle.putString("placeName",exploreDestList.get(0).getDesName());
                            bundle.putString("from","my favorites");
                            bundle.putString("position",0+"");
                            bundle.putSerializable("Explorlist", exploreDestList.get(0));
                            ExploreActivity.destattractionimages.clear();
                            for (int i=0;i<exploreDestList.get(0).getImageList().size();i++)
                            {
                                if(exploreDestList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                                    ExploreActivity.destattractionimages.add(exploreDestList.get(0).getImageList().get(i).getImgName());
                                }
                            }


                            listingDetailsFragment.setArguments(bundle);
                            changeTransaction.commit();
                        } else {
                            Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(), errorLayout);
                        }


                    }


                }

                @Override
                public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                    // Log error here since request failed
                    pb_dialog.dismiss();
                    t.printStackTrace();
                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), errorLayout);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), errorLayout);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return getFavouritelist.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {


            public ViewHolder(View itemView) {
                super(itemView);
                 iv_img_favourites=(SimpleDraweeView)view.findViewById(R.id.iv_img_favourites);
                  tv_img_name=(TextView)view.findViewById(R.id.tv_img_name);
                  tv_time_km_fav_list=(TextView)view.findViewById(R.id.tv_time_km_fav_list);
                  tv_from_to_month=(TextView)view.findViewById(R.id.tv_from_to_month);
                  tv_rating_dest=(TextView)view.findViewById(R.id.tv_rating_dest);

                // final String placeName=tv_img_name.getText().toString();
                iv_like_icon=(ImageView)view.findViewById(R.id.iv_like_icon);
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).tv_text_header.setText("My Favorites");
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
    }
    public void  get_bitmap_from_freco_download(final String url, final String name,final  String besttime)
    {
        Log.d("System out","sharing image path "+url);


//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    dataSource.close();
                    DownloadCode code = new DownloadCode(getActivity(),Constants.TimbThumb_ImagePath,name,bitmap,besttime);
                    code.share_image_text_GPLUS();


                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }
}
