package com.bpcl.happyroads;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.Pojo.GetRO;
import com.bpcl.happyroads.Pojo.ServiceImage;
import com.bpcl.happyroads.Pojo.ServicesPojo;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 10/24/2016.
 */
public class FacilitiesFragment extends Fragment {
    String name = "";
    LinearLayout atm_facility,restaurant_facility,car_garage_facility,hospital_facility,pharmacy_facility,toll_booth_facility,toilet_facility,ro_details_facility,ll_toll_layout;
    TextView tv_petrol_pump_working_hrs,tv_petrol_pump_address,tv_petrol_pump_name,tv_petrol,tv_diesel,tv_cng,tv_petrol_email,tv_petrol_contact_no,tv_petrol_contact_name;
    LinearLayout ll_phone_ro,ll_email_ro,ll_name_ro;
    TextView tv_hospital_name,tv_hospital_emergency_contact_no,tv_hospital_side,tv_hospital_address,tv_hospital_opd_hrs,tv_hospital_ambulance,tv_hospital_other_no,tv_hospital_working_hrs;
    LinearLayout ll_facility_header,ll_hospital_facility,ll_emergency,ll_ambulance,ll_other,ll_contact_header_hospital;
    LinearLayout ll_basic_header,ll_additional_header,basic_services,ll_fuels_header,ll_fuel_content,ll_additional_services_content;
    LinearLayout ll_rest_cuisine_header,ll_rest_cuisine_category,ll_rest_facility_header,ll_rest_facility_content,ll_toilet_type_header,ll_toilet_type_content,ll_toilet_icon;
    LinearLayout ll_email_rest,ll_phone_rest,ll_name_rest,ll_contact_header_rest;
    LinearLayout ll_garage_vehicle_type,ll_garage_facility_header,ll_garage_facility_content,ll_garage_vehicle_type_content;
    TextView tv_garage_contact_name,tv_garage_contact_no;
    SimpleDraweeView iv_toilet_icon;

    TextView tv_rest_name,tv_rest_address,tv_rest_side,tv_rest_working_hrs,tv_rest_ac,tv_rest_contact_name,rest_email,tv_rest_contact_no,tv_toilet_working_hrs,tv_toilet_side,tv_toilet_address,tv_both,tv_indore,tv_outdoor,tv_cuisine,tv_indian,tv_western,tv_seperate,tv_handicape;
    ImageView iv_indian,iv_western,iv_seperate,iv_handicape;
    LinearLayout ll_indore,ll_outdoor,ll_veg,ll_nonveg,ll_foodtype,ll_seating,ll_ac;
    TextView tv_atm_name,tv_atm_address,tv_atm_side,tv_atm_working_hrs,tv_atm_toll_free_no,tv_atm_email;
    LinearLayout ll_atm_icon,ll_email_atm,ll_toll_free_no_atm;
    SimpleDraweeView iv_atm_icon,iv_toll_icon;
    TextView tv_toll_name,tv_toll_address;

    //pharmacy
    TextView tv_pharmacy_name,tv_pharmacy_address,tv_pharmacy_side,tv_pharmacy_working_hrs,tv_pharmacy_contact_no,tv_pharmacy_contact_name;
    LinearLayout ll_contact_header_pharmacy,ll_name_pharmacy,ll_phone_pharmacy;

    //Garage
    TextView tv_garage_name,tv_garage_address,tv_garage_working_hrs,tv_garage_person_name,tv_garage_number,tv_garage_side;
    LinearLayout ll_vehicle_type,ll_type,ll_facilities,ll_add_garage_facilities,nearby_toilet_ll_ll,nearby_toilet_ll,ll_contact_header_garage,ll_name_garage,ll_phone_garage;

    public static GetRO getROArrayList=new GetRO();
    public static ServicesPojo servicesPojoArrayList=new ServicesPojo();
    Gson gson=new Gson();
    ServiceImage serviceImageArrayList =new ServiceImage();
    ArrayList<String> Rofueltypes=new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.facilities_details, container, false);
        init(v);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {

            name = getArguments().getString("facilitytype", "");
        } else {
            name = "";
        }
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });

        if(name.equalsIgnoreCase("r"))
        {
            String jsonOutput = getArguments().getString("rolist","");
            Type listType = new TypeToken<GetRO>(){}.getType();
            getROArrayList = gson.fromJson(jsonOutput, listType);
            ro_details_facility.setVisibility(View.VISIBLE);
            if(!getROArrayList.getROToilet().equalsIgnoreCase(""))
            {
                //ArrayList<GetRO.GetRoBasicServiceDetail> getRoBasicServiceDetailList= new ArrayList<GetRO.GetRoBasicServiceDetail>();
                GetRO.GetRoBasicServiceDetail getRoBasicServiceDetail= new GetRO.GetRoBasicServiceDetail();

                getRoBasicServiceDetail.setROBasicSerID(0);
                getRoBasicServiceDetail.setROBasicSerName("Toilet");
                getRoBasicServiceDetail.setROBasicSerImage("");
                getROArrayList.getGetRoBasicServiceDetails().add(getRoBasicServiceDetail);

            }


            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            ((ExploreActivity)getActivity()).iv_ro_detail.setHierarchy(hierarchy);
            setRoData();

            // getFacilityDetail();
        }else if(name.equalsIgnoreCase("s"))
        {
            String jsonOutput1 = getArguments().getString("servicelist","");
            Type listType1 = new TypeToken<ServicesPojo>(){}.getType();
            servicesPojoArrayList = gson.fromJson(jsonOutput1, listType1);

            GenericDraweeHierarchyBuilder builder = new GenericDraweeHierarchyBuilder(getResources());
            GenericDraweeHierarchy hierarchy = builder.setFadeDuration(10).build();
            hierarchy.setProgressBarImage(new ImageLoadProgressBar());
            ((ExploreActivity)getActivity()).iv_ro_detail.setHierarchy(hierarchy);

            if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("Pharmacy"))
            {
                pharmacy_facility.setVisibility(View.VISIBLE);
                setPharmacyData();
            }else if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("Hospital"))
            {
                hospital_facility.setVisibility(View.VISIBLE);
                setHospitalData();
            }else if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("Restaurant"))
            {
                restaurant_facility.setVisibility(View.VISIBLE);
                setRestaurantData();
            }else if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("ATM"))
            {
                atm_facility.setVisibility(View.VISIBLE);
                ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.GONE);
                ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
                ((ExploreActivity)getActivity()).tv_text_header.setText("ATM");
               // ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(R.color.blueheader)); // transperent color = #00000000
                ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(R.color.blueheader);


                setAtmData();
            }else if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("Toilet"))
            {
                ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.GONE);

                toilet_facility.setVisibility(View.VISIBLE);
                ll_toilet_icon.setVisibility(View.VISIBLE);
                ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
                ((ExploreActivity)getActivity()).tv_text_header.setText("Toilet");
                //    ((ExploreActivity)getActivity()).collapsing_toolbar.setBackgroundColor(getResources().getColor(R.color.colorAccent)); // transperent color = #00000000


                setToiletData();
            }else if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("TollBooth"))
            {
                toll_booth_facility.setVisibility(View.VISIBLE);
                //  ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerCatImage());
                ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.GONE);
                ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
                ((ExploreActivity)getActivity()).tv_text_header.setText("Toll");
                //  ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(R.color.colorAccent)); // transperent color = #00000000
                setTooBoothData();
            }else if(servicesPojoArrayList.getSerCatName().equalsIgnoreCase("Car Garage"))
            {
                //  ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); // transperent color = #00000000

                car_garage_facility.setVisibility(View.VISIBLE);
                setCarGarageData();
            }
        }

        return  v;
    }


    private void setCarGarageData() {
        ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerDefaultImage());
        ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);
        tv_garage_name.setText(servicesPojoArrayList.getSerName());
        if(servicesPojoArrayList.getSerContactPerson().equalsIgnoreCase("")&&servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase(""))
        {
         ll_contact_header_garage.setVisibility(View.GONE);
        }else
        {
        ll_contact_header_garage.setVisibility(View.VISIBLE);
        }
        if(servicesPojoArrayList.getSerContactPerson().equalsIgnoreCase(""))
        {
            ll_name_garage.setVisibility(View.GONE);
        }else
        {
            ll_name_garage.setVisibility(View.VISIBLE);
            tv_garage_person_name.setText(servicesPojoArrayList.getSerContactPerson());
        }
        if(servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase(""))
        {
            ll_phone_garage.setVisibility(View.GONE);
        }else
        {
            ll_phone_garage.setVisibility(View.VISIBLE);
            tv_garage_number.setText("+91 "+servicesPojoArrayList.getSerCustCareNo());

        }
        tv_garage_address.setText(servicesPojoArrayList.getSerAddress1() + ", " + servicesPojoArrayList.getCityname() + ", " + servicesPojoArrayList.getStatename());
       // tv_garage_number.setText("+91 "+servicesPojoArrayList.getSerCustCareNo());
        tv_garage_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_garage_number.getText().toString(),getActivity());
            }
        });
        tv_garage_side.setText(servicesPojoArrayList.getSerSide());
        try {
            if(servicesPojoArrayList.getSerIsFullTime()==true)
            {
                tv_garage_working_hrs.setText("24 hours");
            }else {
                tv_garage_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "hh:mm a"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        addGarageType(ll_vehicle_type,servicesPojoArrayList);
        addOthereServicesThree(ll_add_garage_facilities,servicesPojoArrayList);
    }

    private void setTooBoothData() {
        ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerDefaultImage()));
        tv_toll_name.setText(servicesPojoArrayList.getSerName());
        tv_toll_address.setText(servicesPojoArrayList.getSerAddress1()+", "+servicesPojoArrayList.getCityname()+", "+servicesPojoArrayList.getStatename());


      /*  if (ll_toll_layout.getChildCount() > 0) {
            ll_toll_layout.removeAllViews();

        }*/
/*
        if (servicesPojoArrayList.getSerCatName().equalsIgnoreCase("Toll").si > 0) {
            String[] FeesCatNameList = getAdvancePOIDetaillist.get(0).getPOIEntryFeeCategoryName().split(",");
            String[] POIEntryFeeRegularRates = getAdvancePOIDetaillist.get(0).getPOIEntryFeeRegularRates().split(",");
            String[] POIEntryFeeSeasonRates = getAdvancePOIDetaillist.get(0).getPOIEntryFeeSeasonRates().split(",");
            for (int i = 0; i < FeesCatNameList.length; i++) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v1;
                v1 = inflater.inflate(R.layout.toll_booth_cell, null);
                TableLayout tl_fees_header = (TableLayout) v1.findViewById(R.id.tl_fees_header);
                TextView tv_type_value = (TextView) v1.findViewById(R.id.tv_type_value);
                TextView tv_regular_value = (TextView) v1.findViewById(R.id.tv_regular_value);
                TextView tv_season_value = (TextView) v1.findViewById(R.id.tv_season_value);
                final TextView line = (TextView) v1.findViewById(R.id.line);
                line.setVisibility(View.GONE);
                if (i == 0) {
                    tl_fees_header.setVisibility(View.VISIBLE);
                } else {
                    tl_fees_header.setVisibility(View.GONE);

                }
                tv_type_value.setText(FeesCatNameList[i]);
                if(POIEntryFeeRegularRates.length>i)
                {
                    tv_regular_value.setText(POIEntryFeeRegularRates[i]);

                }else
                {
                    tv_regular_value.setText("-");

                }

                // tv_regular_value.setText(POIEntryFeeRegularRates[i]);

                if(POIEntryFeeSeasonRates.length>i)
                {
                    tv_season_value.setText(POIEntryFeeSeasonRates[i]);
                }else
                {
                    tv_season_value.setText("-");
                }

                if (FeesCatNameList.length-1 == i)
                {
                    line.setVisibility(View.VISIBLE);
                }
                ll_toll_layout.addView(v1);
            }
        }
*/


    }


    private void setAtmData() {
        // ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerCatImage());

        iv_atm_icon.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerDefaultImage()));
        tv_atm_name.setText(servicesPojoArrayList.getSerName());
        tv_atm_address.setText(servicesPojoArrayList.getSerAddress1()+", "+servicesPojoArrayList.getCityname()+", "+servicesPojoArrayList.getStatename());
        tv_atm_side.setText(servicesPojoArrayList.getSerSide());
        try {
            if(servicesPojoArrayList.getSerIsFullTime()==true)
            {
                tv_atm_working_hrs.setText("24 hours");
            }else {
                tv_atm_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "hh:mm a"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if(servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase(""))
        {
            ll_toll_free_no_atm.setVisibility(View.GONE);
        }else
        {
            ll_toll_free_no_atm .setVisibility(View.VISIBLE);
            tv_atm_toll_free_no.setText("+91 "+servicesPojoArrayList.getSerCustCareNo());
        }

        tv_atm_toll_free_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_atm_toll_free_no.getText().toString(),getActivity());
            }
        });
        if(servicesPojoArrayList.getSerEmailId().equalsIgnoreCase(""))
        {
            ll_email_atm.setVisibility(View.GONE);
        }else
        {
            ll_email_atm.setVisibility(View.VISIBLE);
            tv_atm_email.setText(servicesPojoArrayList.getSerEmailId());

        }

    }


    private void setToiletData() {
        //((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerCatImage());

        iv_toilet_icon.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerDefaultImage()));
        tv_toilet_address.setText(servicesPojoArrayList.getSerAddress1()+", "+servicesPojoArrayList.getCityname()+", "+servicesPojoArrayList.getStatename());
        tv_toilet_side.setText(servicesPojoArrayList.getSerSide());
        try {
            if(servicesPojoArrayList.getSerIsFullTime()==true)
            {
                tv_toilet_working_hrs.setText("24 hours");
            }else {
                tv_toilet_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "hh:mm a"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        AddToiletTypes((LinearLayout) ll_toilet_type_content, servicesPojoArrayList);






    }

    /* private void setRestaurantData() {
         ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerCatImage());
         ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);
         tv_rest_name.setText(servicesPojoArrayList.getSerName());
         tv_rest_address.setText(servicesPojoArrayList.getSerAddress1()+", "+servicesPojoArrayList.getCityname()+", "+servicesPojoArrayList.getStatename());
         tv_rest_side.setText(servicesPojoArrayList.getSerSide());
         try {
             tv_rest_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "HH:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "HH:mm a"));
         }
         catch (Exception e)
         {
             e.printStackTrace();
         }
         tv_rest_ac.setText("");
         tv_rest_contact_name.setText(servicesPojoArrayList.getSerContactPerson());
         tv_rest_contact_no.setText(servicesPojoArrayList.getSerCustCareNo());
         rest_email.setText(servicesPojoArrayList.getSerEmailId());

     }*/
    private void setRestaurantData() {
        // ((ExploreActivity) getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath + servicesPojoArrayList.getSerCatImage());
        ((ExploreActivity) getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);
        // ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); // transperent color = #00000000


        ((ExploreActivity) getActivity()).iv_ro_detail.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerDefaultImage()));
        tv_rest_name.setText(servicesPojoArrayList.getSerName());
        if(servicesPojoArrayList.getSerAddress1().equalsIgnoreCase(""))
        {
            tv_rest_address.setText(servicesPojoArrayList.getCityname() + ", " + servicesPojoArrayList.getStatename());

        }else {
            tv_rest_address.setText(servicesPojoArrayList.getSerAddress1() + ", " + servicesPojoArrayList.getCityname() + ", " + servicesPojoArrayList.getStatename());
        }
        tv_rest_side.setText(servicesPojoArrayList.getSerSide());
        try {
            if(servicesPojoArrayList.getSerIsFullTime()==true)
            {
                tv_rest_working_hrs.setText("24 hours");
            }else {
                tv_rest_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "hh:mm a"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String ac = "";

        if(!servicesPojoArrayList.getRestACService().equalsIgnoreCase("")) {
            for (int i = 0; i < servicesPojoArrayList.getGetACServiceLists().size(); i++) {
                if (servicesPojoArrayList.getRestACService().contains(servicesPojoArrayList.getGetACServiceLists().get(i).getId() + "")) {
                    ac += servicesPojoArrayList.getGetACServiceLists().get(i).getName() + ",";
                }
            }
            tv_rest_ac.setText("" + ac.substring(0, ac.length() - 1));
        }
        else
        {
            ll_ac.setVisibility(View.INVISIBLE);
        }

        if(servicesPojoArrayList.getSerContactPerson().equalsIgnoreCase("")&&servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase("")&&servicesPojoArrayList.getSerEmailId().equalsIgnoreCase(""))
        {
            ll_contact_header_rest.setVisibility(View.GONE);
            ll_name_rest.setVisibility(View.GONE);
            ll_phone_rest.setVisibility(View.GONE);
            ll_email_rest.setVisibility(View.GONE);



        }

        else
        {
            ll_contact_header_rest.setVisibility(View.VISIBLE);
            if(servicesPojoArrayList.getSerContactPerson().equalsIgnoreCase(""))
            {
            ll_name_rest.setVisibility(View.GONE);
        }else
        {
            tv_rest_contact_name.setText(servicesPojoArrayList.getSerContactPerson());

        }
        if(servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase(""))
        {
            ll_phone_rest.setVisibility(View.GONE);
        }else {
            tv_rest_contact_no.setText("+91 "+servicesPojoArrayList.getSerCustCareNo());
        }

        if(servicesPojoArrayList.getSerEmailId().equalsIgnoreCase(""))
        {
            ll_email_rest.setVisibility(View.GONE);
        }else {

            rest_email.setText(servicesPojoArrayList.getSerEmailId());
        }
        }


        tv_rest_contact_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_rest_contact_no.getText().toString(),getActivity());
            }
        });



        ll_indore.setVisibility(View.GONE);
        ll_outdoor.setVisibility(View.INVISIBLE);
        tv_both.setVisibility(View.INVISIBLE);

        if(!servicesPojoArrayList.getRestSeatingFacility().equalsIgnoreCase("")) {
            for (int i = 0; i < servicesPojoArrayList.getGetResSeatingFacilitiesList().size(); i++) {
                if (servicesPojoArrayList.getRestSeatingFacility().contains(servicesPojoArrayList.getGetResSeatingFacilitiesList().get(i).getId() + "")) {
                    if (servicesPojoArrayList.getGetResSeatingFacilitiesList().get(i).getValues().equalsIgnoreCase("Indoor")) {
                        ll_indore.setVisibility(View.VISIBLE);

                    } else if (servicesPojoArrayList.getGetResSeatingFacilitiesList().get(i).getValues().equalsIgnoreCase("Outdoor")) {
                        ll_outdoor.setVisibility(View.VISIBLE);
                    } else if (servicesPojoArrayList.getGetResSeatingFacilitiesList().get(i).getValues().equalsIgnoreCase("Both")) {
                        tv_both.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
        else
        {
            ll_seating.setVisibility(View.GONE);
        }

        if (!servicesPojoArrayList.getRestFoodType().equalsIgnoreCase(""))
        {
            ll_veg.setVisibility(View.GONE);
            ll_nonveg.setVisibility(View.GONE);
            for (int i = 0; i < servicesPojoArrayList.getGetFoodTypeLists().size(); i++)
            {

                if (servicesPojoArrayList.getRestFoodType().contains(servicesPojoArrayList.getGetFoodTypeLists().get(i).getFoodId() + ""))
                {
                    if (servicesPojoArrayList.getGetFoodTypeLists().get(i).getFoodTypeName().equalsIgnoreCase("Veg"))
                    {
                        ll_veg.setVisibility(View.VISIBLE);

                    } else if (servicesPojoArrayList.getGetFoodTypeLists().get(i).getFoodTypeName().equalsIgnoreCase("Non Veg"))
                    {
                        ll_nonveg.setVisibility(View.VISIBLE);
                    }

                }
            }
        }
        else {
            ll_foodtype.setVisibility(View.GONE);
        }

        if(!servicesPojoArrayList.getRestCuisine().equalsIgnoreCase("")) {
            String cuiris = "";
            for (int i = 0; i < servicesPojoArrayList.getGetCuisineLists().size(); i++) {
                if (servicesPojoArrayList.getRestCuisine().contains(servicesPojoArrayList.getGetCuisineLists().get(i).getCuisineId() + "")) {
                    cuiris += servicesPojoArrayList.getGetCuisineLists().get(i).getCuisineName() + "  |  ";

                }
            }
            tv_cuisine.setText(cuiris.substring(0, cuiris.length() - 5));
        }
        else
        {
            ll_rest_cuisine_header.setVisibility(View.GONE);
        }



        if(servicesPojoArrayList.getSerOtherFacilityLists().size()>0) {

            addOthereServicesThree(ll_rest_facility_content,servicesPojoArrayList);

        }
        else
        {
            ll_rest_facility_header.setVisibility(View.GONE);
        }


    }

    private void setHospitalData() {

        //  fc_p.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
        // iv_img_favourites.setImageURI(Constants.TimbThumb_ImagePath+exploreDest.get(position).getDesImage());
        ((ExploreActivity) getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).iv_ro_detail.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerDefaultImage()));

        //  ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); // transperent color = #00000000

        // ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerCatImage());
        // ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);


        tv_hospital_name.setText(servicesPojoArrayList.getSerName());
        tv_hospital_side.setText(servicesPojoArrayList.getSerSide());
        tv_hospital_address.setText(servicesPojoArrayList.getSerAddress1()+", "+servicesPojoArrayList.getCityname()+", "+servicesPojoArrayList.getStatename());

        try {
            if(servicesPojoArrayList.getSerIsFullTime()==true)
            {
                tv_hospital_working_hrs.setText("24 hours");
            }else {
                tv_hospital_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "hh:mm a"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(servicesPojoArrayList.getEmergencyContactNo().equalsIgnoreCase("") &&servicesPojoArrayList.getAmbulanceContactNo().equalsIgnoreCase("") && servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase("") )
        {
           ll_contact_header_hospital.setVisibility(View.GONE);
        }else
        {
            ll_contact_header_hospital.setVisibility(View.VISIBLE);

        }

        if(servicesPojoArrayList.getEmergencyContactNo().equalsIgnoreCase(""))
        {
             ll_emergency.setVisibility(View.GONE);
        }else
        {

        ll_emergency.setVisibility(View.VISIBLE);
            tv_hospital_emergency_contact_no.setText("+91 " + servicesPojoArrayList.getEmergencyContactNo());
            }
        tv_hospital_emergency_contact_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_hospital_emergency_contact_no.getText().toString(),getActivity());
            }
        });
        if(servicesPojoArrayList.getAmbulanceContactNo().equalsIgnoreCase(""))
        {
            ll_ambulance.setVisibility(View.GONE);
        }else {
            ll_ambulance.setVisibility(View.VISIBLE);

            tv_hospital_ambulance.setText(servicesPojoArrayList.getAmbulanceContactNo());
        }
       /* tv_hospital_ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling(tv_hospital_ambulance.getText().toString(),getActivity());
            }
        });*/
        if(servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase(""))
        {
            ll_other.setVisibility(View.GONE);
        }else {
            ll_other.setVisibility(View.VISIBLE);

            tv_hospital_other_no.setText("+91 "+servicesPojoArrayList.getSerCustCareNo());
        }

        tv_hospital_other_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_hospital_other_no.getText().toString(),getActivity());
            }
        });
        tv_hospital_opd_hrs.setText("");
        if(servicesPojoArrayList!=null)
        {
            if (servicesPojoArrayList!=null)
                addOthereServicesThree((LinearLayout) ll_hospital_facility, servicesPojoArrayList);
            else
                ll_additional_header.setVisibility(View.GONE);

        }


    }

    private void setPharmacyData() {
        ((ExploreActivity) getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath + servicesPojoArrayList.getSerDefaultImage());

        ((ExploreActivity) getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);

        tv_pharmacy_name.setText(servicesPojoArrayList.getSerName());
        tv_pharmacy_address.setText(servicesPojoArrayList.getSerAddress1() + ", " + servicesPojoArrayList.getCityname() + ", " + servicesPojoArrayList.getStatename());
        tv_pharmacy_side.setText(servicesPojoArrayList.getSerSide());
        try {
            if(servicesPojoArrayList.getSerIsFullTime()==true)
            {
                tv_pharmacy_working_hrs.setText("24 hours");
            }else {
                tv_pharmacy_working_hrs.setText(Constants.formatDate(servicesPojoArrayList.getSerStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(servicesPojoArrayList.getSerEndTime(), "HH:mm:ss", "hh:mm a"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (servicesPojoArrayList.getSerContactPerson().equalsIgnoreCase("") && servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase("")) {
            ll_contact_header_pharmacy.setVisibility(View.GONE);
        } else {
            ll_contact_header_pharmacy.setVisibility(View.VISIBLE);
        }
        if (servicesPojoArrayList.getSerContactPerson().equalsIgnoreCase("")) {
            ll_name_pharmacy.setVisibility(View.GONE);
        } else {
            ll_name_pharmacy.setVisibility(View.VISIBLE);
            tv_pharmacy_contact_name.setText(servicesPojoArrayList.getSerContactPerson());

        }
        if (servicesPojoArrayList.getSerCustCareNo().equalsIgnoreCase("")) {
            ll_phone_pharmacy.setVisibility(View.GONE);
        }
        else
        {
            ll_phone_pharmacy.setVisibility(View.VISIBLE);
            tv_pharmacy_contact_no.setText("+91 " + servicesPojoArrayList.getSerCustCareNo());
        }
        tv_pharmacy_contact_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_pharmacy_contact_no.getText().toString(),getActivity());
            }
        });



    }
    private void setRoData() {
        // ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+getROArrayList.get());
        //  ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.ImagePath+servicesPojoArrayList.getSerCatImage());
        ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Uri.parse(Constants.ImagePath+getArguments().getString("ro_image","")));

       // getImage("default_RO_ImageName",((ExploreActivity)getActivity()).iv_ro_detail);
        ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);
        tv_petrol_pump_name.setText(getROArrayList.getROName());
        if(getROArrayList.getROManagerName().equalsIgnoreCase(""))
        {
            ll_name_ro.setVisibility(View.GONE);
        }else {
            ll_name_ro.setVisibility(View.VISIBLE);
            tv_petrol_contact_name.setText(getROArrayList.getROManagerName());
        }
        if(getROArrayList.getToiletTypesAllArrayList().size()>0) {

            addOthereToiletThree((LinearLayout) nearby_toilet_ll, getROArrayList);


        }

       // tv_petrol_contact_name.setText(getROArrayList.getROManagerName());
      //  tv_petrol_contact_no.setText("+91 "+getROArrayList.getROManagerContactNo());
        if(getROArrayList.getROManagerContactNo().equalsIgnoreCase(""))
                {
                    ll_phone_ro.setVisibility(View.GONE);
                }else {
                    ll_phone_ro.setVisibility(View.VISIBLE);
                    tv_petrol_contact_no.setText("+91 " + getROArrayList.getROManagerContactNo());
                }


        tv_petrol_contact_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.Calling("+91"+tv_petrol_contact_no.getText().toString(),getActivity());
            }
        });
        if(getROArrayList.getROManagerEmailId().equalsIgnoreCase(""))
        {
            ll_email_ro.setVisibility(View.GONE);
        }else {
            ll_email_ro.setVisibility(View.VISIBLE);
            tv_petrol_email.setText(getROArrayList.getROManagerEmailId());
        }
        tv_petrol_pump_address.setText(getROArrayList.getROLocationName()+", "+getROArrayList.getCityName()+", \n"+getROArrayList.getStateName());
        try {
            if(getROArrayList.getROBusHours()==true)
            {
                tv_petrol_pump_working_hrs.setText("24 hours");
            }else {
                tv_petrol_pump_working_hrs.setText(Constants.formatDate(getROArrayList.getROFrom(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(getROArrayList.getROTo(), "HH:mm:ss", "hh:mm a"));
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Rofueltypes.clear();
        if(getROArrayList.getROFuels().contains("1"))
        {
            Rofueltypes.add("Petrol");

        } if(getROArrayList.getROFuels().contains("2"))
        {
            Rofueltypes.add("Diesel");

        }
        Log.d("System out","Rofueltypes size__"+Rofueltypes.size());
        addRoFuelTypes(ll_fuel_content,Rofueltypes);


      /*  if(getROArrayList.getROFuelsType()!=null) {
            ll_fuels_header.setVisibility(View.VISIBLE);
            ll_fuel_content.setVisibility(View.VISIBLE);
            if(getROArrayList.getROFuels().equalsIgnoreCase("1"))
            {
                tv_petrol.setVisibility(View.VISIBLE);
                tv_diesel.setVisibility(View.GONE);
                tv_cng.setVisibility(View.GONE);
            }else if(getROArrayList.getROFuels().equalsIgnoreCase("2"))
            {
                tv_petrol.setVisibility(View.GONE);
                tv_diesel.setVisibility(View.VISIBLE);
                tv_cng.setVisibility(View.GONE);
            }
            else
            {
                tv_petrol.setVisibility(View.VISIBLE);
                tv_diesel.setVisibility(View.VISIBLE);
                tv_cng.setVisibility(View.GONE);
            }
        }
        else
        {
            ll_fuels_header.setVisibility(View.GONE);
            ll_fuel_content.setVisibility(View.GONE);


      }*/

        //   ((ExploreActivity)getActivity()).iv_ro_detail.setImageURI(Constants.TimbThumb_ImagePath+getROArrayList.getROSer);

        //  ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.VISIBLE);
        if(getROArrayList!=null) {
            if (getROArrayList.getGetRoBasicServiceDetails()!=null)
                addNearby((LinearLayout) basic_services, getROArrayList,(LinearLayout) nearby_toilet_ll_ll);

            else
                ll_basic_header.setVisibility(View.GONE);
        }
        if(getROArrayList!=null)
        {
            if (getROArrayList.getROAdditionalService()!=null && getROArrayList.getRoHasAdditionalService()==true)

                addAditionalServices((LinearLayout) ll_additional_services_content, getROArrayList);
            else
                ll_additional_header.setVisibility(View.GONE);

        }


    }
    public void addOthereServices(LinearLayout layout, ServicesPojo servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<servicesPojoArrayList.getSerOtherFacilityLists().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);

            if((i+2)<=servicesPojoArrayList.getSerOtherFacilityLists().size())
            {
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                second_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerName());

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerId())) {
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));

                }
                else {
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerUnSelectedIcon()));
                }

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerId())) {
                    second_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 1).getFacSerSelectedIcon()));
                }
                else {
                    second_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    second_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 1).getFacSerUnSelectedIcon()));
                }

                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                //  first_img.setImageURI(Uri.parse(Constants.TimbThumb_ImagePath+getROArrayList.getROAdditionalService().get(0).getServiceListObj().get(i).getSerCatImage()));
                // first_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getROAdditionalService().get(0).getServiceListObj().get(i).getSerCatIcon()));
                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerId()))
                    first_img.setImageURI(Uri.parse(Constants.ImagePath+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));
                else {
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerUnSelectedIcon()));
                }





            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }

    private void init(View v) {
        atm_facility=(LinearLayout)v.findViewById(R.id.atm_facility);
        ll_additional_header=(LinearLayout)v.findViewById(R.id.ll_additional_header);
        ll_additional_services_content=(LinearLayout)v.findViewById(R.id.ll_additional_services_content);
        basic_services=(LinearLayout)v.findViewById(R.id.basic_services);
        ll_basic_header=(LinearLayout)v.findViewById(R.id.ll_basic_header);
        restaurant_facility=(LinearLayout)v.findViewById(R.id.restaurant_facility);
        car_garage_facility=(LinearLayout)v.findViewById(R.id.car_garage_facility);
        hospital_facility=(LinearLayout)v.findViewById(R.id.hospital_facility);
        pharmacy_facility=(LinearLayout)v.findViewById(R.id.pharmacy_facility);
        toll_booth_facility=(LinearLayout)v.findViewById(R.id.toll_booth_facility);
        ll_toll_layout=(LinearLayout)v.findViewById(R.id.ll_toll_layout);
        toilet_facility=(LinearLayout)v.findViewById(R.id.toilet_facility);

        ro_details_facility=(LinearLayout)v.findViewById(R.id.ro_details_facility);
        ll_phone_ro=(LinearLayout)v.findViewById(R.id.ll_phone_ro);
        ll_email_ro=(LinearLayout)v.findViewById(R.id.ll_email_ro);
        ll_name_ro=(LinearLayout)v.findViewById(R.id.ll_name_ro);
        tv_petrol_pump_name=(TextView)v.findViewById(R.id.tv_petrol_pump_name);
        tv_petrol_contact_name=(TextView)v.findViewById(R.id.tv_petrol_contact_name);
        tv_petrol_contact_no=(TextView)v.findViewById(R.id.tv_petrol_contact_no);
        tv_petrol_email=(TextView)v.findViewById(R.id.tv_petrol_email);
        tv_petrol_pump_address=(TextView)v.findViewById(R.id.tv_petrol_pump_address);
        tv_petrol_pump_working_hrs=(TextView)v.findViewById(R.id.tv_petrol_pump_working_hrs);
      //  tv_cng=(TextView)v.findViewById(R.id.tv_cng);
       // tv_diesel=(TextView)v.findViewById(R.id.tv_diesel);
       // tv_petrol=(TextView)v.findViewById(R.id.tv_petrol);
        ll_fuels_header=(LinearLayout)v.findViewById(R.id.ll_fuels_header);
        ll_fuel_content=(LinearLayout)v.findViewById(R.id.ll_fuel_content);



        tv_pharmacy_working_hrs=(TextView)v.findViewById(R.id.tv_pharmacy_working_hrs);
        tv_pharmacy_side=(TextView)v.findViewById(R.id.tv_pharmacy_side);
        tv_pharmacy_address=(TextView)v.findViewById(R.id.tv_pharmacy_address);
        tv_pharmacy_name=(TextView)v.findViewById(R.id.tv_pharmacy_name);
        tv_pharmacy_contact_name=(TextView)v.findViewById(R.id.tv_pharmacy_contact_name);
        tv_pharmacy_contact_no=(TextView)v.findViewById(R.id.tv_pharmacy_contact_no);
        ll_contact_header_pharmacy=(LinearLayout)v.findViewById(R.id.ll_contact_header_pharmacy);
        ll_name_pharmacy=(LinearLayout)v.findViewById(R.id.ll_name_pharmacy);
        ll_phone_pharmacy=(LinearLayout)v.findViewById(R.id.ll_phone_pharmacy);



        tv_hospital_name=(TextView)v.findViewById(R.id.tv_hospital_name);
        tv_hospital_address=(TextView)v.findViewById(R.id.tv_hospital_address);
        tv_hospital_side=(TextView)v.findViewById(R.id.tv_hospital_side);
        tv_hospital_emergency_contact_no=(TextView)v.findViewById(R.id.tv_hospital_emergency_contact_no);
        tv_hospital_opd_hrs=(TextView)v.findViewById(R.id.tv_hospital_opd_hrs);
        tv_hospital_other_no=(TextView)v.findViewById(R.id.tv_hospital_other_no);
        tv_hospital_ambulance=(TextView)v.findViewById(R.id.tv_hospital_ambulance);
        tv_hospital_working_hrs=(TextView)v.findViewById(R.id.tv_hospital_working_hrs);
        ll_hospital_facility=(LinearLayout)v.findViewById(R.id.ll_hospital_facility);
        ll_emergency=(LinearLayout)v.findViewById(R.id.ll_emergency);
        ll_ambulance=(LinearLayout)v.findViewById(R.id.ll_ambulance);
        ll_other=(LinearLayout)v.findViewById(R.id.ll_other);
        ll_contact_header_hospital=(LinearLayout)v.findViewById(R.id.ll_contact_header_hospital);
        ll_facility_header=(LinearLayout)v.findViewById(R.id.ll_facility_header);

        ll_rest_cuisine_header=(LinearLayout)v.findViewById(R.id.ll_rest_cuisine_header);
        ll_rest_cuisine_category=(LinearLayout)v.findViewById(R.id.ll_rest_cuisine_category);
        ll_rest_facility_header=(LinearLayout)v.findViewById(R.id.ll_rest_facility_header);
        ll_rest_facility_content=(LinearLayout)v.findViewById(R.id.ll_rest_facility_content);
        tv_rest_name=(TextView)v.findViewById(R.id.tv_rest_name);
        rest_email=(TextView)v.findViewById(R.id.rest_email);
        tv_rest_address=(TextView)v.findViewById(R.id.tv_rest_address);
        tv_rest_side=(TextView)v.findViewById(R.id.tv_rest_side);
        tv_rest_working_hrs=(TextView)v.findViewById(R.id.tv_rest_working_hrs);
        tv_rest_ac=(TextView)v.findViewById(R.id.tv_rest_ac);
        tv_rest_contact_name=(TextView)v.findViewById(R.id.tv_rest_contact_name);
        tv_rest_contact_no=(TextView)v.findViewById(R.id.tv_rest_contact_no);
        ll_name_rest=(LinearLayout)v.findViewById(R.id.ll_name_rest);
        ll_phone_rest=(LinearLayout)v.findViewById(R.id.ll_phone_rest);
        ll_email_rest=(LinearLayout)v.findViewById(R.id.ll_email_rest);
        ll_contact_header_rest=(LinearLayout)v.findViewById(R.id.ll_contact_header_rest);


        tv_toilet_address=(TextView)v.findViewById(R.id.tv_toilet_address);
        tv_toilet_side=(TextView)v.findViewById(R.id.tv_toilet_side);
        tv_toilet_working_hrs=(TextView)v.findViewById(R.id.tv_toilet_working_hrs);
        ll_toilet_type_header=(LinearLayout)v.findViewById(R.id.ll_toilet_type_header);
        ll_toilet_type_content=(LinearLayout)v.findViewById(R.id.ll_toilet_type_content);
        ll_toilet_icon=(LinearLayout)v.findViewById(R.id.ll_toilet_icon);
        iv_toilet_icon=(SimpleDraweeView)v.findViewById(R.id.iv_toilet_icon);

        tv_atm_name=(TextView)v.findViewById(R.id.tv_atm_name);
        tv_atm_address=(TextView)v.findViewById(R.id.tv_atm_address);
        tv_atm_side=(TextView)v.findViewById(R.id.tv_atm_side);
        tv_atm_working_hrs=(TextView)v.findViewById(R.id.tv_atm_working_hrs);
        ll_email_atm=(LinearLayout)v.findViewById(R.id.ll_email_atm);
        ll_toll_free_no_atm=(LinearLayout)v.findViewById(R.id.ll_toll_free_no_atm);
        tv_atm_toll_free_no=(TextView)v.findViewById(R.id.tv_atm_toll_free_no);
        tv_atm_email=(TextView)v.findViewById(R.id.tv_atm_email);


        tv_garage_name=(TextView)v.findViewById(R.id.tv_garage_name);
        tv_garage_address=(TextView)v.findViewById(R.id.tv_garage_address);
        tv_garage_side=(TextView)v.findViewById(R.id.tv_garage_side);
        tv_garage_working_hrs=(TextView)v.findViewById(R.id.tv_garage_working_hrs);
      /*  tv_garage_contact_name=(TextView)v.findViewById(R.id.tv_garage_contact_name);
        tv_garage_contact_no=(TextView)v.findViewById(R.id.tv_garage_contact_no);
        ll_garage_vehicle_type=(LinearLayout)v.findViewById(R.id.ll_garage_vehicle_type);
        ll_garage_facility_header=(LinearLayout)v.findViewById(R.id.ll_garage_facility_header);
        ll_garage_facility_content=(LinearLayout) v.findViewById(R.id.ll_garage_facility_content);
        ll_garage_vehicle_type_content=(LinearLayout)v.findViewById(R.id.ll_garage_vehicle_type_content);*/


        //Restaurant
        ll_rest_cuisine_header=(LinearLayout)v.findViewById(R.id.ll_rest_cuisine_header);
        ll_rest_cuisine_category=(LinearLayout)v.findViewById(R.id.ll_rest_cuisine_category);
        ll_rest_facility_header=(LinearLayout)v.findViewById(R.id.ll_rest_facility_header);
        ll_rest_facility_content=(LinearLayout)v.findViewById(R.id.ll_rest_facility_content);
        tv_rest_name=(TextView)v.findViewById(R.id.tv_rest_name);
        tv_rest_address=(TextView)v.findViewById(R.id.tv_rest_address);
        tv_rest_side=(TextView)v.findViewById(R.id.tv_rest_side);
        tv_rest_working_hrs=(TextView)v.findViewById(R.id.tv_rest_working_hrs);
        tv_rest_ac=(TextView)v.findViewById(R.id.tv_rest_ac);
        tv_rest_contact_name=(TextView)v.findViewById(R.id.tv_rest_contact_name);
        tv_rest_contact_no=(TextView)v.findViewById(R.id.tv_rest_contact_no);
        rest_email=(TextView)v.findViewById(R.id.rest_email);
        tv_toilet_address=(TextView)v.findViewById(R.id.tv_toilet_address);
        tv_toilet_side=(TextView)v.findViewById(R.id.tv_toilet_side);
        ll_toilet_type_header=(LinearLayout)v.findViewById(R.id.ll_toilet_type_header);
        ll_indore=(LinearLayout)v.findViewById(R.id.ll_indore);
        ll_outdoor=(LinearLayout)v.findViewById(R.id.ll_outdoor);
        tv_both=(TextView)v.findViewById(R.id.tv_both);
        tv_indore=(TextView)v.findViewById(R.id.tv_indore);
        tv_outdoor=(TextView)v.findViewById(R.id.tv_outdoor);
        ll_veg=(LinearLayout)v.findViewById(R.id.ll_veg);
        ll_nonveg=(LinearLayout)v.findViewById(R.id.ll_nonveg);
        tv_cuisine=(TextView)v.findViewById(R.id.tv_cuisine);
        ll_foodtype=(LinearLayout)v.findViewById(R.id.ll_foodtype);
        ll_seating=(LinearLayout)v.findViewById(R.id.ll_seating);
        ll_ac=(LinearLayout)v.findViewById(R.id.ll_ac);

        tv_atm_name=(TextView)v.findViewById(R.id.tv_atm_name);
        tv_atm_address=(TextView)v.findViewById(R.id.tv_atm_address);
        tv_atm_side=(TextView)v.findViewById(R.id.tv_atm_side);
        tv_atm_working_hrs=(TextView)v.findViewById(R.id.tv_atm_working_hrs);
        tv_atm_toll_free_no=(TextView)v.findViewById(R.id.tv_atm_toll_free_no);
        iv_atm_icon=(SimpleDraweeView) v.findViewById(R.id.iv_atm_icon);
       // ll_atm_icon=(LinearLayout)v.findViewById(R.id.ll_atm_icon);



        //Garage


        tv_garage_name=(TextView)v.findViewById(R.id.tv_garage_name);
        tv_garage_address=(TextView)v.findViewById(R.id.tv_garage_address);
        tv_garage_working_hrs=(TextView)v.findViewById(R.id.tv_garage_working_hrs);
        tv_garage_person_name=(TextView)v.findViewById(R.id.tv_garage_person_name);
        tv_garage_number=(TextView)v.findViewById(R.id.tv_garage_number);
        tv_garage_side=(TextView)v.findViewById(R.id.tv_garage_side);
        ll_vehicle_type=(LinearLayout)v.findViewById(R.id.ll_vehicle_type);
        ll_type=(LinearLayout)v.findViewById(R.id.ll_type);
        ll_facilities=(LinearLayout)v.findViewById(R.id.ll_facilities);
        ll_add_garage_facilities=(LinearLayout)v.findViewById(R.id.ll_add_garage_facilities);
        ll_contact_header_garage=(LinearLayout)v.findViewById(R.id.ll_contact_header_garage);
        ll_name_garage=(LinearLayout)v.findViewById(R.id.ll_name_garage);
        ll_phone_garage=(LinearLayout)v.findViewById(R.id.ll_phone_garage);

        nearby_toilet_ll_ll=(LinearLayout)v.findViewById(R.id.nearby_toilet_ll_ll);
        nearby_toilet_ll=(LinearLayout)v.findViewById(R.id.nearby_toilet_ll);


//TollBooth
        iv_toll_icon=(SimpleDraweeView)v.findViewById(R.id.iv_toll_icon);
        tv_toll_name=(TextView)v.findViewById(R.id.tv_toll_name);
        tv_toll_address=(TextView)v.findViewById(R.id.tv_toll_address);



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity)getActivity()).iv_ro_detail.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).toolbar.setBackgroundResource(0);

        //  ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); // transperent color = #00000000


    }
    public void addNearby(LinearLayout layout,GetRO getROArrayList,final LinearLayout toilet)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<getROArrayList.getGetRoBasicServiceDetails().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);

            ImageView first_arrow=(ImageView)v.findViewById(R.id.first_arrow);
            ImageView second_arrow=(ImageView)v.findViewById(R.id.second_arrow);

            SimpleDraweeView first_img=(SimpleDraweeView) v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView) v.findViewById(R.id.second_img);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);

            if((i+2)<=getROArrayList.getGetRoBasicServiceDetails().size())
            {
                first_txt.setText(getROArrayList.getGetRoBasicServiceDetails().get(i).getROBasicSerName());
                second_txt.setText(getROArrayList.getGetRoBasicServiceDetails().get(i+1).getROBasicSerName());

                first_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getGetRoBasicServiceDetails().get(i).getROBasicSerImage()));
                second_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getGetRoBasicServiceDetails().get(i+1).getROBasicSerImage()));

                if(getROArrayList.getGetRoBasicServiceDetails().get(i).getROBasicSerName().equalsIgnoreCase("Toilet"))
                {
                    first_img.setImageResource(R.drawable.toilet_icon);
                    first_arrow.setVisibility(View.VISIBLE);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(toilet.getVisibility() == View.VISIBLE)
                            {
                                toilet.setVisibility(View.GONE);
                            }
                            else
                            {
                                toilet.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }

                if(getROArrayList.getGetRoBasicServiceDetails().get(i+1).getROBasicSerName().equalsIgnoreCase("Toilet"))
                {
                    second_img.setImageResource(R.drawable.toilet_icon);
                    second_arrow.setVisibility(View.VISIBLE);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(toilet.getVisibility() == View.VISIBLE)
                            {
                                toilet.setVisibility(View.GONE);
                            }
                            else
                            {
                                toilet.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(getROArrayList.getGetRoBasicServiceDetails().get(i).getROBasicSerName());
                first_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getGetRoBasicServiceDetails().get(i).getROBasicSerImage()));

                if(getROArrayList.getGetRoBasicServiceDetails().get(i).getROBasicSerName().equalsIgnoreCase("Toilet"))
                {
                    first_img.setImageResource(R.drawable.toilet_icon);
                    first_img.setVisibility(View.VISIBLE);
                    first_arrow.setVisibility(View.VISIBLE);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(toilet.getVisibility() == View.VISIBLE)
                            {
                                toilet.setVisibility(View.GONE);
                            }
                            else
                            {
                                toilet.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }



            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }

    public void addAditionalServices(LinearLayout layout,GetRO getROArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<getROArrayList.getROAdditionalService().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);

            if((i+2)<=getROArrayList.getROAdditionalService().size())
            {
                first_txt.setText(getROArrayList.getROAdditionalService().get(i).getSerCatName());
                second_txt.setText(getROArrayList.getROAdditionalService().get(i+1).getSerCatName());

                first_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getROAdditionalService().get(i).getSerCatIcon()));
                second_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getROAdditionalService().get(i+1).getSerCatIcon()));
                i++;
            }
            else
            {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(getROArrayList.getROAdditionalService().get(i).getSerCatName());
                //  first_img.setImageURI(Uri.parse(Constants.TimbThumb_ImagePath+getROArrayList.getROAdditionalService().get(i).getSerCatImage()));
                first_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getROAdditionalService().get(i).getSerCatIcon()));





            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }

    public void addFacility(LinearLayout layout,ServicesPojo servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<servicesPojoArrayList.getSerOtherFacilityLists().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);

            if((i+2)<=servicesPojoArrayList.getSerOtherFacilityLists().size())
            {
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                second_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerName());

                first_img.setImageURI(Uri.parse(Constants.ImagePath+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));
                second_img.setImageURI(Uri.parse(Constants.ImagePath+servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerSelectedIcon()));
                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                //  first_img.setImageURI(Uri.parse(Constants.TimbThumb_ImagePath+getROArrayList.getROAdditionalService().get(0).getServiceListObj().get(i).getSerCatImage()));
                first_img.setImageURI(Uri.parse(Constants.ImagePath+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));





            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }
    public void addGarageType(LinearLayout layout, ServicesPojo servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<servicesPojoArrayList.getGarageWheelerTypesLists().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);

            if((i+2)<=servicesPojoArrayList.getGarageWheelerTypesLists().size())
            {
                first_txt.setText(servicesPojoArrayList.getGarageWheelerTypesLists().get(i).getGarageWheelerTypeName());
                second_txt.setText(servicesPojoArrayList.getGarageWheelerTypesLists().get(i+1).getGarageWheelerTypeName());

                if(servicesPojoArrayList.getGarageWheelerType()!=null && servicesPojoArrayList.getGarageWheelerType().contains(""+servicesPojoArrayList.getGarageWheelerTypesLists().get(i).getGarageWheelerTypeID())) {
                    first_img.setImageResource(R.drawable.two_wheeler_icon_1);
                    first_txt.setTextColor(getResources().getColor(android.R.color.black));

                }
                else {
                    first_img.setImageResource(R.drawable.two_wheeler_icon);
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));

                }

                if(servicesPojoArrayList.getGarageWheelerType()!=null && servicesPojoArrayList.getGarageWheelerType().contains(""+servicesPojoArrayList.getGarageWheelerTypesLists().get(i+1).getGarageWheelerTypeID())) {

                    second_img.setImageResource(R.drawable.four_wheeler_icon_1);
                    second_txt.setTextColor(getResources().getColor(R.color.graycolor));
                }
                else {
                    second_img.setImageResource(R.drawable.four_wheeler_icon);
                    second_txt.setTextColor(getResources().getColor(android.R.color.black));
                }
                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.getGarageWheelerTypesLists().get(i).getGarageWheelerTypeName());
                if(servicesPojoArrayList.getGarageWheelerType()!=null && servicesPojoArrayList.getGarageWheelerType().contains(""+servicesPojoArrayList.getGarageWheelerTypesLists().get(i).getGarageWheelerTypeID())) {
                    first_img.setImageResource(R.drawable.two_wheeler_icon_1);
                    first_txt.setTextColor(getResources().getColor(android.R.color.black));

                }
                else {
                    first_img.setImageResource(R.drawable.two_wheeler_icon);
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));

                }




            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }

    public void addOthereServicesThree(LinearLayout layout, ServicesPojo servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<servicesPojoArrayList.getSerOtherFacilityLists().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter_three_call, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);
            LinearLayout three_ll=(LinearLayout)v.findViewById(R.id.third_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);
            SimpleDraweeView three_img=(SimpleDraweeView)v.findViewById(R.id.third_img);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);
            TextView three_txt=(TextView)v.findViewById(R.id.third_txt);

            if((i+3)<=servicesPojoArrayList.getSerOtherFacilityLists().size())
            {
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                second_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerName());
                three_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i+2).getFacSerName());

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerId())) {
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));

                }
                else {
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerUnSelectedIcon()));
                }

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerId())) {
                    second_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 1).getFacSerSelectedIcon()));
                }
                else {
                    second_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    second_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 1).getFacSerUnSelectedIcon()));
                }

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i+2).getFacSerId())) {
                    three_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 2).getFacSerSelectedIcon()));
                }
                else {
                    three_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    three_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 2).getFacSerUnSelectedIcon()));
                }

                i+=2;
            }
            else if((i+2)<=servicesPojoArrayList.getSerOtherFacilityLists().size())
            {
                three_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                second_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerName());

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerId())) {
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));

                }
                else {
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerUnSelectedIcon()));
                }

                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i+1).getFacSerId())) {
                    second_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 1).getFacSerSelectedIcon()));
                }
                else {
                    second_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    second_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i + 1).getFacSerUnSelectedIcon()));
                }

                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerName());
                //  first_img.setImageURI(Uri.parse(Constants.TimbThumb_ImagePath+getROArrayList.getROAdditionalService().get(0).getServiceListObj().get(i).getSerCatImage()));
                // first_img.setImageURI(Uri.parse(Constants.ImagePath+getROArrayList.getROAdditionalService().get(0).getServiceListObj().get(i).getSerCatIcon()));
                if(servicesPojoArrayList.getSerOtherFacility().contains(""+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerId()))
                    first_img.setImageURI(Uri.parse(Constants.ImagePath+servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerSelectedIcon()));
                else {
                    first_txt.setTextColor(getResources().getColor(R.color.graycolor));
                    first_img.setImageURI(Uri.parse(Constants.ImagePath + servicesPojoArrayList.getSerOtherFacilityLists().get(i).getFacSerUnSelectedIcon()));
                }



            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }
    public void addRoFuelTypes(LinearLayout layout, ArrayList<String> rofueltypes)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<rofueltypes.size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter_three_call, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);
            LinearLayout three_ll=(LinearLayout)v.findViewById(R.id.third_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            first_img.setVisibility(View.GONE);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);
            second_img.setVisibility(View.GONE);
            SimpleDraweeView three_img=(SimpleDraweeView)v.findViewById(R.id.third_img);
            three_img.setVisibility(View.GONE);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);
            TextView three_txt=(TextView)v.findViewById(R.id.third_txt);
            if((i+3)<=rofueltypes.size())
            {
                first_txt.setText(rofueltypes.get(i));
                second_txt.setText(rofueltypes.get(i+1));
                three_txt.setText(rofueltypes.get(i+2));



                i+=2;
            }
            else if((i+2)<=rofueltypes.size())
            {
                three_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(rofueltypes.get(i));
                second_txt.setText(rofueltypes.get(i+1));



                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(rofueltypes.get(i));
            }
            Log.d("System out","value of i after add value "+i);

            layout.addView(v);

        }

    }
    public void getImage(String name, final DraweeView img) {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id="1";




        json = "[{     \"keyName\": \"" + name + "\",     \"value\": \""+""+"\" }]";
        Log.d("System out", "Explore Destination from nearby____" + json);



        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ServiceImage> call = apiService.GetValue(json);
        call.enqueue(new Callback<ServiceImage>() {
            @Override
            public void onResponse(Call<ServiceImage> call, Response<ServiceImage> response) {


//                pb_dialog.dismiss();
                pb_dialog.dismiss();
                if (response.body() != null) {



                    serviceImageArrayList=response.body();
                    if (!serviceImageArrayList.getValue().equalsIgnoreCase(""))
                    {
                        Log.d("System out","Response of image set "+Constants.ImagePath+serviceImageArrayList.getValue());
                        img.setImageURI(Uri.parse(Constants.ImagePath+serviceImageArrayList.getValue()));
                        // Collections.reverse(exploreDestList);
                    }
                    else
                    {
                        //Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResDescription(), error_layout);
                    }
                }
            }
            @Override
            public void onFailure(Call<ServiceImage> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    public void addOthereToiletThree(LinearLayout layout ,GetRO servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<servicesPojoArrayList.getToiletTypesAllArrayList().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter_three_call, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);
            LinearLayout three_ll=(LinearLayout)v.findViewById(R.id.third_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);
            SimpleDraweeView three_img=(SimpleDraweeView)v.findViewById(R.id.third_img);

            ImageView simple_first_img=(ImageView)v.findViewById(R.id.simple_first_img);
            ImageView simple_second_img=(ImageView)v.findViewById(R.id.simple_second_img);
            ImageView simple_three_img=(ImageView)v.findViewById(R.id.simple_third_img);

            first_img.setVisibility(View.GONE);
            second_img.setVisibility(View.GONE);
            three_img.setVisibility(View.GONE);

            simple_first_img.setVisibility(View.VISIBLE);
            simple_second_img.setVisibility(View.VISIBLE);
            simple_three_img.setVisibility(View.VISIBLE);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);
            TextView three_txt=(TextView)v.findViewById(R.id.third_txt);

            if((i+3)<=servicesPojoArrayList.getToiletTypesAllArrayList().size())
            {
                first_txt.setText(servicesPojoArrayList.getToiletTypesAllArrayList().get(i).getToiletName());
                second_txt.setText(servicesPojoArrayList.getToiletTypesAllArrayList().get(i+1).getToiletName());
                three_txt.setText(servicesPojoArrayList.getToiletTypesAllArrayList().get(i+2).getToiletName());
                simple_first_img.setImageResource(R.drawable.round_black);
                simple_second_img.setImageResource(R.drawable.round_black);
                simple_three_img.setImageResource(R.drawable.round_black);
                if(!servicesPojoArrayList.getROToilet().contains(""+servicesPojoArrayList.getToiletTypesAllArrayList().get(i).getToiletID())) {

                    first_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_first_img.setImageResource(R.drawable.round_grey);
                }
                if(!servicesPojoArrayList.getROToilet().contains(""+servicesPojoArrayList.getToiletTypesAllArrayList().get(i+1).getToiletID())) {

                    second_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_second_img.setImageResource(R.drawable.round_grey);
                }
                if(!servicesPojoArrayList.getROToilet().contains(""+servicesPojoArrayList.getToiletTypesAllArrayList().get(i+2).getToiletID())) {

                    three_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_three_img.setImageResource(R.drawable.round_grey);
                }
                i+=2;
            }
            else if((i+2)<=servicesPojoArrayList.getToiletTypesAllArrayList().size())
            {
                three_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.getToiletTypesAllArrayList().get(i).getToiletName());
                second_txt.setText(servicesPojoArrayList.getToiletTypesAllArrayList().get(i+1).getToiletName());
                simple_first_img.setImageResource(R.drawable.round_black);
                simple_second_img.setImageResource(R.drawable.round_black);

                if(!servicesPojoArrayList.getROToilet().contains(""+servicesPojoArrayList.getToiletTypesAllArrayList().get(i).getToiletID())) {

                    first_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_first_img.setImageResource(R.drawable.round_grey);
                }
                if(!servicesPojoArrayList.getROToilet().contains(""+servicesPojoArrayList.getToiletTypesAllArrayList().get(i+1).getToiletID())) {

                    second_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_second_img.setImageResource(R.drawable.round_grey);
                }


                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                three_ll.setVisibility(View.INVISIBLE);
                simple_first_img.setImageResource(R.drawable.round_black);
                first_txt.setText(servicesPojoArrayList.getToiletTypesAllArrayList().get(i).getToiletName());



                if(!servicesPojoArrayList.getROToilet().contains(""+servicesPojoArrayList.getToiletTypesAllArrayList().get(i).getToiletID())) {

                    first_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_first_img.setImageResource(R.drawable.round_grey);
                }




            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }
    public void AddToiletTypes(LinearLayout layout,ServicesPojo servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        Log.d("System out"," servicesPojoArrayList toilet types "+servicesPojoArrayList.getGetToiletTypesList().size());
        for(int i=0;i<servicesPojoArrayList.getGetToiletTypesList().size();i++)
        {

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.toilet_types_cell, null);
           TextView tv_toilet_type=(TextView)v.findViewById(R.id.tv_toilet_type);
            ImageView iv_round_toilet=(ImageView)v.findViewById(R.id.iv_round_toilet);
            tv_toilet_type.setText(servicesPojoArrayList.getGetToiletTypesList().get(i).getToiletName());
            if(!servicesPojoArrayList.getToiFeature().contains(servicesPojoArrayList.getGetToiletTypesList().get(i).getToiletID().toString()))
            {
                tv_toilet_type.setTextColor(Color.GRAY);
                iv_round_toilet.setImageResource(R.drawable.round_grey);
            }

            layout.addView(v);

        }

    }

}
