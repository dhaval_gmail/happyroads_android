package com.bpcl.happyroads;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Pojo.CalculateDistance;
import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Poi_Night;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.Direction;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.GetRO;
import com.bpcl.happyroads.Pojo.Insert_Poi;
import com.bpcl.happyroads.Pojo.ListDefaultValues;
import com.bpcl.happyroads.Pojo.ListServiceCategory;
import com.bpcl.happyroads.Pojo.Place;
import com.bpcl.happyroads.Pojo.RemoveTripPoi;
import com.bpcl.happyroads.Pojo.RoutePojo;
import com.bpcl.happyroads.Pojo.ServicesPojo;
import com.bpcl.happyroads.Pojo.TollBooth;
import com.bpcl.happyroads.Pojo.UpdateTrip;
import com.bpcl.happyroads.Utils.CircleImageView;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DirectionsJSONParser;
import com.bpcl.happyroads.Utils.GPSTracker;
import com.bpcl.happyroads.Utils.ImageLoadProgressBar;
import com.bpcl.happyroads.Utils.ImageSaver;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ADMIN on 8/30/2016.
 */
public class RoutePlanningFragment extends Fragment implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener, OnMapReadyCallback {

    static final com.mapbox.mapboxsdk.geometry.LatLng source = new com.mapbox.mapboxsdk.geometry.LatLng(23.034617, 72.501813);
    static final com.mapbox.mapboxsdk.geometry.LatLng destination = new com.mapbox.mapboxsdk.geometry.LatLng(23.1138, 72.5412);
    static boolean isnight = false;
    private static int count = 0;
    private static int count1 = 0;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 125;
    public ImageLoader imageLoader = ImageLoader.getInstance();
    public String select_dpd_type = "";
    public Boolean update_start_date_resume = false;
    public Direction route_direction;
    NavigationView left_navView;
    DrawerLayout drawer_layout_services;
    ImageView iv_bottom_up_arrow, iv_dotted_line_bottom;
    LinearLayout ll_bottom_up, ll_places_between_S_to_D_on_trip_tracking, ll_left_arrow_map, ll_new_trip, ll_on_tracking;
    LinearLayout ll_places_between_S_to_D, ll_S_to_D_on_trip_tracking, ll_plan_at_D_on_trip_tracking, ll_D_to_S_on_trip_tracking;
    TextView tv_S_to_D_tab_line, tv_S_to_D, tv_plan_at_D, tv_plan_at_D_tab_line, tv_D_to_S, tv_D_to_S_tab_line;
    TextView iv_left_arrow_map,iv_left_arrow_map_shut;
    Gson gson = new Gson();
    String name = "";
    ImageView iv_map_my_plan;
    SupportMapFragment mMapFragment;
    ImageView iv_left_arrow_hsv, iv_right_arrow_hsv;
    ViewGroup parent;
    TextView tv_D_to_S_from_left_drawer, tv_good_to_go_map,tv_D_to_S_from_left_drawer1;
    LinearLayout ll_D_to_S_from_left_drawer;
    int i;
    Marker marker;
    int currIndex = 0;
    GoogleMap googleMap;
    HorizontalScrollView hsv;
    ExploreActivity exploreActivity = null;
    FrameLayout fl_pop_up_zoo;
    ImageView iv_place_location;
    TextView tv_explore_from_map;
    LinearLayout ll_facility_content;
    int position = 3;
    boolean tripFlag = false;
    String select = "";
    //Trip Plan
    DatePickerDialog dpd;
    TimePickerDialog tpd, tpd1;
    Calendar now;
    String date;
    String date1 = null;
    /*   Display display;
       mWidth= display.getWidth(); // deprecated
       viewWidth= mWidth/ 3;*/
    LinearLayout ll_route_plan, ll_close_pop_up_map;
    TextView tv_start_date_my_return_trip, tv_start_time_my_return_trip;
    TextView tv_origin, tv_destination, tv_start_date, tv_start_time, tv_end_date, tv_end_time, tv_trip_distance_time, tv_add_poi, tv_services_details,tv_add,tv_edit;
    AutoCompleteTextView tv_place_of_des;
    DBHandler dbHandler;
    List<Create_Trip> createTripList = new ArrayList<>();
    List<Create_Poi> createPoiList = new ArrayList<>();
    String trip_start_date_time = "";
    Integer avgSpeed = 60;
    CircleImageView iv_circle_source, iv_circle_destination;
    Integer tripid = -1;
    Dialog pb_dialog;
    GoogleMap map = null;
    android.os.Handler mHandler = null;
    Runnable mRunnable = null;
    GoogleMap mapView;
    LatLngBounds.Builder b1,desbound;
    Double Latitute = 0.0;
    Double Longitute = 0.0;
    GPSTracker gpsTracker;
    TextView tv_poi_map_name, tv_poi_map_des, tv_poi_map_distance;
    SimpleDraweeView fc_poi_map_image;
    ImageView iv_close_poi_popup;
    Integer orginId, destinationId;
    SessionManager sessionManager;
    Icon icon_des;
    List<ListServiceCategory> listServiceCategoryList;
    List<List<HashMap<String, String>>> routelineresult;
    LinearLayout llserviceslayout;
    TextView tv_services_location, tv_services_side;
    Integer selected_poi_pos = -1;
    Integer selected_ro_pos = -1;
    Integer selected_ser_pos = -1;
    Integer selected_des_pos = -1;
    RecyclerView recycleview_cat_list;
    Service_List_Adapter serviceListAdapter;
    LinearLayoutManager horizontalLayoutManagaer;
    TollBoothAdapter toolBooth_list_adapter;
    Gallery galleryservicelist;
    Integer current_pos = 0;
    Double orginlat = 0.0, originlong = 0.0, deslat = 0.0, deslong = 0.0;
    ArrayAdapter<String> adapter;
    ArrayList<String> placeList;
    ArrayList<ExplorePOI> explorePoiList;
    Integer category_select_pos = -1;
    TextView tv_resume_date_my_trip, tv_resume_time_my_trip;
    Integer night_poi_id = -1;
    List<Create_Poi_Night> create_poi_nightsList;
    Boolean return_trip_plan = false;
    Integer return_plan = 0;
    RelativeLayout error_layout;
    TextView  iv_tollbooth;
    String from = "";
    LinearLayout ll_top_header_plan;
    ImageView iv_edit_stay_time;
    Boolean edit_stay_time = false;
    Integer S_To_D_Plan = 0;
    ArrayList<TollBooth> tollBoothArrayList = new ArrayList<>();
    ImageView iv_map_call;
    LinearLayout ll_des_plan;
    List<DestinationPlan> destinationplanList = new ArrayList<>();
    Double dayhours = 8.0;
    Double starttime = 7.0;
    ArrayList<Integer> noday;
    ArrayList<DestinationPlan> tempdestiDestinationPlanlist;
    boolean editIconUnselected = false;
    LinearLayout ll_add_places_to_plan;
    boolean desplan = false;
    String plantype = "";
    String frommytrips = "";
    String default_EmergencyNumber = "";
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM ,yyyy");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    List<Create_Trip> tempcreateTripList = new ArrayList<>();
    List<Create_Poi> tempcreatePoiList = new ArrayList<>();
    TextView tv_plan_my_return_trip;
    ImageView iv_poi_night_stay;
    View v;
    Double remaining_km = 0.0;
    Double remaining_time = 0.0;
    private DisplayImageOptions options;
    private ImageView dropPinView;
    private ImageButton clearDisplayViewButton;
    private ArrayList<RoutePojo> routepojo = null;
    private List<ServicesPojo> services = null;
    private List<RoutePojo.OnAdminRoutePoiList> onAdminRoutePoiLists = null;
    private List<RoutePojo.OnAdminRoutePoiList> onAdminRouteRoLists = null;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private int main_pos;
    private int main_pos1;
    private EditText setValue1;
    private String url = "";  //draw route url
    private String wayPoints = "";
    private String wayPoints_server="";

    Double poi_latitude = 0.0, poi_longitude = 0.0, stay_hrs = 0.0;
    private int delete_pos;
    Polyline polylineFinal;
    PolylineOptions polylineOptions;
    Direction old_direction_response;

    Integer selected_map_icon_size=60;
    Integer unselected_map_icon_size=40;
    LinearLayout ll_left_arrow_hsv,ll_right_arrow_hsv;
    String temptime="19:00";
     boolean add=false;
    int a,b;


    public static boolean CheckDates(String startDate, String endDate) {


        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = false;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare", String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }
    public static boolean CheckDates1(String startDate, String endDate) {


        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare", String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare", e.toString());
        }
        return b;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // LayoutInflater inflater=(LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (v == null) {
            v = inflater.inflate(R.layout.route_planning_fragment, container, false);
        }
        ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).toolbar.setBackgroundResource(R.drawable.black_bg_home);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_top_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExploreActivity)getActivity()).onBackPressed();
            }
        });

        if (getArguments() != null) {
            plantype = getArguments().getString("plantype", "");
            frommytrips = getArguments().getString("from", "");
        }
        dbHandler = new DBHandler(getActivity());
        tollBoothArrayList = new ArrayList<>();
        pb_dialog = new Dialog(getActivity());
        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        b1 = new LatLngBounds.Builder();
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        if (getArguments() != null)
            tripid = getArguments().getInt("tripId", -1);
        listServiceCategoryList = new ArrayList<>();
        if (tripid == -1) {
            tripid = dbHandler.get_first_row_tripId();

        }

        //  dbHandler.delete_All_Destination_ById(tripid);

        edit_stay_time = false;
        now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),

                now.get(Calendar.DAY_OF_MONTH)
        );
        tpd = TimePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );
        tpd1 = TimePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE), true
        );
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            name = getArguments().getString("from", "");
        } else {
            name = "";
        }
        sessionManager = new SessionManager(getActivity());

      //  ll_facility_content = (LinearLayout) v.findViewById(R.id.ll_facility_content);
        left_navView = (NavigationView) v.findViewById(R.id.left_navView);

        fl_pop_up_zoo = (FrameLayout) v.findViewById(R.id.fl_pop_up_zoo);
        iv_place_location=(ImageView)v.findViewById(R.id.iv_place_location);

        iv_map_my_plan = (ImageView) v.findViewById(R.id.iv_map_my_plan);

        tv_explore_from_map = (TextView) v.findViewById(R.id.tv_explore_from_map);


        drawer_layout_services = (DrawerLayout) v.findViewById(R.id.drawer_layout_route);
        ll_bottom_up = (LinearLayout) v.findViewById(R.id.ll_bottom_up);
        ll_places_between_S_to_D = (LinearLayout) v.findViewById(R.id.ll_places_between_S_to_D);
        ll_left_arrow_map = (LinearLayout) v.findViewById(R.id.ll_left_arrow_map);
        ll_new_trip = (LinearLayout) v.findViewById(R.id.ll_new_trip);
        ll_route_plan = (LinearLayout) v.findViewById(R.id.ll_route_plan);
        ll_close_pop_up_map = (LinearLayout) v.findViewById(R.id.ll_close_pop_up_map);
        // ll_places_between_S_to_D_on_trip_tracking=(LinearLayout)v.findViewById(R.id.ll_places_between_S_to_D_start_tracking);
        ll_S_to_D_on_trip_tracking = (LinearLayout) v.findViewById(R.id.ll_S_to_D_on_trip_tracking);
        ll_D_to_S_on_trip_tracking = (LinearLayout) v.findViewById(R.id.ll_D_to_S_on_trip_tracking);
        ll_plan_at_D_on_trip_tracking = (LinearLayout) v.findViewById(R.id.ll_plan_at_D_on_trip_tracking);
        tv_S_to_D_tab_line = (TextView) v.findViewById(R.id.tv_S_to_D_tab_line);
        tv_S_to_D = (TextView) v.findViewById(R.id.tv_S_to_D);
        tv_plan_at_D = (TextView) v.findViewById(R.id.tv_plan_at_D);
        tv_plan_at_D_tab_line = (TextView) v.findViewById(R.id.tv_plan_at_D_tab_line);
        tv_D_to_S = (TextView) v.findViewById(R.id.tv_D_to_S);
        tv_D_to_S_tab_line = (TextView) v.findViewById(R.id.tv_D_to_S_tab_line);
        tv_D_to_S_from_left_drawer = (TextView) v.findViewById(R.id.tv_D_to_S_from_left_drawer);
        ll_D_to_S_from_left_drawer = (LinearLayout) v.findViewById(R.id.ll_D_to_S_from_left_drawer);
        tv_D_to_S_from_left_drawer1 = (TextView) v.findViewById(R.id.tv_D_to_S_from_left_drawer1);
        tv_good_to_go_map = (TextView) v.findViewById(R.id.tv_good_to_go_map);
        iv_left_arrow_hsv = (ImageView) v.findViewById(R.id.iv_left_arrow_hsv);
        iv_right_arrow_hsv = (ImageView) v.findViewById(R.id.iv_right_arrow_hsv);
        llserviceslayout = (LinearLayout) v.findViewById(R.id.ll_services);
        tv_services_location = (TextView) v.findViewById(R.id.tv_service_location);
        tv_services_side = (TextView) v.findViewById(R.id.tv_service_side);
        error_layout = (RelativeLayout) v.findViewById(R.id.error_layout);
        tv_start_date = (TextView) v.findViewById(R.id.tv_start_date);
        tv_start_time = (TextView) v.findViewById(R.id.tv_start_time);
        tv_end_date = (TextView) v.findViewById(R.id.tv_end_date);
        tv_end_time = (TextView) v.findViewById(R.id.tv_end_time);
        tv_origin = (TextView) v.findViewById(R.id.tv_origin);
        tv_destination = (TextView) v.findViewById(R.id.tv_destination);
        tv_trip_distance_time = (TextView) v.findViewById(R.id.tv_trip_distance_time);
        tv_add_poi = (TextView) v.findViewById(R.id.add_poi);
        tv_edit = (TextView) v.findViewById(R.id.tv_edit);
        tv_add = (TextView) v.findViewById(R.id.tv_add);
        tv_services_details = (TextView) v.findViewById(R.id.tv_services_details);
        iv_circle_source = (CircleImageView) v.findViewById(R.id.iv_circle_source);
        iv_circle_destination = (CircleImageView) v.findViewById(R.id.iv_circle_destination);
        tv_place_of_des = (AutoCompleteTextView) v.findViewById(R.id.tv_place_of_des);
        iv_tollbooth = (TextView) v.findViewById(R.id.iv_tollbooth);
        ll_des_plan = (LinearLayout) v.findViewById(R.id.ll_des_plan);
        FragmentManager myFM = getActivity().getSupportFragmentManager();

        LinearLayout topLinearLayout = new LinearLayout(getActivity());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        fc_poi_map_image = (SimpleDraweeView) v.findViewById(R.id.fc_poi);
        tv_poi_map_name = (TextView) v.findViewById(R.id.tv_poi_name);
        tv_poi_map_des = (TextView) v.findViewById(R.id.tv_poi_des);
        tv_poi_map_distance = (TextView) v.findViewById(R.id.tv_distance_time);
        iv_close_poi_popup = (ImageView) v.findViewById(R.id.iv_close_poi_popup);
        recycleview_cat_list = (RecyclerView) v.findViewById(R.id.recycleview_cat_list);

        ll_top_header_plan = (LinearLayout) v.findViewById(R.id.ll_top_header_plan);
        iv_edit_stay_time = (ImageView) v.findViewById(R.id.iv_edit_stay_time);
        iv_map_call = (ImageView) v.findViewById(R.id.iv_map_call);
        //  recycleview_cat_list.RecyclerViewDisabler ();
        recycleview_cat_list.setNestedScrollingEnabled(false);

        serviceListAdapter = new Service_List_Adapter();
        //((ExploreActivity) getActivity()).ll_header_main.setEnabled(false);

        iv_bottom_up_arrow = (ImageView) v.findViewById(R.id.iv_bottom_up_arrow);
        iv_left_arrow_map_shut = (TextView) v.findViewById(R.id.iv_left_arrow_map_shut);
        iv_left_arrow_map = (TextView) v.findViewById(R.id.iv_left_arrow_map);
       // iv_left_arrow_map.setRotation(90);


       // drawer_layout_services.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, left_navView);
        drawer_layout_services.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, left_navView);
      ActionBarDrawerToggle  mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawer_layout_services,((ExploreActivity)getActivity()).toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                getActivity().supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
               getActivity().supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                ll_bottom_up.setVisibility(View.GONE);
                iv_bottom_up_arrow.setVisibility(View.VISIBLE);


            }
        };
        drawer_layout_services.setDrawerListener(mDrawerToggle);



        ll_left_arrow_hsv = (LinearLayout) v.findViewById(R.id.ll_left_arrow_hsv);
        ll_right_arrow_hsv = (LinearLayout) v.findViewById(R.id.ll_right_arrow_hsv);
        if(ExploreActivity.drawerleft==true)
        {
            ExploreActivity.drawerleft=false;

            DrawerMovement("bottom");

        }else
        {
            DrawerMovement("left");

        }
        ((ExploreActivity) getActivity()).ivMenuHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("System out","menu clicked in route planning____");
                if(drawer_layout_services.isDrawerOpen(left_navView)==true)
                {
                    drawer_layout_services.closeDrawers();
                    ((ExploreActivity)getActivity()).drawerLayout.openDrawer(((ExploreActivity)getActivity()).navigationView);
                }else
                {
                    ((ExploreActivity)getActivity()).drawerLayout.openDrawer(((ExploreActivity)getActivity()).navigationView);

                }

                ll_bottom_up.setVisibility(View.GONE);
            }
        });

    //  DrawerMovement("left");



        setMapAsPerNavigate();



        destinationplanList = dbHandler.get_DestinationList(tripid);

        createTripList = new ArrayList<>();
        createTripList = dbHandler.get_TRIP_By_TripId(tripid);
        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(add==false) {
                    tv_add.setTextColor(getResources().getColor(R.color.textYellow));
                    tv_edit.setTextColor(getResources().getColor(R.color.colorWhite));

                    DrawerMovement("bottom");
                    add=true;
                }else
                {
                    tv_add.setTextColor(getResources().getColor(R.color.colorWhite));
                    DrawerMovement("left");
                    add=false;
                }
            }
        });


        tv_place_of_des.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Constants.hidekeyboard(getActivity(), tv_place_of_des);
                if(tv_place_of_des.getText().toString().length()>1)
                    getlatlongfromplace(tv_place_of_des.getText().toString());

            }
        });

        tv_place_of_des.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE) {
                    //do Whatever you Want to do
                    if(tv_place_of_des.getText().toString().length()>1)
                    getlatlongfromplace(tv_place_of_des.getText().toString());
                }
                return true;
            }
        });





        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (createTripList.size() > 0)
        {
            orginId = createTripList.get(0).getSourceId();
            destinationId = createTripList.get(0).getDestinationId();
            orginlat = createTripList.get(0).getSource_latitude();
            originlong = createTripList.get(0).getSource_longitude();
            deslat = createTripList.get(0).getDestination_latitude();
            deslong = createTripList.get(0).getDestination_longitude();
            tv_S_to_D.setText(createTripList.get(0).getSource() + "-" + createTripList.get(0).getDestination());
            tv_plan_at_D.setText("Plan at " + createTripList.get(0).getDestination());
            tv_D_to_S.setText(createTripList.get(0).getDestination() + "-" + createTripList.get(0).getSource());

            tv_origin.setText(createTripList.get(0).getSource());
            tv_destination.setText(createTripList.get(0).getDestination());

            get_my_trip();
            set_header_origin_destination();



        } else {
            left_navView.setVisibility(View.GONE);

            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
        }



        ll_left_arrow_hsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv_left_arrow_hsv.performClick();
            }
        });
          ll_right_arrow_hsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv_right_arrow_hsv.performClick();
            }
        });
        //tv_place_of_des.setEnabled(false);
        tv_place_of_des.setEnabled(true);


        old_direction_response=new Direction();
        iv_left_arrow_map_shut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.hideSoftKeyBoardOnTabClicked(getActivity(), v);
                drawer_layout_services.closeDrawer(left_navView);
                ll_bottom_up.setVisibility(View.VISIBLE);
                iv_bottom_up_arrow.setVisibility(View.VISIBLE);
                //  iv_left_arrow_map_shut.setImageResource(R.drawable.map_arrow_right_slider);
            }
        });

        tv_place_of_des.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                   Constants.hidekeyboard(getActivity(),v);
                }
            }
        });
        tv_explore_from_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (createTripList.size() > 0) {
                    Constants.frommapplan = "explorefrommap";

                    Bundle b = new Bundle();
                    b.putInt("tripId", tripid);
                    b.putString("from", "routeplan");
                    b.putString("destID", createTripList.get(0).getDestinationId().toString());
                    b.putString("destLat", createTripList.get(0).getDestination_latitude().toString());
                    b.putString("destLong", createTripList.get(0).getDestination_longitude().toString());

                    b.putString("placeName", createTripList.get(0).getDestination().toString());

                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new ExplorePlacesFragment(), b);
                }
            }
        });

        ll_close_pop_up_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                fl_pop_up_zoo.startAnimation(bottomUpDown);
                fl_pop_up_zoo.setVisibility(View.GONE);
            }
        });
        ll_D_to_S_from_left_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_D_to_S_from_left_drawer.performClick();
            }
        });


        tv_D_to_S_from_left_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!createTripList.get(0).getTripstatus().contains("Saved") && createTripList.get(0).getReturn_start_date().length() == 0 && return_trip_plan == false) {

                    final Dialog myTripDialog = new Dialog(getActivity());

                    myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    myTripDialog.setContentView(R.layout.plan_trip_from_to_popup);
                    final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
                    final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
                    tv_start_date_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_return_trip);
                    tv_start_time_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_time_my_return_trip);

                    TextView tv_source = (TextView) myTripDialog.findViewById(R.id.tv_source);
                    TextView tv_des = (TextView) myTripDialog.findViewById(R.id.tv_des);

                    String date = "", time = "";


                    dpd = DatePickerDialog.newInstance(
                            RoutePlanningFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );


                    tpd = TimePickerDialog.newInstance(
                            RoutePlanningFragment.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE), false
                    );


                    tv_source.setText(createTripList.get(0).getDestination());
                    tv_des.setText(createTripList.get(0).getSource());


                    //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");


                    createTripList.clear();
                    createTripList = dbHandler.get_TRIP_By_TripId(tripid);

                    String arrival_date = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime();



                    if (destinationplanList.size() > 0)
                    {

                     Calendar    calendar = Calendar.getInstance();
                        try {
                            calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Double[] des_plan_date_time_eta = get_no_days_destination_plan();

                        calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

                        calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
                        calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

                        arrival_date = sdf.format(calendar.getTime());


                        Log.d("resumetimedparrival_hrs", des_plan_date_time_eta[1].intValue() + " min " + des_plan_date_time_eta[2].intValue());


                    }
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(sdf.parse(arrival_date));
                        calendar.add(Calendar.MINUTE,5);
                        arrival_date=sdf.format(calendar.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    Log.d("resumetimedparrival", arrival_date);

                    try {
                        tv_start_date_my_return_trip.setText(Constants.formatDate(String.valueOf(arrival_date), "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy"));
                        tv_start_time_my_return_trip.setText(Constants.formatDate(String.valueOf(arrival_date), "dd MMM ,yyyy HH:mm", "hh:mm a"));
                    } catch (ParseException e) {
                        e.printStackTrace();

                    }
                    select_dpd_type = "return";
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");
                    Date date1 = null;
                    try {
                        date1 = sdf1.parse(arrival_date);
                        Calendar calendar1 = Calendar.getInstance();
                        calendar1.setTime(date1);

                        dpd.setMinDate(calendar1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    tv_start_date_my_return_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //select="start";
                            select_dpd_type = "return";
                            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


                        }

                    });


                    tv_start_time_my_return_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //  select="stop";
                            select_dpd_type = "return";

                            tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");

                        }
                    });

                    TextView tv_plan_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_plan_my_return_trip);
                    final String finalArrival_date = arrival_date;
                    tv_plan_my_return_trip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String start_date = "", end_time = "";

                            try {
                                start_date = tv_start_date_my_return_trip.getText().toString();
                                end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Log.d("System out","finalArrival_date and  start-end date"+finalArrival_date+"  "+start_date + " " + end_time);
                            if (CheckDates1(finalArrival_date, start_date + " " + end_time))
                            {


                                dbHandler.update_Return_trip_status(tripid);
                                tv_place_of_des.setVisibility(View.GONE);
                                dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, return_trip_plan);

                                myTripDialog.dismiss();
                               ll_D_to_S_from_left_drawer.setVisibility(View.GONE);

                                display_D_To_S();

                            } else {

                                Constants.show_error_popup(getActivity(), "Please change trip return date time.", iv_bottom_up_arrow);

                            }

                        }
                    });
                    ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);
                    myTripDialog.show();
                    ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myTripDialog.dismiss();
                        }
                    });


                } else {
                   ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
                    if (return_plan==1) {
                        display_S_TO_D();
                    } else
                     display_D_To_S();
                }

            }
        });

        iv_tollbooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog myTollBoothDialog = new Dialog(getActivity());

                myTollBoothDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                myTollBoothDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myTollBoothDialog.setContentView(R.layout.pop_up_toolbooth);

                final ListView tollboothlistview = (ListView) myTollBoothDialog.findViewById(R.id.tollboothlist);

                TextView tv_no_tollbooth = (TextView) myTollBoothDialog.findViewById(R.id.tv_no_tollbooth);

                toolBooth_list_adapter = new TollBoothAdapter(getActivity(), tollBoothArrayList);

                tollboothlistview.setAdapter(toolBooth_list_adapter);
                final ImageView imgClosepopup = (ImageView) myTollBoothDialog.findViewById(R.id.ImgClosepopup);
                Log.d("System out", "from toolbooth___" + tollBoothArrayList.size());
                if (tollBoothArrayList.size() == 0) {
                    tollboothlistview.setVisibility(View.GONE);
                    tv_no_tollbooth.setVisibility(View.VISIBLE);
                } else {
                    tollboothlistview.setVisibility(View.VISIBLE);
                    tv_no_tollbooth.setVisibility(View.GONE);
                }
                myTollBoothDialog.show();

                imgClosepopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myTollBoothDialog.dismiss();
                    }
                });


            }
        });
       /* iv_map_route_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawer_layout_services.closeDrawer(left_navView);
                try {
                    for (int i = 0; i < listServiceCategoryList.size(); i++) {
                        listServiceCategoryList.get(i).setStatus(false);
                    }
                    listServiceCategoryList.get(1).setStatus(true);
                    ll_bottom_up.setVisibility(View.VISIBLE);
                   add_marker_poi();
                    serviceListAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                }

            }
        });*/


        tv_good_to_go_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Bundle b = new Bundle();

                if ( createTripList.get(0).getReturn_start_date().equalsIgnoreCase("")) {
                    Log.d("System out","pos in else"+createTripList.get(0).getReturn_start_date().toString());
                    b.putInt("tab_pos", 3);
                    b.putInt("TripId",tripid);
                    Log.d("Sytem out","trip id is" +tripid);
                }else

                {
                   b.putInt("TripId",tripid);
                    b.putInt("tab_pos", 2);
                    Log.d("System out","pos in if"+createTripList.get(0).getReturn_start_date().toString());
                }

                //Constants.show_error_popup_success(getActivity(), "Congratulations! Trip saved.\nHappy Roads will notify you for navigating your trip ", tv_good_to_go_map);
                final Dialog resetpwdDialog=new Dialog(getActivity());
                resetpwdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                resetpwdDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                resetpwdDialog.setContentView(R.layout.reset_pswd_link_layout);
                final TextView tv_alert_msg = (TextView) resetpwdDialog.findViewById(R.id.tv_alert_msg);
                tv_alert_msg.setText("Congratulations! Trip saved.\nHappy Roads will notify you for navigating your trip ");
                                               /*    if(response.body().get(0).getUmMobile().equalsIgnoreCase("")) {
                                tv_alert_msg.setText("Link to reset your password has \n been sent to your email");
                            }
                            else
                            {
                                tv_alert_msg.setText("Link to reset your password has \n been sent to your mobile number");

                        }*/
                       /* if(no_or_email) {
                            tv_alert_msg.setText("Link to reset your password has \n been sent to your mobile number");

                        }
                        else
                        {
                            tv_alert_msg.setText("Link to reset your password has \n been sent to your email");

                        }*/
                resetpwdDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(4500);
                            ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Code for the UiThread
                                    resetpwdDialog.dismiss();
                                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new MyTripWithSwipableFragment(), b);
                                    Log.d("Sytem out","trip id is" +tripid);
                                }
                            });
                        } catch (Exception e) {
                            Log.w("Exception in splash", e);
                        }

                    }
                }).start();




            }
        });


        iv_left_arrow_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (drawer_layout_services.isDrawerOpen(left_navView)) {
                   // iv_left_arrow_map.setImageResource(R.drawable.map_arrow_left_slider);
                    iv_left_arrow_map.setText("C\nL\nO\nS\nE");

                    Constants.hideSoftKeyBoardOnTabClicked(getActivity(), iv_left_arrow_map);
                    drawer_layout_services.closeDrawer(left_navView);
                    ll_bottom_up.setVisibility(View.VISIBLE);
                    if (fl_pop_up_zoo.getVisibility() == View.VISIBLE) {
                        Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_down);
                        fl_pop_up_zoo.startAnimation(bottomUpDown);
                        fl_pop_up_zoo.setVisibility(View.GONE);
                    }
                    if (plantype.contains("myPlan")) {
                        ll_bottom_up.setVisibility(View.GONE);
                        iv_bottom_up_arrow.setVisibility(View.GONE);
                    }
                    //((ExploreActivity)getActivity()).drawerLayout.closeDrawer(((ExploreActivity)getActivity()).navigationView);
                } else {
                    Constants.hideSoftKeyBoardOnTabClicked(getActivity(), iv_left_arrow_map);
                   // iv_left_arrow_map.setImageResource(R.drawable.map_arrow_right_slider);
                    iv_left_arrow_map.setText("O\nP\nE\nN");
                    drawer_layout_services.openDrawer(left_navView);
                    ll_bottom_up.setVisibility(View.GONE);
                    if (fl_pop_up_zoo.getVisibility() == View.VISIBLE) {
                        Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_down);
                        fl_pop_up_zoo.startAnimation(bottomUpDown);
                        fl_pop_up_zoo.setVisibility(View.GONE);
                    }
                    iv_bottom_up_arrow.setVisibility(View.VISIBLE);

                    if (category_select_pos == -1) {
                        category_select_pos = 0;
                    }

                    if (category_select_pos == 0) {

                        if (selected_ro_pos > -1 && onAdminRouteRoLists.size() > 0) {
                            onAdminRouteRoLists.get(selected_ro_pos).setClick_status(false);
                        }

                        if (map != null)
                            add_marker_ro();

                    } else if (category_select_pos == 1) {
                        if (selected_poi_pos > -1 && routepojo.size() > 0) {
                            onAdminRoutePoiLists.get(selected_poi_pos).setClick_status(false);
                        }
                        if (map != null)
                            add_marker_poi();
                    } else {
                       /* if (selected_ser_pos > -1 && services.size() > 0) {
                            services.get(selected_ser_pos).setClick_status(false);
                        }
                        if (map != null)
                            add_marker_services(listServiceCategoryList.get(position).getSerCatId());*/
                        selected_ser_pos = -1;
                        if (services.size() > 0) {
                            try {
                               add_marker_services();
                            } catch (Exception e) {

                            }
                        }

                    }

                }


            }
        });


        ((ExploreActivity) getActivity()).tv_origin_city_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (edit_stay_time) {
                    update_trip_start_date();
                }

            }
        });
        ((ExploreActivity) getActivity()).tv_origin_date_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_stay_time) {
                    update_trip_start_date();
                }

            }
        });

        ((ExploreActivity) getActivity()).ll_edit_date_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edit_stay_time) {
                    update_trip_start_date();
                }

            }
        });

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_stay_time == false) {
                    edit_stay_time = true;
                    iv_edit_stay_time.setImageResource(R.drawable.edit_icon_selected_my_plan);
                    tv_edit.setTextColor(getResources().getColor(R.color.textYellow));
                    tv_add.setTextColor(getResources().getColor(R.color.colorWhite));
                    //((ExploreActivity) getActivity()).ll_header_main.setEnabled(true);
                   // tv_place_of_des.setEnabled(true);
                    ((ExploreActivity) getActivity()).ll_edit_date_header.setBackground(getResources().getDrawable(R.drawable.textbox_edit));

                }
                else
                {
                    edit_stay_time = false;
                   // tv_place_of_des.setEnabled(false);
                    iv_edit_stay_time.setImageResource(R.drawable.edit_icon_unselected_my_plan);
                    tv_edit.setTextColor(getResources().getColor(R.color.colorWhite));

                    ((ExploreActivity) getActivity()).ll_edit_date_header.setBackground(null);
                    //   ((ExploreActivity) getActivity()).ll_header_main.setEnabled(false);
                }
                editIconUnselected = edit_stay_time;
                if (S_To_D_Plan != 1)
                    get_my_trip();
                else {
                    if (!desplan) {
                        for (int i = 1; i < destinationplanList.size(); i++) {
                            getdistancetime(i);
                        }
                    } else {
                        set_destination_layout();
                    }

                    if (destinationplanList.size() == 1) {
                        set_destination_layout();
                    }
                }


            }
        });

        iv_bottom_up_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_bottom_up.getVisibility() == View.GONE)
                {
                    Animation bottomUp = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_up);
                    iv_bottom_up_arrow.startAnimation(bottomUp);
                    ll_bottom_up.startAnimation(bottomUp);
                    ll_bottom_up.setVisibility(View.VISIBLE);
                    iv_bottom_up_arrow.setImageResource(R.drawable.map_arrow_down);
                } else {
                    Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_down);
                    ll_bottom_up.startAnimation(bottomUpDown);
                    iv_bottom_up_arrow.startAnimation(bottomUpDown);
                    ll_bottom_up.setVisibility(View.GONE);
                    iv_bottom_up_arrow.setImageResource(R.drawable.map_arrow_up);

                }


            }
        });

        iv_left_arrow_hsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int positionView = ((LinearLayoutManager) recycleview_cat_list.getLayoutManager()).findFirstVisibleItemPosition();
                Log.d("recycleviewposleft", String.valueOf(positionView));
                recycleview_cat_list.getLayoutManager().scrollToPosition(positionView - 1);

            }
        });
        iv_right_arrow_hsv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int positionView = ((LinearLayoutManager) recycleview_cat_list.getLayoutManager()).findLastVisibleItemPosition();
                recycleview_cat_list.getLayoutManager().scrollToPosition(positionView + 1);
                Log.d("recycleviewposright", String.valueOf(positionView));

            }
        });


        if (Constants.listDefaultValuesArrayList.size() > 0) {
            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

            {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_EmergencyNumber"))
                    default_EmergencyNumber = Constants.listDefaultValuesArrayList.get(i).getValue().toString();

            }
        } else {
            getListDefaultvalues();
        }



        onAdminRouteRoLists = new ArrayList<>();
        onAdminRoutePoiLists = new ArrayList<>();
        services = new ArrayList<>();
        routepojo = new ArrayList<>();

        desplan = false;


        serviceListAdapter = new Service_List_Adapter();
        horizontalLayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycleview_cat_list.setHasFixedSize(true);
      recycleview_cat_list.setLayoutManager(horizontalLayoutManagaer);
        recycleview_cat_list.setAdapter(serviceListAdapter);


        placeList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, placeList);
        tv_place_of_des.setThreshold(1);
        tv_place_of_des.setAdapter(adapter);

        // call to emergency call number
        iv_map_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:" + default_EmergencyNumber));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                } else {     //have got permission
                    try {
                        startActivity(callIntent);  //call activity and make phone call
                    } catch (android.content.ActivityNotFoundException ex) {
                        //  Toast.makeText(getApplicationContext(),"yourActivity is not founded",Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });
        tv_place_of_des.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length() > 0 )
                    getPlaces(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        gpsTracker = new GPSTracker(getActivity());

        getListServices_Category();


        if (Build.VERSION.SDK_INT >= 23) {
            check_permission_marshmallow();
        } else {
            ViewMap();
        }
        tv_add_poi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer pos = Integer.parseInt(tv_add_poi.getTag().toString().substring(1));

                String waytype = "";



                if (tv_add_poi.getTag().toString().startsWith("p")) {

                    if (onAdminRoutePoiLists.get(pos).getRPType().contains("POI")) {
                        waytype = "POI";
                        poi_latitude = Double.valueOf(onAdminRoutePoiLists.get(pos).getPOILatitude());
                        poi_longitude = Double.valueOf(onAdminRoutePoiLists.get(pos).getPOILongitude());
                        stay_hrs = Double.valueOf(onAdminRoutePoiLists.get(pos).getPOISpendTime().replace(":", "."));
                    } else {
                        waytype = "DES";
                        poi_latitude = Double.valueOf(onAdminRoutePoiLists.get(pos).getDesLatitude());
                        poi_longitude = Double.valueOf(onAdminRoutePoiLists.get(pos).getDesLongitude());
                        stay_hrs = 2.0;
                    }



                    getRoute_Planning("insert",set_wayPoint_insert(poi_latitude,poi_longitude));
                } else if (tv_add_poi.getTag().toString().startsWith("r")) {
                    waytype = "RO";
                    poi_latitude = Double.valueOf(Double.valueOf(onAdminRouteRoLists.get(pos).getROLatitude()));
                    poi_longitude = Double.valueOf(Double.valueOf(onAdminRouteRoLists.get(pos).getROLongitude()));
                    stay_hrs = Double.valueOf(onAdminRouteRoLists.get(pos).getROSpendTime().replace(":", "."));
                    getRoute_Planning("insert",set_wayPoint_insert(poi_latitude,poi_longitude));

                } else if (tv_add_poi.getTag().toString().startsWith("s")) {
                    waytype = "Services";
                    poi_latitude = Double.valueOf(Double.valueOf(services.get(pos).getSerLatitude()));
                    poi_longitude = Double.valueOf(Double.valueOf(services.get(pos).getSerLongitude()));
                    stay_hrs = Double.valueOf(services.get(pos).getDefaultSpendTime().toString().replace(":", "."));
                    getRoute_Planning("insert",set_wayPoint_insert(poi_latitude,poi_longitude));
                } else {
                    insert_poi_db();
                }


            }
        });


        tv_services_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer pos = Integer.parseInt(tv_add_poi.getTag().toString().substring(1));
                Bundle b = new Bundle();
                isnight=false;
                if (tv_add_poi.getTag().toString().startsWith("r")) {

                    ExploreActivity.drawerleft=true;

                    getRo(onAdminRouteRoLists.get(pos).getROLatitude(), onAdminRouteRoLists.get(pos).getROLongitude(), onAdminRouteRoLists.get(pos).getROMasterId());
                } else if (tv_add_poi.getTag().toString().startsWith("s")) {
                    ExploreActivity.drawerleft=true;
                    b.putString("facilitytype", "s");
                    String json = gson.toJson(services.get(pos));
                    b.putString("servicelist", json);
                    // b.putString("position",position);
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new FacilitiesFragment(), b);
                }
                else if (tv_add_poi.getTag().toString().startsWith("d")) {
                    ExploreActivity.drawerleft=true;

                    Bundle bundle = new Bundle();
                    bundle.putString("position", String.valueOf(pos));
                    bundle.putString("placeName", explorePoiList.get(pos).getPOIName());
                    bundle.putInt("tripId", tripid);
                    bundle.putString("destId", createTripList.get(0).getDestinationId().toString());
                    bundle.putString("DestName", ((ExploreActivity) getActivity()).tv_text_header.getText().toString());
                    gson = new Gson();
                    String json = gson.toJson(explorePoiList);
                    editor.putString("POIList", json);
                    editor.commit();

                    ExploreActivity.destattractionimages.clear();
                    ((ExploreActivity) getActivity()).setpagerdata(false);
                    for (int i = 0; i < explorePoiList.get(0).getImageList().size(); i++) {
                        if (explorePoiList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                            ExploreActivity.destattractionimages.add(explorePoiList.get(0).getImageList().get(i).getImgName());
                        }
                    }

                    //  bundle=getArguments();

                    ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();


                    // bundle.putAll(getArguments());
                    exploreAttractionFragment.setArguments(bundle);
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                    changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                    changeTransaction.addToBackStack(null);
                    changeTransaction.commit();

                } else if (tv_add_poi.getTag().toString().startsWith("p")) {
                    ExploreActivity.drawerleft=true;

                    int posn = -1;
                    for (int i = 0; i < onAdminRoutePoiLists.size(); i++) {
                        Log.d("System out", "Response of compare " + onAdminRoutePoiLists.get(i).getPOIName() + "   " + tv_poi_map_name.getText().toString());
                        Log.d("System out", "Response of compare " + onAdminRoutePoiLists.get(i).getDesName() + "   " + tv_poi_map_name.getText().toString());
                        if (onAdminRoutePoiLists.get(i).getPOIName().equalsIgnoreCase(tv_poi_map_name.getText().toString())) {
                            getPOIAPI(Integer.parseInt(createTripList.get(0).getDestinationId().toString()), onAdminRoutePoiLists.get(i).getPOIID());
                            break;
                        } else if (onAdminRoutePoiLists.get(i).getDesName().equalsIgnoreCase(tv_poi_map_name.getText().toString())) {
                            getDestinationAPI(onAdminRoutePoiLists.get(i).getDesName());
                            break;
                        }
                    }

                } else if (tv_add_poi.getTag().toString().startsWith("n")) {
                    ExploreActivity.drawerleft=true;

                    Bundle bundle = new Bundle();
                    bundle.putString("position", String.valueOf(pos));
                    bundle.putString("placeName", explorePoiList.get(pos).getPOIName());
                    bundle.putInt("tripId", tripid);
                    bundle.putString("destId", createTripList.get(0).getDestinationId().toString());
                    bundle.putString("plan_type", "PoiNight");
                    bundle.putString("DestName", ((ExploreActivity) getActivity()).tv_text_header.getText().toString());
                    gson = new Gson();
                    String json = gson.toJson(explorePoiList);
                    editor.putString("POIList", json);
                    editor.commit();
                    isnight=true;
                    ExploreActivity.destattractionimages.clear();
                    ((ExploreActivity) getActivity()).setpagerdata(false);
                    for (int i = 0; i < explorePoiList.get(0).getImageList().size(); i++) {
                        if (explorePoiList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                            ExploreActivity.destattractionimages.add(explorePoiList.get(0).getImageList().get(i).getImgName());
                        }
                    }
                    ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                    android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    exploreAttractionFragment.setArguments(bundle);
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
                    changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                    changeTransaction.addToBackStack(null);
                    changeTransaction.commit();
                }

            }
        });

        //set type of plan display
        if (plantype.contains("STOD")) {
            ll_S_to_D_on_trip_tracking.setVisibility(View.VISIBLE);
            ll_D_to_S_on_trip_tracking.setVisibility(View.GONE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.GONE);

            llserviceslayout.setVisibility(View.VISIBLE);
            ll_S_to_D_on_trip_tracking.setVisibility(View.GONE);
            return_plan = 0;
            return_trip_plan = false;

           display_S_TO_D();




        } else if (plantype.contains("DES")) {

            ll_bottom_up.setVisibility(View.GONE);
            iv_bottom_up_arrow.setVisibility(View.GONE);
            drawer_layout_services.closeDrawer(left_navView);
            iv_left_arrow_map.setVisibility(View.GONE);
            ll_S_to_D_on_trip_tracking.setVisibility(View.GONE);
            ll_D_to_S_on_trip_tracking.setVisibility(View.GONE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.VISIBLE);
            llserviceslayout.setVisibility(View.GONE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.GONE);
            display_des_plan();
           ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        } else if (plantype.contains("myPlan")) {
            ll_bottom_up.setVisibility(View.GONE);
            iv_bottom_up_arrow.setVisibility(View.GONE);
            iv_left_arrow_map.setVisibility(View.VISIBLE);
            ll_S_to_D_on_trip_tracking.setVisibility(View.GONE);
            ll_D_to_S_on_trip_tracking.setVisibility(View.GONE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.VISIBLE);
            llserviceslayout.setVisibility(View.GONE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.GONE);
            display_des_plan();
            ll_D_to_S_from_left_drawer.setVisibility(View.GONE);

        } else if (plantype.contains("DTOS")) {
            ll_S_to_D_on_trip_tracking.setVisibility(View.GONE);
            ll_D_to_S_on_trip_tracking.setVisibility(View.VISIBLE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.GONE);
            llserviceslayout.setVisibility(View.VISIBLE);
            ll_D_to_S_on_trip_tracking.setVisibility(View.GONE);
            return_plan = 1;
            return_trip_plan = true;
            display_D_To_S();


        } else {
            ll_S_to_D_on_trip_tracking.setVisibility(View.VISIBLE);
            ll_D_to_S_on_trip_tracking.setVisibility(View.VISIBLE);
            ll_plan_at_D_on_trip_tracking.setVisibility(View.VISIBLE);
            llserviceslayout.setVisibility(View.VISIBLE);

            return_plan = 0;
            return_trip_plan = false;
           display_S_TO_D();
        }


    }

    private void check_permission_marshmallow() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Access Content");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                });
                return;

            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);

            return;
        }
        ViewMap();
        //else open ur
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (getActivity().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void setMapAsPerNavigate() {

        if (name.contains("createtrip")) {


            ll_top_header_plan.setVisibility(View.GONE);
          //  iv_edit_stay_time.setVisibility(View.GONE);
            tv_edit.setVisibility(View.GONE);
            edit_stay_time = false;
        } else {
            ll_top_header_plan.setVisibility(View.VISIBLE);
            tv_edit.setVisibility(View.VISIBLE);

            //   iv_edit_stay_time.setVisibility(View.VISIBLE);
            edit_stay_time = false;
        }

    }

    private void PlacesBetweenStoD(final int pos) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v;

        v = inflater.inflate(R.layout.places_between_src_to_dest, null);

        ImageView iv_dotted_line = (ImageView) v.findViewById(R.id.iv_dotted_line);
        iv_dotted_line_bottom = (ImageView) v.findViewById(R.id.iv_dotted_line_bottom);
        TextView tv_poi_name = (TextView) v.findViewById(R.id.tv_poi_name);
        //    TextView tv_poi_hrs=(TextView)v.findViewById(R.id.tv_poi_hrs);
        final TextView et_stay_time = (TextView) v.findViewById(R.id.et_stay_time);
        TextView tv_departure_time = (TextView) v.findViewById(R.id.tv_departure_time);
        TextView tv_arrival_time = (TextView) v.findViewById(R.id.tv_arrival_time);
        ImageView iv_poi_delete = (ImageView) v.findViewById(R.id.iv_poi_delete);
        TextView tv_poi_distance_time = (TextView) v.findViewById(R.id.tv_poi_distance_time);
        CircleImageView fc_poi_image = (CircleImageView) v.findViewById(R.id.fc_poi_image);
        iv_poi_night_stay = (ImageView) v.findViewById(R.id.iv_poi_night_stay);
        ImageView iv_poi_map = (ImageView) v.findViewById(R.id.iv_poi_map);
        final ImageView iv_poi_up_arrow = (ImageView) v.findViewById(R.id.iv_poi_up_arrow);

        LinearLayout ll_stay_time_layout = (LinearLayout) v.findViewById(R.id.ll_stay_time_layout);
        LinearLayout ll_edit_stay_time = (LinearLayout) v.findViewById(R.id.ll_edit_stay_time);

        final LinearLayout ll_places_between_N_to_D = (LinearLayout) v.findViewById(R.id.ll_places_between_N_to_D);

        tv_poi_name.setText(createPoiList.get(pos).getPoiName());
        et_stay_time.setText(createPoiList.get(pos).getStayTime().toString());


        if (edit_stay_time) {
            et_stay_time.setEnabled(true);
            ll_edit_stay_time.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
            iv_poi_delete.setVisibility(View.VISIBLE);
        } else {
            et_stay_time.setEnabled(false);
            ll_edit_stay_time.setBackground(null);
            iv_poi_delete.setVisibility(View.GONE);
        }
        ll_edit_stay_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_stay_time.performClick();
            }
        });

        et_stay_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   et_stay_time.requestFocus();

                BigDecimal bd = new BigDecimal((createPoiList.get(pos).getStayTime() - Math.floor(createPoiList.get(pos).getStayTime())) * 100);
                bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                int amin = bd.intValue();


                final Dialog timerDialog = new Dialog(getActivity());

                timerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                timerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timerDialog.setContentView(R.layout.timer_dialog);
                timerDialog.show();
                NumberPicker np = (NumberPicker)timerDialog. findViewById(R.id.np);
                NumberPicker np1 = (NumberPicker)timerDialog. findViewById(R.id.np1);
                ImageView ImgClosepopup=(ImageView)timerDialog.findViewById(R.id.ImgClosepopup);
                ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timerDialog.dismiss();
                    }
                });
                Button btn_ok =(Button)timerDialog.findViewById(R.id.btn_ok);
                final String[] hrs= new String[]{"00", "01", "02", "03","04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};
                final String[] mins= new String[]{"00", "15", "30", "45"};

                np.setMinValue(0);
                //Specify the maximum value/number of NumberPicker
                np.setMaxValue(hrs.length-1);
                np1.setMinValue(0);
                np1.setMaxValue(mins.length-1);

                //Gets whether the selector wheel wraps when reaching the min/max value.
                np.setWrapSelectorWheel(true);
                np1.setWrapSelectorWheel(true);

                np.setDisplayedValues(hrs);
                np1.setDisplayedValues(mins);


                np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                np1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                np.setValue( createPoiList.get(pos).getStayTime().intValue()); // example value
                a=createPoiList.get(pos).getStayTime().intValue();
                b=amin;

                if(amin==0)
                {
                    np1.setValue(0);
                }
                else if(amin<=15)
                {
                    np1.setValue(1);
                }
                else if(amin<=30)
                {
                    np1.setValue(2);
                }
                else if(amin>31)
                {
                    np1.setValue(3);
                }


                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                        a=Integer.valueOf(hrs[newVal].toString());
                    }
                });
                np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                        b=Integer.valueOf(mins[newVal].toString());
                    }
                });



                /*tpd1 = TimePickerDialog.newInstance(
                        RoutePlanningFragment.this,
                        createPoiList.get(pos).getStayTime().intValue(),
                        amin, true
                );

                tpd1.show(getActivity().getFragmentManager(), "Timepickerdialog1");*/
/*
                tpd1.setOnTimeSetListener(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                        String timertime = "";
                        try {
                            timertime = Constants.formatDate(hourOfDay + ":" + minute, "HH:mm", "HH.mm");
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }

                        //  Constants.hideSoftKeyBoardOnTabClicked(getActivity(), et_stay_time);
                         et_stay_time.setText(timertime);


                             if(createTripList.get(0).getTripstatus().toLowerCase().contains("upcoming")) {


                                 Double previous_stay_time = createPoiList.get(pos).getStayTime();
                                 dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));

                                 get_my_trip1();

                                 dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), previous_stay_time);


                                 Boolean resume_date_valid = true;
                                 Integer pos1 = 0;
                                 Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                                 for (int i = 0; i < tempcreatePoiList.size(); i++) {

                                     if (tempcreatePoiList.get(i).getPoiStay()) {

                                         String arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();


                                         Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                         if (tempcreatePoiList.get(i).getResumeDate() == null) {
                                             resume_date_valid = true;
                                         } else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                             resume_date_valid = false;
                                             pos1 = i;
                                             break;
                                         }

                                     }


                                 }


                                 if (!resume_date_valid) {
                                     Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination", iv_bottom_up_arrow);
                                     et_stay_time.setText(previous_stay_time.toString());
                                 } else if (!CheckDates(tempcreateTripList.get(0).getEnddate() + " " + tempcreateTripList.get(0).getEndtime(), tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time()) && return_plan == 0 && tempcreateTripList.get(0).getReturnstatus()) {
                                     et_stay_time.setText(previous_stay_time.toString());
                                     update_trip_return_date("Please update return date and time.");
                                 } else {

                                     dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                     ll_places_between_S_to_D.removeAllViews();

                                     get_my_trip();

                                 }
                             }
                             else
                             {
                                 dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                 ll_places_between_S_to_D.removeAllViews();

                                 get_my_trip();
                             }

                    }

                });
*/

                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        timerDialog.dismiss();
                        String timertime = "";

                        timertime = a+"."+b;



                        //  Constants.hideSoftKeyBoardOnTabClicked(getActivity(), et_stay_time);
                        et_stay_time.setText(timertime);


                        if(createTripList.get(0).getTripstatus().toLowerCase().contains("upcoming")) {


                            Double previous_stay_time = createPoiList.get(pos).getStayTime();
                            dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));

                            get_my_trip1();

                            dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), previous_stay_time);


                            Boolean resume_date_valid = true;
                            Integer pos1 = 0;
                            Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                            for (int i = 0; i < tempcreatePoiList.size(); i++) {

                                if (tempcreatePoiList.get(i).getPoiStay()) {

                                    String arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();


                                    Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                    if (tempcreatePoiList.get(i).getResumeDate() == null) {
                                        resume_date_valid = true;
                                    } else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                        resume_date_valid = false;
                                        pos1 = i;
                                        break;
                                    }

                                }


                            }


                            if (!resume_date_valid) {
                               // Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination", iv_bottom_up_arrow);
                                Constants.show_error_popup(getActivity(), "Update date & time to resume your trip from" + tempcreatePoiList.get(pos1).getPoiName() + ".", iv_bottom_up_arrow);
                                et_stay_time.setText(previous_stay_time.toString());
                            } else if (!CheckDates(tempcreateTripList.get(0).getEnddate() + " " + tempcreateTripList.get(0).getEndtime(), tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time()) && return_plan == 0 && tempcreateTripList.get(0).getReturnstatus()) {
                                et_stay_time.setText(previous_stay_time.toString());
                                update_trip_return_date("Please update return date and time.");
                            } else {

                                dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                                ll_places_between_S_to_D.removeAllViews();

                                get_my_trip();

                            }
                        }
                        else
                        {
                            dbHandler.update_stay_km_by_PoiId(createPoiList.get(pos).getWayPointId(), Double.valueOf(et_stay_time.getText().toString()));
                            ll_places_between_S_to_D.removeAllViews();

                            get_my_trip();
                        }

                    }

                });


            }
        });


        Bitmap bitmap = new ImageSaver(getActivity()).
                setFileName(createPoiList.get(pos).getImagename()).
                setExternal(true).
                load();
        fc_poi_image.setImageBitmap(bitmap);

        double roundOff1 = Math.round(createPoiList.get(0).getKM() / 1000 * 100.0) / 100.0;
       // tv_trip_distance_time.setText(roundOff1 + " Kms / " + Constants.convert_minute_hrs_both(createPoiList.get(0).getTime()));
        tv_trip_distance_time.setText(roundOff1 + " Kms ");


        if (createPoiList.get(pos).getPoitype().contains("DES")) {


            if (createPoiList.get(pos).getPoiStay() == true) {
                iv_poi_night_stay.setImageResource(R.drawable.night_halt_icon_map_slider);
                iv_poi_up_arrow.setVisibility(View.GONE);
                iv_poi_map.setVisibility(View.VISIBLE);
                iv_poi_night_stay.setVisibility(View.VISIBLE);
                iv_poi_delete.setVisibility(View.VISIBLE);
                ll_stay_time_layout.setVisibility(View.GONE);


            } else {
                iv_poi_night_stay.setImageResource(R.drawable.night_halt_icon_map_slider_1);
                iv_poi_up_arrow.setVisibility(View.GONE);
                iv_poi_map.setVisibility(View.GONE);
                iv_poi_night_stay.setVisibility(View.VISIBLE);
                iv_poi_delete.setVisibility(View.VISIBLE);
                ll_stay_time_layout.setVisibility(View.VISIBLE);

            }

            if ((createPoiList.get(pos).getAttractionCount() > 0 && createPoiList.get(pos).getPoiStay()) || (createPoiList.get(pos).getAttractionCount() > 0 &&createTripList.get(0).getTripstatus().contains("Saved"))) {
                iv_poi_map.setVisibility(View.VISIBLE);
            } else {
                iv_poi_map.setVisibility(View.GONE);
            }


            create_poi_nightsList = new ArrayList<>();
            create_poi_nightsList.clear();

            create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(createPoiList.get(pos).getTripId(), createPoiList.get(pos).getPoiDesID(), return_plan);

            Log.d("sizeofnight", String.valueOf(create_poi_nightsList.size()));

//set night poi layout
            if (create_poi_nightsList.size() <= 0) {
                iv_poi_up_arrow.setVisibility(View.GONE);

                if (edit_stay_time) {
                    iv_poi_delete.setVisibility(View.VISIBLE);
                } else {
                    iv_poi_delete.setVisibility(View.GONE);
                }


            } else {

                iv_poi_up_arrow.setVisibility(View.VISIBLE);
                //tv_poi_distance_time.setVisibility(View.GONE);

                iv_poi_delete.setVisibility(View.GONE);


                for (int j = 0; j < create_poi_nightsList.size(); j++) {
                    LayoutInflater inflater1 = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    final View v1;

                    v1 = inflater1.inflate(R.layout.places_between_night_to_dest, null);
                    TextView tv_poi_name_n = (TextView) v1.findViewById(R.id.tv_poi_name);
                    //    TextView tv_poi_hrs=(TextView)v.findViewById(R.id.tv_poi_hrs);
                    final EditText et_stay_time_n = (EditText) v1.findViewById(R.id.et_stay_time);


                    final ImageView iv_poi_delete_n = (ImageView) v1.findViewById(R.id.iv_poi_delete);
                    TextView tv_poi_distance_time_n = (TextView) v1.findViewById(R.id.tv_poi_distance_time);
                    CircleImageView fc_poi_image_n = (CircleImageView) v1.findViewById(R.id.fc_poi_image);
                    final View view_bottom_line = v1.findViewById(R.id.view_line);
                    LinearLayout ll_edit_night_stay = (LinearLayout) v1.findViewById(R.id.ll_edit_night_stay);

                    tv_poi_name_n.setText(create_poi_nightsList.get(j).getPoiName());
                    et_stay_time_n.setText(create_poi_nightsList.get(j).getStayTime().toString());
                    if (edit_stay_time) {
                        // et_stay_time_n.setEnabled(true);
                        ll_edit_night_stay.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
                        iv_poi_delete_n.setVisibility(View.VISIBLE);
                    } else {
                        //  et_stay_time_n.setEnabled(false);
                        ll_edit_night_stay.setBackground(null);
                        iv_poi_delete_n.setVisibility(View.GONE);
                    }

                    double roundOff = Math.round((create_poi_nightsList.get(j).getKM() / 1000) * 100.0) / 100.0;

                    if (j == 0)
                       // tv_poi_distance_time_n.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(create_poi_nightsList.get(j).getTime()));
                        tv_poi_distance_time_n.setText(roundOff + " Kms");
                    else {

                        int previouspos = j - 1;
                        Double distance_between = distance(create_poi_nightsList.get(j).getPoiLatitude(), create_poi_nightsList.get(j).getPoiLongitude(), create_poi_nightsList.get(previouspos).getPoiLatitude(), create_poi_nightsList.get(previouspos).getPoiLongitude());
                        double roundOff2 = Math.round(distance_between * 100.0) / 100.0;


                        //Double time = get_time_based_on_KM(distance_between);
                        Double time = roundOff2 * 60;
                        time = Math.round(time * 100.0) / 100.0;
                        Log.d("distime1", String.valueOf(distance_between) + "time" + String.valueOf(time));

                       // tv_poi_distance_time_n.setText(roundOff2 + " Kms / " + Constants.convert_minute_hrs_both(Double.valueOf(time)));
                        tv_poi_distance_time_n.setText(roundOff2 + " Kms " );
                    }


                    bitmap = new ImageSaver(getActivity()).
                            setFileName(create_poi_nightsList.get(j).getImagename()).


                            setExternal(true).
                            load();
                    if (bitmap != null)
                        fc_poi_image_n.setImageBitmap(bitmap);

                    iv_poi_delete_n.setTag(j);
                    et_stay_time_n.setTag(j);
                    et_stay_time_n.setId(j);


                    if (j == create_poi_nightsList.size() - 1) {
                        view_bottom_line.setVisibility(View.GONE);
                    }
                    et_stay_time_n.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            Double stay_time = 0.0;
                            try {
                                stay_time = Double.valueOf(et_stay_time_n.getText().toString());
                            } catch (Exception e) {
                            }

                            BigDecimal bd = new BigDecimal((stay_time - Math.floor(stay_time)) * 100);
                            bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                            int amin = bd.intValue();


                            tpd1 = TimePickerDialog.newInstance(
                                    RoutePlanningFragment.this,
                                    stay_time.intValue(),
                                    amin, true
                            );

                            tpd1 = TimePickerDialog.newInstance(
                                    RoutePlanningFragment.this,
                                    now.get(Calendar.HOUR),
                                    now.get(Calendar.MINUTE), true
                            );

                            tpd1.show(getActivity().getFragmentManager(), "Timepickerdialog1");
                            tpd1.setOnTimeSetListener(new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                                    //  Constants.hideSoftKeyBoardOnTabClicked(getActivity(), et_stay_time_n);
                                    String timertime = "";
                                    try {
                                        timertime = Constants.formatDate(hourOfDay + ":" + minute, "HH:mm", "HH.mm");
                                    } catch (ParseException e1) {
                                        e1.printStackTrace();
                                    }


                                    et_stay_time_n.setText(timertime);
                                    int pos = Integer.valueOf(iv_poi_delete_n.getTag().toString());
                                    dbHandler.update_stay_km_by_NightPoiId(create_poi_nightsList.get(pos).getWayPointId(), Double.valueOf(et_stay_time_n.getText().toString()));

                                    update_night_stay_poi_plan(pos, create_poi_nightsList.get(pos).getPoiId());
                                    ll_places_between_S_to_D.removeAllViews();
                                    get_my_trip();
                                }
                            });
                        }
                    });






                    iv_poi_delete_n.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int pos = Integer.valueOf(iv_poi_delete_n.getTag().toString());
                            dbHandler.delete_Night_POI_ById(create_poi_nightsList.get(pos).getTripId(), create_poi_nightsList.get(pos).getWayPointId());
                            String json = "[{     \"TripWayPointId\": \"" + create_poi_nightsList.get(pos).getWayPointId() + "\"}]";
                            Delete_Poi_Api(create_poi_nightsList.get(pos).getWayPointId(), "POINight", json);
                            get_my_trip();

                        }
                    });
                    iv_poi_up_arrow.setTag(1);
                    iv_poi_up_arrow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if ((int) iv_poi_up_arrow.getTag() == 1) {
                                ll_places_between_N_to_D.setVisibility(View.GONE);
                                iv_poi_up_arrow.setImageResource(R.drawable.arrow_down_map_slider);
                                iv_poi_up_arrow.setTag(0);
                            } else {

                                ll_places_between_N_to_D.setVisibility(View.VISIBLE);
                                iv_poi_up_arrow.setImageResource(R.drawable.arrow_up_map_slider);
                                iv_poi_up_arrow.setTag(1);
                            }

                        }
                    });

                    ll_places_between_N_to_D.addView(v1);


                }
            }


        } else {
            iv_poi_night_stay.setVisibility(View.GONE);
            iv_poi_delete.setVisibility(View.VISIBLE);
            ll_stay_time_layout.setVisibility(View.VISIBLE);
        }

        Log.d("System out", "is night value " + isnight);
        if (isnight == true) {
            iv_poi_map.performClick();
        }
        if (edit_stay_time) {
            iv_poi_delete.setVisibility(View.VISIBLE);
        } else {
            iv_poi_delete.setVisibility(View.GONE);
        }


        //set departure date and time
        if (pos == 0) {


            String tripStartDate = "";

            if (return_plan == 0)
                tripStartDate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime();
            else
                tripStartDate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time();


            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(createPoiList.get(0).getTime());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);


                tv_arrival_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));

                createPoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));


                if (createPoiList.get(pos).getPoiStay() == false)
                {
                    BigDecimal bd = new BigDecimal((createPoiList.get(pos).getStayTime() - Math.floor(createPoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, createPoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tv_departure_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                } else {
                    date = sdf.parse(createPoiList.get(pos).getResumeDate() + " " + createPoiList.get(pos).getResumeTime());
                    calendar.setTime(date);
                    tv_departure_time.setText(createPoiList.get(pos).getResumeDate() + "\n" + convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));


                }

            } catch (ParseException e) {
                e.printStackTrace();

            }


        }

        if (pos >= 1 && (pos <= createPoiList.size() - 1)) {

            int position = pos - 1;


            String tripStartDate = "";
            if(createPoiList.get(position).getActual_arrival_date_time() !=null)
            tripStartDate=  createPoiList.get(position).getActual_dep_date_time();


            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                // int[] convertime=  splitToComponentTimes(createPoiList.get(position).getTime());
                Double requriedtime = createPoiList.get(position).getReqtime();
                calendar.add(Calendar.HOUR, requriedtime.intValue());

                BigDecimal bd = new BigDecimal((requriedtime - Math.floor(requriedtime)) * 100);
                bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                int amin = bd.intValue();

                calendar.add(Calendar.MINUTE, amin);

                tv_arrival_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));


                createPoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));

                if (createPoiList.get(pos).getPoiStay() == false) {
                    bd = new BigDecimal((createPoiList.get(pos).getStayTime() - Math.floor(createPoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, createPoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tv_departure_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                } else {
                    date = sdf.parse(createPoiList.get(pos).getResumeDate() + " " + createPoiList.get(pos).getResumeTime());

                    calendar.setTime(date);


                    tv_departure_time.setText(createPoiList.get(pos).getResumeDate() + "\n" + convert_time_12_format(sdfTime.format(calendar.getTime())));

                    createPoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));


                }

            } catch (ParseException e) {
                e.printStackTrace();

            }

        }

        // eta display
        if (createPoiList.size() == pos + 1) {


            double roundOff=0.0;
            if(return_plan==0) {
                roundOff  =Math.round((createTripList.get(0).getKM()) / 1000 * 100.0) / 100.0;

                createPoiList.get(pos).setReqtime(createTripList.get(0).getTime());
               // tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getTime()));
                tv_poi_distance_time.setText(roundOff + " Kms ");
                add_hours_minute_last(createPoiList.get(pos).getActual_dep_date_time(), createPoiList.get(pos).getActual_dep_date_time(), createPoiList.get(pos).getReqtime());
            }
            else
            {
                roundOff  =Math.round((createTripList.get(0).getReturnKM()) / 1000 * 100.0) / 100.0;

                createPoiList.get(pos).setReqtime(createTripList.get(0).getReturnTime());
                //tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getReturnTime()));
                tv_poi_distance_time.setText(roundOff + " Kms");
                add_hours_minute_last(createPoiList.get(pos).getActual_dep_date_time(), createPoiList.get(pos).getActual_dep_date_time(), createPoiList.get(pos).getReqtime());
            }


        } else {
            double roundOff = Math.round((createPoiList.get(pos + 1).getKM()) / 1000 * 100.0) / 100.0;

           // tv_poi_distance_time.setText(roundOff + " Kms / " + Constants.convert_minute_hrs_both(createPoiList.get(pos + 1).getTime()));
            tv_poi_distance_time.setText(roundOff + " Kms ");

            createPoiList.get(pos).setReqtime(Double.valueOf(Constants.convert_minute_hrs1(createPoiList.get(pos + 1).getTime())));


        }


        iv_poi_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_pos=pos;
                getRoute_Planning("delete",set_wayPoint_delete(createPoiList.get(pos).getWayPointId()));




            }
        });


        iv_poi_night_stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!createTripList.get(0).getTripstatus().contains("Saved") && createPoiList.get(pos).getActual_arrival_date_time() != null) {


                    if (edit_stay_time) {

                        final SessionManager sessionManager1 = new SessionManager(getActivity());
                        final Dialog myTripResumeDialog = new Dialog(getActivity());
                        myTripResumeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        myTripResumeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        myTripResumeDialog.setContentView(R.layout.resume_trip_from_place_popup);
                        tv_resume_date_my_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_date);
                        tv_resume_time_my_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_time);
                        final ImageView imgClosepopup = (ImageView) myTripResumeDialog.findViewById(R.id.ImgClosepopup);
                        TextView tv_resume_destination = (TextView) myTripResumeDialog.findViewById(R.id.tv_resume_destination);

                        TextView tv_save_resume_trip = (TextView) myTripResumeDialog.findViewById(R.id.tv_save_resume_trip);


                        Calendar now = Calendar.getInstance();


                        dpd = DatePickerDialog.newInstance(
                                RoutePlanningFragment.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );

                        tv_resume_destination.setText("Resume Trip From " + createPoiList.get(pos).getPoiName());

                        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
                        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");


                        String arrival_date = createPoiList.get(pos).getActual_arrival_date_time();
                        if (createPoiList.get(pos).getResumeDate() != null)
                        {
                            if (createPoiList.get(pos).getResumeDate().length() > 0)
                            {
                                tv_resume_date_my_trip.setText(createPoiList.get(pos).getResumeDate());
                                try {
                                    tv_resume_time_my_trip.setText(Constants.formatDate(createPoiList.get(pos).getResumeTime(), "HH:mm", "hh:mm a"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {

                                tv_resume_date_my_trip.setText("Tap to set date");
                                tv_resume_time_my_trip.setText("Tap to set time");

                               /* try {
                                    tv_resume_date_my_trip.setText(Constants.formatDate(createPoiList.get(pos).getActual_arrival_date_time(), "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy"));
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(sdf.parse(arrival_date));
                                    calendar.add(Calendar.HOUR, 2);

                                    tv_resume_time_my_trip.setText(Constants.formatDate(sdfTime.format(calendar.getTime()), "HH:mm", "hh:mm a"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }*/
                            }
                        } else {
                           /* try {
                                tv_resume_date_my_trip.setText(Constants.formatDate(createPoiList.get(pos).getActual_arrival_date_time(), "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy"));
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(sdf.parse(arrival_date));
                                calendar.add(Calendar.HOUR, 2);

                                tv_resume_time_my_trip.setText(Constants.formatDate(sdfTime.format(calendar.getTime()), "HH:mm", "hh:mm a"));

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }*/
                            tv_resume_date_my_trip.setText("Tap to set date");
                            tv_resume_time_my_trip.setText("Tap to set time");
                        }




                        Date date = null;
                        try {
                            date = sdf1.parse(arrival_date);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            calendar.add(Calendar.DATE,1);
                            dpd.setMinDate(calendar);
                            arrival_date=sdf.format(calendar.getTime());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        tpd = TimePickerDialog.newInstance(
                                RoutePlanningFragment.this,
                                now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE), false
                        );

                        imgClosepopup.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                myTripResumeDialog.dismiss();
                            }
                        });

                        tv_resume_time_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                select_dpd_type = "resume";
                                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                            }
                        });

                        tv_resume_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                select_dpd_type = "resume";
                                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

                            }
                        });
                        final String finalArrival_date = arrival_date;
                        Log.d("resumetimedparrival", finalArrival_date);

                        tv_save_resume_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                String resume_time = "";

                                try {
                                    resume_time = Constants.formatDate(tv_resume_time_my_trip.getText().toString(), "hh:mm a", "HH:mm");


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (CheckDates(finalArrival_date, tv_resume_date_my_trip.getText().toString() + " " + resume_time)) {

                                    if (createTripList.get(0).getReturnstatus() == true && createTripList.get(0).getReturn_start_date() != null) {

                                        //check resume date
                                        String previous_resume_date = "";
                                        String previous_resume_time = "";

                                        if (createPoiList.get(pos).getResumeDate() != null && createPoiList.get(pos).getPoiStay()) {

                                            previous_resume_date = createPoiList.get(pos).getResumeDate();
                                            previous_resume_time = createPoiList.get(pos).getResumeTime();
                                        }


                                        dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time);
                                        get_my_trip1();
                                        if (previous_resume_date.length() > 1)
                                        {
                                            dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), previous_resume_date, previous_resume_time);
                                        } else {
                                            dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), null, null);
                                        }

                                        if (!CheckDates(tempcreateTripList.get(0).getEnddate() + " " + tempcreateTripList.get(0).getEndtime(), createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time()) && return_plan==0) {

                                            myTripResumeDialog.dismiss();
                                            update_trip_return_date("please update return date and time.");

                                        } else {
                                            dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time);
                                            myTripResumeDialog.dismiss();
                                            get_my_trip();

                                        }
                                    } else {
                                        dbHandler.update_resumedate_time_by_PoiId(createPoiList.get(pos).getPoiId(), tv_resume_date_my_trip.getText().toString(), resume_time);


                                        myTripResumeDialog.dismiss();
                                        get_my_trip();
                                    }


                                } else {

                                    Constants.show_error_popup(getActivity(), "Update date & time to resume your trip. ", iv_bottom_up_arrow);
                                }


                            }
                        });

                        myTripResumeDialog.show();


                    }
                } else {
                    update_start_date_resume = true;
                    update_trip_start_date();
                    // Constants.show_error_popup(getActivity(), "Select your trip dates.", iv_bottom_up_arrow);
                }

            }
        });


        iv_poi_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.hideSoftKeyBoardOnTabClicked(getActivity(), v);
                drawer_layout_services.closeDrawer(left_navView);
                ll_bottom_up.setVisibility(View.GONE);
                iv_bottom_up_arrow.setVisibility(View.GONE);
                night_poi_id = createPoiList.get(pos).getPoiDesID();
                getPOIAPI(createPoiList.get(pos).getPoiDesID(), createPoiList.get(pos).getPoiLatitude(), createPoiList.get(pos).getPoiLongitude());
            }
        });

        update_poi_plan(pos);

        ll_places_between_S_to_D.addView(v);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        exploreActivity = (ExploreActivity) activity;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Syatem out", "onDestroyCalled________");

        // ((ExploreActivity)getActivity()).setmenutonavigationdrawer();
        //  ((ExploreActivity)getActivity()).drawerLayout.openDrawer(((ExploreActivity)getActivity()).navigationView);
    }



    @Override
    public void onResume() {
        super.onResume();
        ((ExploreActivity) getActivity()).toolbar.setBackgroundResource(R.drawable.bg_top);
        ((ExploreActivity) getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_filter_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_menu_header.setVisibility(View.VISIBLE);

        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.VISIBLE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).iv_home_icon_header.setImageResource(R.drawable.back_icon);


        if (map == null) {
            // Try to obtain the map from the SupportMapFragment.
            FragmentManager fm = getChildFragmentManager();
            ((SupportMapFragment) fm.findFragmentById(R.id.map_home1)).getMapAsync(this);


            // Check if we were successful in obtaining the map.

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((ExploreActivity) getActivity()).ll_plan_trip_header.setVisibility(View.GONE);
        //  ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity) getActivity()).toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));

        //  ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = dayOfMonth + " " + (++monthOfYear) + "," + year;


        try {
            date1 = Constants.formatDate(date, "dd MM,yyyy", "dd MMM ,yyyy");
            if (select_dpd_type.contains("resume")) {
                tv_resume_date_my_trip.setText(date1.toString());
                tv_resume_time_my_trip.performClick();
                //  tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
            } else if (select_dpd_type.contains("return")) {


                tv_start_date_my_return_trip.setText(date1);
                tv_start_time_my_return_trip.performClick();

            } else {
                tv_start_date_my_return_trip.setText(date1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String time = hourOfDay + ":" + minute + "";

        try {
            date1 = Constants.formatDate(time, "HH:mm", "hh:mm a");

            if (select_dpd_type.contains("resume"))
            {
                tv_resume_time_my_trip.setText(date1.toString());
            }
            else if (select_dpd_type.contains("return"))
            {
                date=tv_start_date_my_return_trip.getText().toString();
                tv_start_time_my_return_trip.setText(date1);

                String date11 = Constants.formatDate(date + " " + time, "dd MMM ,yyyy HH:mm", "dd MMM, yyyy hh:mm a");

                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date11);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf1.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf1.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);

                calendar1.add(Calendar.HOUR, 0);

                //  currentDateandTime.set
                if (calendar1.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                    tv_start_time_my_return_trip.setText("");
                    //  Constants.show_error_popup(getActivity(), "Start time should be after 2 hours of current time.", tv_plan_my_return_trip);
                    Constants.show_error_popup(getActivity(), "Select valid date & time.", tv_plan_my_return_trip);


                } else if (datenew.before(datecurrent)) {
                    tv_start_time_my_return_trip.setText("");
                    //  Constants.show_error_popup(getActivity(), "Start time should be after 2 hours of current time.", tv_plan_my_return_trip);
                    Constants.show_error_popup(getActivity(), "Select valid date & time.", tv_plan_my_return_trip);

                } else {
                    tv_start_time_my_return_trip.setText(date1);

                }


            } else {
                String date11 = Constants.formatDate(date + " " + time, "dd MM,yyyy HH:mm", "dd MMM, yyyy hh:mm a");

                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date11);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf1.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf1.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);
                //calendar1.add(Calendar.HOUR, 2);
                calendar1.add(Calendar.HOUR, 0);

                //  currentDateandTime.set
                if (calendar1.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                    tv_start_time_my_return_trip.setText("");
                    //  Constants.show_error_popup(getActivity(), "Start time should be after 2 hours of current time.", tv_plan_my_return_trip);
                    Constants.show_error_popup(getActivity(), "Select valid date & time.", tv_plan_my_return_trip);


                } else if (datenew.before(datecurrent)) {
                    tv_start_time_my_return_trip.setText("");
                    //  Constants.show_error_popup(getActivity(), "Start time should be after 2 hours of current time.", tv_plan_my_return_trip);
                    Constants.show_error_popup(getActivity(), "Select valid date & time.", tv_plan_my_return_trip);

                } else {
                    tv_start_time_my_return_trip.setText(date1);

                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void get_my_trip() {

        createTripList = new ArrayList<>();
        createPoiList = new ArrayList<>();
        createPoiList.clear();


        createTripList = dbHandler.get_TRIP_By_TripId(tripid);


        if (createTripList.size() > 0) {


           /* List<Create_Poi> count_create_poi_list = dbHandler.get_POI_TRIP(tripid, 1);
            if (count_create_poi_list.size()>0 && createTripList.get(0).getReturn_start_date()!=null) {
              //  tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
                tv_good_to_go_map.setVisibility(View.VISIBLE);
            } else {
              //  tv_D_to_S_from_left_drawer.setVisibility(View.VISIBLE);
                tv_good_to_go_map.setVisibility(View.GONE);
            }
*/


            if (return_plan == 0)
            {
                tv_origin.setText(createTripList.get(0).getSource());
                tv_destination.setText(createTripList.get(0).getDestination());

                tv_start_time.setText(convert_time_12_format(createTripList.get(0).getStarttime()));

                tv_start_date.setText(createTripList.get(0).getStartdate());


                DecimalFormat df = new DecimalFormat("#.##");
                double distance = Double.valueOf(df.format(createTripList.get(0).getKM() / 1000));


               // tv_trip_distance_time.setText(distance + " Kms / " + Constants.convert_minute_hrs_both(createTripList.get(0).getTime()));
                tv_trip_distance_time.setText(distance + " Kms");


                iv_circle_source.setImageBitmap(Constants.getImage(createTripList.get(0).getSource_image()));

                iv_circle_destination.setImageBitmap(Constants.getImage(createTripList.get(0).getDestination_image()));

                set_header_origin_destination();
                 remaining_km=createTripList.get(0).getKM();
                    remaining_time=createTripList.get(0).getTime();
                Log.d("System out","address for destination__"+(createTripList.get(0).getDes_address().length()));
                try {
                    if (createTripList.get(0).getDes_address().length()>1) {
                        tv_place_of_des.setText(createTripList.get(0).getDes_address());
                    }
                } catch (Exception e) {
                }

                add_hours_minute(createTripList.get(0).getStartdate(), createTripList.get(0).getStarttime(), createTripList.get(0).getTime());


            } else {
                tv_origin.setText(createTripList.get(0).getDestination());
                tv_destination.setText(createTripList.get(0).getSource());


                tv_start_time.setText(convert_time_12_format(createTripList.get(0).getReturn_start_time()));

                tv_start_date.setText(createTripList.get(0).getReturn_start_date());


                DecimalFormat df = new DecimalFormat("#.##");
                double distance = Double.valueOf(df.format(createTripList.get(0).getReturnKM() / 1000));


                tv_trip_distance_time.setText(distance + " Kms");


                iv_circle_source.setImageBitmap(Constants.getImage(createTripList.get(0).getDestination_image()));

                iv_circle_destination.setImageBitmap(Constants.getImage(createTripList.get(0).getSource_image()));
                set_header_origin_destination();
                remaining_km=createTripList.get(0).getReturnKM();
                remaining_time=createTripList.get(0).getReturnTime();
                add_hours_minute(createTripList.get(0).getReturn_start_date(), createTripList.get(0).getReturn_start_time(), createTripList.get(0).getReturnTime());


            }


            ll_places_between_S_to_D.removeAllViews();

            createPoiList = new ArrayList<>();
            createPoiList.clear();
            createPoiList = dbHandler.get_POI_TRIP(createTripList.get(0).getTripId(), return_plan);

            for (int i = 0; i < createPoiList.size(); i++) {

                PlacesBetweenStoD(i);
            }

            update_trip();

        }
    }



    public String convert_time_12_format(String time) {
        String timein12 = "";
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
        try {
            Date _24HourDt = _24HourSDF.parse(time);
            timein12 = _12HourSDF.format(_24HourDt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timein12;
    }

    public String add_hours_minute(String startdate, String starttime, Double Km) {


        String tripStartDate = startdate + " " + starttime;


        //  Log.d("startPoi",currentDateandTime);
        Date date = null;
        try {
            date = sdf.parse(tripStartDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] converttime = splitToComponentTimes(Km);
            calendar.add(Calendar.HOUR, converttime[0]);
            calendar.add(Calendar.MINUTE, converttime[1]);
            tv_end_date.setText(sdfDate.format(calendar.getTime()));
            tv_end_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
            if (return_plan == 0) {
                createTripList.get(0).setEnddate(sdfDate.format(calendar.getTime()));
                createTripList.get(0).setEndtime(sdfTime.format(calendar.getTime()));
                dbHandler.update_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
            } else {
                //  dbHandler.update_Return_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()), return_trip_plan);
                createTripList.get(0).setReturnstatus(true);
                createTripList.get(0).setReturn_end_date(sdfDate.format(calendar.getTime()));
                createTripList.get(0).setReturn_end_time(sdfTime.format(calendar.getTime()));

                dbHandler.update_return_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
            }


        } catch (ParseException e) {
            e.printStackTrace();
            tv_end_date.setText("");
            tv_end_time.setText("");
        }

        set_header_origin_destination();


        return "";
    }

    public String add_hours_minute_last(String startdate, String starttime, Double requriedtime) {
        try {


            String tripStartDate = startdate;


            //  Log.d("startPoi",currentDateandTime);
            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);

                int[] time_h_m_s=splitToComponentTimes(requriedtime);

                calendar.add(Calendar.HOUR,time_h_m_s[0] );
                calendar.add(Calendar.MINUTE, time_h_m_s[1]);
                tv_end_date.setText(sdfDate.format(calendar.getTime()));
                tv_end_time.setText(convert_time_12_format(sdfTime.format(calendar.getTime())));
                if (return_plan == 0) {
                    createTripList.get(0).setEnddate(sdfDate.format(calendar.getTime()));
                    createTripList.get(0).setEndtime(sdfTime.format(calendar.getTime()));
                    dbHandler.update_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));


                } else {

                    createTripList.get(0).setReturnstatus(true);
                    createTripList.get(0).setReturn_end_date(sdfDate.format(calendar.getTime()));
                    createTripList.get(0).setReturn_end_time(sdfTime.format(calendar.getTime()));

                    dbHandler.update_return_end_trip_date_time(tripid, sdfDate.format(calendar.getTime()), sdfTime.format(calendar.getTime()));
                }


            } catch (ParseException e) {
                e.printStackTrace();
                tv_end_time.setText("");
                tv_end_date.setText("");
            }

            set_header_origin_destination();
        } catch (Exception e) {
        }

        return "";
    }

    public void set_header_origin_destination() {

        ((ExploreActivity) getActivity()).tv_origin_city_header.setText(tv_origin.getText().toString());
        ((ExploreActivity) getActivity()).tv_dest_city_header.setText(tv_destination.getText().toString());

        if (edit_stay_time == false) {
            Log.d("System out", "edit_stay_time == false");
            iv_edit_stay_time.setImageResource(R.drawable.edit_icon_unselected_my_plan);
            tv_edit.setTextColor(getResources().getColor(R.color.colorWhite));


            //((ExploreActivity) getActivity()).ll_header_main.setEnabled(true);

            // ((ExploreActivity)getActivity()).tv_origin_date_header.setText("");
            if (tv_start_date.getText().length() > 1) {
                Log.d("System out", "edit_stay_time == false ,tv_start_date.getText().length()>1");

                ((ExploreActivity) getActivity()).ll_edit_date_header.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).ll_edit_date_header.setBackground(null);
                ((ExploreActivity) getActivity()).tv_origin_date_header.setText(tv_start_date.getText().toString() + " " + tv_start_time.getText().toString());
                ((ExploreActivity) getActivity()).tv_dest_date_header.setText(tv_end_date.getText().toString() + " " + tv_end_time.getText().toString());

            } else {
                Log.d("System out", "edit_stay_time == false ,else");

                ((ExploreActivity) getActivity()).ll_edit_date_header.setVisibility(View.INVISIBLE);
                ((ExploreActivity) getActivity()).tv_origin_date_header.setText("Start Date");
                ((ExploreActivity) getActivity()).tv_dest_date_header.setText("");
            }
        } else {
            Log.d("System out", "edit_stay_time == true");

            iv_edit_stay_time.setImageResource(R.drawable.edit_icon_selected_my_plan);
            tv_edit.setTextColor(getResources().getColor(R.color.textYellow));

            ((ExploreActivity) getActivity()).ll_edit_date_header.setBackground(getResources().getDrawable(R.drawable.textbox_edit));
            if (tv_start_date.getText().length() > 1) {
                Log.d("System out", "edit_stay_time == true ,tv_start_date.getText().length()>1");

                ((ExploreActivity) getActivity()).ll_edit_date_header.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).tv_origin_date_header.setText(tv_start_date.getText().toString() + " " + tv_start_time.getText().toString());
                ((ExploreActivity) getActivity()).tv_dest_date_header.setText(tv_end_date.getText().toString() + " " + tv_end_time.getText().toString());

            } else {
                Log.d("System out", "edit_stay_time == true ,else");

                ((ExploreActivity) getActivity()).ll_edit_date_header.setVisibility(View.VISIBLE);
                ((ExploreActivity) getActivity()).tv_origin_date_header.setText("Start Date");
                ((ExploreActivity) getActivity()).tv_dest_date_header.setText("");

            }
            //   ((ExploreActivity) getActivity()).ll_header_main.setEnabled(false);
        }

        tv_explore_from_map.setText("Explore " + tv_destination.getText().toString());
        if(return_plan==0)   //up plan
        {
           // if(createTripList.get(0).getTripstatus().contains("Saved")) {
           // if (createTripList.get(0).getReturn_start_date().length()==0) {

            if(frommytrips.equalsIgnoreCase("mytrips")||(((ExploreActivity) getActivity()).whichActivity==true))
            {
                ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
                tv_explore_from_map.setVisibility(View.GONE);

            }else {

                if (destinationplanList.size()<= 0 )
                {
                    if(createTripList.get(0).getDesNoOfNearByAttraction()==0)
                        tv_explore_from_map.setVisibility(View.GONE);
                    else
                        tv_explore_from_map.setVisibility(View.VISIBLE);
                }
                else
                {
                    tv_explore_from_map.setVisibility(View.GONE);

                }
                ll_D_to_S_from_left_drawer.setVisibility(View.VISIBLE);
                tv_D_to_S_from_left_drawer.setText("Plan My Return Route");
                //  tv_D_to_S_from_left_drawer1.setText("(" + tv_destination.getText().toString() + " to " + tv_origin.getText().toString() + ")");
                tv_D_to_S_from_left_drawer1.setText("(" + (tv_destination.getText().toString().contains(" ") ? tv_destination.getText().toString().substring(0, tv_destination.getText().toString().indexOf(" ")) : tv_destination.getText().toString()) + " to " + (tv_origin.getText().toString().contains(" ") ? tv_origin.getText().toString().substring(0, tv_origin.getText().toString().indexOf(" ")) : tv_origin.getText().toString()) + ")");
            }
         //   }


        }else
        { //down plan
           // if(createTripList.get(0).getTripstatus().contains("Saved")) {
            ll_D_to_S_from_left_drawer.setVisibility(View.GONE);

           /* if (createTripList.get(0).getStartdate().length()==0) {
                ll_D_to_S_from_left_drawer.setVisibility(View.VISIBLE);
                tv_D_to_S_from_left_drawer.setText("Plan My Route ");
                tv_D_to_S_from_left_drawer1.setText("(" + (tv_destination.getText().toString().contains(" ") ? tv_destination.getText().toString().substring(0, tv_destination.getText().toString().indexOf(" ")) : tv_destination.getText().toString()) + " to " + (tv_origin.getText().toString().contains(" ") ? tv_origin.getText().toString().substring(0, tv_origin.getText().toString().indexOf(" ")) : tv_origin.getText().toString()) + ")");
            }*/

        }
       // tv_place_of_des.setHint("Place of Stay at " + tv_destination.getText().toString());
        tv_place_of_des.setHint("Staying at  " + tv_destination.getText().toString());

        try {
            if (createTripList.get(0).getDes_address() != null) {
                tv_place_of_des.setText(createTripList.get(0).getDes_address());
            }
        } catch (Exception e) {
        }
    }

    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

    public void ViewMap() {
        FragmentManager fm = getChildFragmentManager();
        ((SupportMapFragment) fm.findFragmentById(R.id.map_home1)).getMapAsync(RoutePlanningFragment.this);
        //((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map_home1)).getMapAsync(RoutePlanningFragment.this);
    }

    private void getRouteOfPOI(Integer oId, Integer dId) {

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        pb_dialog.show();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String Json = "[{\"RouteOriginId\": \"" + oId + "\",\"RouteDesId\":\"" + dId + "\",\"CurrentLat\":\"23.0605914\", \"CurrentLong\":\"72.52878059999999\"   }]";

        Log.d("Routejson", Json);
        Call<ArrayList<RoutePojo>> call = apiService.RouteExplore(Json);
        call.enqueue(new Callback<ArrayList<RoutePojo>>() {
            @Override
            public void onResponse(Call<ArrayList<RoutePojo>> call, Response<ArrayList<RoutePojo>> response) {
                routepojo = new ArrayList<RoutePojo>();
                routepojo = response.body();
                pb_dialog.dismiss();
                if (routepojo.get(0).getResStatus() == true) {
                    source.setLatitude(Double.parseDouble(routepojo.get(0).getRouteOriginLat()));
                    source.setLongitude(Double.parseDouble(routepojo.get(0).getRouteOriginLong()));
                    destination.setLatitude(Double.parseDouble(routepojo.get(0).getRouteDestinationLat()));
                    destination.setLongitude(Double.parseDouble(routepojo.get(0).getRouteDestinationLong()));
//                Toast.makeText(MainActivity.this, response.body().getResDescription(), Toast.LENGTH_LONG).show();

                    wayPoints = "";

                    url = null;

                    List<Create_Poi> create_poiList1 = new ArrayList<>();
                    create_poiList1 = dbHandler.get_POI_TRIP(tripid, return_plan);


                    for (int i = 0; i < create_poiList1.size(); i++) {

                        wayPoints += create_poiList1.get(i).getPoiLatitude() + "," + create_poiList1.get(i).getPoiLongitude() + "|";
                    }

                    try {
                        JSONArray jsonArray = new JSONArray(routepojo.get(0).getRouteWayPoints());
                        if (jsonArray.length() > 0)
                        {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject explrObject = jsonArray.getJSONObject(i);
                                wayPoints_server += explrObject.get("lat") + "," + explrObject.get("lng") + "|";

                                //    url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, wayPoints);
                            }
                            Log.i("system", "Url of Map if--" + url);
                        } else {
                            //url = Constants.getDirectionsUrl1_without_baseurl(source, destination);
                            Log.i("system", "Url of Map else--" + url);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("routeurl", e.toString());

                    }

                    wayPoints=wayPoints+"|"+wayPoints_server;

                    url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, wayPoints);
                    Log.d("routeurl", url);
                    onAdminRoutePoiLists = new ArrayList<RoutePojo.OnAdminRoutePoiList>();
                    onAdminRouteRoLists = new ArrayList<RoutePojo.OnAdminRoutePoiList>();



                    if (routepojo.get(0).getOnAdminRoutePoiLists().get(0).getResStatus() == true) {
                        if (routepojo.get(0).getOnAdminRoutePoiLists().size() > 0) {
                            for (int i = 0; i < routepojo.get(0).getOnAdminRoutePoiLists().size(); i++) {

                                if (routepojo.get(0).getOnAdminRoutePoiLists().get(i).getRPType().contains("RO"))
                                    onAdminRouteRoLists.add(routepojo.get(0).getOnAdminRoutePoiLists().get(i));
                                else
                                    onAdminRoutePoiLists.add(routepojo.get(0).getOnAdminRoutePoiLists().get(i));
                            }
                        }
                    }
                    getRoute(url);
                    getTollbooth();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<RoutePojo>> call, Throwable t) {
                // Log error here since request failed
                t.printStackTrace();
                pb_dialog.dismiss();
            }
        });
    }

    private void getServices(Double latitude, Double longitude, final String servicetype, Integer routeid) {

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        pb_dialog.show();
        pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String Json = "[{    \"SerId\": \"0\",\"PageNo\": \"1\",\"PageSize\": \"30\", \"SerLatitude\":\"" + latitude + "\",\"SerLongitude\":\"" + longitude + "\",\"radius\":\"999999999\", \"RouteId\": \"" + routeid + "\",  \"SerServiceType\": \"" + servicetype + "\"}]";

        Log.d("servicejson", Json);


        Call<ArrayList<ServicesPojo>> call = apiService.servicesReponse(Json);
        call.enqueue(new Callback<ArrayList<ServicesPojo>>() {
            @Override
            public void onResponse(Call<ArrayList<ServicesPojo>> call, Response<ArrayList<ServicesPojo>> response) {
                pb_dialog.dismiss();
                if (response.body() != null && response.body().size() > 0) {
                    services = new ArrayList<ServicesPojo>();
                    services.clear();
                    services.addAll(response.body());
                    if (services.get(0).getResStatus() == true) {
//                    dbHandler.deleteAllService();
                        map.clear();
                        add_marker_services();

                    } else {
                        map.clear();
                        draw_route();
                        Constants.show_error_popup(getActivity(), "No " + servicetype + " services found.", iv_bottom_up_arrow);
                    }


                }

            }

            @Override
            public void onFailure(Call<ArrayList<ServicesPojo>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
            }
        });
    }





   /* private void  getMarkerBitmapFromView(final int position, final String type, final Double lat, final Double longi, final int imagepos) {
        final View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        final CircleImageView markerImageView = (CircleImageView) customMarkerView.findViewById(R.id.fc_poi);

        String path = "";


        if (type.contains("Services"))

        {

            if (services.get(position).isClick_status())
            {


                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(position).getSerId(), "Service", return_plan) != 0) {
                    path = Constants.ImagePath + services.get(position).getSerMapSelectedImage().toString();
                }
                else {
                    path = Constants.ImagePath + services.get(position).getSerCatImage().toString();
                }
            }
            else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(position).getSerId(), "Service", return_plan) != 0) {
                    path = Constants.ImagePath + services.get(position).getSerMapSelectedImage().toString();
                }
                else {
                    path = Constants.ImagePath + services.get(position).getSerCatImage().toString();
                }
            }
        } else if (type.contains("POI")) {

            //  Log.d("LatLong", type + "," + String.valueOf(routepojo.get(0).getOnAdminRoutePoiLists().get(position).getRPType()) + String.valueOf(lat) + "," + String.valueOf(longi));
            if (onAdminRoutePoiLists.get(position).isClick_status())
            {

                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);

                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(Integer.valueOf(position)).getRouteRefId(), "POI", return_plan) != 0) {

                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                }

            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(Integer.valueOf(position)).getRouteRefId(), "POI", return_plan) != 0) {

                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            }
        } else if (type.contains("RO")) {
            if (onAdminRouteRoLists.get(position).isClick_status()) {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRouteRoLists.get(Integer.valueOf(position)).getROMasterId(), "RO", return_plan) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRouteRoLists.get(Integer.valueOf(position)).getROMasterId(), "RO", return_plan) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            }
        } else if (type.contains("PoiNight")) {

            //   Log.d("LatLong", type+","+String.valueOf(routepojo.get(0).getOnAdminRoutePoiLists().get(position).getRPType())+String.valueOf(lat)+","+String.valueOf(longi));
            if (explorePoiList.get(position).isClick_status()) {

                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_night_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_poi_night_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            }
        } else if (type.contains("DesPlan")) {

            //   Log.d("LatLong", type+","+String.valueOf(routepojo.get(0).getOnAdminRoutePoiLists().get(position).getRPType())+String.valueOf(lat)+","+String.valueOf(longi));
            if (explorePoiList.get(position).isClick_status()) {

                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());
                FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
                markerImageView.setLayoutParams(parms);
                if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                }
                else
                path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
            }
        }


        final String finalPath = path;



                ImageRequest imageRequest = ImageRequestBuilder
                        .newBuilderWithSource(Uri.parse(finalPath))
                        .setAutoRotateEnabled(true)
                        .build();

                ImagePipeline imagePipeline = Fresco.getImagePipeline();
                final DataSource<CloseableReference<CloseableImage>>
                        dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


                dataSource.subscribe(new BaseBitmapDataSubscriber() {

                    @Override
                    public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                        if ( bitmap != null) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {



                                    Log.d("Bitmap", "has come" + String.valueOf(lat) + "," + String.valueOf(longi));
                            //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                            markerImageView.setImageBitmap(bitmap);
                            //   markerImageView.setImageResource(resId);
                            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                            //  customMarkerView.buildDrawingCache();
                            Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                    Bitmap.Config.ARGB_8888);

                            Canvas canvas = new Canvas(returnedBitmap);
                            canvas.drawColor(Color.BLUE, PorterDuff.Mode.SRC_IN);
                            Drawable drawable = customMarkerView.getBackground();

                            if (drawable != null)
                                drawable.draw(canvas);
                            customMarkerView.draw(canvas);

                  *//*  Drawable background = new BitmapDrawable(returnedBitmap);
                    IconFactory iconFactory = IconFactory.getInstance(getActivity());
                    icon_des = iconFactory.fromDrawable(background);*//*

                            map.addMarker(new MarkerOptions().snippet(type + "," + String.valueOf(position)).position(new LatLng(lat, longi)).icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));
                                }
                            });

                            dataSource.close();


                        }
                    }

                    @Override
                    public void onFailureImpl(DataSource dataSource) {
                        if (dataSource != null) {
                            dataSource.close();
                        }


                    }
                }, CallerThreadExecutor.getInstance());




    }*/

    private void  getMarkerBitmapFromView(final int position, final String type, final Double lat, final Double longi, final int imagepos) {

        final View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        final CircleImageView markerImageView = (CircleImageView) customMarkerView.findViewById(R.id.fc_poi);

        String path = "";
        String imagename="";

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());

        if (type.contains("Services"))

        {

            if (services.get(position).isClick_status())
            {


                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());


                if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(position).getSerId(), "Service", return_plan) != 0) {
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                } else {
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                }

            } else {

                if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(position).getSerId(), "Service", return_plan) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                } else {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                }

            }
        }
        else if (type.contains("POI"))
        {

            //  Log.d("LatLong", type + "," + String.valueOf(routepojo.get(0).getOnAdminRoutePoiLists().get(position).getRPType()) + String.valueOf(lat) + "," + String.valueOf(longi));
            if (onAdminRoutePoiLists.get(position).isClick_status())
            {

                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());

                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(Integer.valueOf(position)).getRouteRefId(), "POI", return_plan) != 0) {

                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
                } else {
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage();
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                }


            } else {

                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(Integer.valueOf(position)).getRouteRefId(), "POI", return_plan) != 0) {

                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
                } else {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage();
                }
            }
        }
        else if (type.contains("RO"))
        {
            if (onAdminRouteRoLists.get(position).isClick_status()) {

                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());

                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRouteRoLists.get(Integer.valueOf(position)).getROMasterId(), "RO", return_plan) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
                } else {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage();
                }
            } else {

                if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRouteRoLists.get(Integer.valueOf(position)).getROMasterId(), "RO", return_plan) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
                } else {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage();
                }
            }
        }
        else if (type.contains("PoiNight"))
        {

            //   Log.d("LatLong", type+","+String.valueOf(routepojo.get(0).getOnAdminRoutePoiLists().get(position).getRPType())+String.valueOf(lat)+","+String.valueOf(longi));
            if (explorePoiList.get(position).isClick_status()) {

                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());

                if (dbHandler.check_poi_night_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
                } else {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage();
                }
            } else {
                width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());

                if (dbHandler.check_poi_night_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();
                } else {
                    path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                    imagename = listServiceCategoryList.get(imagepos).getSerCatImage();
                }
            }
        }
       else if (type.equalsIgnoreCase("DesPlan"))
        {

                  if (explorePoiList.get(position).isClick_status())
                  {

                     width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, selected_map_icon_size, getResources().getDisplayMetrics());

                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                       /* path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerMapSelectedImage().toString();
                        imagename = listServiceCategoryList.get(imagepos).getSerMapSelectedImage();*/
                         path = Constants.ImagePath + "20170113_0635poi_map_pin_selected.png";
                        imagename = "20170113_0635poi_map_pin_selected.png";
                    } else {
                        /*path = Constants.ImagePath + listServiceCategoryList.get(imagepos).getSerCatImage().toString();
                        imagename = listServiceCategoryList.get(imagepos).getSerCatImage();*/
                        path = Constants.ImagePath + "20161028_0713poi_map_pin_unselected_1080.png";
                        imagename = "20161028_0713poi_map_pin_unselected_1080.png";
                    }
                  }
                  else
                  {
                    width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unselected_map_icon_size, getResources().getDisplayMetrics());

                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(position)).getPOIID()) != 0) {
                        path = Constants.ImagePath + "20170113_0635poi_map_pin_selected.png";
                        imagename = "20170113_0635poi_map_pin_selected.png";
                    } else {

                        path = Constants.ImagePath + "20161028_0713poi_map_pin_unselected_1080.png";
                        imagename = "20161028_0713poi_map_pin_unselected_1080.png";
                    }
                  }
        }

        FrameLayout.LayoutParams parms = new FrameLayout.LayoutParams(width, width);
        markerImageView.setLayoutParams(parms);

        final String finalPath = path;
        final String finalImagename=imagename;

        Log.d("check_image","name "+ finalImagename +" value "+new ImageSaver(getActivity()).check_file_exists(imagename));

        if (!new ImageSaver(getActivity()).check_file_exists(imagename))
        {


            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(Uri.parse(finalPath))
                    .setAutoRotateEnabled(true)
                    .build();

            ImagePipeline imagePipeline = Fresco.getImagePipeline();
            final DataSource<CloseableReference<CloseableImage>>
                    dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


            dataSource.subscribe(new BaseBitmapDataSubscriber() {

                @Override
                public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                    if (bitmap != null) {



                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {






                                Log.d("Bitmap", "has come" + String.valueOf(lat) + "," + String.valueOf(longi));
                                //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                markerImageView.setImageBitmap(bitmap);
                                //   markerImageView.setImageResource(resId);
                                customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                                customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
                                //  customMarkerView.buildDrawingCache();
                                Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                                        Bitmap.Config.ARGB_8888);

                                Canvas canvas = new Canvas(returnedBitmap);
                                canvas.drawColor(Color.BLUE, PorterDuff.Mode.SRC_IN);
                                Drawable drawable = customMarkerView.getBackground();

                                if (drawable != null)
                                    drawable.draw(canvas);

                                customMarkerView.draw(canvas);

                                map.addMarker(new MarkerOptions().snippet(type + "," + String.valueOf(position)).position(new LatLng(lat, longi)).icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));

                                new ImageSaver(getActivity()).
                                        setFileName(finalImagename).
                                        setExternal(true).
                                        save(bitmap);

                            }
                        });

                        dataSource.close();


                    }
                }

                @Override
                public void onFailureImpl(DataSource dataSource) {
                    if (dataSource != null) {
                        dataSource.close();
                    }


                }
            }, CallerThreadExecutor.getInstance());

        }
        else
        {
            Bitmap bitmap = new ImageSaver(getActivity()).
                    setFileName(imagename).
                    setExternal(true).
                    load();

            markerImageView.setImageBitmap(bitmap);

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());

            Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(returnedBitmap);
            canvas.drawColor(Color.BLUE, PorterDuff.Mode.SRC_IN);
            Drawable drawable = customMarkerView.getBackground();

            if (drawable != null)
                drawable.draw(canvas);

            customMarkerView.draw(canvas);

            map.addMarker(new MarkerOptions().snippet(type + "," + String.valueOf(position)).position(new LatLng(lat, longi)).icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap)));

        }


    }


    private void add_marker_ro() {
        try {
            map.clear();

            draw_route();
            if (onAdminRouteRoLists.size() > 0)
            {

                for (int i = 0; i < onAdminRouteRoLists.size(); i++) {

                    getMarkerBitmapFromView(i, "RO", Double.parseDouble(onAdminRouteRoLists.get(i).getROLatitude()), Double.parseDouble(onAdminRouteRoLists.get(i).getROLongitude()), 0);
                }

            }
        } catch (Exception e) {
        }
    }

    private void add_marker_services() {

        map.clear();
        if (services.size() > 0) {
            try {

                draw_route();
                for (int i = 0; i < services.size(); i++) {


                    getMarkerBitmapFromView(i, "Services", Double.parseDouble(services.get(i).getSerLatitude()), Double.parseDouble(services.get(i).getSerLongitude()),category_select_pos);
                }
            } catch (Exception e) {
            }
        } else {
            map.clear();
            draw_route();
            Constants.show_error_popup(getActivity(), "No service found.", iv_bottom_up_arrow);
        }

    }

    private void add_marker_poi() {

        try {
            map.clear();
            draw_route();
                     for (int i = 0; i < onAdminRoutePoiLists.size(); i++) {

                if (onAdminRoutePoiLists.get(i).getResStatus() == true) {
                     if (onAdminRoutePoiLists.get(i).getRPType().contains("POI")) {
                         getMarkerBitmapFromView(i, "POI", Double.parseDouble(onAdminRoutePoiLists.get(i).getPOILatitude()), Double.parseDouble(onAdminRoutePoiLists.get(i).getPOILongitude()), 1);

                    } else {
                        getMarkerBitmapFromView(i, "POI", Double.parseDouble(onAdminRoutePoiLists.get(i).getDesLatitude()), Double.parseDouble(onAdminRoutePoiLists.get(i).getDesLongitude()), 1);

                    }
                } else {

                    Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", iv_bottom_up_arrow);

                }
            }
        } catch (Exception e) {
        }

    }

    private void add_marker_Night_destination() {
        map.clear();
        if (explorePoiList.size() > 0) {
            try {



                map.animateCamera(CameraUpdateFactory.zoomIn());
               CameraPosition camPos = new CameraPosition
                        .Builder()
                        .zoom(12)
                        .target(new LatLng(Double.parseDouble(explorePoiList.get(0).getPOILatitude()), Double.parseDouble(explorePoiList.get(0).getPOILongitude())))
                        .build();

                map.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));





                //draw_route();
                for (int i = 0; i < explorePoiList.size(); i++) {


                    getMarkerBitmapFromView(i, "PoiNight", Double.parseDouble(explorePoiList.get(i).getPOILatitude()), Double.parseDouble(explorePoiList.get(i).getPOILongitude()), 1);
                }
            } catch (Exception e) {
            }
        } else {

            Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", iv_bottom_up_arrow);

        }
    }

    private void add_marker_destination_plan() {
        desbound = new LatLngBounds.Builder();

        for(int i=0;i<explorePoiList.size();i++)
        {
            desbound.include(new LatLng(Double.parseDouble(explorePoiList.get(i).getPOILatitude()), Double.parseDouble(explorePoiList.get(i).getPOILongitude())));
        }
        map.clear();
        if (explorePoiList.size() > 0) {
            try {

                map.animateCamera(CameraUpdateFactory.zoomIn());
                CameraPosition camPos = new CameraPosition
                        .Builder()
                        .zoom(12)
                        .target(new LatLng(Double.parseDouble(explorePoiList.get(0).getPOILatitude()), Double.parseDouble(explorePoiList.get(0).getPOILongitude())))
                        .build();

                map.animateCamera(CameraUpdateFactory.newLatLngBounds(desbound.build(),50));

                //draw_route();
                for (int i = 0; i < explorePoiList.size(); i++) {


                    getMarkerBitmapFromView(i, "DesPlan", Double.parseDouble(explorePoiList.get(i).getPOILatitude()), Double.parseDouble(explorePoiList.get(i).getPOILongitude()), 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", iv_bottom_up_arrow);

        }
    }

    private void Insert_Poi_Api(final Integer pos, final String waytype, final String wayId, final String duration, Double origindistance, final String imageurl, final Integer desid) {
        // get & set progressbar dialog


        Long distance;


        Double d = origindistance;

        distance = d.longValue();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(true);
        pb_dialog.show();


        String json = "[{     \"TripWayType\": \"" + waytype + "\",     \"TripWayRefId\": \"" + wayId + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"\" ,\"TripEstOutTime\":\"\",\"TripDesId\":" + desid + ", \"TripIsPause\":\"false\" ,\"TripResumeDateTime\":\"\",\"TripDistanceInKm\":" + distance + ",\"TripDistanceInMinute\":" + duration + ",\"TripWayIsReturnPlanned\":" + return_trip_plan + "}]";

        Log.d("insert_poi_plan", json);

        Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
        call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
            @Override
            public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {
                if (response.body() != null) {


                    if (response.body().get(0).getResStatus() == true) {
                        String url = "";
                        if (waytype.contains("Services"))
                        {
                            url = Constants.ImagePath + imageurl;
                        } else {
                            url = Constants.TimbThumb_ImagePath + imageurl + "&width=" + 50 + "&height=" + 50;

                        }
                        //stuff that updates ui
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(url))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


                        dataSource.subscribe(new BaseBitmapDataSubscriber() {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                                if (dataSource.isFinished() && bitmap != null) {

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            //stuff that updates ui


                                            Log.d("Bitmap", "has come");
                                            //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                                            if (waytype.contains("RO"))
                                            {

                                                Create_Poi create_poi = new Create_Poi();
                                                create_poi.setTripId(tripid);
                                                create_poi.setPoiName(onAdminRouteRoLists.get(pos).getROName());
                                                create_poi.setStayTime(Double.valueOf(onAdminRouteRoLists.get(pos).getROSpendTime().replace(":", ".")));
                                                create_poi.setPoiLatitude(Double.valueOf(onAdminRouteRoLists.get(pos).getROLatitude()));
                                                create_poi.setPoiLongitude(Double.valueOf(onAdminRouteRoLists.get(pos).getROLongitude()));
                                                create_poi.setKM(Math.ceil(onAdminRouteRoLists.get(pos).getDistanceFromRouteOrigin() * 1000));
                                                create_poi.setTime(Double.valueOf(onAdminRouteRoLists.get(pos).getDurationFromRouteOrigin()));
                                                create_poi.setPoiStay(false);
                                                create_poi.setArrivalTime(0.0);
                                                create_poi.setDepartureTime(0.0);

                                                if (waytype.contains("RO")) {

                                                    Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                                            R.drawable.map_slider_icon4_selected);
                                                    //  create_poi.setPoiImage(Constants.getImageBytes(icon));
                                                    new ImageSaver(getActivity()).
                                                            setFileName("ro").

                                                            setExternal(true).
                                                            save(icon);

                                                }
                                                //  create_poi.setPoiImage(Constants.getImageBytes(bitmap));

                                                create_poi.setPoitype(waytype);
                                                create_poi.setWayPointId(response.body().get(0).getTripWayPointId());

                                                create_poi.setPoiDesID(Integer.valueOf(wayId));
                                                create_poi.setPoiServerId(onAdminRouteRoLists.get(pos).getROMasterId());

                                                create_poi.setReturnstatus(return_trip_plan);
                                                create_poi.setPoidescription(onAdminRouteRoLists.get(pos).getROLocationName());
                                                create_poi.setImagename("ro");
                                                create_poi.setPoiside(onAdminRouteRoLists.get(pos).getROSide());

                                                dbHandler.add_POI_into_Table(create_poi);

                                                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                                        R.anim.bottom_down);
                                                fl_pop_up_zoo.startAnimation(bottomUpDown);
                                                fl_pop_up_zoo.setVisibility(View.GONE);


                                                draw_route_display_trip();

                                                 add_marker_ro();
                                                pb_dialog.dismiss();


                                                dataSource.close();
                                            } else if (waytype.contains("Services")) {

                                                Create_Poi create_poi = new Create_Poi();
                                                create_poi.setTripId(tripid);
                                                create_poi.setPoiName(services.get(pos).getSerName());
                                                create_poi.setStayTime(Double.valueOf(services.get(pos).getDefaultSpendTime().toString().replace(":", ".")));
                                                create_poi.setPoiLatitude(Double.valueOf(services.get(pos).getSerLatitude()));
                                                create_poi.setPoiLongitude(Double.valueOf(services.get(pos).getSerLongitude()));
                                                create_poi.setKM(Math.ceil(services.get(pos).getDistance() * 1000));
                                                create_poi.setTime(Double.valueOf(services.get(pos).getDuration()));
                                                create_poi.setPoiStay(false);
                                                create_poi.setArrivalTime(0.0);
                                                create_poi.setDepartureTime(0.0);


                                                new ImageSaver(getActivity()).
                                                        setFileName(imageurl).

                                                        setExternal(true).
                                                        save(bitmap);
                                                //  create_poi.setPoiImage(Constants.getImageBytes(bitmap));
                                                create_poi.setPoitype(waytype);
                                                create_poi.setWayPointId(response.body().get(0).getTripWayPointId());

                                                create_poi.setPoiDesID(Integer.valueOf(wayId));
                                                create_poi.setPoiServerId(services.get(pos).getSerCatId());

                                                create_poi.setReturnstatus(return_trip_plan);
                                                create_poi.setPoidescription(services.get(pos).getSerAddress1());
                                                create_poi.setImagename(imageurl);
                                                create_poi.setPoiside(services.get(pos).getSerSide());


                                                dbHandler.add_POI_into_Table(create_poi);

                                                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                                        R.anim.bottom_down);
                                                fl_pop_up_zoo.startAnimation(bottomUpDown);
                                                fl_pop_up_zoo.setVisibility(View.GONE);


                                                draw_route_display_trip();
                                                add_marker_services();

                                                pb_dialog.dismiss();


                                                dataSource.close();
                                            } else if (waytype.contains("PoiNight")) {
                                                Create_Poi_Night create_poi = new Create_Poi_Night();
                                                Log.d("nightpoiid", String.valueOf(night_poi_id));
                                                create_poi.setPoiId(night_poi_id);
                                                create_poi.setTripId(tripid);
                                                create_poi.setPoiName(explorePoiList.get(pos).getPOIName());
                                                Log.d("spendtime", explorePoiList.get(pos).getPOISpendTime().replace(":", "."));
                                                create_poi.setStayTime(Double.valueOf(explorePoiList.get(pos).getPOISpendTime().replace(":", ".")));
                                                create_poi.setPoiLatitude(Double.valueOf(explorePoiList.get(pos).getPOILatitude()));
                                                create_poi.setPoiLongitude(Double.valueOf(explorePoiList.get(pos).getPOILongitude()));
                                                create_poi.setKM(Math.ceil(explorePoiList.get(pos).getDistance() * 1000));
                                                create_poi.setTime(Double.valueOf(explorePoiList.get(pos).getDuration()));
                                                create_poi.setPoiStay(false);
                                                create_poi.setArrivalTime(0.0);
                                                create_poi.setDepartureTime(0.0);
                                                new ImageSaver(getActivity()).
                                                        setFileName(imageurl).

                                                        setExternal(true).
                                                        save(bitmap);

                                                //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));
                                                create_poi.setPoitype("PoiNight");
                                                create_poi.setWayPointId(response.body().get(0).getTripWayPointId());

                                                create_poi.setPoiDesID(explorePoiList.get(pos).getPOINearbyDestination());
                                                create_poi.setPoiServerId(explorePoiList.get(pos).getPOIID());

                                                create_poi.setReturnstatus(return_trip_plan);
                                                create_poi.setPoidescription(explorePoiList.get(pos).getPOIShortDescription());
                                                create_poi.setImagename(imageurl);
                                                dbHandler.add_Night_POI_into_Table(create_poi);

                                                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                                        R.anim.bottom_down);
                                                fl_pop_up_zoo.startAnimation(bottomUpDown);
                                                fl_pop_up_zoo.setVisibility(View.GONE);

                                                get_my_trip();
                                                pb_dialog.dismiss();


                                                dataSource.close();
                                            } else if (waytype.contains("DesPlan")) {
                                                DestinationPlan create_poi = new DestinationPlan();
                                                create_poi.setTripId(tripid);


                                                create_poi.setKM(Math.ceil(explorePoiList.get(pos).getDistance() * 1000));
                                                create_poi.setTime(Double.valueOf(explorePoiList.get(pos).getDuration()));


                                                create_poi.setArrivalTime(0.0);
                                                create_poi.setDepartureTime(0.0);

                                                new ImageSaver(getActivity()).
                                                        setFileName(imageurl).

                                                        setExternal(true).
                                                        save(bitmap);
                                                //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));


                                                create_poi.setPoiName(explorePoiList.get(pos).getPOIName());
                                                create_poi.setPoiLatitude(Double.valueOf(explorePoiList.get(pos).getPOILatitude()));
                                                create_poi.setPoiLongitude(Double.valueOf(explorePoiList.get(pos).getPOILongitude()));
                                                create_poi.setStayTime(Double.valueOf(explorePoiList.get(pos).getPOISpendTime().replace(":", ".")));

                                                create_poi.setPoidescription(explorePoiList.get(pos).getPOIShortDescription());


                                                create_poi.setPoiServerId(explorePoiList.get(pos).getPOIID());

                                                create_poi.setPoitype("DesPlan");
                                                create_poi.setWayPointId(response.body().get(0).getTripWayPointId());

                                                create_poi.setPoiDesID(explorePoiList.get(pos).getPOIID());


                                                create_poi.setImagename(imageurl);


                                                if (explorePoiList.get(pos).getPOIIsFullTime()) {
                                                    create_poi.setStarttime(24.00);
                                                    create_poi.setEndtime(24.00);
                                                } else {
                                                    String starttime = "0:0", endtime = "0:0";

                                                    try {
                                                        starttime = Constants.formatDate(explorePoiList.get(pos).getPOIStartTime(), "HH:mm:ss", "HH:mm");
                                                        endtime = Constants.formatDate(explorePoiList.get(pos).getPOIEndTime(), "HH:mm:ss", "HH:mm");

                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                        starttime = "0:0";
                                                        endtime = "0:0";
                                                    }


                                                    create_poi.setStarttime(Double.valueOf(starttime.replace(":", ".")));
                                                    create_poi.setEndtime(Double.valueOf(endtime.replace(":", ".")));
                                                }


                                                dbHandler.add_Destination_into_Table(create_poi);

                                                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                                        R.anim.bottom_down);
                                                fl_pop_up_zoo.startAnimation(bottomUpDown);
                                                fl_pop_up_zoo.setVisibility(View.GONE);

                                                set_destination_layout();
                                                pb_dialog.dismiss();


                                                dataSource.close();
                                            } else {

                                                Create_Poi create_poi = new Create_Poi();
                                                create_poi.setTripId(tripid);


                                                create_poi.setKM(Math.ceil(onAdminRoutePoiLists.get(pos).getDistanceFromRouteOrigin() * 1000));
                                                create_poi.setTime(Double.valueOf(onAdminRoutePoiLists.get(pos).getDurationFromRouteOrigin()));

                                                create_poi.setPoiStay(false);
                                                create_poi.setArrivalTime(0.0);
                                                create_poi.setDepartureTime(0.0);

                                                new ImageSaver(getActivity()).
                                                        setFileName(imageurl).

                                                        setExternal(true).
                                                        save(bitmap);
                                                //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));

                                                if (onAdminRoutePoiLists.get(pos).getRPType().contains("POI")) {
                                                    create_poi.setPoiName(onAdminRoutePoiLists.get(pos).getPOIName());
                                                    create_poi.setPoiLatitude(Double.valueOf(onAdminRoutePoiLists.get(pos).getPOILatitude()));
                                                    create_poi.setPoiLongitude(Double.valueOf(onAdminRoutePoiLists.get(pos).getPOILongitude()));
                                                    create_poi.setStayTime(Double.valueOf(onAdminRoutePoiLists.get(pos).getPOISpendTime().replace(":", ".")));
                                                    create_poi.setAttractionCount(0);
                                                    create_poi.setPoidescription(onAdminRoutePoiLists.get(pos).getPOIShortDescription());
                                                } else {
                                                    create_poi.setPoiName(onAdminRoutePoiLists.get(pos).getDesName());
                                                    create_poi.setPoiLatitude(Double.valueOf(onAdminRoutePoiLists.get(pos).getDesLatitude()));
                                                    create_poi.setPoiLongitude(Double.valueOf(onAdminRoutePoiLists.get(pos).getDesLongitude()));
                                                    create_poi.setStayTime(2.0);
                                                    //    create_poi.setAttractionCount(onAdminRoutePoiLists.get(pos).getAttractionCount());
                                                    create_poi.setAttractionCount(onAdminRoutePoiLists.get(pos).getDesNoOfNearByAttraction());
                                                    create_poi.setPoidescription(onAdminRoutePoiLists.get(pos).getDesShortDescription());
                                                }


                                                create_poi.setPoiServerId(onAdminRoutePoiLists.get(pos).getRPId());

                                                create_poi.setPoitype(onAdminRoutePoiLists.get(pos).getRPType());
                                                create_poi.setWayPointId(response.body().get(0).getTripWayPointId());


                                                create_poi.setPoiDesID(Integer.valueOf(wayId));

                                                create_poi.setReturnstatus(return_trip_plan);
                                                create_poi.setImagename(imageurl);

                                                dbHandler.add_POI_into_Table(create_poi);

                                                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                                        R.anim.bottom_down);
                                                fl_pop_up_zoo.startAnimation(bottomUpDown);
                                                fl_pop_up_zoo.setVisibility(View.GONE);


                                                draw_route_display_trip();
                                                add_marker_poi();
                                                pb_dialog.dismiss();


                                                dataSource.close();
                                            }

                                            List<Create_Poi> createPoiList1 = new ArrayList<>();
                                            createPoiList1 = dbHandler.get_POI_TRIP(createTripList.get(0).getTripId(), 0);
                                            /*if(createPoiList1.size()==1)
                                            {*/
                                                iv_left_arrow_map.performClick();
                                         //   }


                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }
                                pb_dialog.dismiss();
                            }
                        }, CallerThreadExecutor.getInstance());


                    } else {
                        Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                                R.anim.bottom_down);
                        fl_pop_up_zoo.startAnimation(bottomUpDown);
                        fl_pop_up_zoo.setVisibility(View.GONE);

                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().get(0).getResDescription(), iv_bottom_up_arrow);
                    }
                } else {
                    Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                            R.anim.bottom_down);
                    fl_pop_up_zoo.startAnimation(bottomUpDown);
                    fl_pop_up_zoo.setVisibility(View.GONE);

                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();
                Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                        R.anim.bottom_down);
                fl_pop_up_zoo.startAnimation(bottomUpDown);
                fl_pop_up_zoo.setVisibility(View.GONE);


                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });


    }

    private void Delete_Poi_Api(final Integer waypointid, final String type, String json) {
        // get & set progressbar dialog

       if(!pb_dialog.isShowing())
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        Log.d("json_CreateTrip", json);

        Call<ArrayList<RemoveTripPoi>> call = apiService.RemovePoi(json);
        call.enqueue(new Callback<ArrayList<RemoveTripPoi>>() {
            @Override
            public void onResponse(Call<ArrayList<RemoveTripPoi>> call, final Response<ArrayList<RemoveTripPoi>> response) {
                if (response.body() != null)
                {


                    if (response.body().get(0).getResStatus() == true)
                    {




                            dbHandler.delete_POI_ById(waypointid);

                            draw_route_display_trip();
                           set_selected_map_icon();
                           pb_dialog.dismiss();



                    } else
                    {

                        if(old_direction_response !=null)
                        {
                            route_direction = new Direction();
                            route_direction = old_direction_response;
                        }
                        pb_dialog.dismiss();

                        Constants.show_error_popup(getActivity(), response.body().get(0).getResDescription(), iv_bottom_up_arrow);
                    }
                } else {
                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RemoveTripPoi>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });


    }

    public void draw_route()
    {
        if (routelineresult != null && map != null)

        {

            if(polylineFinal!=null)
            {
                polylineFinal.remove();
            }


            if (return_plan == 0)
            {
                map.addMarker(new MarkerOptions().title(createTripList.get(0).getSource().toString()).snippet("point").position(new LatLng(createTripList.get(0).getSource_latitude(), createTripList.get(0).getSource_longitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.round_blue_map)));
                map.addMarker(new MarkerOptions().title(createTripList.get(0).getDestination().toString()).snippet("point").position(new LatLng(createTripList.get(0).getDestination_latitude(), createTripList.get(0).getDestination_longitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.round_red_map)));
            } else {
                map.addMarker(new MarkerOptions().title(createTripList.get(0).getSource().toString()).snippet("point").position(new LatLng(createTripList.get(0).getSource_latitude(), createTripList.get(0).getSource_longitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.round_red_map)));
                map.addMarker(new MarkerOptions().title(createTripList.get(0).getDestination().toString()).snippet("point").position(new LatLng(createTripList.get(0).getDestination_latitude(), createTripList.get(0).getDestination_longitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.round_blue_map)));

            }




            ArrayList<LatLng> points = new ArrayList<>();
            PolylineOptions lineOptions = new PolylineOptions();
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < routelineresult.size(); i++)
            {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = routelineresult.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    b1.include(position);
                    points.add(position);
                    //  Constants.points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(getActivity().getResources().getColor(R.color.colorAccent));
            }
            LatLngBounds bounds = b1.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);

            polylineFinal=     map.addPolyline(lineOptions);
            map.animateCamera(cu);
        }
    }

    private void getListServices_Category() {
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String Json = "[{     \"SerCatId\": \"0\"}]";

        Call<ArrayList<ListServiceCategory>> call = apiService.ListServiceCategory(Json);
        call.enqueue(new Callback<ArrayList<ListServiceCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ListServiceCategory>> call, Response<ArrayList<ListServiceCategory>> response) {
                if (response.body().get(0).getResStatus() == true) {
                    listServiceCategoryList = new ArrayList<ListServiceCategory>();
                    listServiceCategoryList.clear();
                    listServiceCategoryList.addAll(response.body());
                    for (int i = 0; i < listServiceCategoryList.size(); i++) {
                        listServiceCategoryList.get(i).setStatus(false);
                    }

                    listServiceCategoryList.get(0).setStatus(true);
                    serviceListAdapter.notifyDataSetChanged();

                    pb_dialog.dismiss();
                    //   FacilityContent();

                } else {
                    pb_dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ListServiceCategory>> call, Throwable t) {
                // Log error here since request failed
                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }
            }
        });
    }

    private void getPlaces(String search) {


        ApiInterface apiService = ApiClient.get_retrofit_client_google_autocomplete().create(ApiInterface.class);
        Call<Place> call = apiService.GetPlaces("json?input=" + search + "," + createTripList.get(0).getDestination() + "&sensor=false&key=AIzaSyCd1xkg-MlZlS-2MBGAFWBp5wcWEVoYidk");
        call.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {
                if (response.body() != null) {

                    placeList = new ArrayList<String>();
                    placeList.clear();

                    for (int i = 0; i < response.body().getPredictions().size(); i++) {
                        placeList.add(response.body().getPredictions().get(i).getDescription());
                    }
                    adapter = new ArrayAdapter<String>(getActivity(), R.layout.autocomplete, placeList);
                    // tv_place_of_des.setThreshold(1);
                    tv_place_of_des.setAdapter(adapter);
                    adapter.setNotifyOnChange(true);

                    adapter.notifyDataSetChanged();


                }


            }

            @Override
            public void onFailure(Call<Place> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });

    }

    private void getPOIAPI(Integer destId, Double destLat, Double destLong) {
        // get & set progressbar dialog

        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "";

        json = "[{\"PoiUmId\": \"0\",\"POIID\":\"0\" ,\"radius\":\"100\"   , \"POINearbyDestination\": \"" + destId + "\",  \"PageNo\":1,\"PageSize\":100, \"POILongitude\":\"" + destLong + "\",\"POILatitude\":\"" + destLat + "\",\"CategoryName\":\"Attraction\" , \"POIRating\":0 }]";

        Log.d("System out", "Explore POI____" + json);


        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {


                pb_dialog.dismiss();

                if (response.body() != null) {


                    explorePoiList = new ArrayList<ExplorePOI>();
                    explorePoiList.clear();
                    explorePoiList.addAll(response.body());
                    if (explorePoiList.size() > 0) {
                        if (explorePoiList.get(0).getResStatus() == true) {

                            add_marker_Night_destination();

                            //  Collections.reverse(exploreDestList);


                        }

                    } else {
                        pb_dialog.dismiss();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {


        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.6;
        Log.d("Distnace", String.valueOf(lat1) + "," + String.valueOf(lon1) + "," + String.valueOf(lat2) + "," + String.valueOf(lat1) + "," + String.valueOf(lon2) + ",distance" + String.valueOf(dist));
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public Double get_time_based_on_KM(Double KM) {


        Double time = 0.0;


        time = KM / avgSpeed;


        return time;
    }

    public Double[] TotalKM_Night_Destination(int pos, Integer returnstatus) {

        Double totalKm[] = {0.0, 0.0};

        List<Create_Poi_Night> create_poi_nightsList = new ArrayList<>();

        create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(createPoiList.get(pos).getTripId(), createPoiList.get(pos).getPoiDesID(), returnstatus);

        if (create_poi_nightsList.size() > 0) {


            if (create_poi_nightsList.size() == 1) {
                totalKm[0] = create_poi_nightsList.get(0).getKM();
                totalKm[1] = Double.valueOf(Constants.convert_minute_hrs1(create_poi_nightsList.get(0).getTime())) + create_poi_nightsList.get(0).getStayTime();
                Log.d("Total1time", totalKm[1].toString());
            } else {

                for (int j = 0; j < create_poi_nightsList.size(); j++) {

                    if (j > 0) {
                        int previouspos = j - 1;

                        Double distance = distance(create_poi_nightsList.get(j).getPoiLatitude(), create_poi_nightsList.get(j).getPoiLongitude(), create_poi_nightsList.get(previouspos).getPoiLatitude(), create_poi_nightsList.get(previouspos).getPoiLongitude());


                        totalKm[0] = totalKm[0] + distance * 1000;
                        totalKm[1] = totalKm[1] + get_time_based_on_KM(distance) + create_poi_nightsList.get(j).getStayTime();

                        Log.d("totalKm", totalKm[1].toString());


                    } else {
                        totalKm[0] = create_poi_nightsList.get(0).getKM();
                        totalKm[1] = Double.valueOf(Constants.convert_minute_hrs1(create_poi_nightsList.get(0).getTime())) + create_poi_nightsList.get(0).getStayTime();

                    }

                }
            }
        }
        return totalKm;
    }

    public Double[] TotalKM_Night_Destination1(int pos, Integer returnstatus) {

        Double totalKm[] = {0.0, 0.0};

        List<Create_Poi_Night> create_poi_nightsList = new ArrayList<>();

        create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(tempcreatePoiList.get(pos).getTripId(), tempcreatePoiList.get(pos).getPoiDesID(), returnstatus);

        if (create_poi_nightsList.size() > 0) {


            if (create_poi_nightsList.size() == 1) {
                totalKm[0] = create_poi_nightsList.get(0).getKM();
                totalKm[1] = Double.valueOf(Constants.convert_minute_hrs1(create_poi_nightsList.get(0).getTime())) + create_poi_nightsList.get(0).getStayTime();
                Log.d("Total1time", totalKm[1].toString());
            } else {

                for (int j = 0; j < create_poi_nightsList.size(); j++) {

                    if (j > 0) {
                        int previouspos = j - 1;

                        Double distance = distance(create_poi_nightsList.get(j).getPoiLatitude(), create_poi_nightsList.get(j).getPoiLongitude(), create_poi_nightsList.get(previouspos).getPoiLatitude(), create_poi_nightsList.get(previouspos).getPoiLongitude());


                        totalKm[0] = totalKm[0] + distance * 1000;
                        totalKm[1] = totalKm[1] + get_time_based_on_KM(distance) + create_poi_nightsList.get(j).getStayTime();

                        Log.d("totalKm", totalKm[1].toString());


                    } else {
                        totalKm[0] = create_poi_nightsList.get(0).getKM();
                        totalKm[1] = Double.valueOf(Constants.convert_minute_hrs1(create_poi_nightsList.get(0).getTime())) + create_poi_nightsList.get(0).getStayTime();

                    }

                }
            }
        }
        return totalKm;
    }

    public void update_trip_start_date() {

        final Dialog myTripDialog = new Dialog(getActivity());

        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.plan_trip_from_to_popup);
        final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        tv_start_date_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_return_trip);
        tv_start_time_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_time_my_return_trip);

        TextView tv_source = (TextView) myTripDialog.findViewById(R.id.tv_source);
        TextView tv_des = (TextView) myTripDialog.findViewById(R.id.tv_des);

        String date = "", time = "";
        dpd = DatePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );


        tpd = TimePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );


        if (S_To_D_Plan == 2)
        {
            tv_source.setText(createTripList.get(0).getDestination());
            tv_des.setText(createTripList.get(0).getSource());

            date = createTripList.get(0).getEnddate();
            time = createTripList.get(0).getEndtime();
            tv_start_date_my_return_trip.setText(createTripList.get(0).getReturn_start_date());


            try {
                tv_start_time_my_return_trip.setText(Constants.formatDate(String.valueOf(createTripList.get(0).getReturn_start_time()), "HH:mm", "hh:mm a"));
            } catch (ParseException e) {
                e.printStackTrace();

            }
            select_dpd_type = "return";
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");
            String currentDateandTime = sdf.format(new Date());

            String arrival_date = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime();
            Calendar    calendar = Calendar.getInstance();
            try {
                calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));

            } catch (ParseException e) {
                e.printStackTrace();
            }
            Double[] des_plan_date_time_eta = get_no_days_destination_plan();

            calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

            calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
            calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

            arrival_date = sdf.format(calendar.getTime());
            Date datecurrent = null;
            try {
                datecurrent = sdf.parse(arrival_date);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);
                calendar1.add(Calendar.DATE, 0);
                dpd.setMinDate(calendar1);
            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else if (S_To_D_Plan == 0)
        {
            tv_source.setText(createTripList.get(0).getSource());
            tv_des.setText(createTripList.get(0).getDestination());

            date = createTripList.get(0).getStartdate();
            time = createTripList.get(0).getStarttime();

            select_dpd_type = "return";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
            String currentDateandTime = sdf.format(new Date());

            Date datecurrent = null;
            try {
                datecurrent = sdf.parse(currentDateandTime);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);
                calendar1.add(Calendar.DATE, 0);
                dpd.setMinDate(calendar1);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (date != null && date.length() > 1)
            {
                tv_start_date_my_return_trip.setText(date);


                try {
                    tv_start_time_my_return_trip.setText(Constants.formatDate(String.valueOf(time), "HH:mm", "hh:mm a"));
                } catch (ParseException e) {
                    e.printStackTrace();

                }

            }



        }


        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");


        tv_start_date_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //select="start";
                select_dpd_type = "return";
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


            }

        });

        tv_start_time_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  select="stop";
                select_dpd_type = "return";
                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");


            }
        });

        tv_plan_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_plan_my_return_trip);
        tv_plan_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


             /*   if (tv_start_date_my_return_trip.getText().toString().equalsIgnoreCase(""))

                {
                    Constants.show_error_popup(getActivity(), "Select your trip dates.", iv_bottom_up_arrow);
                } else if (tv_start_time_my_return_trip.getText().toString().equalsIgnoreCase("")) {
                    Constants.show_error_popup(getActivity(), "Please select trip start  time.", iv_bottom_up_arrow);
                } else {

                    String start_date = "", end_time = "";

                    try {
                        start_date = tv_start_date_my_return_trip.getText().toString();
                        end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    if (S_To_D_Plan == 0)
                    {
                        return_plan = 0;
                        return_trip_plan = false;
                        tv_place_of_des.setVisibility(View.VISIBLE);



                        dbHandler.update_Start_trip_date_time(tripid, start_date, end_time);

                        get_my_trip1();

                        dbHandler.update_Start_trip_date_time(tripid, createTripList.get(0).getStartdate(),createTripList.get(0).getStarttime() );


                        Boolean resume_date_valid = true;
                        Integer pos1 = 0;
                        Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                        for (int i = 0; i < tempcreatePoiList.size(); i++) {

                            if (tempcreatePoiList.get(i).getPoiStay())
                            {

                                String arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();


                                Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                if (tempcreatePoiList.get(i).getResumeDate() == null)
                                {
                                    resume_date_valid = true;
                                }
                                else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                    resume_date_valid = false;
                                    pos1 = i;
                                    break;
                                }

                            }


                        }


                        if (!resume_date_valid) {
                            Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination.", iv_bottom_up_arrow);

                        }
                        else if(!CheckDates(tempcreateTripList.get(0).getEnddate()+" "+tempcreateTripList.get(0).getEndtime(),tempcreateTripList.get(0).getReturn_start_date()+" "+tempcreateTripList.get(0).getReturn_start_time()) && return_plan==0 &&  tempcreateTripList.get(0).getReturnstatus())
                        {

                            Constants.show_error_popup(getActivity(), "Update start date & time of your return trip.", iv_bottom_up_arrow);
                        }
                        else {


                            dbHandler.update_Start_trip_date_time(tripid, start_date, end_time);
                            if (createTripList.get(0).getReturn_start_date().length() > 1) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


                                long date_diff_value[] = get_diff_between_date_hours(createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime(), createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time());
                                long return_date_diff[] = get_diff_between_date_hours(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time(), createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time());


                                createTripList.clear();
                                createTripList = dbHandler.get_TRIP_By_TripId(tripid);


                                try {
                                    Calendar calendar1 = Calendar.getInstance();
                                    calendar1.setTime(dateFormat.parse(createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime()));

                                    calendar1.add(Calendar.DATE, (int) date_diff_value[0]);
                                    calendar1.add(Calendar.HOUR, (int) date_diff_value[1]);
                                    calendar1.add(Calendar.MINUTE, (int) date_diff_value[2]);


                                    String return_start_date_time = dateFormat.format(calendar1.getTime());
                                    String return_start_date = Constants.formatDate(return_start_date_time, "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy");
                                    String return_start_time = Constants.formatDate(return_start_date_time, "dd MMM ,yyyy HH:mm", "HH:mm");


                                    createTripList.get(0).setReturn_start_date(return_start_date);
                                    createTripList.get(0).setReturn_start_time(return_start_time);

                                    Calendar calendar2 = Calendar.getInstance();
                                    //     hours=get_diff_between_date_hours(createTripList.get(0).getReturn_start_date()+" "+createTripList.get(0).getReturn_start_time(),createTripList.get(0).getReturn_end_date()+" "+createTripList.get(0).getReturn_end_time());
                                    calendar2.setTime(dateFormat.parse(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time()));
                                    calendar2.add(Calendar.DATE, (int) return_date_diff[0]);
                                    calendar2.add(Calendar.HOUR, (int) return_date_diff[1]);
                                    calendar2.add(Calendar.MINUTE, (int) return_date_diff[2]);

                                    String return_end_date_time = dateFormat.format(calendar2.getTime());

                                    String return_end_date = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy");
                                    String return_end_time = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "HH:mm");


                                    dbHandler.update_Return_trip_date_time(tripid, return_start_date, return_start_time, return_trip_plan);
                                    dbHandler.update_return_end_trip_date_time(tripid, return_end_date, return_end_time);


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                            }


                            myTripDialog.dismiss();


                            get_my_trip();

                            selected_poi_pos = -1;
                            selected_ro_pos = -1;
                            selected_ser_pos = -1;
                            selected_des_pos = -1;

                            for (int i = 0; i < listServiceCategoryList.size(); i++) {
                                listServiceCategoryList.get(i).setStatus(false);
                            }
                            listServiceCategoryList.get(0).setStatus(true);
                            serviceListAdapter.notifyDataSetChanged();

                            if (update_start_date_resume) {
                                update_start_date_resume = false;
                                iv_poi_night_stay.performClick();
                            }


                        }





                    }

                    else {

                        String arrival_date = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime();

                        if (destinationplanList.size() > 0) {
                            Calendar calendar = Calendar.getInstance();
                            try {
                                calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Double[] des_plan_date_time_eta = get_no_days_destination_plan();

                            calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

                            calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
                            calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

                            arrival_date = sdf.format(calendar.getTime());


                            Log.d("resumetimedparrival_hrs", des_plan_date_time_eta[1].intValue() + " min " + des_plan_date_time_eta[2].intValue());


                        }


                        if (CheckDates(arrival_date, start_date + " " + end_time))
                        {

                            dbHandler.update_Return_trip_status(tripid);

                            dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, return_trip_plan);

                            get_my_trip1();

                            dbHandler.update_Return_trip_date_time(tripid, createTripList.get(0).getReturn_start_date(),createTripList.get(0).getReturn_end_time(),createTripList.get(0).getReturnstatus());


                            Boolean resume_date_valid = true;
                            Integer pos1 = 0;
                            Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                            for (int i = 0; i < tempcreatePoiList.size(); i++) {

                                if (tempcreatePoiList.get(i).getPoiStay())
                                {

                                     arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();


                                    Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                    if (tempcreatePoiList.get(i).getResumeDate() == null)
                                    {
                                        resume_date_valid = true;
                                    }
                                    else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                        resume_date_valid = false;
                                        pos1 = i;
                                        break;
                                    }

                                }


                            }


                            if (!resume_date_valid) {
                                Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination", iv_bottom_up_arrow);

                            }

                            else {


                                dbHandler.update_Return_trip_status(tripid);
                                tv_place_of_des.setVisibility(View.GONE);
                                dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, return_trip_plan);

                                myTripDialog.dismiss();


                                get_my_trip();


                                selected_poi_pos = -1;
                                selected_ro_pos = -1;
                                selected_ser_pos = -1;
                                selected_des_pos = -1;

                                for (int i = 0; i < listServiceCategoryList.size(); i++) {
                                    listServiceCategoryList.get(i).setStatus(false);
                                }
                                listServiceCategoryList.get(0).setStatus(true);
                                serviceListAdapter.notifyDataSetChanged();
                                if (update_start_date_resume)
                                {
                                    update_start_date_resume = false;
                                    iv_poi_night_stay.performClick();
                                }
                            }
                        } else {


                            Constants.show_error_popup(getActivity(), "Please change trip return date time.", iv_bottom_up_arrow);

                        }
                    }


                }


            }
        });
        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);

        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

        if (return_trip_plan && createTripList.get(0).getTripstatus().contains("Saved")) {
            Constants.show_error_popup(getActivity(), "Provide the start date & time of your trip. ", iv_bottom_up_arrow);
        } else {
            myTripDialog.show();
        }

*/
                if (tv_start_date_my_return_trip.getText().toString().equalsIgnoreCase(""))

                {
                    Constants.show_error_popup(getActivity(), "Select your trip dates.", iv_bottom_up_arrow);
                } else if (tv_start_time_my_return_trip.getText().toString().equalsIgnoreCase("")) {
                    Constants.show_error_popup(getActivity(), "Please select trip start  time.", iv_bottom_up_arrow);
                } else {

                    String start_date = "", end_time = "";

                    try {
                        start_date = tv_start_date_my_return_trip.getText().toString();
                        end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    if (S_To_D_Plan == 0)
                    {
                        return_plan = 0;
                        return_trip_plan = false;
                        tv_place_of_des.setVisibility(View.VISIBLE);


                        dbHandler.update_Start_trip_date_time(tripid, start_date, end_time);

                        get_my_trip1();

                        dbHandler.update_Start_trip_date_time(tripid, createTripList.get(0).getStartdate(), createTripList.get(0).getStarttime());


                        Boolean resume_date_valid = true;
                        Integer pos1 = 0;
                        Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                        for (int i = 0; i < tempcreatePoiList.size(); i++) {

                            if (tempcreatePoiList.get(i).getPoiStay()) {

                                String arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();


                                Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                if (tempcreatePoiList.get(i).getResumeDate() == null) {
                                    resume_date_valid = true;
                                } else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                    resume_date_valid = false;
                                    pos1 = i;
                                    break;
                                }

                            }


                        }


                        if (!resume_date_valid) {
                            //Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination.", iv_bottom_up_arrow);
                            Constants.show_error_popup(getActivity(), "Update date & time to resume your trip from " + tempcreatePoiList.get(pos1).getPoiName() + ".", iv_bottom_up_arrow);

                        } else if (!CheckDates(tempcreateTripList.get(0).getEnddate() + " " + tempcreateTripList.get(0).getEndtime(), tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time()) && return_plan == 0 && tempcreateTripList.get(0).getReturnstatus()) {

                            Constants.show_error_popup(getActivity(), "Start date of your trip can't be after the return date.", iv_bottom_up_arrow);
                        } else {


                            try {
                                pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                                pb_dialog.setCancelable(false);
                                pb_dialog.show();
                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                String tripdate = start_date + " " + end_time + ":00";
                                String tripenddate = "";

                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm:ss");
                                Date date = null;

                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(createTripList.get(0).getTime());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta", date.toString());
                                    tripenddate = sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    tripenddate = "";

                                }


                                try {
                                    tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    tripdate = "";

                                }

                                try {
                                    tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {

                                    tripenddate = "";
                                }


                                String json = "[{ \"TripName\": \"" + createTripList.get(0).getTripname() + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(0).getSourceId() + ",\"TripDestination_End\":" + createTripList.get(0).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripId\":\"" + createTripList.get(0).getTripId() + "\",\"TripStatus\": \"" + "UpComing" +  "\"}]";
                                Log.d("json_CreateTrip1", json);

                                Call<UpdateTrip> call = apiService.updateTrip(json);

                                final String finalStart_date = start_date;
                                final String finalEnd_time = end_time;
                                call.enqueue(new Callback<UpdateTrip>() {
                                    @Override
                                    public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                                        if (response.body() != null) {

                                            pb_dialog.dismiss();
                                            if (response.body().getResStatus() == true) {
                                                dbHandler.update_Start_trip_date_time(tripid, finalStart_date, finalEnd_time);


                                                if (createTripList.get(0).getReturn_start_date().length() > 1) {
                                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


                                                    long date_diff_value[] = get_diff_between_date_hours(createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime(), createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time());
                                                    long return_date_diff[] = get_diff_between_date_hours(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time(), createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time());


                                                    createTripList.clear();
                                                    createTripList = dbHandler.get_TRIP_By_TripId(tripid);


                                                    try {
                                                        Calendar calendar1 = Calendar.getInstance();
                                                        calendar1.setTime(dateFormat.parse(createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime()));

                                                        calendar1.add(Calendar.DATE, (int) date_diff_value[0]);
                                                        calendar1.add(Calendar.HOUR, (int) date_diff_value[1]);
                                                        calendar1.add(Calendar.MINUTE, (int) date_diff_value[2]);


                                                        String return_start_date_time = dateFormat.format(calendar1.getTime());
                                                        String return_start_date = Constants.formatDate(return_start_date_time, "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy");
                                                        String return_start_time = Constants.formatDate(return_start_date_time, "dd MMM ,yyyy HH:mm", "HH:mm");


                                                        createTripList.get(0).setReturn_start_date(return_start_date);
                                                        createTripList.get(0).setReturn_start_time(return_start_time);

                                                        Calendar calendar2 = Calendar.getInstance();
                                                        //     hours=get_diff_between_date_hours(createTripList.get(0).getReturn_start_date()+" "+createTripList.get(0).getReturn_start_time(),createTripList.get(0).getReturn_end_date()+" "+createTripList.get(0).getReturn_end_time());
                                                        calendar2.setTime(dateFormat.parse(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time()));
                                                        calendar2.add(Calendar.DATE, (int) return_date_diff[0]);
                                                        calendar2.add(Calendar.HOUR, (int) return_date_diff[1]);
                                                        calendar2.add(Calendar.MINUTE, (int) return_date_diff[2]);

                                                        String return_end_date_time = dateFormat.format(calendar2.getTime());

                                                        String return_end_date = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy");
                                                        String return_end_time = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "HH:mm");


                                                        dbHandler.update_Return_trip_date_time(tripid, return_start_date, return_start_time, return_trip_plan);
                                                        dbHandler.update_return_end_trip_date_time(tripid, return_end_date, return_end_time);


                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }


                                                }


                                                myTripDialog.dismiss();


                                                get_my_trip();

                                                selected_poi_pos = -1;
                                                selected_ro_pos = -1;
                                                selected_ser_pos = -1;
                                                selected_des_pos = -1;

                                                for (int i = 0; i < listServiceCategoryList.size(); i++) {
                                                    listServiceCategoryList.get(i).setStatus(false);
                                                }
                                                listServiceCategoryList.get(0).setStatus(true);
                                                serviceListAdapter.notifyDataSetChanged();

                                                if (update_start_date_resume) {
                                                    update_start_date_resume = false;
                                                    iv_poi_night_stay.performClick();
                                                }
                                            } else {
                                                Constants.show_error_popup(getActivity(), "" + response.body().getResDescription(), getActivity().findViewById(R.id.iv_bottom_up_arrow));
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<UpdateTrip> call, Throwable t) {
                                        // Log error here since request failed

                                        t.printStackTrace();
                                        pb_dialog.dismiss();

                                        if (!Constants.isInternetAvailable(getActivity())) {
                                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.iv_bottom_up_arrow));
                                        } else {
                                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.iv_bottom_up_arrow));
                                        }

                                    }
                                });

                            } catch (Exception e) {
                            }


                        }
                    }


                    else {

                        String arrival_date = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime();

                        if (destinationplanList.size() > 0) {
                            Calendar calendar = Calendar.getInstance();
                            try {
                                calendar.setTime(sdfDate.parse(createTripList.get(0).getEnddate()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Double[] des_plan_date_time_eta = get_no_days_destination_plan();

                            calendar.add(Calendar.DATE, des_plan_date_time_eta[0].intValue());

                            calendar.add(Calendar.HOUR, des_plan_date_time_eta[1].intValue());
                            calendar.add(Calendar.MINUTE, des_plan_date_time_eta[2].intValue());

                            arrival_date = sdf.format(calendar.getTime());


                            Log.d("resumetimedparrival_hrs", des_plan_date_time_eta[1].intValue() + " min " + des_plan_date_time_eta[2].intValue());


                        }


                        if (CheckDates(arrival_date, start_date + " " + end_time))
                        {

                            dbHandler.update_Return_trip_status(tripid);

                            dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, return_trip_plan);

                            get_my_trip1();

                            dbHandler.update_Return_trip_date_time(tripid, createTripList.get(0).getReturn_start_date(),createTripList.get(0).getReturn_end_time(),createTripList.get(0).getReturnstatus());


                            Boolean resume_date_valid = true;
                            Integer pos1 = 0;
                            Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                            for (int i = 0; i < tempcreatePoiList.size(); i++) {

                                if (tempcreatePoiList.get(i).getPoiStay())
                                {

                                    arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();


                                    Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                    if (tempcreatePoiList.get(i).getResumeDate() == null)
                                    {
                                        resume_date_valid = true;
                                    }
                                    else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                        resume_date_valid = false;
                                        pos1 = i;
                                        break;
                                    }

                                }


                            }


                            if (!resume_date_valid) {
                                //Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination", iv_bottom_up_arrow);
                                Constants.show_error_popup(getActivity(), "Update date & time to resume your trip from " + tempcreatePoiList.get(pos1).getPoiName() + ".", iv_bottom_up_arrow);

                            }

                            else {


                                dbHandler.update_Return_trip_status(tripid);
                                tv_place_of_des.setVisibility(View.GONE);
                                dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, return_trip_plan);

                                myTripDialog.dismiss();


                                get_my_trip();


                                selected_poi_pos = -1;
                                selected_ro_pos = -1;
                                selected_ser_pos = -1;
                                selected_des_pos = -1;

                                for (int i = 0; i < listServiceCategoryList.size(); i++) {
                                    listServiceCategoryList.get(i).setStatus(false);
                                }
                                listServiceCategoryList.get(0).setStatus(true);
                                serviceListAdapter.notifyDataSetChanged();
                                if (update_start_date_resume)
                                {
                                    update_start_date_resume = false;
                                    iv_poi_night_stay.performClick();
                                }
                            }
                        } else {


                            Constants.show_error_popup(getActivity(), "Please change trip return date time.", iv_bottom_up_arrow);

                        }
                    }


                }


            }
        });
        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);

        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });

        if (return_trip_plan && createTripList.get(0).getTripstatus().contains("Saved")) {
            Constants.show_error_popup(getActivity(), "Provide the start date & time of your trip. ", iv_bottom_up_arrow);
        } else {
            myTripDialog.show();
        }

    }


    public void update_trip_return_date(String msg) {
        final Dialog myTripDialog = new Dialog(getActivity());

        myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myTripDialog.setContentView(R.layout.plan_trip_from_to_popup);
        final LinearLayout ll_start_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_start_date_my_trip);
        final LinearLayout ll_return_date_my_trip = (LinearLayout) myTripDialog.findViewById(R.id.ll_return_date_my_trip);
        tv_start_date_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_date_my_return_trip);
        tv_start_time_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_start_time_my_return_trip);
        TextView tv_error_msg = (TextView) myTripDialog.findViewById(R.id.tv_error_msg);
        TextView tv_source = (TextView) myTripDialog.findViewById(R.id.tv_source);
        TextView tv_des = (TextView) myTripDialog.findViewById(R.id.tv_des);
        tv_error_msg.setVisibility(View.VISIBLE);

            tv_error_msg.setText( msg);



        String date = "", time = "";
        final TextView tv_plan_my_return_trip = (TextView) myTripDialog.findViewById(R.id.tv_plan_my_return_trip);


        dpd = DatePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );


        tpd = TimePickerDialog.newInstance(
                RoutePlanningFragment.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE), false
        );


        tv_source.setText(createTripList.get(0).getDestination());
        tv_des.setText(createTripList.get(0).getSource());

        date = createTripList.get(0).getEnddate();
        time = createTripList.get(0).getEndtime();
        tv_start_date_my_return_trip.setText(createTripList.get(0).getReturn_start_date());


        try {
            tv_start_time_my_return_trip.setText(Constants.formatDate(String.valueOf(createTripList.get(0).getReturn_start_time()), "HH:mm", "hh:mm a"));
        } catch (ParseException e) {
            e.printStackTrace();

        }
        select_dpd_type = "return";
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM ,yyyy");
        Date date1 = null;
        try {
            date1 = sdf1.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date1);

            dpd.setMinDate(calendar);
        } catch (ParseException e) {
            e.printStackTrace();
        }





        tv_start_date_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //select="start";
                select_dpd_type = "return";
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");


            }

        });

        tv_start_time_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  select="stop";
                select_dpd_type = "return";
                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");


            }
        });


        tv_plan_my_return_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (tv_start_date_my_return_trip.getText().toString().equalsIgnoreCase(""))

                {
                    Constants.show_error_popup(getActivity(), "Select your trip dates.", iv_bottom_up_arrow);
                } else if (tv_start_time_my_return_trip.getText().toString().equalsIgnoreCase("")) {
                    Constants.show_error_popup(getActivity(), "Please select trip start  time.", iv_bottom_up_arrow);
                } else {

                    String start_date = "", end_time = "";

                    try {
                        start_date = tv_start_date_my_return_trip.getText().toString();
                        end_time = Constants.formatDate(tv_start_time_my_return_trip.getText().toString(), "hh:mm a", "HH:mm");

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    String arrival_date = tempcreateTripList.get(0).getEnddate() + " " + tempcreateTripList.get(0).getEndtime();
                    Log.d("System out","arrival date_______"+ arrival_date);
                    Log.d("System out","start date_______"+ start_date + " end date__" + end_time);


                    if (CheckDates(arrival_date, start_date + " " + end_time))
                    {

                        dbHandler.update_Return_trip_status(tripid);

                        dbHandler.update_Return_trip_date_time(tripid, start_date, end_time, true);


                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


                        long return_date_diff[] = get_diff_between_date_hours(createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time(), createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time());


                        try {


                            Calendar calendar2 = Calendar.getInstance();
                            //     hours=get_diff_between_date_hours(createTripList.get(0).getReturn_start_date()+" "+createTripList.get(0).getReturn_start_time(),createTripList.get(0).getReturn_end_date()+" "+createTripList.get(0).getReturn_end_time());
                            calendar2.setTime(dateFormat.parse(start_date + " " + end_time));
                            calendar2.add(Calendar.DATE, (int) return_date_diff[0]);
                            calendar2.add(Calendar.HOUR, (int) return_date_diff[1]);
                            calendar2.add(Calendar.MINUTE, (int) return_date_diff[2]);

                            String return_end_date_time = dateFormat.format(calendar2.getTime());

                            String return_end_date = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "dd MMM ,yyyy");
                            String return_end_time = Constants.formatDate(return_end_date_time, "dd MMM ,yyyy HH:mm", "HH:mm");
                            dbHandler.update_return_end_trip_date_time(tripid, return_end_date, return_end_time);
                        } catch (Exception e) {
                        }


                        update_trip();
                        myTripDialog.dismiss();


                    } else {
                        Constants.show_error_popup(getActivity(), "Please change trip return date time.", iv_bottom_up_arrow);
                    }
                }


            }
        });
        ImageView ImgClosepopup = (ImageView) myTripDialog.findViewById(R.id.ImgClosepopup);

        ImgClosepopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTripDialog.dismiss();
            }
        });


        myTripDialog.show();

    }

    private void getTollbooth() {

        if (routepojo.size() > 0) {
            pb_dialog.show();
            Integer routeid = routepojo.get(0).getRouteId();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String Json = "[{\"RouteId\":\"" + routeid + "\"}]";

            Log.d("tollboothjson", Json);


            Call<ArrayList<TollBooth>> call = apiService.GetToolBooth(Json);
            call.enqueue(new Callback<ArrayList<TollBooth>>() {
                @Override
                public void onResponse(Call<ArrayList<TollBooth>> call, Response<ArrayList<TollBooth>> response) {
                    pb_dialog.dismiss();
                    if (response.body() != null && response.body().size() > 0) {

                        if (response.body().get(0).getResStatus() == true) {
//                    dbHandler.deleteAllService();
                            tollBoothArrayList = new ArrayList<TollBooth>();
                            tollBoothArrayList.clear();
                            tollBoothArrayList.addAll(response.body());
                            toolBooth_list_adapter = new TollBoothAdapter(getActivity(), tollBoothArrayList);
                            toolBooth_list_adapter.notifyDataSetChanged();

                        }
                    }

                }

                @Override
                public void onFailure(Call<ArrayList<TollBooth>> call, Throwable t) {
                    // Log error here since request failed
                    pb_dialog.dismiss();
                    t.printStackTrace();
                }
            });
        }
    }

    public void update_poi_plan(int pos) {

        try {


            String triparrival = createPoiList.get(pos).getActual_arrival_date_time() + ":00";
            String tripdeparture =createPoiList.get(pos).getActual_dep_date_time() + ":00";
            String resumedate = "";
            if (createPoiList.get(pos).getResumeDate() != null) {
                resumedate = createPoiList.get(pos).getResumeDate() + " " + createPoiList.get(pos).getResumeTime().toString().replace(".", ":") + ":00";
            }
            try {
                triparrival = Constants.formatDate(triparrival, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {
                e.printStackTrace();

                triparrival = "";

            }

            try {
                tripdeparture = Constants.formatDate(tripdeparture, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripdeparture = "";
            }

            if(resumedate.length()>1) {
                try {
                    resumedate = Constants.formatDate(resumedate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

                } catch (ParseException e) {
                    e.printStackTrace();
                    resumedate = "";

                }
            }

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            String json = "[{\"TripWayType\": \"" + createPoiList.get(pos).getPoitype() + "\",\"TripWayRefId\": \"" + createPoiList.get(pos).getPoiDesID() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"" + triparrival + "\" ,\"TripEstOutTime\":\"" + tripdeparture + "\",\"TripDesId\":" + createPoiList.get(pos).getPoiDesID() + ", \"TripIsPause\":\"false\" ,\"TripResumeDateTime\":\"" + resumedate + "\",\"TripDistanceInKm\":" + createPoiList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + createPoiList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + return_trip_plan + ",\"TripSpendTime\":" + createPoiList.get(pos).getStayTime() + ",\"TripWayPointId\":" + createPoiList.get(pos).getWayPointId() + ",\"TripIsNightStayOn\":" + createPoiList.get(pos).getPoiStay() +",\"TripLegNo\": \""+createPoiList.get(pos).getLegNo()+"\" " + "}]";

            Log.d("json_update_poi_plan1", json);

            Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
            call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
                @Override
                public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {


                }

                @Override
                public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();


                }
            });
        } catch (Exception e) {
        }

    }

    public void update_night_stay_poi_plan(int pos, Integer poidesid) {


        create_poi_nightsList = new ArrayList<>();
        create_poi_nightsList.clear();

        create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(tripid, poidesid, return_plan);


        String triparrival = create_poi_nightsList.get(pos).getPoiDate() + " " + "00:00:00";
        String tripdeparture = create_poi_nightsList.get(pos).getPoiDate() + " " + "00:00:00";

        String resumedate = "";
        if (create_poi_nightsList.get(pos).getResumeDate() != null)

            resumedate = create_poi_nightsList.get(pos).getResumeDate() + " " + create_poi_nightsList.get(pos).getResumeTime().toString().replace(".", ":") + ":00";

        try {
            triparrival = Constants.formatDate(triparrival, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("resumedate2", triparrival);
            triparrival = "";

        }

        try {
            tripdeparture = Constants.formatDate(tripdeparture, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {

            tripdeparture = "";
        }

        try {
            resumedate = Constants.formatDate(resumedate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

        } catch (ParseException e) {
            e.printStackTrace();
            resumedate = "";

        }

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        String json = "[{     \"TripWayType\": \"" + create_poi_nightsList.get(pos).getPoitype() + "\",     \"TripWayRefId\": \"" + create_poi_nightsList.get(pos).getPoiDesID() + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"" + triparrival + "\" ,\"TripEstOutTime\":\"" + tripdeparture + "\",\"TripDesId\":" + create_poi_nightsList.get(pos).getPoiDesID() + ", \"TripIsPause\":\"false\" ,\"TripResumeDateTime\":\"" + resumedate + "\",\"TripDistanceInKm\":" + create_poi_nightsList.get(pos).getKM() / 1000 + ",\"TripDistanceInMinute\":" + create_poi_nightsList.get(pos).getTime() + ",\"TripWayIsReturnPlanned\":" + return_trip_plan + ",\"TripSpendTime\":" + create_poi_nightsList.get(pos).getStayTime() + ",\"TripWayPointId\":" + create_poi_nightsList.get(pos).getWayPointId() + ",\"TripIsNightStayOn\":" + create_poi_nightsList.get(pos).getPoiStay() + "}]";

        Log.d("json_update_poi_plan2", json);

        Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
        call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
            @Override
            public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {


            }

            @Override
            public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();


            }
        });


    }

    public void update_trip() {
        try {
            createTripList = dbHandler.get_TRIP_By_TripId(tripid);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String tripdate="",tripenddate="",returndate="",returndatetimeeta="";
            tripdate = createTripList.get(0).getStartdate() + " " + createTripList.get(0).getStarttime() + ":00";
            tripenddate = createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime() + ":00";

            returndate = createTripList.get(0).getReturn_start_date() + " " + createTripList.get(0).getReturn_start_time() + ":00";


            returndatetimeeta = createTripList.get(0).getReturn_end_date() + " " + createTripList.get(0).getReturn_end_time() + ":00";
            Log.d("start_date", returndate);
            try {
                tripdate = Constants.formatDate(tripdate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

            } catch (ParseException e) {

                tripdate = "";

            }

            try {
                tripenddate = Constants.formatDate(tripenddate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
            } catch (ParseException e) {

                tripenddate = "";
            }
            if(returndate.length()>3) {
                try {
                    returndate = Constants.formatDate(returndate, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {

                    returndate = "";
                }
            }

            if(returndatetimeeta.length()>3) {
                try {
                    returndatetimeeta = Constants.formatDate(returndatetimeeta, "dd MMM ,yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss");

                } catch (ParseException e) {

                    returndatetimeeta = "";

                }
            }

            String tripstatus = createTripList.get(0).getTripstatus();
            if (tripdate.length() > 0 && createTripList.get(0).getTripstatus().contains("Saved"))
                tripstatus = "UpComing";

            String json = "[{     \"TripName\": \"" + createTripList.get(0).getTripname() + "\",\"TripDate\":\"" + tripdate + "\",\"TripEndDateTime\":\"" + tripenddate + "\", \"TripGoingTo_Start\": " + createTripList.get(0).getSourceId() + ",\"TripDestination_End\":" + createTripList.get(0).getDestinationId() + ", \"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",\"TripReturnDate\":\"" + returndate + "\",\"TripReturnDateETA\":\"" + returndatetimeeta + "\",\"TripId\":\"" + createTripList.get(0).getTripId() + "\",\"TripStatus\": \"" + tripstatus + "\",\"TripGooglePlaceDetails\":\"" + createTripList.get(0).getDes_address() +"\",\"TripKm\": \""+createTripList.get(0).getKM()+"\" ,\"TripTime\":\""+createTripList.get(0).getTime()+"\",\"TripReturnKm\":\""+createTripList.get(0).getReturnKM()+"\",\"TripReturnTime\":\""+createTripList.get(0).getReturnTime()+"\",\"TripGooglePlaceLat\":\""+createTripList.get(0).getTripGooglePlaceLat().toString()+"\",\"TripGooglePlaceLong\":\""+createTripList.get(0).getTripGooglePlaceLong().toString()+"\" }]";

            Log.d("json_CreateTrip1", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<UpdateTrip> call = apiService.updateTrip(json);
            call.enqueue(new Callback<UpdateTrip>() {
                @Override
                public void onResponse(Call<UpdateTrip> call, Response<UpdateTrip> response) {
                    if (response.body() != null) {


                    }
                }

                @Override
                public void onFailure(Call<UpdateTrip> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();
                    pb_dialog.dismiss();

                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                    }

                }
            });
        } catch (Exception e) {
        }
    }

    public void getListDefaultvalues() {


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = null;


        json = "[{     \"keyName\": \"default\"    }]";


        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<ListDefaultValues>> call = apiService.GetDefaultValues(json);
        call.enqueue(new Callback<ArrayList<ListDefaultValues>>() {
            @Override
            public void onResponse(Call<ArrayList<ListDefaultValues>> call, Response<ArrayList<ListDefaultValues>> response) {


                if (response.body() != null) {

                    Constants.listDefaultValuesArrayList = new ArrayList<>();


                    if (response.body().size() > 0) {

                        Constants.listDefaultValuesArrayList.addAll(response.body());

                        for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

                        {
                            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_EmergencyNumber"))
                                default_EmergencyNumber = Constants.listDefaultValuesArrayList.get(i).getValue().toString();

                        }


                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ListDefaultValues>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();


            }
        });
    }

    public void set_destination_layout() {
        ll_des_plan.removeAllViews();
        for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

        {
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

        }
        try {

            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            Calendar c = Calendar.getInstance();
            Date date = format.parse(createTripList.get(0).getEndtime());
            Date date1 = format.parse(temptime);
            Date firstPoiDate=format.parse(destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
            c.setTime(date);
            c.add(Calendar.HOUR, 2);
            Log.d("System out","End time of trip "+c.getTime());
            Log.d("System out","End time of day "+date1.getTime());
            if(c.getTimeInMillis()<=date1.getTime())
            {
                if(firstPoiDate.getTime()<=c.getTimeInMillis())
                {
                    destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));

                }
                else {

                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }





                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(0);
            }
            else
            {
                if (destinationplanList.get(0).getStarttime() == 24.00) {
                    destinationplanList.get(0).setArrivalTime(07.00);
                } else {
                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }


                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(1);
            }


        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            destinationplanList.get(0).setDay(1);
            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

        }
        if (destinationplanList.size() > 0) {

            Log.d("System out","Destination poi list size "+destinationplanList.size());
           /* if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(0);*/



            Double tempDepart=destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++)
            {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out","required time  "+reqtime);
                Log.d("System out","depature time  "+destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours)
                {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart=destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out","required time  "+ destinationplanList.get(j).getDay());


            }

        }

        noday = new ArrayList<>();
        noday.clear();

        //  noday.add(1);

        Integer day = 0;
       /* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*/
        if(destinationplanList.get(0).getDay() == 0)
        {
            day = destinationplanList.get(0).getDay();
            noday.add(destinationplanList.get(0).getDay());
        }

        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);
            try {
                if (createTripList.get(0).getEnddate().length() > 0) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                    try {
                        Date date = format.parse(createTripList.get(0).getEnddate());
                        c.setTime(date);
                        c.add(Calendar.DATE, day);
                        destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }

                }
            } catch (Exception e) {
            }

            Log.d("days","pos"+day+"v2"+destinationplanList.get(i).getDay());

            if (day != destinationplanList.get(i).getDay() ) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());

            }


        }

        Log.d("System out","Reponse of set time of days count "+noday.size());
        for (int j = 0; j < noday.size(); j++) {
            AddDayWisePlan(j);
        }


        pb_dialog.dismiss();
    }

    public void AddDayWisePlan(int pos) {


        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v;
        v = inflater.inflate(R.layout.add_day_wise_plan_layout, null);

        TextView tv_day = (TextView) v.findViewById(R.id.tv_day);
        TextView tv_des_date = (TextView) v.findViewById(R.id.tv_des_date);
        tv_des_date.setText("");
        try {

            if (createTripList.get(0).getEnddate().length() > 0) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                try {
                    Date date = format.parse(createTripList.get(0).getEnddate());
                    c.setTime(date);
                    c.add(Calendar.DATE, noday.get(pos));
                    tv_des_date.setText(format.format(c.getTime()));


                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    tv_des_date.setText("");
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
            tv_des_date.setText("");

        }
        Log.d("System out","day pos "+noday.get(pos));
        if (noday.size() > 0) {
            if (noday.get(0).intValue() == 0)
                tv_day.setText("Day " + (noday.get(pos) + 1));
            else
                tv_day.setText("Day " + noday.get(pos));
        }
        else
        {
            tv_day.setText("Day " + noday.get(pos));
        }
        ll_add_places_to_plan = (LinearLayout) v.findViewById(R.id.ll_add_places_to_plan);

        //  ll_day_wise_plan.removeAllViews();
        //  ll_add_places_to_plan.removeAllViews();

        //set time


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == noday.get(pos)) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        if (tempdestiDestinationPlanlist.size() > 0) {
            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            SimpleDateFormat format = new SimpleDateFormat("HH:mm");

            try {
                Date date = format.parse(createTripList.get(0).getEndtime());
                Date date1 = format.parse(temptime);
                Date firstPoiDate=format.parse(tempdestiDestinationPlanlist.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime() && pos ==0 )
                {

                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));

                    }
                    else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }



                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                }
                else
                {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                tv_des_date.setText("");
            }


            Double tempDepart=tempdestiDestinationPlanlist.get(0).getArrivalTime();


            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempDepart) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    //      dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                    if (tempdestiDestinationPlanlist.get(j).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(07.00);
                    } else {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }
                    tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());

                    tempDepart=tempdestiDestinationPlanlist.get(j).getArrivalTime();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }

        }
        ll_add_places_to_plan.removeAllViewsInLayout();
        for (int i = 0; i < tempdestiDestinationPlanlist.size(); i++) {

            // AddPlacesToPlan(i);
        }
        ll_des_plan.addView(v);

    }



    private void getdistancetime(final int pos) {

        pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        //  pb_dialog.show();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CalculateDistance>> call = apiService.GetDistanceTimebylatlong("api/userMobileApi/GetDistanceDurationFromGoogle?lat1=" + destinationplanList.get(pos).getPoiLatitude() + "&lon1=" + destinationplanList.get(pos).getPoiLongitude() + "&lat2=" + destinationplanList.get(pos - 1).getPoiLatitude() + "&lon2=" + destinationplanList.get(pos - 1).getPoiLongitude() + "&apiKey=chndroid");
        call.enqueue(new Callback<ArrayList<CalculateDistance>>() {
            @Override
            public void onResponse(Call<ArrayList<CalculateDistance>> call, Response<ArrayList<CalculateDistance>> response) {
                if (response.body() != null) {

                    pb_dialog.dismiss();


                    if (response.body().get(0).getDistance().getText() != null) {
                        destinationplanList.get(pos - 1).setTime(Double.valueOf(response.body().get(0).getDuration().getText()));
                        destinationplanList.get(pos - 1).setKM(Double.valueOf(response.body().get(0).getDistance().getText()));
                    }

                    if (pos == destinationplanList.size() - 1) {
                        set_destination_layout();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<CalculateDistance>> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });

    }



    private void getRo(String latitude, String longitude, Integer Roid) {


        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String Json = "[{     \"ROMasterId\": \"" + Roid + "\", \"radius\": \"999999\",  \"PageNo\":1,\"PageSize\":100 ,  \"ROLatitude\":\"" + latitude + "\", \"ROLongitude\":\"" + longitude + "\"}]";
        Log.d("Rojson", Json);
        Call<ArrayList<GetRO>> call = apiService.ListRo(Json);
        call.enqueue(new Callback<ArrayList<GetRO>>() {
            @Override
            public void onResponse(Call<ArrayList<GetRO>> call, Response<ArrayList<GetRO>> response) {

                pb_dialog.dismiss();

                if (response.body().get(0).getResStatus() == true) {

                    ArrayList<GetRO> rolist = new ArrayList<GetRO>();
                    rolist.clear();
                    rolist.addAll(response.body());
                    Bundle b = new Bundle();
                    if (listServiceCategoryList.get(0).getResStatus() == true)
                        b.putString("ro_image", listServiceCategoryList.get(0).getSerDefaultImage());
                    b.putString("facilitytype", "r");

                    String json = gson.toJson(rolist.get(0));
                    b.putString("rolist", json);
                    ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new FacilitiesFragment(), b);


                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetRO>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    //destination poi
    private void getDestinationPOIAPI(Integer destId, Double destLat, Double destLong) {
        // get & set progressbar dialog
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        pb_dialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "";

        json = "[{\"PoiUmId\": \"0\",\"POIID\":\"0\" ,\"radius\":\"100\"   , \"POINearbyDestination\": \"" + destId + "\",  \"PageNo\":1,\"PageSize\":100, \"POILongitude\":\"" + destLong + "\",\"POILatitude\":\"" + destLat + "\",\"CategoryName\":\"Attraction\", \"POIRating\":0  }]";

        Log.d("System out", "Explore POI____" + json);


        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {


                pb_dialog.dismiss();

                if (response.body() != null) {


                    explorePoiList = new ArrayList<ExplorePOI>();
                    explorePoiList.clear();
                    explorePoiList.addAll(response.body());
                    if (explorePoiList.size() > 0) {
                        if (explorePoiList.get(0).getResStatus() == true) {

                            add_marker_destination_plan();

                            //  Collections.reverse(exploreDestList);


                        }

                    } else {
                        pb_dialog.dismiss();
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }



    public void getPOIAPI(final int destId, final int pid) {


        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        // explore_places_content_progressbar.setVisibility(View.VISIBLE);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id = "";
        if (sessionManager.isLoggedIn()) {
            id = sessionManager.get_Authenticate_User().getUmId() + "";
        }
        //"POILatitude":"13", "POILongitude":"132"
        //  String json = "[{     \"umId\": \""+"1"+"\",     \"radius\": \"1000\",   \"desName\": \"\",   \"desLongitude\": \"\",   \"desLatitude\": \"\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20 }]";
        // String json="[{\"umId\": \"0\",\"POIID\":\""+0+"\"    , \"POINearbyDestination\": \""+destId+"\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\"  }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";

        // json="[{\"PoiUmId\": \""+id+"\",\"POIID\":\""+0+"\" ,\"radius\":\""+999999+"\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\""+destLong+"\",\"POILatitude\":\""+destLat+"\",\"CategoryName\":\"Attraction\"  }]";
        json = "[{\"PoiUmId\": \"" + id + "\",\"POIID\":\"" + pid + "\" ,\"radius\":\"" + 999999 + "\"   , \"POINearbyDestination\": \"0\",  \"PageNo\":1,\"PageSize\":20, \"POILongitude\":\"" + 123 + "\",\"POILatitude\":\"" + 123 + "\", \"POIRating\":0  }]";

        Log.d("System out", "Explore POI from favorites____" + json);


        Call<ArrayList<ExplorePOI>> call = apiService.explorepoi(json);
        call.enqueue(new Callback<ArrayList<ExplorePOI>>() {
            @Override
            public void onResponse(Call<ArrayList<ExplorePOI>> call, Response<ArrayList<ExplorePOI>> response) {

                pb_dialog.dismiss();

                if (response.body() != null) {


                    ArrayList<ExplorePOI> explorePoiList1 = new ArrayList<ExplorePOI>();

                    explorePoiList1.addAll(response.body());
                    if (explorePoiList1.size() > 0) {
                        if (explorePoiList1.get(0).getResStatus() == true) {

                            //   explore_places_content_progressbar.setVisibility(View.GONE);

                            //  Collections.reverse(exploreDestList);
                            hide_details_layout();

                            String json = gson.toJson(explorePoiList1); // myObject - instance of MyObject
                            editor.putString("POIList", json);
                            editor.commit();

                                    /*if(destinationPlanList.size()>0)
                                    {
                                        for(int i=0;i<explorePoiList.size();i++) {
                                            for(int j=0;j<destinationPlanList.size();j++)
                                            {
                                                Log.d("desid",String.valueOf(destinationPlanList.get(j).getPoiServerId())+"poiid"+explorePoiList.get(i).getPOIID());
                                                if(destinationPlanList.get(j).getPoiServerId()==explorePoiList.get(i).getPOIID())
                                                    ((ExploreActivity) getActivity()).AddtoMyPlanList.add(String.valueOf(i));
                                            }

                                            }
                                        }*/


                            Log.d("desid", "");

                            Bundle bundle = new Bundle();
                            bundle.putString("position", "0");
                            bundle.putString("placeName", explorePoiList1.get(0).getPOIName());
                            bundle.putInt("tripId", 0);
                            bundle.putString("plan_type", "POI");
                            bundle.putString("destId", "" + destId);
                            bundle.putString("DestName", explorePoiList1.get(0).getDesName());
                            json = gson.toJson(explorePoiList1);
                            editor.putString("POIList", json);
                            editor.commit();
                            //  bundle=getArguments();


                            ExploreAttractionFragment exploreAttractionFragment = new ExploreAttractionFragment();
                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();


                            ExploreActivity.destattractionimages.clear();
                            ((ExploreActivity) getActivity()).setpagerdata(false);
                            for (int i = 0; i < explorePoiList1.get(0).getImageList().size(); i++) {
                                if (explorePoiList1.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {


                                    ExploreActivity.destattractionimages.add(explorePoiList1.get(0).getImageList().get(i).getImgName());
                                }
                            }
                            // bundle.putAll(getArguments());
                            exploreAttractionFragment.setArguments(bundle);
                            changeTransaction.replace(R.id.frame, exploreAttractionFragment);
                            changeTransaction.addToBackStack(null);
                            changeTransaction.commit();


                        } else {
                            //  explore_places_content_progressbar.setVisibility(View.GONE);
                            Constants.show_error_popup(getActivity(), explorePoiList.get(0).getResDescription() + "", error_layout);
                        }
                    } else {
                        //   explore_places_content_progressbar.setVisibility(View.GONE);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExplorePOI>> call, Throwable t) {
                // Log error here since request failed
                //  explore_places_content_progressbar.setVisibility(View.GONE);
                pb_dialog.dismiss();

                t.printStackTrace();


            }
        });
    }

    public void getDestinationAPI(final String detination) {
        // get & set progressbar dialog
    /*    final Dialog pb_dialog = MyUtil.get_dialog(ContactActivity.this, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();*/

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id = "1";
        if (sessionManager.isLoggedIn()) {
            id = sessionManager.get_Authenticate_User().getUmId() + "";
        }

        // if (from.equalsIgnoreCase("nearbyplaces")) {

        json = "[{     \"umId\": \"" + id + "\",     \"radius\": \"" + 999999 + "\",   \"desName\": \"" + detination + "\",   \"desLongitude\": \"" + 123 + "\",   \"desLatitude\": \"" + 123 + "\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":20,\"desNotIncludeThisID\":" + 0 + " }]";
        Log.d("System out", "Explore Destination from nearby____" + json);


        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {

                ArrayList<ExploreDestination> exploreDestList = new ArrayList<>();
//                pb_dialog.dismiss();
                pb_dialog.dismiss();
                if (response.body() != null) {


                    exploreDestList.clear();
                    exploreDestList.addAll(response.body());
                    if (exploreDestList.get(0).getResStatus() == true) {


                        // Collections.reverse(exploreDestList);
                        hide_details_layout();
                        ListingDetailsFragment listingDetailsFragment = new ListingDetailsFragment();
                        android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        // ((ExploreActivity)getActivity()).setHeader("Change Password");
                        changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                        changeTransaction.replace(R.id.frame, listingDetailsFragment);
                        changeTransaction.addToBackStack(null);
                        Bundle bundle = new Bundle();
                        bundle.putString("placeName", detination);
                        bundle.putString("from", "sublisting");
                        bundle.putString("position", 0 + "");
                        bundle.putString("plan_type", "DES");
                        bundle.putSerializable("Explorlist", exploreDestList.get(0));
                        String json = gson.toJson(exploreDestList);
                        editor.putString("DestList", json);
                        editor.commit();
                        ExploreActivity.destattractionimages.clear();
                        for (int i = 0; i < exploreDestList.get(0).getImageList().size(); i++) {
                            if (exploreDestList.get(0).getImageList().get(i).getImgDescription().equalsIgnoreCase("Banner")) {
                                ExploreActivity.destattractionimages.add(exploreDestList.get(0).getImageList().get(i).getImgName());
                            }
                        }


                        listingDetailsFragment.setArguments(bundle);
                        changeTransaction.commit();

                    } else {


                        Constants.show_error_popup(getActivity(), "We couldn't find an attraction of your choice. Modify selection.", error_layout);
                    }


                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();


            }
        });
    }

    public long[] get_diff_between_date_hours(String formdate, String todate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        long diff = 0;
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = dateFormat.parse(formdate);
            d2 = dateFormat.parse(todate);

            diff = d2.getTime() - d1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //   Log.d("datediff","time"+dateFormat.format(fromCalender.getTime())+"to"+dateFormat.format(toCalender.getTime()));
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long[] datediffvalue = new long[3];
        datediffvalue[0] = diffDays;
        datediffvalue[1] = diffHours;
        datediffvalue[2] = diffMinutes;
        return datediffvalue;

    }

    public void get_my_trip1() {
        tempcreateTripList = new ArrayList<>();
        tempcreatePoiList = new ArrayList<>();
        tempcreateTripList = dbHandler.get_TRIP_By_TripId(tripid);



        if (tempcreateTripList.size() > 0) {
            String tripStartDate = "";


            if (return_plan == 0) {

                tripStartDate = tempcreateTripList.get(0).getStartdate() + " " + tempcreateTripList.get(0).getStarttime();
            } else {
                tripStartDate = tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time();
            }
            DecimalFormat df = new DecimalFormat("#.##");
            double distance = Double.valueOf(df.format(createTripList.get(0).getKM() / 1000));
            //  Log.d("startPoi",currentDateandTime);
            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] converttime = splitToComponentTimes(distance);
                calendar.add(Calendar.HOUR, converttime[0]);
                calendar.add(Calendar.MINUTE, converttime[1]);

                if (return_plan==0) {
                    tempcreateTripList.get(0).setEnddate(sdfDate.format(calendar.getTime()));
                    tempcreateTripList.get(0).setEndtime(sdfTime.format(calendar.getTime()));


                } else {
                    tempcreateTripList.get(0).setReturn_end_date(sdfDate.format(calendar.getTime()));
                    tempcreateTripList.get(0).setReturn_end_time(sdfTime.format(calendar.getTime()));
                }


            } catch (ParseException e) {
                e.printStackTrace();

            }


            tempcreatePoiList = dbHandler.get_POI_TRIP(createTripList.get(0).getTripId(), return_plan);
            for (int i = 0; i < tempcreatePoiList.size(); i++) {


                PlacesBetweenStoD1(i);
            }
        }
    }

    private void PlacesBetweenStoD1(final int pos) {

        //set Arrival departure date and time pos =0
        if (pos == 0) {


            String tripStartDate = "";

            if (return_plan == 0)
                tripStartDate = tempcreateTripList.get(0).getStartdate() + " " + tempcreateTripList.get(0).getStarttime();
            else
                tripStartDate = tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time();


            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int[] convertime = splitToComponentTimes(tempcreatePoiList.get(0).getTime());
                calendar.add(Calendar.HOUR, convertime[0]);
                calendar.add(Calendar.MINUTE, convertime[1]);


                tempcreatePoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));



                if (tempcreatePoiList.get(pos).getPoiStay() == false) {
                    BigDecimal bd = new BigDecimal((tempcreatePoiList.get(pos).getStayTime() - Math.floor(tempcreatePoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, tempcreatePoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                } else {
                    date = sdf.parse(tempcreatePoiList.get(pos).getResumeDate() + " " + tempcreatePoiList.get(pos).getResumeTime());
                    calendar.setTime(date);

                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));

                }

            } catch (ParseException e) {
                e.printStackTrace();

            }
        }


        //Log.d("arrivalTime","ar"+createPoiList.get(pos).getArrivalTime()+"de"+createPoiList.get(pos).getDepartureTime()+"re"+createPoiList.get(pos).getReqtime());
        //set arrival date and time
        if (pos >= 1 && (pos <= tempcreatePoiList.size() - 1))
        {


            int position = pos - 1;

            String tripStartDate = tempcreatePoiList.get(position).getActual_dep_date_time();


            Date date = null;
            try {
                date = sdf.parse(tripStartDate);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                  int[] requriedtime = splitToComponentTimes(tempcreatePoiList.get(position).getReqtime());
                calendar.add(Calendar.HOUR, requriedtime[0]);

                calendar.add(Calendar.MINUTE, requriedtime[1]);


                tempcreatePoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));


                if (tempcreatePoiList.get(pos).getPoiStay() == false)
                {
                    BigDecimal bd = new BigDecimal((tempcreatePoiList.get(pos).getStayTime() - Math.floor(tempcreatePoiList.get(pos).getStayTime())) * 100);
                    bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                    int min = bd.intValue();

                    calendar.setTime(calendar.getTime());

                    calendar.add(Calendar.HOUR, tempcreatePoiList.get(pos).getStayTime().intValue());
                    calendar.add(Calendar.MINUTE, min);

                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                } else {
                    date = sdf.parse(tempcreatePoiList.get(pos).getResumeDate() + " " + tempcreatePoiList.get(pos).getResumeTime());
                    calendar.setTime(date);
                    tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));

                }

            } catch (ParseException e) {
                e.printStackTrace();

            }

        }

        if (tempcreatePoiList.size() == pos + 1) {


            if(return_plan==0)
            tempcreatePoiList.get(pos).setReqtime(tempcreateTripList.get(0).getTime());
            else
                tempcreatePoiList.get(pos).setReqtime(tempcreateTripList.get(0).getReturnTime());

            Date date = null;
            try {
                date = sdf.parse(tempcreatePoiList.get(pos).getActual_dep_date_time());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                 int[] time_h_m_S=splitToComponentTimes(tempcreatePoiList.get(pos).getReqtime());
                calendar.add(Calendar.HOUR, time_h_m_S[0]);
                calendar.add(Calendar.MINUTE, time_h_m_S[1]);


                if (return_plan == 0) {

                    tempcreateTripList.get(0).setEnddate(sdfDate.format(calendar.getTime()));
                    tempcreateTripList.get(0).setEndtime(sdfTime.format(calendar.getTime()));


                } else {


                    tempcreateTripList.get(0).setReturn_end_date(sdfDate.format(calendar.getTime()));
                    tempcreateTripList.get(0).setReturn_end_time(sdfTime.format(calendar.getTime()));
                }


            } catch (ParseException e) {
                e.printStackTrace();

            }

        } else {


            tempcreatePoiList.get(pos).setReqtime(tempcreatePoiList.get(pos + 1).getTime());


        }


    }


    public void insert_poi_db() {
        Integer pos = Integer.parseInt(tv_add_poi.getTag().toString().substring(1));
        if (tv_add_poi.getTag().toString().startsWith("p")) {

            if (onAdminRoutePoiLists.get(pos).getRPType().contains("POI"))
                Insert_Poi_Api(pos, onAdminRoutePoiLists.get(pos).getRPType(), onAdminRoutePoiLists.get(pos).getPOIID().toString(), onAdminRoutePoiLists.get(pos).getDurationFromRouteOrigin().toString(), onAdminRoutePoiLists.get(pos).getDistanceFromRouteOrigin(), onAdminRoutePoiLists.get(pos).getPOIImage(), destinationId);
            else
                Insert_Poi_Api(pos, onAdminRoutePoiLists.get(pos).getRPType(), onAdminRoutePoiLists.get(pos).getDesID().toString(), onAdminRoutePoiLists.get(pos).getDurationFromRouteOrigin().toString(), onAdminRoutePoiLists.get(pos).getDistanceFromRouteOrigin(), onAdminRoutePoiLists.get(pos).getDesImage(), destinationId);
        } else if (tv_add_poi.getTag().toString().startsWith("r")) {
            Insert_Poi_Api(pos, "RO", onAdminRouteRoLists.get(pos).getROMasterId().toString(), onAdminRouteRoLists.get(pos).getDurationFromRouteOrigin().toString(), onAdminRouteRoLists.get(pos).getDistanceFromRouteOrigin(), "20161013_0512atm-selected.png", destinationId);
        } else if (tv_add_poi.getTag().toString().startsWith("s")) {
            Insert_Poi_Api(pos, "Services", services.get(pos).getSerId().toString(), services.get(pos).getDuration().toString(), services.get(pos).getDistance(), services.get(pos).getSerCatFooterImageSelected(), destinationId);
        } else if (tv_add_poi.getTag().toString().startsWith("n")) {

            Insert_Poi_Api(pos, "PoiNight", explorePoiList.get(pos).getPOIID().toString(), explorePoiList.get(pos).getDuration().toString(), explorePoiList.get(pos).getDistance(), explorePoiList.get(pos).getPOIImage(), explorePoiList.get(pos).getPOINearbyDestination());


        } else if (tv_add_poi.getTag().toString().startsWith("d")) {

            Insert_Poi_Api(pos, "DesPlan", explorePoiList.get(pos).getPOIID().toString(), explorePoiList.get(pos).getDuration().toString(), explorePoiList.get(pos).getDistance(), explorePoiList.get(pos).getPOIImage(), explorePoiList.get(pos).getPOINearbyDestination());


        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (createTripList.size() > 0) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(
                    new CameraPosition.Builder()
                            .target(new LatLng(createTripList.get(0).getSource_latitude(), createTripList.get(0).getSource_longitude()))  // set the camera's center position
                            .zoom(10)  // set the camera's zoom level
                            // set the camera's tilt
                            .build()));
        }
        // map.setMyLocationEnabled(true);

        map.animateCamera(CameraUpdateFactory.zoomIn());




        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {

                //    map.getInfoWindowAdapter().getInfoWindow(marker).setVisibility(View.GONE);

                marker.hideInfoWindow();
                tv_add_poi.setVisibility(View.VISIBLE);
                if (marker.getSnippet().contains("point")) {

                } else {


                    if (marker.getSnippet().contains("POI"))
                    {
                        final String pos = marker.getSnippet().substring(4);
                        //      Toast.makeText(getActivity(), marker.getSnippet()+pos, Toast.LENGTH_SHORT).show();//

                        fl_pop_up_zoo.setVisibility(View.VISIBLE);
                        if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRoutePoiLists.get(Integer.valueOf(pos)).getRouteRefId(), "POI", return_plan) != 0) {
                            tv_add_poi.setVisibility(View.GONE);
                        }
                        if (onAdminRoutePoiLists.get(Integer.valueOf(pos)).getRPType().contains("POI")) {


                            // Log.d("poiImagepath", Constants.TimbThumb_ImagePath + routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIImage());


                            fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIImage() + "&width=" + 150 + "&height=" + 150);
                            fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                            tv_poi_map_name.setText(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIName());
                            tv_poi_map_des.setText(Html.fromHtml(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIShortDescription()));

                            //tv_poi_map_distance.setText((int) Math.ceil(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms [" + Constants.convert_minute_hrs(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDurationFromRouteOrigin().doubleValue()) + "]");
                            tv_poi_map_distance.setText((int) Math.ceil(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms");

                          /*  iv_place_location.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Bundle b=new Bundle();
                                    b.putString("poiLat", onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOILatitude().toString());
                                    b.putString("poiLong", onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOILongitude().toString());
                                    b.putString("name", onAdminRoutePoiLists.get(Integer.valueOf(pos)).getPOIName());


                                    ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new MapviewOfPlace(),b);
                                }
                            });*/

                        } else {
                            fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDesImage() + "&width=" + 150 + "&height=" + 150);
                            fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                            tv_poi_map_name.setText(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDesName());
                            tv_poi_map_des.setText(Html.fromHtml(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDesShortDescription()));

                           // tv_poi_map_distance.setText((int) Math.ceil(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms [" + Constants.convert_minute_hrs(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDurationFromRouteOrigin().doubleValue()) + "]");
                            tv_poi_map_distance.setText((int) Math.ceil(onAdminRoutePoiLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms ");

                        }


                        llserviceslayout.setVisibility(View.GONE);
                        tv_poi_map_des.setVisibility(View.VISIBLE);


                        tv_add_poi.setTag("p" + pos);


                        if (selected_poi_pos > -1)
                            onAdminRoutePoiLists.get(Integer.valueOf(selected_poi_pos)).setClick_status(false);

                        selected_poi_pos = Integer.valueOf(pos);

                        onAdminRoutePoiLists.get(Integer.valueOf(pos)).setClick_status(true);
                        add_marker_poi();


                    } else if (marker.getSnippet().contains("Services")) {
                        String pos = marker.getSnippet().substring(9);
                        fl_pop_up_zoo.setVisibility(View.VISIBLE);

                        if (dbHandler.check_poi_trip_in_local_db(tripid, services.get(Integer.valueOf(pos)).getSerId(), "Service", return_plan) != 0) {
                            tv_add_poi.setVisibility(View.GONE);
                        }

                        fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + services.get(Integer.valueOf(pos)).getSerCatFooterImageSelected() + "&width=" + 150 + "&height=" + 150);
                        fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                        tv_poi_map_name.setText(services.get(Integer.valueOf(pos)).getSerName());

                        // tv_poi_map_des.setText(Html.fromHtml(routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIShortDescription()));

                      //  tv_poi_map_distance.setText((int) Math.ceil(services.get(Integer.valueOf(pos)).getDistance()) + " Kms [" + Constants.convert_minute_hrs(services.get(Integer.valueOf(pos)).getDuration().doubleValue()) + "]");
                        tv_poi_map_distance.setText((int) Math.ceil(services.get(Integer.valueOf(pos)).getDistance()) + " Kms ");
                        tv_services_location.setText(services.get(Integer.valueOf(pos)).getSerAddress1());
                        tv_services_side.setText(services.get(Integer.valueOf(pos)).getSerSide());

                        llserviceslayout.setVisibility(View.VISIBLE);
                        tv_poi_map_des.setVisibility(View.GONE);
                        tv_add_poi.setTag("s" + pos);

                        if (selected_ser_pos > -1)
                            services.get(selected_ser_pos).setClick_status(false);

                        selected_ser_pos = Integer.valueOf(pos);

                        services.get(Integer.valueOf(pos)).setClick_status(true);
                        add_marker_services();


                    } else if (marker.getSnippet().contains("RO")) {
                        String pos = marker.getSnippet().substring(3);
                        fl_pop_up_zoo.setVisibility(View.VISIBLE);

                        if (dbHandler.check_poi_trip_in_local_db(tripid, onAdminRouteRoLists.get(Integer.valueOf(pos)).getROMasterId(), "RO", return_plan) != 0) {
                            tv_add_poi.setVisibility(View.GONE);
                        }

                        fc_poi_map_image.setImageResource(R.drawable.map_slider_icon4_selected);

                        //fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                        tv_poi_map_name.setText(onAdminRouteRoLists.get(Integer.valueOf(pos)).getROName());

                        // tv_poi_map_des.setText(Html.fromHtml(routepojo.get(0).getOnAdminRoutePoiLists().get(Integer.valueOf(pos)).getPOIShortDescription()));

                      //  tv_poi_map_distance.setText((int) Math.ceil(onAdminRouteRoLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms [" + Constants.convert_minute_hrs(onAdminRouteRoLists.get(Integer.valueOf(pos)).getDurationFromRouteOrigin().doubleValue()) + "]");
                        tv_poi_map_distance.setText((int) Math.ceil(onAdminRouteRoLists.get(Integer.valueOf(pos)).getDistanceFromRouteOrigin()) + " Kms ");
                        tv_services_location.setText(onAdminRouteRoLists.get(Integer.valueOf(pos)).getROLocationName());
                        tv_services_side.setText(onAdminRouteRoLists.get(Integer.valueOf(pos)).getROSide());

                        llserviceslayout.setVisibility(View.VISIBLE);
                        tv_poi_map_des.setVisibility(View.GONE);
                        tv_add_poi.setTag("r" + pos);

                        if (selected_ro_pos > -1)
                            onAdminRouteRoLists.get(selected_ro_pos).setClick_status(false);

                        selected_ro_pos = Integer.valueOf(pos);

                        onAdminRouteRoLists.get(Integer.valueOf(pos)).setClick_status(true);
                        add_marker_ro();


                    } else if (marker.getSnippet().contains("PoiNight")) {
                        String pos = marker.getSnippet().substring(9);
                        fl_pop_up_zoo.setVisibility(View.VISIBLE);

                        if (dbHandler.check_poi_night_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(pos)).getPOIID()) != 0) {
                            tv_add_poi.setVisibility(View.GONE);
                        }
                        Log.d("poiImagepath", Constants.TimbThumb_ImagePath + explorePoiList.get(Integer.valueOf(pos)).getPOIImage());

                        fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + explorePoiList.get(Integer.valueOf(pos)).getPOIImage() + "&width=" + 150 + "&height=" + 150);
                        fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                        tv_poi_map_name.setText(explorePoiList.get(Integer.valueOf(pos)).getPOIName());
                        tv_poi_map_des.setText(Html.fromHtml(explorePoiList.get(Integer.valueOf(pos)).getPOIShortDescription()));

                       // tv_poi_map_distance.setText((int) Math.ceil(explorePoiList.get(Integer.valueOf(pos)).getDistance()) + " Kms [" + Constants.convert_minute_hrs(explorePoiList.get(Integer.valueOf(pos)).getDuration().doubleValue()) + "]");
                        tv_poi_map_distance.setText((int) Math.ceil(explorePoiList.get(Integer.valueOf(pos)).getDistance()) + " Kms ");


                        llserviceslayout.setVisibility(View.GONE);
                        tv_poi_map_des.setVisibility(View.VISIBLE);


                        tv_add_poi.setTag("n" + pos);


                        if (selected_des_pos > -1)
                            explorePoiList.get(Integer.valueOf(selected_des_pos)).setClick_status(false);

                        selected_des_pos = Integer.valueOf(pos);

                        explorePoiList.get(Integer.valueOf(selected_des_pos)).setClick_status(true);
                        add_marker_Night_destination();

                    } else if (marker.getSnippet().contains("DesPlan")) {
                        String pos = marker.getSnippet().substring(8);
                        fl_pop_up_zoo.setVisibility(View.VISIBLE);

                        if (dbHandler.check_des_trip_in_local_db(tripid, explorePoiList.get(Integer.valueOf(pos)).getPOIID()) != 0) {
                            tv_add_poi.setVisibility(View.GONE);
                        }


                        Log.d("poiImagepath", Constants.TimbThumb_ImagePath + explorePoiList.get(Integer.valueOf(pos)).getPOIImage());

                        fc_poi_map_image.setImageURI(Constants.TimbThumb_ImagePath + explorePoiList.get(Integer.valueOf(pos)).getPOIImage() + "&width=" + 150 + "&height=" + 150);
                        fc_poi_map_image.getHierarchy().setProgressBarImage(new ImageLoadProgressBar());
                        tv_poi_map_name.setText(explorePoiList.get(Integer.valueOf(pos)).getPOIName());
                        tv_poi_map_des.setText(Html.fromHtml(explorePoiList.get(Integer.valueOf(pos)).getPOIShortDescription()));

                       // tv_poi_map_distance.setText((int) Math.ceil(explorePoiList.get(Integer.valueOf(pos)).getDistance()) + " Kms [" + Constants.convert_minute_hrs(explorePoiList.get(Integer.valueOf(pos)).getDuration().doubleValue()) + "]");
                        tv_poi_map_distance.setText((int) Math.ceil(explorePoiList.get(Integer.valueOf(pos)).getDistance()) + " Kms ");


                        llserviceslayout.setVisibility(View.GONE);
                        tv_poi_map_des.setVisibility(View.VISIBLE);


                        tv_add_poi.setTag("d" + pos);
                        tv_add_poi.setVisibility(View.GONE);


                        if (selected_des_pos > -1)
                            explorePoiList.get(Integer.valueOf(selected_des_pos)).setClick_status(false);

                        selected_des_pos = Integer.valueOf(pos);

                        explorePoiList.get(Integer.valueOf(selected_des_pos)).setClick_status(true);
                        add_marker_destination_plan();


                    }
                }


                return true;
            }
        });


// Execute the request

    }



    private void getRoute(String url) {

        ApiInterface apiService = ApiClient.get_retrofit_client_google().create(ApiInterface.class);
        Call<Direction> call = apiService.GetRoute(url);
        call.enqueue(new Callback<Direction>() {
            @Override
            public void onResponse(Call<Direction> call, Response<Direction> response) 
            {
                if (response.body() != null)
                {


                    try {

                        if (response.body().getRoutes().size() > 0)
                        {
                            route_direction = new Direction();
                            route_direction = response.body();
                            DirectionsJSONParser parser = new DirectionsJSONParser();

                            // Starts parsing data
                            Gson gson = new GsonBuilder().create();

                            String routeresponse = gson.toJson(response.body());
                            routelineresult = parser.parse(new JSONObject(routeresponse));

                            add_marker_ro();

                            set_KM_Time_POI_Display();

                        }
                        else {
                            left_navView.setVisibility(View.GONE);
                            Constants.show_error_popup(getActivity(), "Our system is limited to 8 stoppages in a route. We are working to improve it. ", iv_bottom_up_arrow);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<Direction> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });

    }

    public Double[] get_no_days_destination_plan() {
        Double[] destination_plan = {0.0, 0.0, 0.0};
        Double total_time_stay_last = 0.0;

       /* for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

        {
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

        }





        try {

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
            Calendar c = Calendar.getInstance();
            Date date = format.parse(createTripList.get(0).getEnddate()+" "+createTripList.get(0).getEndtime());
            Date date1 = format.parse(createTripList.get(0).getEnddate()+" "+temptime);
            Date firstPoiDate=format.parse(createTripList.get(0).getEnddate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
            c.setTime(date);
            c.add(Calendar.HOUR, 2);
            Log.d("System out","End time of trip "+c.getTime());
            Log.d("System out","End time of day "+date1.toString());
            Log.d("System out","End time of day "+firstPoiDate.toString());
            if(c.getTimeInMillis()<=date1.getTime())
            {
                if(firstPoiDate.getTime()<=c.getTimeInMillis())
                {
                    destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                }
                else {

                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }





                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(0);
            }
            else
            {
                if (destinationplanList.get(0).getStarttime() == 24.00) {
                    destinationplanList.get(0).setArrivalTime(07.00);
                } else {
                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }


                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(1);
            }


        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            destinationplanList.get(0).setDay(1);
            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

        }
        if (destinationplanList.size() > 0) {

            Log.d("System out","Destination poi list size "+destinationplanList.size());



            Double tempDepart=destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++)
            {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out","required time  "+reqtime);
                Log.d("System out","depature time  "+destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours)
                {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(j).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart=destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out","required time  "+ destinationplanList.get(j).getDay());


            }

        }

        noday = new ArrayList<>();
        noday.clear();





        //  noday.add(1);

        Integer day = 0;
       *//* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*//*
        if(destinationplanList.get(0).getDay() == 0)
        {
            day = destinationplanList.get(0).getDay();
            noday.add(destinationplanList.get(0).getDay());
        }

        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);
            try {
                if (createTripList.get(0).getEnddate().length() > 0) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                    try {
                        Date date = format.parse(createTripList.get(0).getEnddate());
                        c.setTime(date);
                        c.add(Calendar.DATE, day);
                        destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }

                }
            } catch (Exception e) {
            }

            Log.d("days","pos"+day+"v2"+destinationplanList.get(i).getDay());

            if (day != destinationplanList.get(i).getDay() ) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());

            }


        }
*/

        if (destinationplanList.size() > 0) {

            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

            {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

            }


            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                //SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Calendar c = Calendar.getInstance();
                Date date = format.parse(createTripList.get(0).getEnddate() + " " + createTripList.get(0).getEndtime());
                Date date1 = format.parse(createTripList.get(0).getEnddate() + " " + temptime);
                Date firstPoiDate = format.parse(createTripList.get(0).getEnddate() + " " + destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out", "End time of trip " + c.getTime());
                Log.d("System out", "End time of day " + date1.toString());
                Log.d("System out", "End time of day " + firstPoiDate.toString());
                if (c.getTimeInMillis() <= date1.getTime()) {
                    if (firstPoiDate.getTime() <= c.getTimeInMillis()) {
                        destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY) + "." + ((c.get(Calendar.MINUTE)) < 10 ? ("0" + c.get(Calendar.MINUTE)) : c.get(Calendar.MINUTE))));

                    } else {

                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(0);
                } else {
                    if (destinationplanList.get(0).getStarttime() == 24.00) {
                        destinationplanList.get(0).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }


                    destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                    destinationplanList.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                destinationplanList.get(0).setDay(1);
                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

            }

            Log.d("System out", "Destination poi list size " + destinationplanList.size());
           /* if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(0);*/


            Double tempDepart = destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++) {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out", "required time  " + reqtime);
                Log.d("System out", "depature time  " + destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours) {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(j).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart = destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out", "required time  " + destinationplanList.get(j).getDay());


            }


            noday = new ArrayList<>();
            noday.clear();

            //  noday.add(1);

            Integer day = 0;
       /* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*/
            if (destinationplanList.get(0).getDay() == 0) {
                day = destinationplanList.get(0).getDay();
                noday.add(destinationplanList.get(0).getDay());
            }

            for (int i = 0; i < destinationplanList.size(); i++) {

                destinationplanList.get(i).setPos(i);
                try {
                    if (createTripList.get(0).getEnddate().length() > 0) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                        try {
                            Date date = format.parse(createTripList.get(0).getEnddate());
                            c.setTime(date);
                            c.add(Calendar.DATE, day);
                            destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();

                        }

                    }
                } catch (Exception e) {
                }

                Log.d("days", "pos" + day + "v2" + destinationplanList.get(i).getDay());

                if (day != destinationplanList.get(i).getDay()) {
                    day = destinationplanList.get(i).getDay();
                    noday.add(destinationplanList.get(i).getDay());

                }


            }
        }
        Integer last_day = 0;

        if (noday.size() > 0)
            last_day = noday.get(noday.size() - 1);


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == last_day) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        total_time_stay_last = tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getDepartureTime();
        Log.d("total_time_stay_last", String.valueOf(total_time_stay_last));
        Log.d("System out","Reponse of set time of days count "+noday.size());
        Log.d("total_time_stay_last", ""+(noday.get(noday.size()-1)));
        destination_plan[0] = Double.valueOf(noday.get(noday.size()-1)); //day

        destination_plan[1] = Double.valueOf(total_time_stay_last.intValue()); //hours
        BigDecimal bd = new BigDecimal((total_time_stay_last - Math.floor(total_time_stay_last)) * 100);
        bd = bd.setScale(4, RoundingMode.HALF_DOWN);

        destination_plan[2] = Double.valueOf(bd.doubleValue()); //minute


        return destination_plan;


    }

    //set and display route planning
    public void set_KM_Time_POI_Display()
    {
        tempcreatePoiList = new ArrayList<>();
        tempcreatePoiList = dbHandler.get_POI_TRIP(tripid, return_plan);

        remaining_km = 0.0;
        remaining_time = 0.0;

        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++)
        {

            remaining_km = remaining_km + route_direction.getRoutes().get(0).getLegs().get(m).getDistance().getValue();
            remaining_time = remaining_time + route_direction.getRoutes().get(0).getLegs().get(m).getDuration().getValue();

            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                for (int j = 0; j < tempcreatePoiList.size(); j++) {


                    if (tempcreatePoiList.get(j).getFlag() == false)

                    {


                       /* List<LatLng> latLngList = new ArrayList<>();
                        latLngList = decode(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getPolyline().getPoints());

                        for (int i = 0; i < latLngList.size(); i++) {*/


                            double distance1 = distance(tempcreatePoiList.get(j).getPoiLatitude(), tempcreatePoiList.get(j).getPoiLongitude(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getEndLocation().getLat(), route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getEndLocation().getLng());
                            Log.d("distance_point", " dis " + distance1 * 1000);
                            if (distance1 * 1000 <= 500) {


                                if (!tempcreatePoiList.get(j).getFlag())
                                {

                                    tempcreatePoiList.get(j).setFlag(true);
                                    dbHandler.update_trip_poi_track_time_km_google(tempcreatePoiList.get(j).getWayPointId(), remaining_km, remaining_time, m, k);
                                    remaining_km = 0.0;
                                    remaining_time = 0.0;

                                }

                                break;
                            }
                       // }


                    }
                }
            }
        }
        dbHandler.update_KM_Time_Trip(tripid,remaining_km,remaining_time,return_plan);
        get_my_trip();
    }

    /**
     * Decodes an encoded path string into a sequence of LatLngs.
     */
    public List<LatLng> decode(String encodedPath) {

        int len = encodedPath.length();

        List<LatLng> path = new ArrayList<LatLng>(len / 2);
        int index = 0;
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int result = 1;
            int shift = 0;
            int b;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lat += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            result = 1;
            shift = 0;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lng += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            path.add(new LatLng(lat * 1e-5, lng * 1e-5));
        }

        return path;
    }

    private void getRoute_Planning(final String action,String wayPoints) {


        pb_dialog.show();
        ApiInterface apiService = ApiClient.get_retrofit_client_google().create(ApiInterface.class);

        url = null;


        url = Constants.getDirectionsUrl_offline1__without_baseurl(source, destination, wayPoints);

        Log.i("system", "Url of Map if--" + url);

        Call<Direction> call = apiService.GetRoute(url);
        call.enqueue(new Callback<Direction>()
        {
            @Override
            public void onResponse(Call<Direction> call, Response<Direction> response) {
                pb_dialog.dismiss();

                if (response.body() != null) {


                    try {


                        if (response.body().getRoutes().size() > 0)
                        {

                            if(route_direction!=null)
                                old_direction_response=route_direction;
                            route_direction = new Direction();
                            route_direction = response.body();
                            if(action.contains("insert"))
                            {


                                  if(createTripList.get(0).getTripstatus().contains("Saved"))
                                  {
                                      insert_poi_db();
                                  }
                                  else {
                                      Create_Poi create_poi = new Create_Poi();
                                      create_poi.setTripId(tripid);
                                      create_poi.setPoiName("test");
                                      create_poi.setStayTime(stay_hrs);
                                      create_poi.setPoiLatitude(poi_latitude);
                                      create_poi.setPoiLongitude(poi_longitude);
                                      create_poi.setKM(0.0);
                                      create_poi.setTime(0.0);
                                      create_poi.setPoiStay(false);

                                      create_poi.setPoitype("temp");
                                      create_poi.setWayPointId(0000);
                                      create_poi.setPoiDesID(00000);
                                      create_poi.setPoiServerId(0);

                                      if (return_plan == 0)
                                          return_trip_plan = false;
                                      else
                                          return_trip_plan = true;

                                      create_poi.setReturnstatus(return_trip_plan);
                                      create_poi.setPoidescription("temp");
                                      create_poi.setImagename("ro");

                                      dbHandler.add_POI_into_Table(create_poi);
                                      validation_return_date();
                                      dbHandler.delete_POI_ById(0000);

                                      Boolean resume_date_valid = true;
                                      Integer pos1 = 0;
                                      Log.d("temppoilist-size", String.valueOf(tempcreatePoiList.size()));
                                      for (int i = 0; i < tempcreatePoiList.size(); i++) {

                                          if (tempcreatePoiList.get(i).getPoiStay()) {

                                              String arrival_date = tempcreatePoiList.get(i).getActual_arrival_date_time();

                                              Log.d("resumetimedparrival", arrival_date + " pos " + String.valueOf(i));

                                              if (tempcreatePoiList.get(i).getResumeDate() == null) {
                                                  resume_date_valid = true;
                                              } else if (!CheckDates(arrival_date, tempcreatePoiList.get(i).getResumeDate() + " " + tempcreatePoiList.get(i).getResumeTime())) {
                                                  resume_date_valid = false;
                                                  pos1 = i;
                                                  break;
                                              }

                                          }


                                      }


                                      if (!resume_date_valid) {
                                          hide_details_layout();
                                          if (old_direction_response != null) {
                                              route_direction = new Direction();
                                              route_direction = old_direction_response;
                                          }
                                         // Constants.show_error_popup(getActivity(), "Please update trip resume date and time of " + tempcreatePoiList.get(pos1).getPoiName() + " Destination", iv_bottom_up_arrow);
                                          Constants.show_error_popup(getActivity(), "Update date & time to resume your trip from " + tempcreatePoiList.get(pos1).getPoiName() + ".", iv_bottom_up_arrow);

                                      } else if (!CheckDates(tempcreateTripList.get(0).getEnddate() + " " + tempcreateTripList.get(0).getEndtime(), tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time()) && return_plan == 0 && tempcreateTripList.get(0).getReturnstatus()) {
                                          hide_details_layout();

                                          if (old_direction_response != null) {
                                              route_direction = new Direction();
                                              route_direction = old_direction_response;
                                          }

                                          update_trip_return_date("Please update return date and time.");
                                      } else {

                                          insert_poi_db();
                                      }
                                  }


                            }
                            else if(action.contains("delete"))
                            {
                                if (createPoiList.get(delete_pos).getPoitype().contains("DES"))
                                {
                                    create_poi_nightsList = new ArrayList<>();
                                    create_poi_nightsList.clear();

                                    create_poi_nightsList = dbHandler.get_Night_POI_Into_Cart_By_TripId(createPoiList.get(delete_pos).getTripId(), createPoiList.get(delete_pos).getPoiDesID(), return_plan);

                                    JSONArray waypoint_array = new JSONArray();
                                    JSONObject tripwaypoint = new JSONObject();

                                    try {
                                        tripwaypoint = new JSONObject();
                                        tripwaypoint.put("TripWayPointId", createPoiList.get(delete_pos).getWayPointId());
                                        waypoint_array.put(0, tripwaypoint);
                                        for (int i = 0; i < create_poi_nightsList.size(); i++) {

                                            dbHandler.delete_Night_POI_ById(create_poi_nightsList.get(i).getTripId(), create_poi_nightsList.get(i).getWayPointId());
                                            tripwaypoint = new JSONObject();
                                            tripwaypoint.put("TripWayPointId", create_poi_nightsList.get(i).getWayPointId());
                                            waypoint_array.put(i + 1, tripwaypoint);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    String json = waypoint_array.toString();

                                    Delete_Poi_Api(createPoiList.get(delete_pos).getWayPointId(), "POI", json);


                                }
                                else
                                {

                                    String json = "[{     \"TripWayPointId\": \"" + createPoiList.get(delete_pos).getWayPointId() + "\"}]";
                                    Delete_Poi_Api(createPoiList.get(delete_pos).getWayPointId(), "POI", json);
                                }

                            }
                            else
                            {

                                draw_route_display_trip();
                            }
                        } else {
                            Constants.show_error_popup(getActivity(), "Our system is limited to 8 stoppages in a route. We are working to improve it. ", iv_bottom_up_arrow);

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Constants.show_error_popup(getActivity(), "Our system is limited to 8 stoppages in a route. We are working to improve it. ", iv_bottom_up_arrow);
                }




            }

            @Override
            public void onFailure(Call<Direction> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), iv_bottom_up_arrow);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), iv_bottom_up_arrow);
                }

            }
        });

    }

    public void draw_route_display_trip() {

        routelineresult.clear();
        DirectionsJSONParser parser = new DirectionsJSONParser();

        // Starts parsing data
        Gson gson = new GsonBuilder().create();
        String routeresponse = gson.toJson(route_direction);
        try {
            routelineresult = parser.parse(new JSONObject(routeresponse));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        set_KM_Time_POI_Display();
        draw_route();
    }

    //draw route as plannig

    public void validation_return_date() {

        tempcreateTripList = new ArrayList<>();
        tempcreateTripList = dbHandler.get_TRIP_By_TripId(tripid);
        tempcreatePoiList = new ArrayList<>();
        tempcreatePoiList = dbHandler.get_POI_TRIP(tripid, return_plan);
        remaining_km = 0.0;
        remaining_time = 0.0;

        for (int m = 0; m < route_direction.getRoutes().get(0).getLegs().size(); m++)
        {
            remaining_km = remaining_km + route_direction.getRoutes().get(0).getLegs().get(m).getDistance().getValue();
            remaining_time = remaining_time + route_direction.getRoutes().get(0).getLegs().get(m).getDuration().getValue();

            for (int k = 0; k < route_direction.getRoutes().get(0).getLegs().get(m).getSteps().size(); k++) {


                for (int j = 0; j < tempcreatePoiList.size(); j++) {


                    if (tempcreatePoiList.get(j).getFlag() == false)

                    {


                        List<LatLng> latLngList = new ArrayList<>();
                        latLngList = decode(route_direction.getRoutes().get(0).getLegs().get(m).getSteps().get(k).getPolyline().getPoints());

                        for (int i = 0; i < latLngList.size(); i++) {


                            double distance1 = distance(tempcreatePoiList.get(j).getPoiLatitude(), tempcreatePoiList.get(j).getPoiLongitude(), latLngList.get(i).latitude, latLngList.get(i).longitude);
                            Log.d("distance_point", " dis " + distance1 * 1000);
                            if (distance1 * 1000 <= 200) {


                                if (!tempcreatePoiList.get(j).getFlag()) {
                                    tempcreatePoiList.get(j).setStepNo(k);
                                    tempcreatePoiList.get(j).setLegNo(m);
                                    tempcreatePoiList.get(j).setFlag(true);
                                    tempcreatePoiList.get(j).setKM(remaining_km);
                                    tempcreatePoiList.get(j).setTime(remaining_time);
                                    remaining_km = 0.0;
                                    remaining_time = 0.0;
                                }

                                break;
                            }
                        }

                    }
                }
            }
        }

        if(return_plan==0)
        {
            tempcreateTripList.get(0).setTime(remaining_time);
        }
        else
        {
            tempcreateTripList.get(0).setReturnTime(remaining_time);
        }

         for(int pos=0;pos<tempcreatePoiList.size();pos++) {
             //set Arrival departure date and time pos =0
             if (pos == 0)
             {


                 String tripStartDate = "";

                 if (return_plan == 0)
                     tripStartDate = tempcreateTripList.get(0).getStartdate() + " " + tempcreateTripList.get(0).getStarttime();
                 else
                     tripStartDate = tempcreateTripList.get(0).getReturn_start_date() + " " + tempcreateTripList.get(0).getReturn_start_time();


                 Date date = null;
                 try {
                     date = sdf.parse(tripStartDate);
                     Calendar calendar = Calendar.getInstance();
                     calendar.setTime(date);
                     int[] convertime = splitToComponentTimes(tempcreatePoiList.get(0).getTime());
                     calendar.add(Calendar.HOUR, convertime[0]);
                     calendar.add(Calendar.MINUTE, convertime[1]);


                     tempcreatePoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));


                     if (tempcreatePoiList.get(pos).getPoiStay() == false) {
                         BigDecimal bd = new BigDecimal((tempcreatePoiList.get(pos).getStayTime() - Math.floor(tempcreatePoiList.get(pos).getStayTime())) * 100);
                         bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                         int min = bd.intValue();

                         calendar.setTime(calendar.getTime());

                         calendar.add(Calendar.HOUR, tempcreatePoiList.get(pos).getStayTime().intValue());
                         calendar.add(Calendar.MINUTE, min);


                         tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                     } else {
                         date = sdf.parse(tempcreatePoiList.get(pos).getResumeDate() + " " + tempcreatePoiList.get(pos).getResumeTime());
                         calendar.setTime(date);

                         tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));

                     }

                 } catch (ParseException e) {
                     e.printStackTrace();

                 }
             }


             //Log.d("arrivalTime","ar"+createPoiList.get(pos).getArrivalTime()+"de"+createPoiList.get(pos).getDepartureTime()+"re"+createPoiList.get(pos).getReqtime());
             //set arrival date and time
             if (pos >= 1 && (pos <= tempcreatePoiList.size() - 1))
             {


                 int position = pos - 1;

                 String tripStartDate = tempcreatePoiList.get(position).getActual_dep_date_time();


                 Date date = null;
                 try {
                     date = sdf.parse(tripStartDate);
                     Calendar calendar = Calendar.getInstance();
                     calendar.setTime(date);
                     int[] convertime=  splitToComponentTimes(tempcreatePoiList.get(position).getReqtime());

                     calendar.add(Calendar.HOUR, convertime[0]);


                     calendar.add(Calendar.MINUTE,  convertime[1]);


                     tempcreatePoiList.get(pos).setActual_arrival_date_time(sdf.format(calendar.getTime()));


                     if (tempcreatePoiList.get(pos).getPoiStay() == false) {
                       BigDecimal  bd = new BigDecimal((tempcreatePoiList.get(pos).getStayTime() - Math.floor(tempcreatePoiList.get(pos).getStayTime())) * 100);
                         bd = bd.setScale(4, RoundingMode.HALF_DOWN);
                         int min = bd.intValue();

                         calendar.setTime(calendar.getTime());

                         calendar.add(Calendar.HOUR, tempcreatePoiList.get(pos).getStayTime().intValue());
                         calendar.add(Calendar.MINUTE, min);

                         tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));
                     } else {
                         date = sdf.parse(tempcreatePoiList.get(pos).getResumeDate() + " " + tempcreatePoiList.get(pos).getResumeTime());
                         calendar.setTime(date);
                         tempcreatePoiList.get(pos).setActual_dep_date_time(sdf.format(calendar.getTime()));

                     }

                 } catch (ParseException e) {
                     e.printStackTrace();

                 }

             }

             if (tempcreatePoiList.size() == pos + 1) {


                 if (return_plan == 0)
                     tempcreatePoiList.get(pos).setReqtime(tempcreateTripList.get(0).getTime());
                 else
                     tempcreatePoiList.get(pos).setReqtime(tempcreateTripList.get(0).getReturnTime());

                 Date date = null;
                 try {
                     date = sdf.parse(tempcreatePoiList.get(pos).getActual_dep_date_time());
                     Calendar calendar = Calendar.getInstance();
                     calendar.setTime(date);
                     int[] time_h_m_S = splitToComponentTimes(tempcreatePoiList.get(pos).getReqtime());
                     calendar.add(Calendar.HOUR, time_h_m_S[0]);
                     calendar.add(Calendar.MINUTE, time_h_m_S[1]);


                     if (return_plan == 0) {

                         tempcreateTripList.get(0).setEnddate(sdfDate.format(calendar.getTime()));
                         tempcreateTripList.get(0).setEndtime(sdfTime.format(calendar.getTime()));


                     } else {


                         tempcreateTripList.get(0).setReturn_end_date(sdfDate.format(calendar.getTime()));
                         tempcreateTripList.get(0).setReturn_end_time(sdfTime.format(calendar.getTime()));
                     }


                 } catch (ParseException e) {
                     e.printStackTrace();

                 }

             } else {


                 tempcreatePoiList.get(pos).setReqtime(tempcreatePoiList.get(pos + 1).getTime());


             }

         }


    }



    // adapter for horizontal cart item
    public class Service_List_Adapter extends RecyclerView.Adapter<Service_List_Adapter.ViewHolder> {

        public int lastPosition = -1;


        public Service_List_Adapter() {

        }

        // Create new views (invoked by the layout manager)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            // create a new view
            View itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.facility_content_bottom, null);

            // create ViewHolder

            ViewHolder viewHolder = new ViewHolder(itemLayoutView);
            return viewHolder;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

            // - get data from your itemsData at this position
            // - replace the contents of the view with that itemsData

            if (listServiceCategoryList.get(position).isStatus()) {
                viewHolder.tv_facility_name.setTextColor(getResources().getColor(R.color.textYellow));

                fl_pop_up_zoo.setVisibility(View.GONE);
                viewHolder.iv_facility_icon.setImageURI(Constants.ImagePath + listServiceCategoryList.get(position).getSerCatFooterImageSelected());
            } else {
                viewHolder.iv_facility_icon.setImageURI(Constants.ImagePath + listServiceCategoryList.get(position).getSerCatFooterImage());
                viewHolder.tv_facility_name.setTextColor(getResources().getColor(R.color.colorWhite));
            }
            viewHolder.tv_facility_name.setText(listServiceCategoryList.get(position).getSerCatName());
            //setAnimation(viewHolder.itemView, position);

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    category_select_pos = position;

                    for (int i = 0; i < listServiceCategoryList.size(); i++) {
                        listServiceCategoryList.get(i).setStatus(false);
                    }
                    listServiceCategoryList.get(position).setStatus(true);


                    if (position == 0) {

                        if (selected_ro_pos > -1 && onAdminRouteRoLists.size() > 0) {
                            onAdminRouteRoLists.get(selected_ro_pos).setClick_status(false);
                        }

                        if (map != null)
                            add_marker_ro();


                    } else if (position == 1) {
                        if (selected_poi_pos > -1 && routepojo.size() > 0) {
                            onAdminRoutePoiLists.get(selected_poi_pos).setClick_status(false);
                        }
                        if (map != null)
                            add_marker_poi();
                    }
                    else
                    {
                       /* if (selected_ser_pos > -1 && services.size() > 0) {
                            services.get(selected_ser_pos).setClick_status(false);
                        }
                        if (map != null)
                            add_marker_services(listServiceCategoryList.get(position).getSerCatId());*/
                        selected_ser_pos = -1;
                        if (return_plan == 0)
                            getServices(orginlat, originlong, listServiceCategoryList.get(position).getSerCatName(), routepojo.get(0).getRouteId());
                        else
                            getServices(deslat, deslong, listServiceCategoryList.get(position).getSerCatName(), routepojo.get(0).getRouteId());
                    }


                    serviceListAdapter.notifyItemRangeChanged(0, listServiceCategoryList.size());


                }
            });

        }

        // Return the size of your itemsData (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return listServiceCategoryList.size();
        }

        private void setAnimation(View viewToAnimate, int position) {
            // If the bound view wasn't previously displayed on screen, it's animated
            if (position > lastPosition) {
                ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
                viewToAnimate.startAnimation(anim);
                lastPosition = position;
            }
        }

        // inner class to hold a reference to each item of RecyclerView
        public class ViewHolder extends RecyclerView.ViewHolder {


            SimpleDraweeView iv_facility_icon;
            TextView tv_facility_name;

            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                iv_facility_icon = (SimpleDraweeView) itemLayoutView.findViewById(R.id.fc_cat);
                tv_facility_name = (TextView) itemLayoutView.findViewById(R.id.tv_facility_name);

            }
        }


    }

    private class TollBoothAdapter extends BaseAdapter {
        Activity activity;
        private ArrayList<TollBooth> arraylist;

        TollBoothAdapter(Activity activity, ArrayList<TollBooth> ExpertsexpertData11) {
            this.activity = activity;
            this.arraylist = ExpertsexpertData11;

        }

        @Override
        public int getCount() {
            return arraylist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            LayoutInflater infaltor = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.custom_tollbooth_item, null);

            TextView tv_name, tv_distance, tv_remark;


            tv_name = (TextView) v.findViewById(R.id.tv_toolbooth_name);
            tv_distance = (TextView) v.findViewById(R.id.tv_distance);
            tv_remark = (TextView) v.findViewById(R.id.tv_toolbooth_remarks);

            Log.d("tollboothname", arraylist.get(position).getTollBoothName());
            tv_name.setText(arraylist.get(position).getTollBoothName());

            tv_distance.setText(arraylist.get(position).getDistance().toString() + " Kms");
            tv_remark.setText(arraylist.get(position).getTollRemark());


            return v;

        }


    }


    //way point for insert
    public  String set_wayPoint_insert(Double poi_latitude,Double poi_longitude)
    {
        wayPoints="";
        wayPoints=wayPoints_server;
        List<Create_Poi> create_poiList1 = new ArrayList<>();
        create_poiList1 = dbHandler.get_POI_TRIP(tripid, return_plan);


        for (int i = 0; i < create_poiList1.size(); i++) {

            wayPoints += create_poiList1.get(i).getPoiLatitude() + "," + create_poiList1.get(i).getPoiLongitude() + "|";
        }

        wayPoints=wayPoints+poi_latitude.toString()+","+poi_longitude+"|";

        return  wayPoints;

    }

    //way point for delete
    public  String set_wayPoint_delete(Integer waypointId)
    {
        wayPoints="";
        wayPoints=wayPoints_server;
        List<Create_Poi> create_poiList1 = new ArrayList<>();
        create_poiList1 = dbHandler.get_POI_TRIP(tripid, return_plan);


        for (int i = 0; i < create_poiList1.size(); i++) {

            Log.d("wayPoiPointId","pointId"+create_poiList1.get(i).getWayPointId()+" way "+waypointId);

            if(create_poiList1.get(i).getWayPointId().intValue()!=waypointId)
            {
                wayPoints = wayPoints+create_poiList1.get(i).getPoiLatitude() + "," + create_poiList1.get(i).getPoiLongitude() + "|";
            }

        }

        return  wayPoints;

    }

    public  void display_S_TO_D()
    {
        S_To_D_Plan = 0;
        return_plan=0;
        return_trip_plan=false;
        if (plantype.length() <= 1)
        {
            tv_S_to_D.setTextColor(getResources().getColor(R.color.textYellow));
            tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
            tv_plan_at_D.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv_D_to_S.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        }

        ll_route_plan.setVisibility(View.VISIBLE);
        iv_tollbooth.setVisibility(View.GONE);
        ll_des_plan.setVisibility(View.GONE);

        selected_poi_pos = -1;
        selected_ro_pos = -1;
        selected_ser_pos = -1;
        selected_des_pos = -1;
        for (int i = 0; i < listServiceCategoryList.size(); i++) {
            listServiceCategoryList.get(i).setStatus(false);
        }

        if (listServiceCategoryList.size() > 0)
            listServiceCategoryList.get(0).setStatus(true);

        serviceListAdapter.notifyDataSetChanged();

        tv_origin.setText(createTripList.get(0).getSource());
        tv_destination.setText(createTripList.get(0).getDestination());
        set_header();

        getRouteOfPOI(orginId, destinationId);


        tv_place_of_des.setVisibility(View.VISIBLE);
       // ll_bottom_up.setVisibility(View.VISIBLE);
        iv_bottom_up_arrow.setVisibility(View.VISIBLE);
      //  iv_edit_stay_time.setVisibility(View.VISIBLE);
        tv_edit.setVisibility(View.VISIBLE);


        //check des and Return plan
        if (destinationplanList.size()<= 0 )
        {
            if(createTripList.get(0).getDesNoOfNearByAttraction()==0)
                tv_explore_from_map.setVisibility(View.GONE);
            else
                tv_explore_from_map.setVisibility(View.VISIBLE);
        }
        else
        {
            tv_explore_from_map.setVisibility(View.GONE);

        }
        tv_good_to_go_map.setVisibility(View.GONE);
        List<Create_Poi> count_create_poi_list = dbHandler.get_POI_TRIP(tripid, 1);
        if (count_create_poi_list.size() > 0) {
            ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
           /* if ( createTripList.get(0).getReturn_start_date()!=null) {
                tv_good_to_go_map.setVisibility(View.VISIBLE);
            }*/
        } else {
            ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        }
    }
    public  void display_D_To_S()
    {
        return_plan=1;
        return_trip_plan=true;
        List<Create_Poi> count_create_poi_list1 = dbHandler.get_POI_TRIP(tripid, 0);
        if (count_create_poi_list1.size() > 0) {
            ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        }
        else
        {
            ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        }
        createTripList = dbHandler.get_TRIP_By_TripId(tripid);
        llserviceslayout.setVisibility(View.VISIBLE);
        S_To_D_Plan = 2;
        edit_stay_time = false;
        iv_edit_stay_time.setImageResource(R.drawable.edit_icon_unselected_my_plan);
        tv_edit.setTextColor(getResources().getColor(R.color.colorWhite));
        tv_explore_from_map.setVisibility(View.GONE);
        if (plantype.length() <= 1) {
            tv_S_to_D.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv_plan_at_D.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv_D_to_S.setTextColor(getResources().getColor(R.color.textYellow));
            tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
        }

        ll_route_plan.setVisibility(View.VISIBLE);
        iv_tollbooth.setVisibility(View.GONE);
        ll_des_plan.setVisibility(View.GONE);
        return_plan = 1;
        return_trip_plan = true;
        tv_place_of_des.setVisibility(View.GONE);
        selected_poi_pos = -1;
        selected_ro_pos = -1;
        selected_ser_pos = -1;
        selected_des_pos = -1;
        for (int i = 0; i < listServiceCategoryList.size(); i++) {
            listServiceCategoryList.get(i).setStatus(false);
        }
        if (listServiceCategoryList.size() > 0)
            listServiceCategoryList.get(0).setStatus(true);
        serviceListAdapter.notifyDataSetChanged();
     //   ll_bottom_up.setVisibility(View.VISIBLE);
        iv_bottom_up_arrow.setVisibility(View.VISIBLE);
        //iv_edit_stay_time.setVisibility(View.VISIBLE);
        tv_edit.setVisibility(View.VISIBLE);
        tv_origin.setText(createTripList.get(0).getDestination());
        tv_destination.setText(createTripList.get(0).getSource());
        set_header();
        getRouteOfPOI(destinationId, orginId);
      //  tv_D_to_S_from_left_drawer.setVisibility(View.GONE);
       // List<Create_Poi> count_create_poi_list = dbHandler.get_POI_TRIP(tripid, 1);
       // if (count_create_poi_list.size()>0 && createTripList.get(0).getReturn_start_date()!=null) {
        if(frommytrips.equalsIgnoreCase("mytrips"))
        {
            tv_good_to_go_map.setVisibility(View.GONE);

        }else {
            tv_good_to_go_map.setVisibility(View.VISIBLE);
        }
       /* } else {
            tv_good_to_go_map.setVisibility(View.GONE);
        }*/
        tv_start_date.setText(createTripList.get(0).getReturn_start_date());
    }
    public  void display_des_plan()
    {

        tv_explore_from_map.setVisibility(View.GONE);
        llserviceslayout.setVisibility(View.GONE);
        edit_stay_time = false;
       // iv_edit_stay_time.setImageResource(R.drawable.edit_icon_unselected_my_plan);
        tv_edit.setTextColor(getResources().getColor(R.color.colorWhite));

        S_To_D_Plan = 1;
        editIconUnselected = edit_stay_time;
        ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        ll_route_plan.setVisibility(View.GONE);
        iv_tollbooth.setVisibility(View.GONE);
        ll_des_plan.setVisibility(View.VISIBLE);
        if (plantype.length() <= 1) {
            tv_S_to_D.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_S_to_D_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv_plan_at_D.setTextColor(getResources().getColor(R.color.textYellow));
            tv_plan_at_D_tab_line.setBackgroundColor(getResources().getColor(R.color.textYellow));
            tv_D_to_S.setTextColor(getResources().getColor(R.color.colorWhite));
            tv_D_to_S_tab_line.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        }
        return_plan = 0;
        return_trip_plan = false;
        ll_D_to_S_from_left_drawer.setVisibility(View.GONE);
        selected_poi_pos = -1;
        selected_ro_pos = -1;
        selected_ser_pos = -1;
        selected_des_pos = -1;
        for (int i = 0; i < listServiceCategoryList.size(); i++) {
            listServiceCategoryList.get(i).setStatus(false);
        }
        if (listServiceCategoryList.size() > 0)
            listServiceCategoryList.get(0).setStatus(true);
        serviceListAdapter.notifyDataSetChanged();
        if (!desplan)
        {
            for (int i = 1; i < destinationplanList.size(); i++) {
                getdistancetime(i);
            }
        }
        else {
        }
        if (destinationplanList.size() == 1) {
            set_destination_layout();
        }
        tv_origin.setText(createTripList.get(0).getSource());
        tv_destination.setText(createTripList.get(0).getDestination());
        set_header();
        getDestinationPOIAPI(createTripList.get(0).getDestinationId(), createTripList.get(0).getDestination_latitude(), createTripList.get(0).getDestination_longitude());
        ll_bottom_up.setVisibility(View.GONE);
        iv_bottom_up_arrow.setVisibility(View.GONE);
       // iv_edit_stay_time.setVisibility(View.GONE);
        tv_edit.setVisibility(View.GONE);

    }

    public void set_header()
    {
        if(createTripList.size()>0)
        {
            if(return_plan==1) {
                ((ExploreActivity) getActivity()).tv_origin_city_header.setText(tv_origin.getText().toString());
                ((ExploreActivity) getActivity()).tv_dest_city_header.setText(tv_destination.getText().toString());
                tv_start_time.setText(convert_time_12_format(createTripList.get(0).getReturn_start_time()));
                tv_start_date.setText(createTripList.get(0).getReturn_start_date());
                tv_end_date.setText(createTripList.get(0).getReturn_end_date());
                tv_end_time.setText(convert_time_12_format(createTripList.get(0).getReturn_end_time()));
            }
            else
            {
                ((ExploreActivity) getActivity()).tv_origin_city_header.setText(tv_origin.getText().toString());
                ((ExploreActivity) getActivity()).tv_dest_city_header.setText(tv_destination.getText().toString());
                tv_start_time.setText(convert_time_12_format(createTripList.get(0).getStarttime()));
                tv_start_date.setText(createTripList.get(0).getStartdate());
                tv_end_date.setText(createTripList.get(0).getEnddate());
                tv_end_time.setText(convert_time_12_format(createTripList.get(0).getEndtime()));
            }

        }

    }

    public  void hide_details_layout()
    {
        if (fl_pop_up_zoo.getVisibility() == View.VISIBLE) {
            Animation bottomUpDown = AnimationUtils.loadAnimation(getContext(),
                    R.anim.bottom_down);
            fl_pop_up_zoo.startAnimation(bottomUpDown);
            fl_pop_up_zoo.setVisibility(View.GONE);
        }
    }


    public  void set_selected_map_icon()
    {

        if (category_select_pos == 0)
        {

            if (selected_ro_pos > -1)
                onAdminRouteRoLists.get(selected_ro_pos).setClick_status(false);

            if (map != null)
                add_marker_ro();


        }
        else if (category_select_pos == 1) {

            if (selected_poi_pos > -1)
                onAdminRoutePoiLists.get(Integer.valueOf(selected_poi_pos)).setClick_status(false);

            if (map != null)
                add_marker_poi();
        } else {
            if (selected_ser_pos > -1)
                services.get(selected_ser_pos).setClick_status(false);

            if (map != null)
                add_marker_services();
        }

    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    private void getlatlongfromplace(String place) {
//        if (pb_placefrg.getVisibility()==View.GONE){
//            pb_placefrg.setVisibility(View.VISIBLE);
//        }

        final Geocoder geocoder = new Geocoder(getActivity());

        try {
            List<Address> addresses = null;
            Log.d("place_address","place___ "+place.trim().toLowerCase()+"     "+createTripList.get(0).getDestination().toLowerCase());
            Log.d("place_address","place___ "+place.toLowerCase().contains(" "+createTripList.get(0).getDestination().toLowerCase()));
           /* if(place.toLowerCase().contains(" "+createTripList.get(0).getDestination().toLowerCase()))
            {
                addresses=geocoder.getFromLocationName(place, 1);
            }else
            {
                addresses=geocoder.getFromLocationName(place+ "," + createTripList.get(0).getDestination(), 1);
            }*/

        addresses=geocoder.getFromLocationName(place, 1);
           /* String[] placeAddress = place.split(",");
            String tempaddress="";

            for(int i=0;i<placeAddress.length;i++)
            {
                Log.d("placeAddress___","place___ "+placeAddress[i] + createTripList.get(0).getDestination());
                Log.d("place_address","place___ "+placeAddress[i].toString().trim().equalsIgnoreCase(createTripList.get(0).getDestination().toString()));
                if(placeAddress[i].toString().trim().equalsIgnoreCase(createTripList.get(0).getDestination().toString()))
            {
                //addresses=geocoder.getFromLocationName(place, 1);
                tempaddress=place;
                break;
            }else
            {
                //addresses=geocoder.getFromLocationName(place+ "," + createTripList.get(0).getDestination(), 1);
                tempaddress=place+ "," + createTripList.get(0).getDestination();
            }
            }
            addresses=geocoder.getFromLocationName(tempaddress, 1);*/
            Log.d("place_address","size "+addresses.toString());

            if (addresses != null && !addresses.isEmpty() && addresses.size()>0) {


                Address address = addresses.get(0);
                Log.d("place_address","size "+address.toString());
                dbHandler.update_place_stay_at_destination(tripid,place,address.getLatitude(),address.getLongitude());
                update_trip();

                Constants.hidekeyboard(getActivity(),tv_place_of_des);

            }
            else {
                Constants.hidekeyboard(getActivity(),tv_place_of_des);

                tv_place_of_des.setText("");

                Constants.show_error_popup(getActivity(), "Provide valid address. ", tv_place_of_des);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  public void  DrawerMovement(String drawerOpen)
    {

        if(drawerOpen.equalsIgnoreCase("left")) {
          //  iv_left_arrow_map.setImageResource(R.drawable.map_arrow_right_slider);
            iv_left_arrow_map.setText("O\nP\nE\nN");
            drawer_layout_services.openDrawer(left_navView);
            ll_bottom_up.setVisibility(View.GONE);
            iv_bottom_up_arrow.setVisibility(View.GONE);
        }else
        {

            // iv_left_arrow_map.setImageResource(R.drawable.map_arrow_left_slider);
            iv_left_arrow_map.setText("O\nP\nE\nN");
            tv_add.setTextColor(getResources().getColor(R.color.colorWhite));
            drawer_layout_services.closeDrawer(left_navView);
            ll_bottom_up.setVisibility(View.VISIBLE);
            iv_bottom_up_arrow.setVisibility(View.VISIBLE);
        }
    }
}

