package com.bpcl.happyroads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bpcl.happyroads.CustomeClass.CirclePageIndicator;
import com.bpcl.happyroads.Database.DBHandler;
import com.bpcl.happyroads.Pojo.AddFavourite;
import com.bpcl.happyroads.Pojo.CreateTripApi;
import com.bpcl.happyroads.Pojo.Create_Poi;
import com.bpcl.happyroads.Pojo.Create_Poi_Night;
import com.bpcl.happyroads.Pojo.Create_Trip;
import com.bpcl.happyroads.Pojo.DestinationPlan;
import com.bpcl.happyroads.Pojo.ExploreDestination;
import com.bpcl.happyroads.Pojo.ExplorePOI;
import com.bpcl.happyroads.Pojo.GetAdvancePOIDetails;
import com.bpcl.happyroads.Pojo.Insert_Poi;
import com.bpcl.happyroads.Pojo.MyTrip;
import com.bpcl.happyroads.Pojo.PoiOtherFacilityList;
import com.bpcl.happyroads.Pojo.TripListforPOIPlanning;
import com.bpcl.happyroads.Utils.Constants;
import com.bpcl.happyroads.Utils.DownloadCode;
import com.bpcl.happyroads.Utils.ImageSaver;
import com.bpcl.happyroads.Utils.IsNetworkConnection;
import com.bpcl.happyroads.Utils.LoginModule;
import com.bpcl.happyroads.Utils.SessionManager;
import com.bpcl.happyroads.retrofit.ApiClient;
import com.bpcl.happyroads.retrofit.ApiInterface;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ADMIN on 8/30/2016.
 */
public class ExploreAttractionFragment extends Fragment  implements
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener{
    TextView tv_add_to_my_plan_attraction,tv_about_attraction_name,tv_entry_fee_poi;
    String posi = "";
    ListView POIListing_listing;
    String name = "";
    String destName = "";
    ImageView iv_like_icon,iv_share_img;
    boolean like=false;
    FrameLayout main_fl;
    LinearLayout main_ll_second,main_ll_first;
    CirclePageIndicator img_circle_indicator;
    private int screenHeight;
    int originasize = 0;
    int scroll=0;
    private boolean isscroll=true;
    Boolean readmoreclicked=false;
    int pos;
    Boolean isfrombottom = false;
    int lastscrollamount=0;
    View firstChildInList;
    ImageView iv_nearby_activities_poi,iv_from_to_month_poi,iv_gallary_poi,iv_required_time_poi;
    TextView tv_nearby_activities_poi,tv_from_to_month_poi,tv_required_time_poi,tv_nearby_activities_num_poi,tv_rating_poi_detail;
    private int topOffset=0;
    LinearLayout ll_nearby_places_header;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    ArrayList<ExplorePOI> explorePOIList=new ArrayList<>();
    ArrayList<GetAdvancePOIDetails> getAdvancePOIDetaillist=new ArrayList<>();
    ArrayList<TripListforPOIPlanning> myTripArrayList = new ArrayList<>();
    static RelativeLayout error_layout;
    Gson gson=new Gson();
    String FeesCatName="";
    ArrayList<AddFavourite> addFavList=new ArrayList<AddFavourite>();
    SessionManager sessionManager;
    ExplorePOIAdapter explorePOIAdapter;
    DBHandler dbHandler;
    Dialog   pb_dialog;
    Integer tripid=0;
    String destId="";
    Dialog  myTripDialog;
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    boolean tripFlag = false;
    TextView tv_start_date_my_trip, tv_return_date_my_trip;
    String select="";
    String start_date="",start_time="",end_date="",end_time="";
    String date;
    String date1=null;
    ArrayList<String> searchtripesname = new ArrayList<String>();
    ArrayList<String> searchtripesnameby;
    ArrayList<String> searchtripesids = new ArrayList<String>();
    Bitmap originbitmap = null, desbitamp = null;
    String trip_return_end_date = "", trip_return_end_time = "", trip_end_date_eta = "", trip_end_time_eta = "";
    ArrayList<MyTrip> mySpecificTripArrayList = new ArrayList<>();
    String tripdate_eta="",trip_return_date_eta="";
    public static ArrayList<ExploreDestination> exploreDestList=new ArrayList<>();
    public static Bitmap destinationbmp=null;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public static Integer poiId;
    EditText et_trip_name;
    TextView tv_save_my_trip;
     AutoCompleteTextView av_tripname;
    //change
    List<Create_Trip> create_tripArrayList=new ArrayList<>();
    String excepted_date="", excepted_time = "",return_excepted_date="",return_excepted_time="";
    Double dayhours = 8.0;
    Double starttime=7.0;
    List<DestinationPlan> destinationplanList = new ArrayList<>();
    ArrayList<Integer> noday;
    ArrayList<DestinationPlan> tempdestiDestinationPlanlist;
    static Context context;
    String plan_type = "";
    int DestinationPos=0;
    String temptime="19:00";
    boolean setclick=false;



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.explore_attraction_fragment, container, false);
        dbHandler=new DBHandler(getActivity());
        pb_dialog=new Dialog(getActivity());
        context=getActivity();
        initVar(v);
        error_layout=(RelativeLayout)v.findViewById(R.id.error_layout);
        sessionManager=new SessionManager(getActivity());
        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            POIListing_listing.setNestedScrollingEnabled(true);
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            plan_type = bundle.getString("plan_type", "");
            posi = bundle.getString("position");
            name = bundle.getString("placeName");
            destName = bundle.getString("DestName");
            destId = bundle.getString("destId");
            tripid= bundle.getInt("tripId",0);
        }
        Log.d("System out","Response of trip id on create,positon"+tripid+"   "+posi);
        String jsonOutput = sharedpreferences.getString("POIList","");
        Type listType = new TypeToken<ArrayList<ExplorePOI>>(){}.getType();
        if(explorePOIList.size()<=0) {
            explorePOIList = gson.fromJson(jsonOutput, listType);

        }
        Fresco.initialize(getActivity(), Constants.AddImagesToCache(getActivity()));
        //change
        create_tripArrayList = dbHandler.get_TRIP_By_TripId(tripid);
        destinationplanList = dbHandler.get_DestinationList(tripid);
        if (create_tripArrayList.size() > 0) {


            for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++) {
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                    starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
                if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                    dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

            }
            String tripdate = create_tripArrayList.get(0).getReturn_start_date() + " " + starttime;


            excepted_date = create_tripArrayList.get(0).getReturn_start_date();
            try {



            excepted_time =Constants.formatDate(create_tripArrayList.get(0).getReturn_start_time(), "HH:mm", "hh:mm a");
                start_date=create_tripArrayList.get(0).getStartdate();
                Log.d("System out","tripdate___"+start_date +" "+start_time);
                start_time=create_tripArrayList.get(0).getStarttime();
                Log.d("System out","tripdate___"+start_date +" "+start_time);
                end_date  = create_tripArrayList.get(0).getReturn_start_date();
                Log.d("System out","returndate___"+end_date+"  "+end_time);
                end_time = create_tripArrayList.get(0).getReturn_end_time();

                Log.d("System out","returndate___"+end_date+"  "+end_time);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }


           // set_destination_layout();
        }



        ((ExploreActivity)getActivity()).tv_text_header.setText(name);
        if(sharedpreferences.getString("DestList","").equalsIgnoreCase(""))
        {
            destList();
        }
        else {

            String jsonOutput1 = sharedpreferences.getString("DestList", "");
            Type listType1 = new TypeToken<ArrayList<ExploreDestination>>() {
            }.getType();
            exploreDestList = gson.fromJson(jsonOutput1, listType1);
     }
        DestinationPos=0;

        for(int i=0;i<exploreDestList.size();i++)
        {
            if(String.valueOf(exploreDestList.get(i).getDesID()).equalsIgnoreCase(destId))
            {
                DestinationPos=i;
                break;
            }
        }
        if (!TextUtils.isEmpty(posi))
        {
            setdPOIdetail();
            AdvancePOIDetail();
        }
        explorePOIAdapter=new ExplorePOIAdapter(getActivity(),explorePOIList);
        POIListing_listing.setAdapter(explorePOIAdapter);
        if( ((ExploreActivity)getActivity()).pager!=null) {
            ((ExploreActivity)getActivity()).setpagerdata(true);
            final float density = getResources().getDisplayMetrics().density;
            img_circle_indicator.setRadius(2 * density);
            img_circle_indicator.setViewPager(((ExploreActivity) getActivity()).pager);

            //    final float density = getResources().getDisplayMetrics().density;

              //  indicator.setRadius(3 * density);

//
//         NUM_PAGES = 5;

                // Auto start of viewpager
               /* final Handler handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                        }
                        ((ExploreActivity)getActivity()).pager.setCurrentItem(currentPage++, true);
                    }
                };
                Timer swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 3000, 3000);

                // Pager listener over indicator
            img_circle_indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageSelected(int position) {
                        currentPage = position;

                    }

                    @Override
                    public void onPageScrolled(int pos, float arg1, int arg2) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int pos) {

                    }
                });

*/

        }
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenHeight = metrics.heightPixels;
        POIListing_listing.setScrollingCacheEnabled(true);
        POIListing_listing.setSaveEnabled(true);
        originasize = main_ll_first.getHeight();
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);
        poiId=explorePOIList.get(Integer.parseInt(posi)).getPOINearbyDestination();
        iv_gallary_poi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("System out","Response of selected image size "+explorePOIList.get(Integer.parseInt(posi)).getImageList().size());
                ArrayList<String> photoGallary = new ArrayList<>();
                for (int i = 0; i<explorePOIList.get(Integer.parseInt(posi)).getImageList().size(); i++)
                {
                    if(explorePOIList.get(Integer.parseInt(posi)).getImageList().get(i).getImgDescription().equalsIgnoreCase("Photo")) {
                        photoGallary.add(explorePOIList.get(Integer.parseInt(posi)).getImageList().get(i).getImgName());

                    }
                }
                if(photoGallary.size() >0) {
                    Log.d("System out","Response of selected image size in attreaction "+explorePOIList.get(Integer.parseInt(posi)).getImageList().size());
                    GallaryFragment gallaryfragment = new GallaryFragment();
                    FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // ((ExploreActivity)getActivity()).setHeader("Change Password");
                    changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                    changeTransaction.replace(R.id.frame, gallaryfragment);
                    changeTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("position", posi);
                    bundle.putString("from", "POIListing");
                    gallaryfragment.setArguments(bundle);
                    changeTransaction.commit();
                    ((ExploreActivity) getActivity()).rl_main_heder.setVisibility(View.GONE);
                }
                else
                {
                    Constants.show_error_popup(getActivity(),"No images available.",error_layout);
                }
            }
        });
        POIListing_listing.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.d("System out", "scrollState: " + scrollState+" sharedpreferences readmoreclicked "+sharedpreferences.getString("isinreadless","false"));
                if (scrollState == 0 || scrollState == SCROLL_STATE_IDLE) {
                    isscroll = false;
                } else {
                    isscroll = true;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {


                Log.d("System out", "isscroll In Scroll: " + isscroll+" readmoreclicked "+readmoreclicked+"pref value"+sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("true"));
                if (sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("false") &&  isscroll==true ) {
                    if (POIListing_listing != null) {

                        scroll = getScroll(POIListing_listing);
                        if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1)
                        {
                        }
                    }
                    View v = POIListing_listing.getChildAt(0);
                    topOffset = (v == null) ? 0 : v.getTop();
                    if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1) {
                        POIListing_listing.setSelectionFromTop(POIListing_listing.getLastVisiblePosition(), topOffset);
                    }

                    if (scroll == 0 && POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1 && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getTop()!=0) {

                    //    settooriginal();
                    }
                    else if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1
                            && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() <= POIListing_listing.getHeight()) {
                        //else if ((scroll > lastscrollamount + 5 || scroll > lastscrollamount - 5) && scroll >= POIListing_listing.getMaxScrollAmount() / 2) {
                        settopimage();
                    }
                    else if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1
                            && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() >= POIListing_listing.getHeight() && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getTop()!=0)
                    {
                        if (sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("false"))
                        {
                          //  settooriginal();
                        }
                        isfrombottom=false;


                    }

                    else if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1
                            && (POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() == POIListing_listing.getHeight() || POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getTop() == POIListing_listing.getHeight())) {
                        setmiddleimage();
                    }
                }
                else
                {
                    if (POIListing_listing != null) {
                        scroll = getScroll(POIListing_listing);
                        Log.d("System out", "Scroll: " + scroll+"isscroll :::"+isscroll);
                        Log.d("System out", "view pos: " + view.getVerticalScrollbarPosition());
                        Log.d("System out", "view pos: " + view.getScrollY());
                        if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1) {
                            Log.d("System out", "main list height: " + POIListing_listing.getHeight() / 2 + " child height :::" + (POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getHeight()) / 2);
                            Log.d("System out", "main bottom  height: " + POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() + " main height :::" + POIListing_listing.getHeight());
                            Log.d("System out", "main bottom  height: " + POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getTop());
                        }
                    }
                    View v = POIListing_listing.getChildAt(0);
                    topOffset = (v == null) ? 0 : v.getTop();
                    if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1) {
                        POIListing_listing.setSelectionFromTop(POIListing_listing.getLastVisiblePosition(), topOffset);
                    }
                    settopimage();
                    if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1
                            && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() <= POIListing_listing.getHeight()) {

                        //else if ((scroll > lastscrollamount + 5 || scroll > lastscrollamount - 5) && scroll >= POIListing_listing.getMaxScrollAmount() / 2) {
                        settopimage();

                    }
                    else if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1
                            && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() >= POIListing_listing.getHeight() && POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getTop()!=0)
                    {
                        //  settooriginal();

                        //   isfrombottom=false;
                    }

                    else if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1
                            && (POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getBottom() == POIListing_listing.getHeight() || POIListing_listing.getChildAt(POIListing_listing.getChildCount() - 1).getTop() == POIListing_listing.getHeight())) {
                        setmiddleimage();
                    }
                }
                if (POIListing_listing.getLastVisiblePosition() == POIListing_listing.getAdapter().getCount() - 1) {
                    POIListing_listing.setSelectionFromTop(POIListing_listing.getLastVisiblePosition(), topOffset);
                }

            }
        });
        ll_nearby_places_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putString("from","nearbyactivities");
                b.putString("destLat",explorePOIList.get(Integer.parseInt(posi)).getPOILatitude());
                b.putString("destLong",explorePOIList.get(Integer.parseInt(posi)).getPOILongitude());
                b.putString("destID",explorePOIList.get(Integer.parseInt(posi)).getPOIID()+"");
                b.putString("placeName",destName);
                ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new ExplorePlacesFragment(),b);

            }
        });
        iv_like_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
                    if(sessionManager.isLoggedIn())
                    {

                        String json = "[{     \"FavUmId\": \""+sessionManager.get_Authenticate_User().getUmId()+"\", \"FavType\": \"POI\",   \"FavRefId\": \""+explorePOIList.get(Integer.parseInt(posi)).getPOIID()+"\"}]";
                        Log.d("System out", "In add favourite  " + json);
                        AddFavouritesAPI(json,(ImageView) v);
                    }else
                    {
                        LoginModule loginModule = new LoginModule(getActivity(),"favourite Destination");
                        loginModule.signIn();
                    }
                }
                else
                {
                    Constants.show_error_popup(getActivity(), "" + R.string.internet_error, error_layout);
                }
            }
            private void AddFavouritesAPI(final String json, final ImageView imageView) {
                final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
                pb_dialog.setCancelable(false);
                pb_dialog.show();
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ArrayList<AddFavourite>> call = apiService.addFavourites(json);
                call.enqueue(new Callback<ArrayList<AddFavourite>>() {
                    @Override
                    public void onResponse(Call<ArrayList<AddFavourite>> call, Response<ArrayList<AddFavourite>> response) {
                        if (response.body() != null) {
                            pb_dialog.dismiss();
                            addFavList= response.body();
                            if (addFavList.get(0).getResStatus() == true)
                            {
                                if(addFavList.get(0).getResDescription().toLowerCase().contains("add")) {

                                    explorePOIList.get(Integer.parseInt(posi)).setFavId(addFavList.get(0).getFavId());
                                    imageView.setImageResource(R.drawable.like_icon_selected_detail);
                                    final Dialog dialog1 = new Dialog(getActivity());
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog1.setContentView(R.layout.like_remove_layout);
                                    final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                    tv_text_like_remove.setText("Added to your favourites");
                                    dialog1.setCanceledOnTouchOutside(true);
                                    dialog1.show();
                                    like = true;
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(1500);
                                                ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();

                                                        //Code for the UiThread
                                                    }
                                                });
                                                /*runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();
                                                    }
                                                });*/
                                            } catch (Exception e) {
                                                Log.w("Exception in splash", e);
                                            }

                                        }
                                    }).start();


                                }
                                else
                                {

                                    explorePOIList.get(Integer.parseInt(posi)).setFavId(0);
                                    imageView.setImageResource(R.drawable.like_icon_unselected_detail);
                                    final Dialog dialog1 = new Dialog(getActivity());
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog1.setContentView(R.layout.like_remove_layout);
                                    final TextView tv_text_like_remove = (TextView) dialog1.findViewById(R.id.tv_text_like_remove);
                                    tv_text_like_remove.setText("Removed from your favourites");
                                    dialog1.setCanceledOnTouchOutside(true);
                                    dialog1.show();
                                    like = true;
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(1500);
                                                ((Activity) getActivity()).runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();

                                                        //Code for the UiThread
                                                    }
                                                });
                                               /* runOnUiThread(new Runnable() {

                                                    @Override
                                                    public void run() {
                                                        dialog1.dismiss();
                                                    }
                                                });*/
                                            } catch (Exception e) {
                                                Log.w("Exception in splash", e);
                                            }

                                        }
                                    }).start();


                                }
                            }
                            else
                            {
                                // Constants.show_error_popup(getActivity(), ""+addFavList.get(0).getResDescription(), error_layout);
                                //Toast.makeText(context,""+signupList.getResDescription(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<ArrayList<AddFavourite>> call, Throwable t) {
                        // Log error here since request failed
                        pb_dialog.dismiss();
                        t.printStackTrace();
                    }
                });
            }

        });
        ((ExploreActivity)getActivity()).iv_home_icon_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(plan_type.length()>0)
                {


                }else {*/


                    try {
                        ((ExploreActivity) getActivity()).onBackPressed();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
               // }
            }
        });

        return v;
    }

    private void AdvancePOIDetail() {
        if (IsNetworkConnection.checkNetworkConnection(getActivity())) {
            getAdvancePOIDetailAPI();

        }
        else
        {
            Toast.makeText(getActivity(),""+R.string.internet_error,Toast.LENGTH_SHORT).show();

        }
    }

    public void getAdvancePOIDetailAPI() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        final Dialog pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();
        String json = null;
        String id="1";
        int pos=Integer.parseInt(posi);

        json = "[{     \"AdvPOIId\": \""+explorePOIList.get(pos).getPOIID()+"\"}]";
        Log.d("System out","advance profile ____"+json);
        Call<ArrayList<GetAdvancePOIDetails>> call = apiService.getAdvancePoiDetail(json);
        call.enqueue(new Callback<ArrayList<GetAdvancePOIDetails>>() {
            @Override
            public void onResponse(Call<ArrayList<GetAdvancePOIDetails>> call, Response<ArrayList<GetAdvancePOIDetails>> response) {
                pb_dialog.dismiss();
                if (response.body() != null) {
                    getAdvancePOIDetaillist.clear();
                    getAdvancePOIDetaillist.addAll(response.body());
                    if (getAdvancePOIDetaillist.get(0).getResStatus()==true)
                    {

                        if(!getAdvancePOIDetaillist.get(0).getPOIToiletFacility().equalsIgnoreCase(""))
                        {
                            PoiOtherFacilityList poiOtherFacilityList = new PoiOtherFacilityList();
                            poiOtherFacilityList.setPoiAdvOtherId(0);
                            poiOtherFacilityList.setPoiAdvOtherName(getAdvancePOIDetaillist.get(0).getToiletServiceCategoryDetailses().get(0).getSerCatName());
                            poiOtherFacilityList.setPoiAdvOtherImage(getAdvancePOIDetaillist.get(0).getToiletServiceCategoryDetailses().get(0).getSerCatIcon());
                            getAdvancePOIDetaillist.get(0).getPoiOtherFacilityLists().add(poiOtherFacilityList);
                            getAdvancePOIDetaillist.get(0).setPOIOtherFacility(getAdvancePOIDetaillist.get(0).getPOIOtherFacility()+",0");
                        }
                        explorePOIAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Constants.show_error_popup(getActivity(), ""+getAdvancePOIDetaillist.get(0).getResDescription(), error_layout);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<GetAdvancePOIDetails>> call, Throwable t) {
                pb_dialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void initVar(View v) {
        POIListing_listing=(ListView)v.findViewById(R.id.POIListing_listing);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ll_nearby_places_header=(LinearLayout)v.findViewById(R.id.ll_nearby_places_header);
        iv_like_icon=(ImageView)v.findViewById(R.id.iv_like_icon);
        iv_share_img=(ImageView)v.findViewById(R.id.iv_share_img);
        main_fl=(FrameLayout) v.findViewById(R.id.main_fl);
        main_ll_first=(LinearLayout) v.findViewById(R.id.main_ll_first);
        main_ll_second=(LinearLayout) v.findViewById(R.id.main_ll_second);
        iv_nearby_activities_poi=(ImageView)v.findViewById(R.id.iv_nearby_activities_poi);
        iv_gallary_poi=(ImageView)v.findViewById(R.id.iv_gallary_poi);
        iv_required_time_poi=(ImageView)v.findViewById(R.id.iv_required_time_poi);
        tv_nearby_activities_poi=(TextView)v.findViewById(R.id.tv_nearby_activities_poi);
        tv_from_to_month_poi=(TextView)v.findViewById(R.id.tv_from_to_month_poi);
        tv_required_time_poi=(TextView)v.findViewById(R.id.tv_required_time_poi);
        tv_nearby_activities_num_poi=(TextView)v.findViewById(R.id.tv_nearby_activities_num_poi);
        tv_rating_poi_detail=(TextView)v.findViewById(R.id.tv_rating_poi_detail);
        iv_from_to_month_poi=(ImageView)v.findViewById(R.id.iv_from_to_month_poi);
        img_circle_indicator=(CirclePageIndicator)v.findViewById(R.id.img_circle_indicator);
        tv_add_to_my_plan_attraction=(TextView)v.findViewById(R.id.tv_add_to_my_plan_attraction);

    }

    private void setdPOIdetail() {
        int pos=Integer.parseInt(posi);
        if (explorePOIList.size()>0 && pos<explorePOIList.size())
        {
            if (posi.equalsIgnoreCase(String.valueOf(posi)))
            {
              //  ((ExploreActivity)getActivity()).tv_text_header.setText(explorePOIList.get(pos).getDesName());
                if (explorePOIList.get(pos).getPOIBestTimeFrom().replace("null","") != null && explorePOIList.get(pos).getPOIBestTimeTo().replace("null","")!=null) {

                    tv_from_to_month_poi.setText(Constants.getMonthName(Integer.parseInt(explorePOIList.get(pos).getPOIBestTimeFrom())) + " to " + Constants.getMonthName(Integer.parseInt(explorePOIList.get(pos).getPOIBestTimeTo())));
                }
                tv_rating_poi_detail.setText(explorePOIList.get(pos).getPOIRating());
                tv_nearby_activities_num_poi.setText(explorePOIList.get(pos).getPOINoOfNearByAttraction()+"");
                if(explorePOIList.get(pos).getPOISpendTime()!=null) {
                    tv_required_time_poi.setText(explorePOIList.get(pos).getPOISpendTime()+" hrs");
                }
                if(explorePOIList.get(pos).getFavId()==0)
                {
                    iv_like_icon.setImageResource(R.drawable.like_icon_unselected_detail);
                }else
                {
                    iv_like_icon.setImageResource(R.drawable.like_icon_selected_detail);

                }
            }

        }
    }

    private void setmiddleimage() {
        lastscrollamount = scroll;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        lp.weight = 55 ;
        lp1.weight = 45 ;
        main_ll_second.setLayoutParams(lp);
        main_ll_first.setLayoutParams(lp1);
        Log.e("System out","in Middle  main first ll weight ::::"+lp1.weight+" second  ll weight ::::"+lp.weight);
        if ((lp1.weight<70 || lp1.weight==70) &&  lp1.weight>50) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._350sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else if ((lp1.weight<50 ||lp1.weight==50) && lp1.weight<40) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._285sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        } else if ((lp1.weight<40 ||lp1.weight==40) && lp1.weight<30) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._210sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else
        {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._250sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();
        }
        setContentSize("small");

    }

    private void settopimage() {
        isfrombottom=true;
        lastscrollamount = scroll;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        lp.weight = 40 ;
        lp1.weight = 60 ;
        Log.e("System out","in pager   height ::::"+ ((ExploreActivity) getActivity()).pager.getHeight()+" second  ll weight ::::"+ ((ExploreActivity) getActivity()).pager.getWidth());
        Log.e("System out","in BOttom if::::"+lp.weight+" second  ll weight ::::"+lp1.weight);
        main_ll_second.setLayoutParams(lp1);
        main_ll_first.setLayoutParams(lp);

        if ((lp.weight<70 || lp.weight==70) &&  lp.weight>50) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._350sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else if ((lp.weight<50 ||lp.weight==50) && lp.weight<40) {
            // ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._285sdp);
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._205sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        } else if ((lp.weight<40 ||lp.weight==40) && lp.weight<30) {
            // ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._210sdp);
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._170sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else
        {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._250sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();
        }
        setContentSize("small");
    }

    private void settooriginal() {
        lastscrollamount = scroll;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);

        lp.weight = 60 ;
        lp1.weight = 40 ;

        isfrombottom=false;


        Log.e("System out","in Top if::::"+lp.weight+" second  ll weight ::::"+lp1.weight);
        main_ll_second.setLayoutParams(lp1);
        main_ll_first.setLayoutParams(lp);


        if ((lp.weight<70 || lp.weight==70) &&  lp.weight>50) {

            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._350sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else if ((lp.weight<50 ||lp.weight==50) && lp.weight<40) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._285sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        } else if ((lp.weight<40 ||lp.weight==40) && lp.weight<30) {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._210sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();

        }
        else
        {
            ((ExploreActivity) getActivity()).pager.getLayoutParams().height = (int) getResources().getDimension(R.dimen._250sdp);
            ((ExploreActivity) getActivity()).pager.requestLayout();
        }
        setContentSize("big");

    }

    private void setContentSize(String size) {
        Log.d("System out","SIZE IS_-----"+size);
        if(size.equalsIgnoreCase("big"))
        {
            iv_gallary_poi.setImageResource(R.drawable.photo_gallery_big);
            iv_nearby_activities_poi.setImageResource(R.drawable.nearby_places_icon);
            tv_nearby_activities_poi.setVisibility(View.VISIBLE);
            iv_from_to_month_poi.setImageResource(R.drawable.best_time_visit_icon);
           // iv_required_time_poi.setImageResource(R.drawable.required_time_icon);
            //  tv_from_to_month_poi.setTextSize((int)getResources().getDimension(R.dimen._6sdp));
            // tv_required_time_poi.setTextSize((int)getResources().getDimension(R.dimen._6sdp));

        }
        else
        {
            iv_gallary_poi.setImageResource(R.drawable.photo_gallery_small);
            tv_nearby_activities_poi.setVisibility(View.GONE);
         //   iv_required_time_poi.setImageResource(R.drawable.required_time_icon);
            iv_nearby_activities_poi.setImageResource(R.drawable.nearby_icon_small);
            iv_from_to_month_poi.setImageResource(R.drawable.best_time_icon_small);
            //  tv_from_to_month_poi.setTextSize((int)getResources().getDimension(R.dimen._4sdp));
            //  tv_required_time_poi.setTextSize((int)getResources().getDimension(R.dimen._4sdp));

        }
    }
    protected int getScroll(ListView listView) {// as list recycles views , getscrollY wont give us how much it has scrolled, hence we use this hack
        firstChildInList = listView.getChildAt(0);
        if (firstChildInList == null) return 0;
        return -firstChildInList.getTop() + listView.getFirstVisiblePosition() * firstChildInList.getHeight();
    }

    @Override
    public void onResume() {
        super.onResume();
        editor.putString("isinreadless","false").commit();
        editor.putString("as per fees","false").commit();
        editor.putString("know more timings","false").commit();
        ((ExploreActivity)getActivity()).tv_text_header.setText(name);
        ((ExploreActivity)getActivity()).pager.setVisibility(View.VISIBLE);
        //((ExploreActivity)getActivity()).collapsing_toolbar.setTitle("Coorg");
        ((ExploreActivity)getActivity()).exploreheadersearch.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_img_type_bottom.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).ll_text_header.setVisibility(View.VISIBLE);
        ((ExploreActivity)getActivity()).rl_main_heder.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).collapsing_toolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); // transperent color = #00000000
        ((ExploreActivity)getActivity()).ll_filter_header.setVisibility(View.GONE);
        settooriginal();
        if (!TextUtils.isEmpty(posi))
        {
            setdPOIdetail();
            AdvancePOIDetail();
        }
      explorePOIAdapter=new ExplorePOIAdapter(getActivity(),explorePOIList);
        POIListing_listing.setAdapter(explorePOIAdapter);

        // ((ExploreActivity)getActivity()).drawerLayout.setBackgroundColor(getResources().getColor(R.color.colorWhite));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        editor.putString("isinreadless","false").commit();
        editor.putString("as per fees","false").commit();
        editor.putString("know more timings","false").commit();

        ((ExploreActivity)getActivity()).pager.setVisibility(View.GONE);
        ((ExploreActivity)getActivity()).pager.getLayoutParams().height= (int)getResources().getDimension(R.dimen._350sdp);
        ((ExploreActivity)getActivity()).pager.requestLayout();

        // ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
        // ((ExploreActivity)getActivity()).drawerLayout.setBackgroundColor(getResources().getColor(R.color.transparent));

    }




    private class ExplorePOIAdapter extends BaseAdapter {
        Context context;
        ArrayList<ExplorePOI> explorePOIList=new ArrayList<ExplorePOI>();
        int selectedposition;
        ExplorePOIAdapter(Context context,ArrayList<ExplorePOI> explorePOIList) {
            this.context=context;
            this.explorePOIList=explorePOIList;
            selectedposition = Integer.parseInt(posi);
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.poi_detail_cell, null);

            final TextView tv_text_poi=(TextView)v.findViewById(R.id.tv_text_poi);
            final  TextView  tv_category_name_poi=(TextView)v.findViewById(R.id.tv_category_name_poi);
            final  TextView  tv_contact_no_poi=(TextView)v.findViewById(R.id.tv_contact_no_poi);
            final  TextView  tv_working_hrs_poi=(TextView)v.findViewById(R.id.tv_working_hrs_poi);
            final  TextView  tv_address_poi=(TextView)v.findViewById(R.id.tv_address_poi);
            final  TextView  tv_holidays=(TextView)v.findViewById(R.id.tv_holidays);
            final  TextView  tv_photography=(TextView)v.findViewById(R.id.tv_photography);
            final  TextView  tv_parking=(TextView)v.findViewById(R.id.tv_parking);
            final  ImageView  iv_parking=(ImageView) v.findViewById(R.id.iv_parking);
            final  ImageView  iv_photography=(ImageView) v.findViewById(R.id.iv_photography);
            final  LinearLayout  ll_holidays=(LinearLayout) v.findViewById(R.id.ll_holidays);
            final  LinearLayout  ll_local_holidays=(LinearLayout) v.findViewById(R.id.ll_local_holidays);
            final  LinearLayout  ll_state_holidays=(LinearLayout) v.findViewById(R.id.ll_state_holidays);
            final  LinearLayout  ll_national_holidays=(LinearLayout) v.findViewById(R.id.ll_national_holidays);
           // tv_add_to_my_plan_attraction=(TextView)v.findViewById(R.id.tv_add_to_my_plan_attraction);
            final LinearLayout  ll_timing_details_poi=(LinearLayout) v.findViewById(R.id.ll_timing_details_poi);
            tv_entry_fee_poi=(TextView)v.findViewById(R.id.tv_entry_fee_poi);
            final  TextView tv_know_more_poi=(TextView)v.findViewById(R.id.tv_know_more_poi);
            final  TextView tv_read_more_poi=(TextView)v.findViewById(R.id.tv_read_more_poi);
            final  TextView tv_timing_poi=(TextView)v.findViewById(R.id.tv_timing_poi);
            tv_about_attraction_name=(TextView)v.findViewById(R.id.tv_about_attraction_name);
            tv_about_attraction_name.setText("About "+name);
            final LinearLayout ll_entry_fee_detail=(LinearLayout)v.findViewById(R.id.ll_entry_fee_detail);
            final LinearLayout ll_timing=(LinearLayout)v.findViewById(R.id.ll_timing);
            LinearLayout nearby_ll=(LinearLayout)v.findViewById(R.id.nearby_ll);
            LinearLayout nearby_toilet_ll_ll=(LinearLayout)v.findViewById(R.id.nearby_toilet_ll_ll);
            LinearLayout nearby_toilet_ll=(LinearLayout)v.findViewById(R.id.nearby_toilet_ll);
            LinearLayout ll_nearby_header=(LinearLayout)v.findViewById(R.id.ll_nearby_header);
            Boolean flafForTiming=false;

            final  LinearLayout  ll_contact_whole_poi=(LinearLayout) v.findViewById(R.id.ll_contact_whole_poi);
            final  LinearLayout  ll_address_poi=(LinearLayout) v.findViewById(R.id.ll_address_poi);
            final  LinearLayout  ll_phone_poi=(LinearLayout) v.findViewById(R.id.ll_phone_poi);

            tv_address_poi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle b=new Bundle();
                   /*

                    Intent intent = new Intent(VehicleDetailsRSA.this, ExploreActivity.class);*/
                 //   b.putString("destID", explorePOIList.get(0).getpo().toString());
                    b.putString("poiLat", explorePOIList.get(selectedposition).getPOILatitude().toString());
                    b.putString("poiLong", explorePOIList.get(selectedposition).getPOILongitude().toString());
                    b.putString("name", explorePOIList.get(selectedposition).getPOIName().toString());


                   // ((ExploreActivity)getActivity()).replace_fragmnet_bundle(new MapviewOfPlace(),b);
                    Intent i=new Intent(getActivity(),MapviewOfPlace.class);
                    i.putExtras(b);
                    startActivity(i);

                }
            });


            if(getAdvancePOIDetaillist.size()>0) {
                if (getAdvancePOIDetaillist.get(0).getPoiOtherFacilityLists()!=null)
                    addNearby((LinearLayout) nearby_ll, getAdvancePOIDetaillist,(LinearLayout) nearby_toilet_ll_ll);
                else
                    ll_nearby_header.setVisibility(View.GONE);
            }

            if(getAdvancePOIDetaillist.size()>0) {
             //   if (getAdvancePOIDetaillist.get(0).getToiletTypesAllArrayList().size() > 0) {
                    if (getAdvancePOIDetaillist.get(0).getPoiOtherFacilityLists() != null)
                        addOthereServicesThree((LinearLayout) nearby_toilet_ll, getAdvancePOIDetaillist);
                    else
                        nearby_toilet_ll_ll.setVisibility(View.GONE);
               // }
            }

            if(plan_type.length()>0)
            {
                tv_add_to_my_plan_attraction.setVisibility(View.GONE);

            }

            if(getAdvancePOIDetaillist.size()>0) {
                if (getAdvancePOIDetaillist.get(0).getPOIIsEntryFee() == false) {
                    tv_entry_fee_poi.setText("Free");
                }
                else
                {
                    setFees(tv_entry_fee_poi, "Payable", ll_entry_fee_detail);
                }
               /* if(getAdvancePOIDetaillist.get(0).getPOIStateHoliDays().equalsIgnoreCase("") && getAdvancePOIDetaillist.get(0).getPOINationalHoliDays().equalsIgnoreCase("") && getAdvancePOIDetaillist.get(0).getPOILocalHolidayMaster()==null )
                {
                    tv_holidays.setText("No");
                }else
                {
                    if(!getAdvancePOIDetaillist.get(0).getPOIStateHoliDays().equalsIgnoreCase(""))
                    {
                        ll_state_holidays.setVisibility(View.VISIBLE);
                        ll_holidays.setVisibility(View.GONE);
                    }
                    if(!getAdvancePOIDetaillist.get(0).getPOINationalHoliDays().equalsIgnoreCase(""))
                    {
                        ll_national_holidays.setVisibility(View.VISIBLE);
                        ll_holidays.setVisibility(View.GONE);

                    }
                    if(getAdvancePOIDetaillist.get(0).getPOILocalHolidayMaster()!=null )
                    {
                        ll_local_holidays.setVisibility(View.VISIBLE);
                        ll_holidays.setVisibility(View.GONE);

                    }
                }*/
                if(getAdvancePOIDetaillist.get(0).getPOIIsPhotoGraphyAllow()==true)
                {
                    iv_photography.setImageResource(R.drawable.photo_allowed);
                    tv_photography.setText("Photography Allowed");

                }else
                {
                    iv_photography.setImageResource(R.drawable.photo_not_allowed);
                    tv_photography.setText("Photography Not Allowed");

                }


                if(getAdvancePOIDetaillist.get(0).getPOIIsParkingFacility()==true)
                {
                    iv_parking.setImageResource(R.drawable.parking_icon);
                    tv_parking.setText("Free Parking");

                }else
                {
                    iv_parking.setImageResource(R.drawable.no_parking);
                    tv_parking.setText("No Parking");

                }

            }


            if(!TextUtils.isEmpty(posi)) {
                if (explorePOIList.size()>0 && selectedposition<explorePOIList.size()) {
                    if(explorePOIList.get(selectedposition).getPOICustCareNo().equalsIgnoreCase("") && explorePOIList.get(selectedposition).getPOIAddress1().equalsIgnoreCase("") && explorePOIList.get(selectedposition).getCityname().equalsIgnoreCase("") && explorePOIList.get(selectedposition).getStatename().equalsIgnoreCase(""))
                    {
                       ll_contact_whole_poi.setVisibility(View.GONE);
                    }
                    else
                    {
                        ll_contact_whole_poi.setVisibility(View.VISIBLE);
                        tv_address_poi.setText(explorePOIList.get(selectedposition).getPOIAddress1()+", "+explorePOIList.get(selectedposition).getCityname()+", "+explorePOIList.get(selectedposition).getStatename());
                        if(explorePOIList.get(selectedposition).getPOICustCareNo().equalsIgnoreCase(""))
                        {
                            ll_phone_poi.setVisibility(View.GONE);
                        }else
                        {
                            ll_phone_poi.setVisibility(View.VISIBLE);
                            tv_contact_no_poi.setText(explorePOIList.get(selectedposition).getPOICustCareNo());

                        }




                    }

                   // tv_address_poi.setText(explorePOIList.get(selectedposition).getPOIAddress1()+", "+explorePOIList.get(selectedposition).getCityname()+", "+explorePOIList.get(selectedposition).getStatename());
                    // tv_contact_no_poi.setText(explorePOIList.get(selectedposition).getPOIContactPerson());
                  //  tv_contact_no_poi.setText(explorePOIList.get(selectedposition).getPOICustCareNo());
                    tv_contact_no_poi.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {
                            Constants.Calling(tv_contact_no_poi.getText().toString(),context);
                        }
                    });
                    tv_text_poi.setText(Html.fromHtml(explorePOIList.get(selectedposition).getPOIShortDescription()));
                    String CatName="";
                    for (int i=0;i<explorePOIList.get(selectedposition).getTravelTypelList().size();i++)
                    {

                        if (TextUtils.isEmpty(CatName))
                        {
                            CatName=explorePOIList.get(selectedposition).getTravelTypelList().get(i).getTTypeName();
                            tv_category_name_poi.setText(CatName);
                        }else
                        {
                            CatName=CatName+", "+explorePOIList.get(selectedposition).getTravelTypelList().get(i).getTTypeName();
                            tv_category_name_poi.setText(CatName);
                        }
                    }
                    try
                    {
                        if(explorePOIList.get(selectedposition).getPOIIsFullTime()==true)
                        {
                            tv_working_hrs_poi.setText(" 24 Hours");
                        }else {
                          //  tv_working_hrs_poi.setText(Constants.formatDate(explorePOIList.get(selectedposition).getPOIStartTime(), "HH:mm:ss", "hh:mm a") + " - " + Constants.formatDate(explorePOIList.get(selectedposition).getPOIEndTime(), "HH:mm:ss", "hh:mm a"));
                            tv_working_hrs_poi.setText(Constants.formatDate(explorePOIList.get(selectedposition).getPOIStartTime(), "HH:mm", "hh:mm a") + " - " + Constants.formatDate(explorePOIList.get(selectedposition).getPOIEndTime(), "HH:mm", "hh:mm a"));
                            //                            tv_working_hrs_poi.setText(Constants.formatDate(explorePOIList.get(selectedposition).getPOIStartTime(), "HH:mm:ss", "HH:mm a").replace("am","AM").replace("pm","PM") + " - " + Constants.formatDate(explorePOIList.get(selectedposition).getPOIEndTime(), "HH:mm:ss", "HH:mm a").replace("am","AM").replace("pm","PM"));

                        }
                    }
                    catch (ParseException e)
                    {
                        e.printStackTrace();
                    }


                }
            }
            iv_share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url= "https://goo.gl/lTciIP";

                    try {
                       // get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + explorePOIList.get(pos).getPOIImage() + "&width=" + (500) + "&height=" + (500),explorePOIList.get(pos).getPOIImage(),"Destination: "+explorePOIList.get(pos).getPOIName()+"\n"+"Best Time To Visit: "+Constants.getMonthName(Integer.parseInt(explorePOIList.get(pos).getPOIBestTimeFrom()))+" - "+Constants.getMonthName(Integer.parseInt(explorePOIList.get(position).getPOIBestTimeTo())));
                        get_bitmap_from_freco_download(Constants.TimbThumb_ImagePath + explorePOIList.get(pos).getPOIImage() + "&width=" + (500) + "&height=" + (500), explorePOIList.get(pos).getPOIImage(), "Take a peek at " + explorePOIList.get(pos).getPOIName() + ".\n" + "Plan your adventure filled roadtrip with Happy Roads."+Html.fromHtml("<a href='http://"+url+"'>"+url+"</a>"));


                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }



                }


            });


          /*  tv_timing_poi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(tv_timing_poi.getText().toString().equalsIgnoreCase("All Timings"))
                    {
                        editor.putString("know more timings","false").commit();
                        ll_timing_details_poi.setVisibility(View.GONE);
                        tv_know_more_poi.setText("Know More");
                        tv_timing_poi.setText("All Timings");
                    }
                }
            });*/
            if (getAdvancePOIDetaillist.size() > 0) {
                if (getAdvancePOIDetaillist.get(0).getPOITiminingMaster().size()>0) {
                    ll_timing.setVisibility(View.VISIBLE);
                } else {
                    ll_timing.setVisibility(View.GONE);

                }
            }
                tv_know_more_poi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                  //  editor.putString("know more timings","true").commit();
                    if(tv_know_more_poi.getText().toString().equalsIgnoreCase("[Know More]")) {
                        Log.d("System out","know more in if____"+tv_know_more_poi.getText().toString());
                        ll_timing_details_poi.setVisibility(View.VISIBLE);
                        tv_know_more_poi.setText("[Hide]");
                        if(!TextUtils.isEmpty(name)) {
                            {
                                if (ll_timing_details_poi.getChildCount() > 0) {
                                    ll_timing_details_poi.removeAllViews();
                                }
                                if (getAdvancePOIDetaillist.size() > 0) {
                                   if(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().size()>0)
                                    {
                                        for (int i = 0; i < getAdvancePOIDetaillist.get(0).getPOITiminingMaster().size(); i++) {
                                            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                            View v1;
                                            v1 = inflater.inflate(R.layout.timing_details_poi, null);
                                            TextView tv_weekdays_header = (TextView) v1.findViewById(R.id.tv_weekdays_header);
                                            TextView tv_morning_from = (TextView) v1.findViewById(R.id.tv_morning_from);
                                            TextView tv_morning_to = (TextView) v1.findViewById(R.id.tv_morning_to);
                                            TextView tv_evening_from = (TextView) v1.findViewById(R.id.tv_evening_from);
                                            TextView tv_evening_to = (TextView) v1.findViewById(R.id.tv_evening_to);
                                            LinearLayout ll_header_timings = (LinearLayout) v1.findViewById(R.id.ll_header_timings);
                                            LinearLayout ll_evening = (LinearLayout) v1.findViewById(R.id.ll_evening);
                                            LinearLayout ll_morning = (LinearLayout) v1.findViewById(R.id.ll_morning);
                                            if(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOITimingType().equalsIgnoreCase("Morning"))
                                            {
                                                tv_weekdays_header.setVisibility(View.INVISIBLE);
                                                ll_evening.setVisibility(View.GONE);
                                            }
                                            else if(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOITimingType().equalsIgnoreCase("Evening"))
                                            {
                                                tv_weekdays_header.setVisibility(View.INVISIBLE);
                                                ll_morning.setVisibility(View.GONE);
                                            }
                                            tv_weekdays_header.setText(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOITimingType());
                                            tv_morning_from.setText(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOIMorningFrom() + "");
                                            tv_morning_to.setText(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOIMorningTo() + "");
                                            tv_evening_from.setText(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOIEveningFrom() + "");
                                            tv_evening_to.setText(getAdvancePOIDetaillist.get(0).getPOITiminingMaster().get(i).getPOIEveningTo() + "");
                                            ll_timing_details_poi.addView(v1);
                                        }
                                    }else
                                   {

                                   }
                                }
                            }
                        }


                    }else
                    {
                        Log.d("System out","know more in Else____"+tv_know_more_poi.getText().toString());

                        ll_timing_details_poi.setVisibility(View.GONE);
                        tv_know_more_poi.setText("[Know More]");


                    }



                }
            });
          /*  if(sharedpreferences.getString("know more timings","false").equalsIgnoreCase("false"))
            {
                if(tv_timing_poi.getText().toString().equalsIgnoreCase("All Timings"))
                {
                    editor.putString("know more timings","false").commit();
                    ll_timing_details_poi.setVisibility(View.GONE);
                    tv_know_more_poi.setVisibility(View.VISIBLE);
                    tv_timing_poi.setText("All Timings");
                    settooriginal();
                    POIListing_listing.setSelectionAfterHeaderView();

                }
            }else
            {
                settopimage();
                editor.putString("know more timings","true").commit();
                ll_timing_details_poi.setVisibility(View.VISIBLE);
                tv_know_more_poi.setText("Hide");
                tv_timing_poi.setText("All Timings");
            }
*/

            if(!TextUtils.isEmpty(name)) {
                {
                    if (ll_entry_fee_detail.getChildCount() > 0) {
                        ll_entry_fee_detail.removeAllViews();

                    }

                    if (getAdvancePOIDetaillist.size() > 0) {
                        String[] FeesCatNameList = getAdvancePOIDetaillist.get(0).getPOIEntryFeeCategoryName().split(",");
                        String[] POIEntryFeeRegularRates = getAdvancePOIDetaillist.get(0).getPOIEntryFeeRegularRates().split(",");
                        String[] POIEntryFeeSeasonRates = getAdvancePOIDetaillist.get(0).getPOIEntryFeeSeasonRates().split(",");
                        for (int i = 0; i < FeesCatNameList.length; i++) {
                            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View v1;
                            v1 = inflater.inflate(R.layout.entry_fee_cell, null);
                            TableLayout tl_fees_header = (TableLayout) v1.findViewById(R.id.tl_fees_header);
                            TextView tv_type_value = (TextView) v1.findViewById(R.id.tv_type_value);
                            TextView tv_regular_value = (TextView) v1.findViewById(R.id.tv_regular_value);
                            TextView tv_season_value = (TextView) v1.findViewById(R.id.tv_season_value);
                            final TextView line = (TextView) v1.findViewById(R.id.line);
                            line.setVisibility(View.GONE);
                            if (i == 0) {
                                tl_fees_header.setVisibility(View.VISIBLE);
                            } else {
                                tl_fees_header.setVisibility(View.GONE);

                            }
                            tv_type_value.setText(FeesCatNameList[i]);
                            if(POIEntryFeeRegularRates.length>i)
                            {
                                tv_regular_value.setText(POIEntryFeeRegularRates[i]);

                            }else
                            {
                                tv_regular_value.setText("-");

                            }

                            // tv_regular_value.setText(POIEntryFeeRegularRates[i]);

                            if(POIEntryFeeSeasonRates.length>i)
                            {
                                tv_season_value.setText(POIEntryFeeSeasonRates[i]);
                            }else
                            {
                                tv_season_value.setText("-");
                            }

                            if (FeesCatNameList.length-1 == i)
                            {
                                line.setVisibility(View.VISIBLE);
                            }
                            ll_entry_fee_detail.addView(v1);
                        }
                    }
                }
            }
            if (!TextUtils.isEmpty(Html.fromHtml(explorePOIList.get(selectedposition).getPOILongDescription())))
            {
                tv_read_more_poi.setVisibility(View.VISIBLE);
            }
            else
            {
                tv_read_more_poi.setVisibility(View.GONE);
            }
            tv_read_more_poi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (tv_read_more_poi.getText().toString().equalsIgnoreCase("[Read Less]")) {
                        editor.putString("isinreadless","false").commit();
                        POIListing_listing.setSelectionAfterHeaderView();
                        tv_text_poi.setText(Html.fromHtml(explorePOIList.get(selectedposition).getPOIShortDescription()));
                        tv_read_more_poi.setText("[Read More]");
                        readmoreclicked=false;
                        POIListing_listing.smoothScrollToPosition(0);
                        settooriginal();

                    } else
                    {
                        readmoreclicked=true;
                        settopimage();
                        editor.putString("isinreadless","true").commit();
                        tv_text_poi.append(Html.fromHtml(explorePOIList.get(selectedposition).getPOILongDescription()));
                        tv_read_more_poi.setText("[Read Less]");

                    }
                }
            });

            if (sharedpreferences.getString("isinreadless","false").equalsIgnoreCase("true"))
            {
                settopimage();

                tv_text_poi.append(Html.fromHtml(explorePOIList.get(selectedposition).getPOILongDescription()));
                tv_read_more_poi.setText("[Read Less]");
            }
            else
            {
                settooriginal();
                POIListing_listing.setSelectionAfterHeaderView();
                //  tv_text_poi.setMaxLines(3);
                tv_text_poi.setText(Html.fromHtml(explorePOIList.get(selectedposition).getPOIShortDescription()));
                tv_read_more_poi.setText("[Read More]");
            }
            tv_entry_fee_poi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tv_entry_fee_poi.getText().toString().trim().equalsIgnoreCase("Free"))
                    {
                        editor.putString("as per fees","false").commit();
                    }
                    else {
                        editor.putString("as per fees","true").commit();
                        tv_entry_fee_poi.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                        if (ll_entry_fee_detail.getVisibility() == View.GONE) {
                            Log.d("System out","is gone ::::");
                            tv_entry_fee_poi.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_time, 0);
                            ll_entry_fee_detail.setVisibility(View.VISIBLE);
                        } else {
                            tv_entry_fee_poi.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_time, 0);
                            ll_entry_fee_detail.setVisibility(View.GONE);
                        }
                    }

                }
            });
            if(sharedpreferences.getString("as per fees","false").equalsIgnoreCase("true"))
            {
                settopimage();
                editor.putString("as per fees","true").commit();
                tv_entry_fee_poi.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                if (ll_entry_fee_detail.getVisibility() == View.GONE) {
                    tv_entry_fee_poi.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_time, 0);
                    ll_entry_fee_detail.setVisibility(View.VISIBLE);
                } else {
                    tv_entry_fee_poi.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_time, 0);
                    ll_entry_fee_detail.setVisibility(View.GONE);
                }
            }
            else {
                settooriginal();
                POIListing_listing.setSelectionAfterHeaderView();
                editor.putString("as per fees","false").commit();
            }
            tv_add_to_my_plan_attraction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("System out","Response of trip id "+tripid);
                   if(tripid == 0)
                   {
                       if(sessionManager.isLoggedIn())
                       {

                           GetTripList();
                       }else
                       {
                           LoginModule loginModule = new LoginModule(getActivity(),"favourite Destination");
                           loginModule.signIn();
                       }


                   }
                    else {


                       Integer pos1 = Integer.valueOf(posi);
                       if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)

                           if(!excepted_date.trim().equalsIgnoreCase("")) {
                               addData();
                           }
                           else {
                              Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                           }
                       else {
                           Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                       }



                   }



                }
            });

            return v;
        }

        public void Mytrip_Dialog(final Activity activity)
        {
            final SessionManager sessionManager1 = new SessionManager(activity);
             myTripDialog=new Dialog(activity);
            myTripDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            myTripDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myTripDialog.setContentView(R.layout.pop_up_my_trips);
            tv_start_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_start_date_my_trip);
            tv_return_date_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_return_date_my_trip);
            final TextView tv_for_better_dates=(TextView)myTripDialog.findViewById(R.id.tv_for_better_dates);
            final ImageView iv_swt_off_my_trip=(ImageView)myTripDialog.findViewById(R.id.iv_swt_off_my_trip);
            final LinearLayout ll_start_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_start_date_my_trip);
            final LinearLayout ll_return_date_my_trip=(LinearLayout)myTripDialog.findViewById(R.id.ll_return_date_my_trip);
      et_trip_name=(EditText)myTripDialog.findViewById(R.id.et_trip_name);
             av_tripname= (AutoCompleteTextView) myTripDialog.findViewById(R.id.av_tripname);
             tv_save_my_trip=(TextView)myTripDialog.findViewById(R.id.tv_save_my_trip);

            et_trip_name.setVisibility(View.GONE);
            av_tripname.setVisibility(View.VISIBLE);

            av_tripname.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                  /*  if (ShipperTvSelctCtyOriginPlaceOrder.getText().toString().trim().length() == 0 && ShipperTvLocalityOriginPlaceOrder.getText().toString().trim().length() > 0) {
                        Constants.showtoast(getActivity(), "Please select city for origin.", llplaceoredefrg);
                    } else {*/

                    av_tripname.setThreshold(1);
                    if (searchtripesname.size() > 0) {

                        searchtripesnameby = new ArrayList<String>();


                        for (int i = 0; i < searchtripesname.size(); i++) {
                            String str = av_tripname.getText().toString();
                            String origincityselected = ((ExploreActivity) getActivity()).edcityname.getText().toString().replace("Ex - ","").trim();
                            String cityname = searchtripesname.get(i);
                            Log.d("System out", " comparition " + searchtripesname.get(i).toLowerCase() + "      " + str.toLowerCase());
                            Log.d("System out", " comparition  value" + searchtripesname.get(i).toLowerCase().contains(str.toLowerCase()));
                            if (!searchtripesname.get(i).equalsIgnoreCase(origincityselected) && searchtripesname.get(i).toLowerCase().contains(str.toLowerCase())) {
                                searchtripesnameby.add(searchtripesname.get(i));
                            }
                        }
                        ArrayAdapter cityadapter = new ArrayAdapter<String>(getActivity(), R.layout.custontextview, searchtripesnameby);


                        av_tripname.setAdapter(cityadapter);

                    }

                    //}

                }

                @Override
                public void afterTextChanged(Editable s) {
                }


                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                }
            });
            av_tripname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int j, long l) {
                    for(int i=0;i<myTripArrayList.size();i++)
                    {
                        if(av_tripname.getText().toString().equalsIgnoreCase(myTripArrayList.get(i).getTripName()) && myTripArrayList.get(i).getTripDate()!=null)
                        {

                            try {

                                tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                                tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripReturnDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                                start_date=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");
                                Log.d("System out","tripdate___"+start_date +" "+start_time);
                                start_time=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","HH:mm");
                                Log.d("System out","tripdate___"+start_date +" "+start_time);
                                end_date  = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM ,yyyy");
                                Log.d("System out","returndate___"+end_date+"  "+end_time);
                                end_time = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm");

                                Log.d("System out","returndate___"+end_date+"  "+end_time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
          /*  av_tripname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(int i=0;i<myTripArrayList.size();i++)
                    {
                       if(av_tripname.getText().toString().equalsIgnoreCase(myTripArrayList.get(i).getTripName()))
                       {
                           try {
                               tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                               tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));

                           } catch (ParseException e) {
                               e.printStackTrace();
                           }
                       }
                    }

                }
            });*/

           /* av_tripname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        tv_start_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));
                        tv_return_date_my_trip.setText(Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(),"yyyy-MM-dd HH:mm:ss","dd MMM, yyyy hh:mm a"));

                        start_date=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");
                        Log.d("System out","tripdate___"+start_date +" "+start_time);
                        start_time=Constants.formatDate(myTripArrayList.get(i).getTripDate(),"yyyy-MM-dd HH:mm:ss","HH:mm");
                        Log.d("System out","tripdate___"+start_date +" "+start_time);
                        end_date  = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM ,yyyy");
                        Log.d("System out","returndate___"+end_date+"  "+end_time);
                        end_time = Constants.formatDate(myTripArrayList.get(i).getTripEndDateTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm");

                        Log.d("System out","returndate___"+end_date+"  "+end_time);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });*/
            Calendar now = Calendar.getInstance();


            dpd = DatePickerDialog.newInstance(
                    ExploreAttractionFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
            String currentDateandTime = sdf.format(new Date());

            Date date = null;
            try {
                date = sdf.parse(currentDateandTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, 0);
            dpd.setMinDate(calendar);
            tpd = TimePickerDialog.newInstance(
                    ExploreAttractionFragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),false
            );

            //  tpd.setMinTime(calendar.getTime().getHours(),calendar.getTime().getMinutes(),calendar.getTime().getSeconds());
            tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  if(tripFlag==true) {
                        select = "start";
                        dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                  //  }


                }

            });

            tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  if(tripFlag==true) {
                        select = "stop";


                        dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                   // }
                    //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                }
            });

/*
            iv_swt_off_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(tripFlag==false)
                    {
                        iv_swt_off_my_trip.setImageResource(R.drawable.swt_on);
                        tv_for_better_dates.setVisibility(View.GONE);
                        ll_start_date_my_trip.setAlpha(1);
                        ll_return_date_my_trip.setAlpha(1);
                        tv_start_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(tripFlag==true) {
                                    select = "start";
                                    dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                                }


                            }

                        });

                        tv_return_date_my_trip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(tripFlag==true) {
                                    select = "stop";


                                    dpd.show(activity.getFragmentManager(), "Datepickerdialog");
                                }
                                //   tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                            }
                        });
                        tripFlag=true;



                    }
                    else
                    {
                        iv_swt_off_my_trip.setImageResource(R.drawable.swt_off);
                        tv_for_better_dates.setVisibility(View.VISIBLE);
                        ll_start_date_my_trip.setAlpha((float) 0.6);
                        ll_return_date_my_trip.setAlpha((float) 0.6);
                        tripFlag=false;
                    }

                }
            });
*/

            if(exploreDestList.size()>0)
                get_bitmap_from_freco( Constants.TimbThumb_ImagePath+exploreDestList.get(DestinationPos).getDesImage()+"&width="+50+"&height="+50);
            tv_save_my_trip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                       /* if (tv_start_date_my_trip.getText().length() == 0) {
                            setclick = false;
                            Constants.show_error_popup(activity, "Select your trip dates.", activity.findViewById(R.id.error_layout));

                        } else*/ if (av_tripname.getText().toString().length() > 0) {

                            Log.d("Sizemytrip", "trip name size" + searchtripesname.size());
                            String flag = "";
                            for (int i = 0; i < searchtripesname.size(); i++) {
                                Log.d("Sizemytrip", "trip name size in for " + searchtripesname.get(i) + "   " + av_tripname.getText().toString().trim());
                                if (searchtripesname.get(i).equalsIgnoreCase(av_tripname.getText().toString().trim())) {
                                    String tripdate = start_date + " " + start_time;
                                    String returndate = end_date + " " + end_time;
                                    if (returndate.length() > 2) {
                                        if (CheckDates(tripdate, returndate)) {
                                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                            Date date = null;

                                            int pos = Integer.parseInt(posi);
                                            try {
                                                date = sdf.parse(tripdate);
                                                Calendar calendar = Calendar.getInstance();
                                                calendar.setTime(date);
                                                int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
                                                calendar.add(Calendar.HOUR, convertime[0]);
                                                calendar.add(Calendar.MINUTE, convertime[1]);
                                                Log.d("trip_Date_eta", date.toString());
                                                tripdate = sdf.format(calendar.getTime());
                                                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                            } catch (ParseException e) {
                                                e.printStackTrace();

                                            }
                                            if (CheckDates(tripdate, returndate)) {
                                                //  CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                                // searchtripesnameby.add(searchtripesname.get(i));
                                                Log.d("Sizemytrip", "trip name id " + searchtripesids.get(i));
                                                int check_trip_in_db = dbHandler.check_trip_in_local_db(Integer.parseInt(searchtripesids.get(i)));

                                                Log.d("Sizemytrip", check_trip_in_db + "");
                                                tripid = Integer.parseInt(searchtripesids.get(i));
                                                myTripDialog.dismiss();
                                                if (check_trip_in_db >= 1) {

                                                    Integer pos1 = Integer.valueOf(posi);

                                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)
                                                        if (!excepted_date.trim().equalsIgnoreCase("")) {
                                                            addData();
                                                        } else {
                                                            Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                                        }

                                                    else {
                                                        setclick = false;
                                                        Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                                    }
                                                } else {
                                                    getMyTrip(tripid);
                                                    // add_trip_in_to_database(position);
                                                }
                                                flag += "true";
                                            } else {
                                                try {
                                                    setclick = false;
                                                    //Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                                   // Constants.show_error_popup(activity, "Your destination requires " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + "hours of travel. Plan accordingly.", activity.findViewById(R.id.error_layout));
                                                    Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                                                } catch (Exception e) {
                                                    setclick = false;
                                                    e.printStackTrace();
                                                    Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                                }
                                            }
                                        } else {
                                            setclick = false;
                                            int pos = Integer.parseInt(posi);
                                            //  Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                           // Constants.show_error_popup(activity, "Your destination requires " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + "hours of travel. Plan accordingly. ", activity.findViewById(R.id.error_layout));
                                            Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule. ", activity.findViewById(R.id.error_layout));

                                        }
                                    } else {

                                        // Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                                        int pos = Integer.parseInt(posi);
                                        //CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                        // searchtripesnameby.add(searchtripesname.get(i));
                                        Log.d("Sizemytrip", "trip name id " + searchtripesids.get(i));
                                        int check_trip_in_db = dbHandler.check_trip_in_local_db(Integer.parseInt(searchtripesids.get(i)));

                                        Log.d("Sizemytrip", check_trip_in_db + "");
                                        tripid = Integer.parseInt(searchtripesids.get(i));
                                        myTripDialog.dismiss();
                                        if (check_trip_in_db >= 1) {

                                            Integer pos1 = Integer.valueOf(posi);

                                            if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)
                                                if (!excepted_date.trim().equalsIgnoreCase("")) {
                                                    addData();
                                                } else {
                                                    Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                                }

                                            else {
                                                setclick = false;
                                                Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                            }
                                        } else {
                                            getMyTrip(tripid);
                                            // add_trip_in_to_database(position);
                                        }
                                        flag += "true";
                                    }
                                } else {
                                    flag += "false";

                                }
                            }

                            if (!flag.contains("true")) {

                                String tripdate = start_date + " " + start_time;
                                String returndate = end_date + " " + end_time;

                                if (returndate.length() > 2) {
                                    if (CheckDates(tripdate, returndate)) {
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                        Date date = null;

                                        int pos = Integer.parseInt(posi);


                                        try {
                                            date = sdf.parse(tripdate);
                                            Calendar calendar = Calendar.getInstance();
                                            calendar.setTime(date);
                                            int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
                                            calendar.add(Calendar.HOUR, convertime[0]);
                                            calendar.add(Calendar.MINUTE, convertime[1]);
                                            Log.d("trip_Date_eta", date.toString());
                                            tripdate = sdf.format(calendar.getTime());
                                            //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                        } catch (ParseException e) {
                                            e.printStackTrace();

                                        }
                                        if (CheckDates(tripdate, returndate)) {
                                            setclick = false;
                                            CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                        } else {
                                            try {
                                                setclick = false;
                                               // Constants.show_error_popup(activity, "Your destination requires" + Constants.convert_minute_hrs(exploreDestList.get(DestinationPos).getDuration().doubleValue()) + " hours of travel. Plan accordingly.", activity.findViewById(R.id.error_layout));
                                                Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                                            } catch (Exception e) {
                                                setclick = false;
                                                e.printStackTrace();
                                                Constants.show_error_popup(activity, "Change start date & time of your return trip. ", activity.findViewById(R.id.error_layout));
                                            }
                                        }
                                    } else {
                                        setclick = false;
                                        int pos = Integer.parseInt(posi);
                                        //Constants.show_error_popup(activity, "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", activity.findViewById(R.id.error_layout));
                                      //  Constants.show_error_popup(activity, "Your destination requires" + Constants.convert_minute_hrs(exploreDestList.get(DestinationPos).getDuration().doubleValue()) + " hours of travel. Plan accordingly.", activity.findViewById(R.id.error_layout));
                                        Constants.show_error_popup(activity, "You require more time for travel/stay. Reschedule.", activity.findViewById(R.id.error_layout));
                                    }
                                } else {
                                    // Log.d("UserId", sessionManager1.get_Authenticate_User().getUmUdid());
                                    int pos = Integer.parseInt(posi);
                                    CreateTripApi(av_tripname.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), destId, sessionManager1.get_Authenticate_User().getUmUdid(), "", name, activity, myTripDialog);
                                }

                            }
                        } else {
                            setclick = false;
                            Constants.show_error_popup(activity, "Provide trip name.", activity.findViewById(R.id.error_layout));
                        }

                }

            });
            ImageView ImgClosepopup=(ImageView)myTripDialog.findViewById(R.id.ImgClosepopup);
            myTripDialog.show();
            ImgClosepopup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myTripDialog.dismiss();
                }
            });

        }
        public void add_trip_in_to_database(final int pos) {

            pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(false);
            pb_dialog.show();
            //   final SessionManager sessionManager1 = new SessionManager(getActivity());




            // String   trip_return_end_date="",trip_return_end_time="",trip_end_date_eta="",trip_end_time_eta="";



            try {
                if(mySpecificTripArrayList.get(pos).getTripEndDateTime()!=null) {
                    trip_end_date_eta = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripEndDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                    trip_end_time_eta = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripEndDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                }
                else
                {
                    trip_end_date_eta = "";
                    trip_end_time_eta = "";
                }
                if(mySpecificTripArrayList.get(pos).getTripReturnDateETA()!=null) {
                    trip_return_end_date = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDateETA(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");

                    trip_return_end_time = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDateETA(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                }
                else
                {
                    trip_return_end_date = "";
                    trip_return_end_time = "";
                }


            } catch (ParseException e) {
                e.printStackTrace();
                trip_return_end_date = "";
                trip_return_end_time = "";
                trip_end_date_eta = "";
                trip_end_time_eta = "";
            }
            try {
                if(mySpecificTripArrayList.get(pos).getTripDate()!=null) {
                    start_date = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                    start_time = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripDate(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                }
                else
                {
                    start_date = "";
                    start_time = "";
                }

            } catch (ParseException e) {
                e.printStackTrace();
                start_date = "";
                start_time = "";
            }
            try {
                if(mySpecificTripArrayList.get(pos).getTripReturnDate()!=null) {
                    end_date = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDate(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                    end_time = Constants.formatDate(mySpecificTripArrayList.get(pos).getTripReturnDate(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                }
                else
                {
                    end_date = "";
                    end_time = "";
                }

            } catch (ParseException e) {
                e.printStackTrace();
                end_date = "";
                end_time = "";
            }


            final String desurl = Constants.TimbThumb_ImagePath + mySpecificTripArrayList.get(pos).getEndDesImage() + "&width=" + 50 + "&height=" + 50;
            String originurl = Constants.TimbThumb_ImagePath + mySpecificTripArrayList.get(pos).getOrigindesImage() + "&width=" + 50 + "&height=" + 50;

            //stuff that updates ui
            ImageRequest imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(Uri.parse(originurl))
                    .setAutoRotateEnabled(true)
                    .build();

            ImagePipeline imagePipeline = Fresco.getImagePipeline();
            final DataSource<CloseableReference<CloseableImage>>
                    dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


            dataSource.subscribe(new BaseBitmapDataSubscriber()
            {

                @Override
                public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                    if (dataSource.isFinished() && bitmap != null) {
                        Log.d("Bitmap", "has come");

                        originbitmap = bitmap;

                        //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                        ImageRequest imageRequest1 = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(desurl))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline1 = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource1 = imagePipeline1.fetchDecodedImage(imageRequest1, getActivity());


                        dataSource1.subscribe(new BaseBitmapDataSubscriber() {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                                if (dataSource1.isFinished() && bitmap != null) {
                                    Log.d("Bitmap", "has come destination");
                                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                    Create_Trip create_trip = new Create_Trip();
                                    create_trip.setSource(mySpecificTripArrayList.get(pos).getOriginDesName());
                                    create_trip.setDestination(mySpecificTripArrayList.get(pos).getEndDesName());

                                    create_trip.setStartdate(start_date);
                                    create_trip.setStarttime(start_time);

                                    create_trip.setReturn_start_date(end_date);
                                    create_trip.setReturn_start_time(end_time);

                                    create_trip.setEnddate(trip_end_date_eta);
                                    create_trip.setEndtime(trip_end_time_eta);
                                    create_trip.setReturn_end_date(trip_return_end_date);
                                    create_trip.setReturn_end_time(trip_return_end_time);

                                    if (end_date.length() > 1) {
                                        create_trip.setReturnstatus(true);
                                    }


                                    create_trip.setTripname(mySpecificTripArrayList.get(pos).getTripName());
                                    create_trip.setSource_latitude(Double.valueOf(mySpecificTripArrayList.get(pos).getOriginLat()));
                                    create_trip.setSource_longitude(Double.valueOf(mySpecificTripArrayList.get(pos).getOriginLong()));
                                    create_trip.setDestination_latitude(Double.valueOf(mySpecificTripArrayList.get(pos).getEndDeslat()));
                                    create_trip.setDestination_longitude(Double.valueOf(mySpecificTripArrayList.get(pos).getEndDesLong()));


                                    create_trip.setKM(mySpecificTripArrayList.get(pos).getDistance() * 1000);
                                    create_trip.setTime(mySpecificTripArrayList.get(pos).getDuration().doubleValue());
                                    create_trip.setTripstatus(mySpecificTripArrayList.get(pos).getTripStatus());
                                    create_trip.setTriprating("");
                                    create_trip.setTripId(mySpecificTripArrayList.get(pos).getTripId());
                                    create_trip.setSourceId(mySpecificTripArrayList.get(pos).getTripGoingToStart());
                                    create_trip.setDestinationId(mySpecificTripArrayList.get(pos).getTripDestinationEnd());
                                    create_trip.setSource_image(Constants.getImageBytes(originbitmap));
                                    create_trip.setDesNoOfNearByAttraction(mySpecificTripArrayList.get(pos).getEnddesNoOfNearByAttraction());
                                    create_trip.setDestination_image(Constants.getImageBytes(bitmap));
                                    if(mySpecificTripArrayList.get(pos).getTripReturnKm()!=null)
                                        create_trip.setReturnKM(Double.valueOf(mySpecificTripArrayList.get(pos).getTripReturnKm()));
                                    if(mySpecificTripArrayList.get(pos).getTripReturnTime()!=null)
                                        create_trip.setReturnTime(Double.valueOf(mySpecificTripArrayList.get(pos).getTripReturnTime()));

                                    //   DBHandler dbHandler = new DBHandler(getActivity());

                                    dbHandler.add_Trip_into_Table(create_trip);

                                    Log.d("gettripplan",String.valueOf(mySpecificTripArrayList.get(pos).getGetTripPlans().get(0).getResStatus()));
                                    dataSource1.close();
                                    if(mySpecificTripArrayList.get(pos).getGetTripPlans().get(0).getResStatus())
                                    {
                                        int i;
                                        for (i=0; i < mySpecificTripArrayList.get(pos).getGetTripPlans().size(); i++)
                                        {
                                            //  if(!myTripArrayList.get(pos).getGetTripPlans().get(i).getTripWayType().contains("PoiNight"))

                                            add_poi_plan1(mySpecificTripArrayList.get(pos).getGetTripPlans().get(i),pos);
                                        }
                                       /* Bundle b = new Bundle();
                                        b.putInt("tripId", mySpecificTripArrayList.get(pos).getTripId());
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);
                                        pb_dialog.dismiss();*/
                                        pb_dialog.dismiss();

                                        Integer pos1 = Integer.valueOf(posi);
                                        if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)
                                            if(!excepted_date.trim().equalsIgnoreCase("")) {
                                                addData();
                                            }
                                            else {
                                                Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                            }

                                        else {
                                            Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                        }



                                    }
                                    else {
                                        pb_dialog.dismiss();

                                        Integer pos1 = Integer.valueOf(posi);
                                        if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)
                                            if(!excepted_date.trim().equalsIgnoreCase("")) {
                                                addData();
                                            }
                                            else {
                                                Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                            }

                                        else {
                                            Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                        }
                                       /* Bundle b = new Bundle();
                                        b.putInt("tripId", mySpecificTripArrayList.get(pos).getTripId());
                                        ((ExploreActivity) getActivity()).replace_fragmnet_bundle(new RoutePlanningFragment(), b);*/
                                    }
                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }

                                pb_dialog.dismiss();
                                Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                            }
                        }, CallerThreadExecutor.getInstance());

                        dataSource.close();
                    }
                }

                @Override
                public void onFailureImpl(DataSource dataSource) {
                    if (dataSource != null) {
                        dataSource.close();
                    }
                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));

                }
            }, CallerThreadExecutor.getInstance());




        }
        public  void add_poi_plan1(final MyTrip.GetTripPlan getTripPlan, final int pos)
        {
            if(getTripPlan.getGeneralFields().getImage().length()<=1)
                getTripPlan.getGeneralFields().setImage("20161013_0512atm-selected.png");
            if(!getTripPlan.getTripWayType().contains("PoiNight"))
            {

                Log.d("Bitmap", "has come poi");
                Create_Poi create_poi = new Create_Poi();
                create_poi.setTripId(tripid);
                create_poi.setPoiName(getTripPlan.getGeneralFields().getName());
                create_poi.setStayTime(getTripPlan.getTripSpendTime());
                create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
                create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
                create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() / 1000));
                create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));
                create_poi.setPoiStay(false);
                create_poi.setArrivalTime(0.0);
                create_poi.setDepartureTime(0.0);

      /*      if (bitmap != null) {
                if (getTripPlan.getTripWayType().contains("RO")) {
                    Bitmap icon = BitmapFactory.decodeResource(getResources(),
                            R.drawable.map_slider_icon4_selected);
                   // create_poi.setPoiImage(Constants.getImageBytes(icon));

                } else
                    //create_poi.setPoiImage(Constants.getImageBytes(bitmap));
            } else {
                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.drawable.map_slider_icon4_selected);
                create_poi.setPoiImage(Constants.getImageBytes(icon));
            }*/
                create_poi.setPoitype(getTripPlan.getTripWayType());
                create_poi.setWayPointId(getTripPlan.getTripWayPointId());

                create_poi.setPoiDesID(getTripPlan.getTripWayRefId());
                create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

                create_poi.setReturnstatus(getTripPlan.getTripWayIsReturnPlanned());
                create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());
                create_poi.setImagename( getTripPlan.getGeneralFields().getImage());
                if (getTripPlan.getTripLegNo() != null)
                    create_poi.setLegNo(Integer.valueOf(getTripPlan.getTripLegNo()));
                String resume_date = "", resume_time = "";



                try {
                    if (getTripPlan.getTripResumeDateTime() != null) {
                        resume_date = Constants.formatDate(getTripPlan.getTripResumeDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM ,yyyy");
                        resume_time = Constants.formatDate(getTripPlan.getTripResumeDateTime(), "yyyy-MM-dd'T'HH:mm:ss", "HH:mm");
                    } else {
                        resume_date = "";
                        resume_time = "";
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                    resume_date = "";
                    resume_time = "";
                }


                if (resume_date.length() > 1)
                    create_poi.setPoiStay(true);
                create_poi.setResumeDate(resume_date);
                create_poi.setResumeTime(resume_time);

                dbHandler.add_POI_into_Table(create_poi);
            }
            else
            {
                Create_Poi_Night create_poi = new Create_Poi_Night();
                create_poi.setPoiId(getTripPlan.getTripDesId());
                create_poi.setTripId(tripid);
                create_poi.setPoiName(getTripPlan.getGeneralFields().getName());

                create_poi.setStayTime(getTripPlan.getTripSpendTime());
                create_poi.setPoiLatitude(Double.valueOf(getTripPlan.getGeneralFields().getLat()));
                create_poi.setPoiLongitude(Double.valueOf(getTripPlan.getGeneralFields().getLongi()));
                create_poi.setKM(Math.ceil(getTripPlan.getTripDistanceInKm() / 1000));
                create_poi.setTime(Double.valueOf(getTripPlan.getTripDistanceInMinute()));
                create_poi.setPoiStay(false);
                create_poi.setArrivalTime(0.0);
                create_poi.setDepartureTime(0.0);

                //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));
                create_poi.setPoitype("PoiNight");
                create_poi.setWayPointId(getTripPlan.getTripWayPointId());

                create_poi.setPoiDesID(getTripPlan.getTripWayRefId());
                create_poi.setPoiServerId(getTripPlan.getTripWayRefId());

                create_poi.setReturnstatus(getTripPlan.getTripWayIsReturnPlanned());
                create_poi.setPoidescription(getTripPlan.getGeneralFields().getShortDescription());
                create_poi.setImagename( getTripPlan.getGeneralFields().getImage());
                Log.d("add_nightdes",String.valueOf(getTripPlan.getTripWayRefId()));
                dbHandler.add_Night_POI_into_Table(create_poi);


            }


            store_image(getTripPlan.getGeneralFields().getImage());




        }
        public void store_image(String url)
        {
            if(url.length()<=1)
                url="20161013_0512atm-selected.png";

            String originurl = Constants.TimbThumb_ImagePath + url + "&width=" + 50 + "&height=" + 50;

            //stuff that updates ui
            ImageRequest imageRequest2 = ImageRequestBuilder
                    .newBuilderWithSource(Uri.parse(originurl))
                    .setAutoRotateEnabled(true)
                    .build();

            ImagePipeline imagePipeline2 = Fresco.getImagePipeline();
            final DataSource<CloseableReference<CloseableImage>>
                    dataSource2 = imagePipeline2.fetchDecodedImage(imageRequest2, this);

            Log.d("Bitmapurl", originurl+"has come poi");
            final String finalUrl = url;
            dataSource2.subscribe(new BaseBitmapDataSubscriber() {

                @Override
                public void onNewResultImpl(@Nullable final Bitmap bitmap) {


                    new ImageSaver(getActivity()).
                            setFileName(finalUrl).

                            setExternal(true).
                            save(bitmap);

                    Log.d("Bitmapurl", finalUrl+"has come poi");
                    dataSource2.close();


                }
                @Override
                public void onFailureImpl(DataSource dataSource) {
                    if (dataSource != null) {
                        dataSource.close();
                    }


                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }
            }, CallerThreadExecutor.getInstance());
        }

        private void getMyTrip(int TripId) {
            // get & set progressbar dialog

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(true);
            pb_dialog.show();
            String json = "[{\"TripUmID\":" + sessionManager.get_Authenticate_User().getUmId() + ",  \"PageNo\": 1, \"TripId\": "+TripId+",    \"PageSize\": 50 }]";
            Log.d("json_CreateTrip", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<ArrayList<MyTrip>> call = apiService.GetMyTrip(json);
            call.enqueue(new Callback<ArrayList<MyTrip>>() {
                @Override
                public void onResponse(Call<ArrayList<MyTrip>> call, Response<ArrayList<MyTrip>> response) {
                    if (response.body() != null) {


                        pb_dialog.dismiss();

                        if (response.body().get(0).getResStatus() == true) {//call OTP API here
                            mySpecificTripArrayList = new ArrayList<>();
                            mySpecificTripArrayList.addAll(response.body());
                            add_trip_in_to_database(0);
                            // set_adapter(trip_status);


                        } else {
                            Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                        }


                    }


                }

                @Override
                public void onFailure(Call<ArrayList<MyTrip>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();

                    pb_dialog.dismiss();
                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                    }

                }
            });

        }
        public void GetTripList()
        {

            // get & set progressbar dialog
            pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
            pb_dialog.setCancelable(true);
            pb_dialog.show();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            String json = "[{\"TripDestination_End\":" + destId + ",  \"TripUmID\": "+sessionManager.get_Authenticate_User().getUmId() +" }]";
            Log.d("json_CreateTrip", json);
            //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
            Call<ArrayList<TripListforPOIPlanning>> call = apiService.GetMyTripByDestination(json);
            call.enqueue(new Callback<ArrayList<TripListforPOIPlanning>>() {
                @Override
                public void onResponse(Call<ArrayList<TripListforPOIPlanning>> call, Response<ArrayList<TripListforPOIPlanning>> response) {
                    if (response.body() != null) {


                        pb_dialog.dismiss();

                        if (response.body().get(0).getResStatus() == true) {//call OTP API here
                            myTripArrayList = new ArrayList<>();
                            myTripArrayList.addAll(response.body());
                           // set_adapter(trip_status);
                            searchtripesname.clear();
                            for (int i = 0; i < myTripArrayList.size(); i++) {
                                searchtripesname.add(myTripArrayList.get(i).getTripName());
                                searchtripesids.add(String.valueOf(myTripArrayList.get(i).getTripId()));
                            }

                            Mytrip_Dialog(getActivity());

                        } else {
                          //  Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                            myTripArrayList = new ArrayList<>();
                            Mytrip_Dialog(getActivity());

                        }


                    }


                }

                @Override
                public void onFailure(Call<ArrayList<TripListforPOIPlanning>> call, Throwable t) {
                    // Log error here since request failed

                    t.printStackTrace();

                    pb_dialog.dismiss();
                    if (!Constants.isInternetAvailable(getActivity())) {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                    } else {
                        Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                    }

                }
            });


        }

        private void setFees(TextView tv_text,String fees,LinearLayout s) {
            tv_text.setText(fees);
            tv_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


            tv_text.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_time, 0);
            s.setVisibility(View.GONE);



        }
    }

    public void addNearby(LinearLayout layout, ArrayList<GetAdvancePOIDetails> getAdvancePOIDetaillist1, final LinearLayout toilet)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);

            SimpleDraweeView first_img=(SimpleDraweeView) v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);

            ImageView first_arrow=(ImageView)v.findViewById(R.id.first_arrow);
            ImageView second_arrow=(ImageView)v.findViewById(R.id.second_arrow);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);

            if((i+2)<=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().size())
            {
                first_txt.setText(getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i).getPoiAdvOtherName());
                if(getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i).getPoiAdvOtherName().equalsIgnoreCase("Toilet"))
                {
                    first_arrow.setVisibility(View.VISIBLE);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(toilet.getVisibility() == View.VISIBLE)
                            {
                                toilet.setVisibility(View.GONE);
                            }
                            else
                            {
                                toilet.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                second_txt.setText(getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i+1).getPoiAdvOtherName());
                if(getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i+1).getPoiAdvOtherName().equalsIgnoreCase("Toilet"))
                {
                    second_arrow.setVisibility(View.VISIBLE);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(toilet.getVisibility() == View.VISIBLE)
                            {
                                toilet.setVisibility(View.GONE);
                            }
                            else
                            {
                                toilet.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
                first_img.setImageURI(Uri.parse(Constants.ImagePath + getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i).getPoiAdvOtherImage()));
                first_img.setImageURI(Uri.parse(Constants.ImagePath + getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i+1).getPoiAdvOtherImage()));
                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i).getPoiAdvOtherName());
                first_img.setImageURI(Uri.parse(Constants.ImagePath + getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i).getPoiAdvOtherImage()));
                if(getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(i).getPoiAdvOtherName().equalsIgnoreCase("Toilet"))
                {
                    first_arrow.setVisibility(View.VISIBLE);
                    v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(toilet.getVisibility() == View.VISIBLE)
                            {
                                toilet.setVisibility(View.GONE);
                            }
                            else
                            {
                                toilet.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }

            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }
    public void  get_bitmap_from_freco_download(final String url, final String name,final  String besttime)
    {
        Log.d("System out","sharing image path "+url);


//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");

                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    dataSource.close();
                    DownloadCode code = new DownloadCode(getActivity(),Constants.TimbThumb_ImagePath,name,bitmap,besttime);
                    code.share_image_text_GPLUS();


                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                ((ExploreActivity)getActivity()).drawerLayout.setBackgroundResource(R.drawable.main_bg);
            }
        }, CallerThreadExecutor.getInstance());

    }
    public void addOthereServicesThree(LinearLayout layout ,ArrayList<GetAdvancePOIDetails>  servicesPojoArrayList)
    {
      /*  String[] nearby=getAdvancePOIDetaillist1.get(0).getPoiOtherFacilityLists().get(0).getsplit(",");
        int caount=0;*/
        layout.removeAllViews();
        for(int i=0;i<servicesPojoArrayList.get(0).getToiletTypesAllArrayList().size();i++)
        {
            Log.d("System out","value of i"+i);

            View v;
            LayoutInflater infaltor = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infaltor.inflate(R.layout.nearby_facility_adapter_three_call, null);
            LinearLayout first_ll=(LinearLayout)v.findViewById(R.id.first_ll);
            LinearLayout second_ll=(LinearLayout)v.findViewById(R.id.second_ll);
            LinearLayout three_ll=(LinearLayout)v.findViewById(R.id.third_ll);

            SimpleDraweeView first_img=(SimpleDraweeView)v.findViewById(R.id.first_img);
            SimpleDraweeView second_img=(SimpleDraweeView)v.findViewById(R.id.second_img);
            SimpleDraweeView three_img=(SimpleDraweeView)v.findViewById(R.id.third_img);

            ImageView simple_first_img=(ImageView)v.findViewById(R.id.simple_first_img);
            ImageView simple_second_img=(ImageView)v.findViewById(R.id.simple_second_img);
            ImageView simple_three_img=(ImageView)v.findViewById(R.id.simple_third_img);

            first_img.setVisibility(View.GONE);
            second_img.setVisibility(View.GONE);
            three_img.setVisibility(View.GONE);

            simple_first_img.setVisibility(View.VISIBLE);
            simple_second_img.setVisibility(View.VISIBLE);
            simple_three_img.setVisibility(View.VISIBLE);

            TextView first_txt=(TextView)v.findViewById(R.id.first_txt);
            TextView second_txt=(TextView)v.findViewById(R.id.second_txt);
            TextView three_txt=(TextView)v.findViewById(R.id.third_txt);

            if((i+3)<=servicesPojoArrayList.get(0).getToiletTypesAllArrayList().size())
            {
                first_txt.setText(servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i).getToiletName());
                second_txt.setText(servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i+1).getToiletName());
                three_txt.setText(servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i+2).getToiletName());
                simple_first_img.setImageResource(R.drawable.round_black);
                simple_second_img.setImageResource(R.drawable.round_black);
                simple_three_img.setImageResource(R.drawable.round_black);
                if(!servicesPojoArrayList.get(0).getPOIToiletFacility().contains(""+servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i).getToiletName())) {

                    first_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_first_img.setImageResource(R.drawable.round_grey);
                }
                if(!servicesPojoArrayList.get(0).getPOIToiletFacility().contains(""+servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i+1).getToiletName())) {

                    second_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_second_img.setImageResource(R.drawable.round_grey);
                }
                if(!servicesPojoArrayList.get(0).getPOIToiletFacility().contains(""+servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i+2).getToiletName())) {

                    three_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_three_img.setImageResource(R.drawable.round_grey);
                }
                i+=2;
            }
            else if((i+2)<=servicesPojoArrayList.get(0).getToiletTypesAllArrayList().size())
            {
                three_ll.setVisibility(View.INVISIBLE);
                first_txt.setText(servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i).getToiletName());
                second_txt.setText(servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i+1).getToiletName());
                simple_first_img.setImageResource(R.drawable.round_black);
                simple_second_img.setImageResource(R.drawable.round_black);

                if(!servicesPojoArrayList.get(0).getPOIToiletFacility().contains(""+servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i).getToiletName())) {

                    first_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_first_img.setImageResource(R.drawable.round_grey);
                }
                if(!servicesPojoArrayList.get(0).getPOIToiletFacility().contains(""+servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i+1).getToiletName())) {

                    second_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_second_img.setImageResource(R.drawable.round_grey);
                }


                i++;
            }
            else {
                second_ll.setVisibility(View.INVISIBLE);
                three_ll.setVisibility(View.INVISIBLE);
                simple_first_img.setImageResource(R.drawable.round_black);
                first_txt.setText(servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i).getToiletName());



                if(!servicesPojoArrayList.get(0).getPOIToiletFacility().contains(""+servicesPojoArrayList.get(0).getToiletTypesAllArrayList().get(i).getToiletName())) {

                    first_txt.setTextColor(getResources().getColor(R.color.mdtp_light_gray));
                    simple_first_img.setImageResource(R.drawable.round_grey);
                }




            }
            Log.d("System out","value of i after add value "+i);




            layout.addView(v);

        }

    }

    private void Insert_Poi_Api( final Integer pos, final String waytype, final String wayId, final String duration, Double origindistance, final String imageurl, final Integer desid) {
        // get & set progressbar dialog


        Long distance;


        Double d = origindistance * 1000;

        distance = d.longValue();


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


       /* pb_dialog = Constants.get_dialog(getActivity(), R.layout.custom_progressbar);
        pb_dialog.setCancelable(true);
        pb_dialog.show();*/


        String json = "[{     \"TripWayType\": \"" + waytype + "\",     \"TripWayRefId\": \"" + wayId + "\",   \"TripWayTripid\": \"" + tripid + "\",   \"TripWayUmId\": \"" + sessionManager.get_Authenticate_User().getUmId() + "\",   \"TripWayComments\": \"\",      \"TripExtraKeys\":\"\",\"TripEstInDateTime\":\"\" ,\"TripEstOutTime\":\"\",\"TripDesId\":" + desid + ", \"TripIsPause\":\"false\" ,\"TripResumeDateTime\":\"\",\"TripDistanceInKm\":" + distance + ",\"TripDistanceInMinute\":" + duration + ",\"TripWayIsReturnPlanned\":" + false + "}]";

        Log.d("json_CreateTrip", json);

        Call<ArrayList<Insert_Poi>> call = apiService.InsertPoi(json);
        call.enqueue(new Callback<ArrayList<Insert_Poi>>() {
            @Override
            public void onResponse(Call<ArrayList<Insert_Poi>> call, final Response<ArrayList<Insert_Poi>> response) {
                if (response.body() != null) {


                    if (response.body().get(0).getResStatus() == true )
                    {


                        String url = Constants.TimbThumb_ImagePath + imageurl + "&width=" + 50 + "&height=" + 50;

                        //stuff that updates ui
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(url))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);


                        dataSource.subscribe(new BaseBitmapDataSubscriber() {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                                if (dataSource.isFinished() && bitmap != null) {

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            //stuff that updates ui




                                            DestinationPlan create_poi = new DestinationPlan();
                                            create_poi.setTripId(tripid);


                                            create_poi.setKM(Math.ceil(explorePOIList.get(pos).getDistance() * 1000));
                                            create_poi.setTime(Double.valueOf(explorePOIList.get(pos).getDuration()));


                                            create_poi.setArrivalTime(0.0);
                                            create_poi.setDepartureTime(0.0);

                                            new ImageSaver(getActivity()).
                                                    setFileName(imageurl).

                                                    setExternal(true).
                                                    save(bitmap);
                                            //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));


                                            create_poi.setPoiName(explorePOIList.get(pos).getPOIName());
                                            create_poi.setPoiLatitude(Double.valueOf(explorePOIList.get(pos).getPOILatitude()));
                                            create_poi.setPoiLongitude(Double.valueOf(explorePOIList.get(pos).getPOILongitude()));
                                            create_poi.setStayTime(Double.valueOf(explorePOIList.get(pos).getPOISpendTime().replace(":",".")));

                                            create_poi.setPoidescription(explorePOIList.get(pos).getPOIShortDescription());


                                            create_poi.setPoiServerId(explorePOIList.get(pos).getPOIID());

                                            create_poi.setPoitype("DesPlan");
                                            create_poi.setWayPointId(response.body().get(0).getTripWayPointId());

                                            create_poi.setPoiDesID(explorePOIList.get(pos).getPOINearbyDestination());


                                            create_poi.setImagename(imageurl);


                                            if(explorePOIList.get(pos).getPOIIsFullTime())
                                            {
                                                create_poi.setStarttime(24.00);
                                                create_poi.setEndtime(00.00);
                                            }
                                            else
                                            {
                                                String starttime="0:0" ,endtime="0:0";

                                                try {
                                                    starttime=Constants.formatDate(explorePOIList.get(pos).getPOIStartTime(),"HH:mm:ss","HH:mm");
                                                    endtime=Constants.formatDate(explorePOIList.get(pos).getPOIEndTime(),"HH:mm:ss","HH:mm");

                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                    starttime="0:0" ;endtime="0:0";
                                                }


                                                create_poi.setStarttime(Double.valueOf(starttime.replace(":",".")));
                                                create_poi.setEndtime(Double.valueOf(endtime.replace(":",".")));
                                            }


                                            dbHandler.add_Destination_into_Table(create_poi);


                                            pb_dialog.dismiss();


                                            dataSource.close();

                                            if (!TextUtils.isEmpty(posi)) {
                                                Log.d("System out","IF In Explore");
                                                if (((ExploreActivity) getActivity()).AddtoMyPlanList.contains(posi + "")) {
                                                    Log.d("System out","sub IF In Explore");
                                                }
                                                else
                                                {
                                                    Log.d("System out"," sub Else In Explore");
                                                    if (!((ExploreActivity) getActivity()).AddtoMyPlanList.contains(posi)) {
                                                        ((ExploreActivity) getActivity()).AddtoMyPlanList.add(posi);
                                                    }


                                                }
                                            }


                                            ExplorePlacesFragment explorePlacesFragment = new ExplorePlacesFragment();
                                            android.support.v4.app.FragmentTransaction changeTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                            changeTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);

                                            changeTransaction.replace(R.id.frame, explorePlacesFragment);
                                            changeTransaction.addToBackStack(null);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("from","add to my plan");
                                          //  bundle.putString("destLat",""+create_tripArrayList.get(0).getDestination_latitude());
                                           // bundle.putString("destLong",""+create_tripArrayList.get(0).getDestination_longitude());
                                            bundle.putString("destLat",exploreDestList.get(DestinationPos).getDesLatitude());
                                            bundle.putString("destLong",exploreDestList.get(DestinationPos).getDesLongitude());
                                            bundle.putString("destID",explorePOIList.get(Integer.parseInt(posi)).getPOINearbyDestination()+"");
                                            bundle.putString("placeName",destName);
                                            bundle.putInt("tripId",tripid);
                                            Log.d("System out","Response of trip id on create paas to explore "+tripid);
                                            explorePlacesFragment.setArguments(bundle);
                                            changeTransaction.commit();

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }
                                pb_dialog.dismiss();
                            }
                        }, CallerThreadExecutor.getInstance());


                    } else {
                        setclick = false;
                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().get(0).getResDescription(), error_layout);
                    }
                } else {
                    setclick = false;
                    pb_dialog.dismiss();
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Insert_Poi>> call, Throwable t) {
                // Log error here since request failed
                setclick = false;
                t.printStackTrace();
                pb_dialog.dismiss();



                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), error_layout);
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), error_layout);
                }

            }
        });


    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //  SimpleDateFormat formatter = new SimpleDateFormat("dd MMM ,yyyy");
        date = dayOfMonth + " " + (++monthOfYear) + "," + year;

        //  dateTextView.setText(date);


        // formatter.format(date);
        tpd.show(getActivity().getFragmentManager(), "TimePickerDialog");


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {


        String time = hourOfDay+":"+minute+"";

        try {
            date1= Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","dd MMM, yyyy hh:mm a");
            if(select.equalsIgnoreCase("start"))
            {
                start_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                start_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");

                Log.d("System out","date1___"+date1);
                String tripdate=start_date+" "+start_time;
                String returndate=end_date+" "+end_time;
                Log.d("System out","tripdate___"+tripdate);
                Log.d("System out","returndate___"+returndate);
                Date datenew = null;
                SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                try {
                    datenew = format.parse(date1);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
                String currentDateandTime = sdf1.format(new Date());

                Date datecurrent = null;
                try {
                    datecurrent = sdf1.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(datecurrent);
               // calendar1.add(Calendar.HOUR, 2);
                calendar1.add(Calendar.HOUR, 0);
                Log.d("System out","datenew___"+datenew);
                Log.d("System out","current "+datecurrent);
                Log.d("System out","ctime___"+calendar1.getTime());
                Log.d("System out","ctime___"+calendar1.getTime().after(datenew));
                Log.d("System out","ctime___"+datecurrent.before(datenew));

                //  currentDateandTime.set
                if (calendar1.getTime().after(datenew)) {
                    // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                   // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", tv_save_my_trip);


                }else  if (datenew.before(datecurrent))
                {
                   // Constants.show_error_popup(getActivity(),"Start time should be after 2 hours of current time.", tv_save_my_trip);
                    Constants.show_error_popup(getActivity(),"Select valid date & time.", tv_save_my_trip);

                }
              /*  if (calendar.getTime() > datenew.getTime()) {
                    tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
                }*/
                else {
                    tv_start_date_my_trip.setText(date1);
                    if (returndate.length() > 2) {
                        if (CheckDates(tripdate, returndate)) {
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                            Date date = null;
                            int pos = Integer.parseInt(posi);
                            try {
                                date = sdf.parse(tripdate);
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                int[] convertime = splitToComponentTimes(exploreDestList.get(pos).getDuration().doubleValue());
                                calendar.add(Calendar.HOUR, convertime[0]);
                                calendar.add(Calendar.MINUTE, convertime[1]);
                                Log.d("trip_Date_eta", date.toString());
                                tripdate = sdf.format(calendar.getTime());
                                //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                            } catch (ParseException e) {
                                e.printStackTrace();

                            }
                            if (CheckDates(tripdate, returndate)) {
                                // CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.get(pos).getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                            } else {
                                try {
                                   // Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need ", error_layout);
                                  //  Constants.show_error_popup(getActivity(), "Your destination requires " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + "hours of travel. Plan accordingly.", error_layout);
                                    Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule.", error_layout);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. ", error_layout);
                                }
                            }
                        } else {
                            int pos = Integer.parseInt(posi);
                           // Constants.show_error_popup(getActivity(), "Change start date & time of your return trip. you need " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + " reach at destination", error_layout);
                            //Constants.show_error_popup(getActivity(), "Your destination requires " + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + "hours of travel. Plan accordingly.", error_layout);
                            Constants.show_error_popup(getActivity(), "You require more time for travel/stay. Reschedule.", error_layout);
                        }
                    }
                }
            }
            else if(select.equalsIgnoreCase("stop"))
            {
                end_date=Constants.formatDate(date,"dd MM,yyyy","dd MMM ,yyyy");
                end_time=Constants.formatDate(date+" "+time,"dd MM,yyyy HH:mm","HH:mm");
                tv_return_date_my_trip.setText(date1);

                if (av_tripname.getText().toString().length() > 0) {
                    if (tv_start_date_my_trip.getText().length() == 0 && tv_return_date_my_trip.getText().toString().length() > 1) {

                        Constants.show_error_popup(getActivity(),"Select your trip dates.", error_layout);
                    }

                    else
                    {
                        String tripdate=start_date+" "+start_time;
                        String returndate=end_date+" "+end_time;

                        if(returndate.length()>2)
                        {
                            if(CheckDates(tripdate,returndate))
                            {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
                                Date date = null;
                                int pos=Integer.parseInt(posi);
                                try {
                                    date = sdf.parse(tripdate);
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(date);
                                    int[] convertime = splitToComponentTimes(exploreDestList.get(pos).getDuration().doubleValue());
                                    calendar.add(Calendar.HOUR, convertime[0]);
                                    calendar.add(Calendar.MINUTE, convertime[1]);
                                    Log.d("trip_Date_eta",date.toString());
                                    tripdate=sdf.format(calendar.getTime());
                                    //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
                                } catch (ParseException e) {
                                    e.printStackTrace();

                                }
                                if(CheckDates(tripdate,returndate)) {
                                    // CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.get(pos).getDesID().toString(), sessionManager1.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);
                                }
                                else {
                                    try {

                                       Constants.show_error_popup(getActivity(),"Change start date & time of your return trip. you need " , error_layout);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Constants.show_error_popup(getActivity(),"Change start date & time of your return trip. ", error_layout);
                                    }
                                }
                            }
                            else
                            {
                                int pos=Integer.parseInt(posi);
                               // Constants.show_error_popup(getActivity(),"Your destination requires" + Constants.convert_minute_hrs(exploreDestList.get(pos).getDuration().doubleValue()) + "hours of travel. Plan accordingly. ", error_layout);
                                Constants.show_error_popup(getActivity(),"You require more time for travel/stay. Reschedule.", error_layout);
                            }
                        }
                        else {
                            Log.d("UserId", sessionManager.get_Authenticate_User().getUmUdid());
                           /* int pos=Integer.parseInt(posi);
                            CreateTripApi(et_trip_name.getText().toString(), String.valueOf(ExploreActivity.destination_exclude_id), exploreDestList.get(pos).getDesID().toString(), sessionManager.get_Authenticate_User().getUmUdid(), from, name, activity, myTripDialog);*/
                        }
                    }
                } else

                {
                    Constants.show_error_popup(getActivity(),"Please Enter Trip Name", error_layout);
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public static boolean CheckDates(String startDate, String endDate) {



        SimpleDateFormat dfDate = new SimpleDateFormat("dd MMM ,yyyy HH:mm");


        boolean b = false;

        try {
            if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                b = true;  // If start date is before end date.
            } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                b = true;  // If two dates are equal.
            } else {
                b = false; // If start date is after the end date.
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            Log.d("datecompare",String.valueOf(dfDate.parse(startDate).before(dfDate.parse(endDate))));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d("datecompare",e.toString());
        }
        return b;
    }
    public int[] splitToComponentTimes(Double biggy) {
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        int[] ints = {hours, mins, secs};
        return ints;
    }

    public void CreateTripApi(final String tripname, String sourceId, String destinationId, String userId, final String source, final String destination, final Activity activity, final Dialog myTripDialog) {
        // get & set progressbar dialog
        final SessionManager sessionManager1 = new SessionManager(activity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String tripdate=start_date+" "+start_time+":00";
        String tripreturndate=end_date+" "+end_time+":00";

        tripdate_eta="";trip_return_date_eta="";

        Log.d("start_date",tripdate);
        try {
            tripdate=  Constants.formatDate(tripdate,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");

        } catch (ParseException e) {
            e.printStackTrace();
            tripdate="";

        }

        try{
            tripreturndate=  Constants.formatDate(tripreturndate,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        }
        catch (ParseException e){

            tripreturndate="";

        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        Date date = null;
        final int pos=Integer.parseInt(posi);
        try {
            date = sdf.parse(tripdate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
            calendar.add(Calendar.HOUR, convertime[0]);
            calendar.add(Calendar.MINUTE, convertime[1]);
            Log.d("trip_Date_eta",date.toString());
            tripdate_eta=sdf.format(calendar.getTime());
            //  tripdate_eta=  Constants.formatDate(dateeta,"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
            tripdate_eta="";

        }
        try {
            date = sdf.parse(tripreturndate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int[] convertime = splitToComponentTimes(exploreDestList.get(DestinationPos).getDuration().doubleValue());
            calendar.add(Calendar.HOUR, convertime[0]);
            calendar.add(Calendar.MINUTE, convertime[1]);
            trip_return_date_eta=sdf.format(calendar.getTime());
            //  trip_returndate_eta=  Constants.formatDate(dateeta,"dd MMM ,yyyy HH:mm:ss","yyyy-MM-dd HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_date_eta="";

        }


        pb_dialog = Constants.get_dialog(activity, R.layout.custom_progressbar);
        pb_dialog.setCancelable(false);
        pb_dialog.show();

        String json = "[{ \"TripName\": \"" + tripname + "\",\"TripDate\":\"" + tripdate  + "\",\"TripEndDateTime\":\"" + tripdate_eta+"\",\"TripReturnDate\":\"" + tripreturndate +"\",\"TripReturnDateETA\":\"" + trip_return_date_eta   +"\", \"TripGoingTo_Start\": "+sourceId+",\"TripDestination_End\":"+destinationId+", \"TripUmID\":"+sessionManager1.get_Authenticate_User().getUmId()+" }]";
        Log.d("json_CreateTrip",json);
        //  String json="[{     \"umMobile\": \"918401587127\",   \"umFirstName\": \"test\",   \"umLastName\": \"test\",   \"umEmailId\": \"saurin.vala1@gmail.com\",   \"umPassword\": \"1234aA\",   \"umDeviceType\": \"iphone\",   \"umUDID\": \"max200Min0\",     \"umProfilePhoto\": \"max200FilenameMin0.jpg\",   \"umDOB\": \"2015-11-30 00:00:00\",   \"umDescription\": \"lorem empsum \",       \"umLocation\": \"Max200Min0\",   \"umVehicleNumber\": \"Max20Min0\", \"umLat\":\"23.2323\",\"umLong\":\"72.01234\" ,\"umIsPushOn\":true, \"umTitle\":\"Mr.\" , \"umPetroCard\":\"123456789\", \"umReferralCode\":\"123456\" }]\n"
        Call<CreateTripApi> call = apiService.createTrip(json);
        call.enqueue(new Callback<CreateTripApi>() {
            @Override
            public void onResponse(Call<CreateTripApi> call, Response<CreateTripApi> response) {
                if (response.body() != null) {

                    CreateTripApi createTripApi = new CreateTripApi();
                    createTripApi = response.body();
                    if (createTripApi.getResStatus() == true) {//call OTP API here

                        // getDistanceOnRoad(23.034325,72.501297,23.725664,72.533523,createTripApi,tripname,source);
                        tripid=createTripApi.getTripId();
                        add_trip_in_to_database(source,tripname,createTripApi,activity,myTripDialog);

                    } else {
                        pb_dialog.dismiss();
                        Constants.show_error_popup(getActivity(), response.body().getResDescription(), error_layout);
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateTripApi> call, Throwable t) {
                // Log error here since request failed

                t.printStackTrace();
                pb_dialog.dismiss();

                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });


    }




    public void add_trip_in_to_database(final String sourcename, final String tripname, final CreateTripApi createTripApi, final Activity activity, final Dialog myTripDialog)
    {
        final   ArrayList<ExploreDestination> exploreDestList1=new ArrayList<>();

        final SessionManager sessionManager1 = new SessionManager(activity);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{     \"umId\": \"" + sessionManager1.get_Authenticate_User().getUmId() + "\",     \"radius\": \"999999\",   \"desName\": \"" + ExploreActivity.origincity + "\",   \"desLongitude\": \"999\",   \"desLatitude\": \"999\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":100,\"desNotIncludeThisID\":0 }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Log.d("System out", " json " + json);

        trip_return_end_date="";trip_return_end_time="";trip_end_date_eta="";trip_end_time_eta="";

        Log.d("tripdateeta",tripdate_eta+"ret"+trip_return_date_eta);
        try{
            trip_end_date_eta=Constants.formatDate(tripdate_eta,"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");

            trip_end_time_eta=Constants.formatDate(tripdate_eta,"yyyy-MM-dd HH:mm:ss","HH:mm");


        } catch (ParseException e) {
            e.printStackTrace();
            trip_end_date_eta="";trip_end_time_eta="";
        }


        try{

            trip_return_end_date=Constants.formatDate(trip_return_date_eta,"yyyy-MM-dd HH:mm:ss","dd MMM ,yyyy");

            trip_return_end_time=Constants.formatDate(trip_return_date_eta,"yyyy-MM-dd HH:mm:ss","HH:mm");

        } catch (ParseException e) {
            e.printStackTrace();
            trip_return_end_date="";trip_return_end_time="";
        }

        Log.d("tripdateeta1",tripdate_eta+"ret"+trip_return_date_eta+"trip_end_date_eta:"+trip_end_date_eta);


        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {


//                pb_dialog.dismiss();

                if (response.body() != null) {


                    exploreDestList1.clear();
                    exploreDestList1.addAll(response.body());
                    if (exploreDestList1.size() > 0) {


                        String url=Constants.TimbThumb_ImagePath+exploreDestList1.get(0).getDesImage()+"&width="+50+"&height="+50;

                        //stuff that updates ui
                        ImageRequest imageRequest = ImageRequestBuilder
                                .newBuilderWithSource(Uri.parse(url))
                                .setAutoRotateEnabled(true)
                                .build();

                        ImagePipeline imagePipeline = Fresco.getImagePipeline();
                        final DataSource<CloseableReference<CloseableImage>>
                                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




                        dataSource.subscribe(new BaseBitmapDataSubscriber()
                        {

                            @Override
                            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                                if (dataSource.isFinished() && bitmap != null) {
                                    Log.d("Bitmap", "has come");
                                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);


                                    final int pos=Integer.parseInt(posi);




                                    //  dbHandler.deleteAllTrip();
                                    //  String jsonOutput = sharedpreferences.getString("DestList","");
                                    //  Type listType = new TypeToken<ArrayList<ExploreDestination>>(){}.getType();
                                    //     ArrayList<ExploreDestination>  exploreDestList = gson.fromJson(jsonOutput, listType);

                                    Create_Trip create_trip = new Create_Trip();
                                    create_trip.setSource(ExploreActivity.origincity);
                                    create_trip.setDestination(exploreDestList.get(DestinationPos).getDesName());

                                    create_trip.setStartdate(start_date);
                                    create_trip.setStarttime(start_time);

                                    create_trip.setReturn_start_date(end_date);
                                    create_trip.setReturn_start_time(end_time);

                                    create_trip.setEnddate(trip_end_date_eta);
                                    create_trip.setEndtime(trip_end_time_eta);
                                    create_trip.setReturn_end_date(trip_return_end_date);
                                    create_trip.setReturn_end_time(trip_return_end_time);

                                    if(end_date.length()>1)
                                    {
                                        create_trip.setReturnstatus(true);
                                    }


                                    create_trip.setTripname(tripname);
                                    create_trip.setSource_latitude(Double.valueOf(exploreDestList1.get(0).getDesLatitude()));
                                    create_trip.setSource_longitude(Double.valueOf(exploreDestList1.get(0).getDesLongitude()));
                                    create_trip.setDestination_latitude(Double.valueOf(exploreDestList.get(DestinationPos).getDesLatitude()));
                                    create_trip.setDestination_longitude(Double.valueOf(exploreDestList.get(DestinationPos).getDesLongitude()));


                                    create_trip.setKM(exploreDestList.get(DestinationPos).getDistance()*1000);
                                    create_trip.setTime(exploreDestList.get(DestinationPos).getDuration().doubleValue());
                                    create_trip.setTripstatus(createTripApi.getTripStatus());
                                    create_trip.setTriprating(createTripApi.getTripRating());
                                    create_trip.setTripId(createTripApi.getTripId());
                                    create_trip.setSourceId(createTripApi.getTripGoingToStart());
                                    create_trip.setDestinationId(createTripApi.getTripDestinationEnd());
                                    create_trip.setSource_image(Constants.getImageBytes(bitmap));
                                    create_trip.setDesNoOfNearByAttraction(exploreDestList.get(DestinationPos).getDesNoOfNearByAttraction());
                                    create_trip.setDestination_image(Constants.getImageBytes(destinationbmp));
                                 //   DBHandler dbHandler=new DBHandler(activity);

                                    dbHandler.add_Trip_into_Table(create_trip);

                                 //   pb_dialog.dismiss();

                                    myTripDialog.dismiss();
                                    dataSource.close();
                                    Integer pos1 = Integer.valueOf(posi);
                                    create_tripArrayList.clear();
                                    create_tripArrayList = dbHandler.get_TRIP_By_TripId(tripid);
                                    excepted_date = create_tripArrayList.get(0).getReturn_start_date();
                                    try {

                                        excepted_time=Constants.formatDate(create_tripArrayList.get(0).getReturn_start_time(), "HH:mm", "hh:mm a");
                                    }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                    if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)

                                        if(!excepted_date.trim().equalsIgnoreCase("")) {
                                            addData();
                                        }
                                        else {
                                            Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
                                        }

                                    else {
                                        Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
                                    }
                                 /*   Bundle b=new Bundle();
                                    b.putInt("tripId",createTripApi.getTripId());
                                    b.putString("from","createtrip");
                                    ((ExploreActivity)activity).replace_fragmnet_bundle(new RoutePlanningFragment(),b);*/


                                }
                            }

                            @Override
                            public void onFailureImpl(DataSource dataSource) {
                                if (dataSource != null) {
                                    dataSource.close();
                                }

                            }
                        }, CallerThreadExecutor.getInstance());






                    } else {
                        Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(),  error_layout);
                    }

                    pb_dialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });
    }
    public void  get_bitmap_from_freco(final String url)
    {




//stuff that updates ui
        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(url))
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, this);




        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable final Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {
                    Log.d("Bitmap", "has come");
                    //  Bitmap bmp = Bitmap.createBitmap(bitmap);
                    destinationbmp=bitmap;
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    dataSource.close();
                }
                destinationbmp=null;
            }
        }, CallerThreadExecutor.getInstance());


    }

    public void set_destination_layout() {

        for (int i = 0; i < Constants.listDefaultValuesArrayList.size(); i++)

        {
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_Day_StartTime"))
                starttime = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());
            if (Constants.listDefaultValuesArrayList.get(i).getKeyName().equals("default_PerDay_Hours"))
                dayhours = Double.valueOf(Constants.listDefaultValuesArrayList.get(i).getValue());

        }
        try {

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
            Calendar c = Calendar.getInstance();
            Date date = format.parse(create_tripArrayList.get(0).getStartdate()+" "+create_tripArrayList.get(0).getEndtime());
            Date date1 = format.parse(create_tripArrayList.get(0).getStartdate()+" "+temptime);
            Date firstPoiDate=format.parse(create_tripArrayList.get(0).getStartdate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
            c.setTime(date);
            c.add(Calendar.HOUR, 2);
            Log.d("System out","End time of trip "+c.getTime());
            Log.d("System out","End time of day "+date1.getTime());
            if(c.getTimeInMillis()<=date1.getTime())
            {
                if(firstPoiDate.getTime()<=c.getTimeInMillis())
                {
                    destinationplanList.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY)+"."+((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                }
                else {

                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }





                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(0);
            }
            else
            {
                if (destinationplanList.get(0).getStarttime() == 24.00) {
                    destinationplanList.get(0).setArrivalTime(07.00);
                } else {
                    destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
                }


                destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
                destinationplanList.get(0).setDay(1);
            }


        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            destinationplanList.get(0).setDay(1);
            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());

        }
        if (destinationplanList.size() > 0) {

            Log.d("System out","Destination poi list size "+destinationplanList.size());
           /* if (destinationplanList.get(0).getStarttime() == 24.00) {
                destinationplanList.get(0).setArrivalTime(07.00);
            } else {
                destinationplanList.get(0).setArrivalTime(destinationplanList.get(0).getStarttime());
            }


            destinationplanList.get(0).setDepartureTime(destinationplanList.get(0).getArrivalTime() + destinationplanList.get(0).getStayTime());
            destinationplanList.get(0).setDay(0);*/



            Double tempDepart=destinationplanList.get(0).getArrivalTime();

            for (int j = 1; j < destinationplanList.size(); j++)
            {
                Double reqtime = destinationplanList.get(j - 1).getArrivalTime() + destinationplanList.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                destinationplanList.get(j).setArrivalTime(reqtime);
                destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                Log.d("System out","required time  "+reqtime);
                Log.d("System out","depature time  "+destinationplanList.get(j).getDepartureTime() + destinationplanList.get(0).getArrivalTime());
                if ((destinationplanList.get(j).getDepartureTime() - tempDepart) > dayhours)
                {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay() + 1);

                    if (destinationplanList.get(j).getStarttime() == 24.00) {
                        destinationplanList.get(j).setArrivalTime(07.00);
                    } else {
                        destinationplanList.get(j).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }
                    destinationplanList.get(j).setDepartureTime(destinationplanList.get(j).getArrivalTime() + destinationplanList.get(j).getStayTime());

                    tempDepart=destinationplanList.get(j).getArrivalTime();
                    //  dayhours = dayhours * destinationplanList.get(j).getDay();
                } else {
                    destinationplanList.get(j).setDay(destinationplanList.get(j - 1).getDay());
                }
                Log.d("System out","required time  "+ destinationplanList.get(j).getDay());


            }

        }

        noday = new ArrayList<>();
        noday.clear();

        //  noday.add(1);

        Integer day = 0;
       /* day = destinationplanList.get(0).getDay();
        noday.add(destinationplanList.get(0).getDay());*/
        if(destinationplanList.get(0).getDay() == 0)
        {
            day = destinationplanList.get(0).getDay();
            noday.add(destinationplanList.get(0).getDay());
        }

        for (int i = 0; i < destinationplanList.size(); i++) {

            destinationplanList.get(i).setPos(i);
            try {
                if (create_tripArrayList.get(0).getEnddate().length() > 0) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                    try {
                        Date date = format.parse(create_tripArrayList.get(0).getEnddate());
                        c.setTime(date);
                        c.add(Calendar.DATE, day);
                        destinationplanList.get(i).setPoiDate(format.format(c.getTime()));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }

                }
            } catch (Exception e) {
            }

            Log.d("days","pos"+day+"v2"+destinationplanList.get(i).getDay());

            if (day != destinationplanList.get(i).getDay() ) {
                day = destinationplanList.get(i).getDay();
                noday.add(destinationplanList.get(i).getDay());

            }


        }

        Log.d("System out","Reponse of set time of days count "+noday.size());
        for (int j = 0; j < noday.size(); j++) {
            AddDayWisePlan(j);
        }


        pb_dialog.dismiss();
        Validate_destination();
    }
    public void Validate_destination()
    {
        Date expecteddate = null;
        Date returndatenew = null;
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd MMM ,yyyy HH:mm");
            DecimalFormat f = new DecimalFormat("##.00");
            Date date = format1.parse(create_tripArrayList.get(0).getEnddate()+" "+f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".",":"));
            c.setTime(date);
            Log.d("check Destination","last_date "+date.toString());
            Log.d("check Destination","no days "+noday.get(noday.size()-1) +"   "+ create_tripArrayList.get(0).getEnddate() + "   "+f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".",":"));
            c.add(Calendar.DATE,noday.get(noday.size()-1));

            Log.d("check Destination", "last date " + create_tripArrayList.get(0).getEnddate() + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
            Log.d("check Destination", "return start time " + excepted_date + "Time  " + excepted_time);


            String date_excepted_date=excepted_date +" "+excepted_time;
            String date_return_excepted_date=return_excepted_date +" "+Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");
            Log.d("check Destination","new date "+c.getTime());
            expecteddate = format.parse(date_excepted_date);
            String temp=format.format(c.getTime());
            returndatenew= format.parse(temp);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Log.d("check Destination", "return start time " + expecteddate.toString());
        Log.d("check Destination", "new saved time " + returndatenew.toString());
        Log.d("check Destination", "new saved time " + returndatenew.after(expecteddate));
        if (returndatenew.after(expecteddate)) {
            // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            dbHandler.delete_Destination_ById(00);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Constants.show_error_popup(context,"No time for additional attractions. ", error_layout);
                                ((ExploreActivity)getActivity()).iv_home_icon_header.performClick();

                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();










        }
        else
        {
            dbHandler.delete_Destination_ById(00);
            Integer pos1 = Integer.valueOf(posi);
            if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)

                Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
            else {
                Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
            }

        }
    }
    public void AddDayWisePlan(int pos) {





        try {

            if (create_tripArrayList.get(0).getEnddate().length() > 0) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy");
                try {
                    Date date = format.parse(create_tripArrayList.get(0).getEnddate());
                    c.setTime(date);
                    c.add(Calendar.DATE, noday.get(pos));

                    return_excepted_date=format.format(c.getTime());

                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();


        }



        //  ll_day_wise_plan.removeAllViews();
        //  ll_add_places_to_plan.removeAllViews();

        //set time


        tempdestiDestinationPlanlist = new ArrayList<>();
        tempdestiDestinationPlanlist.clear();
        for (int i = 0; i < destinationplanList.size(); i++) {

            if (destinationplanList.get(i).getDay() == noday.get(pos)) {
                tempdestiDestinationPlanlist.add(destinationplanList.get(i));

            }

        }


        if (tempdestiDestinationPlanlist.size() > 0) {
            Calendar c = Calendar.getInstance();
            // set max time to start detination plan

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy HH:mm");

            try {


                Date date = format.parse(create_tripArrayList.get(0).getStartdate()+" "+create_tripArrayList.get(0).getEndtime());
                Date date1 = format.parse(create_tripArrayList.get(0).getStartdate()+" "+temptime);
                Date firstPoiDate=format.parse(create_tripArrayList.get(0).getStartdate()+" "+destinationplanList.get(0).getStarttime().toString().replace(".", ":"));
                c.setTime(date);
                c.add(Calendar.HOUR, 2);
                Log.d("System out","End time of trip "+c.getTime());
                Log.d("System out","End time of day "+date1.getTime());
                if(c.getTimeInMillis()<=date1.getTime() && pos == 0)
                {

                    if(firstPoiDate.getTime()<=c.getTimeInMillis())
                    {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(Double.parseDouble(c.get(Calendar.HOUR_OF_DAY) + "." + ((c.get(Calendar.MINUTE))<10?("0"+c.get(Calendar.MINUTE)):c.get(Calendar.MINUTE))));
                    }
                    else {

                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }



                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(0);
                }
                else
                {
                    if (tempdestiDestinationPlanlist.get(0).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(starttime);
                    } else {
                        tempdestiDestinationPlanlist.get(0).setArrivalTime(tempdestiDestinationPlanlist.get(0).getStarttime());
                    }


                    tempdestiDestinationPlanlist.get(0).setDepartureTime(tempdestiDestinationPlanlist.get(0).getArrivalTime() + tempdestiDestinationPlanlist.get(0).getStayTime());
                    tempdestiDestinationPlanlist.get(0).setDay(1);
                }


            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }



            Double tempDepart=tempdestiDestinationPlanlist.get(0).getArrivalTime();


            for (int j = 1; j < tempdestiDestinationPlanlist.size(); j++) {
                Double reqtime = tempdestiDestinationPlanlist.get(j - 1).getArrivalTime() + tempdestiDestinationPlanlist.get(j - 1).getStayTime() + Double.valueOf(Constants.convert_minute_hrs1(destinationplanList.get(j - 1).getTime()));
                tempdestiDestinationPlanlist.get(j).setArrivalTime(reqtime);
                tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());


                if ((tempdestiDestinationPlanlist.get(j).getDepartureTime() - tempDepart) > dayhours) {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay() + 1);
                    //      dayhours = dayhours * tempdestiDestinationPlanlist.get(j).getDay();
                    if (tempdestiDestinationPlanlist.get(j).getStarttime() == 24.00) {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(07.00);
                    } else {
                        tempdestiDestinationPlanlist.get(j).setArrivalTime(destinationplanList.get(0).getStarttime());
                    }
                    tempdestiDestinationPlanlist.get(j).setDepartureTime(tempdestiDestinationPlanlist.get(j).getArrivalTime() + tempdestiDestinationPlanlist.get(j).getStayTime());

                    tempDepart=tempdestiDestinationPlanlist.get(j).getArrivalTime();
                } else {
                    tempdestiDestinationPlanlist.get(j).setDay(tempdestiDestinationPlanlist.get(j - 1).getDay());
                }

            }


        }

       /* Date expecteddate = null;
        Date returndatenew = null;
        try {

            DecimalFormat f = new DecimalFormat("##.00");
            Log.d("System out", "last date " + return_excepted_date + "Time  " + Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a"));
            Log.d("System out", "return start time " + excepted_date + "Time  " + excepted_time);


            String date_excepted_date=excepted_date +" "+excepted_time;
            String date_return_excepted_date=return_excepted_date +" "+Constants.formatDate(f.format(tempdestiDestinationPlanlist.get(tempdestiDestinationPlanlist.size() - 1).getArrivalTime()).toString().replace(".", ":"), "HH:mm", "hh:mm a");

            SimpleDateFormat format = new SimpleDateFormat("dd MMM ,yyyy hh:mm a");

            expecteddate = format.parse(date_excepted_date);
            returndatenew= format.parse(date_return_excepted_date);
//            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        if (returndatenew.after(expecteddate)) {
            // tpd.show(getActivity().getFragmentManager(),"TimePickerDialog");
            dbHandler.delete_Destination_ById(00);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Constants.show_error_popup(context,"No time for additional attractions. ", error_layout);
                                ((ExploreActivity)getActivity()).iv_home_icon_header.performClick();

                            }
                        });
                    } catch (Exception e) {
                        Log.w("Exception in splash", e);
                    }

                }
            }).start();










        }
        else
        {
            dbHandler.delete_Destination_ById(00);
            Integer pos1 = Integer.valueOf(posi);
            if (dbHandler.check_des_trip_in_local_db(tripid, explorePOIList.get(pos1).getPOIID()) == 0)

                Insert_Poi_Api(pos1, "DesPlan", explorePOIList.get(pos1).getPOIID().toString(), explorePOIList.get(pos1).getDuration().toString(), explorePOIList.get(pos1).getDistance(), explorePOIList.get(pos1).getPOIImage(), explorePOIList.get(pos1).getPOINearbyDestination());
            else {
                Constants.show_error_popup(getActivity(), explorePOIList.get(pos1).getPOIName() + " is already included in your plan.", error_layout);
            }

        }*/


    }


    public void addData()
    {
        DestinationPlan create_poi = new DestinationPlan();
        create_poi.setTripId(tripid);


        create_poi.setKM(Math.ceil(explorePOIList.get(pos).getDistance() * 1000));
        create_poi.setTime(Double.valueOf(explorePOIList.get(pos).getDuration()));


        create_poi.setArrivalTime(0.0);

        //   create_poi.setPoiImage(Constants.getImageBytes(bitmap));


        create_poi.setPoiName(explorePOIList.get(pos).getPOIName());
        create_poi.setPoiLatitude(Double.valueOf(explorePOIList.get(pos).getPOILatitude()));
        create_poi.setPoiLongitude(Double.valueOf(explorePOIList.get(pos).getPOILongitude()));
        create_poi.setStayTime(Double.valueOf(explorePOIList.get(pos).getPOISpendTime().replace(":",".")));

        create_poi.setPoidescription(explorePOIList.get(pos).getPOIShortDescription());


        create_poi.setPoiServerId(explorePOIList.get(pos).getPOIID());

        create_poi.setPoitype("DesPlan");
        create_poi.setWayPointId(00);

        create_poi.setPoiDesID(explorePOIList.get(pos).getPOINearbyDestination());


        create_poi.setImagename("");


        if(explorePOIList.get(pos).getPOIIsFullTime())
        {
            create_poi.setStarttime(24.00);
            create_poi.setEndtime(00.00);
        }
        else
        {
            String starttime="0:0" ,endtime="0:0";

            try {
                starttime=Constants.formatDate(explorePOIList.get(pos).getPOIStartTime(),"HH:mm:ss","HH:mm");
                endtime=Constants.formatDate(explorePOIList.get(pos).getPOIEndTime(),"HH:mm:ss","HH:mm");

            } catch (ParseException e) {
                e.printStackTrace();
                starttime="0:0" ;endtime="0:0";
            }


            create_poi.setStarttime(Double.valueOf(starttime.replace(":",".")));
            create_poi.setEndtime(Double.valueOf(endtime.replace(":",".")));
        }


        dbHandler.add_Destination_into_Table(create_poi);
        destinationplanList.clear();
        destinationplanList = dbHandler.get_DestinationList(tripid);
        set_destination_layout();
    }

    public void destList()
    {

        final SessionManager sessionManager1 = new SessionManager(getActivity());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String json = "[{     \"umId\": \"" + sessionManager1.get_Authenticate_User().getUmId() + "\",     \"radius\": \"999999\",   \"desName\": \"" + name+ "\",   \"desLongitude\": \"999\",   \"desLatitude\": \"999\",      \"TravelTypeTag\":\"\",\"MobileLog\":\"xyz\" ,\"PageNo\":1,\"PageSize\":100,\"desNotIncludeThisID\":0 }]";
        // String json = "[{\"umId\":\"" + "1" + "\"}]";
        Log.d("System out", " json destlist___" + json);
        Call<ArrayList<ExploreDestination>> call = apiService.exploredestination(json);
        call.enqueue(new Callback<ArrayList<ExploreDestination>>() {
            @Override
            public void onResponse(Call<ArrayList<ExploreDestination>> call, Response<ArrayList<ExploreDestination>> response) {
                if (response.body() != null) {
                    if (response.body().size() > 0) {

                        exploreDestList.addAll(response.body());
                    } else {
                        Constants.show_error_popup(getActivity(), ""+exploreDestList.get(0).getResStatus(),  error_layout);
                    }

                    pb_dialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<ArrayList<ExploreDestination>> call, Throwable t) {
                // Log error here since request failed
                pb_dialog.dismiss();
                t.printStackTrace();
                if (!Constants.isInternetAvailable(getActivity())) {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.internet_error), getActivity().findViewById(R.id.error_layout));
                } else {
                    Constants.show_error_popup(getActivity(), getActivity().getString(R.string.server_error), getActivity().findViewById(R.id.error_layout));
                }

            }
        });
    }
}
